<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class CommandsConstants
 * @package Flendoc\AppBundle\Constants
 */
final class CommandsConstants
{
    const COMMAND_CITIES_IMPORT = 'cities:import';

    const COMMAND_UPDATE_USERS_EMAIL_VISIBILITY = 'settings:updateUsersEmailVisibilityCommand';
}
