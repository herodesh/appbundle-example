<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class AppConstants
 * @package Flendoc\AppBundle\Constants
 */
final class FaqConstants
{
    //######################################################
    //########### USERS
    //######################################################
    const FAQ_USERS_WHAT_IS_A_GHOST_USER = 'whatIsAGhostUser';

    const FAQ_USERS_HOW_DO_I_REGISTER = 'usersHowDoIRegister';

    const FAQ_USERS_RESET_PASSWORD = 'usersResetPassword';

    const FAQ_USERS_UPDATE_YOUR_PASSWORD = 'usersUpdateYourPassword';

    const FAQ_USERS_UPDATE_EMAIL_ADDRESS = 'usersUpdateEmailAddress';

    const FAQ_USERS_UPDATE_YOUR_PROFILE = 'usersUpdateYourProfile';

    //######################################################
    //########### MEDICAL CASES
    //######################################################
    const FAQ_MEDICAL_CASES_PATIENT_IDENTIFIER_LIST = 'medicalCasesPatientIdentifierList';

    const FAQ_MEDICAL_CASES_CREATE_NEW_CASE = 'createNewMedicalCase';

    const FAQ_MEDICAL_CASES_CREATE_NEW_CASE_WITHOUT_IMAGE = 'createNewMedicalCaseWithoutImage';

    const FAQ_MEDICAL_CASES_DELETE_MEDICAL_CASE = 'deleteMedicalCase';

    const FAQ_MEDICAL_CASES_PUBLISH_CASE = 'publishNewlyCreatedMedicalCase';

    //######################################################
    //########### DOCTORS VERIFICATION
    //######################################################
    const FAQ_DOCTORS_VERIFICATION_HOW_DO_I_BECOME_VERIFIED = 'doctorsVerificationHowDoIBecomeVerified';

    const FAQ_DOCTORS_VERIFICATION_WHY_DO_I_NEED_TO_BE_VERIFIED = 'doctorsVerificationWhyDoINeedToBeVerified';
}
