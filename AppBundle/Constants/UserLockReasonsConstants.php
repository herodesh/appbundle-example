<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class UserLockReasonsConstants
 * @package Flendoc\AppBundle\Constants
 */
final class UserLockReasonsConstants
{
    const USER_LOCK_FAILED_LOGIN_ATTEMPTS = 'failedLoginAttempts';

    // Add here new constants. Might be if a user is spamming or he is reported too many times...
    //const USER_LOCK_SPAM = 'spamLock';
}
