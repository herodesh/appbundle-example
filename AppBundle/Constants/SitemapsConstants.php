<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class SitemapsConstants
 * @package Flendoc\AppBundle\Constants
 */
class SitemapsConstants
{
    const SITEMAP_MAX_URL = 40000;

    const SITEMAPS_AWS_FOLDER = 'sitemaps';

    const SITEMAPS_MEDICAL_CASES = 'medical_cases';

    const SITEMAPS_MEDICAL_DICTIONARY = 'medical_dictionary';
}
