<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class AppConstants
 * @package Flendoc\AppBundle
 */
final class AppConstants
{
    /** True value */
    const APP_TRUE = 1;

    /** false value */
    const APP_FALSE = 0;

    /** @var string Google Recaptha Url */
    const GOOGLE_RECAPTCHA_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /** @var string Cortical Keywords extractor url */
    const CORTICAL_IO_KEYWORDS_URL = 'http://api.cortical.io/rest/text/keywords?retina_name=en_associative';

    /** @var string Reuters Calais Keywords generator url */
    const REUTERS_CALAIS_KEYWORDS_URL = 'https://api.thomsonreuters.com/permid/calais';

    /** Amazon S3 photo directory */
    const AMAZON_PHOTO_DIRECTORY = 'photos';

    /** Amazon S3 profile photos directory */
    const AMAZON_PROFILE_PHOTO_DIRECTORY = 'profile_photos';

    /** Amazon S3 resume photo */
    const AMAZON_RESUME_PHOTO_DIRECTORY = 'resume_photos';

    /** Amazon S3 medical cases images */
    const AMAZON_MEDICAL_CASES_IMAGE_DIRECTORY = 'medical_cases';

    /** @var string Amazon S3 medical dictionary images */
    const AMAZON_MEDICAL_DICTIONARY_IMAGE_DIRECTORY = 'medical_dictionary';

    /** Amazon S3 chat files */
    const AMAZON_CHATS_FILES_DIRECTORY = 'chat_files';

    /** Amazon S3 documents directory */
    const AMAZON_DOCUMENTS_DIRECTORY = 'documents';

    /** Language default iso */
    const LANGUAGE_DEFAULT_ISO = 'en';

    /** User type admins */
    const USER_TYPE_ADMINS = 'admins';

    /** User type doctors */
    const USER_TYPE_DOCTORS = 'doctors';

    /** User type patients */
    const USER_TYPE_PATIENTS = 'patients';

    /** Display if the action was ajax or not */
    const AJAX_ONLY = 'Not an ajax call';

    /** Upload tmp directory */
    const UPLOAD_TMP_FOLDER = '/tmp/Flendoc/tmpUploadDir';

    /** Upload Cache-Control time */
    const UPLOAD_CACHE_CONTROL = 60 * 60 * 24 * 30; //30 days

    /** @var float|int */
    const INACTIVE_USERS_END_DAYS = 30;

    /** Queries max results */
    const QUERY_MAX_RESULTS = 10;

    /** Defines the size of the random generated file name */
    const RANDOM_FILE_NAME_SIZE = 15;

    /** Application default image extension. We will try to convert all images into this format */
    const DEFAULT_IMAGE_EXTENSION = 'jpg';

    /** Image size full */
    const IMAGE_FULL_SIZE = 2048;

    /** Image size large */
    const IMAGE_LARGE_SIZE = 1024;

    /** Image size medium */
    const IMAGE_MEDIUM_SIZE = 650;

    /** Image size small */
    const IMAGE_SMALL_SIZE = 250;

    /** Image size thumb */
    const IMAGE_THUMB_SIZE = 150;

    /** Image size icon */
    const IMAGE_ICON_SIZE = 50;

    /** Minimum allowed files upload */
    const UPLOAD_FILES_MIN = 1;

    /** Maximum allowed files upload */
    const UPLOAD_FILES_MAX = 20;

    /** Default Cache expiration time - 1 day */
    const CACHE_DEFAULT_EXPIRE_TIME = 60 * 60 * 24 * 1;

    /** Process and display how many weeks the non pending medical cases will be active before deletion */
    const MEDICAL_CASE_EXPIRY_TIME_WEEKS = 2;

    /** Minimum number of characters allowed when editing or posting a new medical case */
    const MEDICAL_CASE_MINIMUM_DESCRIPTION_CHARACTERS = 100;

    /** Number of items to be displayed on chats per page */
    const CHAT_PAGE_ITEMS = 10;

    /** Number of items to be displayed on contacts per page */
    const PAGINATION_PAGE_ITEMS = 10;

    /** Number of minutes after which an email notification for new messages will be sent */
    const NEW_MESSAGE_EMAIL_NOTIFICATION_TIME = 60 * 20;

    /** Default period after a not published medical case will be delete - 2 weeks */
    const DELETE_NOT_PUBLISHED_MEDICAL_CASES_TIME = 60 * 60 * 24 * 14;

    /** Number of items to be displayed on header notifications dropdown */
    const HEADER_NOTIFICATIONS_DROPDOWN_ITEMS = 5;

    /** Number of items to be displayed on notifications page */
    const NOTIFICATIONS_PAGE_ITEMS = 20;

    /** The time after WebSocket Doctrine connection will be pinged */
    const WEB_SOCKETS_DOCTRINE_PING_TIME = 60 * 3;

    /** The number of links to be displayed by default in FAQ overview page */
    const FAQ_LINK_LIST_DISPLAY = 5;

    /** @var Batch size */
    const BATCH_SIZE = 500;

    /** @var int Login attempts until recaptcha is shown */
    const USER_LOGIN_ATTEMPTS_RECAPTCHA = 3;

    /** @var int Login attempts until the account is getting blocked */
    const USER_LOGIN_ATTEMPTS_BLOCK = 10;

    /** @var int Used in queries where a suggestion is required */
    const SUGGESTIONS_LIMIT = 3;
}
