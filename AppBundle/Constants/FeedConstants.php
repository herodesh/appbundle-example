<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class AppConstants
 * @package Flendoc\AppBundle
 */
final class FeedConstants
{
    /** @var string */
    const FEED_PROPERTY_PUBLIC = 'public';

    /** @var string */
    const FEED_PROPERTY_CONTACTS_ONLY = 'contacts';

    /** @var int */
    const FEED_DEFAULT_LIMIT = 5;

    /** @var string */
    const FEED_TYPE_FEED = 'feed';

    /** @var string */
    const FEED_TYPE_MEDICAL_CASE = 'medical_case';
}
