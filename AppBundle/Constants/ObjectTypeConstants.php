<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class ObjectTypeConstants
 * @package Flendoc\AppBundle
 */
final class ObjectTypeConstants
{
    /** @var string */
    const OBJECT_DATA_TYPE_MYSQL = 'odtm1';

    /** @var string */
    const OBJECT_DATA_TYPE_MONGO = 'odtm2';

    /** @var string */
    const OBJECT_MONGO_FEED = 'om3';

    /** @var string */
    //we use it like this because we need to call the views later on
    const OBJECT_MEDICAL_CASE = 'medical_case';

    /** @var string */
    const OBJECT_MEDICAL_DICTIONARY = 'medical_dictionary';

    /** @var string */
    //we use it like this because we need to call the views later on
    const OBJECT_DOCTOR_CHAT = 'doctor_chat';

    /** @var string */
    /*
     * we use it like this because we need to call the views later on
     *
     * There is no class ContactRequest that can be associated with this constant.
     * This type comes from Neo4J and in this case it is a node relationship.
     */
    const RELATIONSHIP_CONTACT_REQUEST = 'contact_request';

    /** @var string */
    /*
     * we use it like this because we need to call the views later on
     *
     * There is no class ContactRequest that can be associated with this constant.
     * This type comes from Neo4J and in this case it is a node relationship.
     */
    const RELATIONSHIP_CONTACT_REQUEST_ACCEPTED = 'contact_request_accepted';

    /** @var string */
    //we use it like this because we need to call the views later on
    const OBJECT_STATUS_UPDATE = 'status_update';

    /** @var string */
    const OBJECT_SHARE_ON_NEWSFEED = 'shared';
}
