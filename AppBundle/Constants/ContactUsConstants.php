<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class AppConstants
 * @package Flendoc\AppBundle\Constants
 */
final class ContactUsConstants
{
    const CU_ACCOUNT_PROBLEM = 'accountProblem';

    const CU_DELETE_ACCOUNT = 'deleteAccount';

    const CU_FAQ_REQUEST = 'faqRequest';

    const CU_OTHERS = 'others';
}
