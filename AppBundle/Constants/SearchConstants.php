<?php

namespace Flendoc\AppBundle\Constants;

/**
 * Class AppConstants
 * @package Flendoc\AppBundle\Constants
 */
final class SearchConstants
{
    const SEARCH_RESULTS = 2;

    const SEARCH_MAIN_RESULTS = 2;

    const SEARCH_TYPE_MEDICAL_CASES = 'search_medical_cases';

    const SEARCH_TYPE_MEDICAL_DICTIONARY = 'search_medical_dictionary';

    const SEARCH_TYPE_USERS = 'search_users';
}
