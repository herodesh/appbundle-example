<?php

namespace Flendoc\AppBundle\Entity\Reviews;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="reviews")
 */
class Reviews extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="review_content", type="text")
     */
    protected $reviewContent;

    /**
     * @ORM\Column(name="review_score", type="text")
     */
    protected $reviewScore;

    /*
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\UserBundle\Entity\User",
     *     inversedBy="reviews"
     * )
     * @ORM\JoinColumn(
     *     name="user_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    //protected $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReviewContent()
    {
        return $this->reviewContent;
    }

    /**
     * @param mixed $reviewContent
     */
    public function setReviewContent($reviewContent)
    {
        $this->reviewContent = $reviewContent;
    }

    /**
     * @return mixed
     */
    public function getReviewScore()
    {
        return $this->reviewScore;
    }

    /**
     * @param mixed $reviewScore
     */
    public function setReviewScore($reviewScore)
    {
        $this->reviewScore = $reviewScore;
    }

    /**
     * @return mixed
     */
    /*public function getUser()
    {
        return $this->user;
    }*/

    /**
     * @param mixed $user
     */
    /*public function setUser($user)
    {
        $this->user = $user;
    }*/
}
