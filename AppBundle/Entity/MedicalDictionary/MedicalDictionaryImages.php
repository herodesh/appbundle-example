<?php

namespace Flendoc\AppBundle\Entity\MedicalDictionary;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MedicalDictionaryImages
 * @package Flendoc\AppBundle\Entity\MedicalDictionary
 *
 * @ORM\Table(name="medical_dictionary_images")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalDictionary\MedicalDictionaryImagesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalDictionaryImages extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="photos", type="text")
     */
    protected $photos;

    /**
     * @ORM\Column(name="string_identifier", type="text", nullable=true)
     */
    protected $stringIdentifier;

    /**
     * @ORM\Column(name="photo_license", type="text", nullable=true)
     */
    protected $photoLicense;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary",
     *     inversedBy="medicalDictionaryImages"
     * )
     * @ORM\JoinColumn(
     *     name="medical_dictionary_id",
     *     referencedColumnName="id",
     *     nullable=true,
     *     onDelete="CASCADE"
     * )
     */
    protected $medicalDictionary;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos): void
    {
        $this->photos = $photos;
    }

    /**
     * @return mixed
     */
    public function getStringIdentifier()
    {
        return $this->stringIdentifier;
    }

    /**
     * @return mixed
     */
    public function getPhotoLicense()
    {
        return $this->photoLicense;
    }

    /**
     * @param mixed $photoLicense
     */
    public function setPhotoLicense($photoLicense): void
    {
        $this->photoLicense = $photoLicense;
    }

    /**
     * @param mixed $stringIdentifier
     */
    public function setStringIdentifier($stringIdentifier): void
    {
        $this->stringIdentifier = $stringIdentifier;
    }

    /**
     * @return mixed
     */
    public function getMedicalDictionary()
    {
        return $this->medicalDictionary;
    }

    /**
     * @param mixed $medicalDictionary
     */
    public function setMedicalDictionary($medicalDictionary): void
    {
        $this->medicalDictionary = $medicalDictionary;
    }
}
