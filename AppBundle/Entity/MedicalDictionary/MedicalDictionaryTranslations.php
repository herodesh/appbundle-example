<?php

namespace Flendoc\AppBundle\Entity\MedicalDictionary;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\HashTags\HashTags;

/**
 * Class MedicalDictionaryTranslations
 * @package Flendoc\AppBundle\Entity\MedicalDictionary
 *
 * @ORM\Table(name="medical_dictionary_translations")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalDictionary\MedicalDictionaryTranslationsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalDictionaryTranslations extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="definition", type="text", nullable=true)
     */
    protected $definition;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="causes", type="text", nullable=true)
     */
    protected $causes;

    /**
     * @ORM\Column(name="symptoms", type="text", nullable=true)
     */
    protected $symptoms;

    /**
     * @ORM\Column(name="diagnosis", type="text", nullable=true)
     */
    protected $diagnosis;

    /**
     * @ORM\Column(name="treatment", type="text", nullable=true)
     */
    protected $treatment;

    /**
     * @ORM\Column(name="prognosis", type="text", nullable=true)
     */
    protected $prognosis;

    /**
     * @ORM\Column(name="prevention", type="text", nullable=true)
     */
    protected $prevention;

    /**
     * @ORM\Column(name="slug", type="text", nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="has_hash_tags", type="boolean")
     */
    protected $hasHashTags = false;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *      name="language_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $language;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary",
     *      inversedBy="medicalDictionaryTranslation"
     * )
     * @ORM\JoinColumn(
     *      name="medical_dictionary_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $medicalDictionary;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\HashTags\HashTags",
     *     mappedBy="medicalDictionaryTranslation"
     * )
     */
    protected $hashTag;

    /**
     * MedicalDictionaryTranslations constructor.
     */
    public function __construct()
    {
        $this->hashTag = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @param mixed $definition
     */
    public function setDefinition($definition): void
    {
        $this->definition = $definition;
    }

    /**
     * @return mixed
     */
    public function getCauses()
    {
        return $this->causes;
    }

    /**
     * @param mixed $causes
     */
    public function setCauses($causes): void
    {
        $this->causes = $causes;
    }

    /**
     * @return mixed
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }

    /**
     * @param mixed $symptoms
     */
    public function setSymptoms($symptoms): void
    {
        $this->symptoms = $symptoms;
    }

    /**
     * @return mixed
     */
    public function getDiagnosis()
    {
        return $this->diagnosis;
    }

    /**
     * @param mixed $diagnosis
     */
    public function setDiagnosis($diagnosis): void
    {
        $this->diagnosis = $diagnosis;
    }

    /**
     * @return mixed
     */
    public function getTreatment()
    {
        return $this->treatment;
    }

    /**
     * @param mixed $treatment
     */
    public function setTreatment($treatment): void
    {
        $this->treatment = $treatment;
    }

    /**
     * @return mixed
     */
    public function getPrognosis()
    {
        return $this->prognosis;
    }

    /**
     * @param mixed $prognosis
     */
    public function setPrognosis($prognosis): void
    {
        $this->prognosis = $prognosis;
    }

    /**
     * @return mixed
     */
    public function getPrevention()
    {
        return $this->prevention;
    }

    /**
     * @param mixed $prevention
     */
    public function setPrevention($prevention): void
    {
        $this->prevention = $prevention;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getMedicalDictionary()
    {
        return $this->medicalDictionary;
    }

    /**
     * @param mixed $medicalDictionary
     */
    public function setMedicalDictionary($medicalDictionary): void
    {
        $this->medicalDictionary = $medicalDictionary;
    }

    /**
     * @return mixed
     */
    public function getHashTag()
    {
        return $this->hashTag;
    }

    /**
     * @param mixed $hashTag
     */
    public function setHashTag($hashTag): void
    {
        $this->hashTag = $hashTag;
    }

    /**
     * @param HashTags $hashTags
     */
    public function addHashTag(HashTags $hashTags)
    {
        if (!$this->hashTag->contains($hashTags)) {
            $this->hashTag->add($hashTags);
        }
    }

    /**
     * @param HashTags $hashTags
     */
    public function removeHashTag(HashTags $hashTags)
    {
        $this->hashTag->removeElement($hashTags);
    }

    /**
     * @return mixed
     */
    public function getHasHashTags()
    {
        return $this->hasHashTags;
    }

    /**
     * @param mixed $hasHashTags
     */
    public function setHasHashTags($hasHashTags): void
    {
        $this->hasHashTags = $hasHashTags;
    }
}
