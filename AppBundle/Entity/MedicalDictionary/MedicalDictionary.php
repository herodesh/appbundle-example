<?php

namespace Flendoc\AppBundle\Entity\MedicalDictionary;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class MedicalDictionary
 * @package Flendoc\AppBundle\Entity\MedicalDictionary
 *
 * @ORM\Table(name="medical_dictionary")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalDictionary\MedicalDictionaryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalDictionary extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinTable(
     *     name="medical_dictionary_authors",
     *     joinColumns={@ORM\JoinColumn(name="medical_dictionary_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id")}
     * )
     */
    protected $authors;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = false;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryTranslations",
     *     mappedBy="medicalDictionary",
     *     cascade={"persist"}
     * )
     */
    protected $medicalDictionaryTranslation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryImages",
     *     mappedBy="medicalDictionary"
     * )
     */
    protected $medicalDictionaryImages;

    /**
     * @ORM\Column(name="sources", type="text", nullable=true)
     */
    protected $sources;

    /**
     * @ORM\Column(name="is_social_added", type="boolean")
     */
    protected $isSocialAdded = false;

    /**
     * MedicalDictionary constructor.
     */
    public function __construct()
    {
        $this->medicalDictionaryTranslation = new ArrayCollection();
        $this->medicalDictionaryImages      = new ArrayCollection();
        $this->authors                      = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param mixed $authors
     */
    public function setAuthors($authors): void
    {
        $this->authors = $authors;
    }

    /**
     * @param Doctors $oDoctor
     *
     * @return $this|void
     */
    public function addAuthors(Doctors $oDoctor)
    {
        if ($this->authors->contains($oDoctor)) {
            return;
        }

        $this->authors->add($oDoctor);

        return $this;
    }

    /**
     * @param Doctors $oDoctor
     *
     * @return bool|void
     */
    public function removeAuthors(Doctors $oDoctor)
    {
        if (!$this->authors->contains($oDoctor)) {
            return;
        }

        return $this->authors->removeElement($oDoctor);
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getMedicalDictionaryTranslation()
    {
        return $this->medicalDictionaryTranslation;
    }

    /**
     * @param mixed $medicalDictionaryTranslation
     */
    public function setMedicalDictionaryTranslation($medicalDictionaryTranslation): void
    {
        $this->medicalDictionaryTranslation = $medicalDictionaryTranslation;
    }

    /**
     * @param MedicalDictionaryTranslations $medicalDictionaryTranslations
     */
    public function addMedicalDictionaryTranslations(MedicalDictionaryTranslations $medicalDictionaryTranslations)
    {
        if (!$this->medicalDictionaryTranslation->contains($medicalDictionaryTranslations)) {
            $this->medicalDictionaryTranslation->add($medicalDictionaryTranslations);
        }
    }

    /**
     * @param MedicalDictionaryTranslations $medicalDictionaryTranslations
     */
    public function removeMedicalDictionaryTranslations(MedicalDictionaryTranslations $medicalDictionaryTranslations)
    {
        $this->medicalDictionaryTranslation->removeElement($medicalDictionaryTranslations);
    }

    /**
     * @return mixed
     */
    public function getMedicalDictionaryImages()
    {
        return $this->medicalDictionaryImages;
    }

    /**
     * @param mixed $medicalDictionaryImages
     */
    public function setMedicalDictionaryImages($medicalDictionaryImages): void
    {
        $this->medicalDictionaryImages = $medicalDictionaryImages;
    }

    /**
     * @param MedicalDictionaryImages $medicalDictionaryImages
     */
    public function addMedicalDictionaryImages(MedicalDictionaryImages $medicalDictionaryImages)
    {
        if (!$this->medicalDictionaryImages->contains($medicalDictionaryImages)) {
            $this->medicalDictionaryImages->add($medicalDictionaryImages);
        }
    }

    /**
     * @param MedicalDictionaryImages $medicalDictionaryImages
     */
    public function removeMedicalDictionaryImages(MedicalDictionaryImages $medicalDictionaryImages)
    {
        $this->medicalDictionaryImages->removeElement($medicalDictionaryImages);
    }

    /**
     * Get Photos from the first MedicalCaseImage
     *
     * @return string|null
     */
    public function getFirstMedicalDictionaryImagePhotos()
    {
        if ($this->medicalDictionaryImages->count()) {
            $oMedicalDictionaryImage = $this->medicalDictionaryImages->first();

            return $oMedicalDictionaryImage->getPhotos();
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param mixed $sources
     */
    public function setSources($sources): void
    {
        $this->sources = $sources;
    }

    /**
     * @return mixed
     */
    public function getIsSocialAdded()
    {
        return $this->isSocialAdded;
    }

    /**
     * @param mixed $isSocialAdded
     */
    public function setIsSocialAdded($isSocialAdded): void
    {
        $this->isSocialAdded = $isSocialAdded;
    }
}
