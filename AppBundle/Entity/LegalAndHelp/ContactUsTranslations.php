<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactUsTranslations
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="contact_us_translations")
 */
class ContactUsTranslations extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *      name="language_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $language;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\ContactUs",
     *      inversedBy="contactUsTranslations"
     * )
     * @ORM\JoinColumn(
     *      name="contact_us_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $contactUs;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getContactUs()
    {
        return $this->contactUs;
    }

    /**
     * @param mixed $contactUs
     */
    public function setContactUs($contactUs)
    {
        $this->contactUs = $contactUs;
    }
}
