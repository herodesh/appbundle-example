<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FaqGroupsTranslations
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="faq_groups_translations")
 */
class FaqGroupsTranslations extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="slug", type="text", nullable=true)
     */
    protected $slug;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *      name="language_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $language;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups",
     *      inversedBy="faqGroupTranslations"
     * )
     * @ORM\JoinColumn(
     *      name="faq_group_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $faqGroup;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getFaqGroup()
    {
        return $this->faqGroup;
    }

    /**
     * @param mixed $faqGroup
     */
    public function setFaqGroup($faqGroup): void
    {
        $this->faqGroup = $faqGroup;
    }

}
