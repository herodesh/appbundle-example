<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Legal
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\LegalAndHelp\LegalRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="legal")
 */
class Legal extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="legal_identifier", type="text")
     */
    protected $legalIdentifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\LegalTranslations",
     *     mappedBy="legal",
     *     cascade={"persist"}
     * )
     */
    protected $legalTranslations;

    /**
     * Legal constructor.
     */
    public function __construct()
    {
        $this->legalTranslations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLegalIdentifier()
    {
        return $this->legalIdentifier;
    }

    /**
     * @param mixed $legalIdentifier
     */
    public function setLegalIdentifier($legalIdentifier): void
    {
        $this->legalIdentifier = $legalIdentifier;
    }

    /**
     * @return mixed
     */
    public function getLegalTranslations()
    {
        return $this->legalTranslations;
    }

    /**
     * @param mixed $legalTranslations
     */
    public function setLegalTranslations($legalTranslations): void
    {
        $this->legalTranslations = $legalTranslations;
    }

    /**
     * @param LegalTranslations $legalTranslation
     */
    public function addLegalTranslations(LegalTranslations $legalTranslation)
    {
        if (!$this->legalTranslations->contains($legalTranslation)) {
            $this->legalTranslations->add($legalTranslation);
        }
    }

    /**
     * @param LegalTranslations $legalTranslation
     */
    public function removeLegalTranslations(LegalTranslations $legalTranslation)
    {
        $this->legalTranslations->removeElement($legalTranslation);
    }
}
