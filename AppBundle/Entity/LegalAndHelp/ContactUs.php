<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ContactUs
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\LegalAndHelp\ContactUsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="contact_us")
 */
class ContactUs extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="contact_us_identifier", type="text")
     */
    protected $contactUsIdentifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\ContactUsTranslations",
     *     mappedBy="contactUs",
     *     cascade={"persist"}
     * )
     */
    protected $contactUsTranslations;

    /**
     * Faq constructor.
     */
    public function __construct()
    {
        $this->contactUsTranslations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContactUsIdentifier()
    {
        return $this->contactUsIdentifier;
    }

    /**
     * @param mixed $contactUsIdentifier
     */
    public function setContactUsIdentifier($contactUsIdentifier): void
    {
        $this->contactUsIdentifier = $contactUsIdentifier;
    }

    /**
     * @return mixed
     */
    public function getContactUsTranslations()
    {
        return $this->contactUsTranslations;
    }

    /**
     * @param mixed $contactUsTranslations
     */
    public function setContactUsTranslations($contactUsTranslations): void
    {
        $this->contactUsTranslations = $contactUsTranslations;
    }

    /**
     * @param ContactUsTranslations $contactUsTranslations
     */
    public function addContactUsTranslations(ContactUsTranslations $contactUsTranslations)
    {
        if (!$this->contactUsTranslations->contains($contactUsTranslations)) {
            $this->contactUsTranslations->add($contactUsTranslations);
        }
    }

    /**
     * @param ContactUsTranslations $contactUsTranslations
     */
    public function removeContactUsTranslations(ContactUsTranslations $contactUsTranslations)
    {
        $this->contactUsTranslations->removeElement($contactUsTranslations);
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayTitle(Languages $oLanguages)
    {
        foreach ($this->getContactUsTranslations() as $oContactUsTranslations) {
            if ($oContactUsTranslations->getLanguage()->getId() == $oLanguages->getId()) {
                return $oContactUsTranslations->getTitle();
            }
        }

        //get primary language
        foreach ($this->getContactUsTranslations() as $oContactUsTranslations) {
            if ($oContactUsTranslations->getLanguage()->getIsPrimary()) {
                return $oContactUsTranslations->getTitle();
            }
        }
    }
}
