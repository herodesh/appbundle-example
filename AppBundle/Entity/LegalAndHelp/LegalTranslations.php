<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LegalTranslations
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="legal_translations")
 */
class LegalTranslations extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *      name="language_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $language;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\Legal",
     *      inversedBy="legalTranslations"
     * )
     * @ORM\JoinColumn(
     *      name="legal_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $legal;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getLegal()
    {
        return $this->legal;
    }

    /**
     * @param mixed $legal
     */
    public function setLegal($legal): void
    {
        $this->legal = $legal;
    }
}
