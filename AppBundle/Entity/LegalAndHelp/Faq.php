<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Faq
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\LegalAndHelp\FaqRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="faq")
 */
class Faq extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups",
     *     inversedBy="faq"
     * )
     * @ORM\JoinColumn(
     *     name="faq_group_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $faqGroups;

    /**
     * @ORM\Column(name="faq_identifier", type="text")
     * @Assert\NotBlank(message="validator.admin.faq.identifier.not.blank")
     */
    protected $faqIdentifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations",
     *     mappedBy="faq",
     *     cascade={"persist"}
     * )
     */
    protected $faqTranslations;

    /**
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = false;

    /**
     * Faq constructor.
     */
    public function __construct()
    {
        $this->faqTranslations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFaqIdentifier()
    {
        return $this->faqIdentifier;
    }

    /**
     * @param mixed $faqIdentifier
     */
    public function setFaqIdentifier($faqIdentifier): void
    {
        $this->faqIdentifier = $faqIdentifier;
    }

    /**
     * @return mixed
     */
    public function getFaqTranslations()
    {
        return $this->faqTranslations;
    }

    /**
     * @param mixed $faqTranslations
     */
    public function setFaqTranslations($faqTranslations): void
    {
        $this->faqTranslations = $faqTranslations;
    }

    /**
     * @param FaqTranslations $faqTranslation
     */
    public function addFaqTranslations(FaqTranslations $faqTranslation)
    {
        if (!$this->faqTranslations->contains($faqTranslation)) {
            $this->faqTranslations->add($faqTranslation);
        }
    }

    /**
     * @param FaqTranslations $faqTranslation
     */
    public function removeFaqTranslations(FaqTranslations $faqTranslation)
    {
        $this->faqTranslations->removeElement($faqTranslation);
    }

    /**
     * @return mixed
     */
    public function getFaqGroups()
    {
        return $this->faqGroups;
    }

    /**
     * @param mixed $faqGroups
     */
    public function setFaqGroups($faqGroups): void
    {
        $this->faqGroups = $faqGroups;
    }
    
    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }
}
