<?php

namespace Flendoc\AppBundle\Entity\LegalAndHelp;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FaqGroups
 * @package Flendoc\AppBundle\Entity\LegalAndHelp
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\LegalAndHelp\FaqGroupsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="faq_groups")
 */
class FaqGroups extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\Faq",
     *     mappedBy="faqGroups",
     *     cascade={"persist"}
     * )
     */
    protected $faq;

    /**
     * @ORM\Column(name="faq_group_identifier", type="text")
     * @Assert\NotBlank(message="validator.admin.faq.identifier.not.blank")
     */
    protected $faqGroupIdentifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroupsTranslations",
     *     mappedBy="faqGroup",
     *     cascade={"persist"}
     * )
     */
    protected $faqGroupTranslations;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = false;

    /**
     * Faq constructor.
     */
    public function __construct()
    {
        $this->faqGroupTranslations = new ArrayCollection();
        $this->faq                  = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFaqGroupIdentifier()
    {
        return $this->faqGroupIdentifier;
    }

    /**
     * @param mixed $faqGroupIdentifier
     */
    public function setFaqGroupIdentifier($faqGroupIdentifier): void
    {
        $this->faqGroupIdentifier = $faqGroupIdentifier;
    }

    /**
     * @return mixed
     */
    public function getFaqGroupTranslations()
    {
        return $this->faqGroupTranslations;
    }

    /**
     * @param mixed $faqGroupTranslations
     */
    public function setFaqGroupTranslations($faqGroupTranslations): void
    {
        $this->faqGroupTranslations = $faqGroupTranslations;
    }

    /**
     * @param FaqGroupsTranslations $faqGroupTranslation
     */
    public function addFaqGroupTranslations(FaqGroupsTranslations $faqGroupTranslation)
    {
        if (!$this->faqGroupTranslations->contains($faqGroupTranslation)) {
            $this->faqGroupTranslations->add($faqGroupTranslation);
        }
    }

    /**
     * @param FaqGroupsTranslations $faqGroupTranslation
     */
    public function removeFaqGroupsTranslations(FaqGroupsTranslations $faqGroupTranslation)
    {
        $this->faqGroupTranslations->removeElement($faqGroupTranslation);
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getFaq()
    {
        return $this->faq;
    }

    /**
     * @param mixed $faq
     */
    public function setFaq($faq): void
    {
        $this->faq = $faq;
    }

    /**
     * @param Faq $faq
     */
    public function addFaq(Faq $faq)
    {
        if (!$this->faq->contains($faq)) {
            $this->faq->add($faq);
        }
    }

    /**
     * @param Faq $faq
     */
    public function removeFaq(Faq $faq)
    {
        $this->faq->removeElement($faq);
    }

    /**
     * @param Languages $oLanguage
     *
     * @return mixed
     */
    public function getDisplayTitle(Languages $oLanguage)
    {
        foreach ($this->getFaqGroupTranslations() as $oFaqGroupTranslation) {
            if ($oFaqGroupTranslation->getLanguage()->getId() == $oLanguage->getId()) {
                return $oFaqGroupTranslation->getTitle();
            }
        }

        //get primary language
        foreach ($this->getFaqGroupTranslations() as $oFaqGroupTranslation) {
            if ($oFaqGroupTranslation->getLanguage()->getIsPrimary()) {
                return $oFaqGroupTranslation->getTitle();
            }
        }
    }

}
