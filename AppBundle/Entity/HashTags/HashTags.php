<?php

namespace Flendoc\AppBundle\Entity\HashTags;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HashTags
 * @package Flendoc\AppBundle\Entity\HashTags
 *
 * @ORM\Table(name="hash_tags")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\HashTags\HashTagsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HashTags extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="tag", type="string")
     */
    protected $tag;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryTranslations",
     *      inversedBy="hashTag"
     * )
     * @ORM\JoinColumn(
     *      name="medical_dictionary_translation_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $medicalDictionaryTranslation;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *      inversedBy="hashTag"
     * )
     * @ORM\JoinColumn(
     *      name="medical_case_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $medicalCase;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getMedicalDictionaryTranslation()
    {
        return $this->medicalDictionaryTranslation;
    }

    /**
     * @param mixed $medicalDictionaryTranslation
     */
    public function setMedicalDictionaryTranslation($medicalDictionaryTranslation): void
    {
        $this->medicalDictionaryTranslation = $medicalDictionaryTranslation;
    }

    /**
     * @return mixed
     */
    public function getMedicalCase()
    {
        return $this->medicalCase;
    }

    /**
     * @param mixed $medicalCase
     */
    public function setMedicalCase($medicalCase): void
    {
        $this->medicalCase = $medicalCase;
    }
}
