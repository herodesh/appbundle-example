<?php

namespace Flendoc\AppBundle\Entity\Languages;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Flendoc\AppBundle\Validator\Constraints as SiteAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Languages\LanguagesRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="languages")
 *
 * @UniqueEntity(fields="iso")
 *
 * @SiteAssert\IsSinglePrimaryLanguageEntry(message="validator.single.entry")
 */
class Languages extends AbstractEntity
{

    /** @var array An array with all languages Iso keys and their names */
    const ALL_LANGUAGES_ISO = [
        'English'   => 'en',
        'German'    => 'de',
        'French'    => 'fr',
        'Spanish'   => 'es',
        'Polish'    => 'pl',
        'Swedish'   => 'sv',
        'Finnish'   => 'fi',
        'Italian'   => 'it',
        'Dutch'     => 'nl',
        'Portugese' => 'pt',
        'Romanian'  => 'ro',
        'Czech'     => 'cs',
        'Greek'     => 'el',
        'Serbian'   => 'sr',
        'Danish'    => 'da',
        'Hungarian' => 'hu',
        'Slovenian' => 'sl',
        'Croatian'  => 'hr',
        'Bulgarian' => 'bg',
        'Norwegian' => 'nb',
        'Slovak'    => 'sk',
        'Turkish'   => 'tr',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\TranslatedLanguages",
     *     mappedBy="language",
     *     cascade={"persist"}
     * )
     */
    protected $translatedLanguages;

    /**
     * @ORM\Column(name="iso", type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     minMessage="validator.minimum.2.characters"
     * )
     */
    protected $iso;

    /**
     * This is going to be used for slugify library in order to get the rule name which is the language name in english
     *
     * @ORM\Column(name="name_identifier", type="string", nullable=false)
     */
    protected $nameIdentifier;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default": 1})
     */
    protected $active;

    /**
     * @ORM\Column(name="is_primary", type="boolean", nullable=true)
     */
    protected $isPrimary;

    /**
     * Languages constructor.
     */
    public function __construct()
    {
        $this->translatedLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTranslatedLanguages()
    {
        return $this->translatedLanguages;
    }

    /**
     * @param mixed $translatedLanguages
     */
    public function setTranslatedLanguages($translatedLanguages)
    {
        $this->translatedLanguages = $translatedLanguages;
    }

    /**
     * @param TranslatedLanguages $oTranslatedLanguage
     */
    public function addTranslatedLanguage(TranslatedLanguages $oTranslatedLanguage)
    {
        if (!$this->translatedLanguages->contains($oTranslatedLanguage)) {
            $this->translatedLanguages->add($oTranslatedLanguage);
        }
    }

    /**
     * @return mixed
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * @param mixed $iso
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
    }

    /**
     * @return mixed
     */
    public function getNameIdentifier()
    {
        return $this->nameIdentifier;
    }

    /**
     * @param mixed $nameIdentifier
     */
    public function setNameIdentifier($nameIdentifier): void
    {
        $this->nameIdentifier = $nameIdentifier;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param mixed $isPrimary
     */
    public function setIsPrimary($isPrimary): void
    {
        $this->isPrimary = $isPrimary;
    }

    /**
     * Accepts string or language Object itself
     *
     * @param $languages
     *
     * @return mixed
     */
    public function getNameByIso($languages = null)
    {
        $languagesValue = ($languages instanceof Languages) ? $languages->getIso() : $languages;

        foreach ($this->getTranslatedLanguages() as $oLanguageTranslatedLanguage) {
            if ($oLanguageTranslatedLanguage->getIso() === $languagesValue) {
                return $oLanguageTranslatedLanguage->getName();
            }
        }

        //get primary language
        foreach ($this->getTranslatedLanguages() as $oLanguageTranslatedLanguage) {
            if ($oLanguageTranslatedLanguage->getLanguage()->getIsPrimary()) {
                return $oLanguageTranslatedLanguage->getName();
            }
        }
    }

}
