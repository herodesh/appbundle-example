<?php

namespace Flendoc\AppBundle\Entity\Languages;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TranslatedLanguages
 * @package Flendoc\AppBundle\Entity\Languages
 *
 * @ORM\Entity()
 * @ORM\Table(name="languages_translated_languages")
 */
class TranslatedLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(name="iso", type="string")
     */
    protected $iso;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages",
     *     inversedBy="translatedLanguages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $language;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="translate_language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $translatedLanguages;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * @param mixed $iso
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getTranslatedLanguages()
    {
        return $this->translatedLanguages;
    }

    /**
     * @param mixed $translatedLanguages
     */
    public function setTranslatedLanguages($translatedLanguages)
    {
        $this->translatedLanguages = $translatedLanguages;
    }
}
