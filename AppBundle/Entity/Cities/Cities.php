<?php

namespace Flendoc\AppBundle\Entity\Cities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Resumes\Resumes;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Cities\CitiesRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="cities")
 */
class Cities extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="state", type="string")
     */
    protected $state = '';

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *      name="country_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $country;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\CityLanguages",
     *     mappedBy="cities",
     *     cascade={"persist"}
     * )
     */
    protected $cityLanguages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes",
     *     mappedBy="wantedCities",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $resumes;

    /**
     * Cities constructor.
     */
    public function __construct()
    {
        $this->cityLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getCityLanguages()
    {
        return $this->cityLanguages;
    }

    /**
     * @param mixed $cityLanguages
     */
    public function setCityLanguages($cityLanguages)
    {
        $this->cityLanguages = $cityLanguages;
    }

    /**
     * @param CityLanguages $cityLanguages
     */
    public function addCityLanguages(CityLanguages $cityLanguages)
    {
        if (!$this->cityLanguages->contains($cityLanguages)) {
            $this->cityLanguages->add($cityLanguages);
        }
    }

    /**
     * @param CityLanguages $cityLanguages
     */
    public function removeCityLanguages(CityLanguages $cityLanguages)
    {
        $this->cityLanguages->removeElement($cityLanguages);
    }

    /**
     * @return mixed
     */
    public function getResumes()
    {
        return $this->resumes;
    }

    /**
     * @param mixed $resumes
     */
    public function setResumes($resumes)
    {
        $this->resumes = $resumes;
    }

    /**
     * @param Resumes $oResume
     */
    public function addResumes(Resumes $oResume)
    {
        if ($this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->add($oResume);
        $oResume->addWantedCity($this);
    }

    /**
     * @param Resumes $oResume
     */
    public function removeResumes(Resumes $oResume)
    {
        if (!$this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->removeElement($oResume);
        $oResume->removeWantedCity($this);
    }

    /**
     * @param $languages
     *
     * @return string
     */
    public function getDisplayCityName($languages)
    {
        $languagesValue = ($languages instanceof Languages) ? $languages->getIso() : $languages;

        foreach ($this->getCityLanguages() as $oCityLanguage) {
            foreach ($oCityLanguage->getLanguages()->getTranslatedLanguages() as $oTranslatedLanguage) {
                if ($oTranslatedLanguage->getLanguage()->getIso() === $languagesValue &&
                    $oCityLanguage->getLanguages()->getIso() === $languagesValue
                ) {
                    return $oCityLanguage->getName();
                }
            }
        }

        //get primary language
        foreach ($this->getCityLanguages() as $oCityLanguage) {
            if ($oCityLanguage->getLanguages()->getIsPrimary()) {
                return $oCityLanguage->getName();
            }
        }

        return 'No name exists for this city in your language';
    }

}
