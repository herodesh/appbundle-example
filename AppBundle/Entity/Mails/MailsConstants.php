<?php

namespace Flendoc\AppBundle\Entity\Mails;

/**
 * Class MailsConstants
 * @package Flendoc\AppBundle\Entity\Mails
 */
final class MailsConstants
{
    const ADMIN_NEW_ACCOUNT = 'adminNewAccount';

    const VALIDATION_DOCUMENTS_ACCEPTED = 'validationDocumentsAccepted';

    const VALIDATION_DOCUMENTS_INCOMPLETE = 'validationDocumentsIncomplete';

    const VALIDATION_DOCUMENTS_NOT_SENT = 'validationDocumentsNotSent';

    const ACCOUNT_ACTIVATED = 'accountActivated';

    const REGISTRATION_CONFIRMATION = 'registrationConfirmation';

    const DOCTORS_FEEDBACK = 'doctorsFeedback';

    const DOCTORS_CONTACT_US = 'doctorsContactUs';

    const RESET_PASSWORD = 'resetPassword';

    const APPOINTMENT_NOTIFICATION = 'appointmentNotification';

    const CONTACT_REQUEST_NOTIFICATION = 'contactRequestNotification';

    const CONTACT_REQUEST_ACCEPTED = 'contactRequestAccepted';

    const NEW_MESSAGE_NOTIFICATION = 'newMessageNotification';

    const CONTACT_INVITATION = 'contactInvitation';

    const ACCOUNT_LOCKED = 'accountLocked';

    const ACCOUNT_UNLOCKED = 'accountUnlocked';

    const NEW_MEDICAL_CASE_ADDED = 'newMedicalCaseAdded';

    const MEDICAL_CASE_USER_ACTION_REQUIRED = 'medicalCaseUserActionRequired';

    const MEDICAL_CASE_APPROVED = 'medicalCaseApproved';

    const MEDICAL_CASE_DELETED = 'medicalCaseDeleted';

    const MEDICAL_CASE_REENABLED = 'medicalCaseReenabled';

    const MEDICAL_CASES_AWAITS_ADMIN_APPROVAL = 'medicalCasesAwaitsAdminApproval';

    const MEDICAL_CASE_UNPUBLISHED_AUTOMATIC_DELETION = 'medicalCaseUnpublishedAutomaticDeletion';

    const NOTIFY_ADMIN_OF_NEW_USER_REGISTRATION = 'notifyAdminOfNewUserRegistration';

    const NOTIFY_ADMIN_OF_NEWLY_SUBMITTED_VERIFICATION_DOCUMENTS = 'notifyAdminOfNewlySubmittedVerificationDocuments';
}
