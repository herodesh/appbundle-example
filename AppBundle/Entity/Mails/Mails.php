<?php

namespace Flendoc\AppBundle\Entity\Mails;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Mails
 * @package Flendoc\AppBundle\Entity\Mails
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Mails\MailsRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="mails")
 * @UniqueEntity("identifier")
 */
class Mails extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="email_identifier", type="string")
     */
    protected $emailIdentifier;

    /**
     * @ORM\Column(name="variables", type="string", nullable=false)
     * @Assert\NotBlank()
     */
    protected $variables;

    /**
     * @ORM\Column(name="subject_variables", type="string", nullable=true)
     */
    protected $subjectVariables;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\Column(name="is_administrative", type="boolean", options={"comment": "refers to administrators emails"})
     */
    protected $isAdministrative = false;

    /**
     * @ORM\Column(name="is_account", type="boolean", options={"comment": "refers to user account emails"})
     */
    protected $isAccount = false;

    /**
     * @ORM\Column(name="is_patient", type="boolean", options={"comment": "refers to patients emails"})
     */
    protected $isPatient = false;

    /**
     * @ORM\Column(name="is_external", type="boolean", options={"comment": "email is not part of the app"})
     */
    protected $isExternal = false;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Mails\MailLanguages",
     *     mappedBy="mail",
     *     cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    protected $mailLanguages;

    /**
     * Mails constructor.
     */
    public function __construct()
    {
        $this->mailLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmailIdentifier()
    {
        return $this->emailIdentifier;
    }

    /**
     * @param mixed $emailIdentifier
     */
    public function setEmailIdentifier($emailIdentifier)
    {
        $this->emailIdentifier = $emailIdentifier;
    }

    /**
     * @return mixed
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param mixed $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }

    /**
     * @return mixed
     */
    public function getSubjectVariables()
    {
        return $this->subjectVariables;
    }

    /**
     * @param mixed $subjectVariables
     */
    public function setSubjectVariables($subjectVariables): void
    {
        $this->subjectVariables = $subjectVariables;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getIsAdministrative()
    {
        return $this->isAdministrative;
    }

    /**
     * @param mixed $isAdministrative
     */
    public function setIsAdministrative($isAdministrative)
    {
        $this->isAdministrative = $isAdministrative;
    }

    /**
     * @return mixed
     */
    public function getIsAccount()
    {
        return $this->isAccount;
    }

    /**
     * @param mixed $isAccount
     */
    public function setIsAccount($isAccount)
    {
        $this->isAccount = $isAccount;
    }

    /**
     * @return mixed
     */
    public function getIsPatient()
    {
        return $this->isPatient;
    }

    /**
     * @param mixed $isPatient
     */
    public function setIsPatient($isPatient)
    {
        $this->isPatient = $isPatient;
    }

    /**
     * @return mixed
     */
    public function getIsExternal()
    {
        return $this->isExternal;
    }

    /**
     * @param mixed $isExternal
     */
    public function setIsExternal($isExternal)
    {
        $this->isExternal = $isExternal;
    }

    /**
     * @return mixed
     */
    public function getMailLanguages()
    {
        return $this->mailLanguages;
    }

    /**
     * @param mixed $mailLanguages
     */
    public function setMailLanguages($mailLanguages)
    {
        $this->mailLanguages = $mailLanguages;
    }

    /**
     * @param MailLanguages $mailLanguages
     */
    public function addMailLanguage(MailLanguages $mailLanguages)
    {
        if (!$this->mailLanguages->contains($mailLanguages)) {
            $this->mailLanguages->add($mailLanguages);
        }
    }

    /**
     * @param MailLanguages $mailLanguages
     */
    public function removeMailLanguage(MailLanguages $mailLanguages)
    {
        if ($this->mailLanguages->contains($mailLanguages)) {
            $this->mailLanguages->removeElement($mailLanguages);
        }
    }

}
