<?php
namespace Flendoc\AppBundle\Entity\Institutions;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="institution_images")
 */
class InstitutionImages extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\Institutions",
     *     inversedBy="institutionImages"
     * )
     * @ORM\JoinColumn(
     *     name="institution_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $institution;

    /**
     * @ORM\Column(name="image", type="text", nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(name="thumbnail", type="text", nullable=true)
     */
    protected $thumbnail;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param mixed $institution
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
}
