<?php

namespace Flendoc\AppBundle\Entity\Institutions;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Institutions\InstitutionsRepository")
 * @ORM\Table(name="institutions")
 * @ORM\HasLifecycleCallbacks()
 */
class Institutions extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="institution_logo", type="string", nullable=true)
     */
    protected $institutionLogo;

    /**
     * @ORM\Column(name="name", type="text")
     * @Assert\NotNull()
     */
    protected $name;

    /**
     * @ORM\Column(name="address", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    protected $address;

    /**
     * @ORM\Column(name="postal_code", type="text", nullable=true)
     */
    protected $postal_code;

    /**
     * @ORM\Column(name="phone", type="text", nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(name="mobile", type="text", nullable=true)
     */
    protected $mobile;

    /**
     * @ORM\Column(name="fax", type="text", nullable=true)
     */
    protected $fax;

    /**
     * @ORM\Column(name="email", type="text", nullable=true)
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(name="website", type="text", nullable=true)
     */
    protected $website;

    /**
     * @ORM\Column(name="is_public", type="boolean")
     */
    protected $is_public = true;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\InstitutionImages",
     *     mappedBy="institution"
     * )
     */
    protected $institutionImages;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\InstitutionLanguages",
     *     mappedBy="institutions",
     *     cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    protected $institutionLanguages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills"
     * )
     * @ORM\JoinTable(name="institution_skills",
     *     joinColumns={@ORM\JoinColumn(name="institution_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")}
     * )
     * @Assert\NotBlank()
     */
    protected $institutionSkills;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id"
     * )
     */
    protected $institutionCountry;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities",
     * )
     * @ORM\JoinColumn(
     *     name="city_id",
     *     referencedColumnName="id"
     * )
     * @Assert\NotBlank()
     */
    protected $institutionCity;

    /**
     * Institutions constructor.
     */
    public function __construct()
    {
        $this->institutionLanguages = new ArrayCollection();
        $this->institutionSkills    = new ArrayCollection();
        $this->institutionImages    = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getInstitutionLogo()
    {
        return $this->institutionLogo;
    }

    /**
     * @param mixed $institutionLogo
     */
    public function setInstitutionLogo($institutionLogo)
    {
        $this->institutionLogo = $institutionLogo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * @param mixed $is_public
     */
    public function setIsPublic($is_public)
    {
        $this->is_public = $is_public;
    }

    /**
     * @return mixed
     */
    public function getInstitutionImages()
    {
        return $this->institutionImages;
    }

    /**
     * @param InstitutionImages $institutionImages
     */
    public function addInstitutionImages(InstitutionImages $institutionImages)
    {
        if (!$this->institutionImages->contains($institutionImages)) {
            $this->institutionImages->add($institutionImages);
        }
    }

    /**
     * @param InstitutionImages $institutionImages
     */
    public function removeInstitutionImages(InstitutionImages $institutionImages)
    {
        $this->institutionImages->remove($institutionImages);
    }

    /**
     * @param mixed $institutionImages
     */
    public function setInstitutionImages($institutionImages)
    {
        $this->institutionImages = $institutionImages;
    }

    /**
     * @return mixed
     */
    public function getInstitutionLanguages()
    {
        return $this->institutionLanguages;
    }

    /**
     * @param InstitutionLanguages $institutionLanguages
     */
    public function addInstitutionLanguage(InstitutionLanguages $institutionLanguages)
    {
        if (!$this->institutionLanguages->contains($institutionLanguages)) {
            $this->institutionLanguages->add($institutionLanguages);
        }
    }

    /**
     * @param InstitutionLanguages $institutionLanguages
     */
    public function removeInstitutionLanguage(InstitutionLanguages $institutionLanguages)
    {
        $this->institutionLanguages->remove($institutionLanguages);
    }

    /**
     * @param mixed $institutionLanguages
     */
    public function setInstitutionLanguages($institutionLanguages)
    {
        $this->institutionLanguages = $institutionLanguages;
    }

    /**
     * @return mixed
     */
    public function getInstitutionCountry()
    {
        return $this->institutionCountry;
    }

    /**
     * @param mixed $institutionCountry
     */
    public function setInstitutionCountry($institutionCountry)
    {
        $this->institutionCountry = $institutionCountry;
    }

    /**
     * @return mixed
     */
    public function getInstitutionCity()
    {
        return $this->institutionCity;
    }

    /**
     * @param mixed $institutionCity
     */
    public function setInstitutionCity($institutionCity)
    {
        $this->institutionCity = $institutionCity;
    }

    /**
     * @return mixed
     */
    public function getInstitutionSkills()
    {
        return $this->institutionSkills;
    }

    /**
     * @param mixed $institutionSkills
     */
    public function setInstitutionSkills($institutionSkills)
    {
        $this->institutionSkills = $institutionSkills;
    }

    /**
     * @param Skills $institutionSkills
     */
    public function addInstitutionSkill(Skills $institutionSkills)
    {
        if (!$this->institutionSkills->contains($institutionSkills)) {
            $this->institutionSkills->add($institutionSkills);
        }
    }

    /**
     * @param Skills $institutionSkills
     */
    public function removeInstitutionSkills(Skills $institutionSkills)
    {
        $this->institutionSkills->remove($institutionSkills);
    }

    /**
     * @param null $sLanguage
     *
     * @return mixed
     */
    public function getInstitutionName($sLanguage = null)
    {
        foreach ($this->getInstitutionLanguages() as $institutionLanguage) {

            if (null != $sLanguage && $institutionLanguage->getLanguages()->getId() == $sLanguage) {
                return $institutionLanguage->getName();
            }

            foreach ($this->getInstitutionCountry()->getCountrySpokenLanguages() as $countrySpokenLanguage) {
                if ($countrySpokenLanguage->getIsPrimary() && ($institutionLanguage->getLanguages()
                                                                                   ->getId() == $countrySpokenLanguage->getLanguage()
                                                                                                                      ->getId())
                ) {
                    return $institutionLanguage->getName();
                }
            }
        }

        return $this->getInstitutionLanguages()->first()->getName();
    }
}
