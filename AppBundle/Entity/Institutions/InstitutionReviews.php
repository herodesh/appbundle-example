<?php
namespace Flendoc\AppBundle\Entity\Institutions;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="institution_reviews")
 */
class InstitutionReviews extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\Institutions",
     *     inversedBy="institutionReviews"
     * )
     * @ORM\JoinColumn(
     *     name="institution_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $institution;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Reviews\Reviews",
     *     inversedBy="institutionReviews"
     * )
     * @ORM\JoinColumn(
     *     name="review_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $review;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param mixed $institution
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }
}
