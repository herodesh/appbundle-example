<?php

namespace Flendoc\AppBundle\Entity\Institutions;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Validator\Constraints\IsInstitutionPrimaryLanguage;
use Flendoc\AppBundle\Validator\Constraints\IsPhone;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="institution_languages", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_LANGUAGE_CONSTRAINT", columns={"language_id", "institution_id"})})
 *
 * @UniqueEntity(fields={"languages", "institutions"})
 */
class InstitutionLanguages extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *      name="language_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\Institutions",
     *      inversedBy="institutionLanguages"
     * )
     * @ORM\JoinColumn(
     *      name="institution_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $institutions;

    /**
     * @ORM\Column(name="area_of_expertise", type="text", nullable=true)
     */
    protected $areaOfExpertise;

    /**
     * @ORM\Column(name="range_of_services", type="text", nullable=true)
     */
    protected $rangeOfServices;

    /**
     * @ORM\Column(name="extra_details", type="text", nullable=true)
     */
    protected $extraDetails;

    /**
     * @ORM\Column(name="is_primary_description", type="boolean")
     */
    protected $isPrimaryDescription = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getInstitutions()
    {
        return $this->institutions;
    }

    /**
     * @param mixed $institutions
     */
    public function setInstitutions($institutions)
    {
        $this->institutions = $institutions;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAreaOfExpertise()
    {
        return $this->areaOfExpertise;
    }

    /**
     * @param mixed $areaOfExpertise
     */
    public function setAreaOfExpertise($areaOfExpertise)
    {
        $this->areaOfExpertise = $areaOfExpertise;
    }

    /**
     * @return mixed
     */
    public function getRangeOfServices()
    {
        return $this->rangeOfServices;
    }

    /**
     * @param mixed $rangeOfServices
     */
    public function setRangeOfServices($rangeOfServices)
    {
        $this->rangeOfServices = $rangeOfServices;
    }

    /**
     * @return mixed
     */
    public function getExtraDetails()
    {
        return $this->extraDetails;
    }

    /**
     * @param mixed $extraDetails
     */
    public function setExtraDetails($extraDetails)
    {
        $this->extraDetails = $extraDetails;
    }

    /**
     * @return mixed
     */
    public function getIsPrimaryDescription()
    {
        return $this->isPrimaryDescription;
    }

    /**
     * @param mixed $isPrimaryDescription
     */
    public function setIsPrimaryDescription($isPrimaryDescription)
    {
        $this->isPrimaryDescription = $isPrimaryDescription;
    }

}
