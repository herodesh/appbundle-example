<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class RegistrationDocumentTypeLanguages
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Entity()
 * @ORM\Table(name="doctor_registration_document_type_languages")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationDocumentTypeLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes",
     *     inversedBy="registrationDocumentTypeLanguages",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="registration_document_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $registrationDocumentTypes;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $languages;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDocumentTypes()
    {
        return $this->registrationDocumentTypes;
    }

    /**
     * @param mixed $registrationDocumentTypes
     */
    public function setRegistrationDocumentTypes($registrationDocumentTypes)
    {
        $this->registrationDocumentTypes = $registrationDocumentTypes;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }
}
