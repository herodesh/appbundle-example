<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class RegistrationDocumentsRejectionLanguages
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Entity()
 * @ORM\Table(name="doctor_registration_documents_rejection_languages")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationDocumentsRejectionLanguages extends AbstractEntity
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="user_notification_message", type="text", nullable=true)
     */
    protected $userNotificationMessage;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection",
     *     inversedBy="registrationDocumentsRejectionLanguages",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="registration_document_rejection_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $registrationDocumentsRejection;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $languages;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUserNotificationMessage()
    {
        return $this->userNotificationMessage;
    }

    /**
     * @param mixed $userNotificationMessage
     */
    public function setUserNotificationMessage($userNotificationMessage)
    {
        $this->userNotificationMessage = $userNotificationMessage;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDocumentsRejection()
    {
        return $this->registrationDocumentsRejection;
    }

    /**
     * @param mixed $registrationDocumentsRejection
     */
    public function setRegistrationDocumentsRejection($registrationDocumentsRejection)
    {
        $this->registrationDocumentsRejection = $registrationDocumentsRejection;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }
}
