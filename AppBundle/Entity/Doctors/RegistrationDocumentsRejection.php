<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class RegistrationDocumentsRejection
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Users\RegistrationDocumentRejectionRepository")
 * @ORM\Table(name="doctor_registration_documents_rejection")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationDocumentsRejection extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejectionLanguages",
     *     mappedBy="registrationDocumentsRejection",
     *     cascade={"persist"}
     * )
     */
    protected $registrationDocumentsRejectionLanguages;

    /**
     * RegistrationDocumentsRejection constructor.
     */
    public function __construct()
    {
        $this->registrationDocumentsRejectionLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDocumentsRejectionLanguages()
    {
        return $this->registrationDocumentsRejectionLanguages;
    }

    /**
     * @param mixed $registrationDocumentsRejectionLanguages
     */
    public function setRegistrationDocumentsRejectionLanguages($registrationDocumentsRejectionLanguages)
    {
        $this->registrationDocumentsRejectionLanguages = $registrationDocumentsRejectionLanguages;
    }

    /**
     * @param RegistrationDocumentsRejectionLanguages $registrationDocumentsRejectionLanguages
     */
    public function addRegistrationDocumentsRejectionLanguage(
        RegistrationDocumentsRejectionLanguages $registrationDocumentsRejectionLanguages
    ) {
        if (!$this->registrationDocumentsRejectionLanguages->contains($registrationDocumentsRejectionLanguages)) {
            $this->registrationDocumentsRejectionLanguages->add($registrationDocumentsRejectionLanguages);
        }
    }

    /**
     * @param RegistrationDocumentsRejectionLanguages $registrationDocumentsRejectionLanguages
     */
    public function removeRegistrationDocumentsRejectionLanguage(
        RegistrationDocumentsRejectionLanguages $registrationDocumentsRejectionLanguages
    ) {
        if ($this->registrationDocumentsRejectionLanguages->contains($registrationDocumentsRejectionLanguages)) {
            $this->registrationDocumentsRejectionLanguages->removeElement($registrationDocumentsRejectionLanguages);
        }
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getRegistrationDocumentsRejectionLanguages() as $oRegistrationDocumentsRejectionLanguage) {
            if ($oRegistrationDocumentsRejectionLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oRegistrationDocumentsRejectionLanguage->getDescription();
            }
        }

        //get primary language
        foreach ($this->getRegistrationDocumentsRejectionLanguages() as $oRegistrationDocumentsRejectionLanguage) {
            if ($oRegistrationDocumentsRejectionLanguage->getLanguages()->getIsPrimary()) {
                return $oRegistrationDocumentsRejectionLanguage->getDescription();
            }
        }
    }
}
