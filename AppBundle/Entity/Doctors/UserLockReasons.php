<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class UserLockReasons
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Users\UserLockReasonsRepository")
 * @ORM\Table(name="doctor_user_lock_reasons")
 * @ORM\HasLifecycleCallbacks()
 */
class UserLockReasons extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="string_identifier", type="text")
     */
    protected $stringIdentifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\UserLockReasonsLanguages",
     *     mappedBy="userLockReasons",
     *     cascade={"persist"}
     * )
     */
    protected $userLockReasonsLanguages;

    /**
     * UserLockReasons constructor.
     */
    public function __construct()
    {
        $this->userLockReasonsLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStringIdentifier()
    {
        return $this->stringIdentifier;
    }

    /**
     * @param mixed $stringIdentifier
     */
    public function setStringIdentifier($stringIdentifier): void
    {
        $this->stringIdentifier = $stringIdentifier;
    }

    /**
     * @return mixed
     */
    public function getUserLockReasonsLanguages()
    {
        return $this->userLockReasonsLanguages;
    }

    /**
     * @param mixed $userLockReasonsLanguages
     */
    public function setUserLockReasonsLanguages($userLockReasonsLanguages)
    {
        $this->userLockReasonsLanguages = $userLockReasonsLanguages;
    }

    /**
     * @param UserLockReasonsLanguages $userLockReasonsLanguages
     */
    public function addUserLockReasonsLanguage(
        UserLockReasonsLanguages $userLockReasonsLanguages
    ) {
        if (!$this->userLockReasonsLanguages->contains($userLockReasonsLanguages)) {
            $this->userLockReasonsLanguages->add($userLockReasonsLanguages);
        }
    }

    /**
     * @param RegistrationDocumentsRejectionLanguages $registrationDocumentsRejectionLanguages
     */
    public function removeUserLockReasonsLanguage(
        UserLockReasonsLanguages $userLockReasonsLanguages
    ) {
        if ($this->userLockReasonsLanguages->contains($userLockReasonsLanguages)) {
            $this->userLockReasonsLanguages->removeElement($userLockReasonsLanguages);
        }
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getUserLockReasonsLanguages() as $oUserLockReasonsLanguage) {
            if ($oUserLockReasonsLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oUserLockReasonsLanguage->getDescription();
            }
        }

        //get primary language
        foreach ($this->getUserLockReasonsLanguages() as $oUserLockReasonsLanguage) {
            if ($oUserLockReasonsLanguage->getLanguages()->getIsPrimary()) {
                return $oUserLockReasonsLanguage->getDescription();
            }
        }
    }
}
