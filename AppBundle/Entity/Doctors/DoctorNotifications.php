<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Doctors\DoctorNotificationsRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="doctor_notifications")
 */
class DoctorNotifications extends AbstractEntity
{
    const TYPE_FRIEND_REQUEST          = 'friend-request';
    const TYPE_NEW_MESSAGE_SENT        = 'new-message';
    const TYPE_NEW_GROUP_CHAT_NAME     = 'new-group-chat-name';
    const TYPE_USER_REMOVAL            = 'group-user-removal';
    const TYPE_USER_ADDED              = 'group-user-added';
    const TYPE_USER_TYPING             = 'user-typing';
    const TYPE_USER_NOT_TYPING         = 'user-not-typing';
    const TYPE_USER_DELETED_ACCOUNT    = 'user-deleted-account';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    protected $type;

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="sentNotifications"
     * )
     * @ORM\JoinColumn(
     *     name="author_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="CASCADE"
     * )
     */
    protected $author;

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="receivedNotifications",
     * )
     * @ORM\JoinColumn(
     *     name="receiver_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="CASCADE"
     * )
     */
    protected $receiver;

    /**
     * @ORM\Column(name="is_read", type="boolean")
     */
    protected $isRead = false;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return DoctorNotifications
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isRead.
     *
     * @param bool $isRead
     *
     * @return DoctorNotifications
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead.
     *
     * @return bool
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set author.
     *
     * @param Doctors $author
     *
     * @return DoctorNotifications
     */
    public function setAuthor(Doctors $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author.
     *
     * @return Doctors
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set receiver.
     *
     * @param Doctors $receiver
     *
     * @return DoctorNotifications
     */
    public function setReceiver(Doctors $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver.
     *
     * @return Doctors
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
}
