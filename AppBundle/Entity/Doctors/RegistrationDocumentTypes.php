<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class RegistrationDocumentTypes
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Users\RegistrationDocumentTypesRepository")
 * @ORM\Table(name="doctor_registration_document_types")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationDocumentTypes extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="is_identification_document", type="boolean")
     */
    protected $isIdentificationDocument = false;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypeLanguages",
     *     mappedBy="registrationDocumentTypes",
     *     cascade={"persist"}
     * )
     */
    protected $registrationDocumentTypeLanguages;

    /**
     * RegistrationDocumentTypes constructor.
     */
    public function __construct()
    {
        $this->registrationDocumentTypeLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIsIdentificationDocument()
    {
        return $this->isIdentificationDocument;
    }

    /**
     * @param mixed $isIdentificationDocument
     */
    public function setIsIdentificationDocument($isIdentificationDocument)
    {
        $this->isIdentificationDocument = $isIdentificationDocument;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDocumentTypeLanguages()
    {
        return $this->registrationDocumentTypeLanguages;
    }

    /**
     * @param mixed $registrationDocumentTypeLanguages
     */
    public function setRegistrationDocumentTypeLanguages($registrationDocumentTypeLanguages)
    {
        $this->registrationDocumentTypeLanguages = $registrationDocumentTypeLanguages;
    }

    /**
     * @param RegistrationDocumentTypeLanguages $oRegistrationDocumentTypeLanguages
     */
    public function addRegistrationDocumentTypeLanguages(
        RegistrationDocumentTypeLanguages $oRegistrationDocumentTypeLanguages
    ) {
        if (!$this->registrationDocumentTypeLanguages->contains($oRegistrationDocumentTypeLanguages)) {
            $this->registrationDocumentTypeLanguages->add($oRegistrationDocumentTypeLanguages);
        }
    }

    /**
     * @param RegistrationDocumentTypeLanguages $oRegistrationDocumentTypeLanguages
     */
    public function removeRegistrationDocumentTypeLanguages(
        RegistrationDocumentTypeLanguages $oRegistrationDocumentTypeLanguages
    ) {
        if ($this->registrationDocumentTypeLanguages->contains($oRegistrationDocumentTypeLanguages)) {
            $this->registrationDocumentTypeLanguages->removeElement($oRegistrationDocumentTypeLanguages);
        }
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getRegistrationDocumentTypeLanguages() as $oRegistrationDocumentTypeLanguage) {
            if ($oRegistrationDocumentTypeLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oRegistrationDocumentTypeLanguage->getName();
            }
        }

        //get primary language
        foreach ($this->getRegistrationDocumentTypeLanguages() as $oRegistrationDocumentTypeLanguage) {
            if ($oRegistrationDocumentTypeLanguage->getLanguages()->getIsPrimary()) {
                return $oRegistrationDocumentTypeLanguage->getName();
            }
        }
    }
}
