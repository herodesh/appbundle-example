<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class RegistrationDocuments
 * @package Flendoc\AppBundle\Entity\Doctors
 *
 * @ORM\Table(name="doctor_registration_documents")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Doctors\RegistrationDocumentsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RegistrationDocuments extends AbstractEntity
{

    /**
     * Constants used in form action
     */
    const REGISTRATION_DOCUMENT_SAVE = 'save';
    const REGISTRATION_DOCUMENT_SAVE_AND_NOTIFY_USER = 'saveAndNotify';
    const REGISTRATION_DOCUMENT_SAVE_AND_FINISH = 'saveAndFinish';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="file_name", type="string", nullable=false)
     */
    protected $fileName;

    /**
     * @ORM\Column(name="original_name", type="string", nullable=false)
     */
    protected $originalName;

    /**
     * @ORM\Column(name="admin_notes", type="text", nullable=true)
     */
    protected $adminNotes;

    /**
     * @ORM\Column(name="is_rejected", type="boolean")
     */
    protected $isRejected = 0;

    /**
     * @ORM\Column(name="is_accepted", type="boolean")
     */
    protected $isAccepted = 0;

    /**
     * @ORM\Column(name="is_processed", type="boolean")
     */
    protected $isProcessed = 0;

    /**
     * @ORM\Column(name="is_user_notified", type="boolean")
     */
    protected $isUserNotified = 0;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctors;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes"
     * )
     * @ORM\JoinColumn(
     *     name="document_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $documentType;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection"
     * )
     * @ORM\JoinColumn(
     *     name="rejection_reason_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $rejectionReason;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param mixed $originalName
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    }

    /**
     * @return mixed
     */
    public function getAdminNotes()
    {
        return $this->adminNotes;
    }

    /**
     * @param mixed $adminNotes
     */
    public function setAdminNotes($adminNotes)
    {
        $this->adminNotes = $adminNotes;
    }

    /**
     * @return mixed
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * @param mixed $isAccepted
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;
    }

    /**
     * @return mixed
     */
    public function getIsRejected()
    {
        return $this->isRejected;
    }

    /**
     * @param mixed $isRejected
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return mixed
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }

    /**
     * @param mixed $isProcessed
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;
    }

    /**
     * @return mixed
     */
    public function getIsUserNotified()
    {
        return $this->isUserNotified;
    }

    /**
     * @param mixed $isUserNotified
     */
    public function setIsUserNotified($isUserNotified)
    {
        $this->isUserNotified = $isUserNotified;
    }

    /**
     * @return mixed
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * @param mixed $doctors
     */
    public function setDoctors($doctors)
    {
        $this->doctors = $doctors;
    }

    /**
     * @return mixed
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param mixed $documentType
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * @return mixed
     */
    public function getRejectionReason()
    {
        return $this->rejectionReason;
    }

    /**
     * @param mixed $rejectionReason
     */
    public function setRejectionReason($rejectionReason)
    {
        $this->rejectionReason = $rejectionReason;
    }

}
