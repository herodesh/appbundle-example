<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Photos\PhotoAlbums;
use Symfony\Component\Validator\Constraints as Assert;
use Flendoc\AppBundle\Validator\Constraints as SiteAssert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Doctors\DoctorProfileImageRepository")
 * @ORM\Table(name="doctor_profile")
 * @ORM\HasLifecycleCallbacks()
 */
class DoctorProfile extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *      inversedBy="doctorProfile"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\Column(name="profile_photo", type="text", nullable=true)
     */
    protected $profilePhoto;

    /**
     * @ORM\Column(name="first_name", type="string", length=100, options={"comment":"User's first name"})
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\NotNull(groups={"registration"})
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=100, options={"comment":"User's last name"})
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\NotNull(groups={"registration"})
     */
    protected $lastName;

    /**
     * @ORM\Column(name="gender", type="string", length=1, nullable=true, options={"comment":"User's gender"})
     */
    protected $gender;

    /**
     * Validation is done in Doctors.php Entity class
     *
     * @ORM\Column(name="academic_title", type="string", length=100, options={"comment":"User's academic name"}, nullable=true)
     */
    protected $academicTitle;

    /**
     * @ORM\Column(name="about_me", type="text", nullable=true)
     */
    protected $aboutMe;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
    protected $country;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities"
     * )
     * @ORM\JoinColumn(
     *     name="city_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
    protected $city;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages",
     *     cascade={"remove"}
     * )
     * @ORM\JoinTable(
     *     name="doctor_spoken_languages",
     *     joinColumns={@ORM\JoinColumn(name="doctor_profile_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $doctorSpokenLanguages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills",
     *     inversedBy="doctorProfile"
     * )
     * @ORM\JoinTable(
     *     name="doctor_skills",
     *     joinColumns={@ORM\JoinColumn(name="doctor_profile_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $doctorSkills;

    /**
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    protected $zipCode;

    /**
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    protected $birthday;

    /**
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     * @SiteAssert\IsPhone(message="validator.phone.number")
     */
    protected $phone;

    /**
     * @ORM\Column(name="website", type="string", length=100, nullable=true)
     * @Assert\Url()
     */
    protected $website;

    /**
     * @ORM\Column(name="skype", type="string", length=100, nullable=true)
     */
    protected $skype;

    /**
     * @ORM\Column(name="is_public", type="boolean")
     */
    protected $isPublic = false;

    /**
     * DoctorProfile constructor.
     */
    public function __construct()
    {
        $this->doctorSpokenLanguages = new ArrayCollection();
        $this->doctorSkills          = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getProfilePhoto()
    {
        return $this->profilePhoto;
    }

    /**
     * @param mixed $profilePhoto
     */
    public function setProfilePhoto($profilePhoto): void
    {
        $this->profilePhoto = $profilePhoto;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s %s', $this->getAcademicTitle(), $this->getFirstName(), $this->getLastName());
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getAcademicTitle()
    {
        return $this->academicTitle;
    }

    /**
     * @param mixed $academicTitle
     */
    public function setAcademicTitle($academicTitle)
    {
        $this->academicTitle = $academicTitle;
    }

    /**
     * @return mixed
     */
    public function getAboutMe()
    {
        return $this->aboutMe;
    }

    /**
     * @param mixed $aboutMe
     */
    public function setAboutMe($aboutMe)
    {
        $this->aboutMe = $aboutMe;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return mixed
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param mixed $skype
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getDoctorSpokenLanguages()
    {
        return $this->doctorSpokenLanguages;
    }

    /**
     * @param mixed $doctorSpokenLanguages
     */
    public function setDoctorSpokenLanguages($doctorSpokenLanguages)
    {
        $this->doctorSpokenLanguages = $doctorSpokenLanguages;
    }

    /**
     * @param Languages $oLanguage
     */
    public function addDoctorSpokenLanguage(Languages $oLanguage)
    {
        if ($this->doctorSpokenLanguages->contains($oLanguage)) {
            return;
        }
        $this->doctorSpokenLanguages->add($oLanguage);
        //@TODO is this needed here?
        $oLanguage->addDoctorProfile($this);
    }

    /**
     * @param Languages $oLanguage
     */
    public function removeUserSpokenLanguages(Languages $oLanguage)
    {
        if (!$this->doctorSpokenLanguages->contains($oLanguage)) {
            return;
        }

        $this->doctorSpokenLanguages->removeElement($oLanguage);
        //@TODO is this needed here?
        $oLanguage->removeDoctorProfile($this);
    }

    /**
     * @return mixed
     */
    public function getDoctorSkills()
    {
        return $this->doctorSkills;
    }

    /**
     * @param $doctorSkills
     */
    public function setDoctorSkills($doctorSkills)
    {
        $this->doctorSkills = $doctorSkills;
    }

    /**
     * @param Skills $oSkill
     */
    public function addDoctorSkill(Skills $oSkill)
    {
        if ($this->doctorSkills->contains($oSkill)) {
            return;
        }

        $this->doctorSkills->add($oSkill);
        $oSkill->addDoctorProfile($this);
    }

    /**
     * @param Skills $oSkill
     */
    public function removeDoctorSkill(Skills $oSkill)
    {
        if (!$this->doctorSkills->contains($oSkill)) {
            return;
        }

        $this->doctorSkills->removeElement($oSkill);
        $oSkill->removeDoctorProfile($this);
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

}
