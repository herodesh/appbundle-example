<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Chats\DoctorChatFile;
use Flendoc\AppBundle\Entity\Chats\DoctorChatMember;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases;
use Flendoc\AppBundle\Entity\Users\AbstractUser;
use Flendoc\AppBundle\Entity\Users\UserRoles;
use Flendoc\AppBundle\Constants\FeedConstants;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Flendoc\AppBundle\Validator\Constraints as FDAssert;

/**
 * @ORM\Table(name="doctors")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Doctors\DoctorRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("username")
 */
class Doctors extends AbstractUser
{
    const REGISTRATION_TYPE_MEDICAL        = 'medical';
    const REGISTRATION_TYPE_ADMINISTRATIVE = 'administrative';
    const CONTACT_SUGGESTIONS_NUMBER       = 3;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Flendoc\AppBundle\Entity\Users\UserRoles",
     *     inversedBy="doctors"
     * )
     * @ORM\JoinTable(
     *     name="doctors_user_role",
     *     joinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $doctorRoles;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Doctors\DoctorProfile",
     *      mappedBy="doctor",
     *      cascade={"persist"}
     * )
     * @Assert\Valid(groups={"registration"})
     */
    protected $doctorProfile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\DoctorMailSettings",
     *     mappedBy="doctor",
     *     cascade={"persist"}
     * )
     */
    protected $mailSettings;

    /**
     * @var ArrayCollection|DoctorNotifications[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\DoctorNotifications",
     *     mappedBy="author"
     * )
     */
    protected $sentNotifications;

    /**
     * @var ArrayCollection|DoctorNotifications[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\DoctorNotifications",
     *     mappedBy="receiver"
     * )
     */
    protected $receivedNotifications;

    /**
     * @var ArrayCollection|DoctorChatMember[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Chats\DoctorChatMember",
     *     mappedBy="doctor"
     * )
     */
    protected $doctorChatMembers;

    /**
     * @var ArrayCollection|DoctorChatFile[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Chats\DoctorChatFile",
     *     mappedBy="doctorUploader"
     * )
     */
    protected $doctorChatFiles;

    /**
     * @var ArrayCollection|MedicalCases[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *     mappedBy="doctor"
     * )
     */
    protected $medicalCases;

    /**
     * @var ArrayCollection|PendingMedicalCases[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases",
     *     mappedBy="doctor"
     * )
     */
    protected $pendingMedicalCases;

    /**
     * @ORM\Column(name="default_wall_setting", type="string", nullable=false)
     */
    protected $defaultWallSetting = FeedConstants::FEED_PROPERTY_PUBLIC;

    /**
     * @ORM\Column(name="is_verified", type="boolean")
     */
    protected $isVerified = 0;

    /**
     * @ORM\Column(name="intro_viewed", type="boolean")
     */
    protected $introViewed = 0;

    /**
     * @ORM\Column(name="verification_documents_sent", type="boolean")
     */
    protected $verificationDocumentsSent = 0;

    /**
     * @ORM\Column(name="is_ghost_user", type="boolean")
     */
    protected $isGhostUser = 0;

    /**
     * @ORM\Column(name="is_fake_account", type="boolean")
     */
    protected $isFakeAccount = 0;

    //elements used for model validation on registration
    //All this elements bellow will be added to user profile
    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     */
    protected $firstName;

    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     */
    protected $lastName;

    /**
     * Not mapped into the database. Needed to add validation here because he will just ignore it in his own class
     */
    protected $academicTitle;

    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     */
    protected $country;

    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     * @FDAssert\CityBelongsToCountry(groups={"registration"})
     */
    protected $city;

    /**
     * Not mapped to the DB
     * @Assert\IsTrue(groups={"registration"})
     */
    protected $terms;
    //end model validation elements

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *     inversedBy="medicalCaseBookmarks",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(
     *    name="medical_case_bookmarks",
     *    joinColumns={@ORM\JoinColumn(name="doctor_id", referencedColumnName="id", onDelete="CASCADE")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="medical_case_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $medicalCaseBookmarks;

    /**
     * @var ArrayCollection|DoctorExistingEmailAddresses[]
     *
     * @ORM\OneToMany(
     *     targetEntity="DoctorExistingEmailAddresses",
     *     mappedBy="doctor",
     *     cascade={"persist"}
     * )
     */
    protected $existingEmailAddresses;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enabled_at", type="datetime", nullable=true)
     */
    protected $enabledAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="no_verification_documents_notifications_sent", type="boolean")
     */
    protected $noVerificationDocumentsNotificationsSent = false;

    /**
     * @ORM\Column(name="verification_documents_sent_at", type="datetime", nullable=true)
     */
    protected $verificationDocumentsSentAt = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mailSettings           = new ArrayCollection();
        $this->doctorRoles            = new ArrayCollection();
        $this->medicalCaseBookmarks   = new ArrayCollection();
        $this->existingEmailAddresses = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getIdentifier();
    }

    /**
     * @return mixed
     */
    public function getAcademicTitle()
    {
        return $this->academicTitle;
    }

    /**
     * @param mixed $academicTitle
     */
    public function setAcademicTitle($academicTitle)
    {
        $this->academicTitle = $academicTitle;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $aDoctorRoles = [];
        foreach ($this->doctorRoles as $oDoctorRole) {
            $aDoctorRoles[] = $oDoctorRole->getName();
        }

        return $aDoctorRoles;
    }

    /**
     * @param mixed $doctorRoles
     */
    public function setRoles($doctorRoles)
    {
        $this->doctorRoles = $doctorRoles;
    }

    /**
     * @param UserRoles $oUserRoles
     */
    public function addRole(UserRoles $oUserRoles)
    {
        if ($this->doctorRoles->contains($oUserRoles)) {
            return;
        }

        $this->doctorRoles->add($oUserRoles);
        $oUserRoles->addDoctor($this);
    }

    /**
     * @param UserRoles $oUserRoles
     */
    public function removeRole(UserRoles $oUserRoles)
    {
        if (!$this->doctorRoles->contains($oUserRoles)) {
            return;
        }

        $this->doctorRoles->removeElement($oUserRoles);
        $oUserRoles->removeDoctor($this);
    }

    /**
     * @return mixed
     */
    public function getDoctorProfile()
    {
        return $this->doctorProfile;
    }

    /**
     * @param mixed $doctorProfile
     */
    public function setDoctorProfile($doctorProfile)
    {
        $this->doctorProfile = $doctorProfile;
    }

    /**
     * @return mixed
     */
    public function getMailSettings()
    {
        return $this->mailSettings;
    }

    /**
     * @param mixed $mailSettings
     */
    public function setMailSettings($mailSettings)
    {
        $this->mailSettings = $mailSettings;
    }

    /**
     * @return mixed
     */
    public function getIsVerified()
    {
        //this is a mix of user type and current field
        if (false == $this->getIsAdministrative() && true == $this->isVerified) {
            return true;
        }

        return false;
        //return $this->isVerified;
    }

    /**
     * @param mixed $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return mixed
     */
    public function getIntroViewed()
    {
        return $this->introViewed;
    }

    /**
     * @param mixed $introViewed
     */
    public function setIntroViewed($introViewed): void
    {
        $this->introViewed = $introViewed;
    }

    /**
     * @return mixed
     */
    public function getVerificationDocumentsSent()
    {
        return $this->verificationDocumentsSent;
    }

    /**
     * @param mixed $verificationDocumentsSent
     */
    public function setVerificationDocumentsSent($verificationDocumentsSent)
    {
        $this->verificationDocumentsSent = $verificationDocumentsSent;
    }

    /**
     * @return mixed
     */
    public function getisGhostUser()
    {
        return $this->isGhostUser;
    }

    /**
     * @param mixed $isGhostUser
     */
    public function setIsGhostUser($isGhostUser): void
    {
        $this->isGhostUser = $isGhostUser;
    }

    /**
     * @return mixed
     */
    public function getIsFakeAccount()
    {
        return $this->isFakeAccount;
    }

    /**
     * @param mixed $isFakeAccount
     */
    public function setIsFakeAccount($isFakeAccount)
    {
        $this->isFakeAccount = $isFakeAccount;
    }

    /**
     * @param DoctorMailSettings $mailSettings
     */
    public function addMailSetting(DoctorMailSettings $mailSettings)
    {
        $this->mailSettings->add($mailSettings);
    }

    /**
     * @param DoctorMailSettings $mailSettings
     */
    public function removeMailSetting(DoctorMailSettings $mailSettings)
    {
        $this->mailSettings->remove($mailSettings);
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param mixed $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return string
     */
    public function getDoctorAccountType()
    {
        if ($this->getIsAdministrative()) {
            return self::REGISTRATION_TYPE_ADMINISTRATIVE;
        }

        return self::REGISTRATION_TYPE_MEDICAL;
    }

    /**
     * Add sentNotification.
     *
     * @param DoctorNotifications $sentNotification
     *
     * @return Doctors
     */
    public function addSentNotification(DoctorNotifications $sentNotification)
    {
        $this->sentNotifications[] = $sentNotification;

        return $this;
    }

    /**
     * Remove sentNotification.
     *
     * @param DoctorNotifications $sentNotification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSentNotification(DoctorNotifications $sentNotification)
    {
        return $this->sentNotifications->removeElement($sentNotification);
    }

    /**
     * Get sentNotifications.
     *
     * @return ArrayCollection|DoctorNotifications[]
     */
    public function getSentNotifications()
    {
        return $this->sentNotifications;
    }

    /**
     * Add receivedNotification.
     *
     * @param DoctorNotifications $receivedNotification
     *
     * @return Doctors
     */
    public function addReceivedNotification(DoctorNotifications $receivedNotification)
    {
        $this->receivedNotifications[] = $receivedNotification;

        return $this;
    }

    /**
     * Remove receivedNotification.
     *
     * @param DoctorNotifications $receivedNotification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReceivedNotification(DoctorNotifications $receivedNotification)
    {
        return $this->receivedNotifications->removeElement($receivedNotification);
    }

    /**
     * Get receivedNotifications.
     *
     * @return ArrayCollection|DoctorNotifications[]
     */
    public function getReceivedNotifications()
    {
        return $this->receivedNotifications;
    }

    /**
     * Add doctorChatMember.
     *
     * @param DoctorChatMember $doctorChatMember
     *
     * @return Doctors
     */
    public function addDoctorChatMember(DoctorChatMember $doctorChatMember)
    {
        $this->doctorChatMembers[] = $doctorChatMember;

        return $this;
    }

    /**
     * Remove doctorChatMember.
     *
     * @param DoctorChatMember $doctorChatMember
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorChatMember(DoctorChatMember $doctorChatMember)
    {
        return $this->doctorChatMembers->removeElement($doctorChatMember);
    }

    /**
     * Get doctorChatMembers.
     *
     * @return ArrayCollection|DoctorChatMember[]
     */
    public function getDoctorChatMembers()
    {
        return $this->doctorChatMembers;
    }

    /**
     * Add doctorChatFile.
     *
     * @param DoctorChatFile $doctorChatFile
     *
     * @return Doctors
     */
    public function addDoctorChatFile(DoctorChatFile $doctorChatFile)
    {
        $this->doctorChatFiles[] = $doctorChatFile;

        return $this;
    }

    /**
     * Remove doctorChatFile.
     *
     * @param DoctorChatFile $doctorChatFile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorChatFile(DoctorChatFile $doctorChatFile)
    {
        return $this->doctorChatFiles->removeElement($doctorChatFile);
    }

    /**
     * Get doctorChatFiles.
     *
     * @return ArrayCollection|DoctorChatFile[]
     */
    public function getDoctorChatFiles()
    {
        return $this->doctorChatFiles;
    }

    /**
     * Add doctorCase.
     *
     * @param MedicalCases $medicalCases
     *
     * @return Doctors
     */
    public function addMedicalCases(MedicalCases $medicalCases)
    {
        $this->medicalCases[] = $medicalCases;

        return $this;
    }

    /**
     * Remove medicalCase.
     *
     * @param MedicalCases $medicalCases
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCases(MedicalCases $medicalCases)
    {
        return $this->medicalCases->removeElement($medicalCases);
    }

    /**
     * Get medicalCases.
     *
     * @return ArrayCollection|MedicalCases[]
     */
    public function getMedicalCases()
    {
        return $this->medicalCases;
    }

    /**
     * Add pendingMedicalCases.
     *
     * @param PendingMedicalCases $pendingMedicalCases
     *
     * @return Doctors
     */
    public function addPendingMedicalCases(PendingMedicalCases $pendingMedicalCases)
    {
        $this->pendingMedicalCases[] = $pendingMedicalCases;

        return $this;
    }

    /**
     * Remove pendingMedicalCases.
     *
     * @param PendingMedicalCases $pendingMedicalCases
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePendingMedicalCases(PendingMedicalCases $pendingMedicalCases)
    {
        return $this->pendingMedicalCases->removeElement($pendingMedicalCases);
    }

    /**
     * Get pendingMedicalCases.
     *
     * @return ArrayCollection|PendingMedicalCases
     */
    public function getPendingMedicalCases()
    {
        return $this->pendingMedicalCases;
    }

    /**
     * Add medicalCaseBookmarks.
     *
     * @param MedicalCases $medicalCaseBookmark
     *
     * @return Doctors
     */
    public function addMedicalCaseBookmark(MedicalCases $medicalCasesBookmarks)
    {
        $this->medicalCaseBookmarks[] = $medicalCasesBookmarks;

        return $this;
    }

    /**
     * Remove medicalCasesBookmarks.
     *
     * @param MedicalCases $medicalCaseBookmark
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCaseBookmark(MedicalCases $medicalCasesBookmarks)
    {
        return $this->medicalCaseBookmarks->removeElement($medicalCasesBookmarks);
    }

    /**
     * Get medicalCasesBookmarks.
     *
     * @return ArrayCollection|MedicalCases[]
     */
    public function getMedicalCaseBookmarks()
    {
        return $this->medicalCaseBookmarks;
    }

    /**
     * @return mixed
     */
    public function getDefaultWallSetting()
    {
        return $this->defaultWallSetting;
    }

    /**
     * @param mixed $defaultWallSetting
     */
    public function setDefaultWallSetting($defaultWallSetting): void
    {
        $this->defaultWallSetting = $defaultWallSetting;
    }

    /**
     * Add doctorRole.
     *
     * @param UserRoles $doctorRole
     *
     * @return Doctors
     */
    public function addDoctorRole(UserRoles $doctorRole)
    {
        $this->doctorRoles[] = $doctorRole;

        return $this;
    }

    /**
     * Remove doctorRole.
     *
     * @param UserRoles $doctorRole
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorRole(UserRoles $doctorRole)
    {
        return $this->doctorRoles->removeElement($doctorRole);
    }

    /**
     * Get doctorRoles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorRoles()
    {
        return $this->doctorRoles;
    }

    /**
     * Add existingEmailAddress.
     *
     * @param DoctorExistingEmailAddresses $existingEmailAddress
     *
     * @return Doctors
     */
    public function addExistingEmailAddress(DoctorExistingEmailAddresses $existingEmailAddress)
    {
        $this->existingEmailAddresses->add($existingEmailAddress);

        return $this;
    }

    /**
     * Remove existingEmailAddress.
     *
     * @param DoctorExistingEmailAddresses $existingEmailAddress
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeExistingEmailAddress(DoctorExistingEmailAddresses $existingEmailAddress)
    {
        return $this->existingEmailAddresses->removeElement($existingEmailAddress);
    }

    /**
     * Get existingEmailAddresses.
     *
     * @return ArrayCollection|DoctorExistingEmailAddresses[]
     */
    public function getExistingEmailAddresses()
    {
        return $this->existingEmailAddresses;
    }

    /**
     * Set enabledAt
     *
     * @param \DateTime|null $oDateTime
     * @throws \Exception
     */
    public function setEnabledAt(\DateTime $oDateTime = null)
    {
        $this->enabledAt = $oDateTime ?: new \DateTime();
    }

    /**
     * Get enabledAt
     *
     * @return \DateTime
     */
    public function getEnabledAt()
    {
        return $this->enabledAt;
    }

    /**
     * Set noVerificationDocumentsNotificationsSent
     *
     * @param bool $bNoVerificationDocumentsNotificationsSent
     */
    public function setNoVerificationDocumentsNotificationsSent($bNoVerificationDocumentsNotificationsSent)
    {
        $this->noVerificationDocumentsNotificationsSent = $bNoVerificationDocumentsNotificationsSent;
    }

    /**
     * Get noVerificationDocumentsNotificationsSent
     *
     * @return bool
     */
    public function getNoVerificationDocumentsNotificationsSent()
    {
        return $this->noVerificationDocumentsNotificationsSent;
    }

    /**
     * Set verificationDocumentsSentAt
     */
    public function setVerificationDocumentsSentAt()
    {
        $this->verificationDocumentsSentAt = new \DateTime();
    }

    /**
     * Get verificationDocumentsSentAt
     *
     * @return \DateTime|null
     */
    public function getVerificationDocumentsSentAt()
    {
        return $this->verificationDocumentsSentAt;
    }
}
