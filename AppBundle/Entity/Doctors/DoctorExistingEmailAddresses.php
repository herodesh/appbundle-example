<?php

namespace Flendoc\AppBundle\Entity\Doctors;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * This entity saves the emails that were already used on the application by other users
 * When a user wants to register and the email was already used in our system he will get an error
 * If a user wants to update his email and the email belongs to him he can do it otherwise he gets an error
 * This was done in order to avoid multiple usages of an email after a user is already using it.
 *
 * * Ticket for this task is here:
 * * https://flendoc.atlassian.net/browse/FLEN-457
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Doctors\DoctorExistingEmailAddressesRepository")
 * @ORM\Table(name="doctor_existing_email_addresses")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("email")
 */
class DoctorExistingEmailAddresses extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="existingEmailAddresses"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=false, unique=true)
     */
    protected $email;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return DoctorExistingEmailAddresses
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set doctor.
     *
     * @param Doctors $doctor
     *
     * @return DoctorExistingEmailAddresses
     */
    public function setDoctor(Doctors $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor.
     *
     * @return Doctors
     */
    public function getDoctor()
    {
        return $this->doctor;
    }
}
