<?php

namespace Flendoc\AppBundle\Entity\Users;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Flendoc\AppBundle\Validator\Constraints as FDAssert;

/**
 * Class AbstractUser
 * @package Flendoc\AppBundle\Entity\Users
 * @UniqueEntity("username", groups={"registration"}, message="registration.username.already.used")
 */
class AbstractUser extends AbstractEntity implements UserInterface, AdvancedUserInterface, \Serializable
{
    const TYPE_DOCTORS = 'doctors';
    const TYPE_ADMINS  = 'admins';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", nullable=false, unique=true)
     * @Assert\NotNull(groups={"registration"})
     * @Assert\Email(groups={"registration"})
     * @FDAssert\IsNotDisposableEmail(groups={"registration"})
     * @FDAssert\EmailUsed(groups={"registration"})
     */
    protected $username;

    /**
     * Not mapped into the database
     */
    protected $gender;

    /**
     * Encrypted password. Must be persisted.
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @Assert\NotBlank(groups={"registration", "passwordUpdate"})
     * @Assert\Length(min=8, max=4096, groups={"registration", "passwordUpdate"})
     */
    protected $plainPassword;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = false;

    /**
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked = false;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Doctors\UserLockReasons"
     * )
     * @ORM\JoinColumn(
     *      name="user_lock_reason_id",
     *      referencedColumnName="id",
     *      onDelete="CASCADE"
     * )
     */
    protected $lockedReason;

    /**
     * @ORM\Column(name="expired", type="boolean")
     */
    protected $expired = false;

    /**
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @ORM\Column(name="credentials_expired", type="boolean")
     */
    protected $credentialsExpired = false;

    /**
     * @ORM\Column(name="default_language", type="string")
     */
    protected $defaultLanguage;

    /**
     * @ORM\Column(name="is_student", type="boolean")
     */
    protected $isStudent = false;

    /**
     * @ORM\Column(name="is_administrative", type="boolean")
     */
    protected $isAdministrative = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return mixed
     */
    public function getLockedReason()
    {
        return $this->lockedReason;
    }

    /**
     * @param mixed $lockedReason
     */
    public function setLockedReason($lockedReason): void
    {
        $this->lockedReason = $lockedReason;
    }

    /**
     * @return mixed
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * @param mixed $expired
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;
    }

    /**
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param mixed $expiresAt
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param mixed $lastLogin
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return mixed
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param mixed $passwordRequestedAt
     */
    public function setPasswordRequestedAt($passwordRequestedAt)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
    }

    /**
     * @return mixed
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * @param mixed $credentialsExpired
     */
    public function setCredentialsExpired($credentialsExpired)
    {
        $this->credentialsExpired = $credentialsExpired;
    }

    /**
     * @return mixed
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * @param mixed $defaultLanguage
     */
    public function setDefaultLanguage($defaultLanguage)
    {
        $this->defaultLanguage = $defaultLanguage;
    }

    /**
     * @return mixed
     */
    public function getIsStudent()
    {
        return $this->isStudent;
    }

    /**
     * @param mixed $isStudent
     */
    public function setIsStudent($isStudent): void
    {
        $this->isStudent = $isStudent;
    }

    /**
     * @return mixed
     */
    public function getIsAdministrative()
    {
        return $this->isAdministrative;
    }

    /**
     * @param mixed $isAdministrative
     */
    public function setIsAdministrative($isAdministrative): void
    {
        $this->isAdministrative = $isAdministrative;
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Erase credentials
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return !$this->getExpired();
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return !$this->getLocked();
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        return !$this->getExpired();
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * @see \Serializable::serialize()
     *
     * @return string
     */
    public function serialize()
    {
        return json_encode([
            $this->id,
            $this->username,
            $this->password,
            $this->enabled,
            $this->locked,
            $this->expired,
        ]);
    }

    /**
     * @see \Serializable::serialize()
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->enabled,
            $this->locked
            ) = json_decode($serialized);
    }

    /**
     * Is PasswordRequest less than 1 day?
     *
     * @param string $dMaxTimeAllowed
     *
     * @return bool
     */
    public function isPasswordRequestNonExpired($dMaxTimeAllowed = '+1 day')
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime && strtotime($dMaxTimeAllowed, $this->getPasswordRequestedAt()
                                                                                                        ->getTimestamp()) > time();
    }

    /**
     * Generate New Token
     *
     * @param int $iLength
     *
     * @return string
     */
    public function generateNewToken($iLength = 32)
    {
        return bin2hex(openssl_random_pseudo_bytes($iLength));
    }

    /**
     * @return null
     */
    public function getRoles()
    {
        return null;
    }
}
