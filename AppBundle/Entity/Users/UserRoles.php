<?php
namespace Flendoc\AppBundle\Entity\Users;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class UserRoles
 * @package Flendoc\UserBundle\Entity
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Users\UserRolesRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user_roles")
 */
class UserRoles extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     mappedBy="doctorRoles",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $doctors;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Admins\Admins",
     *     mappedBy="adminRoles",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $admins;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * @param mixed $doctors
     */
    public function setDoctors($doctors)
    {
        $this->doctors = $doctors;
    }

    /**
     * @param Doctors $oDoctors
     */
    public function addDoctor(Doctors $oDoctors)
    {
        if ($this->doctors->contains($oDoctors)) {
            return;
        }

        $this->doctors->add($oDoctors);
        $oDoctors->addRole($this);
    }

    /**
     * @param Doctors $oDoctors
     */
    public function removeDoctor(Doctors $oDoctors)
    {
        if (!$this->doctors->contains($oDoctors)) {
            return;
        }

        $this->doctors->removeElement($oDoctors);
        $oDoctors->removeRole($this);
    }

    /**
     * @return mixed
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    /**
     * @param mixed $admins
     */
    public function setAdmins($admins)
    {
        $this->admins = $admins;
    }

    /**
     * @param Admins $oAdmins
     */
    public function addAdmin(Admins $oAdmins)
    {
        if ($this->admins->contains($oAdmins)) {
            return;
        }

        $this->admins->add($oAdmins);
        $oAdmins->addRole($this);
    }

    /**
     * @param Admins $oAdmins
     */
    public function removeAdmin(Admins $oAdmins)
    {
        if (!$this->admins->contains($oAdmins)) {
            return;
        }

        $this->admins->removeElement($oAdmins);
        $oAdmins->removeRole($this);
    }

}
