<?php

namespace Flendoc\AppBundle\Entity\Countries;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Countries\CountriesRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="countries")
 * @UniqueEntity("code")
 */
class Countries extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=2, unique=true)
     * @Assert\NotBlank()
     */
    protected $code;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    protected $isActive = false;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Flendoc\AppBundle\Entity\Countries\CountryLanguages",
     *      mappedBy="countries",
     *      cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    protected $countryLanguages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countryLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Add country language
     *
     * @param CountryLanguages $countryLanguages
     */
    public function addCountryLanguage(CountryLanguages $countryLanguages)
    {
        if (!$this->countryLanguages->contains($countryLanguages)) {
            $this->countryLanguages->add($countryLanguages);
        }
    }

    /**
     * remove country language
     *
     * @param CountryLanguages $countryLanguages
     */
    public function removeCountryLanguage(CountryLanguages $countryLanguages)
    {
        $this->countryLanguages->removeElement($countryLanguages);
    }

    /**
     * @return mixed
     */
    public function getCountryLanguages()
    {
        return $this->countryLanguages;
    }

    /**
     * @param mixed $countryLanguages
     */
    public function setCountryLanguages($countryLanguages)
    {
        $this->countryLanguages = $countryLanguages;
    }

    /**
     * Returns the country default name based on the default selected language of the application
     * @return string
     */
    public function getCountryDefaultName()
    {
        foreach ($this->getCountryLanguages() as $oCountryLanguage) {
            if ($oCountryLanguage->getLanguages()->getIsPrimary()) {
                return $oCountryLanguage->getName();
            }
        }

        return $this->getCountryLanguages()->first()->getName();
    }
}
