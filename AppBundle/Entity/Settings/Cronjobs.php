<?php

namespace Flendoc\AppBundle\Entity\Settings;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Cronjobs
 * @package Flendoc\AppBundle\Entity\Settings
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Settings\CronjobsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="settings_cronjobs")
 */
class Cronjobs extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="command_name", type="string")
     */
    protected $commandName;

    /**
     * @ORM\Column(name="command_description", type="string")
     */
    protected $commandDescription;

    /**
     * @ORM\Column(name="has_task_to_do", type="boolean")
     */
    protected $hasTaskToDo = false;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommandName()
    {
        return $this->commandName;
    }

    /**
     * @param mixed $commandName
     */
    public function setCommandName($commandName): void
    {
        $this->commandName = $commandName;
    }

    /**
     * @return mixed
     */
    public function getCommandDescription()
    {
        return $this->commandDescription;
    }

    /**
     * @param mixed $commandDescription
     */
    public function setCommandDescription($commandDescription): void
    {
        $this->commandDescription = $commandDescription;
    }

    /**
     * @return mixed
     */
    public function getHasTaskToDo()
    {
        return $this->hasTaskToDo;
    }

    /**
     * @param mixed $hasTaskToDo
     */
    public function setHasTaskToDo($hasTaskToDo): void
    {
        $this->hasTaskToDo = $hasTaskToDo;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }
}
