<?php

namespace Flendoc\AppBundle\Entity\Jobs;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class JobTypeLanguages
 * @package Flendoc\AppBundle\Entity\Jobs
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Jobs\JobTypeLanguagesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="job_type_languages")
 */
class JobTypeLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id"
     * )
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\JobTypes",
     *     inversedBy="jobTypeLanguages"
     * )
     * @ORM\JoinColumn(
     *     name="job_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $jobType;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    /**
     * @param mixed $jobType
     */
    public function setJobType($jobType)
    {
        $this->jobType = $jobType;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
