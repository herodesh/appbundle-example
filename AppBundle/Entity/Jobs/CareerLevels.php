<?php

namespace Flendoc\AppBundle\Entity\Jobs;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CareerLevels
 * @package Flendoc\AppBundle\Entity\Jobs
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Jobs\CareerLevelsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="career_levels")
 */
class CareerLevels extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\CareerLevelLanguages",
     *     mappedBy="careerLevel",
     *     cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    protected $careerLevelLanguages;

    /**
     * JobTypes constructor.
     */
    public function __construct()
    {
        $this->careerLevelLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCareerLevelLanguages()
    {
        return $this->careerLevelLanguages;
    }

    /**
     * @param mixed $careerLevelLanguages
     */
    public function setCareerLevelLanguages($careerLevelLanguages)
    {
        $this->careerLevelLanguages = $careerLevelLanguages;
    }

    /**
     * @param CareerLevelLanguages $careerLevelLanguage
     */
    public function addCareerLevelLanguage(CareerLevelLanguages $careerLevelLanguage)
    {
        if (!$this->careerLevelLanguages->contains($careerLevelLanguage)) {
            $this->careerLevelLanguages->add($careerLevelLanguage);
        }
    }

    /**
     * @param CareerLevelLanguages $careerLevelLanguage
     */
    public function removeCareerLevelLanguage(CareerLevelLanguages $careerLevelLanguage)
    {
        if ($this->careerLevelLanguages->contains($careerLevelLanguage)) {
            $this->careerLevelLanguages->removeElement($careerLevelLanguage);
        }
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getCareerLevelLanguages() as $oCareerLevelLanguage) {
            if ($oCareerLevelLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oCareerLevelLanguage->getName();
            }
        }

        //get primary language
        foreach ($this->getCareerLevelLanguages() as $oCareerLevelLanguage) {
            if ($oCareerLevelLanguage->getLanguages()->getIsPrimary()) {
                return $oCareerLevelLanguage->getName();
            }
        }
    }

}
