<?php

namespace Flendoc\AppBundle\Entity\Jobs;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Resumes\Resumes;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class JobTypes
 * @package Flendoc\AppBundle\Entity\Jobs
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Jobs\JobTypesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="job_types")
 */
class JobTypes extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\JobTypeLanguages",
     *     mappedBy="jobType",
     *     cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    protected $jobTypeLanguages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes",
     *     mappedBy="jobType",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $resumes;

    /**
     * JobTypes constructor.
     */
    public function __construct()
    {
        $this->jobTypeLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getJobTypeLanguages()
    {
        return $this->jobTypeLanguages;
    }

    /**
     * @param mixed $jobTypeLanguage
     */
    public function setJobTypeLanguages($jobTypeLanguage)
    {
        $this->jobTypeLanguages = $jobTypeLanguage;
    }

    /**
     * @param JobTypeLanguages $jobTypeLanguage
     */
    public function addJobTypeLanguage(JobTypeLanguages $jobTypeLanguage)
    {
        if (!$this->jobTypeLanguages->contains($jobTypeLanguage)) {
            $this->jobTypeLanguages->add($jobTypeLanguage);
        }
    }

    /**
     * @param JobTypeLanguages $jobTypeLanguage
     */
    public function removeJobTypeLanguage(JobTypeLanguages $jobTypeLanguage)
    {
        if ($this->jobTypeLanguages->contains($jobTypeLanguage)) {
            $this->jobTypeLanguages->removeElement($jobTypeLanguage);
        }
    }

    /**
     * @return mixed
     */
    public function getResumes()
    {
        return $this->resumes;
    }

    /**
     * @param mixed $resumes
     */
    public function setResumes($resumes)
    {
        $this->resumes = $resumes;
    }

    /**
     * @param Resumes $oResume
     */
    public function addResume(Resumes $oResume)
    {
        if ($this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->add($oResume);
        $oResume->addJobType($this);
    }

    /**
     * @param Resumes $oResume
     */
    public function removeResume(Resumes $oResume)
    {
        if (!$this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->removeElement($oResume);
        $oResume->removeJobType($this);
    }

    /**
     * @param Languages $oLanguages
     *
     * @return mixed
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getJobTypeLanguages() as $oJobTypeLanguage) {
            if ($oJobTypeLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oJobTypeLanguage->getName();
            }
        }

        //get primary language
        foreach ($this->getJobTypeLanguages() as $oJobTypeLanguage) {
            if ($oJobTypeLanguage->getLanguages()->getIsPrimary()) {
                return $oJobTypeLanguage->getName();
            }
        }
    }

}
