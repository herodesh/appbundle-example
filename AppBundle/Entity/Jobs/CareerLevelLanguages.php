<?php

namespace Flendoc\AppBundle\Entity\Jobs;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CareerLevelLanguages
 * @package Flendoc\AppBundle\Entity\Jobs
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="career_level_languages")
 */
class CareerLevelLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id"
     * )
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\CareerLevels",
     *     inversedBy="careerLevelLanguages"
     * )
     * @ORM\JoinColumn(
     *     name="job_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $careerLevel;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getCareerLevel()
    {
        return $this->careerLevel;
    }

    /**
     * @param mixed $careerLevel
     */
    public function setCareerLevel($careerLevel)
    {
        $this->careerLevel = $careerLevel;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
