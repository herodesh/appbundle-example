<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\HashTags\HashTags;
use Flendoc\AppBundle\Entity\Skills\Skills;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalCases\MedicalCaseRepository")
 * @ORM\Table(name="medical_cases")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalCases extends AbstractMedicalCases
{

    const LATEST_MEDICAL_CASES_SEARCH_LIMIT = 3;
    const MY_PUBLISHED_CASES                = 'my-published-cases';
    const MY_DRAFTS                         = 'my-drafts';
    const MY_CASES_PENDING_APPROVAL         = 'my-cases-pending-approval';

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="medicalCases"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     mappedBy="medicalCaseBookmarks",
     *     cascade={"persist"}
     * )
     */
    protected $medicalCaseBookmarks;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills",
     *     inversedBy="medicalCases"
     * )
     * @ORM\JoinTable(
     *     name="medical_case_skills",
     *     joinColumns={@ORM\JoinColumn(name="medical_case_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")}
     * )
     */
    protected $medicalCaseSkills;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCaseImages",
     *     mappedBy="medicalCase"
     * )
     */
    protected $medicalCaseImages;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\HashTags\HashTags",
     *     mappedBy="medicalCase"
     * )
     */
    protected $hashTag;

    /**
     * @ORM\Column(name="has_hash_tag", type="boolean")
     */
    protected $hasHashTag = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pending", type="boolean", nullable=false)
     */
    protected $isPending = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_admin_revised", type="boolean", nullable=false)
     */
    protected $isAdminRevised = false;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases",
     *      mappedBy="medicalCase"
     * )
     */
    protected $pendingMedicalCases;

    /**
     * @ORM\Column(name="is_social_added", type="boolean")
     */
    protected $isSocialAdded = false;

    /**
     * DoctorCase constructor.
     */
    public function __construct()
    {
        $this->medicalCaseSkills    = new ArrayCollection();
        $this->medicalCaseImages    = new ArrayCollection();
        $this->medicalCaseBookmarks = new ArrayCollection();
        $this->hashTag              = new ArrayCollection();
    }

    /**
     * Set doctor.
     *
     * @param Doctors $doctor
     *
     * @return $this
     */
    public function setDoctor(Doctors $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor.
     *
     * @return Doctors
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set caseSkills.
     *
     * @param ArrayCollection|Skills[] $medicalCaseSkills
     *
     * @return MedicalCases
     */
    public function setMedicalCaseSkills($medicalCaseSkills)
    {
        $this->medicalCaseSkills = $medicalCaseSkills;

        return $this;
    }

    /**
     * Add medicalCasesSkills.
     *
     * @param Skills $medicalCaseSkills
     *
     * @return MedicalCases
     */
    public function addMedicalCaseSkills(Skills $medicalCaseSkills)
    {
        $this->medicalCaseSkills[] = $medicalCaseSkills;

        return $this;
    }

    /**
     * Remove medicalCaseSkill.
     *
     * @param Skills $medicalCaseSkill
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCaseSkill(Skills $medicalCaseSkill)
    {
        return $this->medicalCaseSkills->removeElement($medicalCaseSkill);
    }

    /**
     * Get medicalCaseSkills.
     *
     * @return ArrayCollection|Skills[]
     */
    public function getMedicalCaseSkills()
    {
        return $this->medicalCaseSkills;
    }

    /**
     * Add medicalCaseImage.
     *
     * @param MedicalCaseImages $medicalCaseImage
     *
     * @return MedicalCases
     */
    public function addMedicalCaseImage(MedicalCaseImages $medicalCaseImage)
    {
        $this->medicalCaseImages[] = $medicalCaseImage;

        return $this;
    }

    /**
     * Remove medicalCaseImage.
     *
     * @param MedicalCaseImages $medicalCaseImages
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCaseImage(MedicalCaseImages $medicalCaseImages)
    {
        return $this->medicalCaseImages->removeElement($medicalCaseImages);
    }

    /**
     * @param MedicalCaseImages $medicalCaseImages
     *
     * @return $this
     */
    public function setMedicalCaseImages($medicalCaseImages)
    {
        $this->medicalCaseImages = $medicalCaseImages;

        return $this;
    }

    /**
     * Get medicalCaseImages.
     *
     * @return ArrayCollection|MedicalCaseImages[]
     */
    public function getMedicalCaseImages()
    {
        return $this->medicalCaseImages;
    }

    /**
     * Get Photos from the first MedicalCaseImage
     *
     * @return string|null
     */
    public function getFirstMedicalCaseImagePhotos()
    {
        if ($this->medicalCaseImages->count()) {
            $oMedicalCaseImage = $this->medicalCaseImages->first();

            return $oMedicalCaseImage->getPhotos();
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getHashTag()
    {
        return $this->hashTag;
    }

    /**
     * @param mixed $hashTag
     */
    public function setHashTag($hashTag): void
    {
        $this->hashTag = $hashTag;
    }

    /**
     * @param HashTags $hashTags
     */
    public function addHashTag(HashTags $hashTags)
    {
        if (!$this->hashTag->contains($hashTags)) {
            $this->hashTag->add($hashTags);
        }
    }

    /**
     * @param HashTags $hashTags
     */
    public function removeHashTag(HashTags $hashTags)
    {
        $this->hashTag->removeElement($hashTags);
    }

    /**
     * @return mixed
     */
    public function getHasHashTag()
    {
        return $this->hasHashTag;
    }

    /**
     * @param mixed $hasHashTag
     */
    public function setHasHashTag($hasHashTag): void
    {
        $this->hasHashTag = $hasHashTag;
    }

    /**
     * Set isPending.
     *
     * @param bool $isPending
     *
     * @return MedicalCases
     */
    public function setIsPending($isPending)
    {
        $this->isPending = $isPending;

        return $this;
    }

    /**
     * Get isPending.
     *
     * @return bool
     */
    public function getIsPending()
    {
        return $this->isPending;
    }

    /**
     * Set isAdminRevised.
     *
     * @param bool $isAdminRevised
     *
     * @return MedicalCases
     */
    public function setIsAdminRevised($isAdminRevised)
    {
        $this->isAdminRevised = $isAdminRevised;

        return $this;
    }

    /**
     * Get isAdminRevised.
     *
     * @return bool
     */
    public function getIsAdminRevised()
    {
        return $this->isAdminRevised;
    }

    /**
     * Set doctorCasePending.
     *
     * @param PendingMedicalCases|null $pendingMedicalCases
     *
     * @return MedicalCases
     */
    public function setPendingMedicalCases(PendingMedicalCases $pendingMedicalCases = null)
    {
        $this->pendingMedicalCases = $pendingMedicalCases;

        return $this;
    }

    /**
     * Get doctorCasePending.
     *
     * @return PendingMedicalCases|null
     */
    public function getPendingMedicalCase()
    {
        return $this->pendingMedicalCases;
    }

    /**
     * Add medicalCaseBookmarks.
     *
     * @param Doctors $medicalCaseBookmarks
     *
     * @return MedicalCases
     */
    public function addMedicalCaseBookmarks(Doctors $medicalCaseBookmarks)
    {
        $this->medicalCaseBookmarks[] = $medicalCaseBookmarks;

        return $this;
    }

    /**
     * Remove medicalCaseBookmarks.
     *
     * @param Doctors $medicalCaseBookmarks
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCaseBookmarks(Doctors $medicalCaseBookmarks)
    {
        return $this->medicalCaseBookmarks->removeElement($medicalCaseBookmarks);
    }

    /**
     * Get doctorBookmarkers.
     *
     * @return ArrayCollection|Doctors[]
     */
    public function getMedicalCaseBookmarks()
    {
        return $this->medicalCaseBookmarks;
    }
}
