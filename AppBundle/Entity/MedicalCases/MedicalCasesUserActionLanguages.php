<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class MedicalCasesUserActionLanguages
 * @package Flendoc\AppBundle\Entity\MedicalCases
 *
 * @ORM\Entity()
 * @ORM\Table(name="medical_cases_user_action_languages")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalCasesUserActionLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @ORM\Column(name="user_notification_message", type="text", nullable=true)
     */
    protected $userNotificationMessage;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserAction",
     *     inversedBy="medicalCasesUserActionLanguages",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="medical_cases_user_action_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $medicalCasesUserAction;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $languages;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUserNotificationMessage()
    {
        return $this->userNotificationMessage;
    }

    /**
     * @param mixed $userNotificationMessage
     */
    public function setUserNotificationMessage($userNotificationMessage): void
    {
        $this->userNotificationMessage = $userNotificationMessage;
    }

    /**
     * @return mixed
     */
    public function getMedicalCasesUserAction()
    {
        return $this->medicalCasesUserAction;
    }

    /**
     * @param mixed $medicalCasesUserAction
     */
    public function setMedicalCasesUserAction($medicalCasesUserAction): void
    {
        $this->medicalCasesUserAction = $medicalCasesUserAction;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages): void
    {
        $this->languages = $languages;
    }
}
