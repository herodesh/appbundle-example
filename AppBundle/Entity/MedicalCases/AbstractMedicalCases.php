<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class AbstractMedicalCases
 * @package Flendoc\AppBundle\Entity\MedicalCases
 */
class AbstractMedicalCases extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(name="slug_title", type="string", nullable=false)
     */
    protected $slugTitle;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    protected $description;

    /**
     * @ORM\Column(name="investigation_results", type="text", nullable=true)
     */
    protected $investigationResults;

    /**
     * @ORM\Column(name="sources", type="text", nullable=true)
     */
    protected $sources;

    /**
     * @ORM\Column(name="medical_case_language_iso", type="text", nullable=true)
     */
    protected $medicalCaseLanguageIso;

    /**
     * @ORM\Column(name="is_approved", type="boolean")
     */
    protected $isApproved = 0;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param null $title
     *
     * @return $this
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getSlugTitle()
    {
        return $this->slugTitle;
    }

    /**
     * @param mixed $slugTitle
     */
    public function setSlugTitle($slugTitle): void
    {
        $this->slugTitle = $slugTitle;
    }

    /**
     * Set description.
     *
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getInvestigationResults()
    {
        return $this->investigationResults;
    }

    /**
     * @param mixed $investigationResults
     */
    public function setInvestigationResults($investigationResults): void
    {
        $this->investigationResults = $investigationResults;
    }

    /**
     * @return mixed
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * @param mixed $sources
     */
    public function setSources($sources): void
    {
        $this->sources = $sources;
    }

    /**
     * @return boolean
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param boolean $isApproved
     */
    public function setIsApproved($isApproved): void
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return mixed
     */
    public function getMedicalCaseLanguageIso()
    {
        return $this->medicalCaseLanguageIso;
    }

    /**
     * @param mixed $medicalCaseLanguageIso
     */
    public function setMedicalCaseLanguageIso($medicalCaseLanguageIso): void
    {
        $this->medicalCaseLanguageIso = $medicalCaseLanguageIso;
    }
}
