<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalCases\MedicalCaseImagesRepository")
 * @ORM\Table(name="medical_case_images")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalCaseImages extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="photos", type="text", nullable=false)
     */
    protected $photos;

    /**
     * @var MedicalCases
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *     inversedBy="medicalCaseImages"
     * )
     * @ORM\JoinColumn(
     *     name="medical_case_id",
     *     referencedColumnName="id",
     *     nullable=true,
     *     onDelete="CASCADE"
     * )
     */
    protected $medicalCase;

    /**
     * @var PendingMedicalCases
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases",
     *     inversedBy="medicalCaseImages"
     * )
     * @ORM\JoinColumn(
     *     name="pending_medical_case_id",
     *     referencedColumnName="id",
     *     nullable=true,
     *     onDelete="CASCADE"
     * )
     */
    protected $pendingMedicalCase;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photos.
     *
     * @param string $photos
     *
     * @return MedicalCaseImages
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * Get photos.
     *
     * @return string
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set doctorCase.
     *
     * @param MedicalCases|null $medicalCase
     *
     * @return MedicalCaseImages
     */
    public function setMedicalCase(MedicalCases $medicalCase = null)
    {
        $this->medicalCase = $medicalCase;

        return $this;
    }

    /**
     * Get medicalCase.
     *
     * @return MedicalCases|null
     */
    public function getMedicalCase()
    {
        return $this->medicalCase;
    }

    /**
     * Set medicalCasePending.
     *
     * @param PendingMedicalCases|null $pendingMedicalCase
     *
     * @return MedicalCaseImages
     */
    public function setPendingMedicalCase(PendingMedicalCases $pendingMedicalCase = null)
    {
        $this->pendingMedicalCase = $pendingMedicalCase;

        return $this;
    }

    /**
     * Get medicalCasePending.
     *
     * @return PendingMedicalCases|null
     */
    public function getPendingMedicalCase()
    {
        return $this->pendingMedicalCase;
    }
}
