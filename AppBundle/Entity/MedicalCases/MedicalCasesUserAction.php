<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class MedicalCasesUserAction
 * @package Flendoc\AppBundle\Entity\MedicalCases
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalCases\MedicalCasesUserActionRepository")
 * @ORM\Table(name="medical_cases_user_actions")
 * @ORM\HasLifecycleCallbacks()
 */
class MedicalCasesUserAction extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserActionLanguages",
     *     mappedBy="medicalCasesUserAction",
     *     cascade={"persist"}
     * )
     */
    protected $medicalCasesUserActionLanguages;

    /**
     * RegistrationDocumentsRejection constructor.
     */
    public function __construct()
    {
        $this->medicalCasesUserActionLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMedicalCasesUserActionLanguages()
    {
        return $this->medicalCasesUserActionLanguages;
    }

    /**
     * @param mixed $medicalCasesUserActionLanguages
     */
    public function setMedicalCasesUserActionLanguages($medicalCasesUserActionLanguages): void
    {
        $this->medicalCasesUserActionLanguages = $medicalCasesUserActionLanguages;
    }

    /**
     * @param MedicalCasesUserActionLanguages $medicalCasesUserActionLanguages
     */
    public function addMedicalCasesUserActionLanguage(
        MedicalCasesUserActionLanguages $medicalCasesUserActionLanguages
    ) {
        if (!$this->medicalCasesUserActionLanguages->contains($medicalCasesUserActionLanguages)) {
            $this->medicalCasesUserActionLanguages->add($medicalCasesUserActionLanguages);
        }
    }

    /**
     * @param MedicalCasesUserActionLanguages $medicalCasesUserActionLanguages
     */
    public function removeMedicalCaseUserActionLanguage(
        MedicalCasesUserActionLanguages $medicalCasesUserActionLanguages
    ) {
        if ($this->medicalCasesUserActionLanguages->contains($medicalCasesUserActionLanguages)) {
            $this->medicalCasesUserActionLanguages->removeElement($medicalCasesUserActionLanguages);
        }
    }

    /**
     * @param Languages $oLanguages
     *
     * @return string|null
     */
    public function getDisplayName(Languages $oLanguages)
    {
        foreach ($this->getMedicalCasesUserActionLanguages() as $oMedicalCasesUserActionLanguage) {
            if ($oMedicalCasesUserActionLanguage->getLanguages()->getId() == $oLanguages->getId()) {
                return $oMedicalCasesUserActionLanguage->getDescription();
            }
        }

        //get primary language
        foreach ($this->getMedicalCasesUserActionLanguages() as $oMedicalCasesUserActionLanguage) {
            if ($oMedicalCasesUserActionLanguage->getLanguages()->getIsPrimary()) {
                return $oMedicalCasesUserActionLanguage->getDescription();
            }
        }

        return null;
    }
}
