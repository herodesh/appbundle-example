<?php

namespace Flendoc\AppBundle\Entity\MedicalCases;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Skills\Skills;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\MedicalCases\PendingMedicalCaseRepository")
 * @ORM\Table(name="pending_medical_case")
 * @ORM\HasLifecycleCallbacks()
 */
class PendingMedicalCases extends AbstractMedicalCases
{
    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="pendingMedicalCases"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills",
     *     inversedBy="pendingMedicalCases"
     * )
     * @ORM\JoinTable(
     *     name="pending_medical_case_skills",
     *     joinColumns={@ORM\JoinColumn(name="pending_medical_case_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")},
     * )
     */
    protected $medicalCaseSkills;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCaseImages",
     *     mappedBy="pendingMedicalCase"
     * )
     */
    protected $medicalCaseImages;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *      inversedBy="pendingMedicalCases"
     * )
     * @ORM\JoinColumn(
     *     name="medical_case_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $medicalCase;

    /**
     * @ORM\Column(name="is_email_notification_sent", type="boolean")
     */
    protected $isEmailNotificationSent = 0;

    /**
     * @ORM\Column(name="is_published", type="boolean")
     */
    protected $isPublished = 0;

    /**
     * @ORM\Column(name="is_saved", type="boolean")
     */
    protected $isSaved = 0;

    /**
     * DoctorCasePending constructor.
     */
    public function __construct()
    {
        $this->medicalCaseSkills = new ArrayCollection();
        $this->medicalCaseImages = new ArrayCollection();
    }

    /**
     * @return boolean
     */
    public function getIsEmailNotificationSent()
    {
        return $this->isEmailNotificationSent;
    }

    /**
     * @param boolean $isEmailNotificationSent
     */
    public function setIsEmailNotificationSent($isEmailNotificationSent): void
    {
        $this->isEmailNotificationSent = $isEmailNotificationSent;
    }

    /**
     * @return mixed
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * @return mixed
     */
    public function getIsSaved()
    {
        return $this->isSaved;
    }

    /**
     * @param mixed $isSaved
     */
    public function setIsSaved($isSaved): void
    {
        $this->isSaved = $isSaved;
    }

    /**
     * @param mixed $isPublished
     */
    public function setIsPublished($isPublished): void
    {
        $this->isPublished = $isPublished;
    }

    /**
     * Set doctor.
     *
     * @param Doctors $doctor
     *
     * @return PendingMedicalCases
     */
    public function setDoctor(Doctors $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor.
     *
     * @return Doctors
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @return mixed
     */
    public function getMedicalCaseSkills()
    {
        return $this->medicalCaseSkills;
    }

    /**
     * @param mixed $medicalCaseSkills
     */
    public function setMedicalCaseSkills($medicalCaseSkills): void
    {
        $this->medicalCaseSkills = $medicalCaseSkills;
    }

    /**
     * Add medicalCaseSkills.
     *
     * @param MedicalCaseSkills $medicalCaseSkills
     *
     * @return PendingMedicalCases
     */
    public function addMedicalCaseSkills(Skills $medicalCaseSkills)
    {
        $this->medicalCaseSkills[] = $medicalCaseSkills;

        return $this;
    }

    /**
     * Remove medicalCaseSkills.
     *
     * @param Skills $medicalCaseSkills
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCaseSkills(Skills $medicalCaseSkills)
    {
        return $this->medicalCaseSkills->removeElement($medicalCaseSkills);
    }

    /**
     * @return mixed
     */
    public function getMedicalCaseImages()
    {
        return $this->medicalCaseImages;
    }

    /**
     * @param mixed $medicalCaseImages
     */
    public function setMedicalCaseImages($medicalCaseImages): void
    {
        $this->medicalCaseImages = $medicalCaseImages;
    }

    /**
     * Add medicalCaseImages.
     *
     * @param MedicalCaseImages $medicalCaseImages
     *
     * @return $this|PendingMedicalCases
     */
    public function addMedicalCaseImages(MedicalCaseImages $medicalCaseImages)
    {
        $this->medicalCaseImages[] = $medicalCaseImages;

        return $this;
    }

    /**
     * Remove medicalCaseImages.
     *
     * @param MedicalCaseImages $medicalCaseImages
     *
     * @return bool
     */
    public function removeMedicalCaseImages(MedicalCaseImages $medicalCaseImages)
    {
        return $this->medicalCaseImages->removeElement($medicalCaseImages);
    }

    /**
     * Set doctorCase.
     *
     * @param MedicalCases|null $medicalCase
     *
     * @return $this
     */
    public function setMedicalCases(MedicalCases $medicalCase = null)
    {
        $this->medicalCase = $medicalCase;

        return $this;
    }

    /**
     * Get medicalCase.
     *
     * @return MedicalCases|null
     */
    public function getMedicalCase()
    {
        return $this->medicalCase;
    }

}
