<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ResumeEducationDiplomaTypeLanguages
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_education_diploma_type_languages")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class ResumeEducationDiplomaTypeLanguages extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypes",
     *     inversedBy="resumeEducationDiplomaTypeLanguages"
     * )
     * @ORM\JoinColumn(
     *     name="resume_education_diploma_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeEducationDiplomaType;

    /**
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getResumeEducationDiplomaType()
    {
        return $this->resumeEducationDiplomaType;
    }

    /**
     * @param mixed $resumeEducationDiplomaType
     */
    public function setResumeEducationDiplomaType($resumeEducationDiplomaType)
    {
        $this->resumeEducationDiplomaType = $resumeEducationDiplomaType;
    }

}
