<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ResumeEducationSections
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_education")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeEducation extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities"
     * )
     * @ORM\JoinColumn(
     *     name="city_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="validator.doctors.resume.error.city.not.blank")
     */
    protected $city;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="doctors.resume.error.country.not.blank")
     */
    protected $country;

    /**
     * @ORM\Column(name="institution_name", type="string")
     * @Assert\NotBlank()
     */
    protected $institutionName;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_education_diploma_type_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotNull(message="doctors.resume.error.education.diploma.not.null")
     */
    protected $resumeEducationDiplomaType;

    /**
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    protected $startDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @ORM\Column(name="is_current", type="boolean")
     */
    protected $isCurrent = 0;

    /**
     * @ORM\Column(name="study_profile", type="string")
     */
    protected $studyProfile;

    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getInstitutionName()
    {
        return $this->institutionName;
    }

    /**
     * @param mixed $institutionName
     */
    public function setInstitutionName($institutionName)
    {
        $this->institutionName = $institutionName;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getResumeEducationDiplomaType()
    {
        return $this->resumeEducationDiplomaType;
    }

    /**
     * @param mixed $resumeEducationDiplomaType
     */
    public function setResumeEducationDiplomaType($resumeEducationDiplomaType)
    {
        $this->resumeEducationDiplomaType = $resumeEducationDiplomaType;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getIsCurrent()
    {
        return (boolean)$this->isCurrent;
    }

    /**
     * @param mixed $isCurrent
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;
    }

    /**
     * @return mixed
     */
    public function getStudyProfile()
    {
        return $this->studyProfile;
    }

    /**
     * @param mixed $studyProfile
     */
    public function setStudyProfile($studyProfile)
    {
        $this->studyProfile = $studyProfile;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
