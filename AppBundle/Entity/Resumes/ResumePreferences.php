<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Entity\Jobs\JobTypes;

/**
 * Class ResumePreferences
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_preferences")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ResumePreferences extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\CareerLevels"
     * )
     * @ORM\JoinColumn(
     *     name="career_level_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $careerLevel;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\JobTypes"
     * )
     * @ORM\JoinTable(
     *     name="resume_preferences_job_types",
     *     joinColumns={@ORM\JoinColumn(name="resume_preferences_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="jobs_job_type", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $jobTypes;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills"
     * )
     * @ORM\JoinTable(
     *     name="resume_preferences_skills",
     *     joinColumns={@ORM\JoinColumn(name="resume_preferences_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")},
     * )
     */
    protected $skills;

    /**
     * @ORM\Column(name="looking_for_job", type="boolean", nullable=false)
     */
    protected $lookingForJob = 0;

    /**
     * ResumePreferences constructor.
     */
    public function __construct()
    {
        $this->jobTypes = new ArrayCollection();
        $this->skills   = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getCareerLevel()
    {
        return $this->careerLevel;
    }

    /**
     * @param mixed $careerLevel
     */
    public function setCareerLevel($careerLevel)
    {
        $this->careerLevel = $careerLevel;
    }

    /**
     * @return mixed
     */
    public function getJobTypes()
    {
        return $this->jobTypes;
    }

    /**
     * @param mixed $jobTypes
     */
    public function setJobTypes($jobTypes)
    {
        $this->jobTypes = $jobTypes;
    }

    /**
     * @param JobTypes $jobTypes
     */
    public function addJobTypes(JobTypes $jobTypes)
    {
        if (!$this->jobTypes->contains($jobTypes)) {
            $this->jobTypes->add($jobTypes);
        }
    }

    /**
     * @param JobTypes $jobTypes
     */
    public function removeJobTypes(JobTypes $jobTypes)
    {
        if ($this->jobTypes->contains($jobTypes)) {
            $this->jobTypes->remove($jobTypes);
        }
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @param Skills $skills
     */
    public function addSkills(Skills $skills)
    {
        if (!$this->skills->contains($skills)) {
            $this->skills->add($skills);
        }
    }

    /**
     * @param Skills $skills
     */
    public function removeSkills(Skills $skills)
    {
        if ($this->skills->contains($skills)) {
            $this->skills->remove($skills);
        }
    }

    /**
     * @return mixed
     */
    public function getLookingForJob()
    {
        return $this->lookingForJob;
    }

    /**
     * @param mixed $lookingForJob
     */
    public function setLookingForJob($lookingForJob)
    {
        $this->lookingForJob = $lookingForJob;
    }

}
