<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ResumeSections
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Resumes\ResumeSectionsRepository")
 * @ORM\Table(name="resume_sections")
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeSections extends AbstractEntity
{
    const RESUME_SECTION_HEADER                 = 'header';
    const RESUME_SECTION_CONTACT_DETAILS        = 'contact_details';
    const RESUME_SECTION_PROFESSIONAL_OBJECTIVE = 'professional_objective';
    const RESUME_SECTION_WORK_EXPERIENCE        = 'work_experience';
    const RESUME_SECTION_EDUCATION              = 'education';
    const RESUME_SECTION_SKILLS                 = 'skills';
    const RESUME_SECTION_FOREIGN_LANGUAGES      = 'foreign_languages';
    const RESUME_SECTION_OTHER_INFORMATION      = 'other_information';
    const RESUME_SECTION_DESIRED_JOB            = 'desired_job';
    const RESUME_SECTION_PREFERENCES            = 'preferences';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSectionLanguages",
     *     mappedBy="resumeSections",
     *     cascade={"persist"}
     * )
     */
    protected $resumeSectionLanguages;

    /**
     * @ORM\Column(name="section_name", type="string")
     */
    protected $sectionName;

    /**
     * @ORM\Column(name="order_number", type="integer", nullable=true)
     */
    protected $orderNumber;

    /**
     * @TODO do we actually need this?
     *
     * This is used to easily ignore the order. The secondary value is the value on the side of the page.
     *
     * @ORM\Column(name="is_secondary", type="boolean")
     */
    protected $isSecondary = 0;

    /**
     * ResumeSections constructor.
     */
    public function __construct()
    {
        $this->resumeSectionLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getResumeSectionLanguages()
    {
        return $this->resumeSectionLanguages;
    }

    /**
     * @param mixed $resumeSectionLanguages
     */
    public function setResumeSectionLanguages($resumeSectionLanguages)
    {
        $this->resumeSectionLanguages = $resumeSectionLanguages;
    }

    /**
     * @param ResumeSectionLanguages $resumeSectionLanguage
     */
    public function addResumeSectionLanguages(ResumeSectionLanguages $resumeSectionLanguage)
    {
        if (!$this->resumeSectionLanguages->contains($resumeSectionLanguage)) {
            $this->resumeSectionLanguages->add($resumeSectionLanguage);
        }
    }

    /**
     * @param ResumeSectionLanguages $resumeSectionLanguage
     */
    public function removeResumeSectionLanguages(ResumeSectionLanguages $resumeSectionLanguage)
    {
        if ($this->resumeSectionLanguages->contains($resumeSectionLanguage)) {
            $this->resumeSectionLanguages->removeElement($resumeSectionLanguage);
        }
    }

    /**
     * @return mixed
     */
    public function getSectionName()
    {
        return $this->sectionName;
    }

    /**
     * @param mixed $sectionName
     */
    public function setSectionName($sectionName)
    {
        $this->sectionName = $sectionName;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getIsSecondary()
    {
        return $this->isSecondary;
    }

    /**
     * @param mixed $isSecondary
     */
    public function setIsSecondary($isSecondary)
    {
        $this->isSecondary = $isSecondary;
    }

}
