<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class OtherInformation
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_other_information")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeOtherInformation extends AbstractEntity
{
    const RESUME_OTHER_INFO_DRIVER_LICENSE          = 'driverLicense';
    const RESUME_OTHER_INFO_CERTIFICATION           = 'certification';
    const RESUME_OTHER_INFO_PRIZES_AND_DISTINCTIONS = 'prizesAndDistinctions';
    const RESUME_OTHER_INFO_PROJECTS                = 'projects';
    const RESUME_OTHER_INFO_VOLUNTEERING            = 'volunteering';
    const RESUME_OTHER_INFO_CONFERENCES             = 'conferences';
    const RESUME_OTHER_INFO_PUBLISHED_WORKS         = 'publishedWorks';
    const RESUME_OTHER_INFO_TRAININGS               = 'trainings';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    protected $startDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    protected $endDate; //can be null

    /**
     * @ORM\Column(name="other_information_type", type="string", nullable=false)
     */
    protected $otherInformationType;

    /**
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getOtherInformationType()
    {
        return $this->otherInformationType;
    }

    /**
     * @param mixed $otherInformationType
     */
    public function setOtherInformationType($otherInformationType)
    {
        $this->otherInformationType = $otherInformationType;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
