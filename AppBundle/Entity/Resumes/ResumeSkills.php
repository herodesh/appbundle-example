<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class ResumeSkills
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Entity()
 * @ORM\Table(name="resume_skills")
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeSkills extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills"
     * )
     * @ORM\JoinColumn(
     *     name="resume_skills_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $skills;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills",
     *     cascade={"remove"}
     * )
     * @ORM\JoinTable(
     *     name="resume_skills_list",
     *     joinColumns={@ORM\JoinColumn(name="resume_skills_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $resumeSkillsList;

    /**
     * ResumeSkills constructor.
     */
    public function __construct()
    {
        $this->resumeSkillsList = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return mixed
     */
    public function getResumeSkillsList()
    {
        return $this->resumeSkillsList;
    }

    /**
     * @param mixed $resumeSkillsList
     */
    public function setResumeSkillsList($resumeSkillsList)
    {
        $this->resumeSkillsList = $resumeSkillsList;
    }

    /**
     * @param ResumeSkills $resumeSkills
     */
    public function addResumeSkills(ResumeSkills $resumeSkills): void
    {
        if ($this->resumeSkillsList->contains($resumeSkills)) {
            return;
        }

        $this->resumeSkillsList->add($resumeSkills);
        $resumeSkills->addResumeSkills($this);
    }

    /**
     * @param ResumeSkills $resumeSkills
     */
    public function removeResumeSkills(ResumeSkills $resumeSkills): void
    {
        if (!$this->resumeSkillsList->contains($resumeSkills)) {
            return;
        }

        $this->resumeSkillsList->removeElement($resumeSkills);
        $resumeSkills->removeResumeSkills($this);
    }
}
