<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Jobs\JobTypes;

/**
 * Class Resumes
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Resumes\ResumesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="resumes")
 */
class Resumes extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Languages\Languages"
     * )
     * @ORM\JoinColumn(
     *     name="language_id",
     *     referencedColumnName="id"
     * )
     */
    protected $languages;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    protected $shortDescription;

    /**
     * @ORM\Column(name="contact_email", type="string", nullable=true)
     */
    protected $contactEmail;

    /**
     * @ORM\Column(name="contact_phone", type="string", nullable=true)
     */
    protected $contactPhone;

    /**
     * @ORM\Column(name="salary", type="string", nullable=true)
     */
    protected $salary;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Jobs\JobTypes",
     *     inversedBy="resumes"
     * )
     * @ORM\JoinTable(
     *     name="resume_job_type",
     *     joinColumns={@ORM\JoinColumn(name="resume_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="job_type_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $jobType;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills",
     *     inversedBy="resumes"
     * )
     * @ORM\JoinTable(
     *     name="resume_departments",
     *     joinColumns={@ORM\JoinColumn(name="resume_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skills_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $departments;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities",
     *     inversedBy="resumes"
     * )
     * @ORM\JoinTable(
     *     name="resume_wanted_cities",
     *     joinColumns={@ORM\JoinColumn(name="resume_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $wantedCities;

    /**
     * @ORM\Column(name="driver_licence", type="boolean")
     */
    protected $driverLicence = false;

    /**
     * @ORM\Column(name="travel_availability", type="integer", length=2)
     */
    protected $travelAvailability = 0;

    /**
     * @ORM\Column(name="is_primary", type="boolean")
     */
    protected $isPrimary = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return mixed
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param mixed $contactEmail
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param mixed $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    /**
     * @param mixed $jobType
     */
    public function setJobType($jobType)
    {
        $this->jobType = $jobType;
    }

    /**
     * @param JobTypes $oJobType
     */
    public function addJobType(JobTypes $oJobType)
    {
        if ($this->jobType->contains($oJobType)) {
            return;
        }

        $this->jobType->add($oJobType);
        $oJobType->addResume($this);
    }

    /**
     * @param JobTypes $oJobType
     */
    public function removeJobType(JobTypes $oJobType)
    {
        if (!$this->jobType->contains($oJobType)) {
            return;
        }

        $this->jobType->removeElement($oJobType);
        $oJobType->removeResume($this);
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param mixed $departments
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
    }

    /**
     * @param Skills $oSkill
     */
    public function addDepartment(Skills $oSkill)
    {
        if ($this->departments->contains($oSkill)) {
            return;
        }

        $this->departments->add($oSkill);
        $oSkill->addResume($this);
    }

    /**
     * @param Skills $oSkill
     */
    public function removeDepartment(Skills $oSkill)
    {
        if (!$this->departments->contains($oSkill)) {
            return;
        }

        $this->departments->removeElement($oSkill);
        $oSkill->removeResume($this);
    }

    /**
     * @return mixed
     */
    public function getWantedCities()
    {
        return $this->wantedCities;
    }

    /**
     * @param mixed $wantedCities
     */
    public function setWantedCities($wantedCities)
    {
        $this->wantedCities = $wantedCities;
    }

    /**
     * @param Cities $oCity
     */
    public function addWantedCity(Cities $oCity)
    {
        if ($this->wantedCities->contains($oCity)) {
            return;
        }

        $this->wantedCities->add($oCity);
        $oCity->addResume($this);
    }

    /**
     * @param Cities $oCity
     */
    public function removeWantedCity(Cities $oCity)
    {
        if (!$this->wantedCities->contain($oCity)) {
            return;
        }

        $this->wantedCities->removeElement($oCity);
        $oCity->removeResume($this);
    }

    /**
     * @return mixed
     */
    public function getDriverLicence()
    {
        return $this->driverLicence;
    }

    /**
     * @param mixed $driverLicence
     */
    public function setDriverLicence($driverLicence)
    {
        $this->driverLicence = $driverLicence;
    }

    /**
     * @return mixed
     */
    public function getTravelAvailability()
    {
        return $this->travelAvailability;
    }

    /**
     * @param mixed $travelAvailability
     */
    public function setTravelAvailability($travelAvailability)
    {
        $this->travelAvailability = $travelAvailability;
    }

    /**
     * @return mixed
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param mixed $isPrimary
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary;
    }

}
