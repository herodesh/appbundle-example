<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ResumeHeader
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_header")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeHeader extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\Column(name="avatar", type="text", nullable=true)
     */
    protected $avatar;

    /**
     * @ORM\Column(name="is_avatar_sync", type="boolean")
     */
    protected $isAvatarSync = 0;

    /**
     * @ORM\Column(name="first_name", type="string", nullable=true)
     * @Assert\NotBlank(message="doctors.resume.error.first.name.not.blank")
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", nullable=true)
     * @Assert\NotBlank(message="doctors.resume.error.last.name.not.blank")
     */
    protected $lastName;

    /**
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    protected $birthDate;

    /**
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     */
    protected $gender;

    /**
     * @ORM\Column(name="hide_current_job", type="boolean", nullable=true)
     */
    protected $hideCurrentJob = 0;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Institutions\Institutions"
     * )
     * @ORM\JoinColumn(
     *     name="institution_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $currentInstitution;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities"
     *     )
     * @ORM\JoinColumn(
     *     name="city_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="validator.doctors.resume.error.city.not.blank")
     */
    protected $city;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="doctors.resume.error.country.not.blank")
     */
    protected $country;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getIsAvatarSync()
    {
        return $this->isAvatarSync;
    }

    /**
     * @param mixed $isAvatarSync
     */
    public function setIsAvatarSync($isAvatarSync)
    {
        $this->isAvatarSync = $isAvatarSync;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getHideCurrentJob()
    {
        return (boolean)$this->hideCurrentJob;
    }

    /**
     * @param mixed $hideCurrentJob
     */
    public function setHideCurrentJob($hideCurrentJob)
    {
        $this->hideCurrentJob = $hideCurrentJob;
    }

    /**
     * @return mixed
     */
    public function getCurrentInstitution()
    {
        return $this->currentInstitution;
    }

    /**
     * @param mixed $currentInstitution
     */
    public function setCurrentInstitution($currentInstitution)
    {
        $this->currentInstitution = $currentInstitution;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}
