<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * Class ResumeContactDetails
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_contact_details")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeContactDetails extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\Column(name="email_address", type="string", nullable=true)
     */
    protected $emailAddress;

    /**
     * @ORM\Column(name="contact_phone", type="string", nullable=true)
     */
    protected $contactPhone;

    /**
     * @ORM\Column(name="contact_address", type="string", nullable=true)
     */
    protected $contactAddress;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param mixed $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param mixed $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return mixed
     */
    public function getContactAddress()
    {
        return $this->contactAddress;
    }

    /**
     * @param mixed $contactAddress
     */
    public function setContactAddress($contactAddress)
    {
        $this->contactAddress = $contactAddress;
    }

}
