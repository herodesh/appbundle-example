<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ResumeEducationDiplomaTypes
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_education_diploma_type")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Resumes\ResumeEducationDiplomaTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResumeEducationDiplomaTypes extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypeLanguages",
     *     mappedBy="resumeEducationDiplomaType",
     *     cascade={"persist"}
     * )
     */
    protected $resumeEducationDiplomaTypeLanguages;

    /**
     * ResumeEducationDiplomaTypes constructor.
     */
    public function __construct()
    {
        $this->resumeEducationDiplomaTypeLanguages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getResumeEducationDiplomaTypeLanguages()
    {
        return $this->resumeEducationDiplomaTypeLanguages;
    }

    /**
     * @param mixed $resumeEducationDiplomaTypeLanguages
     */
    public function setResumeEducationDiplomaTypeLanguages($resumeEducationDiplomaTypeLanguages)
    {
        $this->resumeEducationDiplomaTypeLanguages = $resumeEducationDiplomaTypeLanguages;
    }

    /**
     * @param ResumeEducationDiplomaTypeLanguages $resumeEducationDiplomaTypeLanguage
     */
    public function addResumeEducationDiplomaTypeLanguage(
        ResumeEducationDiplomaTypeLanguages $resumeEducationDiplomaTypeLanguage
    ) {
        if (!$this->resumeEducationDiplomaTypeLanguages->contains($resumeEducationDiplomaTypeLanguage)) {
            $this->resumeEducationDiplomaTypeLanguages->add($resumeEducationDiplomaTypeLanguage);
        }
    }

    /**
     * @param ResumeEducationDiplomaTypeLanguages $resumeEducationDiplomaTypeLanguage
     */
    public function removeResumeEducationDiplomaTypeLanguage(
        ResumeEducationDiplomaTypeLanguages $resumeEducationDiplomaTypeLanguage
    ) {
        if ($this->resumeEducationDiplomaTypeLanguages->contains($resumeEducationDiplomaTypeLanguage)) {
            $this->resumeEducationDiplomaTypeLanguages->removeElement($resumeEducationDiplomaTypeLanguage);
        }
    }

    /**
     * @param $languages
     *
     * @return mixed
     */
    public function getDisplayName($languages)
    {
        $languagesValue = ($languages instanceof Languages) ? $languages->getIso() : $languages;

        foreach ($this->getResumeEducationDiplomaTypeLanguages() as $oResumeEducationDiplomaTypeLanguage) {

            foreach ($oResumeEducationDiplomaTypeLanguage->getLanguages()
                                                         ->getTranslatedLanguages() as $oTranslatedLanguage) {
                if ($oTranslatedLanguage->getLanguage()->getIso() === $languagesValue &&
                    $oResumeEducationDiplomaTypeLanguage->getLanguages()->getIso() === $languagesValue
                ) {
                    return $oResumeEducationDiplomaTypeLanguage->getName();
                }
            }
        }

        //get primary language
        foreach ($this->getResumeEducationDiplomaTypeLanguages() as $oResumeEducationDiplomaTypeLanguage) {
            if ($oResumeEducationDiplomaTypeLanguage->getLanguages()->getIsPrimary()) {
                return $oResumeEducationDiplomaTypeLanguage->getName();
            }
        }
    }
}
