<?php

namespace Flendoc\AppBundle\Entity\Resumes;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ResumeWorkExperience
 * @package Flendoc\AppBundle\Entity\Resumes
 *
 * @ORM\Table(name="resume_work_experience")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Resumes\ResumeWorkExperienceRepository")
 */
class ResumeWorkExperience extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors"
     * )
     * @ORM\JoinColumn(
     *     name="doctor_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $doctor;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes"
     * )
     * @ORM\JoinColumn(
     *     name="resume_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resume;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\ResumeSections"
     * )
     * @ORM\JoinColumn(
     *     name="resume_section_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $resumeSection;

    /**
     * @ORM\Column(name="institution_name", type="string")
     * @Assert\NotBlank()
     */
    protected $institutionName;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Skills\Skills"
     * )
     * @ORM\JoinTable(
     *     name="resume_work_experience_skills",
     *     joinColumns={@ORM\JoinColumn(name="resume_work_experience_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $skills;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Cities\Cities"
     * )
     * @ORM\JoinColumn(
     *     name="city_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="validator.doctors.resume.error.city.not.blank")
     */
    protected $city;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Countries\Countries"
     * )
     * @ORM\JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @Assert\NotBlank(message="doctors.resume.error.country.not.blank")
     */
    protected $country;

    /**
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     * @Assert\NotNull(message="doctors.resume.error.start.date.not.blank")
     */
    protected $startDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @ORM\Column(name="is_current", type="boolean")
     */
    protected $isCurrent = 0;

    /**
     * @ORM\Column(name="job_title", type="string")
     * @Assert\NotBlank(message="doctors.resume.error.job.title.not.blank")
     */
    protected $jobTitle;

    /**
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * ResumeWorkExperience constructor.
     */
    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return mixed
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @param mixed $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return mixed
     */
    public function getResumeSection()
    {
        return $this->resumeSection;
    }

    /**
     * @param mixed $resumeSection
     */
    public function setResumeSection($resumeSection)
    {
        $this->resumeSection = $resumeSection;
    }

    /**
     * @return mixed
     */
    public function getInstitutionName()
    {
        return $this->institutionName;
    }

    /**
     * @param mixed $institutionName
     */
    public function setInstitutionName($institutionName)
    {
        $this->institutionName = $institutionName;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @param Skills $skills
     */
    public function addSkill(Skills $skills)
    {
        if (!$this->skills->contains($skills)) {
            $this->skills->add($skills);
        }
    }

    /**
     * @param Skills $skills
     */
    public function removeSkill(Skills $skills)
    {
        if ($this->skills->contains($skills)) {
            $this->skills->remove($skills);
        }
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getIsCurrent()
    {
        return (boolean)$this->isCurrent;
    }

    /**
     * @param mixed $isCurrent
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;
    }

    /**
     * @return mixed
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * @param mixed $jobTitle
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
