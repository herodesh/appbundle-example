<?php

namespace Flendoc\AppBundle\Entity\Chats;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Chats\DoctorChatRepository")
 * @ORM\Table(name="doctor_chat")
 * @ORM\HasLifecycleCallbacks()
 */
class DoctorChat extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group", type="boolean", nullable=false)
     */
    protected $isGroup = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_message_at", type="datetime", nullable=true)
     */
    protected $lastMessageAt;

    /**
     * @var ArrayCollection|DoctorChatMember[]
     *
     * @ORM\OneToMany(
     *     targetEntity="DoctorChatMember",
     *     mappedBy="doctorChat",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $doctorChatMembers;

    /**
     * @var ArrayCollection|DoctorChatFile[]
     *
     * @ORM\OneToMany(
     *     targetEntity="DoctorChatFile",
     *     mappedBy="doctorChat",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $doctorChatFiles;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::setCreatedAt();
        $this->doctorChatMembers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return DoctorChat
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isGroup.
     *
     * @param bool $isGroup
     *
     * @return DoctorChat
     */
    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;

        return $this;
    }

    /**
     * Get isGroup.
     *
     * @return bool
     */
    public function getIsGroup()
    {
        return $this->isGroup;
    }

    /**
     * Add doctorChatMember.
     *
     * @param DoctorChatMember $doctorChatMember
     *
     * @return DoctorChat
     */
    public function addDoctorChatMember(DoctorChatMember $doctorChatMember)
    {
        $this->doctorChatMembers[] = $doctorChatMember;

        return $this;
    }

    /**
     * Remove doctorChatMember.
     *
     * @param DoctorChatMember $doctorChatMember
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorChatMember(DoctorChatMember $doctorChatMember)
    {
        return $this->doctorChatMembers->removeElement($doctorChatMember);
    }

    /**
     * Get doctorChatMembers.
     *
     * @return ArrayCollection|DoctorChatMember[]
     */
    public function getDoctorChatMembers()
    {
        return $this->doctorChatMembers;
    }

    /**
     * Set lastMessageAt.
     *
     * @param \DateTime|null $lastMessageAt
     *
     * @return DoctorChat
     */
    public function setLastMessageAt(\DateTime $lastMessageAt = null)
    {
        $this->lastMessageAt = $lastMessageAt;

        return $this;
    }

    /**
     * Get lastMessageAt.
     *
     * @return \DateTime|null
     */
    public function getLastMessageAt()
    {
        return $this->lastMessageAt;
    }

    /**
     * Add doctorChatFile.
     *
     * @param DoctorChatFile $doctorChatFile
     *
     * @return DoctorChat
     */
    public function addDoctorChatFile(DoctorChatFile $doctorChatFile)
    {
        $this->doctorChatFiles[] = $doctorChatFile;

        return $this;
    }

    /**
     * Remove doctorChatFile.
     *
     * @param DoctorChatFile $doctorChatFile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorChatFile(DoctorChatFile $doctorChatFile)
    {
        return $this->doctorChatFiles->removeElement($doctorChatFile);
    }

    /**
     * Get doctorChatFiles.
     *
     * @return ArrayCollection|DoctorChatFile[]
     */
    public function getDoctorChatFiles()
    {
        return $this->doctorChatFiles;
    }
}
