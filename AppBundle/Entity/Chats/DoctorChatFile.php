<?php

namespace Flendoc\AppBundle\Entity\Chats;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Chats\DoctorChatFileRepository")
 * @ORM\Table(name="doctor_chat_file")
 * @ORM\HasLifecycleCallbacks()
 */
class DoctorChatFile extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="original_file_name", type="string", nullable=false)
     */
    protected $originalFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="message_id", type="string", nullable=true)
     */
    protected $messageId;

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="doctorChatFiles"
     * )
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     */
    protected $doctorUploader;

    /**
     * @var DoctorChat
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Chats\DoctorChat",
     *     inversedBy="doctorChatFiles"
     * )
     */
    protected $doctorChat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unprocessed_upload", type="boolean", nullable=false)
     */
    protected $unprocessedUpload = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::setCreatedAt();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return DoctorChatFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set unprocessedUpload.
     *
     * @param bool $unprocessedUpload
     *
     * @return DoctorChatFile
     */
    public function setUnprocessedUpload($unprocessedUpload)
    {
        $this->unprocessedUpload = $unprocessedUpload;

        return $this;
    }

    /**
     * Get unprocessedUpload.
     *
     * @return bool
     */
    public function getUnprocessedUpload()
    {
        return $this->unprocessedUpload;
    }

    /**
     * Set doctorUploader.
     *
     * @param Doctors $doctorUploader
     *
     * @return DoctorChatFile
     */
    public function setDoctorUploader(Doctors $doctorUploader)
    {
        $this->doctorUploader = $doctorUploader;

        return $this;
    }

    /**
     * Get doctorUploader.
     *
     * @return Doctors
     */
    public function getDoctorUploader()
    {
        return $this->doctorUploader;
    }

    /**
     * Set doctorChat.
     *
     * @param DoctorChat|null $doctorChat
     *
     * @return DoctorChatFile
     */
    public function setDoctorChat(DoctorChat $doctorChat = null)
    {
        $this->doctorChat = $doctorChat;

        return $this;
    }

    /**
     * Get doctorChat.
     *
     * @return DoctorChat|null
     */
    public function getDoctorChat()
    {
        return $this->doctorChat;
    }

    /**
     * Set originalFileName.
     *
     * @param string $originalFileName
     *
     * @return DoctorChatFile
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * Get originalFileName.
     *
     * @return string
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * Set messageId.
     *
     * @param string|null $messageId
     *
     * @return DoctorChatFile
     */
    public function setMessageId($messageId = null)
    {
        $this->messageId = $messageId;

        return $this;
    }

    /**
     * Get messageId.
     *
     * @return string|null
     */
    public function getMessageId()
    {
        return $this->messageId;
    }
}
