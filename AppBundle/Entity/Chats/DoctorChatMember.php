<?php

namespace Flendoc\AppBundle\Entity\Chats;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Chats\DoctorChatMemberRepository")
 * @ORM\Table(name="doctor_chat_member")
 * @ORM\HasLifecycleCallbacks()
 */
class DoctorChatMember extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Doctors
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\Doctors",
     *     inversedBy="doctorChatMembers"
     * )
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     */
    protected $doctor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted_chat", type="boolean", nullable=false)
     */
    protected $deletedChat = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archived_chat", type="boolean", nullable=false)
     */
    protected $archivedChat = false;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="archived_chat_at", type="datetime", nullable=true)
     */
    protected $archivedChatAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="left_chat", type="boolean", nullable=false)
     */
    protected $leftChat = false;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="left_chat_at", type="datetime", nullable=true)
     */
    protected $leftChatAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="soft_delete_at", type="datetime", nullable=true)
     */
    protected $softDeleteAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group_admin", type="boolean", nullable=false)
     */
    protected $isGroupAdmin = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="email_notification_sent", type="boolean", nullable=true)
     */
    protected $emailNotificationSent;

    /**
     * @var DoctorChat
     *
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Chats\DoctorChat",
     *     inversedBy="doctorChatMembers"
     * )
     * @ORM\JoinColumn(name="doctor_chat_id", referencedColumnName="id", nullable=false)
     */
    protected $doctorChat;

    /**
     * @var string
     *
     * @ORM\Column(name="archived_chat_name", type="string", nullable=true)
     */
    protected $archivedChatName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_active_at", type="datetime", nullable=true)
     */
    protected $lastActiveAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="doctor_account_deleted", type="boolean", nullable=true)
     */
    protected $doctorAccountDeleted = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="peer_deleted_account", type="boolean", nullable=true)
     */
    protected $peerDeletedAccount = false;

    /**
     * @var string
     *
     * @ORM\Column(name="previous_doctor_identifier", type="string", nullable=true)
     */
    protected $previousDoctorIdentifier;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::setCreatedAt();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set leftChat.
     *
     * @param bool $leftChat
     *
     * @return DoctorChatMember
     */
    public function setLeftChat($leftChat)
    {
        $this->leftChat = $leftChat;

        return $this;
    }

    /**
     * Get leftChat.
     *
     * @return bool
     */
    public function getLeftChat()
    {
        return $this->leftChat;
    }

    /**
     * Set leftChatAt.
     *
     * @param \DateTime|null $leftChatAt
     *
     * @return DoctorChatMember
     */
    public function setLeftChatAt(\DateTime $leftChatAt = null)
    {
        $this->leftChatAt = $leftChatAt;

        return $this;
    }

    /**
     * Get leftChatAt.
     *
     * @return \DateTime|null
     */
    public function getLeftChatAt()
    {
        return $this->leftChatAt;
    }

    /**
     * Set softDeleteAt.
     *
     * @param \DateTime|null $softDeleteAt
     *
     * @return DoctorChatMember
     */
    public function setSoftDeleteAt(\DateTime $softDeleteAt = null)
    {
        $this->softDeleteAt = $softDeleteAt;

        return $this;
    }

    /**
     * Get softDeleteAt.
     *
     * @return \DateTime|null
     */
    public function getSoftDeleteAt()
    {
        return $this->softDeleteAt;
    }

    /**
     * Set isGroupAdmin.
     *
     * @param bool $isGroupAdmin
     *
     * @return DoctorChatMember
     */
    public function setIsGroupAdmin($isGroupAdmin)
    {
        $this->isGroupAdmin = $isGroupAdmin;

        return $this;
    }

    /**
     * Get isGroupAdmin.
     *
     * @return bool
     */
    public function getIsGroupAdmin()
    {
        return $this->isGroupAdmin;
    }

    /**
     * Set doctor.
     *
     * @param Doctors $doctor
     *
     * @return DoctorChatMember
     */
    public function setDoctor(Doctors $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor.
     *
     * @return Doctors
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set doctorChat.
     *
     * @param DoctorChat $doctorChat
     *
     * @return DoctorChatMember
     */
    public function setDoctorChat(DoctorChat $doctorChat)
    {
        $this->doctorChat = $doctorChat;

        return $this;
    }

    /**
     * Get doctorChat.
     *
     * @return DoctorChat
     */
    public function getDoctorChat()
    {
        return $this->doctorChat;
    }

    /**
     * Set archivedChatName.
     *
     * @param string|null $archivedChatName
     *
     * @return DoctorChatMember
     */
    public function setArchivedChatName($archivedChatName = null)
    {
        $this->archivedChatName = $archivedChatName;

        return $this;
    }

    /**
     * Get archivedChatName.
     *
     * @return string|null
     */
    public function getArchivedChatName()
    {
        return $this->archivedChatName;
    }

    /**
     * Set lastActiveAt.
     *
     * @param \DateTime|null $lastActiveAt
     *
     * @return DoctorChatMember
     */
    public function setLastActiveAt($lastActiveAt = null)
    {
        $this->lastActiveAt = $lastActiveAt;

        return $this;
    }

    /**
     * Get lastActiveAt.
     *
     * @return \DateTime|null
     */
    public function getLastActiveAt()
    {
        return $this->lastActiveAt;
    }

    /**
     * Set deletedChat.
     *
     * @param bool $deletedChat
     *
     * @return DoctorChatMember
     */
    public function setDeletedChat($deletedChat)
    {
        $this->deletedChat = $deletedChat;

        return $this;
    }

    /**
     * Get deletedChat.
     *
     * @return bool
     */
    public function getDeletedChat()
    {
        return $this->deletedChat;
    }

    /**
     * Set archivedChat.
     *
     * @param bool $archivedChat
     *
     * @return DoctorChatMember
     */
    public function setArchivedChat($archivedChat)
    {
        $this->archivedChat = $archivedChat;

        return $this;
    }

    /**
     * Get archivedChat.
     *
     * @return bool
     */
    public function getArchivedChat()
    {
        return $this->archivedChat;
    }

    /**
     * Set archivedChatAt.
     *
     * @param \DateTime|null $archivedChatAt
     *
     * @return DoctorChatMember
     */
    public function setArchivedChatAt($archivedChatAt = null)
    {
        $this->archivedChatAt = $archivedChatAt;

        return $this;
    }

    /**
     * Get archivedChatAt.
     *
     * @return \DateTime|null
     */
    public function getArchivedChatAt()
    {
        return $this->archivedChatAt;
    }

    /**
     * Set emailNotificationSent.
     *
     * @param bool|null $emailNotificationSent
     *
     * @return DoctorChatMember
     */
    public function setEmailNotificationSent($emailNotificationSent = null)
    {
        $this->emailNotificationSent = $emailNotificationSent;

        return $this;
    }

    /**
     * Get emailNotificationSent.
     *
     * @return bool|null
     */
    public function getEmailNotificationSent()
    {
        return $this->emailNotificationSent;
    }

    /**
     * Set doctorAccountDeleted.
     *
     * @param bool $doctorAccountDeleted
     *
     * @return DoctorChatMember
     */
    public function setDoctorAccountDeleted($doctorAccountDeleted)
    {
        $this->doctorAccountDeleted = $doctorAccountDeleted;

        return $this;
    }

    /**
     * Get doctorAccountDeleted.
     *
     * @return bool|null
     */
    public function getDoctorAccountDeleted()
    {
        return $this->doctorAccountDeleted;
    }

    /**
     * Set previousDoctorIdentifier.
     *
     * @param string $previousDoctorIdentifier
     *
     * @return DoctorChatMember
     */
    public function setPreviousDoctorIdentifier($previousDoctorIdentifier)
    {
        $this->previousDoctorIdentifier = $previousDoctorIdentifier;

        return $this;
    }

    /**
     * Get previousDoctorIdentifier.
     *
     * @return string|null
     */
    public function getPreviousDoctorIdentifier()
    {
        return $this->previousDoctorIdentifier;
    }

    /**
     * Set peerDeletedAccount.
     *
     * @param bool $peerDeletedAccount
     *
     * @return DoctorChatMember
     */
    public function setPeerDeletedAccount($peerDeletedAccount)
    {
        $this->peerDeletedAccount = $peerDeletedAccount;

        return $this;
    }

    /**
     * Get peerDeletedAccount.
     *
     * @return bool|null
     */
    public function getPeerDeletedAccount()
    {
        return $this->peerDeletedAccount;
    }
}
