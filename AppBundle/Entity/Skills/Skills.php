<?php

namespace Flendoc\AppBundle\Entity\Skills;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases;
use Flendoc\AppBundle\Entity\Doctors\DoctorProfile;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Resumes\Resumes;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Skills\SkillsRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="skills")
 */
class Skills extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Flendoc\AppBundle\Entity\Skills\SkillLanguages",
     *      mappedBy="skills",
     *      cascade={"all"}
     * )
     * @Assert\Valid()
     */
    protected $skillLanguages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Resumes\Resumes",
     *     mappedBy="departments",
     *     cascade={"persist"}
     * )
     */
    protected $resumes;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Doctors\DoctorProfile",
     *     mappedBy="doctorSkills",
     *     cascade={"persist"}
     * )
     */
    protected $doctorProfile;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\MedicalCases",
     *     mappedBy="medicalCaseSkills",
     *     cascade={"persist"}
     * )
     */
    protected $medicalCases;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases",
     *     mappedBy="medicalCaseSkills",
     *     cascade={"persist"}
     * )
     */
    protected $pendingMedicalCases;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->skillLanguages      = new ArrayCollection();
        $this->medicalCases        = new ArrayCollection();
        $this->pendingMedicalCases = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @param skillLanguages $skillLanguages
     */
    public function addSkillLanguage(SkillLanguages $skillLanguages)
    {
        if (!$this->skillLanguages->contains($skillLanguages)) {
            $this->skillLanguages->add($skillLanguages);
        }
    }

    /**
     * @param SkillLanguages $skillLanguages
     *
     * @return mixed|null
     */
    public function removeSkillLanguage(SkillLanguages $skillLanguages)
    {
        return $this->skillLanguages->remove($skillLanguages);
    }

    /**
     * @return mixed
     */
    public function getSkillLanguages()
    {
        return $this->skillLanguages;
    }

    /**
     * @param mixed $skillLanguages
     */
    public function setSkillLanguages($skillLanguages): void
    {
        $this->skillLanguages = $skillLanguages;
    }

    /**
     * @return mixed
     */
    public function getResumes()
    {
        return $this->resumes;
    }

    /**
     * @param mixed $resumes
     */
    public function setResumes($resumes): void
    {
        $this->resumes = $resumes;
    }

    /**
     * @param Resumes $oResume
     */
    public function addResumes(Resumes $oResume)
    {
        if ($this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->add($oResume);
        $oResume->addDepartment($this);
    }

    /**
     * @param Resumes $oResume
     */
    public function removeResumes(Resumes $oResume)
    {
        if (!$this->resumes->contains($oResume)) {
            return;
        }

        $this->resumes->removeElement($oResume);
        $oResume->removeDepartment($this);
    }

    /**
     * @return mixed
     */
    public function getDoctorProfile()
    {
        return $this->doctorProfile;
    }

    /**
     * @param $doctorProfile
     */
    public function setDoctorProfile($doctorProfile)
    {
        $this->doctorProfile = $doctorProfile;
    }

    /**
     * @param DoctorProfile $oDoctorProfile
     */
    public function addDoctorProfile(DoctorProfile $oDoctorProfile)
    {
        if ($this->doctorProfile->contains($oDoctorProfile)) {
            return;
        }

        $this->doctorProfile->add($oDoctorProfile);
        $oDoctorProfile->addDoctorSkill($this);
    }

    /**
     * @param DoctorProfile $oDoctorProfile
     */
    public function removeDoctorProfile(DoctorProfile $oDoctorProfile)
    {
        if (!$this->doctorProfile->contains($oDoctorProfile)) {
            return;
        }

        $this->doctorProfile->removeElement($oDoctorProfile);
        $oDoctorProfile->removeDoctorSkill($this);
    }

    /**
     * Display default skill name based on the application default language
     *
     * @return mixed
     */
    public function getDisplayDefaultSkillName()
    {
        foreach ($this->getSkillLanguages() as $oSkillLanguage) {
            if ($oSkillLanguage->getLanguages()->getIsPrimary()) {
                return $oSkillLanguage->getName();
            }
        }

        return $this->getSkillLanguages()->first()->getName();
    }

    /**
     * @param $languages
     *
     * @return mixed
     */
    public function getDisplayName($languages)
    {
        $languagesValue = ($languages instanceof Languages) ? $languages->getIso() : $languages;

        foreach ($this->getSkillLanguages() as $oSkillLanguage) {
            foreach ($oSkillLanguage->getLanguages()->getTranslatedLanguages() as $oTranslatedLanguage) {
                if ($oTranslatedLanguage->getLanguage()->getIso() === $languagesValue &&
                    $oSkillLanguage->getLanguages()->getIso() === $languagesValue
                ) {
                    return $oSkillLanguage->getName();
                }
            }
        }

        //get primary language
        foreach ($this->getSkillLanguages() as $oSkillLanguage) {
            if ($oSkillLanguage->getLanguages()->getIsPrimary()) {
                return $oSkillLanguage->getName();
            }
        }
    }

    /**
     * Add doctorCase.
     *
     * @param MedicalCases $medicalCases
     *
     * @return Skills
     */
    public function addMedicalCases(MedicalCases $medicalCases)
    {
        $this->medicalCases[] = $medicalCases;

        return $this;
    }

    /**
     * Remove doctorCase.
     *
     * @param MedicalCases $medicalCases
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedicalCases(MedicalCases $medicalCases)
    {
        return $this->medicalCases->removeElement($medicalCases);
    }

    /**
     * Get medicalCases.
     *
     * @return ArrayCollection|MedicalCases[]
     */
    public function getMedicalCases()
    {
        return $this->medicalCases;
    }

    /**
     * Add pendingMedicalCases.
     *
     * @param PendingMedicalCases $pendingMedicalCases
     *
     * @return Skills
     */
    public function addPendingMedicalCases(PendingMedicalCases $pendingMedicalCases)
    {
        $this->pendingMedicalCases[] = $pendingMedicalCases;

        return $this;
    }

    /**
     * Remove pendingMedicalCases.
     *
     * @param PendingMedicalCases $pendingMedicalCases
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePendingMedicalCases(PendingMedicalCases $pendingMedicalCases)
    {
        return $this->pendingMedicalCases->removeElement($pendingMedicalCases);
    }

    /**
     * Get pendingMedicalCases.
     *
     * @return ArrayCollection|PendingMedicalCases[]
     */
    public function getPendingMedicalCases()
    {
        return $this->pendingMedicalCases;
    }
}
