<?php

namespace Flendoc\AppBundle\Entity\Admins;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\Users\AbstractUser;
use Flendoc\AppBundle\Entity\Users\UserRoles;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="admins")
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Admins\AdminRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("username")
 */
class Admins extends AbstractUser
{
    /**
     * @ORM\ManyToMany(
     *      targetEntity="Flendoc\AppBundle\Entity\Users\UserRoles",
     *     inversedBy="admins"
     * )
     * @ORM\JoinTable(
     *     name="admin_user_role",
     *     joinColumns={@ORM\JoinColumn(name="admin_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $adminRoles;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Admins\AdminProfile",
     *      mappedBy="admin",
     *      cascade={"persist"}
     * )
     */
    protected $adminProfile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Flendoc\AppBundle\Entity\Admins\AdminMailSettings",
     *     mappedBy="admin",
     *     cascade={"persist"}
     * )
     */
    protected $mailSettings;

    /**
     * @ORM\Column(name="has_generated_password", type="boolean", nullable=false)
     */
    protected $hasGeneratedPassword = false;

    //elements used for model validation on registration
    //All this elements bellow will be added to user profile
    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     */
    protected $firstName;

    /**
     * Not mapped into the database
     * @Assert\NotNull(groups={"registration"})
     */
    protected $lastName;
    //end model validation elements

    /**
     * Not mapped into the database
     * @var string
     */
    protected $plainPassword;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mailSettings = new ArrayCollection();
        $this->adminRoles   = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $aAdminRoles = [];
        foreach ($this->adminRoles as $oAdminRole) {
            $aAdminRoles[] = $oAdminRole->getName();
        }

        return $aAdminRoles;
    }

    /**
     * @param mixed $adminRoles
     */
    public function setRoles($adminRoles)
    {
        $this->adminRoles = $adminRoles;
    }

    /**
     * @param UserRoles $oUserRoles
     */
    public function addRole(UserRoles $oUserRoles)
    {
        if ($this->adminRoles->contains($oUserRoles)) {
            return;
        }

        $this->adminRoles->add($oUserRoles);
        $oUserRoles->addAdmin($this);
    }

    /**
     * @param UserRoles $oUserRoles
     */
    public function removeRole(UserRoles $oUserRoles)
    {
        if (!$this->adminRoles->contains($oUserRoles)) {
            return;
        }

        $this->adminRoles->removeElement($oUserRoles);
        $oUserRoles->removeAdmin($this);
    }

    /**
     * @return mixed
     */
    public function getAdminRoles()
    {
        return $this->adminRoles;
    }

    /**
     * @param mixed $adminRoles
     */
    public function setAdminRoles($adminRoles)
    {
        $this->adminRoles = $adminRoles;
    }

    /**
     * @return mixed
     */
    public function getAdminProfile()
    {
        return $this->adminProfile;
    }

    /**
     * @param mixed $adminProfile
     */
    public function setAdminProfile($adminProfile)
    {
        $this->adminProfile = $adminProfile;
    }

    /**
     * @return mixed
     */
    public function getMailSettings()
    {
        return $this->mailSettings;
    }

    /**
     * @param mixed $mailSettings
     */
    public function setMailSettings($mailSettings)
    {
        $this->mailSettings = $mailSettings;
    }

    /**
     * @param AdminMailSettings $mailSettings
     */
    public function addMailSetting(AdminMailSettings $mailSettings)
    {
        $this->mailSettings->add($mailSettings);
    }

    /**
     * @param AdminMailSettings $mailSettings
     */
    public function removeMailSetting(AdminMailSettings $mailSettings)
    {
        $this->mailSettings->remove($mailSettings);
    }

    /**
     * @param string $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param bool $hasGeneratedPassword
     *
     * @return $this
     */
    public function setHasGeneratedPassword($hasGeneratedPassword)
    {
        $this->hasGeneratedPassword = $hasGeneratedPassword;

        return $this;
    }

    /**
     * @return bool
     */
    public function getHasGeneratedPassword()
    {
        return $this->hasGeneratedPassword;
    }
}
