<?php

namespace Flendoc\AppBundle\Entity\Admins;

use Doctrine\ORM\Mapping as ORM;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Flendoc\AppBundle\Validator\Constraints as SiteAssert;

/**
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Admins\AdminProfileImageRepository")
 * @ORM\Table(name="admin_profile")
 * @ORM\HasLifecycleCallbacks()
 */
class AdminProfile extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(
     *      targetEntity="Flendoc\AppBundle\Entity\Admins\Admins",
     *      inversedBy="adminProfile"
     * )
     * @ORM\JoinColumn(
     *     name="admin_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $admin;

    /**
     * @ORM\Column(name="first_name", type="string", length=100, options={"comment":"User's first name"})
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=100, options={"comment":"User's last name"})
     * @Assert\NotBlank()
     */
    protected $lastName;

    /**
     * @ORM\Column(name="gender", type="string", length=1, nullable=true, options={"comment":"User's gender"})
     */
    protected $gender;

    /**
     * @ORM\Column(name="photos", type="string", nullable=true)
     */
    protected $photos;

    /**
     * @ORM\Column(name="about_me", type="text", nullable=true)
     */
    protected $aboutMe;

    /**
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    protected $birthday;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getAboutMe()
    {
        return $this->aboutMe;
    }

    /**
     * @param mixed $aboutMe
     */
    public function setAboutMe($aboutMe)
    {
        $this->aboutMe = $aboutMe;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }
}
