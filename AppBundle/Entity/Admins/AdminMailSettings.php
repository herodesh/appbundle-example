<?php

namespace Flendoc\AppBundle\Entity\Admins;

use Flendoc\AppBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AdminMailSettings
 * @package Flendoc\AppBundle\Entity\Admins
 *
 * @ORM\Entity(repositoryClass="Flendoc\AppBundle\Repository\Admins\AdminMailSettingsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="admin_mail_settings")
 */
class AdminMailSettings extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    protected $enabled = true;

    /**
     * @ORM\Column(name="read_only", type="boolean", nullable=false)
     */
    protected $readOnly = false;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Admins\Admins",
     *     inversedBy="mailSettings"
     * )
     * @ORM\JoinColumn(
     *     name="admin_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $admin;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Flendoc\AppBundle\Entity\Mails\Mails"
     * )
     * @ORM\JoinColumn(
     *     name="mail_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected $mail;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getReadOnly()
    {
        return $this->readOnly;
    }

    /**
     * @param mixed $readOnly
     */
    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }
}
