<?php

namespace Flendoc\AppBundle\Query\Mysql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode,
    Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;

/**
 * Use with caution! Only for small queries!
 *
 * Class Rand
 * @package DoctrineExtensions\Query\Mysql
 *
 * @link    https://github.com/beberlei/DoctrineExtensions/blob/master/src/Query/Mysql/Rand.php
 */
class Rand extends FunctionNode
{
    /**
     * @var SimpleArithmeticExpression
     */
    private $expression = null;

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @return string
     * @throws \Doctrine\ORM\Query\AST\ASTException
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        if ($this->expression) {
            return 'RAND('.$this->expression->dispatch($sqlWalker).')';
        }

        return 'RAND()';
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     *
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        if (Lexer::T_CLOSE_PARENTHESIS !== $lexer->lookahead['type']) {
            $this->expression = $parser->SimpleArithmeticExpression();
        }
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
