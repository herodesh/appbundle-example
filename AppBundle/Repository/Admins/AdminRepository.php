<?php

namespace Flendoc\AppBundle\Repository\Admins;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Users\UserRoles;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class AdminRepository
 * @package Flendoc\AppBundle\Repository\Admins
 */
class AdminRepository extends FlendocRepository
{
    /**
     * Find all users by type. Type is defined in the controller
     *
     * @return array
     */
    public function findAllByType()
    {
        $getRolesQuery = $this->_em->createQueryBuilder()
                                   ->select("GROUP_CONCAT(DISTINCT ur.name SEPARATOR ', ')")
                                   ->from('AppBundle:Users\UserRoles', 'ur');

        $aResult = $this->_findUserAndUserProfile($getRolesQuery);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Find Admins basic details
     *
     * @param           $identifier
     * @param Languages $oLanguages
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBasicDetails($identifier, Languages $oLanguages)
    {
        $aResult = $this->createQueryBuilder('a')
                        ->select('
                            a.id, a.identifier, ap.gender, ap.firstName, ap.lastName, a.username, a.enabled, a.createdAt, a.lastLogin,
                            ap.photos, 
                            l.iso as languageIso, COALESCE(tl1.name, tl2.name) as languageName')
                        ->leftJoin('AppBundle:Admins\AdminProfile', 'ap', 'WITH', 'ap.admin = a.id')
                        ->leftJoin('AppBundle:Languages\Languages', 'l', 'WITH', 'l.iso = :language')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl1', 'WITH', 'tl1.translatedLanguages = l.id AND l.id = :language')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl2', 'WITH', 'tl2.translatedLanguages = l.id AND l.id = :defaultLanguage')
                        ->where('a.identifier =:identifier')
                        ->setParameters([
                            'identifier'      => $identifier,
                            'language'        => $oLanguages,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ])
                        ->getQuery()
                        ->getOneOrNullResult();

        return $aResult;
    }

    /**
     * @TODO Update for admins
     *
     * Find User Location
     */
    public function findUserLocation()
    {
        return;
    }

    /**
     * @param integer $identifier
     *
     * @return array
     */
    public function findRoles($identifier)
    {
        $aUserRoles = $this->createQueryBuilder('a')
                           ->select('ar.name')
                           ->leftJoin('a.adminRoles', 'ar')
                           ->where('a.identifier = :identifier')
                           ->setParameter('identifier', $identifier)
                           ->getQuery()
                           ->getScalarResult();

        $aVal = [];
        if ($aUserRoles) {

            foreach ($aUserRoles as $userRole) {
                $aVal[] = $userRole['name'];
            }
        }

        return $aVal;
    }

    /**
     * @param $adminRole
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAdminMailReceivers($adminRole)
    {
        $qb = $this->createQueryBuilder('a');

        $qb->leftJoin('a.adminRoles', 'ar')
           ->leftJoin(UserRoles::class, 'ur', 'WITH',  'ur = ar')
           ->where('ur.name = :adminRole')
           ->setParameter('adminRole', $adminRole);
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Find users that want to receive the emails
     *
     * @param $sUsername
     * @param $sMailIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMailReceivers($sUsername, $sMailIdentifier)
    {
        $aResult = $this->createQueryBuilder('u')
                        ->leftJoin('UserBundle:UserMailSettings', 'ums', 'WITH', 'ums.user= u.id')
                        ->leftJoin('AppBundle:Mails\Mails', 'm', 'WITH', 'ums.mail = m.id')
                        ->where('u.username = :username')
                        ->andWhere('m.identifier = :mailIdentifier')
                        ->andWhere('ums.enabled = :enabledSetting')
                        ->setParameters([
                            'username'       => $sUsername,
                            'mailIdentifier' => $sMailIdentifier,
                            'enabledSetting' => true,
                        ]);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param null $sRoleQuery
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _findUserAndUserProfile($sRoleQuery = null)
    {
        $aResult = $this->createQueryBuilder('a')
                        ->select('a.id, a.identifier, a.username, a.enabled, ap.firstName, ap.lastName')
                        ->leftJoin('AppBundle:Admins\AdminProfile', 'ap', 'WITH', 'ap.admin = a.id');

        if ($sRoleQuery) {
            $aResult->addSelect("(".$sRoleQuery.") AS roles");
        }

        return $aResult;
    }
}
