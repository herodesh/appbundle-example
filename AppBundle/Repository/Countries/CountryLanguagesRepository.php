<?php

namespace Flendoc\AppBundle\Repository\Countries;

use Doctrine\ORM\EntityRepository;

/**
 * Class CountryLanguagesRepository
 * @package Flendoc\AppBundle\Repository\Countries
 */
class CountryLanguagesRepository extends EntityRepository
{
    /**
     * Find Country name for each active language
     *
     * @param int $iCountryId
     *
     * @return mixed
     */
    public function findNameByCountryId($iCountryId)
    {
        $qb = $this->createQueryBuilder('country_languages');

        $qb
            ->select('country_languages.name', 'languages.iso')
            ->innerJoin('country_languages.countries', 'countries')
            ->innerJoin('country_languages.languages', 'languages')
            ->where(
                $qb->expr()->eq('country_languages.countries', ':countryId')
            )
            ->andWhere(
                $qb->expr()->eq('countries.isActive', ':isActive')
            )
            ->setParameters([
                'countryId' => $iCountryId,
                'isActive'  => true,
            ])
        ;

        return $qb->getQuery()->getResult();
    }
}
