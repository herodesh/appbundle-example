<?php

namespace Flendoc\AppBundle\Repository\Countries;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class CountriesRepository
 * @package Flendoc\AppBundle\Repository\Countries
 */
class CountriesRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('c')
                    ->select('c.id, c.code, c.identifier, c.isActive, 
                              COALESCE(cl1.name, cl2.name) as name')
                    ->leftJoin('c.countryLanguages', 'cl1', 'WITH', 'cl1.languages = :language')
                    ->leftJoin('c.countryLanguages', 'cl2', 'WITH', 'cl2.languages = :defaultLanguage')
                    ->setParameters([
                        'language'        => $oLanguage,
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ]);
    }

    /**
     * @param Languages $oLanguage
     * @param bool      $bJson
     *
     * @return array|false|string
     */
    public function findCountriesByUserLanguage(Languages $oLanguage, $bJson = false)
    {
        if ($bJson) {
            return json_encode($this->findByLanguage($oLanguage)->getQuery()->getScalarResult());
        }

        return $this->findByLanguage($oLanguage)->getQuery()->getScalarResult();
    }

    /**
     * @param           $id
     * @param Languages $oLanguage
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSingleCountryByIdAndUserLanguage($id, Languages $oLanguage)
    {
        return $this->findByLanguage($oLanguage)
                    ->andWhere('c.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param null      $sQueryString
     *
     * @return array
     */
    public function findCountryByQuery(Languages $oLanguage, $sQueryString = null)
    {
        $aResult = $this->findByLanguage($oLanguage);

        if (null != $sQueryString) {
            $aResult->andWhere("COALESCE(cl1.name, cl2.name) LIKE :queryString")
                    ->setParameter(':queryString', '%'.$sQueryString.'%')
                    ->setMaxResults(5);
        }

        return $aResult->getQuery()->getScalarResult();
    }
}
