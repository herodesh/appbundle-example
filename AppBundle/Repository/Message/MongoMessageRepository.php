<?php

namespace Flendoc\AppBundle\Repository\Message;

use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * Class MongoMessageRepository
 * @package Flendoc\AppBundle\Repository\Message
 */
class MongoMessageRepository extends DocumentRepository
{
    /**
     * @param string         $chatIdentifier
     * @param string         $messageId
     * @param int            $limit
     * @param \DateTime|null $oSoftDeletedAt
     * @param \DateTime|null $oLeftChatAt
     *
     * @return mixed
     */
    public function getMessagesByChatMessageIdAndLimit($chatIdentifier, $messageId, $limit, $oSoftDeletedAt = null, $oLeftChatAt = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('id')
            ->lt($messageId)
            ->field('chatIdentifier')
            ->equals($chatIdentifier)
        ;

        if (isset($oSoftDeletedAt)) {
            $qb
                ->field('sendAt')
                ->gt($oSoftDeletedAt)
            ;
        }

        if (isset($oLeftChatAt)) {
            $qb
                ->field('sendAt')
                ->lt($oLeftChatAt)
            ;
        }

        $qb
            ->sort('sendAt', 'desc')
            ->limit($limit)
            ->skip(0)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param string $chatIdentifier
     * @param int    $limit
     * @param \DateTime|null $oSoftDeletedAt
     * @param \DateTime|null $oLeftChatAt
     *
     * @return mixed
     */
    public function getMessagesByChatAndLimit($chatIdentifier, $limit, $oSoftDeletedAt = null, $oLeftChatAt = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('chatIdentifier')
            ->equals($chatIdentifier)
        ;

        if (isset($oSoftDeletedAt)) {
            $qb
                ->field('sendAt')
                ->gt($oSoftDeletedAt)
            ;
        }

        if (isset($oLeftChatAt)) {
            $qb
                ->field('sendAt')
                ->lt($oLeftChatAt)
            ;
        }

        $qb
            ->sort('sendAt', 'desc')
            ->limit($limit)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param $sUserIdentifier
     *
     * @return mixed
     */
    public function getUserRemovalMessage($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('chatAnnouncementUserIdentifier')
            ->equals($sUserIdentifier)
            ->field('chatAnnouncement')
            ->equals(true)
            ->sort('sendAt', 'desc')
            ->limit(1)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @param string $sChatIdentifier
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function removeMessagesByChat($sChatIdentifier)
    {
        $this->createQueryBuilder()
            ->remove()
            ->field('chatIdentifier')
            ->equals($sChatIdentifier)
            ->getQuery()
            ->execute()
        ;
    }
}
