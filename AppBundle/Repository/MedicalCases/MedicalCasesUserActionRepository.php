<?php

namespace Flendoc\AppBundle\Repository\MedicalCases;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserActionLanguages;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class MedicalCasesUserActionRepository
 * @package Flendoc\AppBundle\Repository\MedicalCases
 */
class MedicalCasesUserActionRepository extends FlendocRepository
{

    /**
     * @param Languages $oLanguage
     * @param boolean   $asJson
     *
     * @return array|string
     */
    public function findAllActionsByLanguage(Languages $oLanguage, $asJson = false)
    {
        $aResult = $this->_findByLanguage($oLanguage)->getQuery()->getScalarResult();

        return $asJson ? json_encode($aResult) : $aResult;
    }

    /**
     * @param Languages $oLanguage   User Langueg
     * @param string    $sIdentifier User Identifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneCaseActionByUserLanguageAndIdentifier(Languages $oLanguage, $sIdentifier)
    {
        $aResult = $this->_findByLanguage($oLanguage);
        $aResult->where('mcua.identifier = :sIdentifier')
                ->setParameter('sIdentifier', $sIdentifier);

        return $aResult->getQuery()->getSingleResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('mcua')
                    ->select('mcua.identifier, 
                        COALESCE(mcual1.description, mcual2.description) as description,
                        COALESCE(mcual1.userNotificationMessage, mcual2.userNotificationMessage) as userNotificationMessage'
                    )
                    ->leftJoin(
                        MedicalCasesUserActionLanguages::class,
                        'mcual1',
                        'WITH',
                        'mcual1.medicalCasesUserAction = mcua AND mcual1.languages = :language'
                    )
                    ->leftJoin(
                        MedicalCasesUserActionLanguages::class,
                        'mcual2',
                        'WITH',
                        'mcual2.medicalCasesUserAction = mcua AND  mcual2.languages = :defaultLanguage'
                    )
                    ->setParameters([
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        'language'        => $oLanguage,
                    ]);
    }
}
