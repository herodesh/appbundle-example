<?php

namespace Flendoc\AppBundle\Repository\MedicalCases;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Constants\AppConstants;

/**
 * Class PendingMedicalCaseRepository
 * @package Flendoc\AppBundle\Repository\MedicalCases
 */
class PendingMedicalCaseRepository extends EntityRepository
{

    /**
     * Find all approved medical cases
     *
     * @param bool $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findPendingMedicalCases($bQueryResult = false)
    {
        $qb = $this->createQueryBuilder('pendingMedicalCase');
        $qb
            ->where($qb->expr()->eq('pendingMedicalCase.isPublished', ':isPublished'))
            ->andWhere($qb->expr()->eq('pendingMedicalCase.isApproved', ':isApproved'))
            ->orderBy('pendingMedicalCase.createdAt', 'DESC')
            ->setParameters([
                'isPublished' => true,
                'isApproved'  => false,
            ]);

        if ($bQueryResult) {
            $qb->select('
                pendingMedicalCase.identifier as medicalCaseIdentifier,
                pendingMedicalCase.title as medicalCaseTitle,
                SUBSTRING(pendingMedicalCase.description, 1, 200) as medicalCaseDescription,
                doctor.identifier as doctorIdentifier,
                CONCAT(doctorProfile.firstName, \' \', doctorProfile.lastName) as authorName
                ')
               ->leftJoin('pendingMedicalCase.doctor', 'doctor')
               ->leftJoin('doctor.doctorProfile', 'doctorProfile');

            return $qb->getQuery()->getArrayResult();
        }

        return $qb;
    }

    /**
     * Find all pending medical cases order by create date
     *
     * @param integer $iDoctorId
     * @param integer $iPage
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findPendingMedicalCasesByDoctor($iDoctorId, int $iPage = null)
    {
        $qb = $this->_find_pending_medical_cases();

        if (null != $iPage) {
            $qb->setFirstResult($iPage * AppConstants::PAGINATION_PAGE_ITEMS);
        }

        $qb->setMaxResults(AppConstants::PAGINATION_PAGE_ITEMS);

        $qb
            ->where($qb->expr()->eq('pmc.doctor', $iDoctorId))
            ->andWhere($qb->expr()->eq('pmc.isSaved', true))
            ->andWhere($qb->expr()->eq('pmc.isPublished', 1))
            ->andWhere($qb->expr()->eq('pmc.isApproved', 0));

        return $qb->getQuery()->getResult();

    }

    /**
     * Find all pending medical cases order by create date
     *
     * @param integer $iDoctorId
     * @param integer $iPage
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findDraftsMedicalCasesByDoctor($iDoctorId, int $iPage = null)
    {
        $qb = $this->_find_pending_medical_cases();

        if (null != $iPage) {
            $qb->setFirstResult($iPage * AppConstants::PAGINATION_PAGE_ITEMS);
        }

        $qb->setMaxResults(AppConstants::PAGINATION_PAGE_ITEMS);

        $qb
            ->where($qb->expr()->eq('pmc.doctor', $iDoctorId))
            ->andWhere($qb->expr()->eq('pmc.isSaved', 1))
            ->andWhere($qb->expr()->eq('pmc.isPublished', 0))
            ->andWhere($qb->expr()->eq('pmc.isApproved', 0));

        $aQueryResults = $qb->getQuery()->getArrayResult();

        if (empty($aQueryResults)) {
            return null;
        }

        //we add the images to the end result
        $aEndResults = [];
        foreach ($aQueryResults as $aResults) {
            $qbImages = $this->createQueryBuilder('pmc')
                             ->select('mci.photos')
                             ->leftJoin('pmc.medicalCaseImages', 'mci')
                             ->andWhere('pmc.identifier = :medicalCaseIdentifier')
                             ->setParameter('medicalCaseIdentifier', $aResults['identifier'])
                             ->getQuery()->getScalarResult();

            $aResults['medicalCaseImages'] = call_user_func_array('array_merge_recursive', $qbImages)['photos'];

            //force array for images. We need this for single images in order to be displayed in lightslider
            if (!is_array($aResults['medicalCaseImages'])) {
                $aResults['medicalCaseImages'] = [$aResults['medicalCaseImages']];
            }

            $aEndResults[] = $aResults;
        }

        return $aEndResults;
    }

    /**
     * @param $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountDraftByUser($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder('pmc');

        $qb
            ->select('count(pmc)')
            ->leftJoin('pmc.doctor', 'doctor')
            ->where($qb->expr()->eq('doctor.identifier', ':userIdentifier'))
            ->andWhere($qb->expr()->eq('pmc.isSaved', 1))
            ->andWhere($qb->expr()->eq('pmc.isPublished', 0))
            ->andWhere($qb->expr()->eq('pmc.isApproved', 0))
            ->setParameters([
                'userIdentifier' => $sUserIdentifier,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountPendingByUser($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder('pmc');

        $qb
            ->select('count(pmc)')
            ->leftJoin('pmc.doctor', 'doctor')
            ->where($qb->expr()->eq('doctor.identifier', ':userIdentifier'))
            ->andWhere($qb->expr()->eq('pmc.isSaved', 1))
            ->andWhere($qb->expr()->eq('pmc.isPublished', 1))
            ->andWhere($qb->expr()->eq('pmc.isApproved', 0))
            ->setParameters([
                'userIdentifier' => $sUserIdentifier,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }


    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountPublishedPendingMedicalCases()
    {
        $qb = $this->createQueryBuilder('pmc');
        $qb->select('count(pmc.id)')
           ->where('pmc.isPublished = :isPublished')
           ->andWhere('pmc.isApproved = :isApproved')
           ->setParameters([
               'isPublished' => true,
               'isApproved'  => false,
           ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountNotPublishedPendingMedicalCases()
    {
        $qb = $this->createQueryBuilder('pmc');
        $qb
            ->select('count(pmc.id)')
            ->where($qb->expr()->eq('pmc.isPublished', ':isPublished'))
            ->andWhere($qb->expr()->eq('pmc.isApproved', ':isApproved'))
            ->andWhere($qb->expr()->eq('pmc.isSaved', ':isSaved'))
            ->setParameters([
                'isPublished' => false,
                'isApproved'  => false,
                'isSaved'     => true,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param \DateTime $oDate
     * @param int       $batch
     * @param int|null  $iId
     *
     * @return mixed
     */
    public function findNotPublishedPendingMedicalCases($oDate, $batch, $iId = null)
    {
        $qb = $this->createQueryBuilder('pmc');

        $qb
            ->where($qb->expr()->lt('pmc.createdAt', ':oDate'))
            ->andWhere($qb->expr()->eq('pmc.isPublished', ':isPublished'))
            ->andWhere($qb->expr()->eq('pmc.isApproved', ':isApproved'))
            ->andWhere($qb->expr()->eq('pmc.isSaved', ':isSaved'))
            ->setParameters([
                'oDate'       => $oDate,
                'isPublished' => false,
                'isApproved'  => false,
                'isSaved'     => true,
            ]);

        if (isset($iId)) {
            $qb
                ->andWhere($qb->expr()->gt('pmc.id', ':iId'))
                ->setParameter('iId', $iId);
        }

        $qb->setMaxResults($batch);

        return $qb->getQuery()->getResult();
    }

    /**
     * Abstract function to find for pending medical cases
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _find_pending_medical_cases()
    {
        $qb = $this->createQueryBuilder('pmc');

        $qb
            ->select(' 
                pmc.identifier as identifier, 
                pmc.title as title, 
                pmc.slugTitle as slug, 
                pmc.description as description, 
                pmc.medicalCaseLanguageIso as languageIso,
                pmc.updatedAt as updatedAt,
                pmc.isApproved as isApproved,
                doctor.identifier as doctorIdentifier,
                CONCAT(doctorProfile.firstName, \' \', doctorProfile.lastName) as authorName'
            )
            ->leftJoin('pmc.doctor', 'doctor')
            ->leftJoin('doctor.doctorProfile', 'doctorProfile')
            ->orderBy('pmc.createdAt', 'DESC');

        return $qb;
    }
}
