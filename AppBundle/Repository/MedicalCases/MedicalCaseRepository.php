<?php

namespace Flendoc\AppBundle\Repository\MedicalCases;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Skills\Skills;

/**
 * Class MedicalCaseRepository
 * @package Flendoc\AppBundle\Repository\MedicalCases
 */
class MedicalCaseRepository extends EntityRepository
{

    /**
     * @param integer $iDoctorId
     * @param bool    $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findBookmarkedMedicalCases(
        $iDoctorId,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('medicalCase');
        $qb
            ->innerJoin('medicalCase.medicalCaseBookmarks', 'medicalCaseBookmarks')
            ->where($qb->expr()->eq('medicalCaseBookmarks', $iDoctorId));

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param $iDoctorCaseId
     * @param $iDoctorId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkIfMedicalCaseIsBookmarked($iDoctorCaseId, $iDoctorId)
    {
        $qb = $this->createQueryBuilder('medicalCase');
        $qb
            ->select('count(medicalCase.id)')
            ->innerJoin('medicalCase.medicalCaseBookmarks', 'medicalCaseBookmarks')
            ->where($qb->expr()->eq('medicalCase.id', $iDoctorCaseId))
            ->andWhere($qb->expr()->eq('medicalCaseBookmarks', $iDoctorId));

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Find all approved medical cases
     *
     * @param array $aLanguageIso Medical case display language
     * @param null  $sUserIdentifier
     * @param int   $iPage
     *
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function findPublishedMedicalCases(
        array $aLanguageIso = null,
        $sUserIdentifier = null,
        int $iPage = null
    ) {
        $qb = $this->_find_published_medical_cases($aLanguageIso);

        if ($sUserIdentifier) {
            $qb->andWhere($qb->expr()->eq('doctor.identifier', ':sUserIdentifier'))
               ->setParameter('sUserIdentifier', $sUserIdentifier);
        }

        if (null != $iPage) {
            $qb->setFirstResult($iPage * AppConstants::PAGINATION_PAGE_ITEMS);
        }

        $qb->setMaxResults(AppConstants::PAGINATION_PAGE_ITEMS);

        $aQueryResults = $qb->getQuery()->getArrayResult();

        //we add the images to the end result
        $aEndResults = [];
        foreach ($aQueryResults as $aResults) {
            $qbImages = $this->createQueryBuilder('mc')
                             ->select('mci.photos')
                             ->leftJoin('mc.medicalCaseImages', 'mci')
                             ->andWhere('mc.identifier = :medicalCaseIdentifier')
                             ->setParameter('medicalCaseIdentifier', $aResults['identifier'])
                             ->getQuery()->getScalarResult();

            $aResults['medicalCaseImages'] = call_user_func_array('array_merge_recursive', $qbImages)['photos'];

            //force array for images. We need this for single images in order to be displayed in lightslider
            if (!is_array($aResults['medicalCaseImages'])) {
                $aResults['medicalCaseImages'] = [$aResults['medicalCaseImages']];
            }

            $aEndResults[] = $aResults;
        }

        return $aEndResults;
    }

    /**
     * Get medical cases filtered by skill
     *
     * @param array   $aLanguageIso
     * @param Skills  $oSkill
     * @param integer $iPage
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findMedicalCasesBySkillCategory(array $aLanguageIso, $oSkill, int $iPage = null)
    {
        $qb = $this->_find_published_medical_cases($aLanguageIso);

        if (null != $iPage) {
            $qb->setFirstResult($iPage * AppConstants::PAGINATION_PAGE_ITEMS);
        }

        $qb->setMaxResults(AppConstants::PAGINATION_PAGE_ITEMS);

        $qb
            ->innerJoin('medicalCase.medicalCaseSkills', 'medicalCaseSkills')
            ->andWhere($qb->expr()->eq('medicalCaseSkills.id', $oSkill->getId()))
            ->andWhere($qb->expr()->eq('medicalCase.isApproved', ':isApproved'))
            ->setParameter('isApproved', true)
            ->orderBy('medicalCase.createdAt', 'DESC');

        $aQueryResults = $qb->getQuery()->getArrayResult();

        //we add the images to the end result
        $aEndResults = [];
        foreach ($aQueryResults as $aResults) {
            $qbImages = $this->createQueryBuilder('mc')
                             ->select('mci.photos')
                             ->leftJoin('mc.medicalCaseImages', 'mci')
                             ->andWhere('mc.identifier = :medicalCaseIdentifier')
                             ->setParameter('medicalCaseIdentifier', $aResults['identifier'])
                             ->getQuery()->getScalarResult();

            $aResults['medicalCaseImages'] = call_user_func_array('array_merge_recursive', $qbImages)['photos'];

            //force array for images. We need this for single images in order to be displayed in lightslider
            if (!is_array($aResults['medicalCaseImages'])) {
                $aResults['medicalCaseImages'] = [$aResults['medicalCaseImages']];
            }

            $aEndResults[] = $aResults;
        }

        return $aEndResults;
    }

    /**
     * Get number of medical cases per skill
     *
     * @return mixed
     */
    public function getNumberOfMedicalCasesPerSkill()
    {
        $qb = $this->createQueryBuilder('medicalCase');
        $qb
            ->select('medicalCaseSkills.identifier as skillIdentifier, COUNT(medicalCase.id) as numberOfMedicalCases')
            ->innerJoin('medicalCase.medicalCaseSkills', 'medicalCaseSkills')
            ->where($qb->expr()->eq('medicalCase.isApproved', ':isApproved'))
            ->groupBy('medicalCaseSkills.identifier')
            ->setParameter('isApproved', true);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $sIdentifier
     *
     * @return mixed|null
     */
    public function findMedicalCaseForFeeds($sIdentifier)
    {
        $qb = $this->_find_published_medical_cases();
        $qb
            ->where($qb->expr()->eq('medicalCase.isApproved', ':isApproved'))
            ->andWhere($qb->expr()->eq('medicalCase.identifier', ':identifier'))
            ->setParameters([
                'isApproved' => true,
                'identifier' => $sIdentifier,
            ]);

        $aQueryResults = $qb->getQuery()->getArrayResult();

        if (empty($aQueryResults)) {
            return null;
        }

        //we add the images to the end result
        $aEndResults = [];
        foreach ($aQueryResults as $aResults) {
            $qbImages = $this->createQueryBuilder('mc')
                             ->select('mci.photos')
                             ->leftJoin('mc.medicalCaseImages', 'mci')
                             ->andWhere('mc.identifier = :medicalCaseIdentifier')
                             ->setParameter('medicalCaseIdentifier', $aResults['identifier'])
                             ->getQuery()->getScalarResult();

            $aResults['medicalCaseImages'] = call_user_func_array('array_merge_recursive', $qbImages)['photos'];

            //force array for images. We need this for single images in order to be displayed in lightslider
            if (!is_array($aResults['medicalCaseImages'])) {
                $aResults['medicalCaseImages'] = [$aResults['medicalCaseImages']];
            }

            $aEndResults[] = $aResults;
        }

        return $aEndResults[0];
    }

    /**
     * @param $iLimit
     *
     * @return mixed
     */
    public function findLastMedicalCasesWithLimit($iLimit)
    {
        $qb = $this->createQueryBuilder('mc');

        $qb->where($qb->expr()->eq('mc.isApproved', ':isApproved'))
           ->addOrderBy('mc.id', 'DESC')
           ->setParameter(':isApproved', true)
           ->setMaxResults($iLimit);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all approved medical cases by search key.
     * This does not search in Skills attached to the medical case even if it should (@TODO Not implemented yet)
     *
     * @param null $sSearchKey
     * @param int  $iPage
     * @param null $iPageSize
     * @param null $iMaxResults
     *
     * @return array|\Doctrine\ORM\QueryBuilder|null
     */
    public function findPublishedMedicalCasesBySearchKey(
        $sSearchKey = null,
        $iPage = 1,
        $iPageSize = null,
        $iMaxResults = null
    ) {
        if (null == $sSearchKey) {
            return null;
        }

        $qb = $this->createQueryBuilder('mc');
        $qb->where($qb->expr()->eq('mc.isApproved', ':isApproved'))
           ->andWhere(
               $qb->expr()->like('LOWER(mc.title)',
                   $qb->expr()->literal('%'.$sSearchKey.'%')
               )
           )
           ->setFirstResult($iPageSize * $iPage)
           ->setParameter(':isApproved', true);

        if (null != $iMaxResults) {
            $qb->setMaxResults($iMaxResults);
        } else {
            $qb->setMaxResults($iPageSize);
        }

        $qb
            ->orderBy('mc.createdAt')
            ->orderBy('mc.identifier');

        //we return simple result as everything is prepared in the controller
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $skip
     * @param $limit
     *
     * @return mixed
     */
    public function findWithSkipAndLimit($skip, $limit)
    {
        $qb = $this->createQueryBuilder('mc')
                   ->setFirstResult($skip)
                   ->setMaxResults($limit)
                   ->orderBy('mc.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNumberOfsApprovedAndAdminRevisedMedicalCases()
    {
        $qb = $this->createQueryBuilder('medical_case');

        $qb
            ->select('count(medical_case.id)')
            ->where(
                $qb->expr()->eq('medical_case.isApproved', ':isApproved')
            )
            ->andWhere(
                $qb->expr()->eq('medical_case.isAdminRevised', ':isAdminRevised')
            )
            ->setParameters([
                'isApproved'     => true,
                'isAdminRevised' => true,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCountPublishedByUser($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder('medical_case');

        $qb
            ->select('count(medical_case.id)')
            ->leftJoin('medical_case.doctor', 'doctor')
            ->where(
                $qb->expr()->eq('medical_case.isApproved', ':isApproved')
            )
            ->andWhere(
                $qb->expr()->eq('medical_case.isAdminRevised', ':isAdminRevised')
            )
            ->andWhere(
                $qb->expr()->eq('doctor.identifier', ':userIdentifier')
            )
            ->setParameters([
                'isApproved'     => true,
                'isAdminRevised' => true,
                'userIdentifier' => $sUserIdentifier,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Abstract function to find for published medical cases
     *
     * @param array $aLanguageIso An array of medical cases languages to be displayed
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _find_published_medical_cases(array $aLanguageIso = null)
    {
        $qb = $this->createQueryBuilder('medicalCase');

        $qb
            ->select(' 
                medicalCase.identifier as identifier, 
                medicalCase.title as title, 
                medicalCase.slugTitle as slug, 
                medicalCase.description as description, 
                medicalCase.sources as sources, 
                medicalCase.medicalCaseLanguageIso as languageIso,
                medicalCase.updatedAt as updatedAt,
                medicalCase.createdAt as createdAt,
                medicalCase.isApproved as isApproved,
                pendingMedicalCases.identifier as pendingMedicalCaseIdentifier,
                doctor.identifier as doctorIdentifier,
                CONCAT(doctorProfile.firstName, \' \', doctorProfile.lastName) as authorName'
            )
            ->leftJoin('medicalCase.doctor', 'doctor')
            ->leftJoin('doctor.doctorProfile', 'doctorProfile')
            ->leftJoin('medicalCase.pendingMedicalCases', 'pendingMedicalCases')
            ->where($qb->expr()->eq('medicalCase.isApproved', ':isApproved'));

        if (null != $aLanguageIso) {
            $qb->andWhere('medicalCase.medicalCaseLanguageIso IN (:medicalCaseLanguageIso)')
               ->setParameter('medicalCaseLanguageIso', $aLanguageIso);
        }

        $qb->orderBy('medicalCase.createdAt', 'DESC')
           ->setParameter('isApproved', true);

        return $qb;
    }
}
