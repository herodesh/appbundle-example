<?php

namespace Flendoc\AppBundle\Repository\MedicalCases;

use Doctrine\ORM\EntityRepository;

/**
 * Class MedicalCaseImagesRepository
 * @package Flendoc\AppBundle\Repository\MedicalCases
 */
class MedicalCaseImagesRepository extends EntityRepository
{
}
