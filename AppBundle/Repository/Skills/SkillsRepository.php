<?php

namespace Flendoc\AppBundle\Repository\Skills;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Validator\Constraints\Language;

/**
 * Class SkillsRepository
 * @package Flendoc\AppBundle\Repository\Skills
 */
class SkillsRepository extends FlendocRepository implements RepositoryInterface
{

    /**
     * @param Languages $oLanguage
     * @param string    $sOrder
     *
     * @return array
     */
    public function findAllByLanguageOrdered(Languages $oLanguage, $sOrder = 'ASC')
    {
        $aResult = $this->findByLanguage($oLanguage);

        $aResult = $aResult->orderBy('name', $sOrder);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     * @param null      $aIds
     *
     * @return array
     */
    public function findByLanguageAndId(Languages $oLanguage, $aIds = null)
    {
        $aResult = $this->findByLanguage($oLanguage);

        //get only passed ids
        if (null != $aIds) {
            $aResult->andWhere('s IN (:skillsIds)')
                    ->setParameter('skillsIds', $aIds);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     * @param null|string      $sIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByLanguageAndIdentifier(Languages $oLanguage, $sIdentifier = null)
    {
        $aResult = $this->findByLanguage($oLanguage);

        //get only passed ids
        if (null != $sIdentifier) {
            $aResult->andWhere('s.identifier = :skillIdentifier')
                    ->setParameter('skillIdentifier', $sIdentifier);
        }

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * Find all By primary language
     *
     * @param null      $sQueryString
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findAllByParametersAndLanguage($sQueryString = null, Languages $oLanguage)
    {
        $oSkill = $this->findByLanguage($oLanguage);

        if (null != $sQueryString) {
            $oSkill->andWhere('COALESCE(sl1.name, sl2.name) LIKE :queryString')
                   ->setParameter('queryString', '%'.$sQueryString.'%')
                   ->setMaxResults(10);
        }

        return $oSkill
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $oQueryBuilder = $this->createQueryBuilder('s')
                              ->select('s.id, s.identifier, COALESCE(sl1.name, sl2.name) as name')
                              ->leftJoin('AppBundle:Skills\SkillLanguages', 'sl1', 'WITH', 's = sl1.skills AND sl1.languages = :language')
                              ->leftJoin('AppBundle:Skills\SkillLanguages', 'sl2', 'WITH', 's = sl2.skills AND sl2.languages = :defaultLanguage')
                              ->setParameters([
                                  'language'        => $oLanguage,
                                  'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                              ]);

        return $oQueryBuilder;
    }
}
