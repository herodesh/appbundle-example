<?php

namespace Flendoc\AppBundle\Repository\Skills;

use Doctrine\ORM\EntityRepository;

/**
 * Class SkillLanguagesRepository
 * @package Flendoc\AppBundle\Repository\Skills
 */
class SkillLanguagesRepository extends EntityRepository
{

    /**
     * Find Skills Languages Ids
     *
     * @param $skillId
     *
     * @return array
     */
    public function findSkillLanguagesIds($skillId)
    {
        $aResult = $this->createQueryBuilder('sl')
                        ->select('IDENTITY (sl.languages)')
                        ->where('sl.skills = :skillId')
                        ->setParameter('skillId', $skillId);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Find Skills names for a specific language
     *
     * @param array  $aSkillsIds
     * @param string $sLanguageIso
     *
     * @return mixed
     */
    public function findNamesBySkillsIdsAndIso($aSkillsIds, $sLanguageIso)
    {
        $qb = $this->createQueryBuilder('skill_languages');

        $qb
            ->select('skill_languages.name as translation, skills.identifier')
            ->innerJoin('skill_languages.skills', 'skills')
            ->innerJoin('skill_languages.languages', 'languages')
            ->where(
                $qb->expr()->in('skill_languages.skills', ':ids')
            )
            ->andWhere(
                $qb->expr()->eq('languages.iso', ':iso')
            )
            ->setParameters([
                'ids' => $aSkillsIds,
                'iso' => $sLanguageIso,
            ])
        ;

        return $qb->getQuery()->getResult();
    }
}
