<?php

namespace Flendoc\AppBundle\Repository\Users;

use Flendoc\AppBundle\Entity\Doctors\UserLockReasons;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class UserLockReasonsRepository
 * @package Flendoc\AppBundle\Repository\Users
 */
class UserLockReasonsRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('ulr')
                    ->select('ulr.id, ulr.identifier,
                              COALESCE(ulrl1.name, ulrl2.name) as name,
                              COALESCE(ulrl1.description, ulrl2.description) as description'
                    )
                    ->leftJoin('ulr.userLockReasonsLanguages', 'ulrl1', 'WITH', 'ulrl1.languages = :language')
                    ->leftJoin('ulr.userLockReasonsLanguages', 'ulrl2', 'WITH', 'ulrl2.languages = :defaultLanguage')
                    ->setParameters([
                        'language'        => $oLanguage,
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ]);
    }

    /**
     * @param Languages       $oLanguage
     * @param UserLockReasons $oUserLockReason
     *
     * @return array
     */
    public function findByLanguageAndObject(Languages $oLanguage, UserLockReasons $oUserLockReason)
    {
        $q = $this->findByLanguage($oLanguage);
        $q->andWhere('ulr = :userLockReason')
          ->setParameter('userLockReason', $oUserLockReason);

        return $q->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     * @param string    $sStringIdentifier
     *
     * @return array
     */
    public function findByLanguageAndStringIdentifier(Languages $oLanguage, $sStringIdentifier)
    {
        $q = $this->findByLanguage($oLanguage);
        $q->andWhere('ulr.stringIdentifier = :stringIdentifier')
          ->setParameter('stringIdentifier', $sStringIdentifier);

        return $q->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findByUserLanguage(Languages $oLanguage)
    {
        return $this->findByLanguage($oLanguage)->getQuery()->getScalarResult();
    }
}
