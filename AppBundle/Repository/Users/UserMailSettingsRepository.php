<?php

namespace Flendoc\AppBundle\Repository\Users;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class UserMailSettingsRepository
 * @package Flendoc\AppBundle\Repository\Users
 */
class UserMailSettingsRepository extends FlendocRepository
{
    /**
     * Find UserMailerSettings identifiers
     *
     * @param $oUser
     *
     * @return array
     */
    public function findUserMailSettingsMailId($oUser)
    {
        $aResult = $this->createQueryBuilder('ums')
                        ->select('m.id')
                        ->leftJoin('AppBundle:Mails\Mails', 'm', 'WITH', 'ums.mail = m.id')
                        ->where('ums.admin = :userId')
                        ->setParameter('userId', $oUser)
                        ->getQuery()
                        ->getScalarResult();

        return $aResult;
    }

    /**
     * Find UserMailSettings
     *
     * @param           $id
     * @param Languages $oLanguages
     *
     * @return array
     */
    public function findUserMailSettings($id, Languages $oLanguages)
    {
        $aResult = $this->createQueryBuilder('ums')
                        ->select('ums.id, ums.enabled, ums.readOnly, m.identifier, m.isActive, COALESCE(ml1.subject, ml2.subject) as subject')
                        ->leftJoin('AppBundle:Mails\Mails', 'm', 'WITH', 'ums.mail = m.id')
                        ->leftJoin('AppBundle:Mails\MailLanguages', 'ml1', 'WITH', 'ml1.mail = ums.mail AND ml1.languages = :languages')
                        ->leftJoin('AppBundle:Mails\MailLanguages', 'ml2', 'WITH', 'ml2.mail = ums.mail AND ml2.languages = :defaultLanguage')
                        ->where('ums.admin = :userId')
                        ->andWhere('m.isActive = true')
                        ->setParameters([
                            'userId'          => $id,
                            'languages'       => $oLanguages,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ])
                        ->getQuery()
                        ->getScalarResult();

        return $aResult;

    }
}
