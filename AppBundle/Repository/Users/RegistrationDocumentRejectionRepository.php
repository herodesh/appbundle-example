<?php

namespace Flendoc\AppBundle\Repository\Users;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class RegistrationDocumentRejectionReasonsRepository
 * @package Flendoc\AppBundle\Repository\Users
 */
class RegistrationDocumentRejectionRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('rdr')
                    ->select('rdr.id,
                              COALESCE(rdrl1.description, rdrl2.description) as description')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentsRejectionLanguages', 'rdrl1', 'WITH', 'rdrl1.registrationDocumentsRejection = rdr AND rdrl1.languages = :language')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentsRejectionLanguages', 'rdrl2', 'WITH', 'rdrl2.registrationDocumentsRejection = rdr AND rdrl2.languages = :defaultLanguage')
                    ->setParameters([
                        'language'        => $oLanguage,
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ]);
    }

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findDocumentRejectionReasonsByUserLanguage(Languages $oLanguage)
    {
        return $this->findByLanguage($oLanguage)->getQuery()->getScalarResult();
    }
}
