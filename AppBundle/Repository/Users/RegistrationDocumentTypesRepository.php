<?php

namespace Flendoc\AppBundle\Repository\Users;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class RegistrationDocumentTypesRepository
 * @package Flendoc\AppBundle\Repository\Users
 */
class RegistrationDocumentTypesRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('rdt')
                    ->select('rdt.id, rdt.isIdentificationDocument,
                              COALESCE(rdtl1.name, rdtl2.name) as name')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentTypeLanguages', 'rdtl1', 'WITH', 'rdtl1.registrationDocumentTypes = rdt AND rdtl1.languages = :language')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentTypeLanguages', 'rdtl2', 'WITH', 'rdtl2.registrationDocumentTypes = rdt AND rdtl2.languages = :defaultLanguage')
                    ->setParameters([
                        'language'        => $oLanguage,
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ]);
    }

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findDocumentTypesByUserLanguage(Languages $oLanguage)
    {
        return $this->findByLanguage($oLanguage)->getQuery()->getScalarResult();
    }
}
