<?php

namespace Flendoc\AppBundle\Repository\Users;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRolesRepository
 * @package Flendoc\AppBundle\Repository\Users
 */
class UserRolesRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findRoles()
    {
        $aResult = $this->createQueryBuilder('ur')
                        ->select('ur.id, ur.name');

        return $aResult->getQuery()->getScalarResult();
    }

}
