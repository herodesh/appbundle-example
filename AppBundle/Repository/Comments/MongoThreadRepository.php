<?php

namespace Flendoc\AppBundle\Repository\Comments;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;

/**
 * Class MongoThreadRepository
 * @package Flendoc\AppBundle\Repository\Comments
 */
class MongoThreadRepository extends DocumentRepository
{
    /**
     * Find threads that can switch users. (Non feeds)
     *
     * @param string $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findThreadsThatCanSwitchUsers($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('dependencyType')
            ->notEqual(ObjectTypeConstants::OBJECT_MONGO_FEED)
            ->field('userIdentifier')
            ->equals($sUserIdentifier)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * Find the threads based on the feed identifier. This should delete the comments and likes linked to them as well
     *
     * @param $sFeedIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndRemoveByFeedIdentifier($sFeedIdentifier)
    {
        $qb = $this->createQueryBuilder();
        $qb->remove()
           ->field('dependencyIdentifier')->equals($sFeedIdentifier)
        ;

        return $qb->getQuery()->execute();
    }
}
