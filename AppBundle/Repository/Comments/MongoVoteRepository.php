<?php

namespace Flendoc\AppBundle\Repository\Comments;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Documents\Comment;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;

/**
 * Class MongoVoteRepository
 * @package Flendoc\AppBundle\Repository\Comments
 */
class MongoVoteRepository extends DocumentRepository
{
    /**
     * @param MongoThread $oMongoThread
     * @param string      $sUserIdentifier
     * @param Doctors     $oGhostUser
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndUpdateVotesByThread(MongoThread $oMongoThread, $sUserIdentifier, $oGhostUser)
    {
        //we need to use the updateMulti function in mongo for this one
        $qb = $this->createQueryBuilder()
                   ->findAndUpdate()
                   ->field('thread')->references($oMongoThread)
                   ->field('userIdentifier')->equals($sUserIdentifier)
                   ->field('userIdentifier')->set($oGhostUser->getIdentifier())
                   ->updateMany();

        return $qb->getQuery()->execute();
    }

    /**
     * @param MongoThread $oMongoThread
     * @param string      $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndRemoveVotesByThread(MongoThread $oMongoThread, $sUserIdentifier)
    {
        //we need to use the updateMulti function in mongo for this one
        $qb = $this->createQueryBuilder()
                   ->remove()
                   ->field('thread')->references($oMongoThread)
                   ->field('userIdentifier')->equals($sUserIdentifier);

        return $qb->getQuery()->execute();
    }

    /**
     * @param MongoComment $oMongoComment
     * @param string       $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndRemoveVotesByComment(MongoComment $oMongoComment, $sUserIdentifier)
    {
        $qb = $this->createQueryBuilder()
                   ->remove()
                   ->field('comment')->references($oMongoComment)
                   ->field('userIdentifier')->equals($sUserIdentifier);

        return $qb->getQuery()->execute();
    }
}
