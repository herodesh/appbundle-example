<?php

namespace Flendoc\AppBundle\Repository\Comments;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Documents\Comment;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class MongoCommentRepository
 * @package Flendoc\AppBundle\Repository\Comments
 */
class MongoCommentRepository extends DocumentRepository
{
    /**
     * Find last X number of comments for MongoThread
     *
     * @param string      $sMongoThreadId
     * @param int         $iIsReply
     * @param int|null    $iLimit
     * @param string|null $sCommentId
     * @param string|null $sParentCommentId
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findCommentsByType(
        $sMongoThreadId,
        $iIsReply,
        $iLimit = null,
        $sCommentId = null,
        $sParentCommentId = null
    ) {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('thread')
            ->equals($sMongoThreadId)
            ->field('isReply')
            ->equals(boolval($iIsReply));

        if (isset($sCommentId)) {
            $qb
                ->field('id')
                ->lt($sCommentId);
        }

        if (isset($sParentCommentId)) {
            $qb
                ->field('parent')
                ->equals($sParentCommentId);
        }

        $qb->sort('createdAt', 'desc');

        if (isset($iLimit)) {
            $qb
                ->skip(0)
                ->limit($iLimit);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Find last X number of comments for MongoThread
     *
     * @param string      $sMongoThreadId
     * @param int         $iIsReply
     * @param int|null    $iLimit
     * @param string|null $sCommentId
     * @param string|null $sParentCommentId
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findCommentsAfterByType(
        $sMongoThreadId,
        $iIsReply,
        $iLimit = null,
        $sCommentId = null,
        $sParentCommentId = null
    ) {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('thread')
            ->equals($sMongoThreadId)
            ->field('isReply')
            ->equals(boolval($iIsReply));

        if (isset($sCommentId)) {
            $qb
                ->field('id')
                ->gt($sCommentId);
        }

        if (isset($sParentCommentId)) {
            $qb
                ->field('parent')
                ->equals($sParentCommentId);
        }

        if (isset($iLimit)) {
            $qb
                ->skip(0)
                ->limit($iLimit);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param string      $sMongoThreadId
     * @param bool        $bIsReply
     * @param string|null $sCommentId
     * @param string|null $sParentCommentId
     *
     * @return mixed
     */
    public function countNumberOfComments(
        $sMongoThreadId,
        $bIsReply,
        $sCommentId = null,
        $sParentCommentId = null
    ) {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('thread')
            ->equals($sMongoThreadId)
            ->field('isReply')
            ->equals($bIsReply);

        if (isset($sParentCommentId)) {
            $qb
                ->field('parent')
                ->equals($sParentCommentId);
        }

        if (isset($sCommentId)) {
            $qb
                ->field('id')
                ->lt($sCommentId);
        }

        return $qb->getQuery()->count();
    }

    /**
     * @param string      $sMongoThreadId
     * @param bool        $bIsReply
     * @param string|null $sCommentId
     * @param string|null $sParentCommentId
     *
     * @return mixed
     */
    public function countNumberOfCommentsAfter(
        $sMongoThreadId,
        $bIsReply,
        $sCommentId = null,
        $sParentCommentId = null
    ) {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('thread')
            ->equals($sMongoThreadId)
            ->field('isReply')
            ->equals($bIsReply);

        if (isset($sParentCommentId)) {
            $qb
                ->field('parent')
                ->equals($sParentCommentId);
        }

        if (isset($sCommentId)) {
            $qb
                ->field('id')
                ->gt($sCommentId);
        }

        return $qb->getQuery()->count();
    }

    /**
     * @param string      $sMongoThreadId
     * @param bool        $isReply
     * @param string      $sUserIdentifier
     * @param string|null $sParentCommentId
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findCommentsUserIdentifiers($sMongoThreadId, $isReply, $sUserIdentifier, $sParentCommentId = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->distinct('userIdentifier')
            ->field('thread')
            ->equals($sMongoThreadId)
            ->field('isReply')
            ->equals($isReply);

        if ($sParentCommentId) {
            $qb
                ->field('parent')
                ->equals($sParentCommentId);
        }

        if ($sUserIdentifier) {
            $qb
                ->field('userIdentifier')
                ->notEqual($sUserIdentifier);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param MongoThread $oMongoThread
     * @param string      $sUserIdentifier
     * @param Doctors     $oGhostUser
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndUpdateCommentsByThread(MongoThread $oMongoThread, $sUserIdentifier, $oGhostUser)
    {
        //we need to use the updateMulti function in mongo for this one
        $qb = $this->createQueryBuilder()
                   ->findAndUpdate()
                   ->field('thread')->references($oMongoThread)
                   ->field('userIdentifier')->equals($sUserIdentifier)
                   ->field('userIdentifier')->set($oGhostUser->getIdentifier())
                   ->updateMany();

        return $qb->getQuery()->execute();
    }

    /**
     * @param MongoThread $oMongoThread
     * @param string      $sUserIdentifier
     * @param Doctors     $oGhostUser
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndDeleteCommentsByThread(MongoThread $oMongoThread, $sUserIdentifier)
    {
        //we need to use the updateMulti function in mongo for this one
        $qb = $this->createQueryBuilder()
                   ->remove()
                   ->field('thread')->references($oMongoThread)
                   ->field('userIdentifier')->equals($sUserIdentifier);

        return $qb->getQuery()->execute();
    }

}
