<?php

namespace Flendoc\AppBundle\Repository\Feeds;

use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * Class MongoFeedRepository
 * @package Flendoc\AppBundle\Repository\Feeds
 */
class MongoFeedRepository extends DocumentRepository
{
    /**
     * @param $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findLastAddedFeedUpdate($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder()->refresh(true)->eagerCursor(true);
        $qb->field('creatorUserIdentifier')
           ->equals($sUserIdentifier)
           ->sort('createdAt', 'desc')
           ->limit(1);

        return $qb->getQuery()->execute();
    }

    /**
     * Find the feeds of the user and deletes them from mongoDb
     *
     * @param string $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findAndRemoveFeedByCreator($sUserIdentifier)
    {
        $qb = $this->createQueryBuilder();
        $qb->remove()
            ->field('creatorUserIdentifier')->equals($sUserIdentifier)

            ;

        return $qb->getQuery()->execute();
    }
}


