<?php

namespace Flendoc\AppBundle\Repository\Jobs;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class CareerLevelsRepository
 * @package Flendoc\AppBundle\Repository\Jobs
 */
class CareerLevelsRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->_createQueryBuilder($oLanguage)->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function _createQueryBuilder(Languages $oLanguage)
    {
        $oQueryBuilder = $this->createQueryBuilder('cl')
                              ->select('cl.id, COALESCE(cl1.name, cl2.name) as name')
                              ->leftJoin('AppBundle:Jobs\CareerLevelLanguages', 'cl1', 'WITH', 'cl.id = cl1.careerLevel AND cl1.languages = :language')
                              ->leftJoin('AppBundle:Jobs\CareerLevelLanguages', 'cl2', 'WITH', 'cl.id = cl2.careerLevel AND cl2.languages = :defaultLanguage')
                              ->setParameters([
                                  'language'        => $oLanguage,
                                  'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                              ]);

        return $oQueryBuilder;
    }
}
