<?php

namespace Flendoc\AppBundle\Repository\Jobs;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class JobTypesRepository
 * @package Flendoc\AppBundle\Repository\Jobs
 */
class JobTypesRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->_createQueryBuilder($oLanguage)->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function _createQueryBuilder(Languages $oLanguage)
    {
        $oQueryBuilder = $this->createQueryBuilder('jt')
                              ->select('jt.id, COALESCE(jtl1.name, jtl2.name) as name')
                              ->leftJoin('AppBundle:Jobs\JobTypeLanguages', 'jtl1', 'WITH', 'jt.id = jtl1.jobType AND jtl1.languages = :language')
                              ->leftJoin('AppBundle:Jobs\JobTypeLanguages', 'jtl2', 'WITH', 'jt.id = jtl2.jobType AND jtl2.languages = :defaultLanguage')
                              ->setParameters([
                                  'language'        => $oLanguage,
                                  'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                              ]);

        return $oQueryBuilder;
    }
}
