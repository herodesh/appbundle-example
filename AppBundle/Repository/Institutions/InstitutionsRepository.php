<?php

namespace Flendoc\AppBundle\Repository\Institutions;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class InstitutionsRepository
 * @package Flendoc\AppBundle\Repository\Institutions
 */
class InstitutionsRepository extends FlendocRepository
{

    /**
     * @param                $institutionIdentifier
     * @param Languages|null $oLanguage
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findInstitutionDetails($institutionIdentifier, Languages $oLanguage = null)
    {
        $aResult = $this->createQueryBuilder('i')
                        ->select('i.name, i.identifier, i.institutionLogo, i.name as institutionName, i.address, i.phone, i.website, COALESCE(tl1.name, tl2.name) as languageName, COALESCE(countryL1.name, countryL2.name) as countryName, COALESCE(cityL1.name, cityL2.name) as cityName')
                        ->leftJoin('AppBundle:Institutions\InstitutionLanguages', 'il', 'WITH', 'il.institutions = i')
                        ->leftJoin('AppBundle:Countries\CountryLanguages', 'countryL1', 'WITH', 'i.institutionCountry = countryL1.countries AND countryL1.languages = :language')
                        ->leftJoin('AppBundle:Countries\CountryLanguages', 'countryL2', 'WITH', 'i.institutionCountry = countryL2.countries AND countryL2.languages = :defaultLanguage')
                        ->leftJoin('AppBundle:Countries\CountrySpokenLanguages', 'csl', 'WITH', 'csl.country = i.institutionCountry AND csl.isPrimary = true')
                        ->leftJoin('AppBundle:Cities\CityLanguages', 'cityL1', 'WITH', 'i.institutionCity = cityL1.cities AND cityL1.languages = :language')
                        ->leftJoin('AppBundle:Cities\CityLanguages', 'cityL2', 'WITH', 'i.institutionCity = cityL2.cities AND cityL2.languages = csl.language')
                        ->leftJoin('AppBundle:Languages\Languages', 'l', 'WITH', 'l = :language')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl1', 'WITH', 'tl1.translatedLanguages = l AND tl1.language = :doctorLanguage')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl2', 'WITH', 'tl2.translatedLanguages = l AND tl2.language = :defaultLanguage')
                        ->where('i.identifier = :identifier')
                        ->setParameters([
                            'doctorLanguage'  => $oLanguage,
                            'identifier'      => $institutionIdentifier,
                            'language'        => $oLanguage,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ])
                        ->setMaxResults(1);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $institutionIdentifier
     *
     * @return array
     */
    public function findInstitutionDescription($institutionIdentifier)
    {
        $aResult = $this->createQueryBuilder('i')
                        ->select('il.areaOfExpertise, il.rangeOfServices, il.extraDetails, il.isPrimaryDescription, COALESCE(tl1.name, tl2.name) as languageName, l.iso')
                        ->innerJoin('AppBundle:Institutions\InstitutionLanguages', 'il', 'WITH', 'il.institutions = i')
                        ->leftJoin('AppBundle:Languages\Languages', 'l', 'WITH', 'l = il.languages')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl1', 'WITH', 'tl1.translatedLanguages = l AND tl1.language = il.languages')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl2', 'WITH', 'tl2.translatedLanguages = l AND tl2.language = il.languages')
                        ->where('i.identifier = :institutionIdentifier')
                        ->setParameters([
                            'institutionIdentifier' => $institutionIdentifier,
                        ])
                        ->getQuery()
                        ->getScalarResult();

        return $aResult;
    }

    /**
     * @param                $institutionIdentifier
     * @param Languages|null $oLanguage
     *
     * @return array
     */
    public function findInstitutionSkills($institutionIdentifier, Languages $oLanguage = null)
    {
        return $this->createQueryBuilder('i')
                    ->select('COALESCE(sl1.name, sl2.name) as skillName', 'ias.id')
                    ->leftJoin('i.institutionSkills', 'ias')
                    ->leftJoin('AppBundle:Skills\SkillLanguages', 'sl1', 'WITH', 'sl1.skills = ias AND sl1.languages = :doctorLanguage')
                    ->leftJoin('AppBundle:Skills\SkillLanguages', 'sl2', 'WITH', 'sl2.skills = ias AND sl2.languages = :defaultLanguage')
                    ->where('i.identifier = :institutionIdentifier')
                    ->setParameters([
                        'institutionIdentifier' => $institutionIdentifier,
                        'doctorLanguage'        => $oLanguage,
                        'defaultLanguage'       => $this->selectPrimaryLanguageDQL(),
                    ])
                    ->getQuery()
                    ->getResult();
    }

    /**
     * @param array|null $aFilters Filters will be used to filter the results based on parameters passed from the url
     * @param bool       $bQueryResult If true then display the hydrated results
     *
     * @return \Doctrine\ORM\QueryBuilder|array
     */
    public function findInstitutions(array $aFilters = null, $bQueryResult = false)
    {
        $sDql = $this->createQueryBuilder('i');

        if ($bQueryResult) {
            return $sDql->getQuery()->getResult();
        }

        return $sDql;
    }

    /**
     * @param Languages $oLanguage    The passed Language Object
     * @param bool      $bQueryResult If true then display the hydrated result
     *
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function findInstitutionByUserLanguage(Languages $oLanguage, $bQueryResult = false)
    {
        $sDql = $this->_findAllInstitutionsByCountryUserLanguage($oLanguage);

        if ($bQueryResult) {
            return $sDql->getQuery()->getScalarResult();
        }

        return $sDql;
    }

    /**
     * @param           $identifier
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findInstitutionByUserLanguageAndIdentifier($identifier, Languages $oLanguage)
    {
        $aResult = $this->_findAllInstitutionsByCountryUserLanguage($oLanguage);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function findInstitutionImages($id)
    {
        $aResult = $this->createQueryBuilder('i')
                        ->select('im.id, im.image, im.thumbnail')
                        ->innerJoin('AppBundle:Institutions\InstitutionImages', 'im', 'WITH', 'im.institution = i.id')
                        ->where('i.id = :institutionId')
                        ->setParameter('institutionId', $id)
                        ->getQuery()
                        ->getArrayResult();

        return $aResult;
    }

    /**
     * @inheritdoc
     */
    public function findByLanguage(Languages $oLanguage) { }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function _findAllInstitutionsByCountryUserLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('i')
                    ->select('i.id, i.identifier, i.address, i.name as institutionName, cl.name countryName, citylang.name as cityName')
                    ->leftJoin('AppBundle:Countries\CountrySpokenLanguages', 'csl', 'WITH', 'csl.country = i.institutionCountry AND csl.isPrimary = :isPrimary')
                    ->leftJoin('AppBundle:Countries\CountryLanguages', 'cl', 'WITH', 'cl.countries = i.institutionCountry')
                    ->leftJoin('AppBundle:Cities\CityLanguages', 'citylang', 'WITH', 'citylang.cities = i.institutionCity')
                    ->where('csl.isPrimary = :isPrimary')
                    ->andWhere('citylang.languages = csl.language')
                    ->andWhere('cl.languages = :userLanguage')
                    ->setParameters([
                        'userLanguage' => $oLanguage,
                        'isPrimary'    => true,
                    ])
                    ->groupBy('i, cl.name, citylang');
    }

}
