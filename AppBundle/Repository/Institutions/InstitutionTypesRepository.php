<?php

namespace Flendoc\AppBundle\Repository\Institutions;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class InstitutionTypesRepository
 * @package Flendoc\AppBundle\Repository\Institutions
 */
class InstitutionTypesRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return mixed
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('it')
                    ->select('it.id, it.identifier, COALESCE(itl1.name, itl2.name) as name')
                    ->leftJoin('AppBundle:Institutions\InstitutionTypeLanguages', 'itl1', 'WITH', 'it.id = itl1.institutionTypes AND itl1.languages = :language')
                    ->leftJoin('AppBundle:Institutions\InstitutionTypeLanguages', 'itl2', 'WITH', 'it.id = itl2.institutionTypes AND itl2.languages = :defaultLanguage')
                    ->setParameters([
                        'language'        => $oLanguage,
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ])->getQuery()->getScalarResult();
    }
}
