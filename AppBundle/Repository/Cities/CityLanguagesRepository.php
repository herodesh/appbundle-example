<?php

namespace Flendoc\AppBundle\Repository\Cities;

use Doctrine\ORM\EntityRepository;

/**
 * Class CityLanguagesRepository
 * @package Flendoc\AppBundle\Repository\Cities
 */
class CityLanguagesRepository extends EntityRepository
{

    /**
     * Find City Languages Ids
     *
     * @param $cityId
     *
     * @return array
     */
    public function findCityLanguagesIds($cityId)
    {
        $aResult = $this->createQueryBuilder('cl')
                        ->select('IDENTITY (cl.languages)')
                        ->where('cl.cities = :cityId')
                        ->setParameter('cityId', $cityId);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Find City name for each active language
     *
     * @param int $iCityId
     *
     * @return mixed
     */
    public function findNameByCityId($iCityId)
    {
        $qb = $this->createQueryBuilder('city_languages');

        $qb
            ->select('city_languages.name', 'languages.iso')
            ->innerJoin('city_languages.cities', 'cities')
            ->innerJoin('city_languages.languages', 'languages')
            ->where(
                $qb->expr()->eq('city_languages.cities', ':cityId')
            )
            ->andWhere(
                $qb->expr()->eq('cities.isActive', ':isActive')
            )
            ->setParameters([
                'cityId'   => $iCityId,
                'isActive' => true,
            ])
        ;

        return $qb->getQuery()->getResult();
    }
}
