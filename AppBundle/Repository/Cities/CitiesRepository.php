<?php

namespace Flendoc\AppBundle\Repository\Cities;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\Countries\Countries;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class CitiesRepository
 * @package Flendoc\AppBundle\Repository\Cities
 */
class CitiesRepository extends FlendocRepository
{

    /**
     * Find cities by country main spoken language in json format
     *
     * @param $countryId
     *
     * @return mixed
     */
    public function findCitiesByUserLanguageJson($countryId, $sUserLanguage)
    {
        $aResult = $this->_createQueryBuilder($sUserLanguage);
        $aResult
            ->leftJoin(Countries::class, 'country', 'WITH', 'city.country = country')
            ->andWhere('country.id = :countryId')
            ->setParameter('countryId', $countryId);

        return json_encode($aResult->getQuery()->getResult());
    }

    /**
     * Find Cities by Country and Query String - Search
     *
     * @param string $countryId
     * @param string $queryString
     * @param string $language
     *
     * @return array
     */
    public function findCitiesByCountryAndQueryString($countryId, $language, $queryString)
    {
        $qb = $this->_createQueryBuilder($language);

        $qb
            ->leftJoin(Countries::class, 'country', 'WITH', 'city.country = country')
            ->andWhere('country.id = :countryId')
            ->andWhere('cl1.name LIKE :queryString OR cl2.name LIKE :queryString')
            ->setParameter('countryId', $countryId)
            ->setParameter('queryString', '%'.$queryString.'%')
            ->orderBy('city.id', 'DESC')
            ->setMaxResults(AppConstants::QUERY_MAX_RESULTS)
            ->setParameter('language', $language)
            ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());

        return $qb->getQuery()->getScalarResult();
    }

    /**
     * Create QueryBuilder
     *
     * @param $language
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function _createQueryBuilder($language)
    {
        return $this->createQueryBuilder('city')
                    ->select('city.id, city.identifier, COALESCE(cl1.name, cl2.name) as name')
                    ->leftJoin('city.cityLanguages', 'cl1', 'WITH', 'cl1.languages = :language')
                    ->leftJoin('city.cityLanguages', 'cl2', 'WITH', 'cl2.languages = :defaultLanguage')
                    ->setParameter('language', $language)
                    ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());
    }
}
