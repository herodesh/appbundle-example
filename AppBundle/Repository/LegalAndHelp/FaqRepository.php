<?php

namespace Flendoc\AppBundle\Repository\LegalAndHelp;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroupsTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class FaqRepository
 * @package Flendoc\AppBundle\Repository\LegalAndHelp
 */
class FaqRepository extends FlendocRepository
{

    /**
     * @param Languages $oLanguage
     * @param string    $sFaqIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneFaqByLanguage(Languages $oLanguage, $sFaqIdentifier)
    {
        $aResult = $this->createQueryBuilder('f')
                        ->select('f.id, f.identifier, f.faqIdentifier, f.isActive, f.position, 
                             fl.title, fl.description, fl.slug as slug, fgtl.title as groupTitle, fg.identifier as faqGroupIdentifier')
                        ->leftJoin('f.faqTranslations', 'fl', 'WITH', 'fl.language = :language')
                        ->leftJoin('f.faqGroups', 'fg')
                        ->leftJoin(FaqGroupsTranslations::class, 'fgtl', 'WITH', 'fgtl.faqGroup = f.faqGroups AND fgtl.language = :language')
                        ->andWhere('f.identifier = :faqIdentifier')
                        ->setParameter('language', $oLanguage)
                        ->setParameter('faqIdentifier', $sFaqIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param string    $sFaqStringIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneFaqByLanguageAndStringIdentifier(Languages $oLanguage, $sFaqStringIdentifier)
    {
        $aResult = $this->createQueryBuilder('f')
                        ->select('f.id, f.identifier, f.faqIdentifier, f.isActive, f.position, 
                             fl.title, fl.description, fl.slug as slug, fgtl.title as groupTitle, fg.identifier as faqGroupIdentifier')
                        ->leftJoin('f.faqTranslations', 'fl', 'WITH', 'fl.language = :language')
                        ->leftJoin('f.faqGroups', 'fg')
                        ->leftJoin(FaqGroupsTranslations::class, 'fgtl', 'WITH', 'fgtl.faqGroup = f.faqGroups AND fgtl.language = :language')
                        ->andWhere('f.faqIdentifier = :faqStringIdentifier')
                        ->setParameter('language', $oLanguage)
                        ->setParameter('faqStringIdentifier', $sFaqStringIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param string    $sFaqGroupIdentifier
     *
     * @return mixed
     */
    public function findByFaqGroupLanguageAndIdentifier(Languages $oLanguage, $sFaqGroupIdentifier)
    {
        $aResult = $this->createQueryBuilder('f')
                        ->select('f.id, f.identifier, f.faqIdentifier, f.isActive, f.position, 
                             fl.title, fl.description, fl.slug, (:languageIso) as languageIso')
                        ->leftJoin('f.faqTranslations', 'fl', 'WITH', 'fl.language = :language')
                        ->leftJoin('f.faqGroups', 'fg')
                        ->andWhere('fg.identifier = :sFaqGroupIdentifier')
                        ->setParameter('sFaqGroupIdentifier', $sFaqGroupIdentifier)
                        ->setParameter('language', $oLanguage)
                        ->setParameter('languageIso', $oLanguage->getIso());

        return $aResult->getQuery()->getResult();
    }

    /**
     * Find Faq by Language
     *
     * @param Languages $oLanguage
     * @param bool      $bActive
     *
     * @return array
     */
    public function findFaqByLanguage(Languages $oLanguage, $bActive = false)
    {
        $aResult = $this->findByLanguage($oLanguage);

        if ($bActive) {
            $aResult->where('f.isActive = :isActive')
                    ->setParameter('isActive', true);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastFaqByOrderNumber()
    {
        $aResult = $this->createQueryBuilder('f')
                        ->select('MAX(f.position)');

        return $aResult->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $aResult = $this->createQueryBuilder('f')
                        ->select('f.id, f.identifier, f.faqIdentifier, f.isActive, f.position, 
                             COALESCE(fl1.title, fl2.title) as title,  
                             COALESCE(fl1.description, fl2.description) as description,
                             COALESCE(fl1.slug, fl2.slug) as slug,                     
                             COALESCE(fgtl1.title, fgtl2.title) as groupTitle')
                        ->leftJoin('f.faqTranslations', 'fl1', 'WITH', 'fl1.language = :language')
                        ->leftJoin('f.faqTranslations', 'fl2', 'WITH', 'fl2.language = :defaultLanguage')
                        ->leftJoin(FaqGroupsTranslations::class, 'fgtl1', 'WITH', 'fgtl1.faqGroup = f.faqGroups AND fgtl1.language = :language')
                        ->leftJoin(FaqGroupsTranslations::class, 'fgtl2', 'WITH', 'fgtl2.faqGroup = f.faqGroups AND fgtl2.language = :defaultLanguage')
                        ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL())
                        ->setParameter('language', $oLanguage);

        return $aResult;
    }
}
