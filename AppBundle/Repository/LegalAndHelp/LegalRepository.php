<?php

namespace Flendoc\AppBundle\Repository\LegalAndHelp;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\LegalTranslations;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class LegalRepository
 * @package Flendoc\AppBundle\Repository\LegalAndHelp
 */
class LegalRepository extends FlendocRepository
{
    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $aResult = $this->createQueryBuilder('l')
                        ->select('l.id, l.identifier, l.legalIdentifier, 
                             COALESCE(lt1.title, lt2.title) as title, 
                             COALESCE(lt1.description, lt2.description) as description')
                        ->leftJoin(LegalTranslations::class, 'lt1', 'WITH', 'lt1.legal = l AND lt1.language = :language')
                        ->leftJoin(LegalTranslations::class, 'lt2', 'WITH', 'lt2.legal = l AND lt2.language = :defaultLanguage')
                        ->setParameters([
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                            'language'        => $oLanguage,
                        ]);

        return $aResult->getQuery()->getScalarResult();
    }
}
