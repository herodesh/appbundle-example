<?php

namespace Flendoc\AppBundle\Repository\LegalAndHelp;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUsTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class ContactUsRepository
 * @package Flendoc\AppBundle\Repository\LegalAndHelp
 */
class ContactUsRepository extends FlendocRepository
{

    /**
     * @param Languages $oLanguage
     * @param string    $sContactUsIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneContactUsByLanguage(Languages $oLanguage, $sContactUsIdentifier)
    {
        $aResult = $this->findByLanguage($oLanguage);

        $aResult->andWhere('cu.contactUsIdentifier = :contactUsIdentifier')
                ->setParameter('contactUsIdentifier', $sContactUsIdentifier)
                ->setMaxResults(1);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * Find Faq by Language
     *
     * @param Languages $oLanguage
     * @param bool      $bActive
     *
     * @return array
     */
    public function findContactUsByLanguage(Languages $oLanguage, $bActive = false)
    {
        $aResult = $this->findByLanguage($oLanguage);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $aResult = $this->createQueryBuilder('cu')
                        ->select('cu.id, cu.identifier, cu.contactUsIdentifier,  
                             COALESCE(cul1.title, cul2.title) as title')
                        ->leftJoin('cu.contactUsTranslations', 'cul1', 'WITH', 'cul1.language = :language')
                        ->leftJoin('cu.contactUsTranslations', 'cul2', 'WITH', 'cul2.language = :defaultLanguage')
                        ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL())
                        ->setParameter('language', $oLanguage);

        return $aResult;
    }
}
