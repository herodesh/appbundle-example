<?php

namespace Flendoc\AppBundle\Repository\LegalAndHelp;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroupsTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class FaqGroupRepository
 * @package Flendoc\AppBundle\Repository\LegalAndHelp
 */
class FaqGroupsRepository extends FlendocRepository
{

    /**
     * @param Languages $oLanguage
     * @param string    $sFaqGroupIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneFaqGroupByLanguage(Languages $oLanguage, $sFaqGroupIdentifier)
    {

        $aResult = $this->createQueryBuilder('fg')
                        ->select('fg.id, fg.identifier, fg.faqGroupIdentifier, fg.isActive, fgt.title, fgt.slug,
                        (:languageIso) as languageIso')
                        ->leftJoin('fg.faqGroupTranslations', 'fgt', 'WITH', 'fgt.language = :language')
                        ->andWhere('fg.identifier = :faqGroupIdentifier')
                        ->setParameter('language', $oLanguage)
                        ->setParameter('languageIso', $oLanguage->getIso())
                        ->setParameter('faqGroupIdentifier', $sFaqGroupIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * Find Faq by Language
     *
     * @param Languages $oLanguage
     * @param bool      $bActive
     *
     * @return array
     */
    public function findFaqGroupsByExactLanguage(Languages $oLanguage, $bActive = false)
    {
        $aResult = $this->createQueryBuilder('fg')
                        ->select('fg.id, fg.identifier, fg.faqGroupIdentifier, fg.isActive, fgt.title, fgt.slug,
                        (:languageIso) as languageIso')
                        ->leftJoin('fg.faqGroupTranslations', 'fgt', 'WITH', 'fgt.language = :language')
                        ->setParameter('language', $oLanguage)
                        ->setParameter('languageIso', $oLanguage->getIso());

        if ($bActive) {
            $aResult->where('fg.isActive = :isActive')
                    ->setParameter('isActive', true);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Find Faq by Language
     *
     * @param Languages $oLanguage
     * @param bool      $bActive
     *
     * @return array
     */
    public function findFaqGroupsByLanguage(Languages $oLanguage, $bActive = false)
    {
        $aResult = $this->findByLanguage($oLanguage);

        if ($bActive) {
            $aResult->where('fg.isActive = :isActive')
                    ->setParameter('isActive', true);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastFaqGroupByOrderNumber()
    {
        $aResult = $this->createQueryBuilder('fg')
                        ->select('MAX(fg.position)');

        return $aResult->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $aResult = $this->createQueryBuilder('fg')
                        ->select('fg.id, fg.identifier, fg.faqGroupIdentifier, fg.isActive, 
                             COALESCE(fgl1.title, fgl2.title) as title,
                             COALESCE(fgl1.slug, fgl2.slug) as slug'
                        )
                        ->leftJoin('fg.faqGroupTranslations', 'fgl1', 'WITH', 'fgl1.language = :language')
                        ->leftJoin('fg.faqGroupTranslations', 'fgl2', 'WITH', 'fgl2.language = :defaultLanguage')
                        ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL())
                        ->setParameter('language', $oLanguage);

        return $aResult;
    }
}
