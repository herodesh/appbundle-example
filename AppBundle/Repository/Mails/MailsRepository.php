<?php

namespace Flendoc\AppBundle\Repository\Mails;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class MailsRepository
 * @package Flendoc\AppBundle\Repository\Mails
 */
class MailsRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('m')
                    ->select('m.id, m.variables, m.subjectVariables, m.isActive, m.isAdministrative, m.isAccount, m.emailIdentifier, m.isExternal, 
                             COALESCE(ml1.subject, ml2.subject) as subject, 
                             COALESCE(ml1.content, ml2.content) as content')
                    ->leftJoin('AppBundle:Mails\MailLanguages', 'ml1', 'WITH', 'ml1.mail = m.id AND ml1.languages = :language')
                    ->leftJoin('AppBundle:Mails\MailLanguages', 'ml2', 'WITH', 'ml2.mail = m.id AND ml2.languages = :defaultLanguage')
                    ->setParameters([
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        'language'        => $oLanguage,
                    ]);
    }

    /**
     * Find emails by User spoken Languages
     *
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findMailsByLanguage(Languages $oLanguage)
    {
        $aResult = $this->findByLanguage($oLanguage);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param $sEmailIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneEmailByEmailIdentifier($sEmailIdentifier)
    {
        $aResult = $this->createQueryBuilder('m')
                        ->select('m.id, m.emailIdentifier, m.isActive, m.isAccount, m.isAdministrative, m.isExternal')
                        ->where('m.emailIdentifier = :emailIdentifier')
                        ->setParameter('emailIdentifier', $sEmailIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param           $sEmailIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByLanguageAndEmailIdentifier(Languages $oLanguage, $sEmailIdentifier)
    {
        $aResult = $this->findByLanguage($oLanguage);
        $aResult->andWhere('m.emailIdentifier = :emailIdentifier')
                ->setParameter('emailIdentifier', $sEmailIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * Find Emails
     *
     * @param array|null $aProperties
     *
     * @return array
     */
    public function findMails(array $aProperties = null)
    {
        $aWhereParams = [];

        $aResultQueryBuilder = $this->createQueryBuilder('m')
                                    ->select('m.id, m.emailIdentifier, m.isAdministrative, m.isAccount, m.isPatient');

        if (null != $aProperties) {
            foreach ($aProperties as $sPropertyKey => $aProperty) {
                $aResultQueryBuilder->andWhere('m.'.$sPropertyKey.' = :'.$sPropertyKey);
                $aWhereParams[$sPropertyKey] = $aProperty;
            }

            $aResultQueryBuilder->setParameters($aWhereParams);
        }

        return $aResultQueryBuilder->getQuery()->getScalarResult();
    }
}
