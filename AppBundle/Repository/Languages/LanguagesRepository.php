<?php

namespace Flendoc\AppBundle\Repository\Languages;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class LanguagesRepository
 * @package Flendoc\AppBundle\Repository\Languages
 */
class LanguagesRepository extends FlendocRepository implements RepositoryInterface
{

    /**
     * @param Languages $oLanguages
     *
     * @return array
     */
    public function findActiveLanguagesByUserLanguage(Languages $oLanguages)
    {
        $aResult = $this->findByLanguage($oLanguages);

        $aResult->andWhere('l.active = :active')
                ->setParameter('active', true);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguages
     * @param null      $sQueryString
     *
     * @return array
     */
    public function findAllByParametersAndPrimaryLanguage(Languages $oLanguages, $sQueryString = null)
    {
        $aResult = $this->findByLanguage($oLanguages);

        if (null != $sQueryString) {
            $aResult
                ->select('COALESCE(IDENTITY(tl1.language), IDENTITY(tl2.language)) as id, COALESCE(tl1.name, tl2.name) as name')
                ->andWhere('COALESCE(tl1.name, tl2.name) LIKE :sQueryString')
                ->setParameter('sQueryString', '%'.$sQueryString.'%')
                ->groupBy('name, id')
                ->setMaxResults(AppConstants::QUERY_MAX_RESULTS);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguages
     *
     * @return array
     */
    public function findLanguageIdIsoAndName(Languages $oLanguages)
    {
        $aResult = $this->findByLanguage($oLanguages)
                        ->select('l.id, l.iso, COALESCE(tl1.name, tl2.name) as name')
                        ->getQuery()
                        ->getScalarResult();

        return $aResult;
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('l')
                    ->select('
                        l.id, l.identifier, l.iso, l.active, l.isPrimary,
                        COALESCE(tl1.name, tl2.name) as name
                     ')
                    ->leftJoin('l.translatedLanguages', 'tl1', 'WITH', 'tl1.translatedLanguages = :language')
                    ->leftJoin('l.translatedLanguages', 'tl2', 'WITH', 'tl2.translatedLanguages = :defaultLanguage')
                    ->setParameter('language', $oLanguage)
                    ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());
    }

}
