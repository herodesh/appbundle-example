<?php

namespace Flendoc\AppBundle\Repository\Resumes;

use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Resumes\Resumes;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ResumesRepository
 * @package Flendoc\AppBundle\Repository\Resumes
 */
class ResumesRepository extends FlendocRepository implements RepositoryInterface
{
    /**
     * @param         $resumeIdentifier
     * @param Resumes $oResume
     *
     * @return array|null
     */
    public function findCurrentResumeSections($resumeIdentifier, Resumes $oResume = null)
    {
        if (null == $oResume) {
            return null;
        }

        $aResult = $this->createQueryBuilder('r')
                        ->select('r.id as resume_id, 
                            IDENTITY(r.doctor) as doctor_id, 
                            COALESCE(IDENTITY(rsl1.languages), IDENTITY(rsl2.languages)) as resumeLanguage, 
                            r.name as resume_name, 
                            rs.sectionName as resume_section_name, 
                            rs.id as resume_section_id, 
                            COALESCE(rsl1.name, rsl2.name) as resume_section_language_name,
                            COALESCE(l1.iso, l2.iso) as language_iso
                        ')
                        ->leftJoin('AppBundle:Resumes\ResumeSectionLanguages', 'rsl1', 'WITH', 'rsl1.languages = :resumeLanguage')
                        ->leftJoin('AppBundle:Resumes\ResumeSectionLanguages', 'rsl2', 'WITH', 'rsl2.languages = :defaultLanguage')
                        ->leftJoin('AppBundle:Resumes\ResumeSections', 'rs', 'WITH', '(rsl1.resumeSections = rs AND rsl2.resumeSections = rs) OR (rsl2.resumeSections IS NULL AND rsl1.resumeSections = rs) OR (rsl2.resumeSections = rs AND rsl1.resumeSections IS NULL)')
                        ->leftJoin('AppBundle:Languages\Languages', 'l1', 'WITH', 'l1 = rsl1.languages')
                        ->leftJoin('AppBundle:Languages\Languages', 'l2', 'WITH', 'l2 = rsl2.languages')
                        ->where('r.identifier = :identifier')
                        ->andWhere('rs.id IS NOT NULL')
                        ->andWhere('rs = rsl1.resumeSections OR rs = rsl2.resumeSections')
                        ->setParameters([
                            'resumeLanguage'  => $oResume->getLanguages(),
                            'identifier'      => $resumeIdentifier,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ])
                        ->orderBy('rs.orderNumber', 'ASC')
                        ->getQuery()
                        ->getResult();

        return $aResult;
    }

    /**
     * @param Doctors $oUser
     *
     * @param Doctors $oUser
     * @param         $identifier
     *
     * @return array
     */
    public function findDoctorResumes(Doctors $oUser, $identifier = null)
    {
        $aResult = $this->createQueryBuilder('r')
                        ->select('r.id, r.identifier, r.name, r.isPrimary, l.iso')
                        ->leftJoin('AppBundle:Doctors\Doctors', 'd', 'WITH', 'r.doctor = d.id')
                        ->leftJoin('AppBundle:Languages\Languages', 'l', 'WITH', 'r.languages = l.id')
                        ->where('r.doctor = :doctor')
                        ->setParameter('doctor', $oUser);

        if (null != $identifier) {
            $aResult->andWhere('r.identifier = :identifier')
                    ->setParameter('identifier', $identifier);
        }

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return string
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return 'true';
        // TODO: Implement findByLanguage() method.
    }
}
