<?php

namespace Flendoc\AppBundle\Repository\Resumes;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ResumeSectionLanguagesRepository
 * @package Flendoc\AppBundle\Repository\Resumes
 */
class ResumeSectionLanguagesRepository extends FlendocRepository  implements RepositoryInterface
{
    /**
     * @param Languages $oLanguage
     *
     * @return string
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return 'true';
        // TODO: Implement findByLanguage() method.
    }
}
