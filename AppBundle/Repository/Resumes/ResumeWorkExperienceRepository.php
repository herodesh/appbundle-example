<?php

namespace Flendoc\AppBundle\Repository\Resumes;

use Flendoc\AppBundle\Entity\Resumes\Resumes;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class ResumeWorkExperienceRepository
 * @package Flendoc\AppBundle\Repository\Resumes
 */
class ResumeWorkExperienceRepository extends FlendocRepository
{
    /**
     * @param Resumes $oResume
     *
     * @return mixed
     */
    public function findCurrentJob(Resumes $oResume)
    {
        return $this->createQueryBuilder('we')
                    ->select('we.jobTitle, we.institutionName')
                    ->where('we.isCurrent = :isCurrentJob')
                    ->andWhere('we.resume = :resume')
                    ->setParameters([
                        'isCurrentJob' => true,
                        'resume'       => $oResume,
                    ])
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}
