<?php

namespace Flendoc\AppBundle\Repository\Resumes;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ResumeSectionsRepository
 * @package Flendoc\AppBundle\Repository\Resumes
 */
class ResumeSectionsRepository extends FlendocRepository implements RepositoryInterface
{

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findResumeSectionsByUserLanguage(Languages $oLanguage)
    {
        $aResult = $this->findByLanguage($oLanguage);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        return $this->createQueryBuilder('rs')
                    ->select('rs.id, rs.isSecondary, COALESCE(rsl1.name, rsl2.name) as name')
                    ->leftJoin('AppBundle:Resumes\ResumeSectionLanguages', 'rsl1', 'WITH', 'rs = rsl1.resumeSections AND rsl1.languages = :language')
                    ->leftJoin('AppBundle:Resumes\ResumeSectionLanguages', 'rsl2', 'WITH', 'rs = rsl2.resumeSections AND rsl2.languages = :defaultLanguage')
                    ->setParameters([
                            'language'        => $oLanguage,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ]
                    );
    }
}
