<?php

namespace Flendoc\AppBundle\Repository\Resumes;

use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;
use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Class ResumeEducationDiplomaTypeRepository
 * @package Flendoc\AppBundle\Repository\Resumes
 */
class ResumeEducationDiplomaTypeRepository extends FlendocRepository implements RepositoryInterface
{

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findResumeEducationDiplomaTypeByUserLanguage(Languages $oLanguage)
    {
        $aResult = $this->findByLanguage($oLanguage);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $oQueryBuilder = $this->createQueryBuilder('redt')
                              ->select('redt.id, COALESCE(redtl1.name, redtl2.name) as name')
                              ->leftJoin('AppBundle:Resumes\ResumeEducationDiplomaTypeLanguages', 'redtl1', 'WITH', 'redt = redtl1.resumeEducationDiplomaType AND redtl1.languages = :language')
                              ->leftJoin('AppBundle:Resumes\ResumeEducationDiplomaTypeLanguages', 'redtl2', 'WITH', 'redt = redtl2.resumeEducationDiplomaType AND redtl2.languages = :defaultLanguage')
                              ->setParameters([
                                  'language'        => $oLanguage,
                                  'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                              ]);

        return $oQueryBuilder;
    }
}
