<?php

namespace Flendoc\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class FlendocRepository
 * @package Flendoc\AppBundle\Repository
 */
class FlendocRepository extends EntityRepository
{
    /**
     * Select primary language DQL
     *
     * @return mixed
     */
    public function selectPrimaryLanguageDQL()
    {
        $oLanguageQuery = $this->_em->createQuery('SELECT pLang FROM AppBundle:Languages\Languages AS pLang WHERE pLang.isPrimary = true');

        return $oLanguageQuery->getOneOrNullResult();
    }
}
