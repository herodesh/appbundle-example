<?php

namespace Flendoc\AppBundle\Repository\Chats;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class DoctorChatRepository
 * @package Flendoc\AppBundle\Repository
 */
class DoctorChatRepository extends EntityRepository
{
    /**
     * Get chats for a specified user.
     * If peerUserId is defined will return the private chat between currentUser and peerUser.
     *
     * @param $currentUserId
     * @param $peerUserId
     * @param bool $isChatGroup
     * @param bool $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findChatsByParticipants($currentUserId, $peerUserId, $isChatGroup = null, $bQueryResult = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat');
        $innerQb = $this->createQueryBuilder('doctor_chat2');

        $innerQb = $innerQb
            ->select('doctor_chat2.id')
            ->innerJoin('doctor_chat2.doctorChatMembers', 'doctor_chat_members2')
            ->where($innerQb->expr()->eq('doctor_chat_members2.doctor', $currentUserId))
        ;

        $qb
            ->select('doctor_chat.id as chat_id, doctor_chat.identifier as chat_identifier')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->innerJoin('doctor_chat_members.doctor', 'doctor')
            ->where(
                $qb->expr()->in('doctor_chat.id', $innerQb->getDQL())
            )
            ->andWhere(
                $qb->expr()->eq('doctor.id', $peerUserId)
            );
        ;

        if (isset($isChatGroup)) {
            $qb
                ->andWhere(
                    $qb->expr()->eq('doctor_chat.isGroup', ':isGroup')
                )
                ->setParameter('isGroup', $isChatGroup);
        }

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors $oUser
     * @param bool $bArchivedChat
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function findByUser($oUser, $bArchivedChat = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat');

        $qb
            ->select('doctor_chat.id, doctor_chat.isGroup, doctor_chat.lastMessageAt, doctor_chat_members.softDeleteAt, doctor_chat_members.leftChat, doctor_chat_members.leftChatAt, doctor_chat_members.peerDeletedAccount, doctor_chat_members.archivedChat, doctor_chat_members.archivedChatName')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->where('doctor_chat_members.doctor = :userId')
            ->andWhere($qb->expr()->eq('doctor_chat_members.archivedChat', ':archivedChat'))
            ->andWhere($qb->expr()->eq('doctor_chat_members.deletedChat', ':deletedChat'))
            ->setParameter('deletedChat', false)
            ->setParameter('archivedChat', $bArchivedChat)
            ->setParameter('userId', $oUser)
        ;

        if ($bArchivedChat) {
            $qb->orderBy('doctor_chat_members.archivedChatAt', 'DESC');
        } else {
            $qb->orderBy('doctor_chat.lastMessageAt', 'DESC');
        }

        return $qb;
    }

    /**
     * @param Doctors $oUser
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllByUser($oUser)
    {
        $qb = $this->createQueryBuilder('doctor_chat');

        $qb
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->where('doctor_chat_members.doctor = :userId')
            ->setParameter('userId', $oUser)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Doctors $oUser
     * @param bool    $bArchivedChat
     * @param integer $iLimit
     * @param bool    $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByUserAndLimit(
        $oUser,
        $bArchivedChat = false,
        $iLimit,
        $bQueryResult = false
    ) {
        $qb = $this->findByUser($oUser, $bArchivedChat);
        $qb->setMaxResults($iLimit);

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors $oUser
     * @param string  $sChatIdentifier
     * @param bool    $bArchivedChat
     * @param bool    $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByUserAndChatIdentifier(
        $oUser,
        $sChatIdentifier,
        $bArchivedChat,
        $bQueryResult
    ) {
        $qb = $this->findByUser($oUser, $bArchivedChat);
        $qb
            ->andWhere($qb->expr()->eq('doctor_chat.identifier', ':identifier'))
            ->setParameter('identifier', $sChatIdentifier)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors $oUser
     * @param integer $iActiveChatId
     * @param bool    $bArchivedChat
     * @param integer $iLimit
     * @param bool    $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByUserAndActiveChatId(
        $oUser,
        $iActiveChatId,
        $bArchivedChat = false,
        $iLimit,
        $bQueryResult = false
    ) {
        $qb = $this->findByUser($oUser, $bArchivedChat);
        $qb
            ->andWhere($qb->expr()->neq('doctor_chat.id', $iActiveChatId))
            ->setMaxResults($iLimit)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors   $oUser
     * @param \DateTime $oLastMessageAt
     * @param integer   $iLimit
     * @param string    $sActiveChatIdentifier
     * @param bool      $bArchivedChat
     * @param bool      $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByUserAndLastMessageAt(
        $oUser,
        \DateTime $oLastMessageAt,
        $iLimit,
        $sActiveChatIdentifier = null,
        $bArchivedChat = false,
        $bQueryResult = false
    ) {
        $qb = $this->findByUser($oUser, $bArchivedChat);

        if ($sActiveChatIdentifier) {
            $qb
                ->andWhere($qb->expr()->neq('doctor_chat.identifier', ':activeChatIdentifier'))
                ->setParameter('activeChatIdentifier', $sActiveChatIdentifier)
            ;
        }

        $qb
            ->andWhere($qb->expr()->lt('doctor_chat.lastMessageAt', ':lastMessageAt'))
            ->setParameter('lastMessageAt', $oLastMessageAt)
            ->setMaxResults($iLimit)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors   $oUser
     * @param \DateTime $oArchivedChatAt
     * @param bool      $bArchivedChat
     * @param integer   $iLimit
     * @param bool      $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByUserAndArchivedChatAt(
        $oUser,
        \DateTime $oArchivedChatAt,
        $bArchivedChat = false,
        $iLimit,
        $bQueryResult = false
    ) {
        $qb = $this->findByUser($oUser, $bArchivedChat);
        $qb
            ->andWhere($qb->expr()->lt('doctor_chat_members.archivedChatAt', ':archivedChatAt'))
            ->setParameter('archivedChatAt', $oArchivedChatAt)
            ->setMaxResults($iLimit)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param integer   $iCurrentUserId
     * @param \DateTime $oChatLastMessageAt
     * @param integer   $oChatId
     * @param string    $sSearchKey
     * @param integer   $iLimit
     * @param bool      $bArchivedChat
     * @param bool      $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findChatsBySearchKey(
        $iCurrentUserId,
        $oChatLastMessageAt,
        $oChatId,
        $sSearchKey,
        $iLimit,
        $bArchivedChat,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat');
        $innerQb = $this->createQueryBuilder('doctor_chat2');

        $innerQb = $innerQb
            ->select('doctor_chat2.id')
            ->innerJoin('doctor_chat2.doctorChatMembers', 'doctor_chat_members2')
            ->where($innerQb->expr()->eq('doctor_chat_members2.doctor', $iCurrentUserId))
            ->andWhere($innerQb->expr()->eq('doctor_chat_members2.deletedChat', ':deletedChat'))
            ->andWhere($innerQb->expr()->eq('doctor_chat_members2.archivedChat', ':archivedChat'))
        ;

        $qb
            ->select('doctor_chat.id')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->innerJoin('doctor_chat_members.doctor', 'doctor')
            ->innerJoin('doctor.doctorProfile', 'doctor_profile')
            ->where(
                $qb->expr()->in('doctor_chat.id', $innerQb->getDQL())
            )
            ->andWhere(
                $qb->expr()->neq('doctor_chat_members.doctor', $iCurrentUserId)
            )
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('LOWER(doctor_chat.name)', $qb->expr()->literal('%'.$sSearchKey.'%')),
                    $qb->expr()->like(
                        'LOWER(CONCAT(doctor_profile.firstName, \' \',doctor_profile.lastName))',
                        $qb->expr()->literal('%'.$sSearchKey.'%')
                    )
                )
            )
            ->setParameter('deletedChat', false)
            ->setParameter('archivedChat', $bArchivedChat)
        ;

        if ($oChatLastMessageAt) {
            $qb
                ->andWhere(
                    $qb->expr()->lte('doctor_chat.lastMessageAt', ':date')
                )
                ->andWhere(
                    $qb->expr()->neq('doctor_chat.id', ':chatId')
                )
                ->setParameter('date', $oChatLastMessageAt)
                ->setParameter('chatId', $oChatId)
            ;
        }

        $qb
            ->groupBy('doctor_chat.id')
            ->orderBy('doctor_chat.lastMessageAt', 'DESC')
            ->setMaxResults($iLimit);
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param integer $iCurrentUserId
     * @param string  $sChatsIds
     * @param bool    $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findChatsDetails(
        $iCurrentUserId,
        $sChatsIds,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat');

        $qb
            ->select('doctor_chat.id, doctor_chat.isGroup, doctor_chat.lastMessageAt, doctor_chat_members.softDeleteAt, doctor_chat_members.leftChat, doctor_chat_members.peerDeletedAccount, doctor_chat_members.leftChatAt')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->where($qb->expr()->eq('doctor_chat_members.doctor', $iCurrentUserId))
            ->andWhere($qb->expr()->in('doctor_chat.id', $sChatsIds))
            ->orderBy('doctor_chat.lastMessageAt', 'DESC')
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param int   $iCurrentUserId
     * @param array $aDoctorChatsIds
     * @param bool  $isGroup
     * @param bool  $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findChatsByType(
        $iCurrentUserId,
        $aDoctorChatsIds,
        $isGroup = false,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat');

        $qb
            ->select('doctor_chat.id as doctorChatId, doctor.identifier as doctorIdentifier, CONCAT(doctor_profile.firstName, \' \', doctor_profile.lastName) as doctorName')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->innerJoin('doctor_chat_members.doctor', 'doctor')
            ->innerJoin('doctor.doctorProfile', 'doctor_profile')
            ->where(
                $qb->expr()->in('doctor_chat.id', ':doctorsChatsIds')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat.isGroup', ':isGroup')
            )
            ->andWhere(
                $qb->expr()->neq('doctor_chat_members.doctor', ':userId')
            )
            ->andWhere(
                $qb->expr()->eq('doctor.enabled', ':doctorEnabled')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_members.leftChat', ':leftChat')
            )
            ->setParameter('doctorsChatsIds', $aDoctorChatsIds)
            ->setParameter('userId', $iCurrentUserId)
            ->setParameter('isGroup', $isGroup)
            ->setParameter('doctorEnabled', true)
            ->setParameter('leftChat', false)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param array $aDoctorChatsIds
     * @param bool  $bIsGroup
     * @param bool  $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findNumberOfChatsMembers(
        $aDoctorChatsIds,
        $bIsGroup = true,
        $bQueryResult = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat');

        $qb
            ->select('doctor_chat.id as doctorChatId, COUNT(doctor_chat_members.id) as numberOfChatMembers')
            ->innerJoin('doctor_chat.doctorChatMembers', 'doctor_chat_members')
            ->where(
                $qb->expr()->in('doctor_chat.id', ':doctorsChatsIds')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat.isGroup', ':isGroup')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_members.leftChat', ':leftChat')
            )
            ->groupBy('doctor_chat.id')
            ->setParameter('doctorsChatsIds', $aDoctorChatsIds)
            ->setParameter('isGroup', $bIsGroup)
            ->setParameter('leftChat', false)
        ;

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }
}
