<?php

namespace Flendoc\AppBundle\Repository\Chats;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\Users\AbstractUser;

/**
 * Class DoctorChatMemberRepository
 * @package Flendoc\AppBundle\Repository
 */
class DoctorChatMemberRepository extends EntityRepository
{
    /**
     * Get chat members by chat identifier.
     * Exclude current user.
     *
     * @param string $sChatIdentifier
     * @param int    $iLanguageId
     * @param bool   $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findMembersByChat(
        $sChatIdentifier,
        $iLanguageId,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('doctor.identifier, doctor.isVerified, doctor_chat_member.isGroupAdmin, doctor_chat_member.leftChat, doctor_chat_member.doctorAccountDeleted, doctor_chat_member.identifier as chatMemberIdentifier, doctor_profile.firstName, doctor_profile.lastName, doctor_profile.academicTitle, country_languages.name as countryName, city_languages.name as cityName')
            ->innerJoin('doctor_chat_member.doctorChat', 'doctor_chat')
            ->innerJoin('doctor_chat_member.doctor', 'doctor')
            ->innerJoin('doctor.doctorProfile', 'doctor_profile')
            ->leftJoin('doctor_profile.country', 'country')
            ->leftJoin('country.countryLanguages', 'country_languages')
            ->leftJoin('doctor_profile.city', 'city')
            ->leftJoin('city.cityLanguages', 'city_languages')
            ->where(
                $qb->expr()->eq('doctor_chat.identifier', ':doctorChatIdentifier')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.leftChat', ':leftChat')
            )
            ->andWhere(
                $qb->expr()->eq('doctor.enabled', true)
            )
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('country_languages.languages', $iLanguageId),
                    $qb->expr()->isNull('country_languages.languages')
                )
            )
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('city_languages.languages', $iLanguageId),
                    $qb->expr()->isNull('city_languages.languages')
                )
            )
            ->setParameter('doctorChatIdentifier', $sChatIdentifier)
            ->setParameter('leftChat', false);

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * Get chat members ids by chat identifier.
     * Exclude current user.
     *
     * @param string   $sChatIdentifier
     * @param int|null $iCurrentUserId
     * @param bool     $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findMembersIdsByChat(
        $sChatIdentifier,
        $iCurrentUserId = null,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('doctor.id, doctor.identifier, doctor_chat_member.leftChat')
            ->innerJoin('doctor_chat_member.doctorChat', 'doctor_chat')
            ->innerJoin('doctor_chat_member.doctor', 'doctor')
            ->where(
                $qb->expr()->eq('doctor_chat.identifier', ':doctorChatIdentifier')
            )
            ->andWhere(
                $qb->expr()->eq('doctor.enabled', true)
            )
            ->setParameter('doctorChatIdentifier', $sChatIdentifier);

        if ($iCurrentUserId) {
            $qb
                ->andWhere(
                    $qb->expr()->neq('doctor_chat_member.doctor', ':userId')
                )
                ->setParameter('userId', $iCurrentUserId);
        }

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * Get chat members identifiers by chat identifier.
     * Exclude current user.
     *
     * @param string $sChatIdentifier
     * @param int    $iCurrentUserId
     * @param bool   $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findMembersIdsAndIdentifiersByChat(
        $sChatIdentifier,
        $iCurrentUserId,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('doctor.id, doctor.identifier')
            ->innerJoin('doctor_chat_member.doctorChat', 'doctor_chat')
            ->innerJoin('doctor_chat_member.doctor', 'doctor')
            ->where(
                $qb->expr()->eq('doctor_chat.identifier', ':doctorChatIdentifier')
            )
            ->andWhere(
                $qb->expr()->neq('doctor_chat_member.doctor', $iCurrentUserId)
            )
            ->andWhere(
                $qb->expr()->eq('doctor.enabled', ':doctorEnabled')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.leftChat', ':doctorLeftChat')
            )
            ->setParameters([
                'doctorChatIdentifier' => $sChatIdentifier,
                'doctorLeftChat'       => false,
                'doctorEnabled'        => true,
            ]);

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * Get the number of admins from selected chat.
     *
     * @param $iChatId
     * @param $iCurrentUserId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNumberOfAdmins($iChatId, $iCurrentUserId = null)
    {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('count(doctor_chat_member.id)')
            ->where(
                $qb->expr()->eq('doctor_chat_member.doctorChat', $iChatId)
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.leftChat', ':leftChat')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.isGroupAdmin', true)
            );

        if (null != $iCurrentUserId) {
            $qb->andWhere(
                $qb->expr()->neq('doctor_chat_member.doctor', $iCurrentUserId)
            );
        }

        $qb->setParameter('leftChat', false);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get the number of active group members.
     *
     * @param $iChatId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNumberOfActiveGroupMembers($iChatId)
    {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('count(doctor_chat_member.id)')
            ->where(
                $qb->expr()->eq('doctor_chat_member.doctorChat', $iChatId)
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.leftChat', ':leftChat')
            )
            ->setParameter('leftChat', false);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int  $iDoctorChatId
     * @param bool $bDeletedChat
     * @param bool $bDeletedAccount
     *
     * @return mixed
     */
    public function findByDeletedChatAndDeletedAccount($iDoctorChatId, $bDeletedChat = false, $bDeletedAccount = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->where(
                $qb->expr()->eq('doctor_chat_member.doctorChat', ':doctorChatId')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.deletedChat', ':deletedChat')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.doctorAccountDeleted', ':doctorAccountDeleted')
            )
            ->setParameters([
                'doctorChatId'         => $iDoctorChatId,
                'deletedChat'          => $bDeletedChat,
                'doctorAccountDeleted' => $bDeletedAccount,
            ])
            ->setParameter('deletedChat', false);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find offline users identifiers for a specific DoctorChat.
     *
     * @param array  $aOnlineUsersIds
     * @param string $sDoctorChatIdentifier
     *
     * @return mixed
     */
    public function findOfflineUsersIdentifiers($aOnlineUsersIds, $sDoctorChatIdentifier)
    {
        $qb = $this->createQueryBuilder('doctor_chat_member');

        $qb
            ->select('doctor.identifier')
            ->innerJoin('doctor_chat_member.doctor', 'doctor')
            ->innerJoin('doctor_chat_member.doctorChat', 'doctor_chat')
            ->where(
                $qb->expr()->eq('doctor_chat.identifier', ':doctorChatIdentifier')
            )
            ->andWhere(
                $qb->expr()->notIn('doctor.id', ':offlineUsersIds')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_member.leftChat', ':leftChat')
            )
            ->setParameters([
                'offlineUsersIds'      => $aOnlineUsersIds,
                'doctorChatIdentifier' => $sDoctorChatIdentifier,
                'leftChat'             => false,
            ]);

        return $qb->getQuery()->getResult();
    }
}
