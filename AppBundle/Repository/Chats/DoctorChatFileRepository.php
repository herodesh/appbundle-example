<?php

namespace Flendoc\AppBundle\Repository\Chats;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\Chats\DoctorChatMember;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class DoctorChatFileRepository
 * @package Flendoc\AppBundle\Repository
 */
class DoctorChatFileRepository extends EntityRepository
{
    /**
     * Get chat members ids by chat identifier.
     * Exclude current user.
     *
     * @param DoctorChat        $oDoctorChat
     * @param DoctorChatMember  $oDoctorChatMember
     * @param bool              $bQueryResult
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    public function findByChat($oDoctorChat, $oDoctorChatMember, $bQueryResult = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat_file');

        $qb
            ->select('doctor_chat_file.name, doctor_chat_file.originalFileName, doctor_chat_file.createdAt, CONCAT(doctor_profile.firstName,\' \', doctor_profile.lastName) as doctorFullName')
            ->innerJoin('doctor_chat_file.doctorUploader', 'doctor')
            ->innerJoin('doctor.doctorProfile', 'doctor_profile')
            ->where(
                $qb->expr()->eq('doctor_chat_file.doctorChat', ':doctorChatId')
            )
            ->andWhere(
                $qb->expr()->eq('doctor_chat_file.unprocessedUpload', true)
            )
            ->setParameter('doctorChatId', $oDoctorChat->getId())
        ;

        if ($oDoctorChatMember->getLeftChat()) {
            $qb
                ->andWhere($qb->expr()->lte('doctor_chat_file.createdAt', ':leftChatAt'))
                ->setParameter('leftChatAt', $oDoctorChatMember->getLeftChatAt())
            ;
        }

        if ($oDoctorChatMember->getSoftDeleteAt()) {
            $qb
                ->andWhere($qb->expr()->gte('doctor_chat_file.createdAt', ':softDeletedAt'))
                ->setParameter('softDeletedAt', $oDoctorChatMember->getSoftDeleteAt())
            ;
        }

        $qb->orderBy('doctor_chat_file.createdAt', 'DESC');

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param Doctors $oDoctor
     * @param bool    $bResult
     *
     * @return \Doctrine\ORM\Query|mixed
     */
    public function findByDoctor($oDoctor, $bResult = false)
    {
        $qb = $this->createQueryBuilder('doctor_chat_file')
            ->where('doctor_chat_file.doctorUploader = :doctor')
            ->setParameter('doctor', $oDoctor->getId())
            ->getQuery()
        ;

        if ($bResult) {
            return $qb->getResult();
        }

        return $qb;
    }

    /**
     * @param DoctorChat $oDoctorChat
     */
    public function removeByChat($oDoctorChat)
    {
        $this->createQueryBuilder('doctor_chat_file')
            ->delete()
            ->where('doctor_chat_file.doctorChat = :doctorChat')
            ->setParameter('doctorChat', $oDoctorChat->getId())
        ;
    }
}
