<?php

namespace Flendoc\AppBundle\Repository\Notifications;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Flendoc\AppBundle\Manager\Notifications\NotificationManager;

/**
 * Class MongoNotificationRepository
 * @package Flendoc\AppBundle\Repository\Notifications
 */
class MongoNotificationRepository extends DocumentRepository
{
    /**
     * @param string $sUserIdentifier
     * @param bool   $isMessengerNotification
     *
     * @return int
     */
    public function getUnreadNotificationsNumber($sUserIdentifier, $isMessengerNotification = false)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('receiverIdentifier')
            ->equals($sUserIdentifier)
            ->field('isRead')
            ->equals(false)
        ;

        if ($isMessengerNotification) {
            $qb
                ->field('type')
                ->equals(NotificationManager::NEW_MESSAGE_NOTIFICATION)
            ;
        } else {
            $qb
                ->field('type')
                ->notEqual(NotificationManager::NEW_MESSAGE_NOTIFICATION)
            ;
        }

        return $qb->getQuery()->count();
    }

    /**
     * @param string      $sUserIdentifier
     * @param int         $iLimit
     * @param string|null $sNotificationIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findGeneralNotifications($sUserIdentifier, $iLimit, $sNotificationIdentifier = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('receiverIdentifier')
            ->equals($sUserIdentifier)
            ->field('type')
            ->notEqual(NotificationManager::NEW_MESSAGE_NOTIFICATION)
            ->sort('createdAt', 'desc')
            ->limit($iLimit)
            ->skip(0)
        ;

        if ($sNotificationIdentifier) {
            $qb
                ->field('identifier')
                ->lt($sNotificationIdentifier)
            ;
        }

        return$qb->getQuery()->execute();
    }

    /**
     * @param string      $sUserIdentifier
     * @param string|null $sNotificationIdentifier
     *
     * @return int
     */
    public function getGeneralNotificationsNumber($sUserIdentifier, $sNotificationIdentifier = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->field('receiverIdentifier')
            ->equals($sUserIdentifier)
            ->field('type')
            ->notEqual(NotificationManager::NEW_MESSAGE_NOTIFICATION)
            ->sort('createdAt', 'desc')
        ;

        if ($sNotificationIdentifier) {
            $qb
                ->field('identifier')
                ->lt($sNotificationIdentifier)
            ;
        }

        return $qb->getQuery()->count();
    }

    /**
     * @param string      $sType
     * @param string      $sUserIdentifier
     * @param string|null $sDependencyIdentifier
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function removeNotificationsByType($sType, $sUserIdentifier, $sDependencyIdentifier = null)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->remove()
            ->field('type')
            ->equals($sType)
        ;

        if ($sDependencyIdentifier) {
            $qb
                ->field('dependencyIdentifier')
                ->equals($sDependencyIdentifier)
            ;
        } else {
            $qb
                ->field('receiverIdentifier')
                ->equals($sUserIdentifier)
            ;
        }

        $qb->getQuery()->execute();
    }
}
