<?php

namespace Flendoc\AppBundle\Repository\Settings;

use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class CronjobsRepository
 * @package Flendoc\AppBundle\Repository\Settings
 */
class CronjobsRepository extends FlendocRepository
{
    /**
     * Finding all cronjobs
     *
     * @return array
     */
    public function findAllCronjobs()
    {
        return $this->createQueryBuilder('cj')
                    ->select('cj.id, cj.identifier, cj.commandName, cj.commandDescription, cj.isActive, cj.hasTaskToDo')
                    ->getQuery()->getScalarResult();
    }
}
