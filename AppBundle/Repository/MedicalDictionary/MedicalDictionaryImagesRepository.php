<?php

namespace Flendoc\AppBundle\Repository\MedicalDictionary;

use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class MedicalDictionaryImagesRepository
 * @package Flendoc\AppBundle\Repository\MedicalDictionary
 */
class MedicalDictionaryImagesRepository extends FlendocRepository
{
}