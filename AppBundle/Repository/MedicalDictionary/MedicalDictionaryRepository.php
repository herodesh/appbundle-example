<?php

namespace Flendoc\AppBundle\Repository\MedicalDictionary;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class MedicalDictionaryRepository
 * @package Flendoc\AppBundle\Repository\MedicalDictionary
 */
class MedicalDictionaryRepository extends FlendocRepository
{

    /**
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findAdminOverviewMedicalDictionary(Languages $oLanguage)
    {
        $aResult = $this->findByLanguage($oLanguage);
        $aResult->select('md.id, md.identifier, md.isActive, md.isSocialAdded,
                        COALESCE(ml1.title, ml2.title) as title,
                        COALESCE(ml1.definition, ml2.definition) as definition,
                        SUBSTRING(COALESCE(ml1.description, ml2.description), 1, 100) as description');

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Get medical dictionary by author
     *
     * @param Doctors $oDoctor
     *
     * @return mixed
     */
    public function findMedicalDictionaryByAuthor($oDoctor)
    {
        $aResult = $this->createQueryBuilder('md')
                        ->join('md.authors', 'a')
                        ->where('a = :doctor')
                        ->setParameter('doctor', $oDoctor);

        return $aResult->getQuery()->getResult();
    }

    /**
     * @param Languages $oLanguage
     * @param int       $iLimit
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findRandomEntriesWithLimit(Languages $oLanguage, $iLimit = AppConstants::SUGGESTIONS_LIMIT)
    {
        $sql         = "SELECT md.id FROM medical_dictionary as md WHERE md.is_active = true ORDER BY RAND() LIMIT $iLimit";
        $sqlPrepared = $this->_em->getConnection()->prepare($sql);
        $sqlPrepared->execute();
        $aRandomIds = [];
        while ($val = $sqlPrepared->fetch()) {
            $aRandomIds[] = $val['id'];
        }

        $aResult = $this->findByLanguage($oLanguage);

        //add first image to the results if available
        $aResult->addSelect('(
            SELECT MAX(mdi.photos) FROM AppBundle:MedicalDictionary\MedicalDictionaryImages as mdi
            WHERE md = mdi.medicalDictionary
            ) as medicalDictionaryImage
        ');

        $aResult
            ->andWhere('md.isActive = :isActive')
            ->andWhere('md.id IN (:randomIds)')
            ->setParameter('isActive', true)
            ->setParameter('randomIds', $aRandomIds)
            ->orderBy('md.createdAt', 'DESC');

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findRandomEntryByInput(Languages $oLanguage)
    {
        $iCount = $this->createQueryBuilder('md')
                       ->select('count(md)')
                       ->andWhere('md.isActive = :isActive')
                       ->setParameter('isActive', true)
                       ->getQuery()
                       ->getSingleScalarResult();

        //we need to put -1 because the entries are from 0 and count needs to be 1 less
        $iRandomEntry = rand(0, (int)$iCount - 1);

        $aResult = $this->findByLanguage($oLanguage);
        $aResult->select('md.id, md.identifier, md.isActive,
                        COALESCE(ml1.title, ml2.title) as title,
                        COALESCE(ml1.definition, ml2.definition) as definition,
                        COALESCE(ml1.slug, ml2.slug) as slug,
                        SUBSTRING(COALESCE(ml1.description, ml2.description), 1, 200) as description 
                        ')
                ->setFirstResult($iRandomEntry)
                ->setMaxResults(1)
                ->andWhere('md.isActive = :isActive')
                ->setParameter('isActive', true);

        return $aResult->getQuery()
                       ->getOneOrNullResult();
    }

    /**
     * @param string    $sIdentifier
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findSocialMediaPreview($sIdentifier, Languages $oLanguage)
    {
        $aResult = $this->findByLanguage($oLanguage);

        //add first image to the results if available
        $aResult->addSelect('(
            SELECT MAX(mdi.photos) FROM AppBundle:MedicalDictionary\MedicalDictionaryImages as mdi
            WHERE md = mdi.medicalDictionary
            ) as medicalDictionaryImage
        ');

        $aResult
            ->andWhere('md.isActive = :isActive')
            ->andWhere('md.identifier = :identifier')
            ->setParameter('isActive', true)
            ->setParameter('identifier', $sIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param string    $sIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMedicalDictionaryEntryByLanguage(Languages $oLanguage, $sIdentifier)
    {
        $aResult = $this->findByLanguage($oLanguage);
        $aResult->andWhere('md.identifier = :identifier')
                ->setParameter('identifier', $sIdentifier);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Languages $oLanguage
     * @param int       $iPage
     *
     * @return array
     */
    public function findPaginatedMedicalDictionaryEntries(Languages $oLanguage, int $iPage = null)
    {
        $aResult = $this->findByLanguage($oLanguage);

        //add first image to the results if available
        $aResult->addSelect('(
            SELECT MAX(mdi.photos) FROM AppBundle:MedicalDictionary\MedicalDictionaryImages as mdi
            WHERE md = mdi.medicalDictionary
            ) as medicalDictionaryImage
        ');

        if (null != $iPage) {
            $aResult->setFirstResult($iPage * AppConstants::QUERY_MAX_RESULTS);
        }

        $aResult
            ->andWhere('md.isActive = :isActive')
            ->setMaxResults(AppConstants::QUERY_MAX_RESULTS)
            ->setParameter('isActive', true)
            ->orderBy('md.createdAt', 'DESC');

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param $skip
     * @param $limit
     *
     * @return mixed
     */
    public function findWithSkipAndLimit($skip, $limit)
    {
        $qb = $this->createQueryBuilder('md')
                   ->andWhere('md.isActive = :isActive')
                   ->setFirstResult($skip)
                   ->setMaxResults($limit)
                   ->setParameter(':isActive', true)
                   ->orderBy('md.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all approved medical cases by search key.
     * This does not search in Skills attached to the medical case even if it should (@TODO Not implemented yet)
     *
     * @param Languages $oLanguage
     * @param null      $sSearchKey
     * @param int       $iPage
     * @param null      $iPageSize
     * @param null      $iMaxResults
     * @param bool      $bQueryResult
     *
     * @return array|\Doctrine\ORM\QueryBuilder|null
     */
    public function findPublishedMedicalDictionaryBySearchKey(
        Languages $oLanguage,
        $sSearchKey = null,
        $iPage = 1,
        $iPageSize = null,
        $iMaxResults = null
    ) {
        if (null == $sSearchKey) {
            return null;
        }

        $aResult = $this->findByLanguage($oLanguage);

        //add first image to the results if available
        $aResult->addSelect('(
            SELECT MAX(mdi.photos) FROM AppBundle:MedicalDictionary\MedicalDictionaryImages as mdi
            WHERE md = mdi.medicalDictionary
            ) as medicalDictionaryImage
        ');

        $aResult->andWhere('md.isActive = :isActive')
                ->andWhere('LOWER(COALESCE(ml1.title, ml2.title)) LIKE :searchKey')
                ->setFirstResult($iPageSize * $iPage)
                ->setParameter('searchKey', '%'.$sSearchKey.'%')
                ->setParameter(':isActive', true);

        if (null != $iMaxResults) {
            $aResult->setMaxResults($iMaxResults);
        } else {
            $aResult->setMaxResults($iPageSize);
        }

        $aResult
            ->orderBy('md.createdAt')
            ->orderBy('md.identifier');

        //we return simple result as everything is prepared in the controller
        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNumberOfActiveMedicalDictionaryDocuments()
    {
        $qb = $this->createQueryBuilder('medical_dictionary');

        $qb
            ->select('count(medical_dictionary.id)')
            ->where(
                $qb->expr()->eq('medical_dictionary.isActive', ':isActive')
            )
            ->setParameters([
                'isActive' => true,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string    $sIdentifier
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findGeneratedTagsByIdentifierAndLanguage($sTranslatedIdentifier, Languages $oLanguage)
    {
        $qb = $this->createQueryBuilder('md');
        $qb->select('ht.identifier, ht.tag')
           ->leftJoin('md.medicalDictionaryTranslation', 'mdt')
           ->innerJoin('mdt.hashTag', 'ht')
           ->where('mdt.identifier = :translatedIdentifier')
           ->andWhere('mdt.language = :language')
           ->setParameters([
               'translatedIdentifier' => $sTranslatedIdentifier,
               'language'             => $oLanguage,
           ]);

        return $qb->getQuery()->getScalarResult();
    }

    /**
     * @param string    $sCurrentEntryIdentifier
     * @param array     $aMedicalDictionaryTags
     * @param Languages $oLanguage
     *
     * @return array
     */
    public function findEntriesWithSimilarTagsAndLanguage(
        $sCurrentEntryIdentifier,
        array $aMedicalDictionaryTags,
        Languages $oLanguage
    ) {
        $qb = $this->createQueryBuilder('md');
        $qb->select('md.createdAt, MAX(ht.identifier) as identifier, MAX(ht.tag) as tag, MAX(mdt.slug) as slug, MAX(mdt.title) as title, MAX(mdt.definition) as definition, MAX(md.identifier) as medicalDictionaryIdentifier')
           ->leftJoin('md.medicalDictionaryTranslation', 'mdt')
           ->innerJoin('mdt.hashTag', 'ht')
           ->where('ht.tag IN (:tags)')
           ->andWhere('mdt.language = :language')
           ->andWhere('md.identifier != :currentEntryIdentifier')
           ->setParameters([
               'currentEntryIdentifier' => $sCurrentEntryIdentifier,
               'tags'                   => $aMedicalDictionaryTags,
               'language'               => $oLanguage,
           ])
           ->groupBy('md')
           ->orderBy('md.createdAt', 'DESC')//this might take a lot of time if there are many entries
           ->setMaxResults(AppConstants::SUGGESTIONS_LIMIT);

        return $qb->getQuery()->getScalarResult();
    }

    /**
     * @param Languages $oLanguage
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByLanguage(Languages $oLanguage)
    {
        $qb = $this->createQueryBuilder('md')
                   ->select(' md.id, md.identifier, md.isActive, md.sources, md.isSocialAdded,
                        ml1.identifier as translatedIdentifier,
                        COALESCE(ml1.title, ml2.title) as title,
                        COALESCE(ml1.definition, ml2.definition) as definition,
                        COALESCE(ml1.description, ml2.description) as description,
                        COALESCE(ml1.causes, ml2.causes) as causes,
                        COALESCE(ml1.symptoms, ml2.symptoms) as symptoms,
                        COALESCE(ml1.diagnosis, ml2.diagnosis) as diagnosis,
                        COALESCE(ml1.treatment, ml2.treatment) as treatment,
                        COALESCE(ml1.prognosis, ml2.prognosis) as prognosis,
                        COALESCE(ml1.prevention, ml2.prevention) as prevention,
                        COALESCE(ml1.slug, ml2.slug) as slug
                        ')
                   ->leftJoin('md.medicalDictionaryTranslation', 'ml1', 'WITH', 'ml1.language = :language')
                   ->leftJoin('md.medicalDictionaryTranslation', 'ml2', 'WITH', 'ml2.language = :defaultLanguage')
                   ->leftJoin('ml1.language', 'language1')
                   ->leftJoin('ml2.language', 'language2')
                   ->setParameters([
                       'language'        => $oLanguage,
                       'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                   ]);

        return $qb;
    }
}
