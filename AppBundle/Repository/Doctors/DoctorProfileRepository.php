<?php

namespace Flendoc\AppBundle\Repository\Doctors;

use Doctrine\ORM\EntityRepository;

/**
 * Class DoctorProfileRepository
 * @package Flendoc\AppBundle\Repository
 */
class DoctorProfileRepository extends EntityRepository
{
}
