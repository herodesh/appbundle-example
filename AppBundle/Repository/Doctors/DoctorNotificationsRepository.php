<?php

namespace Flendoc\AppBundle\Repository\Doctors;

use Flendoc\AppBundle\Repository\FlendocRepository;

/**
 * Class AdminRepository
 * @package Flendoc\AppBundle\Repository\Doctors
 */
class DoctorNotificationsRepository extends FlendocRepository
{
    /**
     * Find number of new notifications
     *
     * @param $userId
     *
     * @return array
     */
    public function findNumberOfNewNotifications($userId)
    {
        $qb = $this->createQueryBuilder('dn');
        $qb
            ->select('count(dn.id)')
            ->where($qb->expr()->eq('dn.receiver', $userId))
            ->andWhere($qb->expr()->eq('dn.isRead', 0))
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
}
