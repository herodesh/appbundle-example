<?php

namespace Flendoc\AppBundle\Repository\Doctors;

use Doctrine\ORM\EntityRepository;
use Flendoc\UserBundle\Entity\ProfileImages;
use Flendoc\UserBundle\Entity\UserProfileImages;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class DoctorProfileImageRepository
 * @package Flendoc\AppBundle\Repository\Doctors
 */
class DoctorProfileImageRepository extends EntityRepository
{
    /**
     * Find first inactive
     *
     * @param UserInterface     $oUser
     * @param UserProfileImages $oUserProfileImage
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findFirstInactive(UserInterface $oUser, UserProfileImages $oUserProfileImage)
    {
        $oFirstInactiveProfileImage = $this->createQueryBuilder('p')
                                           ->where('p.userProfile = :userProfile')
                                           ->andWhere('p.id != :profileImageId')
                                           ->setParameters([
                                               'userProfile'    => $oUser->getUserProfile(),
                                               'profileImageId' => $oUserProfileImage->getId(),
                                           ])
                                           ->setMaxResults('1')
                                           ->getQuery()->getOneOrNullResult();

        return $oFirstInactiveProfileImage;
    }

    /**
     * Find all profile images
     *
     * @param $id
     *
     * @return array
     */
    public function findAllProfileImages($id)
    {
        $oUserProfileImages = $this->createQueryBuilder('pi')
                                   ->where('pi.user = :userId')
                                   ->setParameter('userId', $id)
                                   ->getQuery()->getScalarResult();

        return $oUserProfileImages;
    }
}
