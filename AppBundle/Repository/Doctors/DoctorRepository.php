<?php

namespace Flendoc\AppBundle\Repository\Doctors;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use PDO;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * Class DoctorRepository
 * @package Flendoc\AppBundle\Repository\Doctors
 */
class DoctorRepository extends FlendocRepository
{

    /**
     * @param string $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSimpleUserProfileDetails($sUserIdentifier)
    {
        //select profile details
        $aResult = $this->createQueryBuilder('d')
                        ->select('d.id,
                                        d.identifier,
                                        d.username,
                                        d.defaultLanguage, 
                                        d.isVerified, 
                                        d.locked,
                                        d.enabled, 
                                        d.createdAt, 
                                        d.lastLogin,
                                        dp.profilePhoto,
                                        dp.gender, 
                                        dp.firstName, 
                                        dp.lastName,
                                        dp.academicTitle,
                                        CONCAT(dp.academicTitle, \' \', dp.firstName, \' \', dp.lastName) as fullName
                                        ')
                        ->leftJoin('d.doctorProfile', 'dp')
                        ->where('d.identifier = :identifier')
                        ->setParameters([
                            'identifier' => $sUserIdentifier,
                        ])
                        ->setMaxResults(1);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $sQuery
     *
     * @return array
     */
    public function findAllDoctorsByEmail($sQuery)
    {
        //select profile details
        $aResult = $this->createQueryBuilder('d')
                        ->select('d.id, d.identifier, d.username,
                                        CONCAT(dp.academicTitle, \' \', dp.firstName, \' \', dp.lastName) as fullName,
                                        dp.profilePhoto
                                        ')
                        ->leftJoin('d.doctorProfile', 'dp')
                        ->where('d.username LIKE :username')
                        ->setParameters([
                            'username' => '%'.$sQuery.'%',
                        ])
                        ->setMaxResults(AppConstants::QUERY_MAX_RESULTS);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * @param        $sDoctorIdentifier
     * @param string $languageIso Language of the viewer passed for translation
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function findUserLocation($sDoctorIdentifier, $languageIso)
    {
        $aResult = $this->createQueryBuilder('d')
                        ->select('
                            COALESCE(countryL1.name, countryL2.name) as countryName, 
                            COALESCE(cityL1.name, cityL2.name) as cityName 
                        ')
                        ->leftJoin('d.doctorProfile', 'dp')
                        ->leftJoin('AppBundle:Countries\CountryLanguages', 'countryL1', 'WITH', 'dp.country = countryL1.countries AND countryL1.languages = :language')
                        ->leftJoin('AppBundle:Countries\CountryLanguages', 'countryL2', 'WITH', 'dp.country = countryL2.countries AND countryL2.languages = :defaultLanguage')
                        ->leftJoin('AppBundle:Cities\CityLanguages', 'cityL1', 'WITH', 'dp.city = cityL1.cities AND cityL1.languages = :language')
                        ->leftJoin('AppBundle:Cities\CityLanguages', 'cityL2', 'WITH', 'dp.city = cityL2.cities AND cityL2.languages = :defaultLanguage')
                        ->where('d.identifier = :doctorIdentifier')
                        ->setMaxResults(1)
                        ->setParameters([
                            'doctorIdentifier' => $sDoctorIdentifier,
                            'language'         => $this->_em->getRepository('AppBundle:Languages\Languages')
                                                            ->findOneByIso($languageIso),
                            'defaultLanguage'  => $this->selectPrimaryLanguageDQL(),
                        ]);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * Find all users
     *
     * @return array
     */
    public function findAllByType()
    {
        $getRolesQuery = $this->_em->createQueryBuilder()
                                   ->select("GROUP_CONCAT(DISTINCT ur.name SEPARATOR ', ')")
                                   ->from('AppBundle:Users\UserRoles', 'ur');

        $aResult = $this->_findUserAndUserProfile($getRolesQuery);

        return $aResult->getQuery()->getScalarResult();
    }

    /**
     * Find Doctors basic details
     *
     * @param Languages $oLanguages
     * @param           $identifier
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBasicDetails(Languages $oLanguages, $identifier)
    {
        $aResult = $this->createQueryBuilder('d')
                        ->select('
                            d.id, d.identifier, dp.gender, dp.firstName, dp.lastName, d.username, d.enabled, d.createdAt, d.lastLogin,
                            dp.photos, 
                            l.iso as languageIso, COALESCE(tl1.name, tl2.name) as languageName')
                        ->leftJoin('d.doctorProfile', 'dp')
                        ->leftJoin('AppBundle:Languages\Languages', 'l', 'WITH', 'l.iso = d.defaultLanguage')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl1', 'WITH', 'tl1.translatedLanguages = l.id AND l.id = :language')
                        ->leftJoin('AppBundle:Languages\TranslatedLanguages', 'tl2', 'WITH', 'tl2.translatedLanguages = l.id AND l.id = :defaultLanguage')
                        ->where('d.identifier =:userIdentifier')
                        ->groupBy('l')
                        ->setParameters([
                            'userIdentifier'  => $identifier,
                            'language'        => $oLanguages,
                            'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                        ])
                        ->getQuery()
                        ->getOneOrNullResult();

        return $aResult;
    }

    /**
     * @param $sUserIdentifier
     *
     * @return array
     */
    public function findRoles($sUserIdentifier)
    {
        $aUserRoles = $this->createQueryBuilder('d')
                           ->select('dr.name')
                           ->leftJoin('d.doctorRoles', 'dr')
                           ->where('d.identifier = :userIdentifier')
                           ->setParameter('userIdentifier', $sUserIdentifier)
                           ->getQuery()
                           ->getScalarResult();

        $aVal = [];

        if ($aUserRoles) {
            foreach ($aUserRoles as $userRole) {
                $aVal[] = $userRole['name'];
            }
        }

        return $aVal;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findMailManagers()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT u.id, u.username FROM users AS u
                LEFT JOIN `users_user_role` AS uur ON uur.user_id = u.id
                WHERE uur.role_id = (SELECT id FROM `user_roles` AS ur WHERE ur.name= :userRoleName)";

        $sqlReady = $conn->prepare($sql);
        $sqlReady->execute(['userRoleName' => 'ROLE_EMAIL_MANAGEMENT']);

        return $sqlReady->fetchAll();
    }

    /**
     * Find users that want to receive the emails
     *
     * @param $sUsername
     * @param $sMailIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMailReceivers($sUsername, $sMailIdentifier)
    {
        $aResult = $this->createQueryBuilder('u')
                        ->leftJoin('UserBundle:UserMailSettings', 'ums', 'WITH', 'ums.user= u.id')
                        ->leftJoin('AppBundle:Mails\Mails', 'm', 'WITH', 'ums.mail = m.id')
                        ->where('u.username = :username')
                        ->andWhere('m.identifier = :mailIdentifier')
                        ->andWhere('ums.enabled = :enabledSetting')
                        ->setParameters([
                            'username'       => $sUsername,
                            'mailIdentifier' => $sMailIdentifier,
                            'enabledSetting' => true,
                        ]);

        return $aResult->getQuery()->getOneOrNullResult();
    }

    /**
     * @param null $sRoleQuery
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function _findUserAndUserProfile($sRoleQuery = null)
    {
        $aResult = $this->createQueryBuilder('d')
                        ->select('d.id, d.identifier, d.username, d.enabled, d.isVerified, dp.firstName, dp.lastName')
                        ->leftJoin('AppBundle:Doctors\DoctorProfile', 'dp', 'WITH', 'dp.doctor = d.id');

        if ($sRoleQuery) {
            $aResult->addSelect("(".$sRoleQuery.") AS roles");
        }

        return $aResult;
    }

    /**
     * @param $identifier
     *
     * @return array
     */
    public function findUserRegistrationDocuments($identifier)
    {
        return $this->createQueryBuilder('d')
                    ->select('drd.id, drd.fileName, drd.originalName')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocuments', 'drd', 'WITH', 'drd.doctors = d')
                    ->where('d.identifier = :doctorIdentifier')
                    ->setParameter('doctorIdentifier', $identifier)
                    ->getQuery()
                    ->getScalarResult();
    }

    /**
     * @param string    $sSearchKey
     * @param Languages $oLanguage
     * @param integer   $iPage
     * @param integer   $iPageSize
     * @param integer   $iMaxResults
     * @param bool      $bQueryResult
     *
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function findSearchUsers(
        $sSearchKey,
        Languages $oLanguage,
        $iPage = 1,
        $iPageSize = null,
        $iMaxResults = null,
        $bQueryResult = true
    ) {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->select('
                d.id, d.isVerified, d.identifier, 
                dp.firstName, dp.lastName, dp.academicTitle, 
                COALESCE(country_lang1.name, country_lang2.name) as countryName, 
                COALESCE(city_lang1.name, city_lang2.name) as cityName
                ')
            ->innerJoin('d.doctorProfile', 'dp')
            ->leftJoin('dp.country', 'country', 'WITH', 'dp.country = country')
            ->leftJoin('country.countryLanguages', 'country_lang1', 'WITH', 'country_lang1.languages = :language')
            ->leftJoin('country.countryLanguages', 'country_lang2', 'WITH', 'country_lang2.languages = :defaultLanguage')
            ->leftJoin('dp.city', 'city', 'WITH', 'city.country = country')
            ->leftJoin('city.cityLanguages', 'city_lang1', 'WITH', 'city_lang1.languages = :language')
            ->leftJoin('city.cityLanguages', 'city_lang2', 'WITH', 'city_lang2.languages = :defaultLanguage');

        if (!empty($sSearchKey)) {
            $qb->andWhere(
                $qb->expr()->like(
                    'LOWER(CONCAT(dp.firstName, \' \',dp.lastName))',
                    $qb->expr()->literal('%'.$sSearchKey.'%')
                )
            );
        }

        $qb->andWhere('d.enabled = :enabled')
           ->setFirstResult($iPageSize * $iPage)
           ->setParameter('enabled', true)
           ->setParameter('language', $oLanguage)
           ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());

        if (null != $iMaxResults) {
            $qb->setMaxResults($iMaxResults);
        } else {
            $qb->setMaxResults($iPageSize);
        }

        $qb
            ->orderBy('d.createdAt')
            ->orderBy('d.identifier');

        if ($bQueryResult) {
            return $qb->getQuery()->getScalarResult();
        }

        return $qb;
    }

    /**
     * @param int    $iUserId
     * @param int    $iLanguageId
     * @param int    $iCountryId
     * @param int    $iSkillId
     * @param string $sSearchKey
     * @param array  $aIdentifiers
     * @param int    $iPage
     * @param int    $iPageSize
     * @param bool   $enabled
     * @param bool   $bQueryResult If true then display the hydrated results
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findActiveUsers(
        $iUserId,
        $iLanguageId,
        $iCountryId,
        $iSkillId,
        $sSearchKey,
        $aIdentifiers,
        $iPage,
        $iPageSize,
        $enabled = true,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->select('
                d.id, d.isVerified, d.identifier, 
                dp.firstName, dp.lastName, dp.academicTitle, dp.gender, 
                COALESCE(country_lang1.name, country_lang2.name) as countryName, 
                COALESCE(city_lang1.name, city_lang2.name) as cityName
                ')
            ->innerJoin('d.doctorProfile', 'dp')
            ->leftJoin('dp.country', 'country', 'WITH', 'dp.country = country')
            ->leftJoin('country.countryLanguages', 'country_lang1', 'WITH', 'country_lang1.languages = :languageId')
            ->leftJoin('country.countryLanguages', 'country_lang2', 'WITH', 'country_lang2.languages = :defaultLanguage')
            ->leftJoin('dp.city', 'city', 'WITH', 'city.country = country')
            ->leftJoin('city.cityLanguages', 'city_lang1', 'WITH', 'city_lang1.languages = :languageId')
            ->leftJoin('city.cityLanguages', 'city_lang2', 'WITH', 'city_lang2.languages = :defaultLanguage');

        if ($iSkillId) {
            $qb
                ->innerJoin('dp.doctorSkills', 'ds')
                ->innerJoin('ds.skillLanguages', 'ds_languages');
        }

        $qb->where($qb->expr()->neq('d.id', $iUserId))
           ->andWhere($qb->expr()->eq('d.enabled', $enabled));

        if (!empty($aIdentifiers)) {
            $qb->andWhere($qb->expr()->notIn('d.identifier', $aIdentifiers));
        }

        if ($iCountryId) {
            $qb->andWhere($qb->expr()->eq('dp.country', $iCountryId));
        }

        if ($iSkillId) {
            $qb
                ->andWhere($qb->expr()->eq('ds.id', $iSkillId))
                ->andWhere($qb->expr()->eq('ds_languages.languages', $iLanguageId));
        }

        if (!empty($sSearchKey)) {
            $qb->andWhere(
                $qb->expr()->like(
                    'LOWER(CONCAT(dp.firstName, \' \',dp.lastName))',
                    $qb->expr()->literal('%'.$sSearchKey.'%')
                )
            );
        }

        $qb
            ->orderBy('d.identifier', 'ASC')
            ->setFirstResult($iPageSize * $iPage)
            ->setMaxResults($iPageSize);

        $qb->setParameter('languageId', $iLanguageId)
           ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());

        //ghost users should not be displayed in searches
        $qb->andWhere('d.isGhostUser <> :isGhostUser')
           ->setParameter('isGhostUser', true);

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * Find doctor contacts by user identifiers from Neo4J.
     *
     * @param string $sUserIdentifier
     * @param        $languageId
     * @param array  $identifiers
     * @param bool   $enabled
     * @param bool   $bQueryResult If true then display the hydrated results
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findContacts(
        $sUserIdentifier,
        $languageId,
        $identifiers,
        $enabled = true,
        $bQueryResult = false
    ) {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->select('
                d.id, d.isVerified, d.identifier, 
                dp.firstName, dp.lastName, dp.academicTitle, dp.gender, 
                COALESCE(country_lang1.name, country_lang2.name) as countryName, 
                COALESCE(city_lang1.name, city_lang2.name) as cityName
                ')
            ->innerJoin('d.doctorProfile', 'dp')
            ->leftJoin('dp.country', 'country', 'WITH', 'dp.country = country')
            ->leftJoin('country.countryLanguages', 'country_lang1', 'WITH', 'country_lang1.languages = :languageId')
            ->leftJoin('country.countryLanguages', 'country_lang2', 'WITH', 'country_lang2.languages = :defaultLanguage')
            ->leftJoin('dp.city', 'city', 'WITH', 'city.country = country')
            ->leftJoin('city.cityLanguages', 'city_lang1', 'WITH', 'city_lang1.languages = :languageId')
            ->leftJoin('city.cityLanguages', 'city_lang2', 'WITH', 'city_lang2.languages = :defaultLanguage');

        $qb->where('d.identifier != :sUserIdentifier')
           ->andWhere($qb->expr()->eq('d.enabled', $enabled))
           ->setParameter('sUserIdentifier', $sUserIdentifier)
           ->setParameter('languageId', $languageId)
           ->setParameter('defaultLanguage', $this->selectPrimaryLanguageDQL());

        if (!empty($identifiers)) {
            $qb->andWhere($qb->expr()->in('d.identifier', $identifiers));
        }

        //ghost users should not be displayed in searches
        $qb->andWhere('d.isGhostUser <> :isGhostUser')
           ->setParameter('isGhostUser', true);

        if ($bQueryResult) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * @param null $sQueryString
     *
     * @return array
     */
    public function findUsersByQuery($sQueryString = null)
    {
        $qb = $this->createQueryBuilder('d');
        $qb
            ->select('d.id, d.identifier, CONCAT(dp.firstName, \' \', dp.lastName) as name ')
            ->innerJoin('d.doctorProfile', 'dp')
            ->where($qb->expr()->eq('d.enabled', 'true'))
            ->andWhere($qb->expr()->like('CONCAT(dp.firstName, \' \', dp.lastName)', ':queryString'))
            ->setParameter('queryString', '%'.$sQueryString.'%');

        return $qb->getQuery()->getScalarResult();
    }

    /**
     * @param int    $iLimit
     * @param string $sUserIdentifier
     * @param array  $aFriendWithIdentifiers
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findRandomUsersByLimit($iLimit, $sUserIdentifier, $aFriendWithIdentifiers)
    {
        $inQuery = implode(',', array_fill(0, count($aFriendWithIdentifiers), '?'));
        if (empty($inQuery)) {
            $inQuery = '""';
        }

        $conn = $this
            ->getEntityManager()
            ->getConnection();

        $sql = '
            SELECT identifier FROM
            doctors as r1
            JOIN (SELECT CEIL(RAND() * (SELECT MAX(id) FROM doctors)) - ? AS id) AS r2
            WHERE r1.id >= r2.id
            AND r1.enabled = true
            AND r1.is_ghost_user = false
            AND r1.identifier <> ?
            AND r1.identifier NOT IN ('.$inQuery.')
            ORDER BY r1.id ASC
            LIMIT ?';

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1, $iLimit, PDO::PARAM_INT);
        $stmt->bindValue(2, $sUserIdentifier);

        foreach ($aFriendWithIdentifiers as $k => $identifier) {
            $stmt->bindValue(($k + 3), $identifier);
        }

        //limit
        $stmt->bindValue(sizeof($aFriendWithIdentifiers) + 3, $iLimit, PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNumberOfEnabledUsers()
    {
        $qb = $this->createQueryBuilder('doctor');

        $qb
            ->select('count(doctor.id)')
            ->where(
                $qb->expr()->eq('doctor.enabled', ':enabled')
            )
            ->setParameter('enabled', true);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param \DateTime $oPreviousDay
     * @param \DateTime $oToday
     *
     * @return mixed
     */
    public function findUsersRegisteredPreviousDay($oPreviousDay, $oToday)
    {
        $qb = $this->createQueryBuilder('doctor');

        $qb
            ->select('
                dp.firstName,
                dp.lastName,
                doctor.identifier,
                country_lang.name as countryName
                ')
            ->innerJoin('doctor.doctorProfile', 'dp')
            ->leftJoin('dp.country', 'country', 'WITH', 'dp.country = country')
            ->leftJoin('country.countryLanguages', 'country_lang', 'WITH', 'country_lang.languages = :defaultLanguage')
            ->where($qb->expr()->gte('doctor.createdAt', ':previousDay'))
            ->andWhere($qb->expr()->lte('doctor.createdAt', ':today'))
            ->andWhere($qb->expr()->eq('doctor.isGhostUser', ':isGhostUser'))
            ->setParameters([
                'previousDay'     => $oPreviousDay,
                'today'           => $oToday,
                'isGhostUser'     => false,
                'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param \DateTime $oPreviousDay
     * @param \DateTime $oToday
     *
     * @return mixed
     */
    public function findUsersWhoSendVerificationDocumentsPreviousDay($oPreviousDay, $oToday)
    {
        $qb = $this->createQueryBuilder('doctor');

        $qb
            ->select('
                dp.firstName,
                dp.lastName,
                doctor.identifier,
                country_lang.name as countryName
                ')
            ->innerJoin('doctor.doctorProfile', 'dp')
            ->leftJoin('dp.country', 'country', 'WITH', 'dp.country = country')
            ->leftJoin('country.countryLanguages', 'country_lang', 'WITH', 'country_lang.languages = :defaultLanguage')
            ->where($qb->expr()->gte('doctor.verificationDocumentsSentAt', ':previousDay'))
            ->andWhere($qb->expr()->lte('doctor.verificationDocumentsSentAt', ':today'))
            ->andWhere($qb->expr()->eq('doctor.isGhostUser', ':isGhostUser'))
            ->setParameters([
                'previousDay'     => $oPreviousDay,
                'today'           => $oToday,
                'isGhostUser'     => false,
                'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function findUnconfirmedUsers()
    {
        $qb = $this->createQueryBuilder('doctor');
        $qb->where('doctor.enabled = :enabled')
           ->andWhere('doctor.isVerified = :isVerified')
           ->andWhere('doctor.isGhostUser = :isGhostUser')
           ->andWhere('doctor.confirmationToken IS NOT NULL')
           ->setParameters([
               'enabled'           => false,
               'isGhostUser'       => false,
               'isVerified'        => false
           ]);

        return $qb->getQuery()->getResult();
    }
}
