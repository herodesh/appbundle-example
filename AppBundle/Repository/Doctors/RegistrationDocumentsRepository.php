<?php

namespace Flendoc\AppBundle\Repository\Doctors;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Repository\FlendocRepository;
use Flendoc\AppBundle\Repository\RepositoryInterface;

/**
 * Class RegistrationDocumentsRepository
 * @package Flendoc\AppBundle\Repository\Doctors
 */
class RegistrationDocumentsRepository extends FlendocRepository implements RepositoryInterface
{

    /**
     * @param $oUser
     *
     * @return array
     */
    public function findRejectedDocumentsFromUser($oUser)
    {
        return $this->createQueryBuilder('rd')
                    ->select('rd.id, rd.originalName,
                                   COALESCE(rdrl1.description, rdrl2.description) as rejectionDescription')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentsRejection', 'rdr', 'WITH', 'rd.rejectionReason = rdr')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentsRejectionLanguages', 'rdrl1', 'WITH', 'rdrl1.registrationDocumentsRejection = rdr AND rdrl1.languages = :language')
                    ->leftJoin('AppBundle:Doctors\RegistrationDocumentsRejectionLanguages', 'rdrl2', 'WITH', 'rdrl2.registrationDocumentsRejection = rdr AND rdrl2.languages = :defaultLanguage')
                    ->where('rd.doctors = :doctor')
                    ->andWhere('rd.isRejected = :rejected')
                    ->andWhere('rd.isUserNotified = :userNotified')
                    ->setParameters([
                        'doctor'          => $oUser,
                        'rejected'        => true,
                        'userNotified'    => false,
                        'language'        => $this->_em->getRepository('AppBundle:Languages\Languages')
                                                       ->findOneByIso($oUser->getDefaultLanguage()),
                        'defaultLanguage' => $this->selectPrimaryLanguageDQL(),
                    ])
                    ->getQuery()
                    ->getResult();

    }

    public function findByLanguage(Languages $oLanguage)
    {
        // TODO: Implement findByLanguage() method.
    }
}
