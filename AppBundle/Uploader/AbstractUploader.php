<?php

namespace Flendoc\AppBundle\Uploader;

use Flendoc\AppBundle\Constants\AppConstants;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

/**
 * Class AbstractUploader
 * @package Flendoc\AppBundle\Uploader
 */
abstract class AbstractUploader
{

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @param Translator $translator
     */
    public function setTranslator(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Instantiate photo uploaded file object
     *
     * @param      $sPath
     * @param      $sOriginalName
     * @param null $sMimeType
     *
     * @return UploadedFile
     */
    public function uploadedFileInstance($sPath, $sOriginalName, $sMimeType = null)
    {
        return new UploadedFile($sPath, $sOriginalName, $sMimeType);
    }

    /**
     * Process chunks locally
     *
     * @param Request $oRequest
     *
     * @return bool|string
     */
    public function processChunksLocally(Request $oRequest)
    {
        $sFileIdentifier  = $oRequest->get('resumableIdentifier');
        $sFileTotalSize   = $oRequest->get('resumableTotalSize');
        $sFileChunkNumber = $oRequest->get('resumableChunkNumber');

        //generate the upload folder for the specific file
        $sFileDir = $this->_getUploadFolderName($oRequest);

        if ($this->isLocalDirectory($sFileDir)) {
            //scandir and checks number of files = number of chunks
            $sTotalFileSizeOnServer = 0;

            foreach ($oRequest->files as $oFile) {
                move_uploaded_file($oFile->getRealPath(), $sFileDir.$sFileIdentifier.'.part'.$sFileChunkNumber);
                //close the session and allows the AJAX calls to be executed instantly
                session_write_close();
            }

            foreach (scandir($sFileDir) as $sFile) {

                if ($sFile != '.' && $sFile != '..') {
                    $sFileSize              = filesize($sFileDir.$sFile);
                    $sTotalFileSizeOnServer += $sFileSize;
                }
            }

            //Chunk upload process is complete
            if ($sTotalFileSizeOnServer >= $sFileTotalSize) {
                return true;
            }

        } else {

            header('HTTP/1.0 400 Not Acceptable');
            echo $this->getTranslator()->trans('file.upload.error');
        }

        return;
    }

    /**
     * Merge chunks into one file in temp folder
     *
     * @param Request $oRequest
     *
     * @return bool|array
     */
    public function mergeChunks(Request $oRequest)
    {
        $sFileDirectory   = $this->_getUploadFolderName($oRequest);
        $sFileName        = $oRequest->get('resumableFilename');
        $sFileTotalChunks = $oRequest->get('resumableTotalChunks');
        $sFileIdentifier  = $oRequest->get('resumableIdentifier');

        if (($fp = fopen($sFileDirectory.$sFileName, 'w')) !== false) {
            for ($i = 1; $i <= $sFileTotalChunks; $i++) {
                fwrite($fp, file_get_contents($sFileDirectory.$sFileIdentifier.'.part'.$i));
            }
            fclose($fp);

            return [
                'fileName'      => $sFileName,
                'fileDirectory' => $sFileDirectory,
            ];
        }

        return false;
    }

    /**
     * Generates a random file name
     *
     * @param UploadedFile $oFile Proposed file
     * @param bool $bShort Default true. If we need the long file name we can add the second parameter
     *                     and set it as false
     *
     * @return bool|string
     */
    public function generateRandomFileName(UploadedFile $oFile, $bShort = true)
    {
        $sString = sha1(time().$oFile->getRealPath().rand(0, 99999).uniqid());

        if ($bShort) {
            return substr($sString, 0, AppConstants::RANDOM_FILE_NAME_SIZE);
        }

        return $sString;
    }

    /**
     * Rename an uploaded file
     *
     * @param UploadedFile $oFile
     *
     * @return array
     */
    public function renameFile(UploadedFile $oFile)
    {
        $oFilesystem    = new Filesystem();
        $sFileExtension = $oFile->getExtension();

        //rename file
        $sNewFileNameString = $this->generateRandomFileName($oFile);
        $sNewFileName = $sNewFileNameString.'.'.$sFileExtension;
        $sNewFilePath = $oFile->getPath().'/'.$sNewFileName;

        //save new file locally
        $oFilesystem->rename($oFile, $sNewFilePath);

        return [
            'originalFileName' => $oFile->getClientOriginalName(),
            'newFileName'      => $sNewFileName,
            'newFilePath'      => $sNewFilePath,
        ];
    }

    /**
     * This is used to upload images and documents. For images we will need to pass an extra parameter for image sizes
     *
     * @param UploadedFile $oFile
     * @param string       $sUploadFolder
     *
     * @return array
     */
    abstract public function uploadFile(UploadedFile $oFile, $sUploadFolder): array;

    /**
     * The actual upload
     *
     * @param Request $oRequest
     * @param string  $sUploadFolder
     *
     * @return array|mixed|null
     */
    public function upload(Request $oRequest, $sUploadFolder)
    {
        if (true === $this->processChunksLocally($oRequest)) {
            $aMergedChunks = $this->mergeChunks($oRequest);
            $aUploadedFile = $this->uploadFile(
                $this->uploadedFileInstance($aMergedChunks['fileDirectory'].$aMergedChunks['fileName'], $aMergedChunks['fileName']),
                $sUploadFolder
            );

            return $aUploadedFile;
        }

        return false;
    }

    /**
     * Check and Create a Local Directory to store and process chunks
     *
     * @param $sFileDir
     *
     * @return bool
     */
    public function isLocalDirectory($sFileDir)
    {
        if (!is_dir($sFileDir)) {
            umask(0002);
            mkdir($sFileDir, 0755, true);
        }

        return true;
    }

    /**
     * Get Upload Folder Name
     *
     * @param Request $oRequest
     *
     * @return string
     */
    protected function _getUploadFolderName(Request $oRequest)
    {
        $sFileIdentifier  = $oRequest->get('resumableIdentifier');
        $sFileTotalSize   = $oRequest->get('resumableTotalSize');
        $sUploadStartTime = $oRequest->get('uploadStartTime');

        return sprintf("%s/%s/", AppConstants::UPLOAD_TMP_FOLDER, sha1($sFileIdentifier.$sFileTotalSize.$sUploadStartTime));
    }
}
