<?php

namespace Flendoc\AppBundle\Uploader;

use Flendoc\AppBundle\Adapter\AmazonS3\AwsS3Adapter;
use Flendoc\AppBundle\Constants\AppConstants;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

/**
 * Class DocumentsUploader
 * @package Flendoc\AppBundle\Uploader
 */
final class DocumentsUploader extends AbstractUploader implements UploaderInterface
{

    /**
     * @var AwsS3Adapter
     */
    protected $adapter;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var array
     */
    protected $allowedTypes;

    /**
     * PhotoUploader constructor.
     *
     * @param AwsS3Adapter       $adapter
     * @param Translator         $translator
     * @param                    $allowedTypes
     */
    public function __construct(
        AwsS3Adapter $adapter,
        Translator $translator,
        $allowedTypes
    ) {
        $this->adapter      = $adapter;
        $this->translator   = $translator;
        $this->allowedTypes = explode(',', $allowedTypes);
    }

    /**
     * Process and upload chunks locally
     *
     * @param Request $oRequest
     *
     * @return bool|void
     */
    public function processChunksLocally(Request $oRequest)
    {
        if (in_array($oRequest->get('resumableType'), $this->allowedTypes)) {
            return parent::processChunksLocally($oRequest);
        } else {
            header('HTTP/1.0 400 Not Acceptable');
            echo $this->translator->trans('file.upload.error');
        }

        return false;
    }

    /**
     * Merge chunks
     *
     * @param Request $oRequest
     *
     * @return bool|string
     */
    public function mergeChunks(Request $oRequest)
    {
        return parent::mergeChunks($oRequest);
    }

    /**
     * Upload an image to Storage area (eg. AWS S3, etc...)
     *
     * @param UploadedFile $oFile
     * @param string       $sUploadFolder
     *
     * @return array
     */
    public function uploadFile(UploadedFile $oFile, $sUploadFolder): array
    {

        $aNewFile = $this->renameFile($oFile);

        //upload file to storage place
        $this->adapter->upload($aNewFile['newFilePath'], $aNewFile['newFileName'], $sUploadFolder);

        //delete local directory
        //@TODO add this to messaging system execution
        exec('rm -Rf '.$oFile->getPath());

        return $aNewFile;
    }

    /**
     * @param Request $oRequest
     * @param string  $sUploadFolder
     *
     * @return array|mixed|null
     */
    public function upload(Request $oRequest, $sUploadFolder)
    {
        return parent::upload($oRequest, $sUploadFolder);
    }

    /**
     * Delete a photo
     *
     * @param string $sFileName
     *
     * @return bool
     */
    public function delete($sFileName)
    {
        return $this->adapter->delete($sFileName);
    }
}
