<?php

namespace Flendoc\AppBundle\Uploader;

use Flendoc\AppBundle\Adapter\AmazonS3\AwsS3Adapter;
use Flendoc\AppBundle\Constants\AppConstants;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

/**
 * Class PhotoUploader
 * @package Flendoc\AppBundle\Uploader
 */
final class PhotoUploader extends AbstractUploader implements UploaderInterface
{

    /**
     * @var AwsS3Adapter
     */
    protected $adapter;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var array
     */
    protected $allowedTypes;

    /** @var array */
    protected $photoSizes;

    /**
     * PhotoUploader constructor.
     *
     * @param AwsS3Adapter $adapter
     * @param Translator   $translator
     * @param              $allowedTypes
     * @param              $photoSizes
     */
    public function __construct(
        AwsS3Adapter $adapter,
        Translator $translator,
        $allowedTypes,
        $photoSizes
    ) {
        $this->adapter      = $adapter;
        $this->translator   = $translator;
        $this->allowedTypes = explode(',', $allowedTypes);
        $this->photoSizes   = $photoSizes;
    }

    /**
     * Process and upload chunks locally
     *
     * @param Request $oRequest
     *
     * @return bool|void
     */
    public function processChunksLocally(Request $oRequest)
    {
        if (in_array($oRequest->get('resumableType'), $this->allowedTypes)) {
            return parent::processChunksLocally($oRequest);
        } else {
            header('HTTP/1.0 400 Not Acceptable');
            echo $this->translator->trans('file.upload.error');
        }

        return false;
    }

    /**
     * Merge chunks
     *
     * @param Request $oRequest
     *
     * @return array
     */
    public function mergeChunks(Request $oRequest)
    {
        return parent::mergeChunks($oRequest);
    }

    /**
     * Upload an image to Storage area (eg. AWS S3, etc...)
     *
     * @param UploadedFile $oFile
     * @param string       $sUploadFolder
     * @param null         $imageSize Forces a single image size to be uploaded
     *
     * @return array
     */
    public function uploadFile(UploadedFile $oFile, $sUploadFolder = AppConstants::AMAZON_PHOTO_DIRECTORY, $imageSize = null): array
    {
        //in order to upload only 1 size image for ex to be used in Resume
        if (null != $imageSize) {
            $this->photoSizes = [$imageSize];
        }

        $aFiles = []; //an array of generated files and their sizes

        //create images for all needed sizes and upload
        foreach ($this->photoSizes as $iImageSize) {
            //process the images, convert, compress
            $aNewImage = $this->_renameResizeAndConvertFile($oFile, $iImageSize);

            //upload file to storage place
            $this->adapter->upload($aNewImage['newFilePath'], $aNewImage['newFileName'], $sUploadFolder);

            $aFiles[$iImageSize] = $aNewImage['newFileName'];
        }

        //delete local directory
        //@TODO add this to messaging system execution
        exec('rm -Rf '.$oFile->getPath());

        $aUploadedFile['originalFileName'] = $oFile->getClientOriginalName();
        $aUploadedFile['files'] = $aFiles;

        return $aUploadedFile;
    }

    /**
     * The actual upload
     *
     * @param Request $oRequest
     * @param string  $sUploadFolder
     *
     * @return array|mixed|null
     */
    public function upload(Request $oRequest, $sUploadFolder)
    {
        return parent::upload($oRequest, $sUploadFolder);
    }

    /**
     * Delete a photo
     *
     * @param string $sFileName
     *
     * @return bool
     */
    public function delete($sFileName)
    {
        return $this->adapter->delete($sFileName);
    }

    /**
     * @param UploadedFile $oFile
     * @param null         $iImageSize
     *
     * @return array
     */
    protected function _renameResizeAndConvertFile(UploadedFile $oFile, $iImageSize = null)
    {
        $oImageManager = new ImageManager();

        //Create a new file path.
        // It's not the same as file rename since we create a new file format
        $sNewFileName = $this->generateRandomFileName($oFile).'.'.AppConstants::DEFAULT_IMAGE_EXTENSION;
        $sNewFilePath = $oFile->getPath().'/'.$sNewFileName;

        $oImage = $oImageManager->make($oFile->getRealPath());
        if (null != $iImageSize) {
            //add height and crop as a square
            if ($iImageSize < 300) {
                $oImage->fit($iImageSize, $iImageSize, function ($constraint) {
                    $constraint->upsize();
                });
            } else {
                $oImage->resize($iImageSize, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize(); //does not allow the image to be upsized in case it is smaller than cropsize
                });
            }
        }

        //save image in the jpg format. (It changes the file if it's a different format)
        $oImage->save($sNewFilePath);

        //optimize image
        $this->_optimizeJpegImage($sNewFilePath);

        return [
            'newFileName' => $sNewFileName,
            'newFilePath' => $sNewFilePath,
        ];
    }

    /**
     * Optimize image
     *
     * @param string $sFile
     */
    protected function _optimizeJpegImage($sFile)
    {

        if ('jpg' === substr($sFile, -3)) {
            exec('jpegoptim -o -m70 --all-progressive --strip-all '.$sFile.' && chmod 0755 '.$sFile); //changes the chmod of file to be able to read it
        } else {
            throw new \InvalidArgumentException(
                sprintf('Files of type %s are not allowed', substr($sFile, -3))
            );
        }
    }
}
