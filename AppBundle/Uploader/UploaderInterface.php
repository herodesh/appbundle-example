<?php

namespace Flendoc\AppBundle\Uploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface UploaderInterface
 * @package Flendoc\AppBundle\Uploader
 */
interface UploaderInterface
{
    /**
     * Process and upload chunks locally
     *
     * @param Request $oRequest
     *
     * @return mixed
     */
    public function processChunksLocally(Request $oRequest);

    /**
     * Merge locally uploaded chunks into a single file
     *
     * @param Request $oRequest
     *
     * @return mixed
     */
    public function mergeChunks(Request $oRequest);

    /**
     * @TODO Remove as this is not needed anymore
     *
     * Get Allowed Extensions for upload process
     *
     * @param bool $implode
     *
     * @return mixed
     */
    //public function getAllowedExtensions($implode = true);

    /**
     * Upload a file to storage place (eg. AWS S3, ...)
     *
     * @param UploadedFile $oFile
     * @param string       $sUploadFolder
     *
     * @return mixed
     */
    public function uploadFile(UploadedFile $oFile, $sUploadFolder);

    /**
     * Delete a file from the storage place (eg. AWS S3, ...)
     *
     * @param $sFileName
     *
     * @return mixed
     */
    public function delete($sFileName);

    /**
     * @param Request $oRequest
     * @param string  $sUploadFolder
     *
     * @return mixed
     */
    public function upload(Request $oRequest, $sUploadFolder);
}
