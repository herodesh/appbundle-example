<?php

namespace Flendoc\AppBundle\Utils;


/**
 * Class TextUrlConvertor
 * @package Flendoc\AppBundle\Utils
 */
final class TextUrlConvertor
{
    /**
     * Method that finds different occurrences of urls or email addresses in a string.
     * Inspired from:
     * @link https://github.com/liip/LiipUrlAutoConverterBundle/blob/master/Extension/UrlAutoConverterTwigExtension.php
     *
     * @param string $string input string
     *
     * @return string with replaced links
     */
    public function convertUrls($string)
    {
        $pattern        = '/(href="|src=")?([-a-zA-Zа-яёА-ЯЁ0-9@:%_\+.~#?&\*\/\/=]{2,256}\.[a-zа-яё]{2,4}\b(\/?[-\p{L}0-9@:%_\+.~#?&\*\/\/=\(\),;]*)?)/u';
        $stringFiltered = preg_replace_callback($pattern, [$this, 'callbackReplace'], $string);

        return $stringFiltered;
    }

    /**
     * Method used on converUrls
     *
     * @param $matches
     *
     * @return string
     */
    public function callbackReplace($matches)
    {
        if ($matches[1] !== '') {
            return $matches[0]; // don't modify existing <a href="">links</a> and <img src="">
        }
        $url           = $matches[2];
        $urlWithPrefix = $matches[2];
        if (strpos($url, '@') !== false) {
            $urlWithPrefix = 'mailto:'.$url;
        } elseif (strpos($url, 'https://') === 0) {
            $urlWithPrefix = $url;
        } elseif (strpos($url, 'http://') !== 0) {
            $urlWithPrefix = 'http://'.$url;
        }
        //$style = ($this->debugMode) ? ' style="color:'.$this->debugColor.'"' : '';
        // ignore tailing special characters
        // TODO: likely this could be skipped entirely with some more tweakes to the regular expression
        if (preg_match("/^(.*)(\.|\,|\?)$/", $urlWithPrefix, $matches)) {
            $urlWithPrefix = $matches[1];
            $url           = substr($url, 0, -1);
            $punctuation   = $matches[2];
        } else {
            $punctuation = '';
        }

        return '<a href="'.$urlWithPrefix.'" target="_blank">'.$url.'</a>'.$punctuation;
    }
}