<?php

namespace Flendoc\AppBundle\Utils;

use GuzzleHttp\Client;

/**
 * Class DisposableEmailCheck
 * @package Flendoc\AppBundle\Utils
 */
class DisposableEmailCheck
{
    /** @var Client */
    protected $guzzleClient;

    /** @var string */
    protected $emailCheckerKey;

    /** @var string */
    protected $kernelEnvironment;

    /**
     * DisposableEmailCheck constructor.
     *
     * @param Client $guzzleClient
     * @param        $emailCheckerKey
     * @param        $kernelEnvironment
     */
    public function __construct(Client $guzzleClient, $emailCheckerKey, $kernelEnvironment)
    {
        $this->guzzleClient      = $guzzleClient;
        $this->emailCheckerKey   = $emailCheckerKey;
        $this->kernelEnvironment = $kernelEnvironment;
    }

    /**
     * Check if email is disposable
     *
     * @param string $email
     *
     * @return bool|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function isDisposableEmail($email)
    {
        //if email is empty do not do the validation
        if (null != $email) {

            //on development environment this check is disabled
            if ("dev" === $this->kernelEnvironment) {
                return false;
            }

            $url = "http://apilayer.net/api/check?access_key=".$this->emailCheckerKey."&email=".$email."&smtp=1&format=1";

            $response      = $this->guzzleClient->request('GET', $url);
            $decodedResult = (json_decode($response->getBody()->getContents(), true));

            if ($decodedResult['disposable']) {
                return true;
            }

            return false;
        }

        return;
    }
}
