<?php

namespace Flendoc\AppBundle\Utils;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;

/**
 * Class TextUrlPlaceholderParser
 * @package Flendoc\AppBundle\Utils
 */
final class TextUrlPlaceholderParser
{
    /** @var EntityManager */
    protected $em;

    /** @var RequestStack */
    protected $requestStack;

    /** @var Router */
    protected $router;

    /**
     * TextUrlPlaceholderParser constructor.
     *
     * @param EntityManager $em
     * @param RequestStack  $requestStack
     * @param Router        $router
     */
    public function __construct(EntityManager $em, RequestStack $requestStack, Router $router)
    {
        $this->em           = $em;
        $this->requestStack = $requestStack;
        $this->router       = $router;
    }

    /**
     * @param string $sString
     * @param bool   $bIsLink There are cases where we do not want the link such as previews.
     *
     * @return null|string|string[]
     */
    public function parseText($sString, $bIsLink = true)
    {
        //sanitize a bit the string
        //the url definitions needs to be alone on single lines otherwise it will not work.
        $breaks  = ["<br />", "<br>", "<br/>", "<p>"];
        $sString = str_ireplace($breaks, "\r\n", $sString);

        $sRegex = '/\[@url\|([^|]+)\|(\d+)\](?=(?>.*\R+)+^\[\2]:\s+(\S+))|^\[\d+]:\h+\S+/m';

        if ($bIsLink) {
            return preg_replace_callback($sRegex, function ($match) {
                if (isset($match[1])) {
                    return "<a href=".strip_tags($match[3]).">".$match[1]."</a>";
                }
            }, $sString);
        } else {
            return preg_replace_callback($sRegex, function ($match) {
                if (isset($match[1])) {
                    return $match[1];
                }
            }, $sString);
        }
    }
}
