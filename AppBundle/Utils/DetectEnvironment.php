<?php

namespace Flendoc\AppBundle\Utils;

use Symfony\Component\HttpFoundation\ServerBag;

/**
 * Class DetectEnvironment
 * @package Flendoc\AppBundle\Utils
 */
final class DetectEnvironment
{
    /**
     * @param ServerBag $oServerBag
     *
     * @return array
     */
    public function detectUserEnv(ServerBag $oServerBag)
    {
        return [
            'countryName'     => $this->_getGeolocationData($oServerBag),
            'regionName'      => $this->_getGeolocationData($oServerBag, 'region'),
            'operatingSystem' => $this->_getOperatingSystem($oServerBag),
            'browser'         => $this->_getBrowser($oServerBag),
        ];
    }

    /**
     * Detect User's (aproximate) location
     *
     * @param ServerBag $oServerBag
     * @param string    $requestData
     *
     * @return mixed
     */
    protected function _getGeolocationData(ServerBag $oServerBag, $requestData = 'countryName')
    {
        return;
        $userIp      = $oServerBag->get('REMOTE_ADDR');
        $geoLocation = '';//unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$userIp"));

        return $geoLocation['geoplugin_'.$requestData];
    }

    /**
     * Detect operating system
     *
     * @param ServerBag $oServerBag
     *
     * @return string
     */
    protected function _getOperatingSystem(ServerBag $oServerBag)
    {
        $sUserAgent = $oServerBag->get('HTTP_USER_AGENT');

        if (preg_match('/linux/i', $sUserAgent)) {
            return 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $sUserAgent)) {
            return 'Mac';
        } elseif (preg_match('/windows|win32/i', $sUserAgent)) {
            return 'Windows';
        }

        return 'Unknown platform';
    }

    /**
     * Detect browser
     *
     * @param ServerBag $oServerBag
     *
     * @return string
     */
    protected function _getBrowser(ServerBag $oServerBag)
    {
        $sUserAgent = $oServerBag->get('HTTP_USER_AGENT');

        if (strpos($sUserAgent, 'Opera') || strpos($sUserAgent, 'OPR/')) {
            return 'Opera';
        } elseif (strpos($sUserAgent, 'Edge')) {
            return 'Edge';
        } elseif (strpos($sUserAgent, 'Chrome')) {
            return 'Chrome';
        } elseif (strpos($sUserAgent, 'Safari')) {
            return 'Safari';
        } elseif (strpos($sUserAgent, 'Firefox')) {
            return 'Firefox';
        } elseif (strpos($sUserAgent, 'MSIE') || strpos($sUserAgent, 'Trident/7')) {
            return 'Internet Explorer';
        }

        return 'Unknown browser';
    }
}
