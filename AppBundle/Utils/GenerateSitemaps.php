<?php

namespace Flendoc\AppBundle\Utils;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Adapter\AmazonS3\AwsS3Adapter;
use Flendoc\AppBundle\Command\GenerateSitemapsCommand;
use Flendoc\AppBundle\Constants\SearchConstants;
use Flendoc\AppBundle\Constants\SitemapsConstants;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use GuzzleHttp\Client;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class GenerateSitemaps
 * @package Flendoc\AppBundle\Services
 */
class GenerateSitemaps
{
    /** @var EntityManager */
    protected $em;

    /** @var DocumentManager */
    protected $odm;

    /** @var Router */
    protected $router;

    /** @var Client */
    protected $guzzle;

    /** @var Logger */
    protected $logger;

    /** @var string */
    protected $s3Adapter;

    /** @var string */
    protected $s3BaseUrl;

    /** @var string */
    protected $s3BucketName;

    /** @var string */
    protected $rootDir;

    /**
     * GenerateSitemaps constructor.
     *
     * @param EntityManager   $em
     * @param DocumentManager $odm
     * @param Router          $router
     * @param Client          $guzzle
     * @param Logger          $logger
     * @param AwsS3Adapter    $s3Adapter
     * @param string          $s3BaseUrl
     * @param string          $s3BucketName
     * @param string          $rootDir
     */
    public function __construct(
        EntityManager $em,
        DocumentManager $odm,
        Router $router,
        Client $guzzle,
        Logger $logger,
        AwsS3Adapter $s3Adapter,
        $s3BaseUrl,
        $s3BucketName,
        $rootDir
    ) {
        $this->em           = $em;
        $this->odm          = $odm;
        $this->router       = $router;
        $this->guzzle       = $guzzle;
        $this->logger       = $logger;
        $this->s3Adapter    = $s3Adapter;
        $this->s3BaseUrl    = $s3BaseUrl;
        $this->s3BucketName = $s3BucketName;
        $this->rootDir      = $rootDir;
    }

    /**
     * Adds Urls to sitemap
     *
     * @param null   $limit
     * @param null   $skip
     * @param string $sType
     *
     * @return array|null
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function addUrls(
        $limit = null,
        $skip = null,
        $sType = SitemapsConstants::SITEMAPS_MEDICAL_CASES
    ) {
        //we do not send sql logger in command
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $aUrls       = null;
        $aBuildArray = [];

        switch ($sType) {
            case SitemapsConstants::SITEMAPS_MEDICAL_CASES:
                $aEntries = $this->em->getRepository(MedicalCases::class)->findWithSkipAndLimit($skip, $limit);
                if (null === $aEntries) {
                    return null;
                }

                foreach ($aEntries as $oEntry) {
                    $aBuildArray = [
                        'url' => [
                            //seems that Router Generate does not care about schemas so we need to force https
                            'loc'        => str_replace('http://', 'https://',
                                $this->router->generate('doctors_view_medical_case', [
                                    'sIdentifier'          => $oEntry->getIdentifier(),
                                    'sMedicalCaseLanguage' => $oEntry->getMedicalCaseLanguageIso(),
                                    'sMedicalCaseSlug'     => $oEntry->getSlugTitle(),
                                ], Router::ABSOLUTE_URL)
                            ),
                            'lastmod'    => $oEntry->getCreatedAt(),
                            'changefreq' => 'monthly',
                        ],
                    ];
                    $aUrls[]     = $aBuildArray;
                }
                break;

            case SitemapsConstants::SITEMAPS_MEDICAL_DICTIONARY:
                $aResults = $this->em->getRepository(MedicalDictionary::class)->findWithSkipAndLimit($skip, $limit);
                foreach ($aResults as $oResult) {
                    foreach ($oResult->getMedicalDictionaryTranslation() as $oMedicalDictionaryTranslation) {
                        if (!empty($oMedicalDictionaryTranslation->getTitle())) {
                            $aBuildArray[] = [
                                'url' => [
                                    //seems that Router Generate does not care about schemas so we need to force https
                                    'loc'        => str_replace('http://', 'https://',
                                        $this->router->generate('doctors_view_medical_dictionary', [
                                            'sIdentifier'  => $oResult->getIdentifier(),
                                            'sLanguageIso' => $oMedicalDictionaryTranslation->getLanguage()->getIso(),
                                            'sSlug'        => $oMedicalDictionaryTranslation->getSlug(),
                                        ], Router::ABSOLUTE_URL)
                                    ),
                                    'lastmod'    => $oMedicalDictionaryTranslation->getCreatedAt(),
                                    'changefreq' => 'monthly',
                                ],
                            ];
                        }
                    }
                }

                $aUrls = $aBuildArray;
                break;

            default:
                return null;
                break;
        }

        //need to clear this in order to keep memory usage low
        $this->em->clear();

        return $aUrls;
    }

    /**
     * Create sitemap in memory.
     *
     * @param array  $aUrls
     * @param string $xml
     *
     * @return string
     */
    public function createSitemap($aUrls, $xml)
    {
        gc_collect_cycles(); //garbage collector memory release
        unset($xml);
        $xml = null;

        if (null != $aUrls) {
            foreach ($aUrls as $url) {

                $xml .= '<url>'.
                    '<loc>'.htmlspecialchars($url['url']['loc']).'</loc>'.
                    '<lastmod>'.date('Y-m-d', date_timestamp_get(isset($url['url']['lastmod']) ?
                        $url['url']['lastmod'] : new \DateTime())).'</lastmod>'.
                    '<changefreq>monthly</changefreq>';
                $xml .= '</url>';
            }
        }

        return trim($xml);
    }

    /**
     * Save file.
     *
     * @param string $content
     * @param string $filePath
     * @param string $fileName
     *
     * @return bool
     * @access private
     */
    public function createSitemapFile($filePath = '/tmp', $fileName)
    {
        $fileHandle = fopen($filePath.'/'.$fileName, "w");

        fwrite($fileHandle,
            '<?xml version="1.0" encoding="UTF-8"?>'.
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'.
            ' xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'
        );

        return fclose($fileHandle);
    }

    /**
     * Check if a file has xml sitemap tag closed and remove it
     *
     * @param string $filePath
     * @param        $fileName
     *
     * @return bool|int
     */
    public function checkAndRemoveClosedSitemapTag($filePath = "/tmp", $fileName)
    {
        //check if the xml tag is closed and remove the close tag
        $fileContent = file_get_contents($filePath.'/'.$fileName);
        $newContent  = str_replace('</sitemapindex>', '', $fileContent);

        return file_put_contents($filePath.'/'.$fileName, trim($newContent));
    }

    /**
     * @param        $content
     * @param string $filePath
     * @param        $fileName
     *
     * @return bool|int
     */
    public function writeContentToFile($content, $filePath = '/tmp', $fileName)
    {
        if (!file_exists($filePath.'/'.$fileName)) {
            $this->createSitemapFile($filePath, $fileName);
        }

        return file_put_contents($filePath.'/'.$fileName, trim($content), FILE_APPEND | LOCK_EX);
    }

    /**
     * @param string $filePath
     * @param        $fileName
     */
    public function closeWriteContentToFile($filePath = '/tmp', $fileName)
    {
        try {
            file_put_contents($filePath.'/'.$fileName, '</urlset>', FILE_APPEND | LOCK_EX);
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }

        return;
    }

    /**
     * @param string $filePath The local tmp path
     * @param string $generatedFileName
     *
     * @return bool|int
     */
    public function updateIndexFile($filePath, $generatedFileName, $type)
    {
        try {
            $sitemapFileName = 'sitemap.xml';
            $siteMapLocation = 'https://www.flendoc.com/sitemaps/';

            //sitemap directory
            $this->verifySitemapsLocalDirectory();

            if (!file_exists($filePath.'/'.$sitemapFileName)) {
                touch($filePath.'/'.$sitemapFileName);

                $siteIndexHeader = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
                file_put_contents($filePath.'/'.$sitemapFileName, $siteIndexHeader);
            }

            $fileContent = file_get_contents($filePath.'/'.$sitemapFileName);

            //check if file is already declared in the index file
            //if found do not add it to the index
            if (!strstr($fileContent, $generatedFileName)) {

                //remove the end of the file. We can make this operation here since the file is not that large
                $newContent = str_replace('</sitemapindex>', '', $fileContent);
                file_put_contents($filePath.'/'.$sitemapFileName, trim($newContent));

                $newSitemap = '<sitemap>'.
                    '<loc>'.$siteMapLocation.$generatedFileName.'</loc>'.
                    '<lastmod>'.date('Y-m-d').'</lastmod>'.
                    '</sitemap>';

                //put contents
                file_put_contents($filePath.'/'.$sitemapFileName, trim($newSitemap), FILE_APPEND | LOCK_EX);

                //add ending to the file
                file_put_contents($filePath.'/'.$sitemapFileName, '</sitemapindex>', FILE_APPEND | LOCK_EX);
            }
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }

        return;
    }

    /**
     * @param null $generatedFileName
     */
    public function uploadToAws($generatedFileName = null)
    {
        $aFiles = glob(GenerateSitemapsCommand::SITEMAP_FOLDER."/*");

        if ($generatedFileName) {

            try {

                $fileName = explode('/', $generatedFileName);

                $this->s3Adapter->upload(
                    GenerateSitemapsCommand::SITEMAP_FOLDER.'/'.$fileName[3],
                    $fileName[3],
                    SitemapsConstants::SITEMAPS_AWS_FOLDER
                );
            } catch (\Exception $e) {
                echo 'Error occured: '.$e->getMessage();
                $this->logger->log('error', $e->getMessage());
            }
        } else {
            foreach ($aFiles as $file) {
                try {

                    $fileName = explode('/', $file);

                    $this->s3Adapter->upload(
                        GenerateSitemapsCommand::SITEMAP_FOLDER.'/'.$fileName[3],
                        $fileName[3],
                        SitemapsConstants::SITEMAPS_AWS_FOLDER
                    );
                } catch (\Exception $e) {
                    echo 'Error occured: '.$e->getMessage();
                    $this->logger->log('error', $e->getMessage());
                }
            }
        }
    }

    /**
     * Copy sitemaps files to the directory
     */
    public function copyToLocalDirectory()
    {
        $aFiles = glob(GenerateSitemapsCommand::SITEMAP_FOLDER."/*");

        foreach ($aFiles as $file) {
            try {

                $fileName = explode('/', $file);

                //we do not need to copy the general sitemap.xml file
                if ($fileName[3] != 'sitemap.xml') {
                    rename(GenerateSitemapsCommand::SITEMAP_FOLDER.'/'.$fileName[3], $this->rootDir.'/web/sitemaps/'.$fileName[3]);
                }

            } catch (\Exception $e) {
                echo 'Error occured: '.$e->getMessage();
                $this->logger->log('error', $e->getMessage());
            }
        }
    }

    /**
     * Empty local directory sitemaps
     */
    public function emptyLocalDirectory()
    {
        $this->verifySitemapsLocalDirectory();

        $aFiles = glob($this->rootDir.'../web/sitemaps/*');
        foreach ($aFiles as $file) { // iterate files
            if (is_file($file)) {
                unlink($file);
            } // delete file
        }
    }

    /**
     * @return string
     */
    public function verifySitemapsLocalDirectory()
    {
        $localDirectory = $this->rootDir.'/web/sitemaps/';

        if (!is_dir($localDirectory)) {
            umask(000);
            mkdir($localDirectory, 0644);
        }

        return $localDirectory;
    }

    /**
     * Delete all entries from aws
     */
    public function emptyAwsDirectory()
    {
        try {
            echo PHP_EOL.'Receiving the list of object to delete...'.PHP_EOL;
            $objectsToDelete = $this->s3Adapter->getFolderContentFromAws('sitemaps');

            echo PHP_EOL.'Deleting objects...'.PHP_EOL;
            foreach ($objectsToDelete as $objectToDelete) {
                $this->s3Adapter->delete(
                    $objectToDelete['Key']
                );
            }
            echo PHP_EOL.'All Objects in folder deleted. Continue...'.PHP_EOL;
        } catch (\Exception $e) {
            echo 'An error occured: '.$e->getMessage();
            $this->logger->log('error', $e->getMessage());
        }
    }

    /**
     * Update the main sitemap file
     */
    public function updateMainSitemapFile()
    {
        //copy main file locally
        $filePath = $this->rootDir.'/web/sitemap.xml';

        rename(GenerateSitemapsCommand::SITEMAP_FOLDER.'/sitemap.xml', $filePath);
    }

    /**
     * @param string $sitemap
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function submitSitemapToGoogle()
    {
        $sitemapUrl = "http://google.com/ping?sitemap=https://www.flendoc.com/sitemap.xml";

        try {
            $response = $this->guzzle->request('GET', $sitemapUrl);

            return $response->getBody()->getContents();
        } catch (\Exception $e) {
            echo 'An error occured: '.$e->getMessage();
            $this->logger->log('error', $e->getMessage());
        }

        return;
    }
}
