<?php

namespace Flendoc\AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ApplicationLocaleListener
 * @package Flendoc\AppBundle\EventListener
 */
class ApplicationLocaleListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * ApplicationLocaleListener constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param Session               $session
     * @param                       $defaultLocale
     */
    public function __construct(TokenStorageInterface $tokenStorage, Session $session, $defaultLocale)
    {
        $this->tokenStorage  = $tokenStorage;
        $this->session       = $session;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @param GetResponseEvent $oEvent
     */
    public function onKernelRequest(GetResponseEvent $oEvent)
    {

        if (HttpKernel::MASTER_REQUEST != $oEvent->getRequestType()) {
            return;
        }

        $oRequest = $oEvent->getRequest();

        if (!$oRequest->hasPreviousSession()) {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $oRequest->attributes->get('_locale')) {
            $oRequest->getSession()->set('_locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $oRequest->setLocale($oRequest->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 15]],
        ];
    }

}
