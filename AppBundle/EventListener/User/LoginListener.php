<?php

namespace Flendoc\AppBundle\EventListener\User;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Utils\DetectEnvironment;
use Flendoc\AppBundle\Manager\Mails\MailerManager;
use Flendoc\AppBundle\Manager\Users\UserMailSettingsManager;
use Flendoc\AppBundle\Manager\Users\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Class LoginListener
 * @package Flendoc\AppBundle\EventListener\User
 */
class LoginListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /** @var FlendocManager */
    protected $flendocManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var MailerManager
     */
    protected $mailerManager;

    /**
     * @var UserMailSettingsManager
     */
    protected $userMailSettingsManager;

    /**
     * @var DetectEnvironment
     */
    protected $userEnv;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string
     */
    protected $cookies;

    /**
     * LoginListener constructor.
     *
     * @param EntityManager           $em
     * @param FlendocManager          $flendocManager
     * @param UserManager             $userManager
     * @param MailerManager           $mailerManager
     * @param UserMailSettingsManager $userMailSettingsManager
     * @param DetectEnvironment       $userEnv
     * @param Session                 $session
     */
    public function __construct(
        EntityManager $em,
        FlendocManager $flendocManager,
        UserManager $userManager,
        MailerManager $mailerManager,
        UserMailSettingsManager $userMailSettingsManager,
        DetectEnvironment $userEnv,
        Session $session
    ) {
        $this->em                      = $em;
        $this->flendocManager          = $flendocManager;
        $this->userManager             = $userManager;
        $this->mailerManager           = $mailerManager;
        $this->userMailSettingsManager = $userMailSettingsManager;
        $this->userEnv                 = $userEnv;
        $this->session                 = $session;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            SecurityEvents::INTERACTIVE_LOGIN            => 'onSecurityInteractiveLogin',
        ];
    }

    /**
     * @param AuthenticationFailureEvent $authenticationFailureEvent
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\Error
     */
    public function onAuthenticationFailure(AuthenticationFailureEvent $authenticationFailureEvent)
    {
        $sUserName = $authenticationFailureEvent->getAuthenticationToken()->getUsername();

        //get login attempts. If there are too many attempts then the account is blocked
        $this->userManager->tooManyLoginFailuresLockAccountAndNotifyUser($sUserName);

        //save user retry in redis
        $this->userManager->updateLoginAttempts($sUserName);
    }

    /**
     * @param AuthenticationEvent $authenticationEvent
     *
     * @return bool
     */
    public function onAuthenticationSuccess(AuthenticationEvent $authenticationEvent)
    {
        return true;
    }

    /**
     * @param InteractiveLoginEvent $oEvent
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $oEvent)
    {
        $oUser = $oEvent->getAuthenticationToken()->getUser();

        if (is_object($oUser)) {

            if ($oEvent->getRequest()->request->has('g-recaptcha-response') &&
                strlen($oEvent->getRequest()->get('g-recaptcha-response')) <= 0) {
                if (!$this->flendocManager->recaptchaVerify($oEvent->getRequest()->get('g-recaptcha-response'))) {

                    //we need to keep the last username
                    $oEvent->getRequest()->getSession()->set(
                        Security::LAST_USERNAME,
                        $oUser->getUsername()
                    );

                    throw new CustomUserMessageAuthenticationException($this->flendocManager->getTranslator()
                                                                                            ->trans('recaptcha.error'));
                }
            }

            //reset user retry from redis
            $this->userManager->resetLoginAttempts($oUser->getUsername());

            //update last login
            $oUser->setLastLogin(new \DateTime());

            //check if user has email settings set and add or upgrade them
            $this->userMailSettingsManager->checkUserMailSettings($oUser);
            $this->_manageCookies($oEvent, $oUser);

            $sUserDefaultLanguage = $oUser->getDefaultLanguage();
            $oEvent->getRequest()->setLocale($sUserDefaultLanguage);
            $this->session->set('_locale', $sUserDefaultLanguage);

            $this->userManager->saveObject($oUser);

            //rebuild user profile redis
            if ($oUser instanceof Doctors) {
                $aUserProfileData = $this->em->getRepository('AppBundle:Doctors\Doctors')
                                             ->findSimpleUserProfileDetails($oUser->getIdentifier());
                $this->flendocManager->rebuildAndDisplayCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $oUser->getIdentifier(), $aUserProfileData);
            }

            //if user is not verified create a flash message
            if ($oUser instanceof Doctors && !$oUser->getIsVerified()) {
                $this->session->getFlashBag()->add('verificationNeeded', true);
            }
        }
    }

    /**
     * Manage Cookies
     *
     * @param InteractiveLoginEvent $oEvent
     * @param UserInterface         $oUser
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function _manageCookies(InteractiveLoginEvent $oEvent, UserInterface $oUser)
    {
        $oUserEvent = $this->userEnv->detectUserEnv($oEvent->getRequest()->server);

        //get the existing sessions
        $aLoginSessions = (null != $this->userManager->getUserLoginSessions($oUser)) ?
            $this->userManager->getUserLoginSessions($oUser) : [];

        //if cookie is not set create one
        if (!isset($_COOKIE[$this->cookies['default']])) {
            $this->userManager->saveUserCookie($aLoginSessions, $oUser, $oUserEvent, true);
        }

        //if there is a cookie set in the browser but not set in redis
        if ((isset($_COOKIE[$this->cookies['default']])) && empty($aLoginSessions)) {
            $this->userManager->saveUserCookie($aLoginSessions, $oUser, $oUserEvent);
        }
    }
}
