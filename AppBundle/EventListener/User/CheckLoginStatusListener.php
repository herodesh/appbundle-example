<?php

namespace Flendoc\AppBundle\EventListener\User;

use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Utils\DetectEnvironment;
use Flendoc\AppBundle\Manager\Users\UserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class CheckLoginStatusListener
 * @package Flendoc\AppBundle\EventListener\User
 */
final class CheckLoginStatusListener
{

    /**
     * @var ContainerInterface
     */
    protected $authChecker;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /** @var Session */
    protected $session;

    /**
     * @var DetectEnvironment
     */
    protected $userEnv;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var string
     */
    protected $cookies;

    /**
     * CheckLoginStatusListener constructor.
     *
     * @param AuthorizationCheckerInterface $authChecker
     * @param TokenStorage                  $tokenStorage
     * @param DetectEnvironment             $userEnv
     * @param UserManager                   $userManager
     * @param                               $cookies
     */
    public function __construct(
        AuthorizationCheckerInterface $authChecker,
        TokenStorage $tokenStorage,
        Session $session,
        DetectEnvironment $userEnv,
        UserManager $userManager,
        $cookies
    ) {
        $this->authChecker  = $authChecker;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->userEnv      = $userEnv;
        $this->userManager  = $userManager;
        $this->cookies      = $cookies;
    }

    /**
     * @param GetResponseEvent $oEvent
     */
    public function onKernelRequest(GetResponseEvent $oEvent)
    {

        if (HttpKernel::MASTER_REQUEST != $oEvent->getRequestType()) {
            return;
        }

        if (!empty($this->tokenStorage->getToken()) && $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

            //checks if user has a different languages set than what is in the locale session and updates it
            $this->checkAndUpdateUserHasChangedLanguage();

            //if user has changes in roles he needs to be logged out
            if ($this->checkUserHasChangedRoles()) {
                $this->tokenStorage->setToken(null);
                $oResponse = new RedirectResponse('/');
                $oResponse->send();
            }

            $oUser         = $this->tokenStorage->getToken()->getUser();
            $oUserEvent    = $this->userEnv->detectUserEnv($oEvent->getRequest()->server);
            $defaultCookie = $this->cookies['default'];

            //check if there is a cookie set in the browser
            if (!isset($_COOKIE[$defaultCookie]) && ('/login_check' != $oEvent->getRequest()->getPathInfo())) {
                $this->tokenStorage->setToken(null);
                $oResponse = new RedirectResponse('/');
                $oResponse->send();
            }

            //if there is a cookie set in the browser manage it
            if (isset($_COOKIE[$defaultCookie])) {
                //get the list of cookies from Redis
                $aSavedSessions = (null != $this->userManager->getUserLoginSessions($oUser)) ?
                    $this->userManager->getUserLoginSessions($oUser) : [];

                //check if cookie has forceLoginStatus and kill it
                if (isset($aSavedSessions[$_COOKIE[$defaultCookie]]) && $aSavedSessions[$_COOKIE[$defaultCookie]]['forceLogin']) {
                    //then delete the cookie from the list
                    unset($aSavedSessions[$_COOKIE[$defaultCookie]]);
                    $this->userManager->saveUserLoginSessionsToRedis($oUser, $aSavedSessions);
                    setcookie($defaultCookie, $_COOKIE[$defaultCookie], 1, '/');

                    $oResponse = new RedirectResponse('/');
                    $oResponse->send();
                }

                //update the cookie
                if (isset($aSavedSessions[$_COOKIE[$defaultCookie]]) && !$aSavedSessions[$_COOKIE[$defaultCookie]]['forceLogin']) {
                    return $this->userManager->saveUserCookie($aSavedSessions, $oUser, $oUserEvent);
                }

                //create a new cookie
                if (!isset($aSavedSessions[$_COOKIE[$defaultCookie]])) {
                    return $this->userManager->saveUserCookie($aSavedSessions, $oUser, $oUserEvent, true);
                }
            }
        }
    }

    /**
     * Checks if user has different roles.
     * This might happen, for example, on Document validations or if Admin changes user's Roles
     *
     * @return bool
     */
    private function checkUserHasChangedRoles()
    {
        $oUser      = $this->tokenStorage->getToken()->getUser();
        $aUserRoles = ($oUser instanceof Admins) ? $oUser->getAdminRoles() : $oUser->getDoctorRoles();

        $aCurrentUserRoles = [];
        $aTokenRoles       = [];

        foreach ($this->tokenStorage->getToken()->getRoles() as $tokenRole) {
            $aTokenRoles[] = $tokenRole->getRole();
        }

        foreach ($aUserRoles as $userRole) {
            $aCurrentUserRoles[] = $userRole->getName();
        }

        $aFullDiff = (array_merge(array_diff($aTokenRoles, $aCurrentUserRoles), array_diff($aCurrentUserRoles, $aTokenRoles)));

        if (empty($aFullDiff)) {
            return false;
        }

        return true;
    }

    /**
     * Check if user has a different language than locale and update the locale
     */
    private function checkAndUpdateUserHasChangedLanguage()
    {
        $sUserDefaultLanguage = $this->tokenStorage->getToken()->getUser()->getDefaultLanguage();
        $sSessionLocale = $this->session->get('_locale');

        if ($sUserDefaultLanguage != $sSessionLocale) {
            $this->session->set('_locale', $sUserDefaultLanguage);
        }
    }
}
