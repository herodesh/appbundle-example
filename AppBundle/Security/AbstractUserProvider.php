<?php

namespace Flendoc\AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Inspired by FOSUserBundle
 *
 * Class AbstractUserProvider
 * @package Flendoc\AppBundle\Security
 */
abstract class AbstractUserProvider implements UserProviderInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * UserProvider constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    abstract protected function setUserRepository();

    /**
     * @return mixed
     */
    abstract protected function getUserRepository();

    /**
     * @return mixed
     */
    abstract protected function setUserClass();

    /**
     * @return mixed
     */
    abstract protected function getUserClass();

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $oUser = $this->em->getRepository($this->getUserRepository())->findOneByUsername($username);

        if (!$oUser) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $oUser;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $oUser)
    {
        if (!$this->supportsClass(get_class($oUser))) {
            throw new UnsupportedUserException(sprintf('Instance of %s are not supported".', get_class($oUser)));
        }

        if (null === $reloadedUser = $this->em->getRepository($this->getUserRepository())
                                              ->findOneById($oUser->getId())
        ) {
            throw new UsernameNotFoundException(sprintf('User with ID "%s" could not be reloaded.', $oUser->getId()));
        }

        return $reloadedUser;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        $userClass = $this->getUserRepository();

        return $userClass === $class || is_subclass_of($class, $userClass);
    }
}
