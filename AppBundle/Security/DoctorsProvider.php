<?php

namespace Flendoc\AppBundle\Security;

use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Inspired by FOSUserBundle
 *
 * Class DoctorsProvider
 * @package Flendoc\AppBundle\Security
 */
class DoctorsProvider extends AbstractUserProvider
{
    /** @var  Doctors */
    protected $userClass;

    /** @var  Doctors */
    protected $repositoryClass;

    /**
     * @inheritdoc
     */
    public function setUserClass()
    {
        $this->userClass = Doctors::class;
    }

    /**
     * @return mixed
     */
    public function getUserClass()
    {
        return $this->userClass;
    }

    /**
     * @inheritdoc
     */
    public function setUserRepository()
    {
        $this->repositoryClass = Doctors::class;
    }

    /**
     * @return string
     */
    public function getUserRepository()
    {
        return Doctors::class;
    }

}
