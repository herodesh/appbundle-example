<?php

namespace Flendoc\AppBundle\Security\Handler;

use Flendoc\AppBundle\Entity\Admins\Admins;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Translation\Translator;

/**
 * Class AuthenticationSuccessHandler
 * @package Flendoc\AppBundle\Security\Handler
 */
class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /** @var RouterInterface */
    protected $router;

    /**
     * @var FlashBagInterface
     */
    protected $flashBag;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param HttpUtils $httpUtils
     * @param           $router
     * @param           $flashBag
     * @param           $translator
     * @param array     $options
     */
    public function __construct(
        HttpUtils $httpUtils,
        $router,
        $flashBag,
        $translator,
        $options = []
    ) {
        parent::__construct($httpUtils, $options);
        $this->router     = $router;
        $this->flashBag   = $flashBag;
        $this->translator = $translator;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $oUser = $token->getUser();

        if ($oUser instanceof Admins) {
            if ($oUser->getHasGeneratedPassword()) {
                $this->flashBag->add(
                    'success',
                    $this->translator->trans('admin.update.password')
                );

                return new RedirectResponse($this->router->generate('admin_user_change_password'));
            }
        }

        return parent::onAuthenticationSuccess($request, $token);
    }
}
