<?php

namespace Flendoc\AppBundle\Security;

use Flendoc\AppBundle\Entity\Admins\Admins;

/**
 * Inspired by FOSUserBundle
 *
 * Class AdminsProvider
 * @package Flendoc\AppBundle\Security
 */
class AdminsProvider extends AbstractUserProvider
{
    /** @var Admins */
    protected $userClass;

    /** @var  Admins */
    protected $repositoryClass;

    /**
     * @inheritdoc
     */
    public function setUserClass()
    {
        $this->userClass = Admins::class;
    }

    /**
     * @return Admins
     */
    public function getUserClass()
    {
        return $this->userClass;
    }

    /**
     * @inheritdoc
     */
    public function setUserRepository()
    {
        $this->repositoryClass = Admins::class;
    }

    /**
     * @return mixed|string
     */
    public function getUserRepository(): string
    {
        return Admins::class;
    }

}
