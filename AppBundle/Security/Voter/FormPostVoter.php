<?php

namespace Flendoc\AppBundle\Security\Voter;

use Flendoc\AppBundle\Entity\Doctors\DoctorProfile;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserCanSeeVoter
 * @package Flendoc\AppBundle\Security\Voter
 *
 * @url https://stovepipe.systems/post/symfony-security-roles-vs-voters
 */
class FormPostVoter extends Voter
{
    const FORM_POST_VOTER = 'formPostVoter';

    /** @var AccessDecisionManagerInterface */
    protected $decisionManager;

    /**
     * UserCanSeeVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return $attribute == self::FORM_POST_VOTER;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ('anon.' === $token->getUser()) {

            //user can register
            if ($this->userCanRegister($subject)) {
                return true;
            }

            //user forgot his password
            if ($this->userForgotPassword($subject)) {
                return true;
            }

            //user reset his password
            if ($this->userCanResetPassword($subject)) {
                return true;
            }
        }

        //not verified users cannot take actions
        if ($this->decisionManager->decide($token, ['ROLE_DOCTOR_NOT_VERIFIED'])) {
            // user changes his password?
            if ($this->isPasswordChange($subject)) {
                return true;
            }

            //user has forgot his password
            if ($this->isEmailChange($subject)) {
                return true;
            }

            //update user profile should be available
            if ($subject instanceof DoctorProfile || null == $subject) {
                return true;
            }

            return false;
        }

        //everybody else can
        if (
            $this->decisionManager->decide($token, ['ROLE_DOCTOR']) ||
            $this->decisionManager->decide($token, ['ROLE_ADMIN']) ||
            $this->decisionManager->decide($token, ['ROLE_GHOST'])
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param null $subject
     *
     * @return bool
     */
    public function isPasswordChange($subject = null)
    {
        if (null != $subject && $subject instanceof Doctors && null != $subject->getPlainPassword()) {
            return true;
        }

        return false;
    }

    /**
     * @param null $subject
     *
     * @return bool
     */
    public function isEmailChange($subject = null)
    {
        if (null != $subject && array_key_exists('email', $subject)) {
            return true;
        }

        return false;
    }

    /**
     * @param $subject
     *
     * @return bool|void
     */
    public function userCanRegister($subject)
    {
        //if user does not exist
        if (
            $subject instanceof Doctors &&
            null === $subject->getIdentifier() &&
            null === $subject->getPassword() &&
            null != $subject->getPlainPassword()
        ) {
            return true;
        }

        return;
    }

    /**
     * @param $subject
     *
     * @return bool|void
     */
    public function userForgotPassword($subject)
    {
        if (array_key_exists('email', $subject)) {
            return true;
        }

        return;
    }

    /**
     * @param $subject
     *
     * @return bool|void
     */
    public function userCanResetPassword($subject)
    {
        if ($subject instanceof Doctors) {
            return true;
        }

        return;
    }
}
