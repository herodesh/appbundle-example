<?php

namespace Flendoc\AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserCanSeeVoter
 * @package Flendoc\AppBundle\Security\Voter
 *
 * @url https://stovepipe.systems/post/symfony-security-roles-vs-voters
 */
class UserCanSeeVoter extends Voter
{
    const USER_CAN_SEE = 'USER_CAN_SEE';

    /** @var AccessDecisionManagerInterface */
    protected $decisionManager;

    /**
     * UserCanSeeVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return $attribute == self::USER_CAN_SEE;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        //order is important? Seems so ...

        //not verified users cannot take actions
        if ($this->decisionManager->decide($token, ['ROLE_DOCTOR_NOT_VERIFIED'])) {
            return false;
        }

        //everybody else can
        if (
            $this->decisionManager->decide($token, ['ROLE_DOCTOR']) ||
            $this->decisionManager->decide($token, ['ROLE_ADMIN']) ||
            $this->decisionManager->decide($token, ['ROLE_GHOST'])
        ) {
            return true;
        }

        return false;
    }
}
