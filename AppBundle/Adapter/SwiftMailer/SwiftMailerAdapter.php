<?php

namespace Flendoc\AppBundle\Adapter\SwiftMailer;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Adapter\AbstractMailer;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Adapter\MailerAdapterInterface;
use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Entity\Mails\Mails;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Manager\Mails\MailerManager;
use Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SwiftMailer
 * @package Flendoc\AppBundle\Adapter\SwiftMailer
 */
final class SwiftMailerAdapter extends AbstractMailer implements MailerAdapterInterface
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var MailerManager
     */
    protected $mailerManager;

    /**
     * @var TwigEngine
     */
    protected $template;

    /**
     * @var EntityManager
     */
    protected $em;

    /** @var FlendocManager */
    protected $flendocManager;

    protected $logger;

    /** @var string */
    protected $host;

    /**
     * SwiftMailerAdapter constructor.
     *
     * SwiftMailerAdapter constructor.
     *
     * @param                $senderEmail
     * @param                $senderName
     * @param \Swift_Mailer  $mailer
     * @param MailerManager  $mailerManager
     * @param EntityManager  $em
     * @param TwigEngine     $template
     * @param FlendocManager $flendocManager
     * @param Logger         $logger
     * @param                $host
     */
    public function __construct(
        $senderEmail,
        $senderName,
        \Swift_Mailer $mailer,
        MailerManager $mailerManager,
        EntityManager $em,
        TwigEngine $template,
        FlendocManager $flendocManager,
        Logger $logger,
        $host
    ) {
        $this->mailer         = $mailer;
        $this->mailerManager  = $mailerManager;
        $this->em             = $em;
        $this->template       = $template;
        $this->flendocManager = $flendocManager;
        $this->logger         = $logger;
        $this->host           = $host;

        parent::__construct($senderEmail, $senderName);
    }

    /**
     * Send a normal email
     *
     * @param string             $sEmailIdentifier
     * @param UserInterface|null $oUser         Can be null in case we need to send email to outside users
     * @param array|null         $aParameters
     * @param array|null         $aSubjectParameters
     * @param bool               $bForceSending We are using this to force the sending of the email. There might be
     *                                          cases when the email is not in the users cache. For example the user
     *                                          never logged in
     *
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\Error
     */

    public function send(
        $sEmailIdentifier,
        UserInterface $oUser,
        array $aParameters = [],
        array $aSubjectParameters = [],
        $bForceSending = false
    ) {
        //get user data from redis
        $aUserMailSettings = $this->flendocManager->displayCacheData(CacheKeyPrefixes::USER_EMAIL_SETTINGS, $oUser->getIdentifier());

        //if user does not accepts email don't do anything
        if (!$bForceSending && !$this->mailerManager->userAcceptsEmailFrom($oUser, $aUserMailSettings, $sEmailIdentifier)) {
            return;
        }

        //get email basic details
        $aEmail = $this->_findEmail($sEmailIdentifier);

        if ($aEmail['isAccount'] || (!$aEmail['isAccount'] && !$aEmail['isAdministrative'])) {
            $this->_sendEmail($oUser, $sEmailIdentifier, $aParameters, $aSubjectParameters);
        }
    }

    /**
     * Sends email to the admins
     *
     * @param            $sEmailIdentifier
     * @param            $adminRole
     * @param array|null $aParameters
     * @param array|null $aSubjectParameters
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Twig\Error\Error
     */
    public function sendToAdmin(
        $sEmailIdentifier,
        $adminRole,
        array $aParameters = [],
        array $aSubjectParameters = []
    ) {
        //get email basic details
        $aEmail = $this->_findEmail($sEmailIdentifier);

        //send administrative
        if ($aEmail['isAdministrative']) {
            $this->_sendAdministrativeEmail($sEmailIdentifier, $adminRole, $aParameters, $aSubjectParameters);
        }
    }

    /**
     * Sending external emails. The difference with the normal sending is that there is not check done in order to stop sending or not.
     *
     * @param $sEmailIdentifier
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Twig\Error\Error
     */
    /**
     * @param string        $sEmailIdentifier
     * @param UserInterface $oSenderUser
     * @param string        $sTo
     * @param array|null    $aParameters
     * @param array|null    $aSubjectParameters
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Twig\Error\Error
     */
    public function sendExternalMail(
        $sEmailIdentifier,
        UserInterface $oSenderUser,
        $sTo,
        array $aParameters = [],
        array $aSubjectParameters = []
    ) {
        $aEmail = $this->_findEmail($sEmailIdentifier);

        if ($aEmail['isExternal'] && !$aEmail['isAdministrative'] && !$aEmail['isAccount']) {
            $this->_sendExternalEmail($oSenderUser, $sTo, $sEmailIdentifier, $aParameters, $aSubjectParameters);
        }
    }

    /**
     * Generate email content to be added to template
     *
     * @param UserInterface $oUser              Needed to get User preferred language
     * @param string        $sEmailIdentifier   Email Identifier
     * @param array|null    $aParameters        Parameters to be passed in the email
     * @param array|null    $aSubjectParameters Parameters that will be added to the subject
     *
     * @return string
     * @throws \Exception
     */
    protected function _generateEmailContent(
        UserInterface $oUser,
        $sEmailIdentifier,
        array $aParameters = [],
        array $aSubjectParameters = []
    ) {

        $oUserLanguage = $this->em->getRepository('AppBundle:Languages\Languages')
                                  ->findOneByIso($oUser->getDefaultLanguage());
        $aEmail        = $this->em->getRepository('AppBundle:Mails\Mails')
                                  ->findOneByLanguageAndEmailIdentifier($oUserLanguage, $sEmailIdentifier);

        $aMailVariables    = $aEmail['variables'];
        $aSubjectVariables = $aEmail['subjectVariables'];

        try {
            if (!empty($aMailVariables) && count(explode(',', $aMailVariables)) != count($aParameters)) {
                throw new \Exception('Needed variables don\'t match controller given variables');
            }

            if (!empty($aSubjectVariables) && count(explode(',', $aSubjectVariables)) != count($aSubjectParameters)) {
                throw new \Exception('Needed subject variables don\'t match controller given variables');
            }
        } catch (\Exception $exception) {
            $this->logger->log('error', $exception->getMessage());

            return null;
        }

        $aPopulatedEmailContent['content'] = vsprintf($aEmail['content'], $aParameters);
        $aPopulatedEmailContent['subject'] = vsprintf($aEmail['subject'], $aSubjectParameters);

        return $aPopulatedEmailContent;
    }

    /**
     * Render mail content
     *
     * @param array  $aMailContent
     * @param string $sFormat
     *
     * @return string
     * @throws \Twig\Error\Error
     */
    protected function _renderEmailContent($aMailContent, $sFormat = 'html')
    {
        $aRenderParameters = [
            'emailContent' => ($sFormat == 'text') ?
                strip_tags(preg_replace('/<a href=\"(.*)\">(.*)<\/a>/', '$1', $aMailContent['content'])) :
                $aMailContent['content'],
            'emailTitle'   => isset($aMailContent['subject']) ? $aMailContent['subject'] : null,
            'baseUrl'      => $this->host,
        ];

        if ($sFormat == 'text') {
            return $this->template->render('@App/MailTemplates/text_base.html.twig', $aRenderParameters);
        }

        return $this->template->render('@App/MailTemplates/base.html.twig', $aRenderParameters);
    }

    /**
     * Send the email
     *
     * @inheritdoc
     */
    protected function _executeSendEmail($sTo, $sSubject, $sTemplateHtml, $sTemplateText, $sIdentifier)
    {
        $oSwiftMessage = new \Swift_Message();
        $message       = $oSwiftMessage
            ->setSubject($sSubject)
            ->setFrom([$this->mailerSenderEmail => $this->mailerSenderName])
            ->setTo($sTo)
            ->setBody($sTemplateHtml, 'text/html')
            ->addPart($sTemplateText, 'text/plain');
        $message->getHeaders()->addTextHeader('X-Message-ID', $sIdentifier);

        return $this->mailer->send($message);
    }

    /**
     * Send account email
     *
     * @param            $oUser
     * @param string     $sEmailIdentifier
     * @param array|null $aParameters
     * @param array|null $aSubjectParameters
     *
     * @throws \Twig\Error\Error
     */
    private function _sendEmail($oUser, $sEmailIdentifier, array $aParameters = [], array $aSubjectParameters = [])
    {

        $aEmailContent = $this->_generateEmailContent($oUser, $sEmailIdentifier, $aParameters, $aSubjectParameters);

        //this means something went wrong with generation of the content
        if (null === $aEmailContent) {
            return;
        }

        $this->_executeSendEmail($oUser->getUsername(), $aEmailContent['subject'], $this->_renderEmailContent($aEmailContent), $this->_renderEmailContent($aEmailContent, 'text'), $sEmailIdentifier);

        return;
    }

    /**
     * @param UserInterface $oSenderUser
     * @param string        $sTo              Email address to whom the email is sent
     * @param string        $sEmailIdentifier The email identifier
     * @param array|null    $aParameters
     * @param array|null    $aSubjectParameters
     *
     * @throws \Twig\Error\Error
     */
    private function _sendExternalEmail(
        $oSenderUser,
        $sTo,
        $sEmailIdentifier,
        array $aParameters = [],
        array $aSubjectParameters = []
    ) {
        $aEmailContent = $this->_generateEmailContent($oSenderUser, $sEmailIdentifier, $aParameters, $aSubjectParameters);

        if (null === $aEmailContent) {
            return;
        }

        $this->_executeSendEmail($sTo, $aEmailContent['subject'], $this->_renderEmailContent($aEmailContent), $this->_renderEmailContent($aEmailContent, 'text'), $sEmailIdentifier);

        return;
    }

    /**
     * Send Administrative email
     *
     * @param string     $sIdentifier
     * @param string     $adminRole
     * @param array|null $aParameters
     * @param array|null $aSubjectParameters
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Twig\Error\Error
     */
    private function _sendAdministrativeEmail(
        $sIdentifier,
        $adminRole,
        array $aParameters = [],
        array $aSubjectParameters = []
    ) {
        /** @var $oUserRepo */
        $oUserRepo = $this->em->getRepository(Admins::class);
        //find all mail managers
        $aMailReceivers = $oUserRepo->findAdminMailReceivers($adminRole);

        /** @var Admins $oMailReceiver */
        foreach ($aMailReceivers as $oMailReceiver) {
            $sEmailContent = $this->_generateEmailContent($oMailReceiver, $sIdentifier, $aParameters, $aSubjectParameters);

            //this means something went wrong with generation of the content
            if (null === $sEmailContent) {
                continue;
            }

            $this->_executeSendEmail($oMailReceiver->getUsername(), $sEmailContent['subject'], $this->_renderEmailContent($sEmailContent), $this->_renderEmailContent($sEmailContent, 'text'), $sIdentifier);
        }
    }

    /**
     * @param string $sEmailIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Twig\Error\Error
     */
    private function _findEmail($sEmailIdentifier)
    {
        //get email basic details
        $aEmail = $this->em->getRepository(Mails::class)->findOneEmailByEmailIdentifier($sEmailIdentifier);

        if (null == $aEmail) {

            //notify mail management admin that emails are missing
            $aMailReceivers = $this->em->getRepository(Admins::class)->findAdminMailReceivers('ROLE_EMAIL_MANAGEMENT');

            $aEmailContent['content'] = sprintf("Email with identifier %s was not found", $sEmailIdentifier);
            $aEmailContent['subject'] = 'Flendoc.com [ALERT] - Email not found';

            foreach ($aMailReceivers as $oMailReceiver) {
                $this->_executeSendEmail(
                    $oMailReceiver->getUsername(),
                    $aEmailContent['subject'],
                    $this->_renderEmailContent($aEmailContent),
                    $this->_renderEmailContent($aEmailContent, 'text'),
                    'emailAlert'
                );
            }

            return;

        }

        return $aEmail;
    }
}
