<?php

namespace Flendoc\AppBundle\Adapter\Elasticsearch;

use Elasticsearch\ClientBuilder;

/**
 * Class ElasticsearchAdapter
 * @package Flendoc\AppBundle\Adapter\Elasticsearch
 */
final class ElasticsearchAdapter
{
    /** @var ClientBuilder */
    public $client;

    /** @var string */
    protected $host;

    /** @var int */
    protected $port;

    /** @var string */
    protected $user;

    /** @var string */
    protected $pass;

    /**
     * ElasticsearchAdapter constructor.
     *
     * @param ClientBuilder $client
     * @param string        $host
     * @param int           $port
     * @param string        $user
     * @param string        $pass
     */
    public function __construct(ClientBuilder $client, $host, $port, $user, $pass)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;

        $this->client = $client::create()
            ->setHosts([
                [
                    'host' => $host,
                    'port' => $port,
                    'user' => $user,
                    'pass' => $pass,
                ],
            ])
            ->build()
        ;
    }
}
