<?php

namespace Flendoc\AppBundle\Adapter\Slugify;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Monolog\Logger;

/**
 * Class SlugifyManager
 * @package Flendoc\AppBundle\Adapter\Slugify
 */
final class SlugifyManager
{
    /** @var Slugify */
    protected $slugify;

    /** @var EntityManager */
    protected $em;

    /** @var Logger */
    protected $logger;

    /** @var string */
    protected $kernelDir;

    /**
     * SlugifyManager constructor.
     *
     * @param Slugify       $slugify
     * @param EntityManager $em
     * @param Logger        $logger
     * @param string        $kernelDir
     */
    public function __construct(Slugify $slugify, EntityManager $em, Logger $logger, $kernelDir)
    {
        $this->slugify = $slugify;
        $this->em      = $em;
        $this->logger  = $logger;
    }

    /**
     * Generates slug
     *
     * @param $sString
     * @param $sLanguageIso
     *
     * @return string
     */
    public function generateSlug($sString, $sLanguageIso)
    {
        $oLanguage = $this->em->getRepository(Languages::class)->findOneBy(['iso' => $sLanguageIso]);

        $this->slugify->activateRuleSet('default');

        //some rulesets does not exist but they can go fine with the default one.
        try {
            if (is_file($this->kernelDir.'../vendor/cocur/slugify/Resources/rules/'.$oLanguage->getNameIdentifier().'.json')) {
                $this->slugify->activateRuleSet($oLanguage->getNameIdentifier());
            }
        } catch (\Exception $e) {
            $this->logger->log('info', $e->getMessage());
        }

        return $this->slugify->slugify($sString);
    }
}
