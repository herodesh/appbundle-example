<?php

namespace Flendoc\AppBundle\Adapter\Cache;

use Flendoc\AppBundle\Constants\AppConstants;

/**
 * Class CacheKeyPrefixes
 * @package Flendoc\AppBundle\Adapter\Cache
 *
 * Define a map of the key prefixes used for our cached objects and their default expiration time
 *
 * naming convention :
 * 1. The allowed characters for constant names are 'A-Z' (all uppercase) and are using the symbol '_' *only to separate sub domains*
 * 2. The chosen name must reflect meaningful path: starting from a more general domain and ending to the more specific one, of course beside good description
 * 3. To avoid key conflicts the value of the constant must reflect is name in lowercase, substituting '_' with '.'
 * (this means that allowed characters for the key values are 'a-z' and the dot symbol '.')
 */
abstract class CacheKeyPrefixes
{
    /** User connected devices */
    const USER_CONNECTED_DEVICES_SESSIONS = "ucd";

    /** @var string User login attempts */
    const USER_LOGIN_ATTEMPTS = "ula";

    /** User profile details (incl. country, city, spoken languages) */
    const USER_PROFILE_DETAILS = "upd";

    /** stores the user email settings */
    const USER_EMAIL_SETTINGS = "ues";

    /** Application cities */
    const APPLICATION_CITIES = "acit";

    /** Application countries */
    const APPLICATION_COUNTRIES = "ac";

    /** Resumes */
    const RESUMES = "r";

    /** Weekly medical dictionary */
    const WEEKLY_MEDICAL_DICTIONARY = 'wmd';

    /** Daily medical dictionary */
    const DAILY_MEDICAL_DICTIONARY = 'dmd';

    /**
     * Get default expiration time
     *
     * 1 minute = 60 (seconds), - 60 seconds
     * 1 hour = 60 (seconds) * 60 (minutes) - 3600 seconds
     * 1 day = 60 (seconds) * 60 (minutes) * 24 (hours) * 1 (number of days) - 86400 seconds
     * x days = 60 (seconds) * 60 (minutes) * 24 (hours) * x (number of days)
     *
     * "USER_*" prefixed caches are always linked to the user identifier. This will be cleared when specific actions related to user are
     * happening. For example changing the user default language
     * When we have a situation like this we set the $item key as the user identifier
     * $oCacheManager->setKeyPrefix(CacheKeyPrefixes::USER_CONNECTED_DEVICES_SESSIONS);
     * $item = $oCacheManager->getItem($oUser->getIdentifier());
     *
     * @param string $prefix the 'prefix key' constant to identify a particular domain of cache item
     *
     * @return int seconds before the cache item expire
     */
    public static function getExpireTime($prefix)
    {
        switch ($prefix) {

            case self::DAILY_MEDICAL_DICTIONARY:
                return 60 * 60 * 24 * 1; //1 day

            case self::WEEKLY_MEDICAL_DICTIONARY:
                return 60 * 60 * 24 * 7; //7 days

            case self::USER_CONNECTED_DEVICES_SESSIONS :
            case self::USER_PROFILE_DETAILS :
            case self::USER_EMAIL_SETTINGS:
                return 60 * 60 * 24 * 30; // 30 days

            case self::RESUMES:
            case self::APPLICATION_COUNTRIES:
            case self::APPLICATION_CITIES:
            case self::USER_LOGIN_ATTEMPTS:
                return 60 * 60 * 24 * 30 * 365; //1 year
        }

        return AppConstants::CACHE_DEFAULT_EXPIRE_TIME;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getConstants()
    {
        return (new \ReflectionClass(__CLASS__))->getConstants();
    }

    /**
     * CacheKeyPrefixes constructor.
     */
    private final function __construct() { }
}
