<?php

namespace Flendoc\AppBundle\Adapter\Cache;

use Psr\Cache\CacheItemPoolInterface;

/**
 * Interface CacheWrapperInterface
 * @package Flendoc\AppBundle\Adapter\Cache
 */
interface CacheWrapperInterface extends CacheItemPoolInterface
{
    /**
     * Set the current key prefix
     *
     * @param string $prefix the key prefix
     *
     * @throws \Psr\Cache\InvalidArgumentException if $prefix is not a constants from CacheKeyPrefixes
     */
    public function setKeyPrefix($prefix);

    /**
     * Get current key prefix
     *
     * @return string key prefix
     */
    public function getKeyPrefix();

    /**
     * Reset key prefix
     *
     * This method is useful to ensure that the code will follow, will have to set the key prefix
     * trough the call setKeyPrefix(..) before any further cache operations
     */
    public function resetKeyPrefix();
}
