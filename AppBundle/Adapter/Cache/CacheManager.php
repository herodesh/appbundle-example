<?php

namespace Flendoc\AppBundle\Adapter\Cache;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\CacheItemInterface;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Adapter\Cache\CacheWrapperInterface;
use Flendoc\AppBundle\Adapter\Cache\InvalidArgumentException;

/**
 * Class CacheManager
 *
 * it is a full PSR-6 implementation.
 * @link http://www.php-fig.org/psr/psr-6/
 *
 * This class on top of the CacheItemPoolInterface transparently implements: 'key prefixes' and more flexible expire times
 *
 * note: 'key prefix' term has been adopted over a more natural 'namespace' to avoid a possible
 * meaning confusion of 'namespace' utilized in the symfony cache component
 *
 * @package CP\CacheBundle\Service
 */
class CacheManager implements CacheWrapperInterface
{
    /** @var CacheItemPoolInterface */
    protected $cAdapter;

    /** @var null */
    protected $keyPrefix = null;

    /** @var array */
    private $allKeyPrefixes = [];

    /**
     * CacheManager constructor.
     *
     * @param CacheItemPoolInterface $cacheAdapter
     */
    public function __construct(
        CacheItemPoolInterface $cacheAdapter
    ) {
        $this->cAdapter       = $cacheAdapter;
        $this->allKeyPrefixes = CacheKeyPrefixes::getConstants();
    }

    /**
     * Set the current key prefix
     *
     * @param string $prefix the key prefix
     *
     * @throws InvalidArgumentException if $prefix is not a constants from CacheKeyPrefixes
     */
    public function setKeyPrefix($prefix)
    {
        if (in_array($prefix, $this->allKeyPrefixes)) {
            $this->keyPrefix = $prefix;
        } else {
            throw new InvalidArgumentException('the key prefix was not recognized');
        }
    }

    /**
     * Get current key prefix
     *
     * @return string key prefix
     */
    public function getKeyPrefix()
    {
        return $this->keyPrefix;
    }

    /**
     * Reset key prefix
     */
    public function resetKeyPrefix()
    {
        $this->keyPrefix = null;
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException if no key prefix was set or the key is invalid
     */
    public function getItem($key)
    {
        $this->validateKey($key);
        $item = $this->cAdapter->getItem('='. $this->keyPrefix .'='. $key);
        // set time expire is always needed, whatever isHit() returns
        $item->expiresAfter(CacheKeyPrefixes::getExpireTime($this->keyPrefix));
        return $item;
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException if no key prefix was set or the key is invalid
     */
    public function getItems(array $keys = array())
    {
        $prefixedKeys = [];
        foreach ($keys as $key) {
            $this->validateKey($key);
            $prefixedKeys[] = '='. $this->keyPrefix .'='. $key;
        }
        $items = $this->cAdapter->getItems($prefixedKeys);
        $data = [];
        foreach ($items as $index => $item) {
            // set time expire is always needed, whatever isHit() returns
            $item->expiresAfter(CacheKeyPrefixes::getExpireTime($this->keyPrefix));
            $data[$index]=$item;
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException if no key prefix was set or the key is invalid
     */
    public function hasItem($key)
    {
        $this->validateKey($key);
        return $this->cAdapter->hasItem('='. $this->keyPrefix .'='. $key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return $this->cAdapter->clear();
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException if no key prefix was set or the key is invalid
     */
    public function deleteItem($key)
    {
        $this->validateKey($key);
        return $this->cAdapter->deleteItem('='. $this->keyPrefix .'='. $key);
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException if no key prefix was set or the key is invalid
     */
    public function deleteItems(array $keys)
    {
        $prefixedKeys = [];
        foreach ($keys as $key) {
            $this->validateKey($key);
            $prefixedKeys[] = '='. $this->keyPrefix .'='. $key;
        }
        return $this->cAdapter->deleteItems($prefixedKeys);
    }

    /**
     * {@inheritdoc}
     */
    public function save(CacheItemInterface $item)
    {
        // the following checks are performed to avoid operations which are probably in the wrong context
        if (is_null($this->keyPrefix) || (strpos($item->getKey(), '='. $this->keyPrefix .'=') === false)) {
            @trigger_error(
                'the item was not saved because the current keyPrefix is a different one or is not set',
                E_USER_WARNING
            );

            return false;
        }
        return $this->cAdapter->save($item);
    }

    /**
     * {@inheritdoc}
     */
    public function saveDeferred(CacheItemInterface $item)
    {
        // the following checks are performed to avoid operations which are probably in the wrong context
        if (is_null($this->keyPrefix) || (strpos($item->getKey(), '='. $this->keyPrefix .'=') === false)) {
            @trigger_error(
                'the item was not saved because the current keyPrefix is a different one or is not set',
                E_USER_WARNING
            );

            return false;
        }
        return $this->cAdapter->saveDeferred($item);
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        return $this->cAdapter->commit();
    }

    /**
     * validate a key
     *
     * @param string $key key to be validate
     *
     * @throws InvalidArgumentException if key has an invalid format or key prefix was not set
     */
    public function validateKey($key)
    {
        if (is_null($this->keyPrefix)) {
            throw new InvalidArgumentException("the key prefix was not set");
        }
        if (!is_scalar($key) || (trim($key) == "")) {
            throw new InvalidArgumentException('cache key is invalid');
        }
        if (false !== strpbrk($key, '{}()/\@:=')) {
            throw new InvalidArgumentException(sprintf('cache key "%s" contains reserved characters {}()/\@:=', $key));
        }
    }

}
