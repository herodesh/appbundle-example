<?php

namespace Flendoc\AppBundle\Adapter\Cache;

/**
 * Class InvalidArgumentException
 * @package Flendoc\AppBundle\Adapter\Cache
 *
 * @link    http://www.php-fig.org/psr/psr-6/
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Psr\Cache\InvalidArgumentException
{
}
