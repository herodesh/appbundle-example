<?php

namespace Flendoc\AppBundle\Adapter;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface MailerAdapterInterface
 * @package Flendoc\AppBundle\Adapter
 */
interface MailerAdapterInterface
{
    /**
     * Send email
     *
     * @param string $sEmailIdentifier
     * @param UserInterface $oFormDataOrUser
     * @param array         $aParameters
     * @param array         $aSubjectParameters
     *
     * @return mixed
     */
    public function send($sEmailIdentifier, UserInterface $oFormDataOrUser, array $aParameters = [], array $aSubjectParameters = []);

}
