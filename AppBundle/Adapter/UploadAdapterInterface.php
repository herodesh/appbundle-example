<?php

namespace Flendoc\AppBundle\Adapter;

/**
 * Interface AdapterInterface
 * @package Flendoc\AppBundle\Adapter
 */
interface UploadAdapterInterface
{
    /**
     * Upload a file
     *
     * @param $sFilePath
     * @param $sFileName
     * @param $sFolderName
     *
     * @return mixed
     */
    public function upload($sFilePath, $sFileName, $sFolderName);

    /**
     * Remove a file
     *
     * @param $sFile
     *
     * @return bool
     */
    public function delete($sFile);

    /**
     * Rename a file
     *
     * @param $sSource
     * @param $sTarget
     *
     * @return mixed
     */
    public function rename($sSource, $sTarget);

}
