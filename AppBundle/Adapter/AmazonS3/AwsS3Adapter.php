<?php

namespace Flendoc\AppBundle\Adapter\AmazonS3;

use Aws\Command;
use Aws\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;
use Flendoc\AppBundle\Adapter\UploadAdapterInterface;
use Flendoc\AppBundle\Constants\AppConstants;
use Monolog\Logger;

/**
 * Class AwsS3
 * @package Flendoc\AppBundle\Adapter\AmazonS3
 */
final class AwsS3Adapter implements UploadAdapterInterface
{
    /** @var Logger */
    protected $logger;

    /**
     * @var string
     */
    protected $awsKey;

    /**
     * @var string
     */
    protected $awsSecret;

    /**
     * @var string
     */
    protected $bucket;

    /**
     * AwsS3Adapter constructor.
     *
     * @param Logger $logger
     * @param        $sAwsKey
     * @param        $sAwsSecret
     * @param        $sBucket
     */
    public function __construct(Logger $logger, $sAwsKey, $sAwsSecret, $sBucket)
    {
        $this->logger    = $logger;
        $this->awsKey    = $sAwsKey;
        $this->awsSecret = $sAwsSecret;
        $this->bucket    = $sBucket;
    }

    /**
     * Initialize client
     *
     * @return S3Client
     */
    public function getS3()
    {
        return new S3Client([
            'version'                 => 'latest',
            'region'                  => 'eu-central-1',
            'use_accelerate_endpoint' => true,
            'credentials'             => [
                'key'    => $this->awsKey,
                'secret' => $this->awsSecret,
            ],
        ]);
    }

    /**
     * @param string $sFilePath The full path of the file (ex: folder/file.jpg)
     * @param string $sFileName
     * @param string $sFolderName
     *
     * @return bool
     */
    public function upload($sFilePath, $sFileName, $sFolderName)
    {
        $uploader = new MultipartUploader($this->getS3(), $sFilePath, [
            'bucket'          => $this->bucket,
            'key'             => $sFolderName.'/'.$sFileName,
            'before_initiate' => function (Command $command) {
                $command['CacheControl'] = 'max-age='.AppConstants::UPLOAD_CACHE_CONTROL;
            },
        ]);

        do {
            try {
                $result = $uploader->upload();
            } catch (MultipartUploadException $e) {
                //remove the multipart upload or try again?
                $uploader = new MultipartUploader($this->getS3(), $sFilePath, [
                    'state' => $e->getState(),
                ]);
            }
        } while (!isset($result));

        return false;
    }

    /**
     * Gets an object from AWS
     *
     * @param $sFilePath
     *
     * @return \Aws\Result
     */
    public function getObjectFromAws($sFilePath)
    {
        try {
            return $this->getS3()->getObject([
                'Bucket' => $this->bucket,
                'Key'    => $sFilePath,
            ]);
        } catch (\Exception $e) {
            //@TODO log error to monolog

            return $this->getS3()->getObject([
                'Bucket' => $this->bucket,
                'Key'    => "default-not-found-image.png",
            ]);
        }
    }

    /**
     * @param string $sFolderName The folder we should be looking in AWS
     *
     * @return bool|\Iterator
     */
    public function getFolderContentFromAws($sFolderName)
    {
        try {
            return $this->getS3()->getIterator('ListObjects', [
                'Bucket' => $this->bucket,
                'Prefix' => $sFolderName,
            ]);
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage().' '.$e->getLine().' '.$e->getFile());
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function rename($sSource, $sTarget)
    {
        // TODO: Implement rename() method.
    }

    /**
     * @param $sFile
     *
     * @return bool
     */
    public function delete($sFile)
    {
        //check if object exists
        if (!$this->getS3()->doesObjectExist($this->bucket, $sFile)) {
            return false;
        } else {
            $this->getS3()->deleteObject([
                'Bucket' => $this->bucket,
                'Key'    => $sFile,
            ]);

            return true;
        }

        return false;
    }

}
