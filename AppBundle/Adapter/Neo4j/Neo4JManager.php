<?php

namespace Flendoc\AppBundle\Adapter\Neo4j;

use GraphAware\Neo4j\Client\ClientBuilder;

/**
 * Class Neo4JManager
 * @package Flendoc\AppBundle\Adapter\Neo4j
 */
final class Neo4JManager
{
    /** @var \GraphAware\Neo4j\Client\Client */
    public $client;

    /** @var string */
    protected $boltUrl;

    /** @var string */
    protected $httpUrl;

    /** @var string */
    protected $neo4jUser;

    /** @var string */
    protected $neo4jPassword;

    /**
     * Neo4JManager constructor.
     *
     * @param ClientBuilder $client
     * @param string        $boltUrl
     * @param string        $httpUrl
     * @param string        $neo4jUser
     * @param string        $neo4jPassword
     */
    public function __construct(ClientBuilder $client, $httpUrl, $boltUrl, $neo4jUser, $neo4jPassword)
    {
        $this->boltUrl       = $boltUrl;
        $this->httpUrl       = $httpUrl;
        $this->neo4jUser     = $neo4jUser;
        $this->neo4jPassword = $neo4jPassword;

        $this->client = $client::create()
                               ->addConnection('default', 'http://'.$this->neo4jUser.':'.$this->neo4jPassword.'@'.$this->httpUrl)
                               ->addConnection('bolt', 'bolt://'.$this->neo4jUser.':'.$this->neo4jPassword.'@'.$this->boltUrl)
                               ->build();
    }
}
