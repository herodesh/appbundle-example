<?php

namespace Flendoc\AppBundle\Adapter;

/**
 * Class AbstractMailer
 * @package Flendoc\AppBundle\Adapter
 */
abstract class AbstractMailer
{
    /**
     * @var $mailerSenderEmail
     */
    protected $mailerSenderEmail;

    /**
     * @var $mailerSenderName
     */
    protected $mailerSenderName;

    /**
     * AbstractEmailManager constructor.
     *
     * @param $mailerSenderEmail
     * @param $mailerSenderName
     */
    public function __construct(
        $mailerSenderEmail,
        $mailerSenderName
    ) {
        $this->mailerSenderEmail = $mailerSenderEmail;
        $this->mailerSenderName  = $mailerSenderName;
    }
}
