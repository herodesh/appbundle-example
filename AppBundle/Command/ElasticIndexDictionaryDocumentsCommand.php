<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndexUsersCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndexDictionaryDocumentsCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:index:dictionary-documents")
            ->setDescription("Index all active dictionary-documents in the system")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var EntityManager $oDoctrineManager */
        $oDoctrineManager = $oContainer->get('doctrine')->getManager();
        /** @var $oElasticsearchManager $oElasticsearchManager */
        $oElasticsearchIndexerManager =  $oContainer->get('flendoc.elasticsearch.manager');
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        $iBatchSize = $oContainer->getParameter('elasticsearch_batch_size');

        if (!$oElasticClient->indices()->exists(['index' => 'dictionary-documents'])) {
            $output->writeln(sprintf('<error>Index \'%s\' not found</error>', 'dictionary-documents'));
            $oLogger->err(sprintf('Index \'%s\' not found', 'dictionary-documents'));

            return;
        }

        $aResponse = $oDoctrineManager->getRepository(MedicalDictionary::class)->getNumberOfActiveMedicalDictionaryDocuments();
        $iTotalMedicalDictionaries = (int)$aResponse;
        $oQuery = $oDoctrineManager
            ->createQuery('
                SELECT md FROM Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary as md WHERE md.isActive= :isActive')
            ->setParameters([
                'isActive' => true,
            ])
        ;
        $oIterableResult = $oQuery->iterate();
        $aDictionaryDocuments = [];

        foreach ($oIterableResult as $iKey => $aRow) {
            $aDictionaryDocuments[] = $aRow[0];

            if (0 === ($iKey + 1) % $iBatchSize) {
                $iBatchNumber = ($iKey + 1) / $iBatchSize;
                $oElasticsearchIndexerManager->bulkIndexDictionaryDocuments($aDictionaryDocuments, $iTotalMedicalDictionaries, $iBatchSize, $iBatchNumber, $output);
                $aDictionaryDocuments = [];
            }

            $oDoctrineManager->detach($aRow[0]);
        }

        if (sizeof($aDictionaryDocuments)) {
            $oElasticsearchIndexerManager->bulkIndexDictionaryDocuments($aDictionaryDocuments, $iTotalMedicalDictionaries, $iBatchSize, null, $output);
        }
    }
}
