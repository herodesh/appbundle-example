<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Manager\Users\DeleteUserManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Command\LockableTrait;

/**
 * Class DeleteInactiveUsersAccountsCommand
 * @package Flendoc\AppBundle\Command
 */
class DeleteInactiveUsersAccountsCommand extends ContainerAwareCommand
{

    use LockableTrait;

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("users:delete-inactive-user-accounts")
             ->setDescription("Delete users that never activate their profile");
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        if (!$this->lock()) {
            $output->writeln('<info>This command is already running in another process.</info>');

            return;
        }

        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var EntityManager $oDoctrineManager */
        $oDoctrineManager = $oContainer->get('doctrine')->getManager();
        /** @var DeleteUserManager $oDeleteUserManager */
        $oDeleteUserManager = $oContainer->get('flendoc.delete.user.manager');

        //find users that never confirmed themselves
        $aUsers = $oDoctrineManager->getRepository(Doctors::class)->findUnconfirmedUsers();

        $output->writeln('<info>Started to send delete users</info>');

        /** @var Doctors $oUser */
        foreach ($aUsers as $oUser) {

            $oCurrentDate  = new \DateTime();
            $oInterval     = $oCurrentDate->diff($oUser->getCreatedAt());
            $iDaysInterval = $oInterval->days;

            if ($iDaysInterval > AppConstants::INACTIVE_USERS_END_DAYS) {
                //delete user
                $oDeleteUserManager->deleteUser($oUser->getIdentifier());
            }
        }

        $output->writeln('<info>Finished deleting users</info>');

        //release the lock
        $this->release();
    }
}
