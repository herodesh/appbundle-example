<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndicesDeleteCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndicesDeleteCommand extends ContainerAwareCommand
{

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:indices:delete")
            ->setDescription("Delete Elasticsearch indices")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        $sIndices = $oContainer->getParameter('elasticsearch_indices');
        $aIndices = explode(',', $sIndices);

        /*
         * Delete mapping-migrations, users, medical-cases, dictionary-documents indices
         */
        foreach ($aIndices as $sIndex ) {
            if ($oElasticClient->indices()->exists(['index' => $sIndex])) {
                try {
                    $oElasticClient->indices()->delete(['index' => $sIndex]);
                    $output->writeln(sprintf('<info>Deleted index: %s</info>', $sIndex));
                } catch (\Exception $oException) {
                    $output->writeln(sprintf('<error>Failed to delete index %s: %s</error>', $sIndex, $oException->getMessage()));
                    $oLogger->err(sprintf('Failed to delete index %s: %s', $sIndex, $oException->getMessage()));
                }
            }
        }
    }
}
