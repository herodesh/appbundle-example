<?php

namespace Flendoc\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ElasticIndicesGenerateMappingFileCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndicesGenerateMappingFileCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:indices:generate-mapping-file")
            ->setDescription("Generate Elasticsearch mapping file to ElasticMapping directory")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $sRootDirectory      = $this->getContainer()->get('kernel')->getProjectDir();
        $sElasticMappingPath = $sRootDirectory.'/app/ElasticMapping';

        $aMappingSchema = [
            [
                'index' => 'index-name',
                'mappings' => [
                    'properties' => [
                        'property' => [
                            'type' => 'property-type',
                        ]
                    ]
                ],
            ]
        ];

        $oNow = new \DateTime();
        $sNow = $oNow->format('YmdHis');
        $sFileName = sprintf('%s%s.json', 'Version', $sNow);

        $fp = fopen(sprintf('%s/%s', $sElasticMappingPath, $sFileName), 'w');
        fwrite($fp, json_encode($aMappingSchema));
        fclose($fp);

        $output->writeln(sprintf('<info>Generated mapping file \'%s\' </info>', $sFileName));
    }
}
