<?php

namespace Flendoc\AppBundle\Command;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Constants\SitemapsConstants;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateSitemapsCommand
 * @package Flendoc\AppBundle\Command
 */
class GenerateSitemapsCommand extends ContainerAwareCommand
{
    use LockableTrait;

    /** @var string */
    const SITEMAP_FOLDER = '/tmp/flendoc_sitemaps';

    /** @var int 40 MB */
    const SITEMAP_MAX_SIZE = 41943040;

    /**
     * Configure command.
     */
    public function configure()
    {
        $this->setName('sitemaps:generate')
             ->setDescription('Command that generates the site maps')
             ->addOption('sitemapType', null, InputOption::VALUE_REQUIRED, 'What type of Sitemaps needs to be generated');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('<info>This command is already running in another process.</info>');

            return;
        }

        $container               = $this->getContainer();
        $oGenerateSitemapService = $container->get('flendoc.generate.sitemap');

        $output->writeln('<info>Empty local tmp directory.</info>');
        $this->emptyTmpDirectory(); //clean local tmp directory

        $progress = new ProgressBar($output);
        $progress->start();
        $progress->setRedrawFrequency(1);

        $aTypes = [SitemapsConstants::SITEMAPS_MEDICAL_CASES, SitemapsConstants::SITEMAPS_MEDICAL_DICTIONARY];

        //if we do not have any type passed we generate all sitemaps at once
        if (null == $input->getOption('sitemapType')) {
            foreach ($aTypes as $sType) {
                $this->_writeSitemapContent($sType, $output, $progress, $sType);
            }
        } else {
            $this->_writeSitemapContent($this->_getFileNamePrefix($input->getOption('type')), $output, $progress, $input->getOption('type'));
        }

        $progress->finish();

        $output->writeln('<info>Copying to sitemaps folder...</info>');

        try {
            $output->writeln('<info>Empty local sitemaps directory...</info>');
            $oGenerateSitemapService->emptyLocalDirectory(); //clean local directory directory
            $output->writeln('<info>Copying new files to sitemaps folder...</info>');
            $oGenerateSitemapService->copyToLocalDirectory(); //copy new files to local directory

            $output->writeln('<info>Update main sitemap.xml file...</info>');
            $oGenerateSitemapService->updateMainSitemapFile();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $container->get('logger')->log('error', $e->getMessage());
        }

        //ping google search
        $output->writeln('<info>Ping Google for new sitemaps...</info>');
        $oGenerateSitemapService->submitSitemapToGoogle();

        $this->release();
    }

    /**
     * Write sitemap content for a specific type of data (Ex. MedicalCases or MedicalDictionary, etc)
     *
     * @param $fileNamePrefix
     * @param $output
     * @param $progress
     * @param $type
     *
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    private function _writeSitemapContent($fileNamePrefix, $output, $progress, $type)
    {
        gc_collect_cycles(); //garbage collector memory release
        $container               = $this->getContainer();
        $oGenerateSitemapService = $container->get('flendoc.generate.sitemap');
        $maxUrlPerSitemap        = SitemapsConstants::SITEMAP_MAX_URL;

        $i   = 0;
        $j   = 0;
        $k   = 0;
        $xml = null;

        if (!is_dir($this->awsTmpFolder())) {
            $output->writeln('Could not create tmp folder. Exiting...');
        }

        //create urls to be inserted in the sitemaps
        while (null != $aUrls = $oGenerateSitemapService->addUrls(AppConstants::BATCH_SIZE, $i++ * AppConstants::BATCH_SIZE, $type)) {

            //saves few KB so we do not stress too much the memory
            $em = $container->get('doctrine')->getManager();
            $em->getConnection()->getConfiguration()->setSQLLogger(null);
            $em->clear();

            unset($xml);
            $xml = null;

            $xml .= $oGenerateSitemapService->createSitemap($aUrls, $xml);

            //append to file
            if ($maxUrlPerSitemap >= $j * AppConstants::BATCH_SIZE) {

                $oGenerateSitemapService->writeContentToFile($xml, self::SITEMAP_FOLDER, $fileNamePrefix.$k.'.xml');

                //if filesize exceeds the max allowed size create a new file
                //or the numbers of urls is smaller than batch size - end of execution
                if (filesize(self::SITEMAP_FOLDER.'/'.$fileNamePrefix.$k.'.xml') >= self::SITEMAP_MAX_SIZE || (count($aUrls) < AppConstants::BATCH_SIZE)) {

                    $oGenerateSitemapService->closeWriteContentToFile(self::SITEMAP_FOLDER, $fileNamePrefix.$k.'.xml');

                    //update the index file
                    $oGenerateSitemapService->updateIndexFile(self::SITEMAP_FOLDER, $fileNamePrefix.$k.'.xml', $type);

                    $j = 0;
                    $k++;

                    $output->writeln('Writing '.$fileNamePrefix.$k.'.xml');
                    continue;
                }
                unset($xml);
                $xml = null;

            } else {

                //close writing to file
                $oGenerateSitemapService->closeWriteContentToFile(self::SITEMAP_FOLDER, $fileNamePrefix.$k.'.xml');

                //update the index file
                $oGenerateSitemapService->updateIndexFile(self::SITEMAP_FOLDER, $fileNamePrefix.$k.'.xml', $type);

                //reset everything
                unset($xml);
                $xml = null;
                $j   = 0;

                $k++;
                $output->writeln('Writing '.$fileNamePrefix.$k.'.xml');
                gc_collect_cycles(); //garbage collector memory release
            }

            $progress->advance(AppConstants::BATCH_SIZE);
            $j++;
        }
    }

    /**
     * Empty temporary directory
     */
    private function emptyTmpDirectory()
    {
        $aFiles = glob(self::SITEMAP_FOLDER."/*");

        foreach ($aFiles as $sFile) {
            if (is_file($sFile)) {
                unlink($sFile);
            }
        }
    }

    /**
     * Checks and create amazon Tmp folder
     *
     * @return string
     */
    private function awsTmpFolder()
    {
        //uses system /tmp folder
        $awsTmpFolder = self::SITEMAP_FOLDER.'/';

        if (!is_dir($awsTmpFolder)) {
            umask(000);
            mkdir($awsTmpFolder, 0755);
        }

        return $awsTmpFolder;
    }
}
