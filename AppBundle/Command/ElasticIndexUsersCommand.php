<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndexUsersCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndexUsersCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:index:users")
            ->setDescription("Index all enabled users in the system")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var EntityManager $oDoctrineManager */
        $oDoctrineManager = $oContainer->get('doctrine')->getManager();
        /** @var $oElasticsearchManager $oElasticsearchManager */
        $oElasticsearchIndexerManager =  $oContainer->get('flendoc.elasticsearch.manager');
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        $iBatchSize = $oContainer->getParameter('elasticsearch_batch_size');

        if (!$oElasticClient->indices()->exists(['index' => 'users'])) {
            $output->writeln(sprintf('<error>Index \'%s\' not found</error>', 'users'));
            $oLogger->err(sprintf('Index \'%s\' not found', 'users'));

            return;
        }

        $aResponse = $oDoctrineManager->getRepository(Doctors::class)->getNumberOfEnabledUsers();
        $iTotalDoctors = (int)$aResponse;
        $oQuery = $oDoctrineManager
            ->createQuery('SELECT d from Flendoc\AppBundle\Entity\Doctors\Doctors d WHERE d.enabled = :enabled')
            ->setParameter('enabled', true)
        ;
        $oIterableResult = $oQuery->iterate();
        $aDoctors = [];

        foreach ($oIterableResult as $iKey => $aRow) {
            $aDoctors[] = $aRow[0];

            if (0 === ($iKey + 1) % $iBatchSize) {
                $iBatchNumber = ($iKey + 1) / $iBatchSize;
                $oElasticsearchIndexerManager->bulkIndexUsers($aDoctors, $iTotalDoctors, $iBatchSize, $iBatchNumber, $output);
                $aDoctors = [];
            }

            $oDoctrineManager->detach($aRow[0]);
        }

        if (sizeof($aDoctors)) {
            $oElasticsearchIndexerManager->bulkIndexUsers($aDoctors, $iTotalDoctors, $iBatchSize, null, $output);
        }
    }
}
