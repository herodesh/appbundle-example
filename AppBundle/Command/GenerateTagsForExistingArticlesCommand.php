<?php

namespace Flendoc\AppBundle\Command;

use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateTagsForExistingArticlesCommand
 * @package Flendoc\AppBundle\Command
 */
class GenerateTagsForExistingArticlesCommand extends ContainerAwareCommand
{

    use LockableTrait;

    /**
     * Configure command.
     */
    public function configure()
    {
        $this->setName('hashtags:generate')
             ->setDescription('Command that generates hash tags to the existing articles');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('<info>This command is already running in another process.</info>');

            return;
        }

        $container = $this->getContainer();
        $em        = $container->get('doctrine')->getManager();

        //generate medical dictionary tags
        $this->generateMedicalDictionaryTags($container, $em);
        //generate medical cases tags
        $this->generateMedicalCasesTags($container, $em);

        $em->flush();
    }

    /**
     * Generate Medical DictionaryTags
     *
     * @param $container
     * @param $em
     */
    private function generateMedicalDictionaryTags($container, $em)
    {
        //find medical dictionary entries that have no tags
        $aMedicalDictionary        = $em->getRepository(MedicalDictionary::class)->findAll();
        $oMedicalDictionaryManager = $container->get('flendoc.medical.dictionary.manager');

        foreach ($aMedicalDictionary as $oMedicalDictionary) {
            $oMedicalDictionaryManager->generateMedicalDictionaryHashTags($oMedicalDictionary);
            sleep(2);
        }
    }

    /**
     * Generate Medical DictionaryTags
     *
     * @param $container
     * @param $em
     */
    private function generateMedicalCasesTags($container, $em)
    {
        //find medical dictionary entries that have no tags
        $aMedicalCases        = $em->getRepository(MedicalCases::class)->findAll();
        $oMedicalCasesManager = $container->get('flendoc.medical.case.manager');

        foreach ($aMedicalCases as $oMedicalCase) {
            $oMedicalCasesManager->generateMedicalCaseHashTags($oMedicalCase);
            sleep(2);
        }
    }
}
