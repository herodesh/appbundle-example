<?php

namespace Flendoc\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AssetsVersionRenewalCommand
 * @package Flendoc\AppBundle\Command
 */
class AssetsVersionRenewalCommand extends ContainerAwareCommand
{

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("assetic:version:renewal")
             ->setDescription("Update the version of the assets");
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $aFileExtensions   = ['.yml'];
        $sRootDirectory    = $this->getContainer()->get("kernel")->getProjectDir();
        $sUniqueIdentifier = uniqid();

        foreach ($aFileExtensions as $extension) {
            //we use substr to get rid of the first slash which seems to cause troubles
            $sParamYml = $sRootDirectory."/app/config/parameters".$extension;

            if (!file_exists($sParamYml)) {
                throw new \Exception("Could not locate parameters files");
            }

            $aParsedContent = Yaml::parse(file_get_contents($sParamYml));

            $aParsedContent["parameters"]["assets_version"] = $sUniqueIdentifier;

            file_put_contents($sParamYml, Yaml::dump($aParsedContent));
        }
    }
}
