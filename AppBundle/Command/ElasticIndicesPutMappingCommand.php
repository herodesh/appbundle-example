<?php

namespace Flendoc\AppBundle\Command;

use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndicesPutMappingCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndicesPutMappingCommand extends ContainerAwareCommand
{

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:indices:put-mapping")
            ->setDescription("Elasticsearch indices mappings")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');

        $sRootDirectory      = $this->getContainer()->get('kernel')->getProjectDir();
        $sElasticMappingPath = $sRootDirectory.'/app/ElasticMapping';
        $aFiles = scandir($sElasticMappingPath);
        $aFiles = array_diff($aFiles, ['.', '..']);
        $sFileVersionDate = null;

        $sMappingMigrationsVersion = $this->getMappingMigrationsVersion($oElasticClient, $output, $oLogger);

        foreach ($aFiles as $key => $sFile) {
            // Get date from file name
            $aFileInfo = explode('.', $sFile);
            $sFileVersionDate = substr($aFileInfo[0], 7);

            if ($sMappingMigrationsVersion && $sFileVersionDate <= $sMappingMigrationsVersion) {
                continue;
            }

            // Read JSON file
            $sJson = file_get_contents($sElasticMappingPath . '/' . $sFile);
            // Decode JSON
            $aJsonData = json_decode($sJson, true);

            $output->writeln(sprintf('<info>Mapping version: %s</info>', $sFileVersionDate));

            foreach ($aJsonData as $aIndexMapping) {
                // Get index name
                $sIndex = array_key_exists('index', $aIndexMapping) ? $aIndexMapping['index'] : null;
                // Get index mapping
                $aMappings = array_key_exists('mappings', $aIndexMapping) ? $aIndexMapping['mappings'] : null;

                if (!$sIndex) {
                    $output->writeln(sprintf('<error>Key \'index\' is missing from JSON object with id %d</error>', $key));
                    $oLogger->err(sprintf('Key \'index\' is missing from JSON object with id %d', $key));
                    continue;
                }

                if (!$aMappings) {
                    $output->writeln(sprintf('<error>Key \'mappings\' is missing from JSON object with id %d</error>', $key));
                    $oLogger->err(sprintf('Key \'mappings\' is missing from JSON object with id %d', $key));
                    continue;
                }

                $this->putMapping($sIndex, $aMappings, $oElasticClient, $output, $oLogger);
            }
        }

        $this->setMappingMigrationsVersion($sFileVersionDate, $oElasticClient, $output, $oLogger);
    }

    /**
     * Put index mapping
     *
     * @param string          $sIndex
     * @param array           $aMappings
     * @param Client          $oElasticClient
     * @param OutputInterface $oOutput
     * @param Logger          $oLogger
     *
     * @throws \Exception
     */
    private function putMapping($sIndex, $aMappings, $oElasticClient, $oOutput, $oLogger)
    {
            if ($oElasticClient->indices()->exists(['index' => $sIndex])) {
                try {
                    $aMapping = [
                        'index' => $sIndex,
                        'type'  => $sIndex,
                        'body'  => [
                            $sIndex => $aMappings,
                        ]
                    ];
                    $oElasticClient->indices()->putMapping($aMapping);

                    $oOutput->writeln(json_encode($aMapping));
                } catch (\Exception $oException) {
                    $oOutput->writeln(sprintf('<error>Failed to put mapping on index \'%s\': %s</error>', $sIndex, $oException->getMessage()));
                    $oLogger->err(sprintf('Failed to put mapping on index \'%s\': %s', $sIndex, $oException->getMessage()));
                }
            } else {
                $oOutput->writeln(sprintf('<info>Index \'%s\' not found.</info>', $sIndex));
            }
    }

    /**
     * Add or updated version on mapping-migrations index with the date from
     * the last file that has been processed.
     *
     * @param string|null     $sFileVersionDate
     * @param Client          $oElasticClient
     * @param OutputInterface $oOutput
     * @param Logger          $oLogger
     *
     * @return mixed
     */
    private function setMappingMigrationsVersion($sFileVersionDate, $oElasticClient, $oOutput, $oLogger):void
    {
        /*
         * Add/Update document with the date of the last
         * file from which mapping data has been saved.
         */
        if ($sFileVersionDate) {
            /*
             * Search for existing mapping migrations version
             */
            $aResponse = $oElasticClient->search([
                'index' => 'mapping-migrations',
                'type'  => 'mapping-migrations',
            ]);

            $aDocumentId = [];

            if (array_key_exists('hits', $aResponse)) {
                $aHits = $aResponse['hits'];

                if (sizeof($aHits['hits'])) {
                    $aHit = $aHits['hits'][0];

                    $aDocumentId['id'] = $aHit['_id'];
                }
            }

            $aParams = [
                'index' => 'mapping-migrations',
                'type'  => 'mapping-migrations',
                'body'  => [
                    'version' => $sFileVersionDate,
                ],
            ];

            if ($aDocumentId) {
                $aParams['id'] = $aDocumentId['id'];
            }

            try {
                $oElasticClient->index($aParams);
                $oOutput->writeln(sprintf('<info>Mapping migrations version has been updated to: %s</info>', $sFileVersionDate));

            } catch (\Exception $oException) {
                $oOutput->writeln(sprintf('<error>Failed to updated mapping migrations version: %s</error>', $oException->getMessage()));
                $oLogger->err(sprintf('Failed to updated mapping migrations version: %s', $oException->getMessage()));
            }
        }
    }

    /**
     * Return mapping migrations version stored in ES.
     *
     * @param Client          $oElasticClient
     * @param OutputInterface $oOutput
     * @param Logger          $oLogger
     *
     * @return string
     * @throws \Exception
     */
    private function getMappingMigrationsVersion($oElasticClient, $oOutput, $oLogger)
    {
        try {
            $aResponse = $oElasticClient->search([
                'index' => 'mapping-migrations',
                'type'  => 'mapping-migrations',
            ]);

            if (array_key_exists('hits', $aResponse)) {
                $aHits = $aResponse['hits'];

                if (sizeof($aHits['hits'])) {
                    $aHit = $aHits['hits'][0];

                    return $aHit['_source']['version'];
                }
            }
        } catch (\Exception $oException) {
            $oOutput->writeln(sprintf('<error>Failed to get mapping migrations version from \'mapping-migrations\' index : %s</error>', $oException->getMessage()));
            $oLogger->err(sprintf('Failed to get mapping migrations version from \'mapping-migrations\' index : %s', $oException->getMessage()));
        }

        return null;
    }
}
