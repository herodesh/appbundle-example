<?php

namespace Flendoc\AppBundle\Command;

use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndicesCreateCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndicesCreateCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:indices:create")
            ->setDescription("Create Elasticsearch indices")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        $sIndices = $oContainer->getParameter('elasticsearch_indices');

        if ($oElasticClient->indices()->exists(['index' => $sIndices])) {
            $output->writeln('<info>Indices already exists</info>');

            return;
        }

        /*
         * Create mapping-migrations, users, medical-cases, dictionary-documents indices
         */
        try {
            $aIndices = explode(',', $sIndices);

            foreach ($aIndices as $sIndex ) {
                $oElasticClient->indices()->create([
                    'index' => $sIndex,
                    'body'  => [
                        'settings' => [
                            'number_of_shards'   => 5,
                            'number_of_replicas' => 1,
                        ],
                    ],
                ]);

                if ('mapping-migrations' === $sIndex) {
                    $this->putMappingMigrationsIndex($oElasticClient, $output, $oLogger);
                }

                $output->writeln(sprintf('<info>Index created: %s</info>', $sIndex));
            }
        } catch (\Exception $oException) {
            $output->writeln(sprintf('<error>Failed to create indices: %s</error>', $oException->getMessage()));
            $oLogger->err(sprintf('Failed to create indices: %s', $oException->getMessage()));
            $this->rollBackIndices($oElasticClient, $output, $oLogger);
        }
    }

    /**
     * Put mapping schema for mapping-migrations index.
     * Mapping-migrations index is used in order to keep
     * track of the last mapping version file that has been
     * run on the system.
     *
     * @param Client          $oElasticClient
     * @param OutputInterface $oOutput
     * @param Logger          $oLogger
     *
     * @return bool
     * @throws \Exception
     */
    public function putMappingMigrationsIndex($oElasticClient, $oOutput, $oLogger)
    {
        try {
            /*
             * Put mapping schema for mapping-migrations index
             */
            $oElasticClient->indices()->putMapping([
                'index' => 'mapping-migrations',
                'type'  => 'mapping-migrations',
                'body'  => [
                    'mapping-migrations' => [
                        'properties' => [
                            'version' => [
                                'type' => 'keyword',
                            ],
                        ],
                    ],
                ]
            ]);

            return true;
        } catch (\Exception $oException) {
            $oOutput->writeln(sprintf('<error>Failed to put mapping on index \'mapping-migrations\': %s</error>', $oException->getMessage()));
            $oLogger->err(sprintf('Failed to put mapping on index \'mapping-migrations\': %s', $oException->getMessage()));

            return false;
        }
    }

    /**
     * If any error is raised during indices create function
     * we have to delete all the indices that has already been created.
     *
     * @param Client          $oElasticClient
     * @param OutputInterface $oOutput
     * @param Logger          $oLogger
     *
     * @throws \Exception
     */
    private function rollBackIndices($oElasticClient, $oOutput, $oLogger)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        $sIndices = $oContainer->getParameter('elasticsearch_indices');
        $oOutput->writeln('<info>Rolling back created indices</info>');
        $oLogger->info('Rolling back created indices');

        $aIndices = explode(',', $sIndices);

        /*
         * Delete mapping-migrations, users, medical-cases, dictionary-documents indices
         */
        foreach ($aIndices as $sIndex ) {
            if ($oElasticClient->indices()->exists(['index' => $sIndex])) {
                try {
                    $oElasticClient->indices()->delete(['index' => $sIndex]);
                    $oOutput->writeln(sprintf('<info>Deleted index: %s</info>', $sIndex));
                    $oLogger->info(sprintf('Deleted index: %s', $sIndex));
                } catch (\Exception $oException) {
                    $oOutput->writeln(sprintf('<error>Failed to delete index %s: %s</error>', $sIndex, $oException->getMessage()));
                    $oLogger->err(sprintf('Failed to delete index %s: %s', $sIndex, $oException->getMessage()));
                }
            }
        }
    }
}
