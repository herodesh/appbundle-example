<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocuments;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes;
use Flendoc\AppBundle\Entity\HashTags\HashTags;
use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUs;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups;
use Flendoc\AppBundle\Entity\LegalAndHelp\Legal;
use Flendoc\AppBundle\Entity\Mails\Mails;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Form\RegistrationDocuments\Type\RegistrationDocumentsType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RemoveEmptyTranslationsCommand
 * @package Flendoc\AppBundle\Command
 */
class RemoveEmptyTranslationsCommand extends ContainerAwareCommand
{

    use LockableTrait;

    /**
     * Configure command.
     */
    public function configure()
    {
        $this->setName('translations:clear')
             ->setDescription('This clears the empty translations for all entities');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('<info>This command is already running in another process.</info>');

            return;
        }

        $container = $this->getContainer();
        /** @var EntityManager $em */
        $em = $container->get('doctrine')->getManager();

        //clears entities
        $this->clearRegistrationDocumentTypes($em);

        $this->clearRejectionReasons($em);

        $this->clearMedicalDictionary($em);

        $this->clearFaqGroups($em);

        $this->clearFaq($em);

        $this->clearMails($em);

        $this->clearLegal($em);

        $this->clearContactUs($em);

        $this->clearSkills($em);
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearRegistrationDocumentTypes(EntityManager $em)
    {
        $aRegDocsType = $em->getRepository(RegistrationDocumentTypes::class)->findAll();
        foreach ($aRegDocsType as $oRegDocsType) {
            $aRegDocsTypeTranslations = $oRegDocsType->getRegistrationDocumentTypeLanguages();

            foreach ($aRegDocsTypeTranslations as $oRegDocsTypeTranslation) {
                if (null === $oRegDocsTypeTranslation->getName()) {
                    $em->remove($oRegDocsTypeTranslation);

                }
            }
        }
        $em->flush();

    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearRejectionReasons(EntityManager $em)
    {
        $aRejectReasons = $em->getRepository(RegistrationDocumentsRejection::class)->findAll();
        foreach ($aRejectReasons as $oRejectReason) {
            $aRejectReasonTranslations = $oRejectReason->getRegistrationDocumentsRejectionLanguages();

            foreach ($aRejectReasonTranslations as $oRejectionReasonTranslation) {
                if (null === $oRejectionReasonTranslation->getDescription()) {
                    $em->remove($oRejectionReasonTranslation);

                }
            }
        }
        $em->flush();

    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearMedicalDictionary(EntityManager $em)
    {
        $aMedicalDictionary = $em->getRepository(MedicalDictionary::class)->findAll();
        foreach ($aMedicalDictionary as $oMedicalDictionary) {
            $aMedicalDictionaryTranslations = $oMedicalDictionary->getMedicalDictionaryTranslation();

            foreach ($aMedicalDictionaryTranslations as $oMedicalDictionaryTranslation) {
                if (null === $oMedicalDictionaryTranslation->getTitle()) {
                    $em->remove($oMedicalDictionaryTranslation);

                }
            }
        }
        $em->flush();

    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearFaqGroups(EntityManager $em)
    {
        $aFaqGroups = $em->getRepository(FaqGroups::class)->findAll();
        foreach ($aFaqGroups as $oFaqGroup) {
            $aFaqGroupsTranslations = $oFaqGroup->getFaqGroupTranslations();

            foreach ($aFaqGroupsTranslations as $oFaqGroupTranslations) {
                if (null === $oFaqGroupTranslations->getTitle()) {
                    $em->remove($oFaqGroupTranslations);
                }
            }
        }
        $em->flush();
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearFaq(EntityManager $em)
    {
        $aFaq = $em->getRepository(Faq::class)->findAll();
        foreach ($aFaq as $oFaq) {
            $aFaqTranslations = $oFaq->getFaqTranslations();

            foreach ($aFaqTranslations as $oFaqTranslations) {
                if (null === $oFaqTranslations->getTitle()) {
                    $em->remove($oFaqTranslations);
                }
            }
        }
        $em->flush();
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearMails(EntityManager $em)
    {
        $aMails = $em->getRepository(Mails::class)->findAll();
        foreach ($aMails as $oMail) {
            $aMailTranslations = $oMail->getMailLanguages();

            foreach ($aMailTranslations as $oMailTranslation) {
                if (null === $oMailTranslation->getContent()) {
                    $em->remove($oMailTranslation);
                }
            }
        }
        $em->flush();
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearLegal(EntityManager $em)
    {
        $aLegal = $em->getRepository(Legal::class)->findAll();
        foreach ($aLegal as $oLegal) {
            $aLegalTranslations = $oLegal->getLegalTranslations();

            foreach ($aLegalTranslations as $oLegalTranslation) {
                if (null === $oLegalTranslation->getTitle()) {
                    $em->remove($oLegalTranslation);
                }
            }
        }
        $em->flush();
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearContactUs(EntityManager $em)
    {
        $aContactUs = $em->getRepository(ContactUs::class)->findAll();
        foreach ($aContactUs as $oContactUs) {
            $aContactUsTranslations = $oContactUs->getContactUsTranslations();

            foreach ($aContactUsTranslations as $oContactUsTranslations) {
                if (null === $oContactUsTranslations->getTitle()) {
                    $em->remove($oContactUsTranslations);
                }
            }
        }
        $em->flush();
    }

    /**
     * @param EntityManager $em
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearSkills(EntityManager $em)
    {
        $aSkills = $em->getRepository(Skills::class)->findAll();
        foreach ($aSkills as $oSkill) {
            $aSkillTranslations = $oSkill->getSkillLanguages();

            foreach ($aSkillTranslations as $oSkillTranslations) {
                if (null === $oSkillTranslations->getName()) {
                    $em->remove($oSkillTranslations);
                }
            }
        }
        $em->flush();
    }
}
