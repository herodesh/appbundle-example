<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticIndexUsersCommand
 * @package Flendoc\AppBundle\Command
 */
class ElasticIndexMedicalCasesCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("elastic:index:medical-cases")
            ->setDescription("Index all approved medical-cases in the system")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var EntityManager $oDoctrineManager */
        $oDoctrineManager = $oContainer->get('doctrine')->getManager();
        /** @var $oElasticsearchManager $oElasticsearchManager */
        $oElasticsearchIndexerManager =  $oContainer->get('flendoc.elasticsearch.manager');
        /** @var ElasticsearchAdapter $oElasticAdapter */
        $oElasticAdapter = $oContainer->get('flendoc.elasticsearch.adapter');
        /** @var Client $oElasticClient */
        $oElasticClient = $oElasticAdapter->client;
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        $iBatchSize = $oContainer->getParameter('elasticsearch_batch_size');

        if (!$oElasticClient->indices()->exists(['index' => 'medical-cases'])) {
            $output->writeln(sprintf('<error>Index \'%s\' not found</error>', 'medical-cases'));
            $oLogger->err(sprintf('Index \'%s\' not found', 'medical-cases'));

            return;
        }

        $aResponse = $oDoctrineManager->getRepository(MedicalCases::class)->getNumberOfsApprovedAndAdminRevisedMedicalCases();
        $iTotalMedicalCases = (int)$aResponse;
        $oQuery = $oDoctrineManager
            ->createQuery('SELECT md from Flendoc\AppBundle\Entity\MedicalCases\MedicalCases md WHERE md.isApproved = :isApproved AND md.isAdminRevised = :isAdminRevised')
            ->setParameters([
                'isApproved'     => true,
                'isAdminRevised' => true,
            ])
        ;
        $oIterableResult = $oQuery->iterate();
        $aMedicalCases = [];

        foreach ($oIterableResult as $iKey => $aRow) {
            $aMedicalCases[] = $aRow[0];

            if (0 === ($iKey + 1) % $iBatchSize) {
                $iBatchNumber = ($iKey + 1) / $iBatchSize;
                $oElasticsearchIndexerManager->bulkIndexMedicalCases($aMedicalCases, $iTotalMedicalCases, $iBatchSize, $iBatchNumber, $output);
                $aMedicalCases = [];
            }

            $oDoctrineManager->detach($aRow[0]);
        }

        if (sizeof($aMedicalCases)) {
            $oElasticsearchIndexerManager->bulkIndexMedicalCases($aMedicalCases, $iTotalMedicalCases, $iBatchSize, null, $output);
        }
    }
}
