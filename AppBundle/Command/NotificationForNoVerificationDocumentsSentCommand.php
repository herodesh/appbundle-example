<?php

namespace Flendoc\AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Mails\MailsConstants;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Translation\Translator;

/**
 * Class AssetsVersionRenewalCommand
 * @package Flendoc\AppBundle\Command
 */
class NotificationForNoVerificationDocumentsSentCommand extends ContainerAwareCommand
{

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this->setName("notification:no-documents-sent")
            ->setDescription("Send an email notification(after 2 days, 1 week, 2 weeks, 1 month) for users that didn't send verification documents.")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $oContainer */
        $oContainer = $this->getContainer();
        /** @var EntityManager $oDoctrineManager */
        $oDoctrineManager = $oContainer->get('doctrine')->getManager();
        /** @var SwiftMailer */
        $oMailer = $oContainer->get('flendoc.swift.mailer.adapter');
        /** @var Router $oRouter */
        $oRouter = $oContainer->get('router');
        /** @var Logger $oLogger */
        $oLogger = $oContainer->get('logger');
        /** @var Translator $oTranslator */
        $oTranslator = $oContainer->get('translator');

        $aNotificationDays = [2, 7, 14, 30];
        $iMaxInterval = max($aNotificationDays);

        $aUsers = $oDoctrineManager
            ->getRepository(Doctors::class)
            ->findBy([
                'enabled'                                  => true,
                'isVerified'                               => false,
                'isGhostUser'                              => false,
                'noVerificationDocumentsNotificationsSent' => false,
            ])
        ;

        $output->writeln('<info>Started to send email notifications for not sending validation documents</info>');

        /** @var Doctors $oUser */
        foreach ($aUsers as $oUser) {
            /*
             * This is a fallback for users that are already enabled
             * and enabledAt field is null.
             */
            if (!$oUser->getEnabledAt()) {
                $oUser->setEnabledAt($oUser->getCreatedAt());
            }

            $oEnabledAt = $oUser->getEnabledAt();
            $oCurrentDate = new \DateTime();
            $oInterval = $oCurrentDate->diff($oEnabledAt);
            $iDaysInterval = $oInterval->days;

            if ($iDaysInterval > $iMaxInterval || in_array($iDaysInterval, $aNotificationDays, true)) {
                $iDaysIntervalKey = array_search($iDaysInterval, $aNotificationDays);

                if (false === $iDaysIntervalKey || $iDaysIntervalKey === sizeof($aNotificationDays) - 1) {
                    $sReminder = $oTranslator->trans('validation.documents.last.reminder');
                } else {
                    $iDays = $aNotificationDays[$iDaysIntervalKey + 1] - $iDaysInterval;
                    $sReminder = $oTranslator->trans('validation.documents.reminder', ['%number_of_days%' => $iDays]);
                }

                //send email
                try {
                    // Send notification email
                    $aEmailParameters = [
                        'firstName'   => $oUser->getDoctorProfile()->getFirstName(),
                        'homeUrl'     => $oRouter->generate(
                            'doctors_feeds',
                            [],
                            UrlGenerator::ABSOLUTE_URL),
                        'documentsUrl' => $oRouter->generate(
                            'validation_documents_request_documents',
                            [],
                            UrlGenerator::ABSOLUTE_URL),
                        'reminder' => $sReminder,
                    ];
                    $oMailer->send(MailsConstants::VALIDATION_DOCUMENTS_NOT_SENT, $oUser, $aEmailParameters);

                    if ($iDaysInterval >= $iMaxInterval) {
                        $oUser->setNoVerificationDocumentsNotificationsSent(true);
                    }

                    $output->writeln(sprintf(
                        '<info>Email sent to user %s(%d days period notification)</info>',
                        $oUser->getUsername(),
                        $iDaysInterval < $iMaxInterval ? $iDaysInterval : $iMaxInterval
                    ));
                    $oLogger->info(sprintf('<info>Email sent to user %s(%d days period notification)</info>',
                        $oUser->getUsername(),
                        $iDaysInterval < $iMaxInterval ? $iDaysInterval : $iMaxInterval
                    ));
                } catch (\Exception $e) {
                    $output->writeln(sprintf(
                        '<error>Email NOT sent to user %s(%d days period notification): %s</error>',
                        $oUser->getUsername(),
                        $iDaysInterval < $iMaxInterval ? $iDaysInterval : $iMaxInterval,
                        $e->getMessage()
                    ));
                    $oLogger->err(sprintf(
                        '<error>Email NOT sent to user %s(%d days period notification): %s</error>',
                        $oUser->getUsername(),
                        $iDaysInterval < $iMaxInterval ? $iDaysInterval : $iMaxInterval,
                        $e->getMessage()
                    ));
                }
            }

            $oDoctrineManager->persist($oUser);
        }

        $oDoctrineManager->flush();

        $output->writeln('<info>Finished sending email notifications</info>');
    }
}
