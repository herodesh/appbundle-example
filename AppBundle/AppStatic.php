<?php

namespace Flendoc\AppBundle;

/**
 * Class AppStatic
 * @package Flendoc\AppBundle
 */
final class AppStatic
{

    const ENC_KEY    = 'simpleFDEncKey!123#';
    const ENC_CYPHER = 'AES-128-GCM';

    /**
     * Encrypts a string
     *
     * @param $string
     *
     * @return string
     */
    public static function simpleEncrypt($string)
    {
        return bin2hex(base64_encode($string));
    }

    /**
     * Decrypts a string
     *
     * @param $encryptedString
     *
     * @return string
     */
    public static function simpleDecrypt($encryptedString)
    {
        return base64_decode(hex2bin($encryptedString));
    }

    /**
     * @param $sJsonString
     * @param $displaySize
     *
     * @return mixed
     */
    public static function displayImageNameFromJson($sJsonString, $displaySize)
    {
        $aJsonDecoded = json_decode($sJsonString, true);

        return $aJsonDecoded['files'][$displaySize]; //the upload generates 2 arrays. Files contains the sizes we need
    }

    /**
     * @param $sControllerPath
     *
     * @return string
     */
    public static function getControllerName($sControllerPath): string
    {
        $pattern = '/\\\([a-zA-Z]*)\Controller::/s';
        $matches = [];
        preg_match($pattern, $sControllerPath, $matches);

        return strtolower($matches[1]);
    }

    /**
     * Generates a random identifier.
     *
     * @return string 8 Characters long string
     */
    public static function generateIdentifier(): string
    {
        $stringIdentifier = str_replace('.', '', uniqid('', true));
        //add date time to string
        $stringIdentifier = hash('crc32b', date('Y-m-d H:i:s')).$stringIdentifier;
        //hash with crc32 -> results a 8 char string + 6 chars from the date = 14 chars
        $stringIdentifier = date('j').hash('crc32b', $stringIdentifier).date('yn');

        return $stringIdentifier;
    }

    /**
     * @param        $startDate
     * @param string $endDate
     * @param string $return Return the type of the response (eg. y - year, m - month, d - day)
     *
     * @return mixed
     */
    public static function calculateDateDiff($startDate, $endDate = 'now', $return = 'y')
    {
        if (!$endDate instanceof \DateTime) {
            $endDate = date_create($endDate);
        }

        return date_diff($startDate, $endDate)->$return;
    }

    /**
     * Display singular or plural for months based on the count
     *
     * @param int    $count
     * @param string $subdivision
     *
     * @return string
     */
    public static function displayTimeDivisions($count = 0, $subdivision = 'y')
    {
        switch ($subdivision) {
            case 'd':
                return ($count > 1) ? 'site.date.days' : 'site.date.day';
                break;

            case 'm':
                return ($count > 1) ? 'site.date.months' : 'site.date.month';
                break;

            default:
                return ($count > 1) ? 'site.date.years' : 'site.date.year';
                break;
        }
    }

    /**
     * @return \DateTime
     */
    public static function newDateTime()
    {
        return new \DateTime();
    }

    /**
     * Gets the url of AWS S3 Storage folder
     *
     * @param string $sDirectoryName The directory where the files should be stored (ex: AppConstants::AMAZON_MEDICAL_CASES_IMAGE_DIRECTORY)
     * @param array  $aParameters    This should be either $this->getParameter('app_defaults') or manually passed parameters
     *
     * @return string
     */
    public static function getS3StorageFolder($sDirectoryName, array $aParameters)
    {
        $sS3Url = sprintf(
            "//%s/%s/%s/",
            $aParameters['s3_base_url'],
            $aParameters['s3_bucket_name'],
            $sDirectoryName
        );

        return $sS3Url;
    }

    /**
     * Trims and camelize a string by delimiter
     *
     * @param string $sString
     * @param string $sDelimiter
     *
     * @return string
     */
    public static function trimAndCamelizeString($sString, $sDelimiter)
    {
        $aWords  = explode($sDelimiter, $sString);
        $aWords  = array_map('ucfirst', $aWords);
        $sString = implode('', $aWords);

        return $sString;
    }

    /**
     * Transforms a string into a function name
     * * ex. search_medical_dictionary becomes searchMedicalDictionary
     *
     * @param string $sType
     * @param string $sSearch
     * @param string $sReplace
     *
     * @return string
     */
    public static function functionNameTransform($sType, $sSearch = '_', $sReplace = ' ')
    {
        //make first string individual words
        $sFunctionName = strtolower(str_replace($sSearch, $sReplace, $sType));
        //makes the string function ready. Ex: searchSomething or getSomeElement
        $sFunctionName = lcfirst(str_replace(' ', '', ucwords($sFunctionName)));

        return $sFunctionName;
    }

    /**
     * Redis cache has some reserved characters: {}()/\@:=
     * Usernames are emails so we need to ensure that the @ is changed into something that is accepted by the cache system
     *
     * @param string $sUserName
     *
     * @return mixed
     */
    public static function userNameCacheReady($sUserName)
    {
        return str_replace('@', '|_at_|', $sUserName);
    }
}
