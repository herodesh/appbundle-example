<?php

namespace Flendoc\AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Message\MongoMessageRepository")
 */
class MongoMessage
{
    const ANNOUNCEMENT_TYPE_JOINED_CONVERSATION = 'joined_conversation';
    const ANNOUNCEMENT_TYPE_LEFT_CONVERSATION = 'left_conversation';
    const ANNOUNCEMENT_TYPE_REMOVED_FROM_CONVERSATION = 'removed_from_conversation';
    const ANNOUNCEMENT_TYPE_USER_DELETED_ACCOUNT = 'deleted_account';

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $message;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $userIdentifier;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $chatIdentifier;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $sendAt;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $tag;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $chatAnnouncement = false;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $chatAnnouncementType;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $chatAnnouncementUserIdentifier;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set sendAt
     *
     * @param date $sendAt
     * @return $this
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;
        return $this;
    }

    /**
     * Get sendAt
     *
     * @return date $sendAt
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * Get tag
     *
     * @return string $tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set chatAnnouncement
     *
     * @param boolean $chatAnnouncement
     * @return $this
     */
    public function setChatAnnouncement($chatAnnouncement)
    {
        $this->chatAnnouncement = $chatAnnouncement;
        return $this;
    }

    /**
     * Get chatAnnouncement
     *
     * @return boolean $chatAnnouncement
     */
    public function getChatAnnouncement()
    {
        return $this->chatAnnouncement;
    }

    /**
     * Set chatAnnouncementType
     *
     * @param string $chatAnnouncementType
     * @return $this
     */
    public function setChatAnnouncementType($chatAnnouncementType)
    {
        $this->chatAnnouncementType = $chatAnnouncementType;
        return $this;
    }

    /**
     * Get chatAnnouncementType
     *
     * @return string $chatAnnouncementType
     */
    public function getChatAnnouncementType()
    {
        return $this->chatAnnouncementType;
    }

    /**
     * Set userIdentifier
     *
     * @param string $userIdentifier
     * @return $this
     */
    public function setUserIdentifier($userIdentifier)
    {
        $this->userIdentifier = $userIdentifier;

        return $this;
    }

    /**
     * Get userIdentifier
     *
     * @return string $userIdentifier
     */
    public function getUserIdentifier()
    {
        return $this->userIdentifier;
    }

    /**
     * Set chatIdentifier
     *
     * @param string $chatIdentifier
     *
     * @return $this
     */
    public function setChatIdentifier($chatIdentifier)
    {
        $this->chatIdentifier = $chatIdentifier;

        return $this;
    }

    /**
     * Get chatIdentifier
     *
     * @return string $chatIdentifier
     */
    public function getChatIdentifier()
    {
        return $this->chatIdentifier;
    }

    /**
     * Set chatAnnouncementUserIdentifier
     *
     * @param string $chatAnnouncementUserIdentifier
     *
     * @return $this
     */
    public function setChatAnnouncementUserIdentifier($chatAnnouncementUserIdentifier)
    {
        $this->chatAnnouncementUserIdentifier = $chatAnnouncementUserIdentifier;

        return $this;
    }

    /**
     * Get chatAnnouncementUserIdentifier
     *
     * @return string $chatAnnouncementUserIdentifier
     */
    public function getChatAnnouncementUserIdentifier()
    {
        return $this->chatAnnouncementUserIdentifier;
    }
}
