<?php

namespace Flendoc\AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Flendoc\AppBundle\AppStatic;

/**
 * Class AbstractMongoDocument
 * @package Flendoc\AppBundle\Document
 *
 * @MongoDB\Document()
 * @MongoDB\HasLifecycleCallbacks()
 */
class AbstractMongoDocument
{
    /**
     * @MongoDB\Id()
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $identifier;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @MongoDB\PrePersist()
     */
    public function setCreatedAt(): void
    {
        $this->createdAt = new \DateTime;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @MongoDB\PrePersist()
     * @MongoDB\PreUpdate()
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @MongoDB\PrePersist()
     */
    public function setIdentifier(): void
    {
        $this->identifier = AppStatic::generateIdentifier();
    }
}
