<?php

namespace Flendoc\AppBundle\Document\Comments;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as MongoDBUnique;
use Flendoc\AppBundle\Document\AbstractMongoDocument;

/**
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Comments\MongoThreadRepository")
 * @MongoDBUnique(fields={"id"})
 * @MongoDBUnique(fields={"dependencyIdentifier"})
 */
class MongoThread extends AbstractMongoDocument
{
    /**
     * @MongoDB\Id()
     */
    protected $id;

    /**
     * Tells if new comments can be added in this thread.
     *
     * @var boolean
     *
     * @MongoDB\Field(type="boolean")
     */
    protected $isCommentable = true;

    /**
     * Number of comments.
     *
     * @var int
     *
     * @MongoDB\Field(type="integer")
     */
    protected $numComments = 0;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(
     *     targetDocument="MongoComment",
     *     mappedBy="thread",
     *     cascade={"remove"}
     * )
     */
    protected $comments;

    /**
     * The date of the last comment.
     *
     * @var \Datetime
     *
     * @MongoDB\Field(type="date")
     */
    protected $lastCommentAt = null;

    /**
     * User identifier.
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $userIdentifier;

    /**
     * Dependency class identifier.
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $dependencyIdentifier;

    /**
     * Dependency class type
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $dependencyType;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(
     *     targetDocument="MongoVote",
     *     mappedBy="thread",
     *     cascade={"remove"}
     * )
     */
    protected $votes;

    /**
     * @var int
     *
     * @MongoDB\Field(type="int")
     */
    protected $totalVotesNumber = 0;

    /**
     * MongoThread constructor.
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->votes    = new ArrayCollection();
    }

    /**
     * Set isCommentable
     *
     * @param boolean $isCommentable
     *
     * @return $this
     */
    public function setIsCommentable($isCommentable)
    {
        $this->isCommentable = $isCommentable;
        return $this;
    }

    /**
     * Get isCommentable
     *
     * @return boolean $isCommentable
     */
    public function getIsCommentable()
    {
        return $this->isCommentable;
    }

    /**
     * Set numComments
     *
     * @param integer $numComments
     *
     * @return $this
     */
    public function setNumComments($numComments)
    {
        $this->numComments = $numComments;
        return $this;
    }

    /**
     * Get numComments
     *
     * @return integer $numComments
     */
    public function getNumComments()
    {
        return $this->numComments;
    }

    /**
     * Add comment
     *
     * @param MongoComment $comment
     */
    public function addComment(MongoComment $comment)
    {
        $this->comments[] = $comment;
    }

    /**
     * Remove comment
     *
     * @param MongoComment $comment
     */
    public function removeComment(MongoComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return ArrayCollection $comments
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set lastCommentAt
     *
     * @param \DateTime $lastCommentAt
     *
     * @return $this
     */
    public function setLastCommentAt(\DateTime $lastCommentAt)
    {
        $this->lastCommentAt = $lastCommentAt;

        return $this;
    }

    /**
     * Get lastCommentAt
     *
     * @return \DateTime $lastCommentAt
     */
    public function getLastCommentAt()
    {
        return $this->lastCommentAt;
    }

    /**
     * Set userIdentifier
     *
     * @param string $userIdentifier
     *
     * @return $this
     */
    public function setUserIdentifier($userIdentifier)
    {
        $this->userIdentifier = $userIdentifier;

        return $this;
    }

    /**
     * Get userIdentifier
     *
     * @return string $userIdentifier
     */
    public function getUserIdentifier()
    {
        return $this->userIdentifier;
    }

    /**
     * Set dependencyIdentifier
     *
     * @param string $dependencyIdentifier
     *
     * @return $this
     */
    public function setDependencyIdentifier($dependencyIdentifier)
    {
        $this->dependencyIdentifier = $dependencyIdentifier;

        return $this;
    }

    /**
     * Get dependencyIdentifier
     *
     * @return string $dependencyIdentifier
     */
    public function getDependencyIdentifier()
    {
        return $this->dependencyIdentifier;
    }

    /**
     * Set dependencyType
     *
     * @param string $dependencyType
     *
     * @return $this
     */
    public function setDependencyType($dependencyType)
    {
        $this->dependencyType = $dependencyType;

        return $this;
    }

    /**
     * Get dependencyType
     *
     * @return string $dependencyType
     */
    public function getDependencyType()
    {
        return $this->dependencyType;
    }

    /**
     * Add vote
     *
     * @param MongoVote $vote
     */
    public function addVote(MongoVote $vote)
    {
        $this->votes[] = $vote;
    }

    /**
     * Remove vote
     *
     * @param MongoVote $vote
     */
    public function removeVote(MongoVote $vote)
    {
        $this->votes->removeElement($vote);
    }

    /**
     * Get votes
     *
     * @return ArrayCollection $votes
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @return int
     */
    public function addVoteToTotalVotesNumber() {
        return $this->totalVotesNumber += 1;
    }

    /**
     * @return int
     */
    public function removeVoteFromTotalVotesNumber() {

        if ($this->totalVotesNumber) {
            return $this->totalVotesNumber -= 1;
        }

        return $this->totalVotesNumber;
    }

    /**
     * @return int
     */
    public function getTotalVotesNumber()
    {
        return $this->totalVotesNumber;
    }
}
