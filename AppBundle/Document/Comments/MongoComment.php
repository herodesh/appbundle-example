<?php

namespace Flendoc\AppBundle\Document\Comments;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Flendoc\AppBundle\Document\AbstractMongoDocument;

/**
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Comments\MongoCommentRepository")
 */
class MongoComment extends AbstractMongoDocument
{

    const NUMBER_OF_COMMENTS_PER_PAGE = 2;
    const NUMBER_OF_REPLIES_PER_PAGE = 1;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $body;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $userIdentifier;

    /**
     * @var boolean
     *
     * @MongoDB\Field(type="boolean")
     */
    protected $isReply = false;

    /**
     * @var MongoComment
     *
     * @MongoDB\ReferenceOne(
     *     targetDocument="MongoComment",
     *     storeAs="id",
     *     inversedBy="children"
     * )
     * @MongoDB\Index()
     */
    protected $parent;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(
     *     targetDocument="MongoComment",
     *     mappedBy="parent",
     *     cascade={"remove"}
     * )
     */
    protected $children;

    /**
     * @var MongoThread
     *
     * @MongoDB\ReferenceOne(
     *     targetDocument="MongoThread",
     *     storeAs="id",
     *     inversedBy="comments"
     * )
     */
    protected $thread;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(
     *     targetDocument="MongoVote",
     *     mappedBy="comment",
     *     cascade={"remove"}
     * )
     */
    protected $votes;

    /**
     * @var int
     *
     * @MongoDB\Field(type="int")
     */
    protected $totalVotesNumber = 0;

    /**
     * MongoComment constructor.
     */
    public function __construct()
    {
        $this->children  = new ArrayCollection();
        $this->votes     = new ArrayCollection();
    }

    /**
     * Set comment body
     *
     * @param string $body
     *
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get comment body
     *
     * @return string $body
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set userIdentifier
     *
     * @param string $userIdentifier
     *
     * @return $this
     */
    public function setUserIdentifier($userIdentifier)
    {
        $this->userIdentifier = $userIdentifier;

        return $this;
    }

    /**
     * Get userIdentifier
     *
     * @return int $userIdentifier
     */
    public function getUserIdentifier()
    {
        return $this->userIdentifier;
    }

    /**
     * Set isReply
     *
     * @param boolean $isReply
     * @return $this
     */
    public function setIsReply($isReply)
    {
        $this->isReply = $isReply;
        return $this;
    }

    /**
     * Get isReply
     *
     * @return boolean $isReply
     */
    public function getIsReply()
    {
        return $this->isReply;
    }

    /**
     * Set parent
     *
     * @param MongoComment $parent
     *
     * @return $this
     */
    public function setParent(MongoComment $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return MongoComment $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param MongoComment $child
     */
    public function addChild(MongoComment $child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove child
     *
     * @param MongoComment $child
     */
    public function removeChild(MongoComment $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return ArrayCollection $children
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set thread
     *
     * @param MongoThread $thread
     *
     * @return $this
     */
    public function setThread(MongoThread $thread)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return MongoThread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Add vote
     *
     * @param MongoVote $vote
     */
    public function addVote(MongoVote $vote)
    {
        $this->votes[] = $vote;
    }

    /**
     * Remove vote
     *
     * @param MongoVote $vote
     */
    public function removeVote(MongoVote $vote)
    {
        $this->votes->removeElement($vote);
    }

    /**
     * Get votes
     *
     * @return ArrayCollection $votes
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @return int
     */
    public function addVoteToTotalVotesNumber() {
        return $this->totalVotesNumber += 1;
    }

    /**
     * @return int
     */
    public function removeVoteFromTotalVotesNumber() {
        return $this->totalVotesNumber -= 1;
    }

    /**
     * @return int
     */
    public function getTotalVotesNumber()
    {
        return $this->totalVotesNumber;
    }
}
