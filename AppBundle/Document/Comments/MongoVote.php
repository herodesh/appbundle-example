<?php

namespace Flendoc\AppBundle\Document\Comments;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Flendoc\AppBundle\Document\AbstractMongoDocument;

/**
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Comments\MongoVoteRepository")
 */
class MongoVote extends AbstractMongoDocument
{
    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $userIdentifier;

    /**
     * @var MongoComment
     *
     * @MongoDB\ReferenceOne(
     *     targetDocument="MongoComment",
     *     storeAs="id",
     *     inversedBy="votes"
     * )
     */
    protected $comment;

    /**
     * @var MongoThread
     *
     * @MongoDB\ReferenceOne(
     *     targetDocument="MongoThread",
     *     storeAs="id",
     *     inversedBy="votes"
     * )
     */
    protected $thread;

    /**
     * Set userIdentifier
     *
     * @param string $userIdentifier
     *
     * @return $this
     */
    public function setUserIdentifier($userIdentifier)
    {
        $this->userIdentifier = $userIdentifier;
        return $this;
    }

    /**
     * Get userIdentifier
     *
     * @return string $userIdentifier
     */
    public function getUserIdentifier()
    {
        return $this->userIdentifier;
    }

    /**
     * Set comment
     *
     * @param MongoComment $comment
     *
     * @return $this
     */
    public function setComment(MongoComment $comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return MongoComment $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set thread
     *
     * @param MongoThread $thread
     *
     * @return $this
     */
    public function setThread(MongoThread $thread)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return MongoThread $thread
     */
    public function getThread()
    {
        return $this->thread;
    }
}
