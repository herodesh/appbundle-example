<?php

namespace Flendoc\AppBundle\Document\Notifications;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Flendoc\AppBundle\Document\AbstractMongoDocument;

/**
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Notifications\MongoNotificationRepository")
 */
class MongoNotification extends AbstractMongoDocument
{
    /**
     * Receiver Doctor class identifier.
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $receiverIdentifier;

    /**
     * Sender Doctor class identifier.
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $senderIdentifier;

    /**
     * Dependency class type.
     * This type takes values from ObjectTypeConstants class.
     *
     * @var string|null
     *
     * @MongoDB\Field(type="string")
     */
    protected $dependencyType = null;

    /**
     * Dependency class identifier.
     * This identifier is strictly used in correlation with classes
     * defined in ObjectKeyEntityTrait class.
     *
     * @var string|null
     *
     * @MongoDB\Field(type="string")
     */
    protected $dependencyIdentifier = null;

    /**
     * Object class identifier.
     * This identifier refers to an object that is used inside a Dependency class.
     * (e.g. A MedicalCase with MongoComments. In this case dependencyIdentifier
     * will be MedicalCase identifier and objectIdentifier will be MongoComment identifier)
     *
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $objectIdentifier;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    protected $type;

    /**
     * @var boolean
     *
     * @MongoDB\Field(type="boolean")
     */
    protected $isRead = false;

    /**
     * Set receiverIdentifier
     *
     * @param string $receiverIdentifier
     *
     * @return $this
     */
    public function setReceiverIdentifier($receiverIdentifier)
    {
        $this->receiverIdentifier = $receiverIdentifier;

        return $this;
    }

    /**
     * Get receiverIdentifier
     *
     * @return string $receiverIdentifier
     */
    public function getReceiverIdentifier()
    {
        return $this->receiverIdentifier;
    }

    /**
     * Set senderIdentifier
     *
     * @param string $senderIdentifier
     *
     * @return $this
     */
    public function setSenderIdentifier($senderIdentifier)
    {
        $this->senderIdentifier = $senderIdentifier;

        return $this;
    }

    /**
     * Get senderIdentifier
     *
     * @return string $senderIdentifier
     */
    public function getSenderIdentifier()
    {
        return $this->senderIdentifier;
    }

    /**
     * Set objectIdentifier
     *
     * @param string $objectIdentifier
     *
     * @return $this
     */
    public function setObjectIdentifier($objectIdentifier)
    {
        $this->objectIdentifier = $objectIdentifier;

        return $this;
    }

    /**
     * Get objectIdentifier
     *
     * @return string $objectIdentifier
     */
    public function getObjectIdentifier()
    {
        return $this->objectIdentifier;
    }

    /**
     * Set dependencyType
     *
     * @param string $dependencyType
     *
     * @return $this
     */
    public function setDependencyType($dependencyType)
    {
        $this->dependencyType = $dependencyType;

        return $this;
    }

    /**
     * Get dependencyType
     *
     * @return string $dependencyType
     */
    public function getDependencyType()
    {
        return $this->dependencyType;
    }

    /**
     * Set dependencyIdentifier
     *
     * @param string $dependencyIdentifier
     *
     * @return $this
     */
    public function setDependencyIdentifier($dependencyIdentifier)
    {
        $this->dependencyIdentifier = $dependencyIdentifier;

        return $this;
    }

    /**
     * Get dependencyIdentifier
     *
     * @return string $dependencyIdentifier
     */
    public function getDependencyIdentifier()
    {
        return $this->dependencyIdentifier;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get $type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return $this
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean $isRead
     */
    public function getIsRead()
    {
        return $this->isRead;
    }
}
