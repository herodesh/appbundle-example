<?php

namespace Flendoc\AppBundle\Document\Feed;

use Flendoc\AppBundle\Document\AbstractMongoDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class MongoFeed
 * @package Flendoc\AppBundle\Document\Feed
 *
 * @MongoDB\Document(repositoryClass="Flendoc\AppBundle\Repository\Feeds\MongoFeedRepository")
 */
class MongoFeed extends AbstractMongoDocument
{
    /**
     * @MongoDB\Field(type="string")
     */
    protected $creatorUserIdentifier;

    /**
     * Should be the current object data type (mongo, mysql).
     * For example a new medical case is mysql and a share of this one will be mongo
     *
     * @MongoDB\Field(type="string")
     */
    protected $objectDataType;

    /**
     * Should be the current object entity type (ex. medical_case). This is not the same as feedType because we need
     * to be careful with sharing...
     *
     *
     * @MongoDB\Field(type="string")
     */
    protected $objectEntityType;

    /**
     * What type of entity feed is this (ex. mongo_feed)
     *
     * We use this for comments!
     *
     * @MongoDB\Field(type="string")
     */
    protected $feedEntityType;

    /**
     * What type of feed type is this (ex. status_update, medical_case, etc...)
     *
     * @MongoDB\Field(type="string")
     */
    protected $feedType;

    /**
     * Original object identifier is needed in case we are making a share
     *
     * @MongoDB\Field(type="string")
     */
    protected $originalObjectIdentifier;

    /**
     * Should keep the original data type of object (mysql or mongo). With this we know on which database we should be looking
     *
     * @MongoDB\Field(type="string")
     */
    protected $originalObjectDataType;

    /**
     * Should keep the type of object (medical_case, share, etc)
     *
     * @MongoDB\Field(type="string")
     */
    protected $originalObjectEntityType;

    /**
     * @MongoDB\Field(type="string", nullable=true)
     */
    protected $content = null;

    /**
     * contentIdentifier relates to an identifier of an object that is saved before and it's not a status update (ex. Medical Case)
     *
     * @MongoDB\Field(type="string")
     */
    protected $contentIdentifier = null;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $urlContent = "[]";

    /**
     * @MongoDB\Field(type="string")
     */
    protected $privacy = 'public';

    /**
     * @MongoDB\Field(type="bool")
     */
    protected $isShared = false;

    /**
     * Default is true. There are few cases like Medical cases where we do not need this to be true
     *
     * @MongoDB\Field(type="bool")
     */
    protected $isEditable = true;

    /**
     * @return mixed
     */
    public function getCreatorUserIdentifier()
    {
        return $this->creatorUserIdentifier;
    }

    /**
     * @param mixed $creatorUserIdentifier
     */
    public function setCreatorUserIdentifier($creatorUserIdentifier): void
    {
        $this->creatorUserIdentifier = $creatorUserIdentifier;
    }

    /**
     * @return mixed
     */
    public function getOriginalObjectIdentifier()
    {
        return $this->originalObjectIdentifier;
    }

    /**
     * @param mixed $originalObjectIdentifier
     */
    public function setOriginalObjectIdentifier($originalObjectIdentifier): void
    {
        $this->originalObjectIdentifier = $originalObjectIdentifier;
    }

    /**
     * @return mixed
     */
    public function getOriginalObjectEntityType()
    {
        return $this->originalObjectEntityType;
    }

    /**
     * @param mixed $originalObjectEntityType
     */
    public function setOriginalObjectEntityType($originalObjectEntityType): void
    {
        $this->originalObjectEntityType = $originalObjectEntityType;
    }

    /**
     * @return mixed
     */
    public function getOriginalObjectDataType()
    {
        return $this->originalObjectDataType;
    }

    /**
     * @param mixed $originalObjectDataType
     */
    public function setOriginalObjectDataType($originalObjectDataType): void
    {
        $this->originalObjectDataType = $originalObjectDataType;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getContentIdentifier()
    {
        return $this->contentIdentifier;
    }

    /**
     * @param mixed $contentIdentifier
     */
    public function setContentIdentifier($contentIdentifier): void
    {
        $this->contentIdentifier = $contentIdentifier;
    }

    /**
     * @return mixed
     */
    public function getUrlContent()
    {
        return $this->urlContent;
    }

    /**
     * @param mixed $urlContent
     */
    public function setUrlContent($urlContent): void
    {
        $this->urlContent = $urlContent;
    }

    /**
     * @return mixed
     */
    public function getFeedEntityType()
    {
        return $this->feedEntityType;
    }

    /**
     * @param mixed $feedEntityType
     */
    public function setFeedEntityType($feedEntityType): void
    {
        $this->feedEntityType = $feedEntityType;
    }

    /**
     * @return mixed
     */
    public function getFeedType()
    {
        return $this->feedType;
    }

    /**
     * @param mixed $feedType
     */
    public function setFeedType($feedType): void
    {
        $this->feedType = $feedType;
    }

    /**
     * @return mixed
     */
    public function getObjectDataType()
    {
        return $this->objectDataType;
    }

    /**
     * @param mixed $objectDataType
     */
    public function setObjectDataType($objectDataType): void
    {
        $this->objectDataType = $objectDataType;
    }

    /**
     * @return mixed
     */
    public function getObjectEntityType()
    {
        return $this->objectEntityType;
    }

    /**
     * @param mixed $objectEntityType
     */
    public function setObjectEntityType($objectEntityType): void
    {
        $this->objectEntityType = $objectEntityType;
    }

    /**
     * @return mixed
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * @param mixed $privacy
     */
    public function setPrivacy($privacy): void
    {
        $this->privacy = $privacy;
    }

    /**
     * @return mixed
     */
    public function getIsShared()
    {
        return $this->isShared;
    }

    /**
     * @param mixed $isShared
     */
    public function setIsShared($isShared): void
    {
        $this->isShared = $isShared;
    }

    /**
     * @return mixed
     */
    public function getIsEditable()
    {
        return $this->isEditable;
    }

    /**
     * @param mixed $isEditable
     */
    public function setIsEditable($isEditable): void
    {
        $this->isEditable = $isEditable;
    }
}
