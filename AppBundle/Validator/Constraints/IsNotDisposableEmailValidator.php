<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Flendoc\AppBundle\Utils\DisposableEmailCheck;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsNotDisposableEmailValidator
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsNotDisposableEmailValidator extends ConstraintValidator
{
    /** @var DisposableEmailCheck */
    protected $disposableEmailChecker;

    /**
     * IsNotDisposableEmailValidator constructor.
     *
     * @param DisposableEmailCheck $disposableEmailCheck
     */
    public function __construct(DisposableEmailCheck $disposableEmailCheck)
    {
        $this->disposableEmailChecker = $disposableEmailCheck;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function validate($value, Constraint $constraint): void
    {
        if ($this->disposableEmailChecker->isDisposableEmail($value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }

        return;
    }
}
