<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsInstitutionPrimaryLanguageValidator
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsInstitutionPrimaryLanguageValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $existingLanguage = 0;
        foreach ($value as $institutionLanguage) {
            if ($institutionLanguage->getIsPrimaryDescription()) {
                $existingLanguage += 1;

                if ($existingLanguage > 1) {
                    $this->context->buildViolation($constraint->message)->addViolation();
                    break;
                }
            }
        }

    }
}
