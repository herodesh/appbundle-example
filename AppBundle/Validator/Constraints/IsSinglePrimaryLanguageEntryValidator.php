<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsSingleActiveLanguageValidator
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsSinglePrimaryLanguageEntryValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * Construct
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed      $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        $aInitialValue = $this->em->getUnitOfWork()->getOriginalEntityData($object);

        //if it's already primary ignore
        if ($aInitialValue && $aInitialValue['isPrimary']) {
            return;
        }

        $oActiveExists = $this->em->getRepository('AppBundle:Languages\Languages')->findOneByIsPrimary(true);

        if ($oActiveExists && $object->getIsPrimary()) {
            $this->context->buildViolation($constraint->message)
                          ->atPath('isPrimary')
                          ->addViolation();
        }
    }
}
