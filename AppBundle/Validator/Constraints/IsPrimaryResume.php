<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * Class IsPrimaryResume
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsPrimaryResume extends Constraint
{
    /** @var string */
    public $message = "validator.you.have.setup.your.primary.resume";

    /**
     * @var Doctors
     */
    protected $user;

    /**
     * IsPrimaryResume constructor.
     *
     * @param mixed|null $options
     */
    public function __construct($options)
    {
        if ($options['user'] instanceof Doctors) {
            $this->user = $options['user'];
        } else {
            throw new MissingOptionsException('Could not find the right option. Should be Instance of User class', $options);
        }

        parent::__construct($options);
    }

    /**
     * @return Doctors
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
