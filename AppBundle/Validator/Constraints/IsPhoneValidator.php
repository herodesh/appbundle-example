<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsPhoneValidator
 * @package Flendoc\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class IsPhoneValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value) {
            if (!preg_match('/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/', $value, $matches) || (strlen($value) < 7)) {
                $this->context->buildViolation($constraint->message)
                              ->addViolation();
            }
        }

        return;
    }
}
