<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Doctors\DoctorExistingEmailAddresses;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Repository\Doctors\DoctorExistingEmailAddressesRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EmailUsedValidator
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class EmailUsedValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * Construct
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /*
         * Check the availability of the email.
         * If this email was previously used by the current user
         * he can successfully use it, not otherwise.
         */
        /** @var DoctorExistingEmailAddressesRepository $oDoctorExistingEmailAddressesRepository */
        $oDoctorExistingEmailAddressesRepository = $this->em->getRepository(DoctorExistingEmailAddresses::class);
        /** @var DoctorExistingEmailAddresses $oDoctorExistingEmailAddress */
        $oDoctorExistingEmailAddress = $oDoctorExistingEmailAddressesRepository
            ->findOneBy([
                'email' => $value
            ]);
        $oDoctor = $this->em->getRepository(Doctors::class)
            ->findOneBy([
                'username' => $value
            ]);
        $oLoggedUser = $this->tokenStorage->getToken()->getUser();

        if (($oDoctorExistingEmailAddress && $oDoctorExistingEmailAddress->getDoctor() !== $oLoggedUser) ||
            ($oDoctor && $oDoctor !== $oLoggedUser)
        ) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }

        return;
    }
}
