<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IsSinglePrimaryLanguageEntry
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsSinglePrimaryLanguageEntry extends Constraint
{
    /**
     * @var string
     */
    public $message = "validator.primary.language.already.set";

    /**
     * @return string
     */
    public function getTargets()
    {
        //is a class constraint
        return self::CLASS_CONSTRAINT;
    }

    /**
     * ValidatedBy
     *
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
