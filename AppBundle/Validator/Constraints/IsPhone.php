<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IsPhone
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsPhone extends Constraint
{
    /** @var string */
    public $message = "validator.invalid.phone.number";

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
