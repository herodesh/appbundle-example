<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IsSinglePrimaryLanguage
 * @package Flendoc\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class IsInstitutionPrimaryLanguage extends Constraint
{
    /** @var string */
    public $message = "There is already a primary language set.";

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
