<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CityBelongsToCountryValidator
 * @package Flendoc\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class CityBelongsToCountryValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value) {
            $oData = $this->context->getRoot()->getData();

            if ($oData->getCity()->getCountry() != $oData->getCountry()) {
                $this->context->buildViolation($constraint->message)
                              ->addViolation();
            }
        }

        return;
    }
}
