<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EmailUsed
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class EmailUsed extends Constraint
{
    /** @var string */
    public $message = "validator.email.in.use";

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
