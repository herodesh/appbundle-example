<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsPrimaryResumeValidator
 *
 * @Annotation
 *
 * @package Flendoc\AppBundle\Validator\Constraints
 */
class IsPrimaryResumeValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var User
     */
    protected $user;

    /**
     * IsPrimaryResumeValidator constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        //check if there is a Resume for the given user that is set primary
        $oDoctor          = $constraint->getUser();
        $oIsPrimaryExists = $this->em->getRepository('AppBundle:Resumes\Resumes')->findOneBy([
            'doctor'    => $oDoctor,
            'isPrimary' => true,
        ]);

        if ($oIsPrimaryExists && $value) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
