<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IsNotDisposableEmail
 * @package Flendoc\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class IsNotDisposableEmail extends Constraint
{
    /** @var string */
    public $message = "validator.is.not.disposable.email";

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
