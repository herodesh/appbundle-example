<?php

namespace Flendoc\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class CityBelongsToCountry
 * @package Flendoc\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class CityBelongsToCountry extends Constraint
{
    /** @var string */
    public $message = "validator.city.does.not.belong.to.country";

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    /**
     * Gets all the inputs from the Form Type Class
     *
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
            self::PROPERTY_CONSTRAINT,
        ];
    }
}
