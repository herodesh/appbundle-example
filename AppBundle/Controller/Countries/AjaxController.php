<?php

namespace Flendoc\AppBundle\Controller\Countries;

use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package Flendoc\AppBundle\Controller\Countries
 *
 * @Route("/ajax/countries")
 */
class AjaxController extends FlendocController
{
    /**
     * Find countries in the select2 dropdown
     *
     * @Route("/find-country", name="ajax_find_country")
     *
     * @param Request $oRequest
     *
     * @return JsonResponse
     */
    public function findCountriesAction(Request $oRequest)
    {
        $aCountries = $this->getRepository('AppBundle:Countries\Countries')
                           ->findCountryByQuery($this->getUserLanguage(), $oRequest->query->get('q'), true);

        if (empty($aCountries)) {
            return $this->json([], 200);
        }

        return $this->json($this->select2ArrayKeyModifier('name', $aCountries), 200);
    }

    /**
     * Gets Cities from country based on search string
     *
     * @Route("/find-city/{countryIdentifier}", name="ajax_get_cities_from_country")
     *
     * @param Request $oRequest
     * @param string  $countryIdentifier
     *
     * @return JsonResponse
     */
    public function ajaxGetCitiesByCountryAction(Request $oRequest, $countryIdentifier = null)
    {
        $aCities = $this->getRepository(Cities::class)
                        ->findCitiesByCountryAndQueryString($countryIdentifier, $this->getUserLanguage(), $oRequest->query->get('q'));

        if (empty($aCities)) {
            return $this->json([], Response::HTTP_OK);
        }

        return $this->json($this->select2ArrayKeyModifier('name', $aCities), Response::HTTP_OK);
    }
}
