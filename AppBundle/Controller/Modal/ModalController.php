<?php

namespace Flendoc\AppBundle\Controller\Modal;

use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Repository\LegalAndHelp\FaqRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ModalController
 * @package Flendoc\AppBundle\Controller
 *
 * @Route("/flendoc")
 */
class ModalController extends FlendocController
{

    /**
     * Loads the application modal confirmation
     *
     * @Route("/load-modal-confirmation/{modalTemplate}", name="flendoc_load_modal_confirmation")
     *
     * @param Request    $oRequest      Symfony Request
     * @param string     $modalTemplate The name of the template
     * @param array|null $aParameters   Other parameters that needs to be passed to the view
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \ErrorException
     */
    public function loadModalConfirmation(
        Request $oRequest,
        $modalTemplate = 'default_confirmation_modal',
        array $aParameters = null
    ) {
        $this->checkAjaxRequest($oRequest);

        return $this->json(
            $this->renderView('@App/ConfirmationModals/'.$modalTemplate.'.html.twig', [
                'parameters' => $aParameters,
            ]),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/load-faq-modal/{modalTemplate}/{sFaqStringIdentifier}", name="doctors_faq_load_modal")
     *
     * @param Request     $oRequest
     * @param null|string $modalTemplate
     * @param null|string $sFaqStringIdentifier
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \ErrorException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadFaqContentModal(Request $oRequest, $modalTemplate = null, $sFaqStringIdentifier = null)
    {
        $this->checkAjaxRequest($oRequest);

        /** @var FaqRepository $oFaqRepository */
        $oFaqRepository = $this->getRepository(Faq::class);

        if (null == $modalTemplate || null == $sFaqStringIdentifier) {
            return $this->json(['message' => 'error'], Response::HTTP_NOT_FOUND);
        }

        $aFaqData = $oFaqRepository
            ->findOneFaqByLanguageAndStringIdentifier($this->getUserLanguage(), $sFaqStringIdentifier)
        ;

        return $this->json(
            $this->renderView('@App/ConfirmationModals/'.$modalTemplate.'.html.twig', [
                'aFaqData' => $aFaqData
            ]),
            Response::HTTP_OK
        );
    }

}
