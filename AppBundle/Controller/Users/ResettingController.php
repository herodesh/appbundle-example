<?php

namespace Flendoc\AppBundle\Controller\Users;

use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Adapter\SwiftMailer\SwiftMailer;
use Flendoc\AppBundle\Entity\Mails\MailsConstants;
use Flendoc\AppBundle\Form\Users\Type\RequestResetPasswordType;
use Flendoc\AppBundle\Form\Users\Type\ResetPasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * Class ResettingController
 * @package Flendoc\DoctorsBundle\Controller
 *
 * @Route("/resetting")
 */
class ResettingController extends FlendocController
{

    /**
     * Adds security layer to recovery password
     *
     * @Route("/reset-password-request/{type}", name="user_reset_password_request")
     *
     * @param Request $oRequest
     * @param string $type
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\Error
     */
    public function requestAction(Request $oRequest, $type)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect('doctors_feeds');
        }

        $oForm = $this->createForm(RequestResetPasswordType::class);

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {

            $oUserManager = $this->get('flendoc.'.$type.'.manager');
            /** @var SwiftMailer\ */
            $oMailer = $this->get('flendoc.swift.mailer.adapter');

            $oUser = $this->getRepositoryByUserType($type)->findOneByUsername($oFormData['email']);

            //if user was not found send a fake success message
            if (null === $oUser) {
                return $this->redirectToRoute('resetting_check_email');
            }

            if ($oUser->isPasswordRequestNonExpired()) {
                return $this->render('@App/User/Resetting/passwordAlreadyRequested.html.twig');
            }

            if (!$oUser->getConfirmationToken()) {
                $oUser->setConfirmationToken($oUser->generateNewToken());
            }

            $sUserProfileGetter = $this->_getProfileSetter($type);
            $aMailParameters    = [
                'firstName' => $oUser->$sUserProfileGetter()->getFirstName(),
                'action'    => $this->generateUrl('reset_password_action', [
                    'type'  => $type,
                    'token' => $oUser->getConfirmationToken(),
                ], UrlGenerator::ABSOLUTE_URL),
            ];

            $oMailer->send(MailsConstants::RESET_PASSWORD, $oUser, $aMailParameters);
            $oUser->setPasswordRequestedAt(new \DateTime());

            $oUserManager->saveObject($oUser);

            return $this->render('@App/User/Resetting/checkEmail.html.twig');
        }

        return $this->render('@App/User/Resetting/request.html.twig', [
            'form' => $oForm->createView(),
            'type' => $type,
        ]);
    }

    /**
     * Reset password Action
     *
     * @Route("/reset-password/{type}/{token}", name="reset_password_action")
     *
     * @param Request $oRequest
     * @param string  $type
     * @param string  $token
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resetPasswordAction(Request $oRequest, $type, $token)
    {
        /** @var $oUserManager */
        $oUserManager = $this->get('flendoc.users.manager');

        $oUser = $this->getRepositoryByUserType($type)->findOneByConfirmationToken($token);
        if (null === $oUser) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $oForm = $this->createForm(ResetPasswordType::class, $oUser);

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {
            if (null == $oFormData->getPlainPassword()) {
                $this->addFlash('success', $this->trans('resetting.mail.no.action'));

                return $this->redirect('/login');
            }

            $oUserManager->updatePassword($oFormData);
            $this->addFlash('success', $this->trans('resetting.password.updated'));

            return $this->redirect('/login');
        }

        return $this->render('@App/User/Resetting/reset.html.twig', [
            'form'  => $oForm->createView(),
            'token' => $token,
        ]);
    }

    /**
     * Check Email action
     *
     * @Route("/resetting-check-email", name="resetting_check_email")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkEmailAction()
    {
        return $this->render('@App/User/Resetting/checkEmail.html.twig');
    }
}
