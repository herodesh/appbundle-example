<?php

namespace Flendoc\AppBundle\Controller\Users;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Form\Users\Type\ChangeEmailType;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ChangePasswordController
 * @package Flendoc\AppBundle\Controller\Users
 */
class ChangeEmailController extends FlendocController
{
    /**
     * @Route("/email-change", name="change_email")
     *
     * @param Request $oRequest
     *
     * @return RedirectResponse|Response
     * @throws GuzzleException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function requestEmailChangeAction(Request $oRequest)
    {
        /** @var $oUserManager */
        $oUserManager = $this->get('flendoc.users.manager');
        /** @var IndexerManager $oElasticsearchManager */
        $oElasticsearchIndexerManager = $this->get('flendoc.elasticsearch.manager');

        $oForm = $this->createForm(ChangeEmailType::class);

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {
            $oUserManager->changeUsername($this->getUser(), $oFormData['email']);
            $oElasticsearchIndexerManager->reindexUser($this->getUser()->getIdentifier());

            //rebuild user profile redis
            $sUserIdentifier = $this->getUser()->getIdentifier();
            $aUserProfileData = $this->getRepository(Doctors::class)
                ->findSimpleUserProfileDetails($sUserIdentifier);
            $oUserManager->rebuildAndDisplayCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $sUserIdentifier, $aUserProfileData);

            $this->addFlash('success', $this->trans('settings.general.account.email.updated'));

            //redirect to it's own start page
            switch ($oRequest->get('type')) {
                case AppConstants::USER_TYPE_DOCTORS:
                    return $this->redirect($this->generateUrl('doctors_email_settings_overview'));
                    break;

                case AppConstants::USER_TYPE_ADMINS:
                    return $this->redirect($this->generateUrl('admin_dashboard'));
                    break;
            }
        }

        return $this->render('@App/User/Resetting/change_email.html.twig', [
            'form' => $oForm->createView(),
        ]);
    }
}
