<?php

namespace Flendoc\AppBundle\Controller\Users;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Form\Doctors\Type\EditLocationType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxUpdateLocationController
 * @package Flendoc\AppBundle\Controller\Users
 *
 * @Route("/ajax-update-location")
 */
class AjaxUpdateLocationController extends FlendocController
{

    /**
     * Manage Users Location in Ajax Modal
     *
     * @Route("/get-user-location-form/{identifier}/{type}", name="ajax_get_user_location_form")
     *
     * @param Request $oRequest
     * @param         $identifier   User Identifier
     * @param null    $type         User Type
     *
     * @return JsonResponse
     */
    public function createFormUserLocationAction(Request $oRequest, $identifier, $type = null)
    {
        /** @var $oAdminRepo */
        $oUserTypeRepo = $this->getRepositoryByUserType($type);

        $oForm = $this->_createForm($oRequest, $identifier, $type);

        $sResponse = new JsonResponse(
            $this->renderView('@App/User/Countries/changeLocationForm.html.twig', [
                'form'               => $oForm->createView(),
                'userProfileDetails' => $oUserTypeRepo->findUserProfileDetails($identifier, $this->getUserLanguage()),
            ]),
            200);

        return $sResponse;
    }

    /**
     * Manage Users Location in Ajax Modal
     *
     * @Route("/process-user-location-form/{identifier}/{type}", name="ajax_process_user_location_form")
     *
     * @param Request $oRequest
     * @param         $identifier
     * @param null    $type
     *
     * @return JsonResponse
     */
    public function processUserLocationAction(Request $oRequest, $identifier, $type = null)
    {
        /** @var $oUserManager */
        $oUserManager = $this->get('flendoc.users.manager');
        /** @var $oAdminRepo */
        $oUserTypeRepo = $this->getRepositoryByUserType($type);

        $oForm = $this->_createForm($oRequest, $identifier, $type);

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {
            $oUserManager->saveObject($oFormData);

            $this->addFlash('success', $this->trans('users.location.update.success'));

            return new JsonResponse(['message' => 'Ok'], 200);
        }

        $sResponse = new JsonResponse([
            'form' => $this->renderView('@App/User/Countries/changeLocationForm.html.twig', [
                'form'               => $oForm->createView(),
                'userProfileDetails' => $oUserTypeRepo->findUserProfileDetails($identifier, $this->getUserLanguage()),
            ]),
        ], 400);

        return $sResponse;
    }

    /**
     * Create form
     *
     * @param Request $oRequest
     * @param         $identifier
     * @param null    $type
     *
     * @return \Symfony\Component\Form\Form|JsonResponse
     */
    private function _createForm(Request $oRequest, $identifier, $type = null)
    {
        if (!$oRequest->isXmlHttpRequest()) {
            return new JsonResponse(AppConstants::AJAX_ONLY, 400);
        }

        $oUserTypeRepo = $this->getRepositoryByUserType($type);
        $oUser         = $oUserTypeRepo->findOneByIdentifier($identifier);

        if (!$oUser) {
            return new JsonResponse('Could not find user', 400);
        }

        $getUserProfile = $this->_getProfileSetter($type);

        $oForm = $this->createForm(EditLocationType::class, $oUser->$getUserProfile(), [
            'language' => $this->getUserLanguage(),
            'manager'  => $this->getDoctrine()->getManager(),
        ]);

        return $oForm;
    }

}
