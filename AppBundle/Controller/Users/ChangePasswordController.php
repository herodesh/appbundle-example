<?php

namespace Flendoc\AppBundle\Controller\Users;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Form\Users\Type\ChangePasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ChangePasswordController
 * @package Flendoc\AppBundle\Controller\Users
 */
class ChangePasswordController extends FlendocController
{
    /**
     * @Route("/password-change", name="change_password")
     *
     * @param Request $oRequest
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function requestPasswordChangeAction(Request $oRequest)
    {
        /** @var $oUserManager */
        $oUserManager = $this->get('flendoc.users.manager');

        $oForm = $this->createForm(ChangePasswordType::class, $this->getUser());

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {
            $oUserManager->updatePassword($oFormData);
            $this->addFlash('success', $this->trans('settings.general.account.password.updated'));

            //redirect to it's own start page
            switch ($oRequest->get('type')) {
                case AppConstants::USER_TYPE_DOCTORS:
                    return $this->redirect($this->generateUrl('doctors_feeds'));
                    break;

                case AppConstants::USER_TYPE_ADMINS:
                    return $this->redirect($this->generateUrl('admin_dashboard'));
                    break;
            }
        }

        return $this->render('@App/User/Resetting/change_password.html.twig', [
            'form' => $oForm->createView(),
        ]);
    }
}
