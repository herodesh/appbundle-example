<?php

namespace Flendoc\AppBundle\Controller;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Security\Voter\FormPostVoter;
use Flendoc\AppBundle\Traits\App\EntityManagerTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class FlendocController
 * @package Flendoc\AppBundle\Controller
 *
 * @Route("/flendoc")
 */
class FlendocController extends Controller
{

    /** Deals with entity managers and repositories */
    use EntityManagerTrait;

    /**
     * @param Request $oRequest
     *
     * @throws \ErrorException
     */
    protected function checkAjaxRequest(Request $oRequest)
    {
        if (!$oRequest->isXmlHttpRequest()) {
            throw new \ErrorException(AppConstants::AJAX_ONLY, 400);
        }
    }

    /**
     * @param string $level
     * @param string $message
     */
    protected function log($level, $message)
    {
        $oLogger = $this->get('logger');
        $oLogger->log($level, $message);
    }

    /**
     * @Route("/download-image/{sFileDirectory}/{sImageName}", name="flendoc_download_aws_file")
     *
     * @param string $sFileDirectory
     * @param string $sFileName
     *
     * @return Response
     */
    public function downloadAwsFile($sFileDirectory, $sFileName)
    {
        //get S3 object. Key: folder/filename
        $oAwsObject = $this->get('flendoc.awss3.adapter')
                           ->getObjectFromAws($sFileDirectory.'/'.$sFileName);
        $oAwsObject = $oAwsObject->get('Body');

        $oResponse   = new Response($oAwsObject);
        $disposition = $oResponse->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $sFileName
        );

        $oResponse->headers->set('Content-Disposition', $disposition);

        return $oResponse;
    }

    /**
     * In Select2 the display element should be named "text". This method changes the field name into text where needed.
     *
     * @param $sFieldName
     * @param $aOriginalArray
     *
     * @return array
     */
    protected function select2ArrayKeyModifier($sFieldName, $aOriginalArray = null)
    {
        if (null != $aOriginalArray) {
            return array_map(function ($field) use ($sFieldName) {
                return [
                    'id'         => $field['id'],
                    'identifier' => isset($field['identifier']) ? $field['identifier'] : null,
                    'text'       => $field[$sFieldName],
                ];
            }, $aOriginalArray);
        }

        return null;
    }

    /**
     * Redirect to referer
     *
     * @param Request $oRequest
     *
     * @return RedirectResponse
     */
    protected function redirectToReferer(Request $oRequest)
    {
        $sRequestOrigin  = $oRequest->headers->get('host');
        $sRequestReferer = str_replace($sRequestOrigin, '', strstr($oRequest->headers->get('referer'), $sRequestOrigin));

        return $this->redirect($sRequestReferer);
    }

    /**
     * Render SubMenus
     *
     * @param array $aMenu
     *
     * @return string
     */
    protected function renderSubMenu(array $aMenu)
    {
        return $this->renderView(':layout:subMenu.html.twig', [
            'menu' => $aMenu,
        ]);
    }

    /**
     * Translate a string
     *
     * @param            $sString
     * @param array|null $parameters
     * @param string     $domain
     * @param null       $locale
     *
     * @return string
     */
    protected function trans($sString, array $parameters = [], $domain = "messages", $locale = null): string
    {
        return $this->get('translator')->trans($sString, $parameters, $domain, $locale);
    }

    /**
     * Translate a string for admin bundle
     *
     * @param string     $sString
     * @param array|null $parameters
     * @param string     $domain
     * @param null       $locale
     *
     * @return string
     */
    protected function adminTrans($sString, array $parameters = [], $domain = "admin", $locale = null): string
    {
        return $this->get('translator')->trans($sString, $parameters, $domain, $locale);
    }

    /**
     * Translate a string
     *
     * @param string     $sString
     * @param int        $number
     * @param array|null $parameters
     * @param string     $domain
     * @param null       $locale
     *
     * @return string
     */
    protected function transChoice(
        $sString,
        $number,
        array $parameters = [],
        $domain = "messages",
        $locale = null
    ): string {
        return $this->get('translator')->transChoice($sString, $number, $parameters, $domain, $locale);
    }

    /**
     * Process form
     *
     * @param Request $oRequest
     * @param Form    $oForm
     * @param null    $sRecaptcha
     *
     * @return bool|mixed|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function processPostForm(Request $oRequest, Form $oForm, $sRecaptcha = null)
    {
        if (Request::METHOD_POST != $oRequest->getMethod()) {
            return false;
        }

        $oForm->handleRequest($oRequest);

        if ($oForm->isSubmitted() && $oForm->isValid()) {
            /** @var FlendocManager $oFlendocManager */
            $oFlendocManager = $this->get('flendoc.manager');

            //check for recaptcha
            if (isset($sRecaptcha) && !$oFlendocManager->recaptchaVerify($sRecaptcha)) {
                $oForm->get('googleRecaptcha')
                      ->addError($oFlendocManager->createObject(FormError::class, 'doctors.contact.us.recaptcha.error'));

                return false;
            }

            //if user is not verified he cannot post anything
            if (!$this->isGranted(FormPostVoter::FORM_POST_VOTER, $oForm->getData())) {

                //for ajax calls we do not need the flash message
                if (!$oRequest->isXmlHttpRequest()) {
                    $this->addFlash('error', $this->trans('doctors.permission.not.enough'));
                }

                return $this->denyAccessUnlessGranted(FormPostVoter::FORM_POST_VOTER);
            }

            return $oForm->getData();
        }

        return;
    }

    /**
     * Get User Language Iso
     *
     * The correct order for checking (higher one has priority) is:
     * - check browsers "locale" cookie
     * - check the user and his defaultLanguageSetting
     * - check the application default locale
     *
     * @return Languages Language object
     */
    protected function getUserLanguage()
    {
        //if there is a cookie with name locale set get the locale preference
        if (null !== $this->get('session')->get('_locale')) {
            return $this->_getLanguageByIso($this->get('session')->get('_locale'));
        }

        //if user is loggedin get the user preference
        if ((null !== $this->getUser()) && $sDefaultLanguage = $this->getUser()->getDefaultLanguage()) {
            return $this->_getLanguageByIso($sDefaultLanguage);
        }

        return $this->_getLanguageByIso($this->getParameter('locale'));
    }

    /**
     * Get Repository by user type
     *
     * @param $type
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository|null
     */
    protected function getRepositoryByUserType($type)
    {
        switch ($type) {
            case AppConstants::USER_TYPE_ADMINS:
                return $this->getRepository(Admins::class);
                break;
            case AppConstants::USER_TYPE_DOCTORS:
                return $this->getRepository(Doctors::class);
                break;
            case AppConstants::USER_TYPE_PATIENTS:
                return null;
                break;
        }

        return null;
    }

    /**
     * Get User Profile by Type
     *
     * @param $type
     *
     * @return null|string
     */
    protected function _getProfileSetter($type)
    {
        switch ($type) {
            case AppConstants::USER_TYPE_ADMINS:
                return 'getAdminProfile';
                break;
            case AppConstants::USER_TYPE_DOCTORS:
                return 'getDoctorProfile';
                break;
            case AppConstants::USER_TYPE_PATIENTS:
                return 'getPatientProfile';
                break;
        }

        return null;
    }

    /**
     * @param $sIso
     *
     * @return Languages
     */
    private function _getLanguageByIso($sIso)
    {
        return $this->getRepository(Languages::class)->findOneBy([
            'iso' => $sIso,
        ]);
    }
}
