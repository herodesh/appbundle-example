<?php

namespace Flendoc\AppBundle\Controller\Skills;

use Flendoc\AppBundle\Controller\FlendocController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package Flendoc\AppBundle\Controller\Skills
 *
 * @Route("/ajax/skills")
 */
class AjaxController extends FlendocController
{
    /**
     * Ajax get Skills from query
     *
     * @Route("/find-skills", name="ajax_get_skills")
     *
     * @param Request $oRequest
     *
     * @return JsonResponse
     */
    public function ajaxGetSkillsAction(Request $oRequest): JsonResponse
    {
        $aSkills = $this->getRepository('AppBundle:Skills\Skills')
                                ->findAllByParametersAndLanguage($oRequest->query->get('q'), $this->getUserLanguage());

        if (empty($aSkills)) {
            return $this->json([], Response::HTTP_OK);
        }

        return $this->json($this->select2ArrayKeyModifier('name', $aSkills), Response::HTTP_OK);
    }
}
