<?php

namespace Flendoc\AppBundle\Controller\Languages;

use Flendoc\AppBundle\Controller\FlendocController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package Flendoc\AppBundle\Controller\Languages
 *
 * @Route("/ajax/languages")
 */
class AjaxController extends FlendocController
{
    /**
     * Ajax get Skills from query
     *
     * @Route("/find-languages", name="ajax_get_languages")
     *
     * @param Request $oRequest
     *
     * @return JsonResponse
     */
    public function ajaxGetLanguagesAction(Request $oRequest): JsonResponse
    {
        $aLanguages = $this->getRepository('AppBundle:Languages\Languages')
                           ->findAllByParametersAndPrimaryLanguage($this->getUserLanguage(), $oRequest->query->get('q'), true);

        if (null == $aLanguages) {
            return $this->json([], 200);
        }

        return $this->json($this->select2ArrayKeyModifier('name', $aLanguages), 200);
    }
}
