<?php

namespace Flendoc\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\Security;

/**
 * Class SecurityController
 * @package Flendoc\App
 * Bundle\Controller
 * @link    http://stackoverflow.com/questions/25853472/in-symfony-2-how-do-i-handle-routing-for-multiple-domains
 */
abstract class AbstractSecurityController extends FlendocController
{
    /**
     * @param Request $oRequest
     * @param string  $sHomeRoute             Ex. 'doctors_feeds'
     * @param string  $sRegisterUnlockRequest Ex. 'account_unlock_request'
     * @param string  $sViewPath              Ex. '@Doctors/Security/login.html.twig'
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function loginAction(Request $oRequest, $sHomeRoute, $sRegisterUnlockRequest, $sViewPath)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($sHomeRoute);
        }

        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $oSession = $oRequest->getSession();

        $authErrorKey    = '';
        $lastUsernameKey = '';

        if (class_exists('\Symfony\Component\Security\Core\Security')) {
            $authErrorKey    = Security::AUTHENTICATION_ERROR;
            $lastUsernameKey = Security::LAST_USERNAME;
        }

        // get the error if any (works with forward and redirect -- see below)
        if ($oRequest->attributes->has($authErrorKey)) {
            $error = $oRequest->attributes->get($authErrorKey);
        } elseif (null !== $oSession && $oSession->has($authErrorKey)) {
            $error = $oSession->get($authErrorKey);
            $oSession->remove($authErrorKey);
        } else {
            $error = null;
        }

        //if the account was not activated yet
        if (null != $error && $error instanceof DisabledException) {
            $this->addFlash('notice', $this->trans('registration.account.is.not.active'));

            return $this->redirectToRoute('request_confirmation_email');
        }

        //if the account is locked then redirect to instructions to unlock
        if (null != $error && $error instanceof LockedException) {
            return $this->redirectToRoute($sRegisterUnlockRequest);
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $oSession) ? '' : $oSession->get($lastUsernameKey);

        if ($this->has('security.csrf.token_manager')) {
            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        } else {
            $csrfToken = null;
        }

        $iLoginAttempts = $this->get('flendoc.users.manager')->getLoginAttempts($oSession->get(Security::LAST_USERNAME));

        return $this->render($sViewPath, [
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token'    => $csrfToken,
            'loginAttempts' => $iLoginAttempts
        ]);
    }

    /**
     * Login check
     */
    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * Logout Action
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }
}
