<?php

namespace Flendoc\AppBundle\Controller\Doctors;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Controller\FlendocController;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Form\Users\Type\ProfileType;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxUpdateLocationController
 * @package Flendoc\AppBundle\Controller\Users
 *
 * @Route("/ajax-edit-profile")
 */
class AjaxEditProfileController extends FlendocController
{

    /**
     * Manage Users Location in Ajax Modal
     *
     * @Route("/get-edit-profile-form/{type}/{sUserIdentifier}", name="ajax_get_edit_profile_form")
     *
     * @param Request     $oRequest
     * @param string      $type
     * @param string|null $sUserIdentifier
     *
     * @return JsonResponse
     * @throws \ErrorException
     */
    public function createFormAction(Request $oRequest, $type, $sUserIdentifier = null)
    {
        /** @var $oAdminRepo */
        $oUserTypeRepo   = $this->getRepositoryByUserType($type);
        $sUserIdentifier = (null == $sUserIdentifier) ? $this->getUser()->getIdentifier() : $sUserIdentifier;

        $oForm = $this->_createForm($oRequest, $type, $sUserIdentifier);

        $sResponse = $this->json(
            $this->renderView('@Doctors/Profile/editProfileForm.html.twig', [
                'form'        => $oForm->createView(),
                'userProfile' => $oUserTypeRepo->findSimpleUserProfileDetails($sUserIdentifier, $this->getUserLanguage()),
            ]),
            Response::HTTP_OK);

        return $sResponse;
    }

    /**
     * Manage Users Location in Ajax Modal
     *
     * @Route("/process-edit-profile-form/{type}/{identifier}", name="ajax_process_edit_profile_form")
     *
     * @param Request $oRequest
     * @param string  $type
     * @param null    $identifier
     *
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ErrorException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function processFormAction(Request $oRequest, $type, $identifier = null)
    {
        /** @var $oUserManager */
        $oUserManager = $this->get('flendoc.doctors.manager');
        /** @var $oAdminRepo */
        $oUserTypeRepo = $this->getRepositoryByUserType($type);
        /** @var $oFlendocManager */
        $oFlendocManager = $this->get('flendoc.manager');
        /** @var IndexerManager $oElasticsearchManager */
        $oElasticsearchIndexerManager = $this->get('flendoc.elasticsearch.manager');

        $sIdentifier = (null == $identifier) ? $this->getUser()->getIdentifier() : $identifier;

        $oForm = $this->_createForm($oRequest, $type, $sIdentifier);

        if ($oFormData = $this->processPostForm($oRequest, $oForm)) {

            $oUserManager->matchAndMergeNodeRelationships($sIdentifier, $oFormData);
            $oUserManager->saveObject($oFormData);
            $oElasticsearchIndexerManager->reindexUser($sIdentifier);

            $this->addFlash('success', $this->trans('doctors.profile.updated'));
            $oFlendocManager->deleteCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $sIdentifier);

            return $this->json(['message' => 'Ok'], Response::HTTP_OK);
        }

        $sResponse = $this->json([
            'form' => $this->renderView('@Doctors/Profile/editProfileForm.html.twig', [
                'form'               => $oForm->createView(),
                'userProfileDetails' => $oUserTypeRepo->findSimpleUserProfileDetails($sIdentifier, $this->getUserLanguage()),
            ]),
        ], Response::HTTP_BAD_REQUEST);

        return $sResponse;
    }

    /**
     * Create form
     *
     * @param Request     $oRequest
     * @param string      $type
     * @param string|null $identifier
     *
     * @return \Symfony\Component\Form\FormInterface|JsonResponse
     * @throws \ErrorException
     */
    private function _createForm(Request $oRequest, $type, $identifier = null)
    {
        $this->checkAjaxRequest($oRequest);

        $sIdentifier = (null == $identifier) ? $this->getUser()->getIdentifier() : $identifier;

        $oUserTypeRepo = $this->getRepositoryByUserType($type);
        $oUser         = $oUserTypeRepo->findOneByIdentifier($sIdentifier);

        if (!$oUser) {
            return $this->json('Could not find user', Response::HTTP_BAD_REQUEST);
        } elseif ($oUser->getIdentifier() != $this->getUser()->getIdentifier()) {
            return $this->json('Cannot edit user', Response::HTTP_BAD_REQUEST);
        }

        $getUserProfile = $this->_getProfileSetter($type);

        $oForm = $this->createForm(ProfileType::class, $oUser->$getUserProfile(), [
            'entityManager' => $this->getEntityManager(),
            'language'      => $this->getUserLanguage(),
        ]);

        return $oForm;
    }

}
