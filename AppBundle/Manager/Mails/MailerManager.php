<?php

namespace Flendoc\AppBundle\Manager\Mails;

use Flendoc\AppBundle\Entity\Doctors\ContactsInvitesExistingEmailAddresses;
use Flendoc\AppBundle\Entity\Mails\MailLanguages;
use Flendoc\AppBundle\Entity\Mails\Mails;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\Mails\MailsDefaultPropertiesTrait;
use Flendoc\AppBundle\Utils\DisposableEmailCheck;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class MailerManager
 * @package Flendoc\AppBundle\Manager\Mails
 */
class MailerManager extends FlendocManager
{

    use MailsDefaultPropertiesTrait;

    /**
     * @var DisposableEmailCheck
     */
    protected $disposableChecker;

    /**
     * MailerManager constructor.
     *
     * @param DisposableEmailCheck $disposableChecker
     */
    public function __construct(DisposableEmailCheck $disposableChecker)
    {
        $this->disposableChecker = $disposableChecker;
    }

    /**
     * Check if user accepts email from a specific email identifier
     * This will be used to see if an email can be sent to an user or not
     *
     * @param array         $aUserMailSettings
     * @param string        $sEmailIdentifier
     * @param UserInterface $oUser
     *
     * @return bool
     */
    public function userAcceptsEmailFrom(UserInterface $oUser, $aUserMailSettings, $sEmailIdentifier)
    {

        //if user does not have any mail settings (ex. User is just signed up)
        if (empty($aUserMailSettings) || (!$oUser->getEnabled() && !$oUser->getLocked() && !$oUser->getExpired())) {
            return true;
        }

        foreach ($aUserMailSettings as $aUserEmailSetting) {
            if ("1" == $aUserEmailSetting['enabled'] &&
                "1" == $aUserEmailSetting['isActive'] &&
                $sEmailIdentifier == $aUserEmailSetting['emailIdentifier']
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Synchronize emails from traits
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function synchronizeEmail()
    {
        //existing in the database
        $aExistingEmails = $this->getRepository('AppBundle:Mails\Mails')->findMails();

        $aExistingEmailIdentifiers = [];
        foreach ($aExistingEmails as $aExistingEmail) {
            $aExistingEmailIdentifiers[] = $aExistingEmail['emailIdentifier'];
        }

        foreach ($this->getMails()->aMails as $aMails) {
            foreach ($aMails as $sKey => $aMail) {
                if (!in_array($sKey, $aExistingEmailIdentifiers)) {
                    $this->em->persist($this->_createMailFromTrait($aMail));
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Adding the email to the database
     *
     * @param string $sEmail email that needs to be saved
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addInvitedExistingEmail($sEmail)
    {
        $oMail = $this->createObject(ContactsInvitesExistingEmailAddresses::class);

        $oMail->setEmail($sEmail);

        $this->saveObject($oMail);
    }

    /**
     * Create email object from trait data
     * Update the DB with newly created emails
     *
     * @param array $aDefaultMail
     *
     * @return Mails
     * @throws \Doctrine\ORM\ORMException
     */
    private function _createMailFromTrait(array $aDefaultMail)
    {

        $oMail = new Mails();
        $oMail->setEmailIdentifier($aDefaultMail['emailIdentifier']);
        $oMail->setVariables($aDefaultMail['variables']);
        $oMail->setSubjectVariables($aDefaultMail['subjectVariables']);
        $oMail->setIsActive($aDefaultMail['isActive']);
        $oMail->setIsAdministrative($aDefaultMail['isAdministrative']);
        $oMail->setIsPatient($aDefaultMail['isPatient']);
        $oMail->setIsAccount($aDefaultMail['isAccount']);
        $oMail->setIsExternal($aDefaultMail['isExternal']);

        foreach ($aDefaultMail['content'] as $sKey => $aContent) {
            $oMailLanguage = new MailLanguages();
            $oMailLanguage->setLanguages($this->getRepository('AppBundle:Languages\Languages')->findOneByIso($sKey));
            $oMailLanguage->setMail($oMail);
            $oMailLanguage->setSubject($aContent['subject']);
            $oMailLanguage->setContent($aContent['content']);
            $oMailLanguage->setShortDescription($aContent['shortDescription']);

            $oMail->addMailLanguage($oMailLanguage);
        }

        return $oMail;
    }

    /**
     * Checks if email is valid. Code was inspired by Symfony Email validator
     *
     * @param string $sEmail
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function isEmailValid($sEmail)
    {
        $sEmail = (string) $sEmail;

        //check if the email has an email pattern
        if (!preg_match('/^.+\@\S+\.\S+$/', $sEmail)) {
            return false;
        }

        // check if email is disposable
        if ($this->disposableChecker->isDisposableEmail($sEmail)) {
            return false;
        }

        return true;
    }
}
