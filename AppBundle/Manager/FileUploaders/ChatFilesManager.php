<?php

namespace Flendoc\AppBundle\Manager\FileUploaders;

use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\Chats\DoctorChatFile;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Uploader\DocumentsUploader;

/**
 * Class ChatFilesManager
 * @package Flendoc\AppBundle\Manager\FileUploaders
 */
class ChatFilesManager extends FlendocManager
{
    /** @var DocumentsUploader */
    protected $documentsUploader;

    /**
     * MessagingFilesManager constructor.
     *
     * @param DocumentsUploader $documentsUploader
     */
    public function __construct(DocumentsUploader $documentsUploader)
    {
        $this->documentsUploader = $documentsUploader;
    }

    /**
     * @param string $sChatIdentifier
     * @param array $aFile
     * @param Doctors $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFile($sChatIdentifier, $aFile, $oUser):void
    {
        /** @var DoctorChatFile $oDoctorChatFile */
        $oDoctorChatFile = $this->createObject(DoctorChatFile::class);

        $oDoctorChat = $this
            ->getRepository(DoctorChat::class)
            ->findOneBy([
                'identifier' => $sChatIdentifier,
            ])
        ;

        $oDoctorChatFile
            ->setDoctorChat($oDoctorChat)
            ->setDoctorUploader($oUser)
            ->setName($aFile['newFileName'])
            ->setOriginalFileName($aFile['originalFileName'])
        ;
        $this->saveObject($oDoctorChatFile);
    }
}
