<?php

namespace Flendoc\AppBundle\Manager\FileUploaders;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Uploader\PhotoUploader;

/**
 * Class ProfilePhotoManager
 * @package Flendoc\AppBundle\Manager\FileUploaders
 */
class ProfilePhotoManager extends FlendocManager
{

    /** @var PhotoUploader */
    protected $photoUploader;

    /**
     * PhotosManager constructor.
     *
     * @param PhotoUploader $photoUploader
     */
    public function __construct(PhotoUploader $photoUploader)
    {
        $this->photoUploader = $photoUploader;
    }

    /**
     * @param $oDoctor
     * @param $aFiles
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function saveProfileImage($oDoctor, $aFiles): void
    {
        $this->deleteProfileImage($oDoctor);

        $oDoctor->getDoctorProfile()->setProfilePhoto(json_encode($aFiles));

        //save object
        $this->saveObject($oDoctor);
        //update redis entry with the new data
        $this->_rebuildProfileCache($oDoctor);
    }

    /**
     * Delete profile image from database and AWS
     *
     * @param      $oDoctor
     * @param bool $bRebuildCache
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteProfileImage($oDoctor, $bRebuildCache = false): void
    {
        $sPreviousImages = $oDoctor->getDoctorProfile()->getProfilePhoto();

        //delete previous image from AWS
        if (null != $sPreviousImages) {
            $aPreviousImages = json_decode($sPreviousImages, true);

            foreach ($aPreviousImages['files'] as $photo) {
                $this->photoUploader->delete(sprintf('%s/%s', AppConstants::AMAZON_PROFILE_PHOTO_DIRECTORY, $photo));
            }
        }

        $oDoctor->getDoctorProfile()->setProfilePhoto(null);

        //save object
        $this->saveObject($oDoctor);

        //rebuild profile cache
        if ($bRebuildCache) {
            $this->_rebuildProfileCache($oDoctor);
        }
    }

    /**
     * @param $oDoctor
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function _rebuildProfileCache($oDoctor): void
    {
        //find profile details based on user language
        $aProfileDetails = $this->getRepository(Doctors::class)
                                ->findSimpleUserProfileDetails($oDoctor->getIdentifier());

        $this->rebuildAndDisplayCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $oDoctor->getIdentifier(), $aProfileDetails);
    }
}
