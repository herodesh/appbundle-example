<?php

namespace Flendoc\AppBundle\Manager\LegalAndHelp;

use Flendoc\AppBundle\Adapter\Slugify\SlugifyManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\HelpAndLegal\FaqTrait;

/**
 * Class LegalAndHelpManager
 * @package Flendoc\AppBundle\Manager\LegalAndHelp
 */
class LegalAndHelpManager extends FlendocManager
{

    use FaqTrait;

    /** @var SlugifyManager */
    protected $slugify;

    /**
     * LegalAndHelpManager constructor.
     *
     * @param SlugifyManager $slugify
     */
    public function __construct(SlugifyManager $slugify)
    {
        $this->slugify = $slugify;
    }

    /**
     * @param Faq  $oObject
     * @param bool $bNewEntry
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFaq($oObject, $bNewEntry = false)
    {
        // If it's a new entry we need to set the position
        if ($bNewEntry) {
            //get last order
            $oLastFaqOrder = $this->getRepository(Faq::class)->findLastFaqByOrderNumber();

            $oObject->setPosition((int)$oLastFaqOrder + 1);
        }

        //set up the slug entries
        foreach($oObject->getFaqTranslations() as $oFaqTranslations) {
            if (!empty($oFaqTranslations->getTitle())) {
                $sSlugifyText = $this->slugify->generateSlug($oFaqTranslations->getTitle(), $oFaqTranslations->getLanguage()->getIso());
                $oFaqTranslations->setSlug($sSlugifyText);
            }
        }

        $this->saveObject($oObject);
    }

    /**
     * @param $oObject
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFaqGroup($oObject)
    {
        //set up the slug entries
        foreach($oObject->getFaqGroupTranslations() as $oFaqGroupTranslations) {
            if (!empty($oFaqGroupTranslations->getTitle())) {
                $sSlugifyText = $this->slugify->generateSlug($oFaqGroupTranslations->getTitle(), $oFaqGroupTranslations->getLanguage()->getIso());
                $oFaqGroupTranslations->setSlug($sSlugifyText);
            }
        }

        $this->saveObject($oObject);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function synchronizeFaqs()
    {
        //existing in the database
        $aExistingFaqs = $this->getRepository(Faq::class)->findAll();

        $aExistingFaqsStringIdentifiers = [];
        foreach ($aExistingFaqs as $aExistingFaq) {
            $aExistingFaqsStringIdentifiers[] = $aExistingFaq->getFaqIdentifier();
        }

        foreach ($this->getFaqs()->aFaqs as $aFaqs) {
            foreach ($aFaqs as $sKey => $aFaq) {
                if (!in_array($sKey, $aExistingFaqsStringIdentifiers)) {
                    $this->em->persist($this->_createFaqFromTrait($aFaq));
                }
            }
        }

        $this->em->flush();
    }

    /**
     * @param array $aFaq
     *
     * @return Faq
     */
    private function _createFaqFromTrait(array $aFaq)
    {
        $oFaq = new Faq();

        $oFaq->setPosition($aFaq['position']);
        $oFaq->setIsActive($aFaq['isActive']);
        $oFaq->setFaqIdentifier(trim($aFaq['faqIdentifier']));

        //add email content
        foreach ($aFaq['content'] as $sLanguageIso => $aFaqContent) {
            $oFaqTranslations = new FaqTranslations();

            $oFaqTranslations->setFaq($oFaq);
            $oFaqTranslations->setLanguage($this->em->getRepository(Languages::class)
                                                   ->findOneByIso($sLanguageIso));
            $oFaqTranslations->setTitle(trim($aFaqContent['title']));
            $oFaqTranslations->setDescription(trim($aFaqContent['description']));

            $oFaq->addFaqTranslations($oFaqTranslations);
        }

        return $oFaq;
    }

}
