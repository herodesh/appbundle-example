<?php

namespace Flendoc\AppBundle\Manager\Resumes;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\DoctorProfile;
use Flendoc\AppBundle\Entity\Resumes\ResumeHeader;
use Flendoc\AppBundle\Entity\Resumes\ResumeOtherInformation;
use Flendoc\AppBundle\Entity\Resumes\Resumes;
use Flendoc\AppBundle\Entity\Resumes\ResumeSectionLanguages;
use Flendoc\AppBundle\Entity\Resumes\ResumeSections;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\Resume\ResumeSectionTrait;
use Flendoc\AppBundle\Uploader\PhotoUploader;

/**
 * Class ResumeManager
 * @package Flendoc\AppBundle\Manager\Resumes
 */
class ResumeManager extends FlendocManager
{
    use ResumeSectionTrait;

    /** @var PhotoUploader */
    protected $photoUploader;

    /**
     * ResumeManager constructor.
     *
     * @param PhotoUploader $photoUploader
     */
    public function __construct(PhotoUploader $photoUploader)
    {
        $this->photoUploader = $photoUploader;
    }

    /**
     * Saves the image used in the resume
     *
     * @param array $aFiles
     * @param       $resumeIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveResumeImage(array $aFiles, $resumeIdentifier): void
    {
        $oResumeHeader = $this->em->getRepository('AppBundle:Resumes\ResumeHeader')
                                  ->findOneByResume($this->em->getRepository('AppBundle:Resumes\Resumes')
                                                             ->findOneByIdentifier($resumeIdentifier));

        //find and remove the file from amazon
        if (!$oResumeHeader->getIsAvatarSync()) {
            $this->photoUploader->delete(sprintf("%s/%s", AppConstants::AMAZON_RESUME_PHOTO_DIRECTORY, $oResumeHeader->getAvatar()));
        }

        $oResumeHeader->setAvatar($aFiles['files'][AppConstants::IMAGE_SMALL_SIZE]);
        $oResumeHeader->setIsAvatarSync(false);

        $this->em->flush();
    }

    /**
     * Synchronizes the resumes section
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function synchronizeResumeSections(): void
    {
        //check for existing entries in the DB
        $aExistingSections    = $this->getRepository('AppBundle:Resumes\ResumeSections')->findAll();
        $aExistingSectionName = [];

        foreach ($aExistingSections as $aExistingSection) {
            $aExistingSectionName[] = $aExistingSection->getSectionName();
        }

        foreach ($this->aResumeSections as $resumeSection) {
            if (!in_array($resumeSection['section_name'], $aExistingSectionName)) {
                $this->em->persist($this->_createResumeSectionsFromTrait($resumeSection));
            }
        }

        $this->em->flush();
    }

    /**
     * Synchronizes the header section of the CV with the data from Doctors profile
     *
     * @param Resumes       $oResume
     * @param DoctorProfile $oDoctorProfile
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function synchronizeResumeHeaderSection(Resumes $oResume, DoctorProfile $oDoctorProfile): void
    {
        $oResumeHeader = $this->getRepository('AppBundle:Resumes\ResumeHeader')->findOneByResume($oResume);

        if (null == $oResumeHeader) {

            //delete existing resume data
            $this->deleteCacheData(CacheKeyPrefixes::RESUMES, $oResume->getIdentifier());

            $oResumeHeader = $this->createObject(ResumeHeader::class);

            $oResumeHeader->setResume($oResume);
            $oResumeHeader->setDoctor($oDoctorProfile->getDoctor());
            $oResumeHeader->setResumeSection($this->getRepository('AppBundle:Resumes\ResumeSections')
                                                  ->findOneBySectionName(ResumeSections::RESUME_SECTION_HEADER));
            $oResumeHeader->setCountry($oDoctorProfile->getCountry());
            $oResumeHeader->setCity($oDoctorProfile->getCity());
            $oResumeHeader->setFirstName($oDoctorProfile->getFirstName());
            $oResumeHeader->setLastName($oDoctorProfile->getLastName());
            $oResumeHeader->setGender($oDoctorProfile->getGender());
            $oResumeHeader->setBirthDate($oDoctorProfile->getBirthday());

            $this->em->persist($oResumeHeader);
            $this->em->flush();
        }
    }

    /**
     * Translates the section title based on section key / name
     *
     * @param $sectionType
     * @param $languageIso
     *
     * @return string
     */
    public function otherInformationTitleFromSectionType($sectionType, $languageIso): string
    {
        switch ($sectionType) {
            default:
                $sFormTitle = 'nothing selected';
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_DRIVER_LICENSE:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.driver.license', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_CERTIFICATION:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.certification', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_PRIZES_AND_DISTINCTIONS:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.prizes.and.distinctions', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_PROJECTS:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.projects', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_VOLUNTEERING:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.volunteering', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_CONFERENCES:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.conferences', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_PUBLISHED_WORKS:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.published.works', [], 'messages', $languageIso);
                break;

            case ResumeOtherInformation::RESUME_OTHER_INFO_TRAININGS:
                $sFormTitle = $this->translator->trans('doctors.resume.other.info.title.trainings', [], 'messages', $languageIso);
                break;
        }

        return $sFormTitle;
    }

    /**
     * Delete resume image
     *
     * @param ResumeHeader $oResumeHeader
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteResumeImage(ResumeHeader $oResumeHeader): void
    {
        $this->photoUploader->delete(sprintf("%s/%s", AppConstants::AMAZON_RESUME_PHOTO_DIRECTORY, $oResumeHeader->getAvatar()));
        $oResumeHeader->setAvatar(null);
        $this->em->flush();
    }

    /**
     * @param $resumeSection
     *
     * @return ResumeSections
     * @throws \Doctrine\ORM\ORMException
     */
    private function _createResumeSectionsFromTrait($resumeSection): ResumeSections
    {
        $oResumeSection = new ResumeSections();

        $oResumeSection->setSectionName($resumeSection['section_name']);
        $oResumeSection->setOrderNumber($resumeSection['properties']['order']);
        $oResumeSection->setIsSecondary($resumeSection['properties']['isSecondary']);

        foreach ($resumeSection['content'] as $sLanguageKey => $sContent) {
            $oResumeSectionLanguage = new ResumeSectionLanguages();
            $oResumeSectionLanguage->setLanguages($this->em->getRepository('AppBundle:Languages\Languages')
                                                           ->findOneByIso($sLanguageKey)
            );
            $oResumeSectionLanguage->setName($sContent);
            $oResumeSectionLanguage->setResumeSections($oResumeSection);

            $oResumeSection->addResumeSectionLanguages($oResumeSectionLanguage);
        }

        return $oResumeSection;
    }
}
