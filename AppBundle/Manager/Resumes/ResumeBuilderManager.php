<?php

namespace Flendoc\AppBundle\Manager\Resumes;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class ResumeBuilderManager
 * @package Flendoc\AppBundle\Manager\Resumes
 */
class ResumeBuilderManager extends FlendocManager
{

    /**
     * Builds the structure of the resume, saves it in redis
     *
     * @param $resume_identifier
     *
     * @return array|null
     * @throws \Exception
     */
    public function buildResume($resume_identifier)
    {
        $oResumeRepo = $this->getRepository('AppBundle:Resumes\Resumes');

        $oResume               = $oResumeRepo->findOneByIdentifier($resume_identifier);
        $currentResumeSections = $oResumeRepo->findCurrentResumeSections($resume_identifier, $oResume);
        $aResumeContent        = [];

        if (null == $oResume) {
            return null;
        }

        $this->cacheManager->setKeyPrefix(CacheKeyPrefixes::RESUMES);
        $item = $this->cacheManager->getItem($oResume->getIdentifier());

        //if there is no entry in the database related to the resume prepare the data that needs to be saved
        //This is a special case and we use the cache manager like this and not from the FlendocManager
        if (!$item->isHit()) {
            foreach ($currentResumeSections as $resumeSection) {
                $entityName = 'AppBundle:Resumes\Resume'.str_replace('_', '', ucwords($resumeSection['resume_section_name'], '_'));

                $aResumeContent[$resumeSection['resume_section_name']]['defaults'] = $resumeSection;

                try {
                    if ($this->getRepository($entityName) instanceof EntityRepository) {

                        $rsn         = $resumeSection['resume_section_name'];
                        $languageIso = $resumeSection['language_iso'];
                        $oLanguage   = $this->em->getRepository('AppBundle:Languages\Languages')
                                                ->findOneByIso($languageIso);

                        $sectionData = $this->em->createQueryBuilder('q')
                                                ->select($rsn)
                                                ->from($entityName, $rsn)
                                                ->where($rsn.'.doctor = :doctors')
                                                ->andWhere($rsn.'.resume = :resume')
                                                ->setParameter('doctors', $this->em->getReference('AppBundle:Doctors\Doctors', $resumeSection['doctor_id']))
                                                ->setParameter('resume', $oResume)
                                                ->getQuery()
                                                ->getResult();

                        //build the CV Structure
                        switch ($rsn) {
                            //build CV header
                            case 'header':

                                if ($sectionData) {

                                    $currentJob = $this->em->getRepository('AppBundle:Resumes\ResumeWorkExperience')
                                                           ->findCurrentJob($oResume);

                                    $aResumeContent[$rsn]['header'] = [
                                        'fullName'       => $sectionData[0]->getFirstName().' '.$sectionData[0]->getLastName(),
                                        'age'            => $this->_createDateTimeText($sectionData[0]->getBirthDate(), new \DateTime('now'), $languageIso),
                                        'city'           => $sectionData[0]->getCity()->getDisplayCityName($languageIso),
                                        'country'        => $sectionData[0]->getCountry()->getCountryDefaultName($languageIso),
                                        'gender'         => $sectionData[0]->getGender(),
                                        'currentJob'     => $currentJob,
                                        'hideCurrentJob' => $sectionData[0]->getHideCurrentJob(),
                                        'avatar'         => $sectionData[0]->getAvatar(),
                                        'identifier'     => $sectionData[0]->getIdentifier(),
                                    ];
                                } else {
                                    $aResumeContent[$rsn]['header'] = null;
                                }

                                break;

                            //build contact details
                            case 'contact_details':

                                if ($sectionData) {
                                    $aResumeContent[$rsn]['contact_details'] = [
                                        'emailAddress'   => $sectionData[0]->getEmailAddress(),
                                        'contactPhone'   => $sectionData[0]->getContactPhone(),
                                        'contactAddress' => $sectionData[0]->getContactAddress(),
                                        'identifier'     => $sectionData[0]->getIdentifier(),
                                    ];
                                } else {
                                    $aResumeContent[$rsn]['contact_details'] = null;
                                }
                                break;

                            //build professional objective
                            case 'professional_objective':

                                if ($sectionData) {
                                    $aResumeContent[$rsn]['professional_objective'] = [
                                        'description' => $sectionData[0]->getDescription(),
                                        'identifier'  => $sectionData[0]->getIdentifier(),
                                    ];
                                } else {
                                    $aResumeContent[$rsn]['professional_objective'] = null;
                                }
                                break;

                            //build work experience
                            case 'work_experience':

                                if ($sectionData) {
                                    foreach ($sectionData as $sd) {
                                        $aResumeContent[$rsn]['work_experience'][] = [
                                            'jobTitle'           => $sd->getJobTitle(),
                                            'description'        => $sd->getDescription(),
                                            'startDate'          => $sd->getStartDate(),
                                            'endDate'            => $sd->getEndDate(),
                                            'isCurrent'          => $sd->getIsCurrent(),
                                            'cityName'           => $sd->getCity()->getDisplayCityName($languageIso),
                                            'countryName'        => $sd->getCountry()->getCountryDefaultName(),
                                            'workTimePeriod'     => $this->_createDateTimePeriod($sd->getStartDate(), $sd->getEndDate(), $sd->getIsCurrent(), $languageIso),
                                            'workTimePeriodText' => $this->_createDateTimeText($sd->getStartDate(), $sd->getEndDate(), $languageIso, true),
                                            'institution'        => $sd->getInstitutionName(),
                                            'identifier'         => $sd->getIdentifier(),
                                        ];
                                    }

                                } else {
                                    $aResumeContent[$rsn]['work_experience'] = null;
                                }
                                break;

                            //build education
                            case 'education':

                                if ($sectionData) {
                                    foreach ($sectionData as $sd) {

                                        $aResumeContent[$rsn]['education'][] = [
                                            'studyProfile'        => $sd->getStudyProfile(),
                                            'diplomaType'         => $sd->getResumeEducationDiplomaType()
                                                                        ->getDisplayName($languageIso),
                                            'startDate'           => $sd->getStartDate(),
                                            'endDate'             => $sd->getEndDate(),
                                            'isCurrent'           => $sd->getIsCurrent(),
                                            'studyTimePeriod'     => $this->_createDateTimePeriod($sd->getStartDate(), $sd->getEndDate(), $sd->getIsCurrent(), $languageIso, true),
                                            'studyTimePeriodText' => $this->_createDateTimeText($sd->getStartDate(), $sd->getEndDate(), $languageIso, true),
                                            'institution'         => $sd->getInstitutionName(),
                                            'description'         => $sd->getDescription(),
                                            'identifier'          => $sd->getIdentifier(),
                                        ];
                                    }
                                } else {
                                    $aResumeContent[$rsn]['education'] = null;
                                }

                                break;

                            //build skills
                            case 'skills':

                                if ($sectionData) {
                                    foreach ($sectionData as $sd) {
                                        foreach ($sd->getResumeSkillsList() as $resumeSkill) {
                                            $aResumeContent[$rsn]['skills']['identifier'] = $sd->getIdentifier(); //the skills group identifier
                                            $aResumeContent[$rsn]['skills'][]             = [
                                                'skills' => $resumeSkill->getDisplayName($languageIso),
                                            ];
                                        }

                                    }
                                } else {
                                    $aResumeContent[$rsn]['skills'] = null;
                                }
                                break;

                            //build foreign languages
                            case 'foreign_languages':

                                if ($sectionData) {
                                    foreach ($sectionData as $sd) {
                                        $aResumeContent[$rsn]['foreign_languages'][] = [
                                            'foreignLanguage' => $sd->getLanguage()->getNameByIso($languageIso),
                                            'knowledgeLevel'  => $sd->getKnowledgeLevel(),
                                            'languageIso'     => $languageIso,
                                            'identifier'      => $sd->getIdentifier(),
                                        ];
                                    }
                                } else {
                                    $aResumeContent[$rsn]['foreign_languages'] = null;
                                }

                                break;

                            //build other information
                            case 'other_information':

                                if ($sectionData) {
                                    foreach ($sectionData as $sd) {
                                        $aResumeContent[$rsn]['other_information'][$sd->getOtherInformationType()][] = [
                                            'startDate'   => $sd->getStartDate(),
                                            'endDate'     => $sd->getEndDate(),
                                            'timePeriod'  => $this->_createDateTimePeriod($sd->getStartDate(), $sd->getEndDate(), null, $languageIso, true),
                                            'name'        => $sd->getName(),
                                            'description' => $sd->getDescription(),
                                            'identifier'  => $sd->getIdentifier(),
                                        ];

                                        //sort to display dates in their date order
                                        usort($aResumeContent[$rsn]['other_information'][$sd->getOtherInformationType()],
                                            function ($a, $b) {
                                                return new \DateTime(date_format($a['endDate'], 'Y-m-d')) <=> new \DateTime(date_format($b['endDate'], 'Y-m-d'));
                                            }
                                        );
                                    }
                                } else {
                                    $aResumeContent[$rsn]['other_information'] = null;
                                }
                                break;

                            case 'preferences':
                                if ($sectionData) {

                                    //anonymous function to create job type entries
                                    $jobType = function () use ($sectionData, $oLanguage) {
                                        $jobTypeEntries = [];
                                        foreach ($sectionData[0]->getJobTypes() as $jobType) {
                                            $jobTypeEntries[] = $jobType->getDisplayName($oLanguage);
                                        }

                                        return $jobTypeEntries;
                                    };

                                    $skills = function () use ($sectionData, $oLanguage) {
                                        $skillsEntry = [];
                                        foreach ($sectionData[0]->getSkills() as $skills) {
                                            $skillsEntry[] = $skills->getDisplayName($oLanguage);
                                        }

                                        return $skillsEntry;
                                    };

                                    $aResumeContent[$rsn]['preferences'] = [
                                        'lookingForJob' => $sectionData[0]->getLookingForJob(),
                                        'careerLevel'   => $sectionData[0]->getCareerLevel()
                                                                          ->getDisplayName($oLanguage),
                                        'jobType'       => $jobType(),
                                        'skills'        => $skills(),
                                        'identifier'    => $sectionData[0]->getIdentifier(),
                                    ];
                                } else {
                                    $aResumeContent[$rsn]['preferences'] = null;
                                }

                                break;

                            case
                            'desired_job':
                                break;
                        }
                    }
                } catch
                (\Exception $e) {
                    echo $e->getMessage();
                }
            }

            $aResumeContent = $this->rebuildAndDisplayCacheData(CacheKeyPrefixes::RESUMES, $oResume->getIdentifier(), $aResumeContent);
        } else {
            //if item is hit get the data
            $aResumeContent = $item->get();
        }

        return $aResumeContent;
    }

    /**
     * @param         $startDate
     * @param         $endDate
     * @param         $languageIso
     * @param boolean $month
     *
     * @return string
     */
    private
    function _createDateTimeText(
        $startDate,
        $endDate,
        $languageIso,
        $month = false
    ) {
        if (null == $startDate) {
            return null;
        }

        $yearsCount     = AppStatic::calculateDateDiff($startDate, $endDate);
        $monthCount     = AppStatic::calculateDateDiff($startDate, $endDate, 'm');
        $workPeriodText = '';

        if ($yearsCount > 0) {
            $workPeriodText .= $yearsCount.' '.$this->translator->trans(AppStatic::displayTimeDivisions($yearsCount), [], 'messages', $languageIso);
        }

        if ($monthCount > 0 && $month) {
            if ($yearsCount > 0) {
                $workPeriodText .= ' '.$this->translator->trans('site.and', [], 'messages', $languageIso).' ';
            }
            $workPeriodText .= $monthCount.' '.$this->translator->trans(AppStatic::displayTimeDivisions($monthCount, 'm'), [], 'messages', $languageIso);
        }

        return $workPeriodText;
    }

    /**
     * @param      $startDate
     * @param      $endDate
     * @param      $isCurrent
     * @param      $languageIso
     * @param bool $yearsOnly Display only years
     *
     * @return string
     */
    private
    function _createDateTimePeriod(
        $startDate,
        $endDate,
        $isCurrent = null,
        $languageIso,
        $yearsOnly = false
    ): string {
        $dateFormat         = ($yearsOnly) ? 'Y' : 'F Y';
        $presentTranslation = $this->translator->trans('site.date.present', [], 'messages', $languageIso);

        $startDate = date_format($startDate, $dateFormat);
        $startDate = $this->_translateDate($startDate, $languageIso);
        $endDate   = (null != $endDate) ? date_format($endDate, $dateFormat) : null;

        if (null != $endDate) {
            $endDate = $this->_translateDate($endDate, $languageIso);
        }

        $periodTime = (string)$startDate.((null != $isCurrent && null == $endDate) ? ' - '.$presentTranslation :
                ((null != $endDate) ? ' - ' : '').$endDate);

        return $periodTime;
    }

    /**
     * @param $date
     * @param $languageIso
     *
     * @return string
     */
    private
    function _translateDate(
        $date,
        $languageIso
    ): string {
        $aDate = explode(' ', $date);

        if (isset($aDate[1])) {
            $date = implode(' ', [
                $this->translator->trans('site.'.strtolower($aDate[0]), [], 'messages', $languageIso),
                $aDate[1],
            ]);
        } else {
            return $date;
        }

        return $date;
    }
}
