<?php

namespace Flendoc\AppBundle\Manager;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\Doctors;

/**
 * Class FlendocManager
 * @package Flendoc\AppBundle\Manager
 */
class FlendocManager extends AbstractFlendocManager implements FlendocManagerInterface
{
    /**
     * @param $oObject
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveObject($oObject)
    {
        $this->em->persist($oObject);
        $this->em->flush();

        return $oObject;
    }

    /**
     * @param $oObject
     *
     * @return mixed
     */
    public function saveMongoObject($oObject)
    {
        $this->odm->persist($oObject);
        $this->odm->flush();

        return $oObject;
    }

    /**
     * @param array $aObjects
     */
    public function saveMongoObjects($aObjects): void
    {
        foreach ($aObjects as $oObject) {
            $this->odm->persist($oObject);
        }

        $this->odm->flush();
    }

    /**
     * @param $oObject
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteObject($oObject): void
    {
        $this->em->remove($oObject);
        $this->em->flush();
    }

    /**
     * @param $oObject
     */
    public function deleteMongoObject($oObject): void
    {
        $this->odm->remove($oObject);
        $this->odm->flush();
    }

    /**
     * Create an empty object
     *
     * @param string       $sObject    Class name string
     * @param string|array $properties Properties to be passed to the object
     *
     * @return object
     */
    public function createObject($sObject, $properties = null)
    {
        if (null != $properties) {
            return new $sObject($properties);
        }

        return new $sObject();
    }

    /**
     * Display cached data based on prefix key and item key, null if nothing is found
     *
     * @param $prefixKey
     * @param $itemKey
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function displayCacheData($prefixKey, $itemKey)
    {
        $this->cacheManager->setKeyPrefix($prefixKey);
        $item = $this->cacheManager->getItem($itemKey);

        if ($item->isHit()) {
            return $item->get();
        }

        return [];
    }

    /**
     * Save entry to redis cache
     *
     * @param      $prefixKey
     * @param      $itemKey
     * @param null $passedData
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function saveCacheData($prefixKey, $itemKey, $passedData = null): void
    {
        if (null != $passedData) {
            $this->cacheManager->setKeyPrefix($prefixKey);
            $item = $this->cacheManager->getItem($itemKey);
            $item->set($passedData);
            $this->cacheManager->save($item);
            $this->cacheManager->resetKeyPrefix();
        }
    }

    /**
     * Rebuilds cache data based on the prefixKey, itemKey and passedData. Returns null if there is no data given
     *
     * @param      $prefixKey
     * @param      $itemKey
     * @param null $passedData
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function rebuildAndDisplayCacheData($prefixKey, $itemKey, $passedData = null)
    {
        $this->cacheManager->setKeyPrefix($prefixKey);
        $item = $this->cacheManager->getItem($itemKey);

        //first delete entry if any
        if (null != $passedData) {
            //delete existing entries
            if ($item->isHit()) {
                $this->cacheManager->deleteItem($itemKey);
            }
        }

        //saving entry to Cache
        $this->saveCacheData($prefixKey, $itemKey, $passedData);

        //we need to get again the item in order for the display to work properly
        $this->cacheManager->setKeyPrefix($prefixKey);
        $item = $this->cacheManager->getItem($itemKey);

        if ($item->isHit()) {
            return $item->get();
        }

        return [];
    }

    /**
     * Delete cache data when needed
     *
     * @param $prefixKey
     * @param $itemKey
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteCacheData($prefixKey, $itemKey)
    {
        $this->cacheManager->setKeyPrefix($prefixKey);
        $item = $this->cacheManager->getItem($itemKey);

        if ($item->isHit()) {
            //delete the exiting entry
            $this->cacheManager->deleteItem($itemKey);
        }
    }

    /**
     * @param string $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserProfileDetails($sUserIdentifier)
    {
        return $this->em->getRepository(Doctors::class)
                        ->findSimpleUserProfileDetails($sUserIdentifier);
    }

    /**
     * We use reCaptcha v2
     *
     * @param $sRecaptcha String returned by google
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function recaptchaVerify($sRecaptcha)
    {
        $sUrl = AppConstants::GOOGLE_RECAPTCHA_URL;

        $aResponse = $this->guzzleClient->request('POST', $sUrl, [
            'form_params' => [
                "secret"   => $this->googleRecaptchaSecretKey,
                "response" => $sRecaptcha,
            ],
        ]);

        return json_decode($aResponse->getBody()->getContents())->success;
    }

    /**
     * We are using Cortical.io API to extract the keywords from texts
     *
     * @param $sText
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function extractKeywordsFromText($sText)
    {
        $aResponse = [];
        $sUrl      = AppConstants::REUTERS_CALAIS_KEYWORDS_URL;

        $aCurlResponse = $this->guzzleClient->request('POST', $sUrl, [

            'headers' => [
                'Content-Type'               => 'text/plain',
                'x-ag-access-token'          => $this->reutersCalaisApiKey,
                'Accept'                     => 'application/json',
                'outputFormat'               => 'application/json',
                'x-calais-selectiveTags'     => 'socialtags',
                'omitOutputtingOriginalText' => true,

            ],

            'form_params' => [
                'body' => $sText,
            ],
        ]);

        $aCurlResponse = json_decode($aCurlResponse->getBody()->getContents(), true);
        //we do not want terms that are smaller than 3 characters
        foreach ($aCurlResponse as $aItem) {
            //From the documentation - https://developers.refinitiv.com/sites/default/files/RefinitivIntelligentTaggingAPIUserGuideR12_6_0.pdf
            //1 – Only the social tags with importance level 1 (very centric) are included in the output.
            //2 – Social tags assigned importance levels 1 (very centric) and 2 (somewhat centric) are included in the output.
            //3 – All social tags are included in the output. (The same result as not using this header at all.)
            //dump()
            if (isset($aItem['importance']) && "2" === $aItem['importance']) {
                $aResponse[] = $aItem['name'];
            }
        }

        return $aResponse;
    }

}
