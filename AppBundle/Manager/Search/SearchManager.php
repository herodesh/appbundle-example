<?php

namespace Flendoc\AppBundle\Manager\Search;

use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Manager\Doctors\MedicalCaseManager;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use Flendoc\AppBundle\Manager\FlendocManager;
use Symfony\Bridge\Twig\TwigEngine;

/**
 * Class SearchManager
 * @package Flendoc\AppBundle\Manager\Search
 */
class SearchManager extends FlendocManager
{
    /** @var TwigEngine */
    protected $templating;

    /** @var MedicalCaseManager */
    protected $medicalCaseManager;

    /** @var IndexerManager */
    protected $indexerManager;

    /** @var array */
    protected $appDefaults;

    /**
     * SearchManager constructor.
     *
     * @param TwigEngine         $templating
     * @param MedicalCaseManager $medicalCaseManager
     * @param IndexerManager     $indexerManager
     * @param array              $appDefaults
     */
    public function __construct(
        TwigEngine $templating,
        MedicalCaseManager $medicalCaseManager,
        IndexerManager $indexerManager,
        $appDefaults
    ) {
        $this->templating         = $templating;
        $this->medicalCaseManager = $medicalCaseManager;
        $this->indexerManager     = $indexerManager;
        $this->appDefaults        = $appDefaults;
    }

    /**
     * @param Languages    $oLanguage
     * @param string       $sSearchTerm
     * @param integer      $iPage
     * @param integer      $iPageSize
     * @param integer|null $iSearchMainResults
     * @param string|null  $sSearchAfter
     * @param boolean      $bSimpleDisplay
     *
     * @return mixed
     * @throws \Twig\Error\Error
     */
    public function searchDoctors(
        Languages $oLanguage,
        $sSearchTerm,
        $iPage,
        $iPageSize = null,
        $sSearchAfter = null,
        $iSearchMainResults = null,
        $bSimpleDisplay = false
    ) {
        $aResponse        = $this->indexerManager->searchUsers(
            $sSearchTerm,
            $oLanguage->getIso(),
            $iPageSize,
            $sSearchAfter,
            $iSearchMainResults
        );
        $sNextSearchAfter = null;

        if ($aResponse) {
            $aDoctors         = $aResponse['results'];
            $sNextSearchAfter = $aResponse['searchAfter'];
        } else {
            $aDoctors = $this
                ->getRepository(Doctors::class)
                ->findSearchUsers($sSearchTerm, $oLanguage, $iPage, $iPageSize, $iSearchMainResults);
        }

        $aDoctorsResult = [];
        foreach ($aDoctors as $aDoctor) {
            $aDoctorsResult[] = $this->templating->render('@Doctors/Contacts/_partials/_single_contact_layout.html.twig', [
                'contact'       => $aDoctor,
                'simpleDisplay' => $bSimpleDisplay,
                'searchAfter'   => $sNextSearchAfter,
            ]);
        }

        return $aDoctorsResult;
    }

    /**
     * Search for medical cases
     *
     * @param Languages    $oLanguage
     * @param string       $sSearchTerm
     * @param integer      $iPage
     * @param integer|null $iPageSize
     * @param string|null  $sSearchAfter
     * @param null         $iSearchMainResults
     * @param boolean      $bSimpleDisplay
     *
     * @return array
     * @throws \Twig\Error\Error
     */
    public function searchMedicalCases(
        Languages $oLanguage,
        $sSearchTerm,
        $iPage,
        $iPageSize = null,
        $sSearchAfter = null,
        $iSearchMainResults = null,
        $bSimpleDisplay = false
    ) {
        $aResponse        = $this->indexerManager->searchMedicalCases(
            $sSearchTerm,
            $oLanguage->getIso(),
            $iPageSize,
            $sSearchAfter,
            $iSearchMainResults
        );
        $sNextSearchAfter = null;

        if ($aResponse) {
            $aMedicalCases    = $aResponse['results'];
            $sNextSearchAfter = $aResponse['searchAfter'];
        } else {
            $aMedicalCasesQueryResults = $this->getRepository(MedicalCases::class)
                                              ->findPublishedMedicalCasesBySearchKey($sSearchTerm, $iPage, $iPageSize, $iSearchMainResults);
            $aMedicalCases             = $this->medicalCaseManager->prepareMedicalCaseForQuickViews($aMedicalCasesQueryResults, $oLanguage);
        }

        $aMedicalCasesResult = [];
        foreach ($aMedicalCases as $aMedicalCase) {

            $aMedicalCasesResult[] = $this->templating->render('@Doctors/MedicalCases/partials/_single_medical_case_search_result.html.twig', [
                'medicalCase'   => $aMedicalCase,
                's3Url'         => AppStatic::getS3StorageFolder(AppConstants::AMAZON_MEDICAL_CASES_IMAGE_DIRECTORY, $this->appDefaults),
                'simpleDisplay' => $bSimpleDisplay,
                'searchAfter'   => $sNextSearchAfter,
            ]);
        }

        return $aMedicalCasesResult;
    }

    /**
     * Search for medical cases
     *
     * @param Languages    $oLanguage
     * @param string       $sSearchTerm
     * @param integer      $iPage
     * @param integer|null $iPageSize
     * @param string|null  $sSearchAfter
     * @param null         $iSearchMainResults
     * @param boolean      $bSimpleDisplay
     *
     * @return array
     * @throws \Twig\Error\Error
     */
    public function searchMedicalDictionary(
        Languages $oLanguage,
        $sSearchTerm,
        $iPage,
        $iPageSize = null,
        $sSearchAfter = null,
        $iSearchMainResults = null,
        $bSimpleDisplay = false
    ) {
        $aResponse        = $this->indexerManager->searchMedicalDictionary(
            $sSearchTerm,
            $oLanguage->getIso(),
            $iPageSize,
            $sSearchAfter,
            $iSearchMainResults
        );
        $sNextSearchAfter = null;

        //if we get some response from ES display it otherwise display from MySql
        if ($aResponse) {
            $aMedicalDictionaries = $aResponse['results'];
            $sNextSearchAfter     = $aResponse['searchAfter'];
        } else {
            $aMedicalDictionaries = $this->getRepository(MedicalDictionary::class)
                                         ->findPublishedMedicalDictionaryBySearchKey($oLanguage, $sSearchTerm, $iPage, $iPageSize, $iSearchMainResults);
        }

        $aMedicalDictionariesResult = [];
        foreach ($aMedicalDictionaries as $aMedicalDictionaryEntry) {
            $aMedicalDictionariesResult[] = $this->templating->render('@Doctors/MedicalDictionary/_partials/_single_medical_dictionary_search_result.html.twig', [
                'medicalDictionaryEntry' => $aMedicalDictionaryEntry,
                's3Url'                  => AppStatic::getS3StorageFolder(
                    AppConstants::AMAZON_MEDICAL_DICTIONARY_IMAGE_DIRECTORY,
                    $this->appDefaults
                ),
                'searchAfter'            => $sNextSearchAfter,
                'simpleDisplay'          => true,
            ]);
        }

        return $aMedicalDictionariesResult;
    }
}
