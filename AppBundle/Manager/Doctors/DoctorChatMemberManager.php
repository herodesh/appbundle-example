<?php

namespace Flendoc\AppBundle\Manager\Doctors;

use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\Chats\DoctorChatMember;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class DoctorChatMemberManager
 * @package Flendoc\AppBundle\Manager\Doctors
 */
class DoctorChatMemberManager extends FlendocManager
{
    /**
    * Update the time when the user was last active on a chat
    *
    * @param string  $sChatIdentifier
    * @param integer $userId
    */
    public function updateMemberLastActivity($sChatIdentifier, $userId): void
    {
        /** @var DoctorChat $oDoctorChat */
        $oDoctorChat = $this
            ->getRepository(DoctorChat::class)
            ->findOneBy([
                'identifier' => $sChatIdentifier,
            ])
        ;
        /** @var DoctorChatMember $oDoctorChatMember */
        $oDoctorChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctor'     => $userId,
                'doctorChat' => $oDoctorChat,
            ])
        ;

        $oDoctorChatMember->setLastActiveAt(new \DateTime());
        $oDoctorChatMember->setEmailNotificationSent(false);
        $this->saveObject($oDoctorChatMember);
    }
}
