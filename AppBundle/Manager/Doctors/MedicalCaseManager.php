<?php

namespace Flendoc\AppBundle\Manager\Doctors;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\HashTags\HashTags;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCaseImages;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases;
use Flendoc\AppBundle\Constants\FeedConstants;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Manager\Comments\CommentsManager;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use Flendoc\AppBundle\Manager\Feeds\FeedsManager;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\App\ObjectKeyEntityTrait;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Uploader\PhotoUploader;

/**
 * Class MedicalCaseManager
 * @package Flendoc\AppBundle\Manager\Doctors
 */
class MedicalCaseManager extends FlendocManager
{
    use ObjectKeyEntityTrait;

    /** @var FeedsManager */
    protected $feedsManager;

    /** @var PhotoUploader */
    protected $photoUploader;

    /** @var CommentsManager */
    protected $commentsManager;

    /** @var IndexerManager */
    protected $indexerManager;

    /**
     * MedicalCaseManager constructor.
     *
     * @param FeedsManager    $feedsManager
     * @param PhotoUploader   $photoUploader
     * @param CommentsManager $commentsManager
     * @param IndexerManager  $indexerManager
     */
    public function __construct(
        FeedsManager $feedsManager,
        PhotoUploader $photoUploader,
        CommentsManager $commentsManager,
        IndexerManager $indexerManager
    ) {
        $this->feedsManager    = $feedsManager;
        $this->photoUploader   = $photoUploader;
        $this->commentsManager = $commentsManager;
        $this->indexerManager  = $indexerManager;
    }

    /**
     * @param array     $aMedicalCases
     * @param Languages $oUserLanguages
     *
     * @return array
     */
    public function prepareMedicalCaseForQuickViews(array $aMedicalCases, Languages $oUserLanguages)
    {
        $aMedicalCasesResult = [];
        foreach ($aMedicalCases as $oMedicalCase) {

            //anonymous function to get skills names
            $aMedicalCaseSkillsResult = function ($oMedicalCase) use ($oUserLanguages) {
                $aMedicalCaseSkills    = $oMedicalCase->getMedicalCaseSkills();
                $aMedicalCaseSkillName = [];
                foreach ($aMedicalCaseSkills as $oMedicalCaseSkill) {
                    $aMedicalCaseSkillName[] = [
                        'identifier' => $oMedicalCaseSkill->getIdentifier(),
                        'name'       => $oMedicalCaseSkill->getDisplayName($oUserLanguages),
                    ];
                }

                return $aMedicalCaseSkillName;
            };

            $aMedicalCasesResult[] = [
                'identifier'             => $oMedicalCase->getIdentifier(),
                'title'                  => $oMedicalCase->getTitle(),
                'description'            => $oMedicalCase->getDescription(),
                'image'                  => ($oMedicalCase->getMedicalCaseImages()->first()) ?
                    $oMedicalCase->getMedicalCaseImages()->first()->getPhotos() : null,
                'skills'                 => $aMedicalCaseSkillsResult($oMedicalCase),
                'slugTitle'              => $oMedicalCase->getSlugTitle(),
                'medicalCaseLanguageIso' => $oMedicalCase->getMedicalCaseLanguageIso(),
            ];
        }

        return $aMedicalCasesResult;
    }

    /**
     * Permanently delete medical cases and everything related to them
     *
     * @param PendingMedicalCases $oPendingMedicalCase
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function permanentlyDeleteMedicalCase(PendingMedicalCases $oPendingMedicalCase)
    {
        $sS3MedicalCasesFolder = AppConstants::AMAZON_MEDICAL_CASES_IMAGE_DIRECTORY;
        //get Medical cases images list in order to remove them
        $aMedicalCasesImages = [];
        foreach ($oPendingMedicalCase->getMedicalCaseImages() as $oPendingMedicalCaseImage) {
            foreach (json_decode($oPendingMedicalCaseImage->getPhotos(), true)['files'] as $sFile) {
                $aMedicalCasesImages[] = $sFile;
            }
        }

        //remove Medical Case
        if (null != $oPendingMedicalCase->getMedicalCase()) {
            $oMedicalCase = $oPendingMedicalCase->getMedicalCase();

            //delete medical case feed
            $oMedicalCaseMongoFeed = $this->getMongoRepository(MongoFeed::class)->findOneBy([
                'contentIdentifier' => $oMedicalCase->getIdentifier(),
                'feedType'          => ObjectTypeConstants::OBJECT_MEDICAL_CASE,
            ]);
            $this->odm->remove($oMedicalCaseMongoFeed);

            //delete medical case from ES
            $this->indexerManager->deleteMedicalCaseDocument($oMedicalCase);
            $this->em->remove($oMedicalCase);
        }

        //remove Pending Medical Case
        $this->em->remove($oPendingMedicalCase);

        //remove images from Aws
        foreach ($aMedicalCasesImages as $sMedicalCaseImage) {
            $this->photoUploader->delete(sprintf('%s/%s', $sS3MedicalCasesFolder, $sMedicalCaseImage));
        }

        $this->em->flush();

    }

    /**
     * @param Doctors $oDoctor
     *
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createEmptyMedicalCase(Doctors $oDoctor)
    {
        $oPendingMedicalCase = new PendingMedicalCases();

        $oPendingMedicalCase->setDoctor($oDoctor);
        $oPendingMedicalCase->setTitle('');
        $oPendingMedicalCase->setSlugTitle('');
        $oPendingMedicalCase->setDescription('');

        $this->em->persist($oPendingMedicalCase);

        $this->em->flush();

        return $oPendingMedicalCase->getIdentifier();
    }

    /**
     * Updates statuses of Medical cases and Published Medical cases to be reeditable again
     *
     * @param MedicalCases $oMedicalCase
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function reenableMedicalCaseEditMode(MedicalCases $oMedicalCase)
    {
        //set medical case properties
        $oMedicalCase->setIsAdminRevised(false);
        $oMedicalCase->setIsApproved(false);

        //remove medical case has tags
        foreach ($oMedicalCase->getHashTag() as $oHashTag) {
            $oMedicalCase->removeHashTag($oHashTag);
        }

        //set pending medical case properties
        $oPendingMedicalCase = $oMedicalCase->getPendingMedicalCase();

        $oPendingMedicalCase->setIsApproved(false);
        $oPendingMedicalCase->setIsPublished(false);

        $this->em->flush();
    }

    /**
     * @param PendingMedicalCases $oPendingMedicalCase
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function savePendingMedicalCaseToMedicalCase(PendingMedicalCases $oPendingMedicalCase)
    {
        //if was already published do not add the node to Neo4J
        $bFeed = false;
        if (null != $oPendingMedicalCase->getMedicalCase()) {
            $oMedicalCase = $oPendingMedicalCase->getMedicalCase();
        } else {
            $bFeed = true;
            //create new Medical Case
            $oMedicalCase = $this->createObject(MedicalCases::class);
        }

        //update MedicalCase
        $oMedicalCase->setTitle($oPendingMedicalCase->getTitle());
        $oMedicalCase->setSlugTitle($oPendingMedicalCase->getSlugTitle());
        $oMedicalCase->setDescription($oPendingMedicalCase->getDescription());
        $oMedicalCase->setInvestigationResults($oPendingMedicalCase->getInvestigationResults());
        $oMedicalCase->setSources($oPendingMedicalCase->getSources());
        $oMedicalCase->setDoctor($oPendingMedicalCase->getDoctor());
        $oMedicalCase->setMedicalCaseSkills($oPendingMedicalCase->getMedicalCaseSkills());
        $oMedicalCase->setIsApproved(true);
        $oMedicalCase->setIsPending(false);
        $oMedicalCase->setIsAdminRevised(true);
        $oMedicalCase->setMedicalCaseLanguageIso($oPendingMedicalCase->getMedicalCaseLanguageIso());

        /**
         * Update images
         * @var MedicalCaseImages $oImage
         */
        foreach ($oPendingMedicalCase->getMedicalCaseImages() as $oImage) {
            $oMedicalCase->addMedicalCaseImage($oImage);
            $oImage->setMedicalCase($oMedicalCase);
        }

        $oPendingMedicalCase->setMedicalCases($oMedicalCase);
        $oPendingMedicalCase->setIsPublished(true);
        $oPendingMedicalCase->setIsApproved(true);

        $this->em->persist($oMedicalCase);

        //generate medical case keywords
        $this->generateMedicalCaseHashTags($oMedicalCase, false);

        if ($bFeed) {
            $this->createNewMedicalCaseFeedEntry($oMedicalCase, $oMedicalCase->getDoctor());
        }

        $this->em->flush();
    }

    /**
     * Create new feed entry for Medical Cases
     *
     * @param              $oFormData
     * @param MedicalCases $oMedicalCase
     * @param Doctors      $oSharingUser
     */
    public function createNewMedicalCaseFeedEntry(MedicalCases $oMedicalCase, Doctors $oSharingUser, $oFormData = null)
    {
        /**
         * Create a new feed entry in mongo
         *
         * @var MongoFeed $oMongoMedicalCase
         */
        $oMongoMedicalCase = $this->createObject(MongoFeed::class);

        $oMongoMedicalCase->setCreatorUserIdentifier($oSharingUser->getIdentifier());
        $oMongoMedicalCase->setObjectDataType(ObjectTypeConstants::OBJECT_DATA_TYPE_MYSQL);
        $oMongoMedicalCase->setObjectEntityType(ObjectTypeConstants::OBJECT_MEDICAL_CASE);
        $oMongoMedicalCase->setContentIdentifier($oMedicalCase->getIdentifier());
        $oMongoMedicalCase->setFeedEntityType(ObjectTypeConstants::OBJECT_MONGO_FEED);
        $oMongoMedicalCase->setFeedType(ObjectTypeConstants::OBJECT_MEDICAL_CASE);
        $oMongoMedicalCase->setIsEditable(false); //this is not editable on the feed.

        //in case the user added some text with his share
        if (null != $oFormData) {
            $oMongoMedicalCase->setContent($oFormData->getContent());
            $oMongoMedicalCase->setIsShared(true);
        }

        $this->saveMongoObject($oMongoMedicalCase);

        /*
         * Create a MongoThread related to this MongoFeed item.
         * If MongoThread was already created do not make another one.
         */
        $this->commentsManager->addMongoThread(
            self::$aEntityToKey[MongoFeed::class],
            $oMongoMedicalCase->getIdentifier(),
            $oSharingUser->getIdentifier(),
            $oMongoMedicalCase->getIsShared() ? true : false
        );

        /*
         * Create a MongoThread for the medical case itself.
         * If MongoThread was already created do not make another one.
         */
        $this->commentsManager->addMongoThread(
            self::$aEntityToKey[MedicalCases::class],
            $oMedicalCase->getIdentifier(),
            $oSharingUser->getIdentifier()
        );

        //Save new FeedItem to Neo4J on publish
        $this->feedsManager->addNeo4JFeedItemNode(
            $oSharingUser->getIdentifier(),
            ObjectTypeConstants::OBJECT_MEDICAL_CASE,
            //we get the mongo entry identifier and not the real object identifier because we care about feeds here
            $oMongoMedicalCase->getIdentifier(),
            ObjectTypeConstants::OBJECT_DATA_TYPE_MYSQL,
            $oMongoMedicalCase->getIdentifier(),
            (null != $oFormData) ? $oFormData->getPrivacy() : FeedConstants::FEED_PROPERTY_PUBLIC,
            false
        );
    }

    /**
     * Generates Keywords for a given medical case entry
     *
     * @param MedicalCases $oMedicalCase
     * @param bool         $bFlush
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateMedicalCaseHashTags(MedicalCases $oMedicalCase, $bFlush = true)
    {
        //do not overwrite existing hashtags
        if ($oMedicalCase->getHasHashTag()) {
            return;
        }

        $sText = $oMedicalCase->getDescription().' '.
            $oMedicalCase->getInvestigationResults();

        if (empty(trim($sText))) {
            return;
        }

        $sText = json_encode(strip_tags($sText));

        $aResponse = $this->extractKeywordsFromText($sText);

        foreach ($aResponse as $item) {
            $oHashTag = new HashTags();
            $oHashTag->setTag($item);
            $oHashTag->setMedicalCase($oMedicalCase);
            $oMedicalCase->addHashTag($oHashTag);

            $this->em->persist($oHashTag);
        }

        $oMedicalCase->setHasHashTag(true);

        $this->em->persist($oMedicalCase);

        if ($bFlush) {
            $this->em->flush();
        }

    }
}
