<?php

namespace Flendoc\AppBundle\Manager\Doctors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use Flendoc\AppBundle\Adapter\SwiftMailer\SwiftMailerAdapter;
use Flendoc\AppBundle\Entity\Doctors\DoctorExistingEmailAddresses;
use Flendoc\AppBundle\Entity\Doctors\DoctorProfile;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\Contacts\ContactRequestDefaultStatusesTrait;
use Monolog\Logger;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class DoctorManager
 * @package Flendoc\AppBundle\Manager\Doctors
 */
class DoctorManager extends FlendocManager
{
    use ContactRequestDefaultStatusesTrait;

    /** @var PasswordEncoderInterface */
    protected $passwordEncoder;

    /** @var EntityManager */
    protected $em;

    /** @var Neo4JManager $oNeo4JManager */
    protected $neo4JManager;

    /** @var $logger Logger */
    protected $logger;

    /** @var SwiftMailerAdapter */
    protected $mailer;

    /**
     * DoctorManager constructor.
     *
     * @param UserPasswordEncoder $passwordEncoder
     * @param EntityManager       $em
     * @param Neo4JManager        $oNeo4JManager
     * @param Logger              $logger
     * @param SwiftMailerAdapter  $mailer
     */
    public function __construct(
        UserPasswordEncoder $passwordEncoder,
        EntityManager $em,
        Neo4JManager $oNeo4JManager,
        Logger $logger,
        SwiftMailerAdapter $mailer
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->em              = $em;
        $this->neo4JManager    = $oNeo4JManager;
        $this->logger          = $logger;
        $this->mailer          = $mailer;
    }

    /**
     * Process registration data, creates user profile
     * and new entry for DoctorsExistingEmailAddresses
     *
     * @param UserInterface $oDoctor
     *
     * @return UserInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public function processRegistrationData(UserInterface $oDoctor)
    {
        $oDoctorProfile              = new DoctorProfile();
        $oDoctorExistingEmailAddress = new DoctorExistingEmailAddresses();
        $oDoctorSpokenLanguage       = new ArrayCollection();
        $oLanguage                   = $this->getRepository(Languages::class)
                                            ->findOneByIso($oDoctor->getDefaultLanguage());

        $oDoctorProfile->setAcademicTitle($oDoctor->getAcademicTitle());
        $oDoctorProfile->setFirstName($oDoctor->getFirstName());
        $oDoctorProfile->setLastName($oDoctor->getLastName());
        $oDoctorProfile->setGender($oDoctor->getGender());
        $oDoctorProfile->setDoctor($oDoctor);
        $oDoctorProfile->setCountry($oDoctor->getCountry());
        $oDoctorProfile->setCity($oDoctor->getCity());

        $oDoctorSpokenLanguage->add($oLanguage);
        $oDoctorProfile->setDoctorSpokenLanguages($oDoctorSpokenLanguage);

        $oDoctor->setDoctorProfile($oDoctorProfile);
        $sPassword = $this->passwordEncoder->encodePassword($oDoctor, $oDoctor->getPlainPassword());
        $oDoctor->setPassword($sPassword);
        $oDoctor->setConfirmationToken($oDoctor->generateNewToken());
        //this is a hack create a transformer
        $oDoctor->setDefaultLanguage($oDoctor->getDefaultLanguage());
        $oDoctor->addRole($this->getEntityManager()
                               ->getRepository('AppBundle:Users\UserRoles')
                               ->findOneByName('ROLE_DOCTOR_NOT_VERIFIED'));

        //Add new entry for DoctorsExistingEmailAddresses
        $oDoctorExistingEmailAddress->setDoctor($oDoctor);
        $oDoctorExistingEmailAddress->setEmail($oDoctor->getUsername());
        $oDoctor->addExistingEmailAddress($oDoctorExistingEmailAddress);


        return $oDoctor;
    }

    /**
     * Creates a new Doctor Node to Neo4J graph database
     *
     * @param string $identifier
     * @param string $firstName
     * @param string $lastName
     * @param string $status
     *
     * @return bool
     */
    public function createNeo4JNode($identifier, $firstName, $lastName, $status)
    {
        try {
            $this
                ->neo4JManager
                ->client
                ->run('
                    CREATE (d:Doctor {identifier:"'.$identifier.'", firstName:"'.$firstName.'", lastName:"'.$lastName.'", status:"'.$status.'"})
                ');

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Update status field for and existing Doctor node
     *
     * @param string $identifier
     * @param string $status
     *
     * @return bool
     */
    public function matchAndMergeNodeStatus($identifier, $status)
    {
        try {
            $this
                ->neo4JManager
                ->client
                ->run('
                    MATCH (doctor:Doctor {identifier: {identifier}})
                    SET doctor.status = {status}
                    ',
                    [
                        'identifier' => $identifier,
                        'status'     => $status,
                    ]
                );

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Update an existing Doctor node with hasSkill, livesInCountry, livesInCity relationships
     *
     * @param string      $identifier Doctor Identifier
     * @param array|mixed $formData   DoctorProfile data
     *
     * @return bool
     */
    public function matchAndMergeNodeRelationships($identifier, $formData)
    {
        $sCountryIdentifier = $formData->getCountry() ? $formData->getCountry()->getIdentifier() : null;
        $sCityIdentifier    = $formData->getCity() ? $formData->getCity()->getIdentifier() : null;
        $aDoctorSkills      = method_exists($formData, 'getDoctorSkills') ? $formData->getDoctorSkills() : null;

        $aDoctorSkillsIdentifiers = [];

        if (isset($aDoctorSkills)) {
            /** @var Skills $oDoctorSkill */
            foreach ($aDoctorSkills as $oDoctorSkill) {
                $aDoctorSkillsIdentifiers[] = $oDoctorSkill->getIdentifier();
            }
        }

        $stack = $this->neo4JManager->client->stack();

        if (isset($sCountryIdentifier)) {
            //Remove old liveInCountry relationship
            $stack->push('
                MATCH (n)-[relationship:livesInCountry]-(doctor:Doctor {identifier: {dIdentifier}})
                DELETE relationship
            ', [
                'dIdentifier' => $identifier,
            ]);

            //Add new liveInCountry relationship
            $stack->push('
                MATCH (doctor:Doctor {identifier: {dIdentifier}})
                MATCH (country:Country {identifier: {cIdentifier}})
                MERGE (doctor)-[:livesInCountry]-(country)
            ', [
                'dIdentifier' => $identifier,
                'cIdentifier' => $sCountryIdentifier,
            ]);
        }

        if (isset($sCityIdentifier)) {
            //Remove old livesInCity relationship
            $stack->push('
                MATCH (n)-[relationship:livesInCity]-(doctor:Doctor {identifier: {dIdentifier}})
                DELETE relationship
            ', [
                'dIdentifier' => $identifier,
            ]);

            //Add new livesInCity relationship
            $stack->push('
                MATCH (doctor:Doctor {identifier: {dIdentifier}})
                MATCH (city:City {identifier: {cIdentifier}})
                MERGE (doctor)-[:livesInCity]-(city)
            ', [
                'dIdentifier' => $identifier,
                'cIdentifier' => $sCityIdentifier,
            ]);
        }

        //Remove old hasSkill relationships
        $stack->push('
                MATCH (n)-[relationships:hasSkill]-(doctor:Doctor {identifier: {dIdentifier}})
                DELETE relationships
            ', [
            'dIdentifier' => $identifier,
        ]);

        if (!empty($aDoctorSkillsIdentifiers)) {
            //Add new hasSkill relationships
            foreach ($aDoctorSkillsIdentifiers as $sDoctorSkillsIdentifier) {
                $stack->push('
                    MATCH (doctor:Doctor {identifier: {dIdentifier}})
                    MATCH (skill:Skills {identifier: {sIdentifier}})
                    MERGE (doctor)-[:hasSkill]-(skill)
                ', [
                    'dIdentifier' => $identifier,
                    'sIdentifier' => $sDoctorSkillsIdentifier,
                ]);
            }
        }

        try {
            $this->neo4JManager->client->runStack($stack);

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Find all contacts that have friendWith relationship with the current doctor.
     *
     * @param string $sUserIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchContacts($sUserIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(n)-[:isFriendWith]-(:Doctor {identifier:{sUserIdentifier}})
                        RETURN DISTINCT n.identifier as identifier
                    ', [
                        'sUserIdentifier' => $sUserIdentifier,
                    ]
                );

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Find all contacts that have isFriendWith relationship with the current doctor.
     *
     * @param string $identifier
     * @param string $status
     * @param bool   $author
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchContactsByIsFriendWithStatus($identifier, $status, $author = false)
    {
        if ($status === self::$statusPending && $author) {
            $query = 'MATCH(n)<-[:isFriendWith {status:"'.$status.'"}]-(:Doctor {identifier:"'.$identifier.'"}) RETURN DISTINCT n.identifier as identifier';
        } elseif ($status === self::$statusPending && !$author) {
            $query = 'MATCH(n)-[:isFriendWith {status:"'.$status.'"}]->(:Doctor {identifier:"'.$identifier.'"}) RETURN DISTINCT n.identifier as identifier';
        } else {
            $query = 'MATCH(n)-[:isFriendWith {status:"'.$status.'"}]-(:Doctor {identifier:"'.$identifier.'"}) RETURN DISTINCT n.identifier as identifier';
        }

        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run($query);

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    public function countContactsByFriendWithStatus($sUserIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(n)-[r:isFriendWith {status:{sStatusAccepted}}]-(:Doctor {identifier:{sUserIdentifier}})
                        RETURN {sStatusAccepted} as status, count(*) as contacts
                        UNION
                        MATCH(n)<-[r:isFriendWith {status:{sStatusPending}}]-(:Doctor {identifier:{sUserIdentifier}})
                        RETURN {sStatusSent} as status, count(*) as contacts
                        UNION
                        MATCH(n)-[r:isFriendWith {status:{sStatusPending}}]->(:Doctor {identifier:{sUserIdentifier}})
                        RETURN {sStatusPending} as status, count(*) as contacts
                        UNION
                        MATCH(n)-[r:isFriendWith {status:{sStatusBlocked}}]-(:Doctor {identifier:{sUserIdentifier}})
                        RETURN {sStatusBlocked} as status, count(*) as contacts
                    ',
                    [
                        'sStatusSent'     => self::$statusSent,
                        'sStatusPending'  => self::$statusPending,
                        'sStatusAccepted' => self::$statusAccepted,
                        'sStatusBlocked'  => self::$statusBlocked,
                        'sUserIdentifier' => $sUserIdentifier,
                    ]
                );

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Find contacts suggestions for a specified Doctor.
     * Contacts are searched through 2 and 3 (:Doctors)-[isFriendWith] hops in Neo4J data set.
     *
     * @param string $sUserIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchFriendsOfFriendsSuggestions($sUserIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(d:Doctor{identifier: {sUserIdentifier}})-[:isFriendWith*2..2 {status:"accepted"}]-(doctor:Doctor)
                        WHERE doctor.identifier <> {sUserIdentifier}
                        AND doctor.status = "active"
                        AND NOT (d)-[:isFriendWith]-(doctor)
                        RETURN doctor.identifier as identifier
                        ORDER BY doctor.identifier DESC LIMIT 1000
                        UNION
                        MATCH(d:Doctor{identifier: {sUserIdentifier}})-[:isFriendWith*3..3 {status:"accepted"}]-(doctor:Doctor)
                        WHERE doctor.identifier <> {sUserIdentifier}
                        AND doctor.status = "active"
                        AND NOT (d)-[:isFriendWith]-(doctor)
                        RETURN doctor.identifier as identifier
                        ORDER BY doctor.identifier DESC LIMIT 1000
                    ',
                    [
                        'sUserIdentifier' => $sUserIdentifier,
                    ]
                );

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * @param string $sCurrentUserIdentifier
     * @param string $sIdentifier
     * @param string $sRelationship
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchNumberOfContactsByRelationship($sCurrentUserIdentifier, $sIdentifier, $sRelationship)
    {
        $query = 'MATCH (doctor:Doctor {identifier:"'.$sCurrentUserIdentifier.'"}) ';

        if ($sRelationship === 'hasSkill') {
            $query .= 'MATCH (n:Doctor)-[:hasSkill]-(skill:Skills {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN COUNT(n) AS num';
        } elseif ($sRelationship === 'livesInCity') {
            $query .= 'MATCH (n:Doctor)-[:livesInCity]-(city:City {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN COUNT(n) AS num';
        } else {
            $query .= 'MATCH (n:Doctor)-[:livesInCountry]-(country:Country {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN COUNT(n) AS num';
        }

        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run($query);

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * @param string $sCurrentUserIdentifier
     * @param string $sIdentifier
     * @param string $sRelationship
     * @param int    $iPosition
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchContactByRelationshipAndPosition(
        $sCurrentUserIdentifier,
        $sIdentifier,
        $sRelationship,
        $iPosition
    ) {
        $query = 'MATCH (doctor:Doctor {identifier:"'.$sCurrentUserIdentifier.'"}) ';

        if ($sRelationship === 'hasSkill') {
            $query .= 'MATCH (n:Doctor)-[:hasSkill]-(skill:Skills {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN n.identifier as identifier ORDER BY n.identifier SKIP '.$iPosition.' LIMIT 1';
        } elseif ($sRelationship === 'livesInCity') {
            $query .= 'MATCH (n:Doctor)-[:livesInCity]-(city:City {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN n.identifier as identifier ORDER BY n.identifier SKIP '.$iPosition.' LIMIT 1';
        } else {
            $query .= 'MATCH (n:Doctor)-[:livesInCountry]-(country:Country {identifier:"'.$sIdentifier.'"}) WHERE n.identifier <> doctor.identifier AND n.status = "active" AND NOT (n)-[:isFriendWith]-(doctor) RETURN n.identifier as identifier ORDER BY n.identifier SKIP '.$iPosition.' LIMIT 1';
        }

        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run($query);

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Create the isFriendWith relationship between two doctors.
     *
     * @param string $sIdentifier
     * @param string $sReceiverIdentifier
     * @param string $sAuthorIdentifier
     * @param string $sStatus
     *
     * @return bool
     */
    public function matchAndMergeContactRequest($sIdentifier, $sReceiverIdentifier, $sAuthorIdentifier, $sStatus): bool
    {
        try {
            $this
                ->neo4JManager
                ->client
                ->run(
                    '
                        MATCH (receiver:Doctor {identifier:{sReceiverIdentifier}}), (author:Doctor {identifier:{sAuthorIdentifier}})
                        MERGE (author)-[:isFriendWith {status:{sStatus}, identifier:{sIdentifier}}]->(receiver)
                    ',
                    [
                        'sIdentifier'         => $sIdentifier,
                        'sReceiverIdentifier' => $sReceiverIdentifier,
                        'sAuthorIdentifier'   => $sAuthorIdentifier,
                        'sStatus'             => $sStatus,
                    ]
                );

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Update the isFriendWith relationship between two doctors.
     *
     * @param string $receiverIdentifier
     * @param string $authorIdentifier
     * @param string $status
     *
     * @return bool
     */
    public function mergeContactRequest($receiverIdentifier, $authorIdentifier, $status): bool
    {
        try {
            $this
                ->neo4JManager
                ->client
                ->run(
                    '
                        MERGE (receiver:Doctor {identifier:"'.$receiverIdentifier.'"})-[f:isFriendWith]-(author:Doctor {identifier:"'.$authorIdentifier.'"})
                        SET f.status = "'.$status.'"
                    '
                );

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Find isFriendWith relationship status from Neo4J.
     *
     * @param string $receiverIdentifier
     * @param string $authorIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchContactRequestStatus($receiverIdentifier, $authorIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(:Doctor {identifier:"'.$receiverIdentifier.'"})-[friendship:isFriendWith]-(:Doctor {identifier:"'.$authorIdentifier.'"})
                        RETURN friendship.status as status
                    '
                );

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Find isFriendWith relationship status from Neo4J.
     *
     * @param string $sReceiverIdentifier
     * @param string $sAuthorIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function matchContactRequestIdentifier($sReceiverIdentifier, $sAuthorIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(:Doctor {identifier:{sReceiverIdentifier}})-[friendship:isFriendWith]-(:Doctor {identifier:{sAuthorIdentifier}})
                        RETURN friendship.identifier as identifier
                    ',
                    [
                        'sReceiverIdentifier' => $sReceiverIdentifier,
                        'sAuthorIdentifier'   => $sAuthorIdentifier,
                    ]
                );

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Remove a isFriendWith relationship between two users.
     *
     * @param string $receiverIdentifier
     * @param string $authorIdentifier
     * @param string $status
     *
     * @return bool
     */
    public function removeContactRequest($receiverIdentifier, $authorIdentifier, $status): bool
    {
        try {
            $this
                ->neo4JManager
                ->client
                ->run('
                        MATCH(:Doctor {identifier:"'.$receiverIdentifier.'"})-[friendship:isFriendWith]-(:Doctor {identifier:"'.$authorIdentifier.'"})
                        DELETE friendship
                    '
                );

            return true;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }
}
