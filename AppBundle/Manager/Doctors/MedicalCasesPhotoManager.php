<?php

namespace Flendoc\AppBundle\Manager\Doctors;

use Flendoc\AppBundle\Entity\MedicalCases\MedicalCaseImages;
use Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Repository\MedicalCases\PendingMedicalCaseRepository;
use Flendoc\AppBundle\Uploader\PhotoUploader;

/**
 * Class MedicalCasesPhotoManager
 * @package Flendoc\AppBundle\Manager\FileUploaders
 */
class MedicalCasesPhotoManager extends FlendocManager
{

    /** @var PhotoUploader */
    protected $photoUploader;

    /**
     * MedicalCasesPhotoManager constructor.
     *
     * @param PhotoUploader $photoUploader
     */
    public function __construct(PhotoUploader $photoUploader)
    {
        $this->photoUploader = $photoUploader;
    }

    /**
     * @param array  $aUploadedFile
     * @param string $sPendingMedicalCaseIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveMedicalCaseImages($aUploadedFile, $sPendingMedicalCaseIdentifier)
    {
        /** @var MedicalCaseImages $oDoctorCaseImage */
        $oDoctorCaseImage = $this->createObject(MedicalCaseImages::class);

        $sPhotos = json_encode($aUploadedFile);

        $oDoctorCaseImage->setPhotos($sPhotos);
        $oDoctorCaseImage->setPendingMedicalCase($this->getRepository(PendingMedicalCases::class)->findOneBy([
            'identifier' => $sPendingMedicalCaseIdentifier
        ]));

        $this->saveObject($oDoctorCaseImage);

        return $oDoctorCaseImage->getIdentifier();
    }

    /**
     * @param string $sIdentifier
     * @param string $sAwsFolder
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteMedicalCaseImagesFromDatabaseAndAws($sIdentifier, $sAwsFolder)
    {
        $oDoctorCaseImage = $this
            ->getRepository(MedicalCaseImages::class)
            ->findOneBy(['identifier' => $sIdentifier]);

        if ($oDoctorCaseImage) {
            $aPhotos = json_decode($oDoctorCaseImage->getPhotos(), true);
            $this->deleteObject($oDoctorCaseImage);

            //delete from amazonaws
            foreach ($aPhotos['files'] as $photo) {
                $this->photoUploader->delete(sprintf('%s/%s', $sAwsFolder, $photo));
            }

            return true;
        }

        return false;
    }
}
