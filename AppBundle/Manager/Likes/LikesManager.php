<?php

namespace Flendoc\AppBundle\Manager\Likes;

use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Comments\MongoVote;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class CommentsManager
 * @package Flendoc\AppBundle\Manager\Likes
 */
class LikesManager extends FlendocManager
{
    const ACTION_TYPE_LIKE = 'like';
    const ACTION_TYPE_DISLIKE = 'dislike';

    /**
     * @param MongoThread $oMongoThread
     * @param string      $sUserIdentifier
     *
     * @return string
     */
    public function manageThreadLikes($oMongoThread, $sUserIdentifier)
    {
        /** @var  $oMongoVoteRepository */
        $oMongoVoteRepository = $this->odm->getRepository(MongoVote::class);

        $oMongoVote = $oMongoVoteRepository
            ->findOneBy([
                'thread' => $oMongoThread->getId(),
                'userIdentifier' => $sUserIdentifier
            ])
        ;

        if (!$oMongoVote) {
            /** @var MongoVote $oMongoVote */
            $oMongoVote = $this->createObject(MongoVote::class);
            $oMongoVote->setThread($oMongoThread);
            $oMongoVote->setUserIdentifier($sUserIdentifier);
            $oMongoThread->addVoteToTotalVotesNumber();
            $sAction = self::ACTION_TYPE_LIKE;

            $this->odm->persist($oMongoVote);
            $this->odm->persist($oMongoThread);
            $this->odm->flush();
        } else {
            $oMongoThread->removeVoteFromTotalVotesNumber();
            $sAction = self::ACTION_TYPE_DISLIKE;

            $this->odm->remove($oMongoVote);
            $this->odm->persist($oMongoThread);
            $this->odm->flush();
        }

        return $sAction;
    }

    /**
     * @param MongoComment $oMongoComment
     * @param string       $sUserIdentifier
     *
     * @return string
     */
    public function manageCommentLikes($oMongoComment, $sUserIdentifier)
    {
        /** @var  $oMongoVoteRepository */
        $oMongoVoteRepository = $this->odm->getRepository(MongoVote::class);

        $oMongoVote = $oMongoVoteRepository
            ->findOneBy([
                'comment' => $oMongoComment->getId(),
                'userIdentifier' => $sUserIdentifier
            ])
        ;

        if (!$oMongoVote) {
            /** @var MongoVote $oMongoVote */
            $oMongoVote = $this->createObject(MongoVote::class);
            $oMongoVote->setComment($oMongoComment);
            $oMongoVote->setUserIdentifier($sUserIdentifier);
            $oMongoComment->addVoteToTotalVotesNumber();
            $sAction = self::ACTION_TYPE_LIKE;

            $this->odm->persist($oMongoVote);
            $this->odm->persist($oMongoComment);
            $this->odm->flush();
        } else {
            $oMongoComment->removeVoteFromTotalVotesNumber();
            $sAction = self::ACTION_TYPE_DISLIKE;

            $this->odm->remove($oMongoVote);
            $this->odm->persist($oMongoComment);
            $this->odm->flush();
        }

        return $sAction;
    }
}
