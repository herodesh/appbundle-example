<?php

namespace Flendoc\AppBundle\Manager;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Adapter\Cache\CacheManager;
use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class AbstractFlendocManager
 * @package Flendoc\AppBundle\Manager
 */
abstract class AbstractFlendocManager
{
    /**
     * @var
     */
    protected $class;

    /** @var EntityManager */
    protected $em;

    /** @var DocumentManager $mongoDbManager */
    protected $odm;

    /** @var Translator */
    protected $translator;

    /** @var CacheManager */
    protected $cacheManager;

    /** @var Neo4JManager */
    protected $neo4JManager;

    /** @var TokenStorage */
    protected $tokeStorage;

    /** @var Client */
    protected $guzzleClient;

    /** @var string */
    protected $googleRecaptchaSecretKey;

    /** @var string */
    protected $reutersCalaisApiKey;

    /**
     * @param Neo4JManager $neo4JManager
     */
    public function setNeo4JManager(Neo4JManager $neo4JManager)
    {
        $this->neo4JManager = $neo4JManager;
    }

    /**
     * @param DocumentManager $odm
     */
    public function setMongoDbManager(DocumentManager $odm): void
    {
        $this->odm = $odm;
    }

    /**
     * @return DocumentManager
     */
    public function getMongoDbManager()
    {
        return $this->odm;
    }

    /**
     * @param CacheManager $cacheManager
     */
    public function setCacheManager(CacheManager $cacheManager): void
    {
        $this->cacheManager = $cacheManager;
    }

    /**
     * @param EntityManager $em
     */
    public function setEntityManager(EntityManager $em): void
    {
        $this->em = $em;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Get any ORM repository
     *
     * @param $sRepository
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($sRepository)
    {
        return $this->em->getRepository($sRepository);
    }

    /**
     * Get any ODM repository
     *
     * @param $sRepository
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getMongoRepository($sRepository)
    {
        return $this->odm->getRepository($sRepository);
    }

    /**
     * @param Translator $translator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param TokenStorage $tokenStorage
     */
    public function setTokenStorage(TokenStorage $tokenStorage): void
    {
        $this->tokeStorage = $tokenStorage;
    }

    /**
     * @return TokenStorage
     */
    public function getTokenStorage()
    {
        return $this->tokeStorage;
    }

    /**
     * @return Client
     */
    public function getGuzzleClient(): Client
    {
        return $this->guzzleClient;
    }

    /**
     * @param Client $guzzleClient
     */
    public function setGuzzleClient(Client $guzzleClient): void
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @return string
     */
    public function getGoogleRecaptchaSecretKey(): string
    {
        return $this->googleRecaptchaSecretKey;
    }

    /**
     * @param string $googleRecaptchaSecretKey
     */
    public function setGoogleRecaptchaSecretKey(string $googleRecaptchaSecretKey): void
    {
        $this->googleRecaptchaSecretKey = $googleRecaptchaSecretKey;
    }

    /**
     * @return string
     */
    public function getReutersCalaisApiKey(): string
    {
        return $this->reutersCalaisApiKey;
    }

    /**
     * @param string $reutersCalaisApiKey
     */
    public function setReutersCalaisApiKey(string $reutersCalaisApiKey): void
    {
        $this->reutersCalaisApiKey = $reutersCalaisApiKey;
    }
}
