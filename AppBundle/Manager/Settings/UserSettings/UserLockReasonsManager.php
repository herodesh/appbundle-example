<?php

namespace Flendoc\AppBundle\Manager\Settings\UserSettings;

use Flendoc\AppBundle\Entity\Doctors\UserLockReasons;
use Flendoc\AppBundle\Entity\Doctors\UserLockReasonsLanguages;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\Settings\UserLockReasonsTrait;

/**
 * Class LegalAndHelpManager
 * @package Flendoc\AppBundle\Manager\LegalAndHelp
 */
class UserLockReasonsManager extends FlendocManager
{
    use UserLockReasonsTrait;

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function synchronizeLockReasons()
    {
        //existing in the database
        $aExistingLocks = $this->getRepository(UserLockReasons::class)->findAll();

        $aExistingLocksStringIdentifiers = [];
        foreach ($aExistingLocks as $aExistingLock) {
            $aExistingLocksStringIdentifiers[] = $aExistingLock->getStringIdentifier();
        }

        foreach ($this->userLockReason as $aLockReason) {
            if (!in_array($aLockReason['stringIdentifier'], $aExistingLocksStringIdentifiers)) {
                $this->em->persist($this->_createLockReasonFromTrait($aLockReason));
            }
        }

        $this->em->flush();
    }

    /**
     * @param array $aLockReason
     *
     * @return UserLockReasons
     */
    private function _createLockReasonFromTrait(array $aLockReason)
    {
        $oUserLockReason = new UserLockReasons();

        $oUserLockReason->setStringIdentifier(trim($aLockReason['stringIdentifier']));

        //add email content
        foreach ($aLockReason['content'] as $sLanguageIso => $aLockReasonContent) {
            $oUserLockReasonTranslations = new UserLockReasonsLanguages();

            $oUserLockReasonTranslations->setUserLockReasons($oUserLockReason);
            $oUserLockReasonTranslations->setLanguages($this->em->getRepository(Languages::class)
                                                                ->findOneByIso($sLanguageIso));
            $oUserLockReasonTranslations->setName(trim($aLockReasonContent['title']));
            $oUserLockReasonTranslations->setDescription(trim($aLockReasonContent['description']));
            $oUserLockReasonTranslations->setUserNotificationMessage(trim($aLockReasonContent['description']));

            $oUserLockReason->addUserLockReasonsLanguage($oUserLockReasonTranslations);
        }

        return $oUserLockReason;
    }

}
