<?php

namespace Flendoc\AppBundle\Manager\Settings\Cronjobs;

use Flendoc\AppBundle\Entity\Settings\Cronjobs;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class CronjobsManager
 * @package Flendoc\AppBundle\Manager\Settings\Cronjobs
 */
class CronjobsManager extends FlendocManager
{
    /**
     * @param string $sCommandName
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function activateHasTaskToDo($sCommandName)
    {
        $oCronjob = $this->getRepository(Cronjobs::class)->findOneBy([
            'commandName' => $sCommandName,
        ]);

        if (null != $oCronjob) {
            $oCronjob->setHasTaskToDo(true);

            $this->getEntityManager()->flush();

            return true;
        }

        return false;
    }
}
