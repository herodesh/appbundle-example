<?php

namespace Flendoc\AppBundle\Manager\WebSocket;

use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Manager\AbstractFlendocManager;
use Gos\Bundle\WebSocketBundle\Periodic\PeriodicInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class DoctrinePeriodicPingManager extends AbstractFlendocManager implements PeriodicInterface
{
    /**
     * @var LoggerInterface|NullLogger
     */
    protected $logger;

    /**
     * @var int|float
     */
    protected $timeout;

    /**
     * @param LoggerInterface  $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = null === $logger ? new NullLogger() : $logger;
        $this->timeout = AppConstants::WEB_SOCKETS_DOCTRINE_PING_TIME;
    }

    /**
     * @param int|float $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * Function that keeps alive connection with MySQL
     */
    public function tick()
    {
        try {
            $startTime = microtime(true);
            $this->getEntityManager()->getConnection()->ping();
            $endTime = microtime(true);
            $this->logger->notice(sprintf('Successfully ping sql server (~%s ms)', round(($endTime - $startTime) * 100000), 2));
        } catch (\Exception $e) {
            $this->logger->warning('Sql server is gone, and unable to reconnect');
        }
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }
}
