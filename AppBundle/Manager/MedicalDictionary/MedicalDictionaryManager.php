<?php

namespace Flendoc\AppBundle\Manager\MedicalDictionary;

use Flendoc\AppBundle\Adapter\Slugify\SlugifyManager;
use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Entity\HashTags\HashTags;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryImages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Uploader\PhotoUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class MedicalDictionaryManager
 * @package Flendoc\AppBundle\Manager\MedicalDictionary
 */
class MedicalDictionaryManager extends FlendocManager
{

    /** @var SlugifyManager */
    protected $slugify;

    /** @var PhotoUploader */
    protected $photoUploader;

    /** @var Router */
    protected $router;

    /**
     * MedicalDictionaryManager constructor.
     *
     * @param SlugifyManager $slugify
     * @param PhotoUploader  $photoUploader
     * @param Router         $router
     */
    public function __construct(SlugifyManager $slugify, PhotoUploader $photoUploader, Router $router)
    {
        $this->slugify       = $slugify;
        $this->photoUploader = $photoUploader;
        $this->router        = $router;
    }

    /**
     * @param array  $aUploadedFile
     * @param string $sMedicalDictionaryIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveMedicalDictionaryImage(array $aUploadedFile, $sMedicalDictionaryIdentifier)
    {
        /** @var MedicalDictionaryImages $oDoctorDictionaryImage */
        $oDoctorDictionaryImage = $this->createObject(MedicalDictionaryImages::class);

        $sPhotos = json_encode($aUploadedFile);

        $oDoctorDictionaryImage->setMedicalDictionary($this->getRepository(MedicalDictionary::class)->findOneBy([
            'identifier' => $sMedicalDictionaryIdentifier,
        ]));
        $oDoctorDictionaryImage->setPhotos($sPhotos);

        $this->saveObject($oDoctorDictionaryImage);

        return $oDoctorDictionaryImage->getIdentifier();
    }

    /**
     * Saves a Medical Dictionary Entry
     *
     * @param Request           $oRequest
     * @param MedicalDictionary $oFormData
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveMedicalDictionaryEntry(Request $oRequest, MedicalDictionary $oFormData)
    {
        foreach ($oFormData->getMedicalDictionaryImages() as $oMedicalDictionaryImage) {
            foreach ($oRequest->request->get('image') as $key => $item) {
                if ($oMedicalDictionaryImage->getIdentifier() === $key) {
                    $oMedicalDictionaryImage->setPhotoLicense($item);
                }
            }
        }

        foreach ($oFormData->getMedicalDictionaryTranslation() as $oMedicalDictionaryTranslation) {
            $oMedicalDictionaryTranslation->setSlug(
                $this->slugify->generateSlug(
                    $oMedicalDictionaryTranslation->getTitle(),
                    $oMedicalDictionaryTranslation->getLanguage()->getIso()
                )
            );
        }

        if ($oFormData->getIsActive()) {
            $this->generateMedicalDictionaryHashTags($oFormData, false);
        }

        return $this->saveObject($oFormData);
    }

    /**
     * @param string $sImageIdentifier
     * @param string $sAwsFolder
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteMedicalDictionaryImagesFromDatabaseAndAws($sImageIdentifier, $sAwsFolder)
    {
        $oMedicalDictionaryImage = $this
            ->getRepository(MedicalDictionaryImages::class)
            ->findOneBy(['identifier' => $sImageIdentifier]);

        if ($oMedicalDictionaryImage) {
            $aPhotos = json_decode($oMedicalDictionaryImage->getPhotos(), true);
            $this->deleteObject($oMedicalDictionaryImage);

            //delete from amazonaws
            foreach ($aPhotos['files'] as $photo) {
                $this->photoUploader->delete(sprintf('%s/%s', $sAwsFolder, $photo));
            }

            return true;
        }

        return false;
    }

    /**
     * Generates Keywords for a given medical dictionary entry
     *
     * @param MedicalDictionary $oMedicalDictionary
     * @param bool              $bFlush
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateMedicalDictionaryHashTags(MedicalDictionary $oMedicalDictionary, $bFlush = true)
    {
        $aMedicalDictionaryTranslations = $oMedicalDictionary->getMedicalDictionaryTranslation();

        foreach ($aMedicalDictionaryTranslations as $oMedicalDictionaryTranslation) {

            //do not overwrite existing hashtags
            if ($oMedicalDictionaryTranslation->getHasHashTags()) {
                continue;
            }

            //we do not use all the text to make the response faster
            $sText = $oMedicalDictionaryTranslation->getTitle().' '.
                $oMedicalDictionaryTranslation->getDefinition().' '.
                $oMedicalDictionaryTranslation->getDescription().' '.
                $oMedicalDictionaryTranslation->getCauses().' '.
                $oMedicalDictionaryTranslation->getSymptoms().' '.
                $oMedicalDictionaryTranslation->getDiagnosis();

            if (empty(trim($sText))) {
                continue;
            }

            $sText = json_encode(strip_tags($sText));

            $aResponse = $this->extractKeywordsFromText($sText);

            foreach ($aResponse as $item) {
                $oHashTag = new HashTags();
                $oHashTag->setTag($item);
                $oHashTag->setMedicalDictionaryTranslation($oMedicalDictionaryTranslation);
                $oMedicalDictionaryTranslation->addHashTag($oHashTag);

                $this->em->persist($oHashTag);
            }

            $oMedicalDictionaryTranslation->setHasHashTags(true);
            $this->em->persist($oMedicalDictionaryTranslation);
        }

        if ($bFlush) {
            $this->em->flush();
        }

    }

    /**
     * Generate a list of suggested medical dictionary entries to display based on tags
     *
     * @param string $sLanguageIso
     * @param string $sMedicalDictionaryIdentifier
     * @param array  $aMedicalDictionaryEntry
     *
     * @return array
     */
    public function generateSuggestionArticles(
        $sLanguageIso,
        $sMedicalDictionaryIdentifier,
        array $aMedicalDictionaryEntry
    ) {
        $oMedicalDictionaryRepository = $this->getRepository(MedicalDictionary::class);

        $oLanguage              = $this->getRepository(Languages::class)->findOneBy(['iso' => $sLanguageIso]);
        $aMedicalDictionaryTags = array_column($oMedicalDictionaryRepository->findGeneratedTagsByIdentifierAndLanguage(
            $aMedicalDictionaryEntry['translatedIdentifier'],
            $oLanguage
        ), 'tag');

        $aSimilarMedicalDictionaryEntries = $oMedicalDictionaryRepository->findEntriesWithSimilarTagsAndLanguage(
            $sMedicalDictionaryIdentifier,
            $aMedicalDictionaryTags,
            $oLanguage
        );

        $aGeneratedUrls = [];

        foreach ($aSimilarMedicalDictionaryEntries as $aSimilarMedicalDictionaryEntry) {
            $aGeneratedUrls[] = [
                'title' => $aSimilarMedicalDictionaryEntry['title'],
                'definition' => strip_tags(substr($aSimilarMedicalDictionaryEntry['definition'], 0, 150)),
                'url'   => $this->router->generate('doctors_view_medical_dictionary', [
                    'sIdentifier'  => $aSimilarMedicalDictionaryEntry['medicalDictionaryIdentifier'],
                    'sLanguageIso' => $sLanguageIso,
                    'sSlug'        => $aSimilarMedicalDictionaryEntry['slug'],
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];
        }

        return $aGeneratedUrls;
    }
}
