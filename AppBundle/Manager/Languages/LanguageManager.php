<?php

namespace Flendoc\AppBundle\Manager\Languages;

use Flendoc\AppBundle\Entity\Languages\TranslatedLanguages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Traits\Languages\LanguagesDefaultTrait;

/**
 * Class LanguageManager
 * @package Flendoc\AppBundle\Manager\Languages
 */
class LanguageManager extends FlendocManager
{
    use LanguagesDefaultTrait;

    /**
     * Synchronize Languages entries
     */
    public function synchronizeLanguages()
    {
        //find all existing languages
        $aDatabaseLanguages = $this->em->getRepository('AppBundle:Languages\Languages')->findAll();

        //buildAllDBIso
        $aDbLanguagesIso = [];
        foreach ($aDatabaseLanguages as $oDatabaseLanguage) {
            $aDbLanguagesIso[] = $oDatabaseLanguage->getIso();
        }

        //build all Iso's from trait
        $aTraitIso = [];
        foreach ($this->aLanguages as $key => $aLanguage) {
            $aTraitIso[] = $aLanguage['iso'];
        }

        //get through traits entries
        foreach ($aTraitIso as $item) {
            //if it does not exist in the database languages
            if (!in_array($item, $aDbLanguagesIso)) {
                foreach ($this->aLanguages as $key => $aLanguage) {
                    if ($aLanguage['iso'] == $item) {
                        $oLanguages = new Languages();
                        $oLanguages->setIso($aLanguage['iso']);
                        $oLanguages->setActive($aLanguage['isActive']);
                        $oLanguages->setIsPrimary($aLanguage['isPrimary']);

                        $this->em->persist($oLanguages);
                    }
                }
                $this->em->flush();
            }
        }

        foreach ($aTraitIso as $item) {
            if (!in_array($item, $aDbLanguagesIso)) {
                foreach ($this->aLanguages as $aLanguage) {
                    if ($aLanguage['iso'] == $item) {
                        foreach ($aLanguage['languages'] as $langKey => $langItem) {
                            $oTranslatedLanguage = new TranslatedLanguages();

                            $oTranslatedLanguage->setName($langItem);
                            $oTranslatedLanguage->setIso($langKey);
                            $oTranslatedLanguage->setLanguage($this->em->getRepository('AppBundle:Languages\Languages')
                                                                       ->findOneByIso($aLanguage['iso'])
                            );
                            $oTranslatedLanguage->setTranslatedLanguages($this->em->getRepository('AppBundle:Languages\Languages')
                                                                                  ->findOneByIso($langKey)
                            );

                            $this->em->persist($oTranslatedLanguage);
                        }
                    }
                }
            }
            $this->em->flush();
        }
        $this->em->flush();

        return;
    }

}
