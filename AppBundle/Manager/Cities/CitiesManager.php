<?php

namespace Flendoc\AppBundle\Manager\Cities;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class CitiesManager
 * @package Flendoc\AppBundle\Manager\Cities
 */
class CitiesManager extends FlendocManager
{

    /** Cities rom country Id. Ex: The key will be cities_from_country_id_XXX_YYY, where XXX is the id of the country and YYY is the language iso */
    const CITIES_FROM_COUNTRY_ID_AND_USER_LANGUAGE = 'cities_from_the_country_id_and_user_language_';

    /**
     * Saves a city in DB and Neo4j
     *
     * @param Cities $oCity
     */
    public function saveCity(Cities $oCity): void
    {
        //save to MySQL DB
        $this->saveObject($oCity);
        //save to Neo4J DB
        $this->neo4JManager->client->run('
            MERGE (c:City {identifier: "'.$oCity->getIdentifier().'"})
            SET c+={name: "'.$oCity->getDisplayCityName(AppConstants::LANGUAGE_DEFAULT_ISO).'", countryIdentifier: "'.$oCity->getCountry()
                                                                                                                            ->getIdentifier().'"}
            RETURN c;
        ');
    }

    /**
     * Get Cities by Redis Cache. If not found save to redis cache
     * We should always clear the cities array on any changes we made
     *
     * @param $sCountryId
     * @param $sUserLanguage
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getCitiesByRedisCache($sCountryId, $sUserLanguage)
    {
        $aCountries = $this->displayCacheData(CacheKeyPrefixes::APPLICATION_CITIES, self::CITIES_FROM_COUNTRY_ID_AND_USER_LANGUAGE.$sCountryId.$sUserLanguage);

        if (!empty ($aCountries)) {
            return $aCountries;
        } else {
            $aCities = $this->getRepository(Cities::class)
                            ->findCitiesByUserLanguageJson($sCountryId, $sUserLanguage);

            return $this->rebuildAndDisplayCacheData(
                CacheKeyPrefixes::APPLICATION_CITIES,
                self::CITIES_FROM_COUNTRY_ID_AND_USER_LANGUAGE.$sCountryId.$sUserLanguage,
                $aCities
            );
        }
    }
}
