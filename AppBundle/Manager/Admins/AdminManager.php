<?php

namespace Flendoc\AppBundle\Manager\Admins;

use Flendoc\AppBundle\Entity\Admins\AdminProfile;
use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AdminManager
 * @package Flendoc\AppBundle\Manager\Admins
 */
class AdminManager extends FlendocManager
{

    /**
     * @var UserPasswordEncoder
     */
    protected $passwordEncoder;

    /**
     * DoctorManager constructor.
     *
     * @param UserPasswordEncoder $passwordEncoder
     */
    public function __construct(UserPasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Update user's Password
     *
     * @param UserInterface $oAdmin
     */
    public function updatePassword(UserInterface $oAdmin)
    {
        if (null != $oAdmin->getPlainPassword()) {
            $sPassword = $this->passwordEncoder->encodePassword($oAdmin, $oAdmin->getPlainPassword());
            $oAdmin->setPassword($sPassword);
            $oAdmin->setPasswordRequestedAt(null);
            $oAdmin->setConfirmationToken(null);

            $this->saveObject($oAdmin);
        }

        return;
    }

    /**
     * Process registration data and save admin
     *
     * @param Admins $oAdmin
     *
     * @return Admins
     */
    public function processRegistrationData(Admins $oAdmin)
    {
        $oAdminProfile = new AdminProfile();
        $oAdminProfile->setFirstName($oAdmin->getFirstName());
        $oAdminProfile->setLastName($oAdmin->getLastName());
        $oAdminProfile->setGender($oAdmin->getGender());
        $oAdminProfile->setAdmin($oAdmin);

        $oAdmin->setAdminProfile($oAdminProfile);
        $oAdmin->setDefaultLanguage($oAdmin->getDefaultLanguage());
        $oAdmin->addRole($this->getEntityManager()
            ->getRepository('AppBundle:Users\UserRoles')
            ->findOneByName('ROLE_ADMIN'))
        ;
        $oAdmin->setEnabled(true);
        $oAdmin->setHasGeneratedPassword(true);

        $sPlainPassword = $oAdmin->generateNewToken(10);
        $oAdmin->setPlainPassword($sPlainPassword);
        $sPassword = $this->passwordEncoder->encodePassword($oAdmin, $sPlainPassword);
        $oAdmin->setPassword($sPassword);

        return $oAdmin;
    }
}
