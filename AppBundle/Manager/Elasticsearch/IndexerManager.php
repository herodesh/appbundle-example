<?php

namespace Flendoc\AppBundle\Manager\Elasticsearch;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Elasticsearch\Client;
use Flendoc\AppBundle\Adapter\Elasticsearch\ElasticsearchAdapter;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\Cities\CityLanguages;
use Flendoc\AppBundle\Entity\Countries\Countries;
use Flendoc\AppBundle\Entity\Countries\CountryLanguages;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryTranslations;
use Flendoc\AppBundle\Entity\Skills\SkillLanguages;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Manager\AbstractFlendocManager;
use Flendoc\AppBundle\Repository\Cities\CityLanguagesRepository;
use Flendoc\AppBundle\Repository\Countries\CountryLanguagesRepository;
use Flendoc\AppBundle\Repository\Skills\SkillLanguagesRepository;
use Monolog\Logger;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerManager extends AbstractFlendocManager
{
    /**
     * @var ElasticsearchAdapter
     */
    protected $adapter;

    /** @var EntityManager */
    protected $em;

    /** @var Logger */
    protected $logger;

    /** @var Client */
    protected $oElasticClient;

    /**
     * IndexerManager constructor.
     *
     * @param ElasticsearchAdapter $adapter
     * @param EntityManager        $em
     * @param Logger               $logger
     */
    public function __construct(
        ElasticsearchAdapter $adapter,
        EntityManager $em,
        Logger $logger
    ) {
        $this->adapter        = $adapter;
        $this->oElasticClient = $adapter->client;
        $this->em             = $em;
        $this->logger         = $logger;
    }

    /**
     * @param string      $sSearchTerm
     * @param string      $sLangIso
     * @param int         $iPageSize
     * @param string|null $sSearchAfter
     * @param int|null    $iMainSearchPageSize
     *
     * @return array|false
     */
    public function searchUsers(
        $sSearchTerm,
        $sLangIso,
        $iPageSize,
        $sSearchAfter = null,
        $iMainSearchPageSize = null
    ) {

        if (!$this->oElasticClient->ping() || !$this->oElasticClient->indices()->exists(['index' => 'users'])) {
            return false;
        }

        $sSearchTerm = strtolower($sSearchTerm);
        $sNextSearchAfter = null;
        $aUsers = [];
        $iSize = $iMainSearchPageSize ?: $iPageSize;

        $aParams = [
            'index' => 'users',
            'type'  => 'users',
            'body'  => [
                'size'  => $iSize,
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'regexp' => [
                                    'searchableName' => sprintf('.*%s.*', $sSearchTerm)
                                ]
                            ],
                            [
                                'match' => [
                                    'enabled' => true,
                                ]
                            ]
                        ]
                    ]
                ],
                'sort'  => [
                    [
                        'createdAt' => 'asc',
                    ],
                    [
                        'identifier' => 'asc'
                    ],
                ],
            ],
        ];

        // Here we have to handle pagination on search page
        if ($sSearchAfter) {
            $aParams = $this->addSearchAfterParams($aParams, $sSearchAfter);
        }

        try {
            $aResults = $this->oElasticClient->search($aParams);
        } catch (\Exception $oException) {
            $this->logger->err(sprintf(
                    '<error>Failed to search on users after %s : %s</error>',
                    $sSearchTerm ,
                    $oException->getMessage())
            );

            return false;
        }

        if (isset($aResults['hits']['hits']) && count($aResults['hits']['hits'])) {
            $aSort = [];
            $aHits = $aResults['hits']['hits'];

            foreach ($aHits as $aHit) {
                $aHitSource = $aHit['_source'];
                $aUser = [
                    'identifier' => $aHitSource['identifier'],
                    'name'       => $aHitSource['fullName'],
                    'email'      => $aHitSource['email'],
                    'isVerified' => $aHitSource['isVerified'],
                ];

                if (array_key_exists('country', $aHitSource) && array_key_exists($sLangIso, $aHitSource['country']['translations'])) {
                    $aUser['countryName'] = $aHitSource['country']['translations'][$sLangIso];
                }

                if (array_key_exists('city', $aHitSource) && array_key_exists($sLangIso, $aHitSource['city']['translations'])) {
                    $aUser['cityName'] = $aHitSource['city']['translations'][$sLangIso];
                }

                $aUsers[] = $aUser;
                $aSort = $aHit['sort'];
            }

            if ($aSort) {
                $sNextSearchAfter = sprintf('%s$%s', $aSort[0], $aSort[1]);
                $sNextSearchAfter = base64_encode($sNextSearchAfter);
            }
        }

        return [
            'results'     => $aUsers,
            'searchAfter' => $sNextSearchAfter,
        ];
    }

    /**
     * @param Doctors $oDoctor
     * @param string  $sId
     */
    public function indexUser($oDoctor, $sId = null):void
    {
        $aBodyParams = [
            'fullName'       => $oDoctor->getDoctorProfile()->getFullName(),
            'searchableName' => strtolower($oDoctor->getDoctorProfile()->getFullName()),
            'email'          => $oDoctor->getUsername(),
            'identifier'     => $oDoctor->getIdentifier(),
            'isVerified'     => $oDoctor->getIsVerified(),
            'enabled'        => $oDoctor->getEnabled(),
            'createdAt'      => strtotime($oDoctor->getCreatedAt()->format('Y-m-d H:i:s')),
        ];

        $aBodyParams = $this->addUserCountryParam($aBodyParams, $oDoctor->getDoctorProfile()->getCountry());
        $aBodyParams = $this->addUserCityParam($aBodyParams, $oDoctor->getDoctorProfile()->getCity());
        $aParams = [
            'index' => 'users',
            'type'  => 'users',
            'body'  => $aBodyParams,
        ];

        if ($sId) {
            $aParams['id'] = $sId;
        }

        try {
            $response = $this->oElasticClient->index($aParams);
            // Unset the bulk response when you are done to save memory
            unset($response);
        } catch (\Exception $oException) {
            $this->logger->err(sprintf('<error>Failed to index user %s : %s</error>',$oDoctor->getIdentifier() ,$oException->getMessage()));
        }
    }

    /**
     * @param string $sIdentifier
     */
    public function reindexUser($sIdentifier):void
    {
        /** @var Doctors $oDoctor */
        $oDoctor = $this->em->getRepository(Doctors::class)
            ->findOneBy([
                'identifier' => $sIdentifier,
            ])
        ;

        if (!$oDoctor) {
            return;
        }

        $sId = null;
        $aParams = [
            'index' => 'users',
            'type' => 'users',
            'body' => [
                'query' => [
                    'match' => [
                        'identifier' => $oDoctor->getIdentifier(),
                    ],
                ],
            ],
        ];
        $aResponse = $this->oElasticClient->search($aParams);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];
            }
        }

        $this->indexUser($oDoctor, $sId);
    }

    /**
     * @param array                $aDoctors
     * @param integer              $iTotalDoctors
     * @param integer              $iBatchSize
     * @param integer|null         $iBatchNumber
     * @param OutputInterface|null $oOutput
     */
    public function bulkIndexUsers(
        $aDoctors,
        $iTotalDoctors,
        $iBatchSize = 250,
        $iBatchNumber = null,
        OutputInterface $oOutput = null
    ):void {
        $aParams = ['body' => []];

        if (sizeof($aDoctors)) {
            /** @var Doctors $oDoctor */
            foreach ($aDoctors as $iKey=>$oDoctor) {
                $aParams['body'][] = [
                    'index' => [
                        '_index' => 'users',
                        '_type' => 'users',
                    ]
                ];

                $aDoctor = [
                    'fullName'       => $oDoctor->getDoctorProfile()->getFullName(),
                    'searchableName' => strtolower($oDoctor->getDoctorProfile()->getFullName()),
                    'email'          => $oDoctor->getUsername(),
                    'identifier'     => $oDoctor->getIdentifier(),
                    'isVerified'     => $oDoctor->getIsVerified(),
                    'enabled'        => $oDoctor->getEnabled(),
                    'createdAt'      => strtotime($oDoctor->getCreatedAt()->format('Y-m-d H:i:s')),
                ];

                $aDoctor = $this->addUserCountryParam($aDoctor, $oDoctor->getDoctorProfile()->getCountry());
                $aDoctor = $this->addUserCityParam($aDoctor, $oDoctor->getDoctorProfile()->getCity());
                $aParams['body'][] = $aDoctor;


                // Every 250 documents stop and send the bulk request
                if (0 === ($iKey + 1) % $iBatchSize) {
                    $aParams = $this->bulkIndex($aParams, $iTotalDoctors, $iBatchNumber, $oOutput);
                }
            }
        }

        // Send the last batch if exists
        if (!empty($aParams['body'])) {
            $this->indexLastBatch($aParams, $iTotalDoctors, $oOutput);
        }

        if ($oOutput) {
            $oOutput->writeln('<info>Successfully index enabled users</info>');
        } else {
            $this->logger->info('<info>Successfully index enabled users</info>');
        }
    }

    /**
     * @param string      $sUserIdentifier
     * @param string|null $sId
     *
     * @return bool
     */
    public function deleteUser($sUserIdentifier, $sId = null)
    {
        if ($sId) {
            return $this->deleteUserQuery($sUserIdentifier, $sId);
        }

        $aResponse = $this->oElasticClient->search([
            'index' => 'users',
            'type'  => 'users',
            'body'  => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'identifier' => $sUserIdentifier,

                                ]
                            ],
                        ]
                    ]
                ]
            ]
        ]);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];

                return $this->deleteUserQuery($sUserIdentifier, $sId);
            }
        }

        return false;
    }

    /**
     * @param string $sUserIdentifier
     * @param string $sId
     *
     * @return bool
     */
    private function deleteUserQuery($sUserIdentifier, $sId)
    {
        try {
            $aParams = [
                'index' => 'users',
                'type'  => 'users',
                'id'    => $sId,
            ];
            $response = $this->oElasticClient->delete($aParams);
            // Unset the bulk response when you are done to save memory
            unset($response);

            return true;
        } catch (\Exception $oException) {
            $this->logger->err(sprintf('<error>Failed to delete user %s : %s</error>', $sUserIdentifier, $oException->getMessage()));
        }

        return false;
    }

    /**
     * @param string      $sSearchTerm
     * @param string      $sLangIso
     * @param int         $iPageSize
     * @param string|null $sSearchAfter
     * @param int|null    $iMainSearchPageSize
     *
     * @return array|false
     */
    public function searchMedicalCases(
        $sSearchTerm,
        $sLangIso,
        $iPageSize,
        $sSearchAfter = null,
        $iMainSearchPageSize = null
    ) {

        if (!$this->oElasticClient->ping() || !$this->oElasticClient->indices()->exists(['index' => 'medical-cases'])) {
            return false;
        }

        $sSearchTerm = strtolower($sSearchTerm);
        $sNextSearchAfter = null;
        $aMedicalCases = [];
        $iSize = $iMainSearchPageSize ?: $iPageSize;

        $aParams = [
            'index' => 'medical-cases',
            'type'  => 'medical-cases',
            'body'  => [
                'size'  => $iSize,
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'regexp' => [
                                    'searchableTitle' => sprintf('.*%s.*', $sSearchTerm)
                                ]
                            ],
                            [
                                'match' => [
                                    'approved' => true,
                                ]
                            ]
                        ]
                    ]
                ],
                'sort'  => [
                    [
                        'createdAt' => 'asc',
                    ],
                    [
                        'identifier' => 'asc'
                    ],
                ],
            ],
        ];

        // Here we have to handle pagination on search page
        if ($sSearchAfter) {
            $aParams = $this->addSearchAfterParams($aParams, $sSearchAfter);
        }

        try {
            $aResults = $this->oElasticClient->search($aParams);
        } catch (\Exception $oException) {
            $this->logger->err(sprintf(
                '<error>Failed to search on medical cases after %s : %s</error>',
                $sSearchTerm ,
                $oException->getMessage())
            );

            return false;
        }

        if (isset($aResults['hits']['hits']) && count($aResults['hits']['hits'])) {
            $aSort = [];
            $aHits = $aResults['hits']['hits'];

            foreach ($aHits as $aHit) {
                $aHitSource = $aHit['_source'];

                $aMedicalCase = [
                    'identifier'             => $aHitSource['identifier'],
                    'medicalCaseLanguageIso' => $sLangIso,
                    'slugTitle'              => $aHitSource['slug'],
                    'title'                  => $aHitSource['title'],
                    'description'            => $aHitSource['description'],
                ];

                if (array_key_exists('image', $aHitSource)) {
                    $aMedicalCase['image'] = $aHitSource['image'];
                }

                $aSkills = [];
                if (array_key_exists('skills', $aHitSource) && sizeof($aHitSource['skills'])) {
                    foreach ($aHitSource['skills'] as $aSkill) {
                        $aSkills[] = ['name' => $aSkill['translation']];
                    }

                    $aMedicalCase['skills'] = $aSkills;
                }

                $aMedicalCases[] = $aMedicalCase;
                $aSort = $aHit['sort'];
            }

            if ($aSort) {
                $sNextSearchAfter = sprintf('%s$%s', $aSort[0], $aSort[1]);
                $sNextSearchAfter = base64_encode($sNextSearchAfter);
            }
        }

        return [
            'results'     => $aMedicalCases,
            'searchAfter' => $sNextSearchAfter,
        ];
    }

    /**
     * @param MedicalCases $oMedicalCase
     */
    public function indexMedicalCase($oMedicalCase):void
    {
        if ($oMedicalCase->getIsApproved() && $oMedicalCase->getIsAdminRevised()) {
            $sId = null;
            $aParams = $this->getMedicalCaseSearchParams($oMedicalCase->getIdentifier());
            $aResponse = $this->oElasticClient->search($aParams);

            if (array_key_exists('hits', $aResponse)) {
                $aHits = $aResponse['hits'];

                if (sizeof($aHits['hits'])) {
                    $aHit = $aHits['hits'][0];
                    $sId = $aHit['_id'];
                }
            }

            $aParams = [
                'index' => 'medical-cases',
                'type'  => 'medical-cases',
                'body'  => $this->getMedicalCaseDocumentBody($oMedicalCase),
            ];

            if ($sId) {
                $aParams['id'] = $sId;
            }

            try {
                $response = $this->oElasticClient->index($aParams);
                // Unset the bulk response when you are done to save memory
                unset($response);
            } catch (\Exception $oException) {
                $this->logger->err(sprintf('<error>Failed to index medical case %s : %s</error>',$oMedicalCase->getIdentifier() ,$oException->getMessage()));
            }
        }
    }

    /**
     * @param MedicalCases $oMedicalCase
     */
    public function disableMedicalCaseDocument($oMedicalCase)
    {
        $sId = null;
        $aParams = $this->getMedicalCaseSearchParams($oMedicalCase->getIdentifier());
        $aResponse = $this->oElasticClient->search($aParams);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];

                $aParams = [
                    'index' => 'medical-cases',
                    'type'  => 'medical-cases',
                    'id'    => $sId,
                    'body'  => [
                        'doc' => [
                            'approved' => false,
                        ],
                    ],
                ];

                try {
                    $response = $this->oElasticClient->update($aParams);
                    // Unset the bulk response when you are done to save memory
                    unset($response);
                } catch (\Exception $oException) {
                    $this->logger->err(sprintf('<error>Failed to update medical case %s : %s</error>',$oMedicalCase->getIdentifier() ,$oException->getMessage()));
                }
            }
        }
    }

    /**
     * @param array                $aMedicalCases
     * @param integer              $iTotalMedicalCases
     * @param integer              $iBatchSize
     * @param integer|null         $iBatchNumber
     * @param OutputInterface|null $oOutput
     */
    public function bulkIndexMedicalCases(
        $aMedicalCases,
        $iTotalMedicalCases,
        $iBatchSize = 250,
        $iBatchNumber = null,
        OutputInterface $oOutput = null
    ):void {
        $aParams = ['body' => []];

        if (sizeof($aMedicalCases)) {
            /** @var MedicalCases $oMedicalCase */
            foreach ($aMedicalCases as $iKey => $oMedicalCase) {
                $aParams['body'][] = [
                    'index' => [
                        '_index' => 'medical-cases',
                        '_type' => 'medical-cases',
                    ]
                ];
                $aParams['body'][] = $this->getMedicalCaseDocumentBody($oMedicalCase);

                // Every 250 documents stop and send the bulk request
                if (0 === ($iKey + 1) % $iBatchSize) {
                    $aParams = $this->bulkIndex($aParams, $iTotalMedicalCases, $iBatchNumber, $oOutput);
                }
            }
        }

        // Send the last batch if exists
        if (!empty($aParams['body'])) {
            $this->indexLastBatch($aParams, $iTotalMedicalCases, $oOutput);
        }

        if ($oOutput) {
            $oOutput->writeln('<info>Successfully index approved medical cases</info>');
        } else {
            $this->logger->info('<info>Successfully index approved medical cases</info>');
        }
    }

    /**
     * @param MedicalCases $oMedicalCase
     *
     * @return bool
     */
    public function deleteMedicalCaseDocument($oMedicalCase)
    {
        $sId = null;
        $aParams = $this->getMedicalCaseSearchParams($oMedicalCase->getIdentifier());
        $aResponse = $this->oElasticClient->search($aParams);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];

                $aParams = [
                    'index' => 'medical-cases',
                    'type'  => 'medical-cases',
                    'id'    => $sId,
                ];

                try {
                    $response = $this->oElasticClient->delete($aParams);
                    // Unset the bulk response when you are done to save memory
                    unset($response);
                } catch (\Exception $oException) {
                    $this->logger->err(sprintf('<error>Failed to delete medical case %s : %s</error>',$oMedicalCase->getIdentifier() ,$oException->getMessage()));
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @TODO Handle search functionality after front-end is done.
     *
     * @param string      $sSearchTerm
     * @param string      $sLangIso
     * @param int         $iPageSize
     * @param string|null $sSearchAfter
     * @param int|null    $iMainSearchPageSize
     *
     * @return array|false
     */
    public function searchMedicalDictionary(
        $sSearchTerm,
        $sLangIso,
        $iPageSize,
        $sSearchAfter = null,
        $iMainSearchPageSize = null
    ) {
        if (!$this->oElasticClient->ping() || !$this->oElasticClient->indices()->exists(['index' => 'dictionary-documents'])) {
            return false;
        }

        $sSearchTerm = strtolower($sSearchTerm);
        $sNextSearchAfter = null;
        $aDictionaryDocuments = [];
        $iSize = $iMainSearchPageSize ?: $iPageSize;

        $aParams = [
            'index' => 'dictionary-documents',
            'type'  => 'dictionary-documents',
            'body'  => [
                'size'  => $iSize,
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'active' => true,
                                ]
                            ],
                            [
                                'nested' => [
                                    'path'  => 'translations',
                                    'query' => [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'regexp' => [
                                                        'translations.searchableTitle' => sprintf('.*%s.*', $sSearchTerm)
                                                    ],
                                                ],
                                                [
                                                    'match' => [
                                                        'translations.languageIso' => $sLangIso,
                                                    ],
                                                ],
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'sort'  => [
                    [
                        'createdAt' => 'asc',
                    ],
                    [
                        'identifier' => 'asc'
                    ],
                ],
            ],
        ];

        // Here we have to handle pagination on search page
        if ($sSearchAfter) {
            $aParams = $this->addSearchAfterParams($aParams, $sSearchAfter);
        }

        try {
            $aResults = $this->oElasticClient->search($aParams);
        } catch (\Exception $oException) {
            $this->logger->err(sprintf(
                    '<error>Failed to search on medical dictionaries after %s : %s</error>',
                    $sSearchTerm ,
                    $oException->getMessage())
            );

            return false;
        }

        if (isset($aResults['hits']['hits']) && count($aResults['hits']['hits'])) {
            $aSort = [];
            $aHits = $aResults['hits']['hits'];

            foreach ($aHits as $aHit) {
                $aHitSource = $aHit['_source'];

                $aDictionaryDocument = [
                    'identifier' => $aHitSource['identifier'],
                ];

                foreach ($aHitSource['translations'] as $aDocumentTranslation) {
                    if ($sLangIso === $aDocumentTranslation['languageIso']) {
                        $aDictionaryDocument['title']      = $aDocumentTranslation['title'];
                        $aDictionaryDocument['definition'] = $aDocumentTranslation['definition'];

                        if (array_key_exists('image', $aDictionaryDocument)) {
                            $aMedicalCase['medicalDictionaryImage'] = $aDictionaryDocument['image'];
                        }
                        break;
                    }

                }

                $aDictionaryDocuments[] = $aDictionaryDocument;
                $aSort = $aHit['sort'];
            }

            if ($aSort) {
                $sNextSearchAfter = sprintf('%s$%s', $aSort[0], $aSort[1]);
                $sNextSearchAfter = base64_encode($sNextSearchAfter);
            }
        }

        return [
            'results'     => $aDictionaryDocuments,
            'searchAfter' => $sNextSearchAfter,
        ];
    }

    /**
     * @param MedicalDictionary $oDictionaryDocument
     */
    public function indexDictionaryDocument($oDictionaryDocument):void
    {
        $sId = null;
        $aHit = [];
        $aParams = $this->getDictionaryDocumentSearchParams($oDictionaryDocument->getIdentifier());
        $aResponse = $this->oElasticClient->search($aParams);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];
            }
        }

        if (!empty($aHit) || $oDictionaryDocument->getIsActive()) {
            $aDictionaryDocumentsBody =  $this->getDictionaryDocumentBody($oDictionaryDocument);

            if (!key_exists('translations', $aDictionaryDocumentsBody)) {

                if ($sId) {
                    $this->deleteDictionaryDocument($oDictionaryDocument, $sId);
                }
                return;
            }

            $aParams = [
                'index' => 'dictionary-documents',
                'type'  => 'dictionary-documents',
                'body'  => $aDictionaryDocumentsBody,
            ];

            if ($sId) {
                $aParams['id'] = $sId;
            }

            try {
                $response = $this->oElasticClient->index($aParams);
                // Unset the bulk response when you are done to save memory
                unset($response);
            } catch (\Exception $oException) {
                $this->logger->err(sprintf('<error>Failed to index medical dictionary document %s : %s</error>', $oDictionaryDocument->getIdentifier(), $oException->getMessage()));
            }
        }
    }

    /**
     * @param array                $aDictionaryDocuments
     * @param integer              $iTotalDictionaryDocuments
     * @param integer              $iBatchSize
     * @param integer|null         $iBatchNumber
     * @param OutputInterface|null $oOutput
     */
    public function bulkIndexDictionaryDocuments(
        $aDictionaryDocuments,
        $iTotalDictionaryDocuments,
        $iBatchSize = 250,
        $iBatchNumber = null,
        OutputInterface $oOutput = null
    ):void {
        $aParams = ['body' => []];
        $iCounter = 0;

        if (!empty($aDictionaryDocuments)) {
            /** @var MedicalDictionary $oDictionaryDocument */
            foreach ($aDictionaryDocuments as $iKey => $oDictionaryDocument) {
                $aDictionaryDocumentsBody =  $this->getDictionaryDocumentBody($oDictionaryDocument);

                if (key_exists('translations', $aDictionaryDocumentsBody)) {
                    $aParams['body'][] = [
                        'index' => [
                            '_index' => 'dictionary-documents',
                            '_type' => 'dictionary-documents',
                        ]
                    ];
                    $aParams['body'][] = $aDictionaryDocumentsBody;
                    $iCounter++;
                }


                // Every 250 documents stop and send the bulk request
                if (0 !== $iCounter && 0 === $iCounter % $iBatchSize) {
                    $aParams = $this->bulkIndex($aParams, $iTotalDictionaryDocuments, $iBatchNumber, $oOutput);
                }
            }
        }

        // Send the last batch if exists
        if (!empty($aParams['body'])) {
            $this->indexLastBatch($aParams, $iTotalDictionaryDocuments, $oOutput);
        }

        if ($oOutput) {
            $oOutput->writeln('<info>Successfully index active medical dictionary documents</info>');
        } else {
            $this->logger->info('<info>Successfully index active medical dictionary documents</info>');
        }
    }

    /**
     * @param MedicalDictionary $oDictionaryDocument
     * @param string|null       $sId
     *
     * @return bool
     */
    public function deleteDictionaryDocument($oDictionaryDocument, $sId = null)
    {
        if ($sId) {
            return $this->deleteDictionaryDocumentQuery($oDictionaryDocument, $sId);
        }

        $aResponse = $this->oElasticClient->search([
            'index' => 'dictionary-documents',
            'type'  => 'dictionary-documents',
            'body'  => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'identifier' => $oDictionaryDocument->getIdentifier(),

                                ]
                            ],
                        ]
                    ]
                ]
            ]
        ]);

        if (array_key_exists('hits', $aResponse)) {
            $aHits = $aResponse['hits'];

            if (sizeof($aHits['hits'])) {
                $aHit = $aHits['hits'][0];
                $sId = $aHit['_id'];

                return $this->deleteDictionaryDocumentQuery($oDictionaryDocument, $sId);
            }
        }

        return false;
    }

    /**
     * @param array  $aParams
     * @param string $sSearchAfter
     *
     * @return array
     */
    private function addSearchAfterParams($aParams, $sSearchAfter)
    {
        $sSearchAfter = base64_decode($sSearchAfter);

        if (strpos($sSearchAfter, '$')) {
            $sSearchAfter = explode('$', $sSearchAfter);
            $aParams['body']['search_after'] = [
                $sSearchAfter[0], // User createdAt field in milliseconds
                $sSearchAfter[1], // User identifier
            ];
        }

        return $aParams;
    }

    /**
     * @param array                $aParams
     * @param integer              $iTotalItems
     * @param integer              $iBatchNumber
     * @param OutputInterface|null $oOutput
     *
     * @return array
     */
    private function bulkIndex($aParams, $iTotalItems, $iBatchNumber, OutputInterface $oOutput = null)
    {
        try {
            $aResponses = $this->oElasticClient->bulk($aParams);
            $iBulkLength = sizeof($aParams['body']) / 2;

            if ($oOutput) {
                $oOutput->writeln(sprintf('<info>INDEXED %d items [%d remaining]</info>', $iBulkLength, $iTotalItems - $iBulkLength));
            } else {
                $this->logger->info(sprintf('<info>INDEXED %d items [%d remaining]</info>', $iBulkLength, $iTotalItems - $iBulkLength));
            }

            // Unset the bulk response when you are done to save memory
            unset($aResponses);
        } catch (\Exception $oException) {
            if ($oOutput) {
                $oOutput->writeln(sprintf('<error>Failed to execute batch %d: %s</error>', $iBatchNumber, $oException->getMessage()));
            } else {
                $this->logger->err(sprintf('<error>Failed to execute batch %d: %s</error>', $iBatchNumber, $oException->getMessage()));
            }
        }

        return ['body' => []];
    }

    /**
     * @param array   $aParams
     * @param integer $iTotalItems
     *
     * @param OutputInterface|null $oOutput
     */
    private function indexLastBatch($aParams, $iTotalItems, OutputInterface $oOutput = null)
    {
        try {
            $this->oElasticClient->bulk($aParams);
            $iBulkLength = sizeof($aParams['body']) / 2;

            if ($oOutput) {
                $oOutput->writeln(sprintf('<info>INDEXED %d items [%d remaining]</info>', $iBulkLength, $iTotalItems - $iBulkLength));
            } else {
                $this->logger->info(sprintf('<info>INDEXED %d items [%d remaining]</info>', $iBulkLength, $iTotalItems - $iBulkLength));
            }
        } catch (\Exception $oException) {
            if ($oOutput) {
                $oOutput->writeln(sprintf('<error>Failed to execute last batch: %s</error>', $oException->getMessage()));
            } else {
                $this->logger->err(sprintf('<error>Failed to execute last batch: %s</error>', $oException->getMessage()));
            }
        }
    }

    /**
     * @param Countries $oCountry
     *
     * @return array
     */
    private function getUserCountryLanguages($oCountry)
    {
        /** @var CountryLanguagesRepository $oCountryLanguagesRepository */
        $oCountryLanguagesRepository = $this->em->getRepository(CountryLanguages::class);

        $aCountryLanguages = $oCountryLanguagesRepository
            ->findNameByCountryId($oCountry->getId())
        ;

        $aCountryLangs = [];
        foreach ($aCountryLanguages as $aCountryLanguage) {
            $aCountryLangs[$aCountryLanguage["iso"]] = $aCountryLanguage["name"];
        }

        return $aCountryLangs;
    }

    /**
     * @param Cities $oCity
     *
     * @return array
     */
    private function getUserCityLanguages($oCity)
    {
        /** @var CityLanguagesRepository $oCityLanguagesRepository */
        $oCityLanguagesRepository = $this->em->getRepository(CityLanguages::class);

        $aCityLanguages = $oCityLanguagesRepository
            ->findNameByCityId($oCity->getId())
        ;

        $aCityLangs = [];
        foreach ($aCityLanguages as $aCityLanguage) {
            $aCityLangs[$aCityLanguage["iso"]] = $aCityLanguage["name"];
        }

        return $aCityLangs;
    }

    /**
     * @param array     $aParams
     * @param Countries $oCountry
     *
     * @return array
     */
    private function addUserCountryParam($aParams, $oCountry)
    {
        if ($oCountry) {
            $aCountryLanguages = $this->getUserCountryLanguages($oCountry);
            $aParams['country'] = [
                'identifier' => $oCountry->getIdentifier(),
                'translations' => $aCountryLanguages,
            ];
        }

        return $aParams;
    }

    /**
     * @param array  $aParams
     * @param Cities $oCity
     *
     * @return array
     */
    private function addUserCityParam($aParams, $oCity)
    {
        if ($oCity) {
            $aCityLanguages = $this->getUserCityLanguages($oCity);
            $aParams['city'] = [
                'identifier' => $oCity->getIdentifier(),
                'translations' => $aCityLanguages,
            ];
        }

        return $aParams;
    }

    /**
     * @param MedicalCases $oMedicalCase
     *
     * @return array
     */
    private function getMedicalCaseDocumentBody($oMedicalCase)
    {
        $body = [
            'title'           => $oMedicalCase->getTitle(),
            'searchableTitle' => strtolower($oMedicalCase->getTitle()),
            'description'     => substr($oMedicalCase->getDescription(), 0, 200),
            'identifier'      => $oMedicalCase->getIdentifier(),
            'slug'            => $oMedicalCase->getSlugTitle(),
            'approved'        => $oMedicalCase->getIsApproved(),
            'createdAt'       => strtotime($oMedicalCase->getCreatedAt()->format('Y-m-d H:i:s')),
        ];

        $sImage = $oMedicalCase->getFirstMedicalCaseImagePhotos();
        if ($sImage) {
            $body['image'] = $sImage;
        }

        $aSkills = $this->getMedicalCaseSkillsLanguages($oMedicalCase);
        if (sizeof($aSkills)) {
            $body['skills'] = $aSkills;
        }

        return $body;
    }

    /**
     * @param string $sIdentifier
     *
     * @return array
     */
    private function getMedicalCaseSearchParams($sIdentifier)
    {
        return $aParams = [
            'index' => 'medical-cases',
            'type'  => 'medical-cases',
            'body'  => [
                'query' => [
                    'match' => [
                        'identifier' => $sIdentifier,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param MedicalCases $oMedicalCase
     *
     * @return array
     */
    private function getMedicalCaseSkillsLanguages($oMedicalCase)
    {
        /** @var SkillLanguagesRepository $oSkillLanguagesRepository */
        $oSkillLanguagesRepository = $this->em->getRepository(SkillLanguages::class);
        /** @var ArrayCollection $aMedicalCaseSkills */
        $aMedicalCaseSkills = $oMedicalCase->getMedicalCaseSkills();

        if ($aMedicalCaseSkills->count()) {
            $aIds = [];

            /** @var Skills $oSkill */
            foreach ($aMedicalCaseSkills as $oSkill) {
                $aIds[] = $oSkill->getId();
            }

            $aSkillsNames = $oSkillLanguagesRepository->findNamesBySkillsIdsAndIso($aIds, $oMedicalCase->getMedicalCaseLanguageIso());
            return $aSkillsNames;
        }

        return [];
    }

    /**
     * @param MedicalDictionary $oDictionaryDocument
     *
     * @return array
     */
    private function getDictionaryDocumentBody($oDictionaryDocument)
    {
        $body = [
            'active'          => $oDictionaryDocument->getIsActive(),
            'createdAt'       => strtotime($oDictionaryDocument->getCreatedAt()->format('Y-m-d H:i:s')),
            'identifier'      => $oDictionaryDocument->getIdentifier(),
        ];
        $aTranslations = [];

        /** @var MedicalDictionaryTranslations $oMedicalDictionaryTranslation */
        foreach ($oDictionaryDocument->getMedicalDictionaryTranslation() as $oMedicalDictionaryTranslation) {
            $sTitle       = $oMedicalDictionaryTranslation->getTitle();
            $sDefinition = $oMedicalDictionaryTranslation->getDefinition();
            $sSlug        = $oMedicalDictionaryTranslation->getSlug();

            if (isset($sTitle) && isset($sDefinition)) {
                $aTranslation = [
                    'languageIso'     => $oMedicalDictionaryTranslation->getLanguage()->getIso(),
                    'title'           => $sTitle,
                    'searchableTitle' => strtolower($sTitle),
                    'definition'      => strip_tags(substr($sDefinition, 0, 200)),
                ];

                if (isset($sSlug)) {
                    $aTranslation['slug'] = $sSlug;
                }

                $aTranslations[] = $aTranslation;
            }
        }

        if(!empty($aTranslations)) {
            $body['translations'] = $aTranslations;
        }

        $sImage = $oDictionaryDocument->getFirstMedicalDictionaryImagePhotos();
        if ($sImage) {
            $body['image'] = $sImage;
        }

        return $body;
    }

    /**
     * @param string $sIdentifier
     *
     * @return array
     */
    private function getDictionaryDocumentSearchParams($sIdentifier)
    {
        return $aParams = [
            'index' => 'dictionary-documents',
            'type'  => 'dictionary-documents',
            'body'  => [
                'query' => [
                    'match' => [
                        'identifier' => $sIdentifier,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param MedicalDictionary $oDictionaryDocument
     * @param string            $sId
     *
     * @return bool
     */
    private function deleteDictionaryDocumentQuery($oDictionaryDocument, $sId)
    {
        try {
            $aParams = [
                'index' => 'dictionary-documents',
                'type'  => 'dictionary-documents',
                'id'    => $sId,
            ];
            $response = $this->oElasticClient->delete($aParams);
            // Unset the bulk response when you are done to save memory
            unset($response);

            return true;
        } catch (\Exception $oException) {
            $this->logger->err(sprintf('<error>Failed to delete medical dictionary document %s : %s</error>', $oDictionaryDocument->getIdentifier(), $oException->getMessage()));
        }

        return false;
    }
}
