<?php

namespace Flendoc\AppBundle\Manager\Messaging;

use Flendoc\AppBundle\Adapter\SwiftMailer\SwiftMailerAdapter;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Document\MongoMessage;
use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\Chats\DoctorChatFile;
use Flendoc\AppBundle\Entity\Chats\DoctorChatMember;
use Flendoc\AppBundle\Entity\Doctors\DoctorNotifications;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Mails\MailsConstants;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Manager\Notifications\NotificationManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Repository\Chats\DoctorChatFileRepository;
use Flendoc\AppBundle\Repository\Chats\DoctorChatMemberRepository;
use Flendoc\AppBundle\Repository\Chats\DoctorChatRepository;
use Flendoc\AppBundle\Repository\Doctors\DoctorRepository;
use Flendoc\AppBundle\Repository\Message\MongoMessageRepository;
use Flendoc\AppBundle\Uploader\DocumentsUploader;
use Flendoc\DoctorsBundle\Traits\TypeIdentifierProfileTrait;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Monolog\Logger;
use Symfony\Component\Routing\Router;

/**
 * Class MessagingManager
 * @package Flendoc\AppBundle\Manager\Messaging
 */
class MessagingManager extends FlendocManager
{

    use TypeIdentifierProfileTrait;

    /**
     * @var PusherInterface
     */
    protected $zmqPusher;

    /**
     * @var MessagingEncryptionManager
     */
    protected $messagingEncryptionManager;

    /** @var $logger Logger */
    protected $logger;

    /** @var $mailerAdapter SwiftMailerAdapter */
    protected $mailerAdapter;

    /** @var $router Router */
    protected $router;

    /** @var NotificationManager $notificationManager */
    protected $notificationManager;

    /** @var DocumentsUploader */
    protected $documentUploader;

    /**
     * MessagingManager constructor.
     *
     * @param PusherInterface            $oZmqPusher
     * @param MessagingEncryptionManager $oMessagingEncryptionManager
     * @param Logger                     $oLogger
     * @param SwiftMailerAdapter         $oMailer
     * @param Router                     $oRouter
     * @param NotificationManager        $oNotificationManager
     * @param DocumentsUploader          $documentsUploader
     */
    public function __construct(
        PusherInterface $oZmqPusher,
        MessagingEncryptionManager $oMessagingEncryptionManager,
        Logger $oLogger,
        SwiftMailerAdapter $oMailer,
        Router $oRouter,
        NotificationManager $oNotificationManager,
        DocumentsUploader $documentsUploader
    ) {
        $this->logger                     = $oLogger;
        $this->zmqPusher                  = $oZmqPusher;
        $this->messagingEncryptionManager = $oMessagingEncryptionManager;
        $this->mailerAdapter              = $oMailer;
        $this->router                     = $oRouter;
        $this->notificationManager        = $oNotificationManager;
        $this->documentUploader           = $documentsUploader;
    }

    /**
     * Authenticate user on messenger thread.
     *
     * @param string  $threadIdentifier
     * @param integer $userId
     *
     * @return bool
     */
    public function authenticateUser($threadIdentifier, $userId)
    {
        /** @var DoctorChat $oDoctorChat */
        $oDoctorChat = $this
            ->getRepository(DoctorChat::class)
            ->findOneBy([
                'identifier' => $threadIdentifier,
            ]);

        $oDoctorChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctorChat' => $oDoctorChat,
                'doctor'     => $userId,
                'leftChat'   => false,
            ]);

        if (!$oDoctorChatMember) {
            return false;
        }

        return true;
    }

    /**
     * Save message and associate it with an user and a chat.
     * Return false if DoctorChat/Doctor was not found and message id otherwise.
     *
     * @param string     $message
     * @param DoctorChat $oDoctorChat
     * @param Doctors    $oDoctor
     *
     * @return bool|MongoMessage
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveMessage($message, $oDoctorChat, $oDoctor)
    {
        $oDoctorChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctor'     => $oDoctor,
                'doctorChat' => $oDoctorChat,
            ]);

        $oMessageSentAt = new \DateTime();

        /*
         * Encrypt message
         * Response should be an array with a ciphertext and a tag
         */
        $aResponse = $this
            ->messagingEncryptionManager
            ->encryptMessage(
                $message,
                $oMessageSentAt,
                $oDoctor->getIdentifier(),
                $oDoctorChat->getIdentifier()
            );

        if ($aResponse) {
            /** @var MongoMessage $oChatMessage */
            $oChatMessage = $this->createObject(MongoMessage::class);
            $oChatMessage->setUserIdentifier($oDoctor->getIdentifier());
            $oChatMessage->setChatIdentifier($oDoctorChat->getIdentifier());
            $oChatMessage->setMessage($aResponse['ciphertext']);
            $oChatMessage->setSendAt($oMessageSentAt);
            $oChatMessage->setTag($aResponse['tag']);

            $this->saveMongoObject($oChatMessage);

            /*
             * Set the datetime for the last message sent on chat
             */
            $oDoctorChat->setLastMessageAt($oMessageSentAt);
            /*
             * Set the datetime for the last
             * active field on doctor chat member
             */
            $oDoctorChatMember->setLastActiveAt(new \DateTime());
            $oDoctorChatMember->setEmailNotificationSent(false);

            $this->saveObject($oDoctorChat);

            return $oChatMessage;
        }

        return false;
    }

    /**
     * Save an announcement message.
     *
     * @param string     $sAnnouncementType
     * @param DoctorChat $oChat
     * @param Doctors    $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return MongoMessage
     */
    public function saveAnnouncement($sAnnouncementType, $oChat, $oUser)
    {
        $oMessageSentAt = new \DateTime();

        /** @var MongoMessage $oChatMessage */
        $oChatMessage = $this->createObject(MongoMessage::class);
        $oChatMessage->setChatIdentifier($oChat->getIdentifier());
        $oChatMessage->setSendAt($oMessageSentAt);
        $oChatMessage->setChatAnnouncement(true);
        $oChatMessage->setChatAnnouncementType($sAnnouncementType);
        $oChatMessage->setChatAnnouncementUserIdentifier($oUser->getIdentifier());

        $this->saveMongoObject($oChatMessage);

        /*
         * Set the datetime for the last message sent on chat
         */
        $oChat->setLastMessageAt($oMessageSentAt);

        $this->saveObject($oChat);

        return $oChatMessage;
    }

    /**
     * @param MongoMessage $oMessage
     *
     * @return string
     */
    public function getMessageSentAtDateTime(MongoMessage $oMessage)
    {
        /** @var \DateTime $oSentAt */
        $oSentAt = $oMessage->getSendAt();
        $sSentAt = $oSentAt->format('d M Y, H:i');

        return $sSentAt;
    }

    /**
     * Add user to an existing chat thread.
     *
     * @param DoctorChat $oDoctorChat
     * @param integer    $iUserId
     *
     * @return DoctorChatMember
     */
    public function addConversationMember($oDoctorChat, $iUserId)
    {
        $oDoctor = $this->getRepository(Doctors::class)->find($iUserId);

        /** @var DoctorChatMember $oDoctorChatMember */
        $oDoctorChatMember = $this->createObject(DoctorChatMember::class);
        $oDoctorChatMember
            ->setDoctor($oDoctor)
            ->setDoctorChat($oDoctorChat);

        return $oDoctorChatMember;
    }

    /**
     * Update user that already belongs to the chat but was previously removed.
     *
     * @param DoctorChat $oDoctorChat
     * @param integer    $iUserId
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateConversationMember($oDoctorChat, $iUserId): void
    {
        /** @var $oDoctorChatMember $oDoctorChatMember */
        $oDoctorChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctorChat' => $oDoctorChat,
                'doctor'     => $iUserId,
            ]);

        if ($oDoctorChatMember->getLeftChat()) {
            $oDoctorChatMember->setLeftChat(false);
            $oDoctorChatMember->setLeftChatAt(null);
            $this->saveObject($oDoctorChatMember);
        }
    }

    /**
     * @param string       $sChatIdentifier
     * @param MongoMessage $oMongoMessage
     * @param string       $sSenderIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function sendNotificationForNewMessage(
        $sChatIdentifier,
        $oMongoMessage,
        $sSenderIdentifier
    ): void {
        $oChat = $this
            ->getRepository(DoctorChat::class)
            ->findOneBy([
                'identifier' => $sChatIdentifier,
            ]);

        /*
         * Refresh the persistent state of the Chat
         */
        try {
            $this->em->refresh($oChat);
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());
        }

        /** @var DoctorChatMember $oChatMember */
        foreach ($oChat->getDoctorChatMembers() as $oChatMember) {
            /*
             * Refresh the persistent state of the ChatMember
             */
            try {
                $this->em->refresh($oChatMember);
            } catch (\Exception $oException) {
                $this->logger->info($oException->getMessage());
            }

            if (!$oChatMember->getLeftChat()) {
                /** @var Doctors $oDoctor */
                $oDoctor                   = $oChatMember->getDoctor();
                $bMessageSentByCurrentUser = true;

                /*
                 * Set deletedChat flag to false in case
                 * any chat member deleted the conversation
                 */
                if ($oChatMember->getDeletedChat()) {
                    $oChatMember->setDeletedChat(false);
                }

                $oChatMember->setArchivedChat(false);
                $oChatMember->setArchivedChatAt();
                $this->saveObject($oChatMember);

                if ($sSenderIdentifier !== $oDoctor->getIdentifier()) {
                    $bMessageSentByCurrentUser = false;
                }

                if (($oChatMember->getDoctor()->getIdentifier() !== $oMongoMessage->getUserIdentifier())
                    && !$oChatMember->getEmailNotificationSent()
                ) {
                    $this->sendNotificationEmailForNewMessages($oMongoMessage, $oChatMember, $sSenderIdentifier);
                }

                $this->zmqPusher->push(
                    [
                        'sNotificationType'         => DoctorNotifications::TYPE_NEW_MESSAGE_SENT,
                        'sChatIdentifier'           => $sChatIdentifier,
                        'sMessageIdentifier'        => $oMongoMessage->getId(),
                        'bMessageSentByCurrentUser' => $bMessageSentByCurrentUser,
                        'userLeftChat'              => $oChatMember->getLeftChat(),
                    ],
                    'doctor_messenger_notification_topic',
                    [
                        'identifier' => $oChatMember->getDoctor()->getIdentifier(),
                    ]
                );
            }
        }
    }

    /**
     * @param array  $aOnlineUsersIds
     * @param string $sDoctorChatIdentifier
     * @param string $sSenderIdentifier
     */
    public function sendNotificationForOfflineUsers($aOnlineUsersIds, $sDoctorChatIdentifier, $sSenderIdentifier)
    {
        if (!empty($aOnlineUsersIds)) {
            /** @var DoctorChatMemberRepository $oDoctorChatMemberRepository */
            $oDoctorChatMemberRepository = $this->getRepository(DoctorChatMember::class);
            $aOfflineUsersIdentifiers    = $oDoctorChatMemberRepository->findOfflineUsersIdentifiers($aOnlineUsersIds, $sDoctorChatIdentifier);

            /** @var array $aUserIdentifier */
            foreach ($aOfflineUsersIdentifiers as $aUserIdentifier) {
                $sOfflineUserIdentifier = $aUserIdentifier['identifier'];

                $this->notificationManager->notify(
                    NotificationManager::NEW_MESSAGE_NOTIFICATION,
                    $sSenderIdentifier,
                    $sOfflineUserIdentifier,
                    $sDoctorChatIdentifier,
                    ObjectTypeConstants::OBJECT_DOCTOR_CHAT
                );
            }
        }
    }

    /**
     * @param DoctorChat $oChat
     */
    public function sendNotificationForNewGroupChatName(DoctorChat $oChat): void
    {
        /** @var DoctorChatMember $oChatMember */
        foreach ($oChat->getDoctorChatMembers() as $oChatMember) {
            if (!$oChatMember->getLeftChat()) {
                $this->zmqPusher->push(
                    [
                        'sNotificationType' => DoctorNotifications::TYPE_NEW_GROUP_CHAT_NAME,
                        'sChatIdentifier'   => $oChat->getIdentifier(),
                        'sChatName'         => $oChat->getName(),
                    ],
                    'doctor_messenger_notification_topic',
                    [
                        'identifier' => $oChatMember->getDoctor()->getIdentifier(),
                    ]
                );
            }
        }
    }

    /**
     * @param DoctorChat        s$oChat
     * @param string            $sUserIdentifier
     * @param string            $sAnnouncementType
     * @param MongoMessage|null $oMongoMessage
     */
    public function sendNotificationForUserRemoval(
        $oChat,
        $sUserIdentifier,
        $sAnnouncementType,
        $oMongoMessage
    ): void {
        /*
         * Send announcement message to all the members from the chat
         */
        $this->zmqPusher->push(
            [
                'eventType'         => DoctorNotifications::TYPE_NEW_MESSAGE_SENT,
                'isAnnouncement'    => true,
                'eventStatus'       => 'sent',
                'messageIdentifier' => $oMongoMessage->getId(),
            ],
            'doctor_chat_thread_topic',
            [
                'identifier' => $oChat->getIdentifier(),
            ]
        );

        $this->sendNotificationForNewMessage(
            $oChat->getIdentifier(),
            $oMongoMessage,
            $sUserIdentifier
        );

        /*
         * Notify that chat member that has left/has been removed from the conversation
         */
        $this->zmqPusher->push(
            [
                'sNotificationType' => DoctorNotifications::TYPE_USER_REMOVAL,
                'sAnnouncementType' => $sAnnouncementType,
                'sMessageId'        => $oMongoMessage->getId(),
                'sChatIdentifier'   => $oChat->getIdentifier(),
                'sUserIdentifier'   => $sUserIdentifier,
            ],
            'doctor_messenger_notification_topic',
            [
                'identifier' => $sUserIdentifier,
            ]
        );
    }

    /**
     * @param DoctorChat $oChat
     * @param array      $aMembersIdentifiers
     */
    public function sendNotificationForAddedUser(
        $oChat,
        $aMembersIdentifiers
    ): void {
        /*
         * Create new announcement message that someone joined the conversation
         * and broadcast that message to all conversation members
         */
        foreach ($aMembersIdentifiers as $sMemberIdentifier) {
            /** @var Doctors $oDoctor */
            $oDoctor = $this
                ->em
                ->getRepository(Doctors::class)
                ->findOneBy(
                    [
                        'identifier' => $sMemberIdentifier,
                    ]
                );

            /*
            * Notify that chat member that has been added to conversation
            */
            $this->zmqPusher->push(
                [
                    'sNotificationType' => DoctorNotifications::TYPE_USER_ADDED,
                    'sChatIdentifier'   => $oChat->getIdentifier(),
                ],
                'doctor_messenger_notification_topic',
                [
                    'identifier' => $oDoctor->getIdentifier(),
                ]
            );

            $oAnnouncementMessage = $this->saveAnnouncement(
                MongoMessage::ANNOUNCEMENT_TYPE_JOINED_CONVERSATION,
                $oChat,
                $oDoctor
            );

            $this->sendNotificationForNewMessage(
                $oChat->getIdentifier(),
                $oAnnouncementMessage,
                $oDoctor->getIdentifier()
            );

            /*
             * Send announcement message to all the members from the chat
             */
            $this->zmqPusher->push(
                [
                    'eventType'         => DoctorNotifications::TYPE_NEW_MESSAGE_SENT,
                    'isAnnouncement'    => true,
                    'eventStatus'       => 'sent',
                    'messageIdentifier' => $oAnnouncementMessage->getId(),
                ],
                'doctor_chat_thread_topic',
                [
                    'identifier' => $oChat->getIdentifier(),
                ]
            );
        }
    }

    /**
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserChats($sUserIdentifier)
    {
        /** @var DoctorRepository $oDoctorsRepository */
        $oDoctorsRepository = $this->em->getRepository(Doctors::class);
        /** @var DoctorChatRepository $oDoctorChatRepostiroy */
        $oDoctorChatRepository = $this->em->getRepository(DoctorChat::class);

        /** @var Doctors $oUser */
        $oUser        = $oDoctorsRepository->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);
        $aDoctorChats = $oDoctorChatRepository->findAllByUser($oUser);

        /** @var DoctorChat $oDoctorChat */
        foreach ($aDoctorChats as $oDoctorChat) {
            /** @var DoctorChatMember $oDoctorChatMember */
            foreach ($oDoctorChat->getDoctorChatMembers() as $oDoctorChatMember) {
                if ($oDoctorChatMember->getDoctor() !== $oUser) {
                    $this->sendNotificationForUserDeletingAccount($oDoctorChat, $oDoctorChatMember);
                    $this->updatePeerDeletedAccountForSingleChats($oDoctorChat, $oDoctorChatMember);
                }

                $this->moveChatFilesToGhostUser($oDoctorChatMember, $oUser);
                $this->changeDoctorChatMemberToGhostUser($oDoctorChatMember, $oUser);
            }
            $this->randomlySetGroupChatAdmin($oDoctorChat, $oUser);
        }
    }

    /**
     * Unset the admin role if the user leaves
     *
     * @param DoctorChat $oDoctorChat
     * @param Doctors    $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function randomlySetGroupChatAdmin($oDoctorChat, $oUser)
    {
        /** @var DoctorChatMemberRepository $oDoctorChatMemberRepository */
        $oDoctorChatMemberRepository  = $this->getRepository(DoctorChatMember::class);
        $aPossibleChatAdmins          = [];
        $iMembersLeftButNotDeletedIds = 0;
        $iGroupChatAdmins             = $oDoctorChatMemberRepository
            ->getNumberOfAdmins($oDoctorChat->getId(), $oUser->getId());

        // If one admin is found nothing to do here
        if ($iGroupChatAdmins > 0) {
            return;
        }

        /** @var $oDoctorChatMember $oDoctorChatMember */
        foreach ($oDoctorChat->getDoctorChatMembers() as $oDoctorChatMember) {
            /*
             * Consider a valid admin a member that didn't left or deleted the chat
             * or deleted the associated user account.
             */
            if ($oDoctorChatMember->getDoctor() !== $oUser
                && !$oDoctorChatMember->getLeftChat()
                && !$oDoctorChatMember->getDoctorAccountDeleted()
                && !$oDoctorChatMember->getDeletedChat()
            ) {
                array_push($aPossibleChatAdmins, $oDoctorChatMember);
            }

            /*
             * Check for users that left the chat but didn't delete it.
             * We have to keep the chat in this situation.
             */
            if ($oDoctorChatMember->getDoctor() !== $oUser
                && $oDoctorChatMember->getLeftChat()
                && !$oDoctorChatMember->getDeletedChat()
            ) {
                $iMembersLeftButNotDeletedIds += 1;
            }
        }

        if (0 === sizeof($aPossibleChatAdmins) && 0 === $iMembersLeftButNotDeletedIds) {
            $this->removeDoctorChat($oDoctorChat);
        } elseif (sizeof($aPossibleChatAdmins)) {
            $iRandomUserIndex = rand(0, sizeof($aPossibleChatAdmins) - 1);
            /** @var DoctorChatMember $oChatMember */
            $oChatMember = $aPossibleChatAdmins[$iRandomUserIndex];
            $oChatMember->setIsGroupAdmin(true);
            $this->saveObject($oChatMember);
        }

        return;
    }

    /**
     * @param DoctorChat       $oDoctorChat
     * @param DoctorChatMember $oDoctorChatMember
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updatePeerDeletedAccountForSingleChats($oDoctorChat, $oDoctorChatMember)
    {
        if (!$oDoctorChat->getIsGroup()) {
            $oDoctorChatMember->setPeerDeletedAccount(true);
            $this->saveObject($oDoctorChatMember);
        }
    }

    /**
     * @param DoctorChat       $oDoctorChat
     * @param DoctorChatMember $oDoctorChatMember
     */
    protected function sendNotificationForUserDeletingAccount($oDoctorChat, $oDoctorChatMember)
    {
        $aData = [
            'sNotificationType' => DoctorNotifications::TYPE_USER_DELETED_ACCOUNT,
            'isAnnouncement'    => false,
            'sChatIdentifier'   => $oDoctorChat->getIdentifier(),
        ];

        if (!$oDoctorChat->getIsGroup()) {
            try {
                /*
                 * Save an announcement message to the chat.
                 */
                $oMongoMessage = $this->saveAnnouncement(
                    MongoMessage::ANNOUNCEMENT_TYPE_USER_DELETED_ACCOUNT,
                    $oDoctorChat,
                    $oDoctorChatMember->getDoctor()
                );

                $aData = array_merge($aData, [
                    'isAnnouncement'    => true,
                    'sAnnouncementType' => MongoMessage::ANNOUNCEMENT_TYPE_USER_DELETED_ACCOUNT,
                    'sMessageId'        => $oMongoMessage->getId(),
                ]);
            } catch (\Exception $oException) {
                $this->logger->log('error', $oException->getMessage());
            }
        }

        /*
         * Send announcement message to all the members from the chat
         * Send also a new message to peer user if chat is not group.
         */
        $this->zmqPusher->push(
            $aData,
            'doctor_messenger_notification_topic',
            [
                'identifier' => $oDoctorChatMember->getDoctor()->getIdentifier(),
            ]
        );
    }

    /**
     * @param DoctorChatMember $oDoctorChatMember
     * @param Doctors          $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function changeDoctorChatMemberToGhostUser($oDoctorChatMember, $oUser)
    {
        /** @var DoctorRepository $oDoctorsRepository */
        $oDoctorsRepository = $this->em->getRepository(Doctors::class);

        if ($oDoctorChatMember->getDoctor() === $oUser) {
            /** @var Doctors $oGhostUser */
            $oGhostUser = $oDoctorsRepository->findOneBy([
                'isGhostUser' => true,
                'enabled'     => true,
            ]);

            $oDoctorChatMember->setDoctor($oGhostUser);
            $oDoctorChatMember->setDoctorAccountDeleted(true);
            $oDoctorChatMember->setPreviousDoctorIdentifier($oUser->getIdentifier());
            $oDoctorChatMember->setIsGroupAdmin(false);

            $this->saveObject($oDoctorChatMember);
        }
    }

    /**
     * @param DoctorChatMember $oDoctorChatMember
     * @param Doctors          $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function moveChatFilesToGhostUser($oDoctorChatMember, $oUser)
    {
        /** @var DoctorChatFileRepository $oDoctorChatFilesRepository */
        $oDoctorChatFilesRepository = $this->em->getRepository(DoctorChatFile::class);
        /** @var DoctorRepository $oDoctorsRepository */
        $oDoctorsRepository = $this->em->getRepository(Doctors::class);

        if ($oDoctorChatMember->getDoctor() === $oUser) {
            /** @var Doctors $oGhostUser */
            $oGhostUser = $oDoctorsRepository->findOneBy([
                'isGhostUser' => true,
                'enabled'     => true,
            ]);
            $oQuery     = $oDoctorChatFilesRepository->findByDoctor($oUser);

            $aIterableResults = $oQuery->iterate();
            $i                = 0;

            foreach ($aIterableResults as $oResult) {
                /** @var DoctorChatFile $oChatFile */
                $oChatFile = $oResult[0];
                $oChatFile->setDoctorUploader($oGhostUser);
                $this->em->persist($oChatFile);

                if (0 === ($i % AppConstants::BATCH_SIZE)) {
                    $this->em->flush();
                }

                $i++;
            }
            //flush the rest of the entries outside the batch
            $this->em->flush();
        }
    }

    /**
     * @param DoctorChat       $oDoctorChat
     * @param DoctorChatMember $oDoctorChatMember
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return boolean
     */
    public function permanentlyDeleteChat($oDoctorChat, $oDoctorChatMember)
    {
        /** @var DoctorChatMemberRepository $oDoctorChatMemberRepository */
        $oDoctorChatMemberRepository = $this->getRepository(DoctorChatMember::class);

        if ($oDoctorChat->getIsGroup()) {
            $aDoctorChatMembers = $oDoctorChatMemberRepository->findByDeletedChatAndDeletedAccount($oDoctorChat->getId());

            if (1 === sizeof($aDoctorChatMembers) && $aDoctorChatMembers[0] === $oDoctorChatMember) {
                $this->removeDoctorChat($oDoctorChat);

                return true;
            }

        } else {
            if ($oDoctorChatMember->getPeerDeletedAccount()) {
                $this->removeDoctorChat($oDoctorChat);

                return true;
            }
        }

        return false;
    }

    /**
     * @param DoctorChat $oDoctorChat
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function removeDoctorChat($oDoctorChat)
    {
        /** @var MongoMessageRepository $oMessageRepository */
        $oMessageRepository = $this->getMongoRepository(MongoMessage::class);

        foreach ($oDoctorChat->getDoctorChatFiles() as $oDoctorChatFile) {
            $this->documentUploader->delete(sprintf('%s/%s', AppConstants::AMAZON_CHATS_FILES_DIRECTORY, $oDoctorChatFile->getName()));
        }

        $this->em->remove($oDoctorChat);
        $this->em->flush();
        $oMessageRepository->removeMessagesByChat($oDoctorChat->getIdentifier());
    }

    /**
     * @param MongoMessage     $oMongoMessage
     * @param DoctorChatMember $oChatMember
     */
    protected function sendNotificationEmailForNewMessages($oMongoMessage, $oChatMember, $sSenderIdentifier)
    {
        $bSendEmail = true;

        if ($oChatMember->getLastActiveAt()) {
            /** @var \DateInterval $oDateInterval */
            $iSendAtTimestamp       = $oMongoMessage->getSendAt()->getTimestamp();
            $iLastActiveAtTimestamp = $oChatMember->getLastActiveAt()->getTimestamp();
            $iDateIntervalSeconds   = $iSendAtTimestamp - $iLastActiveAtTimestamp;

            if (AppConstants::NEW_MESSAGE_EMAIL_NOTIFICATION_TIME > $iDateIntervalSeconds) {
                $bSendEmail = false;
            }
        }

        if ($bSendEmail) {
            $sInboxLink = $this->router->generate(
                'messaging_thread_overview',
                [
                    'sIdentifier' => $oChatMember->getDoctorChat()->getIdentifier(),
                ],
                Router::ABSOLUTE_URL
            );

            $aSender     = $this->getOrRegenerateRedisUserProfile($sSenderIdentifier, $this, $this->em);
            $sSenderName = sprintf('%s %s %s', $aSender['academicTitle'], $aSender['firstName'], $aSender['lastName']);

            // Prepare user email
            $aMailParameters = [
                'firstName'  => $oChatMember->getDoctor()->getDoctorProfile()->getFirstName(),
                'senderName' => $sSenderName,
                'inboxLink'  => $sInboxLink,
            ];

            $aSubjectParameters = [
                'senderName' => $sSenderName,
            ];

            // Send email
            $this->mailerAdapter->send(
                MailsConstants::NEW_MESSAGE_NOTIFICATION,
                $oChatMember->getDoctor(),
                $aMailParameters,
                $aSubjectParameters
            );

            $oChatMember->setEmailNotificationSent(true);
            $this->saveObject($oChatMember);
        }
    }

    /**
     * Associate each attachment with the message
     *
     * @param array  $aAttachmentsIdentifiers
     * @param string $sMessageId
     *
     * @return array
     */
    public function associateAttachmentsToMessage(array $aAttachmentsIdentifiers, $sMessageId)
    {
        $aAttachments = [];

        foreach ($aAttachmentsIdentifiers as $attachmentsIdentifier) {
            $oDoctorChatFile = $this
                ->getRepository(DoctorChatFile::class)
                ->findOneBy([
                    'identifier' => $attachmentsIdentifier,
                ]);

            $oDoctorChatFile->setMessageId($sMessageId);
            $oDoctorChatFile->setUnprocessedUpload(true);
            $this->saveObject($oDoctorChatFile);

            $aAttachment    = [
                'name'         => $oDoctorChatFile->getName(),
                'originalName' => $oDoctorChatFile->getOriginalFileName(),
            ];
            $aAttachments[] = $aAttachment;
        }

        return $aAttachments;
    }

    /**
     * @param DoctorChat $oChat
     * @param Doctors    $oUser
     *
     * @return string
     */
    public function getChatName(DoctorChat $oChat, Doctors $oUser)
    {
        /** @var DoctorChatRepository $oDoctorChatRepository */
        $oDoctorChatRepository = $this->getRepository(DoctorChat::class);
        /** @var DoctorChatMemberRepository $oDoctorChatMemberRepository */
        $oDoctorChatMemberRepository = $this->getRepository(DoctorChatMember::class);
        $sChatName                   = $oChat->getName();

        if (!isset($sChatName)) {
            /** @var DoctorChatMember $oDoctorChatMember */
            $oDoctorChatMember     = $oDoctorChatMemberRepository
                ->findOneBy([
                    'doctorChat' => $oChat,
                    'doctor'     => $oUser,
                ]);
            $aNumberOfChatsMembers = $oDoctorChatRepository
                ->findNumberOfChatsMembers(
                    [$oChat->getId()],
                    $oChat->getIsGroup(),
                    true
                );
            $aNumberOfChatMembers  = $aNumberOfChatsMembers[0];

            $sChatName = '';
            /** @var DoctorChatMember $oChatMember */
            foreach ($oChat->getDoctorChatMembers() as $oChatMember) {
                if ($oChatMember->getDoctor() !== $oUser && !$oChatMember->getLeftChat()) {
                    $sChatName = $oChatMember->getDoctor()->getDoctorProfile()->getFullName();
                    break;
                }
            }

            /*
             * Add other members as a total number in the chat name
             */
            $iNumberOfChatMembers = intval($aNumberOfChatMembers['numberOfChatMembers']);
            $sChatName            = $this->changeChatName($sChatName, $iNumberOfChatMembers, $oDoctorChatMember->getLeftChat());
        }

        return $sChatName;
    }

    /**
     * @param string $sChatName
     * @param int    $iNumberOfChatMembers
     * @param bool   $bHasLeft
     *
     * @return string
     */
    public function changeChatName($sChatName, $iNumberOfChatMembers, $bHasLeft)
    {
        if (($iNumberOfChatMembers - 2 > 0 && !$bHasLeft) || ($iNumberOfChatMembers - 2 >= 0 && $bHasLeft)) {
            $iNumberOfChatMembers -= 1;
            $sChatName            = sprintf(
                '%s %s',
                $sChatName,
                $this->translator->trans(
                    'doctors.messenger.more.members',
                    [
                        '%members%' => $iNumberOfChatMembers,
                    ]
                )
            );
        }

        return $sChatName;
    }

    /**
     * @param DoctorChat $oChat
     * @param Doctors    $oUser
     * @param string     $sChatName
     *
     * @return boolean
     */
    public function setChatName(DoctorChat $oChat, Doctors $oUser, $sChatName)
    {
        $oChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctor'     => $oUser,
                'doctorChat' => $oChat,
            ]);

        if (!$oChatMember || !$oChatMember->getIsGroupAdmin()) {
            return false;
        }

        $sChatName = trim($sChatName);
        $sChatName = htmlspecialchars($sChatName);

        if (!$sChatName) {
            return false;
        }

        $oChat->setName($sChatName);
        $this->saveObject($oChat);
        $this->sendNotificationForNewGroupChatName($oChat);

        return true;
    }

    /**
     * Second parameter for this function, $peerUserIdOrIdentifier,
     * could be an User Id or an User Identifier because Select2
     * plugin return users ids and into the application is used
     * user identifier in order to unique identify an user.
     *
     * @param Doctors $oCurrentUser
     * @param string  $sPeerUserIdentifier
     *
     * @return bool|string
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function start1To1Conversation($oCurrentUser, $sPeerUserIdentifier)
    {
        /** @var DoctorRepository $oDoctorsRepository */
        $oDoctorRepository = $this->getRepository(Doctors::class);
        /** @var DoctorChatRepository $oDoctorChatRepository */
        $oDoctorChatRepository = $this->getRepository(DoctorChat::class);

        $oPeerUser = $oDoctorRepository
            ->findOneBy([
                'identifier' => $sPeerUserIdentifier,
            ]);

        if (!$oPeerUser) {
            return false;
        }

        /** @var DoctorChat $aChat */
        $aChat = $oDoctorChatRepository
            ->findChatsByParticipants(
                $oCurrentUser->getId(),
                $oPeerUser->getId(),
                false,
                true
            );

        if (sizeof($aChat)) {
            $this->unarchiveConvForConvMember($oCurrentUser->getId(), $aChat[0]['chat_id']);
        }

        $sConversationIdentifier = null;
        $aConversationMembers    = [$oCurrentUser, $oPeerUser];

        if (empty($aChat)) {
            $sConversationIdentifier = $this->createConversation($oCurrentUser, $aConversationMembers);
        } else {
            $sConversationIdentifier = $this->updateConversation($aChat[0]);
        }

        return $sConversationIdentifier;
    }

    /**
     * Start a group conversation
     *
     * @param Doctors $oCurrentUser
     * @param array   $aConversationMembers
     *
     * @return bool|mixed|null
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function startGroupConversation($oCurrentUser, $aConversationMembers)
    {
        /** @var DoctorChatRepository $oDoctorChatRepository */
        $oDoctorChatRepository = $this->getRepository(DoctorChat::class);
        /** @var DoctorRepository $oDoctorRepository */
        $oDoctorRepository = $this->getRepository(Doctors::class);

        $aConversationMembersIdentifiers = [];
        foreach ($aConversationMembers as $aConversationMember) {
            $aConversationMembersIdentifiers[] = $aConversationMember['identifier'];
        }

        $aConversationMembers = $oDoctorRepository->findBy(['identifier' => $aConversationMembersIdentifiers]);

        if ($aConversationMembers) {
            $oConversationMember     = $aConversationMembers[0];
            $aChats                  = $oDoctorChatRepository
                ->findChatsByParticipants(
                    $oCurrentUser->getId(),
                    $oConversationMember->getId(),
                    true,
                    true
                );
            $aConversationMembers[]  = $oCurrentUser;
            $sConversationIdentifier = null;

            if (empty($aChats)) {
                $sConversationIdentifier = $this->createConversation($oCurrentUser, $aConversationMembers);

                return $sConversationIdentifier;
            }

            foreach ($aChats as $aChat) {
                /** @var DoctorChat $oDoctorChat */
                $oDoctorChat        = $oDoctorChatRepository->findOneBy(['id' => $aChat['chat_id']]);
                $aDoctorChatMembers = $oDoctorChat->getDoctorChatMembers();

                $aDoctors = [];
                foreach ($aDoctorChatMembers as $oDoctorChatMember) {
                    $aDoctors[] = $oDoctorChatMember->getDoctor();
                }

                $aConversationMembersDiff = array_diff($aDoctors, $aConversationMembers);

                if (empty($aConversationMembersDiff)) {
                    $sConversationIdentifier = $this->updateConversation($aChat);
                    break;
                }
            }

            if (!$sConversationIdentifier) {
                $sConversationIdentifier = $this->createConversation($oCurrentUser, $aConversationMembers);
            }

            return $sConversationIdentifier ? $sConversationIdentifier : false;
        }

        return false;
    }

    /**
     * Create a new conversation
     *
     * @param Doctors $oCurrentUser
     * @param array   $aConversationMembers
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function createConversation($oCurrentUser, $aConversationMembers)
    {
        /** @var DoctorChat $oDoctorChat */
        $oDoctorChat = $this->createObject(DoctorChat::class);
        $oDoctorChat->setLastMessageAt(new \DateTime());

        foreach ($aConversationMembers as $oConversationMember) {
            /** @var DoctorChatMember $oDoctorChatMember */
            $oDoctorChatMember = $this->createObject(DoctorChatMember::class);
            $oDoctorChatMember
                ->setDoctor($oConversationMember)
                ->setDoctorChat($oDoctorChat);

            if ($oCurrentUser === $oConversationMember) {
                $oDoctorChatMember->setIsGroupAdmin(true);
            }

            $oDoctorChat->addDoctorChatMember($oDoctorChatMember);
        }

        if (sizeof($aConversationMembers) > 2) {
            $oDoctorChat->setIsGroup(true);
        }

        $this->saveObject($oDoctorChat);

        return $oDoctorChat->getIdentifier();
    }

    /**
     * Update existent conversation
     *
     * @param array $aChat
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateConversation($aChat)
    {
        /** @var DoctorChat $oChat */
        $aDoctorChatMembers = $this
            ->getRepository(DoctorChatMember::class)
            ->findBy([
                'doctorChat' => $aChat['chat_id'],
            ]);

        /*
         * Set deletedChat flag to false
         * for those users that didn't leave the chat
         */
        if ($aDoctorChatMembers) {
            foreach ($aDoctorChatMembers as $oDoctorChatMember) {
                if (!$oDoctorChatMember->getLeftChat()) {
                    $oDoctorChatMember->setDeletedChat(false);
                    $this->saveObject($oDoctorChatMember);
                }
            }
        }

        return $aChat['chat_identifier'];
    }

    /**
     * Set a chat as unarchived if is already archived.
     *
     * @param int $iUserId
     * @param int $iChatId
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function unarchiveConvForConvMember($iUserId, $iChatId)
    {
        /** @var DoctorChatMember $oDoctorChatMember */
        $oDoctorChatMember = $this
            ->getRepository(DoctorChatMember::class)
            ->findOneBy([
                'doctor'     => $iUserId,
                'doctorChat' => $iChatId,
            ]);

        if ($oDoctorChatMember) {
            $oDoctorChatMember->setArchivedChat(false);
            $oDoctorChatMember->setArchivedChatAt();
            $this->saveObject($oDoctorChatMember);
        }

        return;
    }
}
