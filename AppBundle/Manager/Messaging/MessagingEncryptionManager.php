<?php

namespace Flendoc\AppBundle\Manager\Messaging;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Document\MongoMessage;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class MessagingManager
 * @package Flendoc\AppBundle\Manager\Messaging
 */
class MessagingEncryptionManager extends FlendocManager
{
    /** @var EntityManager */
    protected $em;

    /**
     * MessagingEncryptionManager constructor.
     *
     * @param EntityManager   $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param string    $sPlaintext
     * @param \DateTime $oMessageCreatedAt
     * @param string    $sUserIdentifier
     * @param string    $sChatIdentifier
     *
     * @return bool|array
     */
    public function encryptMessage(
        $sPlaintext,
        \DateTime $oMessageCreatedAt,
        $sUserIdentifier,
        $sChatIdentifier
    ) {
        $cipher = "aes-128-gcm";
        $iv = substr($sUserIdentifier, 16);

        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $iDate = $oMessageCreatedAt->getTimestamp();
            $key = $iDate.$sChatIdentifier.$sUserIdentifier;
            $sCiphertext = openssl_encrypt($sPlaintext, $cipher, $key, $options=0, $iv, $tag);

            $aResponse = [
                'ciphertext' => $sCiphertext,
                'tag'        => utf8_encode($tag),
            ];
            return $aResponse;
        }

        return false;
    }

    /**
     * @param MongoMessage $oMessage
     *
     * @return bool|string
     */
    public function decryptMessage(MongoMessage $oMessage)
    {
        $sCiphertext = $oMessage->getMessage();
        $sChatIdentifier = $oMessage->getChatIdentifier();
        $sUserIdentifier = $oMessage->getUserIdentifier();
        $tag = $oMessage->getTag();
        $cipher = "aes-128-gcm";
        $iv = substr($sUserIdentifier, 16);

        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $iDate = $oMessage->getSendAt()->getTimestamp();
            $key = $iDate.$sChatIdentifier.$sUserIdentifier;
            $tag = utf8_decode($tag);
            $sOriginalPlaintext = openssl_decrypt($sCiphertext, $cipher, $key, $options=0, $iv, $tag);

            return $sOriginalPlaintext;
        }

        return false;
    }
}
