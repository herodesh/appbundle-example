<?php

namespace Flendoc\AppBundle\Manager\Institutions;

use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Entity\Institutions\InstitutionTypeLanguages;
use Flendoc\AppBundle\Entity\Institutions\InstitutionTypes;
use Flendoc\AppBundle\Traits\Institutions\InstitutionTypesTrait;

/**
 * Class InstitutionTypeManager
 * @package Flendoc\AppBundle\Manager\Institutions
 */
class InstitutionTypeManager extends FlendocManager
{
    use InstitutionTypesTrait;

    /**
     * Create institutionType object from trait data
     *
     * @param array $aInstitutionTypes
     *
     * @return mixed
     */
    public function createInstitutionTypeFromTrait(array $aInstitutionTypes)
    {
        $oInstitutionType = new InstitutionTypes();
        $oInstitutionType->setId($aInstitutionTypes['id']);

        foreach ($aInstitutionTypes['content'] as $sKey => $aContent) {
            $oInstitutionTypeLanguages = new InstitutionTypeLanguages();
            $oInstitutionTypeLanguages->setInstitutionTypes($oInstitutionType);
            $oInstitutionTypeLanguages->setLanguages($this->getRepository('AppBundle:Languages\Languages')
                                                          ->findOneByIso($sKey));

            $oInstitutionTypeLanguages->setName($aContent);

            $oInstitutionType->addInstitutionTypeLanguage($oInstitutionTypeLanguages);
        }

        return $oInstitutionType;
    }

    /**
     * Save an array of InstitutionType objects
     *
     * @param array $aInstitutionTypes
     */
    public function saveInstitutionTypesArray(array $aInstitutionTypes)
    {
        foreach ($aInstitutionTypes as $oInstitutionType) {
            $this->em->persist($oInstitutionType);
        }

        $this->em->flush();
    }

}
