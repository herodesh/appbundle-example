<?php

namespace Flendoc\AppBundle\Manager\Institutions;

use Flendoc\AppBundle\Entity\Institutions\InstitutionLanguages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Entity\Institutions\InstitutionImages;
use Flendoc\AppBundle\Entity\Institutions\Institutions;
use Flendoc\AppBundle\Uploader\PhotoUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class InstitutionManager
 * @package Flendoc\AppBundle\Manager\Institutions
 */
class InstitutionManager extends FlendocManager
{
    /**
     * @var PhotoUploader
     */
    protected $photoUploader;

    /**
     * InstitutionManager constructor.
     *
     * @param PhotoUploader $photoUploader
     */
    public function __construct(PhotoUploader $photoUploader)
    {
        $this->photoUploader = $photoUploader;
    }

    /**
     * @param InstitutionLanguages $oInstitutionLanguage
     * @param string               $sInstitutionIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveInstitutionLanguage(InstitutionLanguages $oInstitutionLanguage, $sInstitutionIdentifier): void
    {
        $oInstitution = $this->getRepository('AppBundle:Institutions\Institutions')
                             ->findOneByIdentifier($sInstitutionIdentifier);
        $oInstitutionLanguage->setInstitutions($oInstitution);

        $this->saveObject($oInstitutionLanguage);
    }

    /**
     * @TODO rebuild/refactor
     *
     * Save institution Image
     *
     * @param Request $oRequest
     * @param string  $institutionIdentifier
     * @param string  $sInstitutionImages
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveInstitutionImage(Request $oRequest, $institutionIdentifier, $sInstitutionImages): bool
    {
        if ($this->photoUploader->processChunksLocally($oRequest)) {
            if ($aMergedResult = $this->photoUploader->mergeChunks($oRequest)) {

                $oUploadedFile = $this->photoUploader->uploadedFileInstance($aMergedResult['fileDirectory'].$aMergedResult['fileName'], $aMergedResult['fileName']);
                $aResponse     = $this->photoUploader->upload($oUploadedFile, $sInstitutionImages);
                $oInstitution  = $this->getRepository('AppBundle:Institutions\Institutions')
                                      ->findOneByIdentifier($institutionIdentifier);

                $oInstitutionImage = new InstitutionImages();
                $oInstitutionImage->setImage($aResponse['image']);
                $oInstitutionImage->setThumbnail($aResponse['thumbnail']);
                $oInstitutionImage->setInstitution($oInstitution);

                $this->saveObject($oInstitutionImage);

                return true;
            }
        }

        return false;
    }

    /**
     * Delete Institution Image
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteInstitutionImage($id)
    {
        //@TODO Check what is happening here. Should be probably passing the object from controller?
        $oInstitutionImage = $this->em->getRepository('AppBundle:Institutions\InstitutionImages')->find(['id' => $id]);

        $bDeleteImage = $this->photoUploader->delete($oInstitutionImage->getImage());
        $bDeleteThumb = $this->photoUploader->delete($oInstitutionImage->getThumbnail());

        if ($bDeleteImage && $bDeleteThumb) {

            $this->deleteObject($oInstitutionImage);

            return true;
        }

        return false;
    }

    /**
     * @TODO - Create deactivate institutions
     *
     * @param Request      $oRequest
     * @param Institutions $oInstitutions
     */
    public function deactivateInstitution(Request $oRequest, Institutions $oInstitutions)
    {

    }

    /**
     * @TODO - Create permanently delete institutions
     *
     * @param Request      $oRequest
     * @param Institutions $oInstitutions
     */
    public function permanentlyDeleteInstitution(Request $oRequest, Institutions $oInstitutions)
    {

    }
}
