<?php

namespace Flendoc\AppBundle\Manager\Skills;

use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class SkillsManager
 * @package Flendoc\AppBundle\Manager\Skills
 */
class SkillsManager extends FlendocManager
{
    /**
     * Saves skills to DB and to neo4j
     *
     * @param Skills $oSkills
     */
    public function saveSkills(Skills $oSkills): void
    {
        //save to the DB
        $this->saveObject($oSkills);
        //create or update in Neo4j
        $this->neo4JManager->client->run('
            MERGE (s:Skills {identifier: "'.$oSkills->getIdentifier().'"})
            SET s+={name: "'.$oSkills->getDisplayDefaultSkillName().'"}
            RETURN s;
        ');
    }

    /**
     * Delete a skill from DB and delete and detach his connections it from Neo4J
     *
     * @param Skills $oSkills
     */
    public function deleteSkills(Skills $oSkills)
    {
        //delete from DB
        $this->deleteObject($oSkills);
        //delete from Neo4J
        $this->neo4JManager->client->run('
            MATCH (s:Skills {identifier: "'.$oSkills->getIdentifier().'"})
            DETACH DELETE s
        ');
    }
}
