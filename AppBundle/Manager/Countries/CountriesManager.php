<?php

namespace Flendoc\AppBundle\Manager\Countries;

use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Entity\Countries\Countries;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CountriesManager
 * @package Flendoc\AppBundle\Manager\Countries
 */
class CountriesManager extends FlendocManager
{

    /**
     * Save Country
     *
     * @param Request   $oRequest
     * @param Countries $oCountries
     * @param bool      $bWithRemove
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveCountry(Request $oRequest, Countries $oCountries, $bWithRemove = false): void
    {
        //save to DB
        $this->saveObject($oCountries);
        //update or create entry in Neo4J
        $this->neo4JManager->client->run('
            MERGE (c:Country {identifier: "'.$oCountries->getIdentifier().'"})
            SET c+={name: "'.$oCountries->getCountryDefaultName().'"}
            RETURN c;
        ');
    }

}
