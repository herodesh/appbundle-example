<?php

namespace Flendoc\AppBundle\Manager\MongoThreadManager;

use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Comments\MongoVote;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class CommentsManager
 * @package Flendoc\AppBundle\Manager\Likes
 */
class ThreadManager extends FlendocManager
{

    /**
     * @param MongoThread $oThread
     * @param string      $sUserIdentifier
     *
     * @return string
     */
    public function deleteThread(MongoThread $oThread, $sUserIdentifier)
    {
        $aComments = $this->getMongoRepository(MongoComment::class)->findBy([
            'thread' => $oThread,
            'userIdentifier' => $sUserIdentifier
        ]);

        //delete comments
        foreach($aComments as $oComment) {
            $this->getMongoRepository(MongoVote::class)->findAndRemoveVotesByComment(
                $oComment, $sUserIdentifier
            );

            $this->odm->remove($oComment);
        }

        //remove comments related to threads to be deleted
        $this->getMongoRepository(MongoComment::class)->findAndDeleteCommentsByThread(
            $oThread, $sUserIdentifier
        );

        //remove likes from thread
        $this->getMongoRepository(MongoVote::class)->findAndRemoveVotesByThread(
            $oThread, $sUserIdentifier
        );

        //remove thread
        $this->deleteMongoObject($oThread);
    }
}
