<?php

namespace Flendoc\AppBundle\Manager\Comments;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Manager\Notifications\NotificationManager;
use Flendoc\AppBundle\Repository\Comments\MongoCommentRepository;
use Flendoc\AppBundle\Repository\Comments\MongoThreadRepository;
use Flendoc\AppBundle\Repository\Feeds\MongoFeedRepository;
use Flendoc\AppBundle\Traits\App\ObjectKeyEntityTrait;

/**
 * Class CommentsManager
 * @package Flendoc\AppBundle\Manager\Comments
 */
class CommentsManager extends FlendocManager
{
    use ObjectKeyEntityTrait;

    /** @var NotificationManager */
    protected $notificationManager;

    /**
     * CommentsManager constructor.
     *
     * @param NotificationManager $oNotificationManager
     */
    public function __construct(NotificationManager $oNotificationManager)
    {
        $this->notificationManager  = $oNotificationManager;
    }

    const COMMENT_TYPE_FIRST_LEVEL  = 'comment';
    const COMMENT_TYPE_SECOND_LEVEL = 'reply';
    const LIKE_TYPE_THREAD = 'thread';
    const LIKE_TYPE_COMMENT = 'comment';
    const EVENT_NEW_COMMENT = 'new-comment';
    const EVENT_LIKE = 'like';
    const EVENT_EDIT_COMMENT = 'edit-comment';
    const EVENT_DELETE_COMMENT = 'delete-comment';

    /**
     * @param string $sEntityIdentifier
     * @param string $sUserIdentifier
     * @param string $sMessage
     * @param string $bIsReply
     * @param string $sEntityKey
     * @param string $sParentIdentifier
     *
     * @return boolean|string
     */
    public function addMongoComment(
        $sUserIdentifier,
        $sEntityIdentifier,
        $bIsReply,
        $sMessage,
        $sEntityKey,
        $sParentIdentifier = null
    ) {
        /** @var MongoCommentRepository $oMongoCommentRepository */
        $oMongoCommentRepository = $this->odm->getRepository(MongoComment::class);
        /** @var DocumentManager|EntityManager $oManager */
        $oManager = is_subclass_of(self::$aKeyToEntityTypes[$sEntityKey], AbstractEntity::class) ?
            $this->getEntityManager() :
            $this->getMongoDbManager()
        ;

        /**
         * Add a new comment
         * @var MedicalCases|MongoFeed $oDependencyObject
         */
        $oDependencyObject = $oManager
            ->getRepository(self::$aKeyToEntityTypes[$sEntityKey])
            ->findOneBy([
                'identifier' => $sEntityIdentifier
            ])
        ;

        if (!$oDependencyObject) {
            return false;
        }

        $sMongoThreadUserIdentifier = $this->getMongoThreadUserIdentifier($oDependencyObject);

        /*
         * Check if any MongoThread with this
         * sEntityIdentifier was created before.
         */
        $oMongoThread = $this->addMongoThread(
            $sEntityKey,
            $sEntityIdentifier,
            $sMongoThreadUserIdentifier
        );

        /** @var MongoComment $oMongoComment */
        $oMongoComment = $this->createObject(MongoComment::class);
        $oMongoComment->setUserIdentifier($sUserIdentifier);
        $oMongoComment->setBody($sMessage);
        $oMongoComment->setIsReply($bIsReply);
        $oMongoComment->setThread($oMongoThread);

        if ($sParentIdentifier) {
            /** @var MongoComment $oParentMongoComment */
            $oParentMongoComment = $oMongoCommentRepository
                ->findOneBy([
                    'identifier' => $sParentIdentifier,
                ])
            ;
            $oMongoComment->setParent($oParentMongoComment);
        }

        $this->odm->persist($oMongoComment);

        // Update lastCommentAt field from MongoThread
        $oMongoThread->setLastCommentAt($oMongoComment->getCreatedAt());
        $this->odm->persist($oMongoThread);

        $this->odm->flush();

        /*
         * Notify users that a new comment has been added
         */
        $this->notificationManager->notify(
            NotificationManager::NEW_COMMENT_NOTIFICATION,
            $sUserIdentifier,
            null,
            $sEntityIdentifier,
            $sEntityKey,
            $oMongoComment->getIdentifier(),
            $sParentIdentifier
        );

        return $oMongoComment->getIdentifier();
    }

    /**
     * @param string $sDependencyType
     * @param string $sDependencyIdentifier
     * @param string $sUserIdentifier
     * @param bool   $bIsCommentable
     *
     * @return MongoThread
     */
    public function addMongoThread($sDependencyType, $sDependencyIdentifier, $sUserIdentifier, $bIsCommentable = true)
    {
        /** @var MongoThreadRepository $oMongoThreadRepository */
        $oMongoThreadRepository = $this->odm->getRepository(MongoThread::class);
        /** @var MongoFeedRepository $oMongoFeedRepository */
        $oMongoFeedRepository = $this->odm->getRepository(MongoFeed::class);

        /** @var MongoThread $oMongoThread */
        $oMongoThread = $oMongoThreadRepository
            ->findOneBy([
                'dependencyIdentifier' => $sDependencyIdentifier,
            ])
        ;

        if ($oMongoThread && $oMongoThread->getDependencyIdentifier() === self::$aEntityToKey[MongoFeed::class]) {
            /** @var MongoFeed $oMongoFeed */
            $oMongoFeed = $oMongoFeedRepository
                ->findOneBy([
                    'identifier' => $oMongoThread->getDependencyIdentifier()
                ])
            ;

            if ($oMongoFeed && $oMongoFeed->getFeedType() === self::$aEntityToKey[MedicalCases::class] && !$oMongoFeed->getIsShared()) {
                $oMongoThread->setIsCommentable(false);
                $this->saveMongoObject($oMongoThread);
            }
        }

        if (!$oMongoThread) {
            $oMongoThread = $this->createObject(MongoThread::class);
            $oMongoThread->setDependencyType($sDependencyType);
            $oMongoThread->setDependencyIdentifier($sDependencyIdentifier);
            $oMongoThread->setUserIdentifier($sUserIdentifier);
            $oMongoThread->setIsCommentable($bIsCommentable);

            $this->saveMongoObject($oMongoThread);
        }

        return $oMongoThread;
    }

    /**
     * @param MedicalCases|MongoFeed $oDependencyObject
     *
     * @return string
     */
    private function getMongoThreadUserIdentifier($oDependencyObject)
    {
        switch (get_class($oDependencyObject)) {
            case self::$aKeyToEntityTypes[ObjectTypeConstants::OBJECT_MEDICAL_CASE]:
                return  $oDependencyObject->getDoctor()->getIdentifier();
            case self::$aKeyToEntityTypes[ObjectTypeConstants::OBJECT_MONGO_FEED]:
                return  $oDependencyObject->getCreatorUserIdentifier();
        }

        return '';
    }
}
