<?php

namespace Flendoc\AppBundle\Manager\Jobs;

use Flendoc\AppBundle\Entity\Jobs\CareerLevelLanguages;
use Flendoc\AppBundle\Entity\Jobs\CareerLevels;
use Flendoc\AppBundle\Entity\Jobs\JobTypeLanguages;
use Flendoc\AppBundle\Entity\Jobs\JobTypes;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Traits\Jobs\CareerLevelTrait;
use Flendoc\AppBundle\Traits\Jobs\JobTypesTrait;

/**
 * Class JobsManager
 * @package Flendoc\AppBundle\Manager\Jobs
 */
class JobsManager extends FlendocManager
{

    use JobTypesTrait;
    use CareerLevelTrait;

    /**
     * Synchronize Job Types
     */
    public function synchronizeJobTypes(): void
    {
        //check for existing entries in the DB
        $aExistingJobTypes    = $this->getRepository('AppBundle:Jobs\JobTypes')->findAll();
        $aExistingJobTypeName = [];

        foreach ($aExistingJobTypes as $aExistingJobType) {
            $aExistingJobTypeName[] = $aExistingJobType->getName();
        }

        foreach ($this->aJobTypes as $name => $jobType) {
            if (!in_array($name, $aExistingJobTypeName)) {
                $this->em->persist($this->_createJobTypeFromTrait($name, $jobType));
            }
        }

        $this->em->flush();
    }

    /**
     * Synchronize CareerLevel
     */
    public function synchronizeCareerLevels(): void
    {
        //check for existing entries in the DB
        $aExistingCareerLevels     = $this->getRepository('AppBundle:Jobs\CareerLevels')->findAll();
        $aExistingCareerLevelsName = [];

        foreach ($aExistingCareerLevels as $aExistingCareerLevel) {
            $aExistingCareerLevelsName[] = $aExistingCareerLevel->getName();
        }

        foreach ($this->aCareerLevel as $name => $careerLevel) {
            if (!in_array($name, $aExistingCareerLevelsName)) {
                $this->em->persist($this->_createCareerLevelFromTrait($name, $careerLevel));
            }
        }

        $this->em->flush();
    }

    /**
     * @param $name
     * @param $jobType
     *
     * @return JobTypes
     */
    private function _createJobTypeFromTrait($name, $jobType)
    {
        $oJobType = new JobTypes();

        $oJobType->setName($name);

        foreach ($jobType as $sLanguageKey => $sContent) {
            $oJobTypeLanguage = new JobTypeLanguages();
            $oJobTypeLanguage->setLanguages($this->em->getRepository('AppBundle:Languages\Languages')
                                                     ->findOneByIso($sLanguageKey)
            );
            $oJobTypeLanguage->setName($sContent);
            $oJobTypeLanguage->setJobType($oJobType);

            $oJobType->addJobTypeLanguage($oJobTypeLanguage);
        }

        return $oJobType;
    }

    /**
     * @param $name
     * @param $careerLevel
     *
     * @return CareerLevels
     */
    private function _createCareerLevelFromTrait($name, $careerLevel)
    {
        $oCareerLevel = new CareerLevels();

        $oCareerLevel->setName($name);

        foreach ($careerLevel as $sLanguageKey => $sContent) {
            $oCareerLevelLanguage = new CareerLevelLanguages();
            $oCareerLevelLanguage->setLanguages($this->em->getRepository('AppBundle:Languages\Languages')
                                                         ->findOneByIso($sLanguageKey)
            );
            $oCareerLevelLanguage->setName($sContent);
            $oCareerLevelLanguage->setCareerLevel($oCareerLevel);

            $oCareerLevel->addCareerLevelLanguage($oCareerLevelLanguage);
        }

        return $oCareerLevel;
    }
}
