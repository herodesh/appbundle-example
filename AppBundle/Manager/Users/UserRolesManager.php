<?php

namespace Flendoc\AppBundle\Manager\Users;

use Flendoc\AppBundle\Entity\Users\UserRoles;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class UserRolesManager
 * @package Flendoc\AppBundle\Manager\Users
 */
class UserRolesManager extends FlendocManager
{
    /**
     * Sync UserRoleGroups
     *
     * @param array $aUserRoles
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function syncUserRoleProperties(array $aUserRoles)
    {
        //get all User Roles
        $aExistingUserRoles = $this->getRepository('AppBundle:Users\UserRoles')->findAll();

        $aUserRoleNames = [];
        foreach ($aExistingUserRoles as $oExistingUserRole) {
            $aUserRoleNames[] = $oExistingUserRole->getName();
        }

        foreach ($aUserRoles as $key => $aUserRole) {
            if (!in_array($key, $aUserRoleNames)) {
                $oNewUserRole = new UserRoles();
                $oNewUserRole->setName($aUserRole['name']);
                $oNewUserRole->setId($aUserRole['id']);

                $this->em->persist($oNewUserRole);
            }
        }

        $this->em->flush();
    }

}
