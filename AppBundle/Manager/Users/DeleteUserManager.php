<?php

namespace Flendoc\AppBundle\Manager\Users;

use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Comments\MongoVote;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Document\Notifications\MongoNotification;
use Flendoc\AppBundle\Entity\Doctors\DoctorExistingEmailAddresses;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use Flendoc\AppBundle\Manager\FileUploaders\ProfilePhotoManager;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Manager\Messaging\MessagingManager;
use Flendoc\AppBundle\Manager\MongoThreadManager\ThreadManager;
use Monolog\Logger;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class DeleteUserManager
 * @package Flendoc\AppBundle\Manager\Users
 */
class DeleteUserManager extends FlendocManager
{

    /** @var TokenStorage */
    protected $tokenStorage;

    /** @var UserManager */
    protected $userManager;

    /** @var MessagingManager */
    protected $messagingManager;

    /** @var ProfilePhotoManager */
    protected $profilePhotoManager;

    /** @var Neo4JManager */
    protected $neo4JManager;

    /** @var Logger */
    protected $logger;

    /** @var IndexerManager */
    protected $indexerManager;

    /** @var ThreadManager */
    protected $threadManager;

    /**
     * DeleteUserManager constructor.
     *
     * @param TokenStorage        $tokenStorage
     * @param UserManager         $oUserManager
     * @param MessagingManager    $messagingManager
     * @param ProfilePhotoManager $profilePhotoManager
     * @param Neo4JManager        $neo4JManager
     * @param Logger              $logger
     * @param IndexerManager      $indexerManager
     * @param ThreadManager       $threadManager
     */
    public function __construct(
        TokenStorage $tokenStorage,
        UserManager $oUserManager,
        MessagingManager $messagingManager,
        ProfilePhotoManager $profilePhotoManager,
        Neo4JManager $neo4JManager,
        Logger $logger,
        IndexerManager $indexerManager,
        ThreadManager $threadManager
    ) {
        $this->tokenStorage        = $tokenStorage;
        $this->userManager         = $oUserManager;
        $this->messagingManager    = $messagingManager;
        $this->profilePhotoManager = $profilePhotoManager;
        $this->neo4JManager        = $neo4JManager;
        $this->logger              = $logger;
        $this->indexerManager      = $indexerManager;
        $this->threadManager       = $threadManager;
    }

    /**
     * Delete user from the system
     *
     * @param string $sUserIdentifier
     * @param bool   $bMoveToGhostUser
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteUser($sUserIdentifier, $bMoveToGhostUser = false)
    {

        $this->disableUser($sUserIdentifier);

        if ($bMoveToGhostUser) {
            $this->moveMedicalCasesToGhostUser($sUserIdentifier);
            $this->moveMedicalDictionaryToGhostUser($sUserIdentifier);
            $this->moveCommentsAndLikesToGhostUser($sUserIdentifier);
        } else {
            $this->deleteMedicalCasesFromUser($sUserIdentifier);
            $this->deleteMedicalDictionaryFromUser($sUserIdentifier);

            //DO i need to delete this? Maybe this comments are important. If not deleted then add to GhostUser.
            //$this->deleteCommentsAndLikesFromUser($sUserIdentifier);
        }

        $this->deleteFeedCommentsAndLikes($sUserIdentifier);
        $this->deleteUserNotifications($sUserIdentifier);

        $this->deleteFeedItems($sUserIdentifier);
        $this->deleteUserMessages($sUserIdentifier);
        $this->deleteProfileImage($sUserIdentifier);
        $this->clearNeo4JEntries($sUserIdentifier);
        $this->deleteUserFromES($sUserIdentifier);
        $this->deleteRedisEntriesForUser($sUserIdentifier);

        $this->deleteUserEntryFromDatabase($sUserIdentifier);
    }

    /**
     * Disable User. We need to disable user first so it does not interfere with other users.
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function disableUser($sUserIdentifier)
    {
        $oUser = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);

        $this->userManager->updateAccountStatus($oUser, true);
    }

    /**
     * Move Medical cases to GhostUser
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function moveMedicalCasesToGhostUser($sUserIdentifier)
    {
        //find all medical cases that belong to specific user
        $oDoctor       = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);
        $aMedicalCases = $this->getRepository(MedicalCases::class)->findBy([
            'doctor' => $oDoctor,
        ]);

        $oGhostUser = $this->getGhostUser();

        foreach ($aMedicalCases as $oMedicalCase) {
            $oMedicalCase->setDoctor($oGhostUser);
        }

        //save medical cases
        $this->getEntityManager()->flush();
    }

    /**
     * Delete medical cases
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteMedicalCasesFromUser($sUserIdentifier)
    {
        //find all medical cases that belong to specific user
        $oDoctor       = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);
        $aMedicalCases = $this->getRepository(MedicalCases::class)->findBy([
            'doctor' => $oDoctor,
        ]);

        foreach ($aMedicalCases as $oMedicalCase) {
            $this->getEntityManager()->remove($oMedicalCase);
        }

        //delete medical cases
        $this->getEntityManager()->flush();
    }

    /**
     * Move Medical dictionary selected author to GhostUser
     *
     * @param $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function moveMedicalDictionaryToGhostUser($sUserIdentifier)
    {
        $oUser = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);

        $oGhostUser = $this->getGhostUser();

        //find all medical dictionary entries that belong to specific user
        $aMedicalDictionary = $this->getRepository(MedicalDictionary::class)->findMedicalDictionaryByAuthor($oUser);

        foreach ($aMedicalDictionary as $oMedicalDictionary) {
            $oMedicalDictionary->removeAuthors($oUser);

            foreach ($oMedicalDictionary->getAuthors() as $oAuthor) {
                if ($oAuthor->getIsGhostUser()) {
                    continue 2;
                }
            }

            $oMedicalDictionary->addAuthors($oGhostUser);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * Move Medical dictionary selected author to GhostUser
     *
     * @param $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteMedicalDictionaryFromUser($sUserIdentifier)
    {
        $oUser = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);

        //find all medical dictionary entries that belong to specific user
        $aMedicalDictionary = $this->getRepository(MedicalDictionary::class)->findMedicalDictionaryByAuthor($oUser);

        foreach ($aMedicalDictionary as $oMedicalDictionary) {
            $this->getEntityManager()->remove($oMedicalDictionary);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * Move all comments and likes that are not feed related to GhostUser. For ex. Medical Cases comments
     *
     * @param $sUserIdentifier
     */
    public function moveCommentsAndLikesToGhostUser($sUserIdentifier)
    {
        $oGhostUser = $this->getGhostUser();

        //get all the threads that are not feeds
        $aThreads = $this->getMongoRepository(MongoThread::class)->findThreadsThatCanSwitchUsers($sUserIdentifier);
        foreach ($aThreads as $oThread) {

            //move all comments to the ghost user
            $this->getMongoRepository(MongoComment::class)
                 ->findAndUpdateCommentsByThread($oThread, $sUserIdentifier, $oGhostUser);

            //move all likes to the ghost user
            $this->getMongoRepository(MongoVote::class)
                 ->findAndUpdateVotesByThread($oThread, $sUserIdentifier, $oGhostUser);
        }
    }

    /**
     * Delete all feed comments and likes for a specific user.
     *
     * @param $sUserIdentifier
     */
    public function deleteFeedCommentsAndLikes($sUserIdentifier)
    {
        //delete comments that have thread dependencyType feed related -> om3
        $aFeedThreads = $this->getMongoRepository(MongoThread::class)->findBy([
            'dependencyType' => ObjectTypeConstants::OBJECT_MONGO_FEED,
        ]);

        foreach ($aFeedThreads as $oFeedThread) {
            //delete all comments from the feed
            $this->getMongoRepository(MongoComment::class)
                 ->findAndDeleteCommentsByThread($oFeedThread, $sUserIdentifier);

            //delete all likes from the feed
            $this->getMongoRepository(MongoVote::class)
                 ->findAndRemoveVotesByThread($oFeedThread, $sUserIdentifier);
        }

    }

    /**
     * Deletes the notifications of the user
     *
     * @param string $sUserIdentifier
     */
    public function deleteUserNotifications($sUserIdentifier)
    {
        //find and delete user notifications
        $aUserNotifications = $this->getMongoRepository(MongoNotification::class)->findBy([
            'receiverIdentifier' => $sUserIdentifier,
        ]);

        foreach ($aUserNotifications as $oUserNotification) {
            $this->deleteMongoObject($oUserNotification);
        }
    }

    /**
     * Deleted the feeds that are saved in mongo DB. This are different than the ones in Neo4j
     *
     * @param $sUserIdentifier
     */
    public function deleteFeedItems($sUserIdentifier)
    {
        $aUserFeedsToBeDeleted = $this->getMongoRepository(MongoFeed::class)->findBy([
            'creatorUserIdentifier' => $sUserIdentifier,
        ]);

        foreach ($aUserFeedsToBeDeleted as $oUserFeed) {
            //find all threads linked to the creator feeds
            $oThread = $this->getMongoRepository(MongoThread::class)
                            ->findOneBy([
                                'dependencyIdentifier' => $oUserFeed->getIdentifier(),
                            ]);

            //thread manager
            $this->threadManager->deleteThread($oThread, $sUserIdentifier);
        }

        $this->getMongoRepository(MongoFeed::class)->findAndRemoveFeedByCreator($sUserIdentifier);
    }

    /**
     * Deletes all user chats, including archived ones
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserMessages($sUserIdentifier)
    {
        $this->messagingManager->deleteUserChats($sUserIdentifier);
    }

    /**
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteProfileImage($sUserIdentifier)
    {
        $oDoctor = $this->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);

        $this->profilePhotoManager->deleteProfileImage($oDoctor);
    }

    /**
     * When users change their email addresses the email is assigned to them.
     * Therefor this emails needs to be removed as well.
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserRegisteredEmails($sUserIdentifier)
    {
        $aUserExistingEmails = $this->getRepository(DoctorExistingEmailAddresses::class)->findBy([
            'doctor' => $this->getRepository(Doctors::class)->findOneBy([
                'identifier' => $sUserIdentifier,
            ]),
        ]);

        foreach ($aUserExistingEmails as $oUserExistingEmail) {
            $this->getEntityManager()->remove($oUserExistingEmail);
        }

        $this->getEntityManager()->flush();
    }

    /**
     *
     * // Procedure Description
     * // Paramenter to the query: IDENTIFIER (Please note it's UPPERCASE)
     * // This Procedure executes first query and pass it's result(here shared_feeds) to second query in batches of size: 10000,
     * // second query set the status for each of these feeds to 'DELETE'
     *
     * // Match the Doctor then his/her feeds
     * // Get all the feeds that has ORIGIN to these feeds
     * // Pass these feeds to second query to set the status
     * // Second query set the status for each of these shared feeds
     * // Finally Procedure returns the count of all committed Operations, here it will be updated_feeds count
     * CALL apoc.periodic.iterate(
     * "MATCH (:Doctor {identifier:{userIdentifier}})-[:FEED_ITEM|NEXT*]->(feed:FeedEntry)
     * WITH feed
     * MATCH (feed)<-[:ORIGIN*1..]-(shared_feed:FeedEntry)
     * RETURN shared_feed",
     * "SET shared_feed.status='DELETE'",
     * {batchSize:10000, iterateList:true, parallel:true, params:{userIdentifier:{IDENTIFIER}}}) YIELD committedOperations AS updated_feeds
     * WITH updated_feeds
     *
     * // This query match the doctor and his/her feeds and delete their relationships and then delete these nodes
     * MATCH (doctor:Doctor {identifier:{IDENTIFIER}})-[:FEED_ITEM|NEXT*]->(feeds:FeedEntry)
     * DETACH DELETE doctor,feeds
     *
     * // This statement returns the counts of operations
     * Return updated_feeds, COUNT(feeds) AS deleted_feeds
     *
     * @param string $sUserIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function clearNeo4JEntries($sUserIdentifier)
    {
        $neoClient = $this->neo4JManager->client;
        try {
            $result = $neoClient->run('
        CALL apoc.periodic.iterate(
            "MATCH (:Doctor {identifier:{userIdentifier}})-[:FEED_ITEM|NEXT*]->(feed:FeedEntry)
            WITH feed
            MATCH (feed)<-[:ORIGIN*1..]-(shared_feed:FeedEntry)
            RETURN shared_feed", 
            "SET shared_feed.deleted=\'true\'",
            {batchSize:10000, iterateList:true, parallel:true, params:{userIdentifier:{userIdentifierValue}}}) YIELD committedOperations AS updated_feeds
            WITH updated_feeds
            MATCH (doctor:Doctor {identifier:{userIdentifierValue}})
            OPTIONAL MATCH (doctor)-[:FEED_ITEM|NEXT*]->(feeds:FeedEntry)
            DETACH DELETE doctor, feeds
            Return updated_feeds, COUNT(feeds) AS deleted_feeds', [
                'userIdentifierValue' => $sUserIdentifier,
            ]);

            return $result;
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());

            return false;
        }
    }

    /**
     * @param string $sUserIdentifier
     */
    public function deleteUserFromES($sUserIdentifier)
    {
        $this->indexerManager->deleteUser($sUserIdentifier);
    }

    /**
     * Deletes user from the DB
     *
     * @param string $sUserIdentifier
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserEntryFromDatabase($sUserIdentifier)
    {
        $oUser = $this->getRepository(Doctors::class)->findOneBy(['identifier' => $sUserIdentifier]);
        $oUser->setEnabled(false);
        $oUser->setLocked(true);
        $oUser->setExpired(true);

        $this->getEntityManager()->remove($oUser);
        $this->getEntityManager()->flush();
    }

    /**
     * Delete redis entries for the user
     *
     * @param string $sUserIdentifier
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteRedisEntriesForUser($sUserIdentifier)
    {
        $this->deleteCacheData(CacheKeyPrefixes::USER_LOGIN_ATTEMPTS, $sUserIdentifier);
        $this->deleteCacheData(CacheKeyPrefixes::USER_CONNECTED_DEVICES_SESSIONS, $sUserIdentifier);
        $this->deleteCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $sUserIdentifier);
        $this->deleteCacheData(CacheKeyPrefixes::USER_EMAIL_SETTINGS, $sUserIdentifier);
    }

    /**
     * Get the ghost User
     *
     * @return object|null
     */
    private function getGhostUser()
    {
        $oGhostUser = $this->getRepository(Doctors::class)->findOneBy([
            'isGhostUser' => true,
            'enabled'     => true,
        ]);

        return $oGhostUser;
    }
}
