<?php

namespace Flendoc\AppBundle\Manager\Users;

use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocuments;
use Flendoc\AppBundle\Manager\FlendocManager;

/**
 * Class UserRegistrationManager
 * @package Flendoc\AppBundle\Manager\Users
 */
class UserRegistrationManager extends FlendocManager
{
    /**
     * @param         $aFile
     * @param Doctors $oDoctor
     */
    public function saveSentDocuments($aFile, Doctors $oDoctor)
    {
        $oDocuments = new RegistrationDocuments();

        $oDoctor->setVerificationDocumentsSent(true);
        $oDoctor->setVerificationDocumentsSentAt();

        $oDocuments->setDoctors($oDoctor);
        $oDocuments->setFileName($aFile['newFileName']);
        $oDocuments->setOriginalName($aFile['originalFileName']);

        $this->saveObject($oDocuments);
    }

    /**
     * @param array $aRegistrationDocuments
     *
     * @return bool
     */
    public function getDocumentsProcessingStatus(array $aRegistrationDocuments)
    {
        foreach ($aRegistrationDocuments as $oRegistrationDocuments) {
            if (!$oRegistrationDocuments->getIsProcessed()) {
                return false;
            }
        }

        return true;
    }

}
