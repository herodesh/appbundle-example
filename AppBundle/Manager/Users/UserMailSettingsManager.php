<?php

namespace Flendoc\AppBundle\Manager\Users;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Adapter\Cache\CacheManager;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Admins\AdminMailSettings;
use Flendoc\AppBundle\Entity\Doctors\DoctorMailSettings;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Manager\FlendocManager;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserMailSettingsManager
 * @package Flendoc\AppBundle\Manager\Users
 */
class UserMailSettingsManager extends FlendocManager
{
    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /** @var FlendocManager */
    protected $flendocManager;

    /**
     * UserMailSettingsManager constructor.
     *
     * @param CacheManager   $cacheManager
     * @param FlendocManager $flendocManager
     */
    public function __construct(CacheManager $cacheManager, FlendocManager $flendocManager)
    {
        $this->cacheManager   = $cacheManager;
        $this->flendocManager = $flendocManager;
    }

    /**
     * Check for User Mail Settings
     *
     * @param UserInterface $oUser
     *
     * @return array|mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function checkUserMailSettings(UserInterface $oUser)
    {
        $oUserMailRepo = $this->_getUserMailSettingsRepo($oUser);

        $aUserMailSettingsMailId = $oUserMailRepo->findUserMailSettingsMailId($oUser);
        $aMails                  = $this->em->getRepository('AppBundle:Mails\Mails')->findMails();

        $this->saveObject($this->updateUserMailSettings($oUser, $aMails, $aUserMailSettingsMailId));

        //rebuild user's mail cache settings
        $oUserLanguage = $this->em->getRepository('AppBundle:Languages\Languages')
                                  ->findOneByIso($oUser->getDefaultLanguage());

        return $this->displayOrRebuildUserMailSettings($oUser, $oUserLanguage);
    }

    /**
     * Create and save all email settings for a user
     *
     * @param UserInterface $oUser
     * @param array         $aMails
     *
     * @return UserInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAllMailSettingsForUser(UserInterface $oUser, array $aMails)
    {
        //create all email settings for user
        $aUserEmailSettings = new ArrayCollection();
        foreach ($aMails as $aMail) {
            if ($oMailSettings = $this->createUserMailSetting($aMail, $oUser)) {
                $aUserEmailSettings->add($oMailSettings);
            }
        }
        $oUser->setMailSettings($aUserEmailSettings);

        return $oUser;
    }

    /**
     * Update UserMailSettings
     *
     * @param UserInterface $oUser
     * @param array         $aMails
     * @param array         $aUserMailSettingsMailId
     *
     * @return UserInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateUserMailSettings(UserInterface $oUser, array $aMails, array $aUserMailSettingsMailId)
    {
        foreach ($aMails as $aMail) {
            if ($oMailSettings = $this->createUserMailSetting($aMail, $oUser)) {
                if (false === array_search($aMail['id'], array_column($aUserMailSettingsMailId, 'id'))) {
                    $oUser->addMailSetting($oMailSettings);
                }
            }
        }

        return $oUser;
    }

    /**
     * Enable or Disable mail setting
     *
     * @param $type
     * @param $id
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function enableDisableMailSetting($type, $id)
    {
        $oMailSetting = $this->_getUserMailSettingsRepo($type)->findOneById($id);

        if (!$oMailSetting->getReadOnly()) {
            $oMailSetting->setEnabled(1 - $oMailSetting->getEnabled());
            $this->saveObject($oMailSetting);
        }

        return;
    }

    /**
     * Create UserMailSetting
     * Creates a new entry for user email setting and attaches the proper user to it based on the user type
     *
     * @param array         $aMail
     * @param UserInterface $oUser
     *
     * @return AdminMailSettings|DoctorMailSettings|null
     * @throws \Doctrine\ORM\ORMException
     */
    public function createUserMailSetting(array $aMail, UserInterface $oUser)
    {
        switch ($this->_getUserType($oUser)) {
            case 'admins';
                if ($aMail['isPatient'] || (!$aMail['isAdministrative'] && !$aMail['isAccount'])) {
                    return null;
                }
                $oUserMailSettings = new AdminMailSettings();
                $oUserMailSettings->setAdmin($oUser);

                break;
            case 'doctors':
                if ($aMail['isPatient'] || $aMail['isAdministrative']) {
                    return null;
                }
                $oUserMailSettings = new DoctorMailSettings();
                $oUserMailSettings->setDoctor($oUser);
                break;
            /*case 'patients':
                break;*/
        }

        $oUserMailSettings->setMail($this->em->getReference('AppBundle:Mails\Mails', $aMail['id']));
        if ($aMail['isAccount']) {
            $oUserMailSettings->setReadOnly(true);
        }

        return $oUserMailSettings;
    }

    /**
     * Find UserMailSettings from RedisCache. If not found rebuild the cache
     *
     * @param           $oUser
     * @param Languages $oUserLanguage
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function displayOrRebuildUserMailSettings($oUser, Languages $oUserLanguage)
    {

        $aUserMailSettings = $this->_getUserMailSettingsRepo($oUser)
                                  ->findUserMailSettings($oUser->getId(), $oUserLanguage);

        return $this->flendocManager->rebuildAndDisplayCacheData(CacheKeyPrefixes::USER_EMAIL_SETTINGS, $oUser->getIdentifier(), $aUserMailSettings);
    }

    /**
     * @param $objectOrType
     *
     * @return \Doctrine\ORM\EntityRepository|\Flendoc\AppBundle\Repository\Admins\AdminMailSettingsRepository|\Flendoc\AppBundle\Repository\Doctors\DoctorMailSettingsRepository|null
     */
    private function _getUserMailSettingsRepo($objectOrType)
    {
        $type = $objectOrType;
        if ($objectOrType instanceof UserInterface) {
            $type = $this->_getUserType($objectOrType);
        }

        switch ($type) {
            case AppConstants::USER_TYPE_ADMINS:
                return $this->em->getRepository('AppBundle:Admins\AdminMailSettings');
                break;
            case AppConstants::USER_TYPE_DOCTORS:
                return $this->em->getRepository('AppBundle:Doctors\DoctorMailSettings');
                break;
            case AppConstants::USER_TYPE_PATIENTS:
                break;
        }

        return null;
    }

    /**
     * @param UserInterface $oUser
     *
     * @return string
     */
    private function _getUserType(UserInterface $oUser)
    {
        $aClassPaths = explode('\\', get_class($oUser));
        $sUserType   = (strtolower(end($aClassPaths)));

        return $sUserType;
    }
}
