<?php

namespace Flendoc\AppBundle\Manager\Users;

use Doctrine\Common\Collections\ArrayCollection;
use Flendoc\AppBundle\Adapter\Cache\CacheKeyPrefixes;
use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use Flendoc\AppBundle\Adapter\SwiftMailer\SwiftMailerAdapter;
use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Constants\UserLockReasonsConstants;
use Flendoc\AppBundle\Entity\Admins\Admins;
use Flendoc\AppBundle\Entity\Doctors\DoctorExistingEmailAddresses;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Doctors\UserLockReasons;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Mails\MailsConstants;
use Flendoc\AppBundle\Entity\Users\AbstractUser;
use Flendoc\AppBundle\Manager\Elasticsearch\IndexerManager;
use Flendoc\AppBundle\Manager\FlendocManager;
use Monolog\Logger;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserManager
 * @package Flendoc\AppBundle\Manager\Users
 */
class UserManager extends FlendocManager
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $cookies;

    /**
     * @var UserPasswordEncoder
     */
    protected $passwordEncoder;

    /** @var Neo4JManager $oNeo4JManager */
    protected $neo4JManager;

    /** @var $logger Logger */
    protected $logger;

    /** @var IndexerManager */
    protected $indexerManager;

    /** @var Router */
    protected $router;

    /** @var SwiftMailerAdapter */
    protected $swiftMailer;

    /**
     * UserManager constructor.
     *
     * @param Session             $session
     * @param                     $cookies
     * @param UserPasswordEncoder $passwordEncoder
     * @param Neo4JManager        $oNeo4JManager
     * @param Logger              $logger
     * @param IndexerManager      $oIndexerManager
     * @param Router              $router
     * @param SwiftMailerAdapter  $swiftMailer
     */
    public function __construct(
        Session $session,
        $cookies,
        UserPasswordEncoder $passwordEncoder,
        Neo4JManager $oNeo4JManager,
        Logger $logger,
        IndexerManager $oIndexerManager,
        Router $router,
        SwiftMailerAdapter $swiftMailer
    ) {
        $this->session         = $session;
        $this->cookies         = $cookies;
        $this->passwordEncoder = $passwordEncoder;
        $this->neo4JManager    = $oNeo4JManager;
        $this->logger          = $logger;
        $this->indexerManager  = $oIndexerManager;
        $this->router          = $router;
        $this->swiftMailer     = $swiftMailer;
    }

    /**
     * @TODO - This is not a security hazzard? What if the session can be grabbed and used somewhere
     *
     * Get user login sessions in order to check if the user is logged in or not
     *
     * @param UserInterface $oUser
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getUserLoginSessions(UserInterface $oUser)
    {
        return $this->displayCacheData(CacheKeyPrefixes::USER_CONNECTED_DEVICES_SESSIONS, $oUser->getIdentifier());
    }

    /**
     * @TODO - This is not a security hazzard? What if the session can be grabbed and used somewhere
     *
     * Save login sessions in order to use them and disconnect connected devices
     *
     * @param UserInterface $oUser
     * @param array         $aSavedSessions
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function saveUserLoginSessionsToRedis(UserInterface $oUser, array $aSavedSessions): void
    {
        $this->saveCacheData(CacheKeyPrefixes::USER_CONNECTED_DEVICES_SESSIONS, $oUser->getIdentifier(), $aSavedSessions);
    }

    /**
     * @param string $sSessionUserName
     *
     * @return array|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getLoginAttempts($sSessionUserName)
    {
        if (null == $sSessionUserName || is_array($this->displayCacheData(CacheKeyPrefixes::USER_LOGIN_ATTEMPTS, AppStatic::userNameCacheReady($sSessionUserName)))) {
            return 0;
        }

        return $this->displayCacheData(CacheKeyPrefixes::USER_LOGIN_ATTEMPTS, AppStatic::userNameCacheReady($sSessionUserName));
    }

    /**
     * @param string $sSessionUserName
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function updateLoginAttempts($sSessionUserName)
    {
        $iLoginAttempts = $this->getLoginAttempts($sSessionUserName);

        if (empty($iLoginAttempts)) {
            $iLoginAttempts = 0; //redis manager returns an empty array
        }

        $this->saveCacheData(CacheKeyPrefixes::USER_LOGIN_ATTEMPTS, AppStatic::userNameCacheReady($sSessionUserName), $iLoginAttempts + 1);
    }

    /**
     * @param string $sSessionUserName
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function resetLoginAttempts($sSessionUserName)
    {
        $this->deleteCacheData(CacheKeyPrefixes::USER_LOGIN_ATTEMPTS, AppStatic::userNameCacheReady($sSessionUserName));
    }

    /**
     * @param string $sSessionUserName
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Twig\Error\Error
     */
    public function tooManyLoginFailuresLockAccountAndNotifyUser($sSessionUserName)
    {
        $iLoginAttempts = $this->getLoginAttempts($sSessionUserName);

        if ($iLoginAttempts >= AppConstants::USER_LOGIN_ATTEMPTS_BLOCK) {
            $oUser = $this->getRepository(Doctors::class)->findOneBy([
                'username' => $sSessionUserName,
            ]);

            if (null != $oUser) {
                $oLockReason = $this->getRepository(UserLockReasons::class)->findByLanguageAndStringIdentifier(
                    $this->getRepository(Languages::class)->findOneBy([
                        'iso' => $oUser->getDefaultLanguage(),
                    ]),
                    UserLockReasonsConstants::USER_LOCK_FAILED_LOGIN_ATTEMPTS
                );

                $oUser->setLocked(true);
                $oUser->setLockedReason($this->getRepository(UserLockReasons::class)->findOneBy([
                    'stringIdentifier' => UserLockReasonsConstants::USER_LOCK_FAILED_LOGIN_ATTEMPTS,
                ]));

                $aEmailParameters = [
                    'firstName'  => $oUser->getDoctorProfile()->getFirstName(),
                    'lockReason' => $oLockReason[0]['description'],
                    'contactUrl' => $this->router->generate('doctors_contact_us_form', [], Router::ABSOLUTE_URL),
                ];

                //send email
                $this->swiftMailer->send(MailsConstants::ACCOUNT_LOCKED, $oUser, $aEmailParameters, [], true);

                //save locked user
                $this->em->flush();
            }
        }
    }

    /**
     * @param UserInterface $oUser
     * @param array         $aMails
     *
     * @return UserInterface
     */
    public function createAllEmailSettingsForUser(UserInterface $oUser, array $aMails)
    {
        //create all email settings for user
        $aUserEmailSettings = new ArrayCollection();
        foreach ($aMails as $aMail) {
            if ($oMailSettings = $this->userMailSettingsManager->createUserMailSetting($aMail, $oUser)) {
                $aUserEmailSettings->add($oMailSettings);
            }
        }
        $oUser->setMailSettings($aUserEmailSettings);

        return $oUser;
    }

    /**
     * Create or update the cookie
     *
     * @param array $aLoginSessions
     * @param       $oUser
     * @param       $oUserEvent
     * @param bool  $bCreate
     */
    public function saveUserCookie(array $aLoginSessions, $oUser, $oUserEvent, $bCreate = false)
    {
        //@TODO Check if I can add baseencode instead of 'default' ... $sCookieIdentifier = base64_encode($oUser->getIdentifier());
        $oCookieLifetime = new \DateTime($this->cookies['lifetime']);
        $sCookieName     = $bCreate ? sha1(uniqid().rand(0, 9999)) : $_COOKIE[$this->cookies['default']];
        $oCookie         = new Cookie($this->cookies['default'], $sCookieName, $oCookieLifetime);

        $aLoginSessions[$oCookie->getValue()] = [
            'lastLogin'  => new \DateTime('now'),
            'userEnv'    => $oUserEvent,
            'forceLogin' => false,
        ];

        $this->saveUserLoginSessionsToRedis($oUser, $aLoginSessions);

        setcookie($this->cookies['default'], $oCookie->getValue(), strtotime($this->cookies['lifetime']), '/');
    }

    /**
     * Save User Current language
     *
     * @param UserInterface $oUser
     * @param Request       $oRequest
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveCurrentUserLanguage(UserInterface $oUser, Request $oRequest)
    {
        $sLanguage = $oRequest->request->get('selectLanguage');

        $oRequest->setLocale($sLanguage);
        $this->session->set('_locale', $sLanguage);

        $oUser->setDefaultLanguage($sLanguage);
        $this->em->flush($oUser);
    }

    /**
     * Save user permissions
     *
     * @param               $oRequestParameterBag
     * @param UserInterface $oUser
     *
     * @return bool
     */
    public function saveUserRoles($oRequestParameterBag, UserInterface $oUser)
    {
        //Remove existing roles
        foreach ($oUser->getRoles() as $sKey => $oUserRoles) {
            $oRole = $this->getRepository('AppBundle:Users\UserRoles')->findOneByName($oUserRoles);
            $oUser->removeRole($oRole);
        }

        try {
            foreach ($oRequestParameterBag->get('role') as $sRoleKey => $aValues) {
                $oRole = $this->getRepository('AppBundle:Users\UserRoles')->findOneByName($sRoleKey);
                $oUser->addRole($oRole);
            }

            $this->saveObject($oUser);

            return true;
        } catch (\Exception $oException) {
            return false;
        }
    }

    /**
     * Update user's Password
     *
     * @param $oUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePassword($oUser): void
    {
        if (null != $oUser->getPlainPassword()) {
            $sPassword = $this->passwordEncoder->encodePassword($oUser, $oUser->getPlainPassword());
            $oUser->setPassword($sPassword);
            $oUser->setPasswordRequestedAt(null);
            $oUser->setConfirmationToken(null);

            if ($oUser instanceof Admins && $oUser->getHasGeneratedPassword()) {
                $oUser->setHasGeneratedPassword(false);
            }

            $this->saveObject($oUser);
        }

        return;
    }

    /**
     * @param UserInterface $oUser
     * @param string        $sUsername
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changeUsername($oUser, $sUsername): void
    {
        $oDoctorExistingEmailAddress = $this->getRepository(DoctorExistingEmailAddresses::class)
                                            ->findOneBy([
                                                'email' => $sUsername,
                                            ]);

        if (!$oDoctorExistingEmailAddress) {
            /** @var DoctorExistingEmailAddresses $oDoctorExistingEmailAddress */
            $oDoctorExistingEmailAddress = new DoctorExistingEmailAddresses();
            $oDoctorExistingEmailAddress->setDoctor($oUser);
            $oDoctorExistingEmailAddress->setEmail($sUsername);
            $oUser->addExistingEmailAddress($oDoctorExistingEmailAddress);
        }

        $oUser->setUsername($sUsername);

        $this->saveObject($oUser);
    }

    /**
     * @param AbstractUser $oUser
     * @param bool         $bDisable Forces disabling of the user
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function updateAccountStatus(AbstractUser $oUser, $bDisable = false): void
    {
        if ($bDisable) {
            $status = false;
        } else {
            $status = $oUser->getEnabled() ? false : true;
        }
        $oUser->setEnabled($status);
        $this->saveObject($oUser);
        $this->indexerManager->reindexUser($oUser->getIdentifier());

        if ($oUser instanceof Doctors) {
            try {
                $this
                    ->neo4JManager
                    ->client
                    ->run('
                        MATCH (doctor:Doctor {identifier: {identifier}})
                        SET doctor.status = {status}
                        ',
                        [
                            'identifier' => $oUser->getIdentifier(),
                            'status'     => $status ? 'active' : 'inactive',
                        ]
                    );
            } catch (\Exception $oException) {
                $this->logger->info($oException->getMessage());
            }
        }

        //rebuild user profile redis
        $aUserProfileData = $this->getUserProfileDetails($oUser->getIdentifier());
        $this->rebuildAndDisplayCacheData(CacheKeyPrefixes::USER_PROFILE_DETAILS, $oUser->getIdentifier(), $aUserProfileData);
    }
}
