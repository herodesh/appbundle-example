<?php

namespace Flendoc\AppBundle\Manager\Feeds;

use Flendoc\AppBundle\Adapter\Curler\Curler;
use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Comments\MongoVote;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Constants\FeedConstants;
use Flendoc\AppBundle\Manager\Comments\CommentsManager;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Manager\Notifications\NotificationManager;
use Flendoc\AppBundle\Repository\Comments\MongoThreadRepository;
use Flendoc\AppBundle\Repository\Comments\MongoVoteRepository;
use Flendoc\AppBundle\Traits\App\ObjectKeyEntityTrait;
use Flendoc\AppBundle\Utils\MetaParser;
use Flendoc\DoctorsBundle\Traits\Feeds\FeedPostViewTrait;
use GraphAware\Common\Result\Record;
use Monolog\Logger;
use Symfony\Component\Translation\Translator;

/**
 * Class FeedsManager
 * @package Flendoc\AppBundle\Manager\Feeds
 */
class FeedsManager extends FlendocManager
{

    use ObjectKeyEntityTrait;

    use FeedPostViewTrait;

    /** @var Neo4JManager $oNeo4JManager */
    protected $neo4JManager;

    /** @var \Twig_Environment */
    protected $twig;

    /** @var $logger Logger */
    protected $logger;

    /** @var Translator */
    protected $translator;

    /**
     * FeedsManager constructor.
     *
     * @param Neo4JManager      $oNeo4JManager
     * @param \Twig_Environment $twig
     * @param Logger            $logger
     * @param Translator        $translator
     */
    public function __construct(
        Neo4JManager $oNeo4JManager,
        \Twig_Environment $twig,
        Logger $logger,
        Translator $translator
    ) {
        $this->neo4JManager = $oNeo4JManager;
        $this->twig         = $twig;
        $this->logger       = $logger;
        $this->translator   = $translator;
    }

    /**
     * Parse content of a link
     *
     * @param null $string
     *
     * @return string
     */
    public function parseFeedLink($string = null)
    {
        if (null == $string) {
            return json_encode([]);
        }

        //find occurrences of a url in a string
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $string, $match);

        if (null != $match[0]) {
            try {
                $curler = new Curler();

                $url = $match[0][0];

                $responseBody   = $curler->get($url);
                $parser         = new MetaParser($responseBody, $url);
                $aParserDetails = $parser->getDetails();

                //if the url is openGraph compatible will receive better parameters
                if (!empty($aParserDetails['openGraph'])) {
                    $openGraph = $aParserDetails['openGraph'];

                    $sUrl         = (!empty($openGraph['url'])) ? $openGraph['url'] :
                        (!empty($aParserDetails['url']) ? $aParserDetails['url'] : []);
                    $sImage       = (!empty($openGraph['image'])) ?
                        (@getimagesize($openGraph['image']) ? $openGraph['image'] : []) : [];
                    $sSiteName    = (!empty($openGraph['site_name'])) ? $openGraph['site_name'] : [];
                    $sTitle       = (!empty($openGraph['title'])) ? $openGraph['title'] : [];
                    $sDescription = (!empty($openGraph['description'])) ? $openGraph['description'] : [];
                } else {
                    $sUrl         = (!empty($aParserDetails['url'])) ? $aParserDetails['url'] : [];
                    $sImage       = (!empty($aParserDetails['images'])) ?
                        (@getimagesize($aParserDetails['images'][0]) ? $aParserDetails['images'][0] : []) : [];
                    $sSiteName    = (!empty($aParserDetails['base'])) ? $aParserDetails['base'] : [];
                    $sTitle       = (!empty($aParserDetails['title'])) ? $aParserDetails['title'] : [];
                    $sDescription = (!empty($aParserDetails['description'])) ? $aParserDetails['description'] : [];
                }

                $aResult = [
                    'url'         => $sUrl,
                    'image'       => $sImage,
                    'siteName'    => $sSiteName,
                    'title'       => $sTitle,
                    'description' => $sDescription,
                ];

                return json_encode($aResult);
            } catch (\Exception $e) {
                $this->logger->log('error', $e->getMessage());
            }
        }

        return json_encode([]);
    }

    /**
     * Update neo4j node properties
     *
     * @param MongoFeed $oMongoFeedData
     *
     * @return bool
     * @throws \GraphAware\Neo4j\Client\Exception\Neo4jException
     */
    public function updateNeo4JNode(MongoFeed $oMongoFeedData)
    {
        $neoClient = $this->neo4JManager->client;
        $stack     = $neoClient->stack();

        $query = '
            MATCH (p { identifier:{itemIdentifier} })
            SET p.privacy = {privacy}
        ';

        $stack->push($query, [
            'itemIdentifier' => $oMongoFeedData->getIdentifier(),
            'privacy'        => $oMongoFeedData->getPrivacy(),
        ]);

        $neoClient->runStack($stack);

        return true;
    }

    /**
     * Creates a new feed item node
     *
     * @param string      $sUserIdentifier           A string with the user identifier
     * @param string      $sObjectFeedType           What type of share is it (ex. medical_case, share_status, etc.)
     *                                               This one comes from ObjectTypeConstants::XXX
     * @param string      $sObjectIdentifier         The identifier of the entry that is saved in database (mongo or sql)
     * @param string      $sObjectDataType           Can be mongo or mysql. Based on this the query to get the content is made
     *                                               against specific database
     * @param string|null $sOriginalObjectIdentifier Identifier of the original object or null
     * @param string      $sPrivacy                  Privacy of the entry
     * @param bool        $bIsShare                  Mark object as being shared or not
     *
     * @return bool
     */
    public function addNeo4JFeedItemNode(
        $sUserIdentifier,
        $sObjectFeedType,
        $sObjectIdentifier,
        $sObjectDataType,
        $sOriginalObjectIdentifier = null,
        $sPrivacy,
        bool $bIsShare = false
    ) {
        // Add sharedObjectIdentifier and isShare fields in order to manage shared entities too
        try {
            $neoClient = $this->neo4JManager->client;
            $stack     = $neoClient->stack();
            $query     = '';

            if ($bIsShare) {
                $query .= 'MATCH (n)-[:FEED_ITEM|:NEXT]-(sharedNode) WHERE n.identifier={originalObjectIdentifier}
                        
                        WITH n
                        
                        ';
            }

            $query .= 'MATCH (d:Doctor { identifier:{doctorIdentifier} }) //the doctor that is making the action
                OPTIONAL MATCH (d)-[r:FEED_ITEM]-(secondlatestupdate)
                
                DELETE r
                
                CREATE UNIQUE (d)-[:FEED_ITEM]->(latest_update:FeedEntry {
                    identifier:{identifier}, //object identifier that is the same as the one in the database
                    objectDataType:{objectDataType}, //what type of object is it mysql or mongo
                    feedType:{feedType}, //what type is it (status_update, shared, etc)
                    actor:{actor}, //The doctor that made the share action
                    deleted: "false", //all feeds have deleted false when posted
                    ';

            if ($bIsShare) {
                //we need the original identifier
                $query .= 'originalObjectIdentifier:{originalObjectIdentifier},';
            } else {
                //we use the element identifier
                $query .= 'originalObjectIdentifier:null,';
            }

            $query .= '
                    privacy:{privacy},
                    published: timestamp()
                    }
                )';

            if ($bIsShare) {
                $query .= '-[:ORIGIN]->(n)';
            }

            $query .= '    
                WITH latest_update, collect(secondlatestupdate) AS seconds
                FOREACH (x IN seconds | CREATE UNIQUE (latest_update)-[:NEXT]->(x))
                RETURN latest_update.text AS new_status';

            //Insert a new feed Item
            $stack->push($query,
                [
                    //the same as object identifier. Will help us on share on feed
                    'identifier'               => $sObjectIdentifier,
                    'feedType'                 => $sObjectFeedType,
                    'objectDataType'           => $sObjectDataType,
                    'actor'                    => $sUserIdentifier,
                    'doctorIdentifier'         => $sUserIdentifier,
                    'originalObjectIdentifier' => $sOriginalObjectIdentifier,
                    'privacy'                  => $sPrivacy,
                ]
            );

            $neoClient->runStack($stack);

            return true;
        } catch (\Exception $oException) {
            $this->logger->log('error', $oException->getMessage());

            return false;
        }
    }

    /**
     * Gets the user personal feeds
     *
     * @param string $sCurrentUserIdentifier
     * @param string $sTargetUserIdentifier
     * @param null   $iPage
     * @param int    $iLimit
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function getPersonalFeeds(
        $sCurrentUserIdentifier,
        $sTargetUserIdentifier,
        $iPage = null,
        $iLimit = FeedConstants::FEED_DEFAULT_LIMIT
    ) {
        try {
            $neoClient = $this->neo4JManager->client;

            $result = $neoClient->run('
                MATCH (me : Doctor {identifier:{sCurrentUserIdentifier}}),
                (user : Doctor {identifier:{sTargetUserIdentifier}}),
                (user)-[:FEED_ITEM|:NEXT*1..]-(feed)
    
                OPTIONAL MATCH (feed)-[:ORIGIN]->(origin)
                
                WITH me, user, feed, origin
                
                MATCH (feed)
                
                WHERE 
                    (NOT (me)-[:IS_HIDDEN]->(feed))
                AND 
                    (feed.deleted = "false")
                AND 
                ( 
                    {sCurrentUserIdentifier} = {sTargetUserIdentifier}
                    OR 
                        feed.feedType = "shared" 
                    AND 
                        feed.privacy="public" 
                    AND 
                        ( origin.privacy="public" OR me.identifier=origin.actor OR (me)-[:isFriendWith { status : "accepted" }]-(:Doctor{identifier: origin.actor}))
                    OR 
                        feed.feedType = "shared" 
                    AND 
                        (me)-[:isFriendWith { status : "accepted" }]-(user) 
                    AND 
                        ( origin.privacy="public" OR me.identifier=origin.actor OR (me)-[:isFriendWith { status : "accepted" }]-(:Doctor{identifier: origin.actor}))
                    OR
                        (NOT feed.feedType = "shared" AND ( feed.privacy="public" OR (me)-[:isFriendWith { status : "accepted" }]-(user) ))
                )
                return  user as Friend, feed as Feeds
                
                ORDER BY Feeds.published DESC
                SKIP {page}
                LIMIT {limit}
            ', [
                'sCurrentUserIdentifier' => $sCurrentUserIdentifier,
                'sTargetUserIdentifier'  => $sTargetUserIdentifier,
                'page'                   => (int)$iPage * $iLimit,
                'limit'                  => $iLimit,
            ]);

            return $result;
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());

            return false;
        }
    }

    /**
     * @param string  $sUserIdentifier
     * @param string  $sStatus
     * @param integer $iPage Should be the page number the feed is used for skip
     * @param integer $iLimit
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function getDirectFriendsFeeds(
        $sUserIdentifier,
        $sStatus = 'accepted',
        $iPage = null,
        $iLimit = FeedConstants::FEED_DEFAULT_LIMIT
    ) {
        try {
            $neoClient = $this->neo4JManager->client;

            //what types of feeds do we show on the feeds
            $aAllowedFeedTypes = [
                ObjectTypeConstants::OBJECT_STATUS_UPDATE,
                ObjectTypeConstants::OBJECT_MEDICAL_CASE,
            ];

            $result = $neoClient->run('
            
            MATCH (me : Doctor {identifier:{doctorIdentifier}})-[rels:isFriendWith*0..1]-(myfriend),
                  (myfriend)-[:FEED_ITEM|:NEXT*1..]-(feeds)
                  
            WHERE ALL (r IN rels WHERE r.status = {status})
                
            OPTIONAL MATCH (feeds)-[:ORIGIN]->(origin)
                
            WITH me, myfriend, feeds, origin
                
            MATCH (feeds)
                
            WHERE 
                (NOT (me)-[:FEED_ITEM|:NEXT*0..]-(feeds))
            AND 
                (NOT (me)-[:IS_HIDDEN]->(feeds))
            AND 
                (feeds.deleted = "false")
            AND 
            ( 
                feeds.feedType IN {aAllowedFeedTypes} 
                
                OR
                
                origin.privacy="'.FeedConstants::FEED_PROPERTY_PUBLIC.'" 
                
                OR 
                (me)-[:isFriendWith]-(:Doctor{identifier: origin.actor})
                
                OR 
                me.identifier=origin.actor
            )
                            
                RETURN myfriend as Friend, feeds as Feeds
                ORDER BY Feeds.published DESC
                SKIP {page}
                LIMIT {limit}
            ', [
                    'doctorIdentifier'  => $sUserIdentifier,
                    'status'            => $sStatus,
                    'aAllowedFeedTypes' => array_values($aAllowedFeedTypes),
                    'page'              => (int)$iPage * $iLimit,
                    'limit'             => $iLimit,
                ]
            );

            return $result;
        } catch (\Exception $oException) {

            $this->logger->log('error', $oException->getMessage());

            return false;
        }
    }

    /**
     * Get last entry by user
     *
     * @param string $sUserIdentifier
     *
     * @return array
     */
    public function getUserLastFeedFromNeo($sUserIdentifier)
    {
        try {
            $result = $this->neo4JManager->client->run('
                MATCH (me:Doctor {identifier: {identifier}})-[:FEED_ITEM]-(feedItem) 
                
                RETURN me as Friend, feedItem as Feeds;
            ', [
                'identifier' => $sUserIdentifier,
            ]);

            return $this->returnSingleProcessedNeoRecord($result->getRecord());

        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }
    }

    /**
     * Return a single entry from neo based on passed identifier
     *
     * @param string $sFeedIdentifier
     *
     * @return array
     */
    public function getSingleFeedFromNeo($sFeedIdentifier)
    {

        try {
            $result = $this->neo4JManager->client->run('
                MATCH ()-[:FEED_ITEM|:NEXT*0..]-(feedItem) 
                WHERE feedItem.identifier = {identifier}
                AND feedItem.deleted = "false"
                RETURN feedItem as Feeds
                LIMIT 1
                ;
            ', [
                'identifier' => $sFeedIdentifier,
            ]);

            return $this->returnSingleProcessedNeoRecord($result->getRecord());

        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }

        return null;
    }

    /**
     * Returns the processed neo record ready for view
     *
     * @param Record $record
     *
     * @return array
     */
    public function returnSingleProcessedNeoRecord(Record $record)
    {
        $oFriendRecord = $record->hasValue('Friend') ? $record->get('Friend') : null;
        $oFeedRecord   = $record->get('Feeds');

        $aRecord['sFeedIdentifier']           = $oFeedRecord->get('identifier');
        $aRecord['sUserIdentifier']           = (null != $oFriendRecord) ? $oFriendRecord->get('identifier') :
            $oFeedRecord->get('actor');
        $aRecord['sOriginalObjectIdentifier'] = $oFeedRecord->hasValue('originalObjectIdentifier') ?
            $oFeedRecord->get('originalObjectIdentifier') : null;
        $aRecord['sObjectDataType']           = $oFeedRecord->get('objectDataType');
        $aRecord['sFeedType']                 = $oFeedRecord->get('feedType');
        $aRecord['sFeedPrivacy']              = $oFeedRecord->get('privacy');

        return $aRecord;
    }

    /**
     * This is a special case we are using ID instead of Identifier because neo4j nodes do not have unique identifiers
     *
     * @param string $sFeedIdentifier
     * @param string $sUserIdentifier
     *
     * @return \GraphAware\Common\Result\Result|null
     */
    public function hideFeedFromNewsfeed($sFeedIdentifier, $sUserIdentifier)
    {
        try {
            $result = $this->neo4JManager->client->run('
                MATCH 
                    (d:Doctor {identifier:{doctorIdentifier}}),
                    (node)
                WHERE node.identifier = {nodeIdentifier}
                MERGE (d)-[:IS_HIDDEN]->(node)
            ', [
                'doctorIdentifier' => $sUserIdentifier,
                'nodeIdentifier'   => $sFeedIdentifier,
            ]);

            return $result;
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }

        return null;
    }

    /**
     * @param $sFeedIdentifier
     * @param $sUserIdentifier
     *
     * @return \GraphAware\Common\Result\Result|null
     */
    public function markDeleteNewsFeedItem($sFeedIdentifier, $sUserIdentifier)
    {
        try {
            $result = $this->neo4JManager->client->run('
            MATCH 
                (d:Doctor {identifier:{sUserIdentifier}}),
                (node)
            WHERE node.identifier = {sFeedIdentifier}
            SET node.deleted = "true" //seems that true is a reserved word so we will need to make it string
            RETURN node
        ', [
                'sUserIdentifier' => $sUserIdentifier,
                'sFeedIdentifier' => $sFeedIdentifier,
            ]);

            //delete entry from mongo DB
            $oMongoEntry = $this->getMongoRepository(MongoFeed::class)->findOneBy([
                'identifier' => $sFeedIdentifier,
            ]);
            if (null != $oMongoEntry) {
                $this->deleteMongoObject($oMongoEntry);
            }

            return $result;
        } catch (\Exception $e) {
            $this->logger->log('error', $e->getMessage());
        }

        return null;
    }

    /**
     * @param $sObjectIdentifier
     *
     * @return bool|\GraphAware\Common\Result\Result|null
     */
    public function getNeo4jFeedItem($sObjectIdentifier)
    {
        try {
            $result = $this
                ->neo4JManager
                ->client
                ->run('
                    MATCH(feedItem:FeedItem {identifier:{identifier}})
                    RETURN feedItem.identifier as identifier, feedItem.createdAt as createdAt
                ', [
                    'identifier' => $sObjectIdentifier,
                ]);

            return $result;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }
    }

    /**
     * Gets and display the feed in json format. It passes all the data we need for displaying the feed item
     *
     * @param array $aFeeds
     * @param bool  $bIsPreview
     * @param bool  $bIsSelfPost
     *
     * @return array
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function fetchAndDisplayFeedByType(array $aFeeds, $bIsPreview = false, $bIsSelfPost = false)
    {
        /** @var MongoThreadRepository $oMongoThreadRepository */
        $oMongoThreadRepository = $this->getMongoRepository(MongoThread::class);
        /** @var MongoVoteRepository $oMongoVoteRepository */
        $oMongoVoteRepository = $this->getMongoRepository(MongoVote::class);
        /** @var array $aFeedResponse An array that will hold the json display of a feed */
        $aFeedResponse = [];

        foreach ($aFeeds as $aFeed) {

            $oDependencyObject       = null;
            $sOriginalFeedIdentifier = null;
            $oFeed                   = $this->getMongoRepository(MongoFeed::class)
                                            ->findOneBy(['identifier' => $aFeed['sFeedIdentifier']]);

            $aFeedResponse[] = $this->displayFeedPost( //comes from the trait
                $oFeed,
                $oMongoThreadRepository,
                $oMongoVoteRepository,
                $bIsPreview,
                $bIsSelfPost
            );

        }

        return $aFeedResponse;
    }

    /**
     * Display preview for shares that are not coming from feed page (ex. Medical cases)
     *
     * @param null|array  $aEntry
     * @param null|string $sType
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function displayPreviewForSharesByType($aEntry = null, $sType = null)
    {
        return $this->twig->render('@Doctors/Feeds/FeedsViewByType/feed_'.$sType.'.html.twig', [
            'doctor'                  => $this->getRepository(Doctors::class)->findOneBy([
                'identifier' => $aEntry['doctorIdentifier'],
            ]),
            //object identifier from Mongo
            'sFeedIdentifier'         => $aEntry['identifier'],
            'sFeedType'               => $sType,
            'sFeedPrivacy'            => null,
            //we need to use this one not to conflict with multiple objectIdentifiers
            'data'                    => $aEntry,
            'createdAt'               => $aEntry['createdAt']->format('d M Y - H:i'),
            'isPreview'               => true,
            'isSelfPost'              => true,
            //the previous identifier will be used for shares to keep the original identifier on
            'sOriginalFeedIdentifier' => $aEntry['identifier'],
            'feedShareTypeText'       => $this->translator->trans('doctors.feeds.type.status.shared'),
            'sharedPostHtmlContent'   => $this->sharedPostHtmlContent($aEntry['identifier']),
            //we need to use this one in order to specify object type for Comments module. EntityKey is page specific
            'sEntityKey'              => self::$aEntityToKey[MongoFeed::class],
            'notificationIdentifier'  => null,
            'likeThreadType'          => CommentsManager::LIKE_TYPE_THREAD,
            'likeNotificationType'    => NotificationManager::NEW_LIKE_NOTIFICATION,
            'isLiked'                 => false,
            'totalVotesNumber'        => 0,
            'isShared'                => false,
            'isEditable'              => true,
            'isCommentable'           => false,
        ]);
    }

    /**
     *
     * Updates feed items
     *
     * @param MongoFeed $oMongoEntry
     * @param array     $aOriginalObjectsValues
     *
     * @throws \GraphAware\Neo4j\Client\Exception\Neo4jException
     */
    public function updateFeedItem(MongoFeed $oMongoEntry)
    {
        $oMongoEntry->setUrlContent($this->parseFeedLink($oMongoEntry->getContent()));

        $this->saveMongoObject($oMongoEntry);

        $this->updateNeo4JNode($oMongoEntry);
    }

    /**
     * Save a shared item on the feed
     *
     * @param mixed           $oFormData
     * @param Doctors         $oCurrentUser
     * @param CommentsManager $oCommentsManager
     * @param string          $sShareType
     */
    public function saveSharedFeedItem($oFormData, $oCurrentUser, $oCommentsManager, $sShareType)
    {
        $oFormData->setCreatorUserIdentifier($oCurrentUser->getIdentifier());
        $oFormData->setObjectDataType(ObjectTypeConstants::OBJECT_DATA_TYPE_MONGO);
        $oFormData->setFeedEntityType(ObjectTypeConstants::OBJECT_MONGO_FEED);

        $oFormData->setFeedType($sShareType);
        $oFormData->setObjectEntityType(ObjectTypeConstants::OBJECT_MONGO_FEED);

        $oFormData->setUrlContent($this->parseFeedLink($oFormData->getContent()));

        if ($sShareType === ObjectTypeConstants::OBJECT_SHARE_ON_NEWSFEED) {
            $oFormData->setIsShared(true);
        }

        $this->saveMongoObject($oFormData);

        //add entry to neo4j
        $this->addNeo4JFeedItemNode(
            $oCurrentUser->getIdentifier(),
            $sShareType,
            $oFormData->getIdentifier(),
            ObjectTypeConstants::OBJECT_DATA_TYPE_MONGO,
            (null != $oFormData->getOriginalObjectIdentifier()) ? $oFormData->getOriginalObjectIdentifier() : null,
            $oFormData->getPrivacy(),
            (null != $oFormData->getOriginalObjectIdentifier()) ? true : false
        );

        /*
         * Create a MongoThread related to this MongoFeed item.
         * If MongoThread was already created do not make another one.
         */
        $oCommentsManager->addMongoThread(
            self::$aEntityToKey[MongoFeed::class],
            $oFormData->getIdentifier(),
            $oFormData->getCreatorUserIdentifier()
        );
    }
}
