<?php

namespace Flendoc\AppBundle\Manager\Notifications;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Document\Notifications\MongoNotification;
use Flendoc\AppBundle\Entity\AbstractEntity;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Repository\Comments\MongoCommentRepository;
use Flendoc\AppBundle\Repository\Comments\MongoThreadRepository;
use Flendoc\AppBundle\Repository\Notifications\MongoNotificationRepository;
use Flendoc\AppBundle\Traits\App\ObjectKeyActionTrait;
use Flendoc\AppBundle\Traits\App\ObjectKeyEntityTrait;
use Flendoc\AppBundle\Traits\Contacts\ContactRequestDefaultStatusesTrait;
use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class NotificationManager
 * @package Flendoc\AppBundle\Manager\Notifications
 */
class NotificationManager extends FlendocManager
{
    use ObjectKeyEntityTrait;
    use ObjectKeyActionTrait;
    use ContactRequestDefaultStatusesTrait;

    const NEW_MESSAGE_NOTIFICATION = 'new_message';
    const NEW_CONTACT_REQUEST_NOTIFICATION = 'new_contact_request';
    const NEW_CONTACT_REQUEST_ACCEPTED_NOTIFICATION = 'new_contact_request_accepted';
    const NEW_COMMENT_NOTIFICATION = 'new_comment';
    const NEW_LIKE_NOTIFICATION = 'new_like';
    const NEW_COMMENT_LIKE_NOTIFICATION = 'new_comment_like';

    /** @var PusherInterface $zmqPusher */
    protected $zmqPusher;

    /** @var Logger $logger */
    protected $logger;

    /** @var Router $router */
    protected $router;

    /**
     * NotificationManager constructor.
     *
     * @param PusherInterface $oZmqPusher
     * @param Logger          $oLogger
     * @param Router          $oRouter
     */
    public function __construct(
        PusherInterface $oZmqPusher,
        Logger $oLogger,
        Router $oRouter
    ) {
        $this->zmqPusher = $oZmqPusher;
        $this->logger    = $oLogger;
        $this->router    = $oRouter;
    }

    /**
     * Notifies one or more users based on the notification type.
     *
     * @param string      $sNotificationType
     * @param string      $sSenderIdentifier
     * @param string      $sReceiverIdentifier
     * @param string|null $sDependencyIdentifier
     * @param string|null $sDependencyType
     * @param string|null $sObjectIdentifier
     * @param string|null $sParentObjectIdentifier
     */
    public function notify(
        $sNotificationType,
        $sSenderIdentifier,
        $sReceiverIdentifier,
        $sDependencyIdentifier = null,
        $sDependencyType = null,
        $sObjectIdentifier = null,
        $sParentObjectIdentifier = null
    ) {
        switch ($sNotificationType) {
            case self::NEW_COMMENT_NOTIFICATION:
                $savedCommentsNotifications = $this->saveCommentsNotifications(
                    $sNotificationType,
                    $sSenderIdentifier,
                    $sDependencyIdentifier,
                    $sDependencyType,
                    $sObjectIdentifier,
                    $sParentObjectIdentifier
                );

                if ($savedCommentsNotifications) {
                    foreach ($savedCommentsNotifications as $sReceiverIdentifier => $savedNotification) {
                        $this->zmqPusher->push(
                            [
                                'notificationIdentifier' => $savedNotification->getIdentifier(),
                                'notificationType'       => $sNotificationType,
                                'senderIdentifier'       => $sSenderIdentifier,
                                'receiverIdentifier'     => $sReceiverIdentifier,
                                'dependencyIdentifier'   => $sDependencyIdentifier,
                                'dependencyType'         => $sDependencyType,
                                'objectIdentifier'       => $sObjectIdentifier
                            ],
                            'doctor_notification_topic',
                            [
                                'identifier' => $sReceiverIdentifier,
                            ]
                        );
                    }
                }

                break;
            default:
                /** @var MongoNotification|bool $savedNotification */
                $savedNotification = $this->saveNotification(
                    $sNotificationType,
                    $sSenderIdentifier,
                    $sReceiverIdentifier,
                    $sDependencyIdentifier,
                    $sDependencyType,
                    $sObjectIdentifier
                );

                if ($savedNotification) {
                    $this->zmqPusher->push(
                        [
                            'notificationIdentifier' => $savedNotification->getIdentifier(),
                            'notificationType'       => $sNotificationType,
                            'senderIdentifier'       => $sSenderIdentifier,
                            'receiverIdentifier'     => $sReceiverIdentifier,
                            'dependencyIdentifier'   => $sDependencyIdentifier,
                            'dependencyType'         => $sDependencyType,
                            'objectIdentifier'       => $sObjectIdentifier
                        ],
                        'doctor_notification_topic',
                        [
                            'identifier' => $sReceiverIdentifier,
                        ]
                    );
                }
        }
    }

    /**
     * Removes notifications by type.
     * This is useful especially on messenger notifications.
     *
     * @param string      $sNotificationType
     * @param string      $sUserIdentifier
     * @param string|null $sDependencyIdentifier
     */
    public function removeNotifications($sNotificationType, $sUserIdentifier, $sDependencyIdentifier = null)
    {
        /** @var MongoNotificationRepository $oMongoNotificationRepository */
        $oMongoNotificationRepository = $this->getMongoRepository(MongoNotification::class);

        try {
            $oMongoNotificationRepository->removeNotificationsByType($sNotificationType, $sUserIdentifier, $sDependencyIdentifier);
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());
        }
    }

    /**
     * Function that saves a notification if valid.
     *
     * @param string      $sNotificationType
     * @param string      $sSenderIdentifier
     * @param string      $sReceiverIdentifier
     * @param string      $sDependencyIdentifier
     * @param string      $sDependencyType
     * @param string|null $sObjectIdentifier
     *
     * @return bool|MongoNotification
     */
    protected function saveNotification(
        $sNotificationType,
        $sSenderIdentifier,
        $sReceiverIdentifier,
        $sDependencyIdentifier,
        $sDependencyType,
        $sObjectIdentifier = null
    ) {
        $bValidToSave = $this->isValidToSave($sNotificationType, $sReceiverIdentifier, $sDependencyIdentifier);

        if ($bValidToSave) {
            /** @var MongoNotification $oMongoNotification */
            $oMongoNotification = $this->createObject(MongoNotification::class);
            $oMongoNotification
                ->setSenderIdentifier($sSenderIdentifier)
                ->setReceiverIdentifier($sReceiverIdentifier)
                ->setType($sNotificationType)
                ->setDependencyIdentifier($sDependencyIdentifier)
                ->setDependencyType($sDependencyType)
                ->setObjectIdentifier($sObjectIdentifier)
            ;

            $this->saveMongoObject($oMongoNotification);

            return $oMongoNotification;
        }

        return false;
    }

    /**
     * Function that checks if a notification is already
     * sent for messenger conversation.
     *
     * @param string $sNotificationType
     * @param string $sReceiverIdentifier
     * @param string $sDependencyIdentifier
     *
     * @return bool
     */
    protected function isValidToSave($sNotificationType, $sReceiverIdentifier, $sDependencyIdentifier)
    {
        if (self::NEW_MESSAGE_NOTIFICATION === $sNotificationType) {
            $oMongoNotificationRepository = $this->getMongoRepository(MongoNotification::class);

            /** @var MongoNotification $oMongoNotification */
            $oMongoNotification = $oMongoNotificationRepository
                ->findOneBy([
                    'receiverIdentifier'   => $sReceiverIdentifier,
                    'type'                 => self::NEW_MESSAGE_NOTIFICATION,
                    'dependencyIdentifier' => $sDependencyIdentifier,
                ])
            ;

            if ($oMongoNotification) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function that saves notifications raised by new comments and new likes.
     *
     * @param string      $sNotificationType
     * @param string      $sSenderIdentifier
     * @param string      $sDependencyIdentifier
     * @param string      $sDependencyType
     * @param string|null $sObjectIdentifier
     * @param string|null $sParentObjectIdentifier
     *
     * @return array|bool
     */
    protected function saveCommentsNotifications(
        $sNotificationType,
        $sSenderIdentifier,
        $sDependencyIdentifier,
        $sDependencyType,
        $sObjectIdentifier,
        $sParentObjectIdentifier
    ) {
        /** @var MongoThreadRepository $oMongoThreadRepository */
        $oMongoThreadRepository = $this->getMongoRepository(MongoThread::class);
        /** @var MongoCommentRepository $oMongoCommentRepository */
        $oMongoCommentRepository = $this->getMongoRepository(MongoComment::class);
        $aSavedNotifications = [];
        $oParentMongoComment = null;

        /** @var MongoThread $oMongoThread */
        $oMongoThread = $oMongoThreadRepository
            ->findOneBy([
                'dependencyIdentifier' => $sDependencyIdentifier
            ])
        ;

        if ($sParentObjectIdentifier) {
            /** @var MongoComment $oParentMongoComment */
            $oParentMongoComment = $oMongoCommentRepository
                ->findOneBy([
                    'identifier' => $sParentObjectIdentifier,
                ])
            ;
        }

        try {
            $aMongoCommentsUsers = $oMongoCommentRepository
                ->findCommentsUserIdentifiers(
                    $oMongoThread->getId(),
                    $sParentObjectIdentifier ? true : false,
                    $sSenderIdentifier,
                    $sParentObjectIdentifier && $oParentMongoComment ? $oParentMongoComment->getId() : null
                )
            ;
        } catch (\Exception $oException) {
            $this->logger->info($oException->getMessage());

            return false;
        }

        /** @var MongoComment $oMongoComment */
        foreach ($aMongoCommentsUsers as $sMongoCommentUserIdentifier) {
            if (!array_key_exists($sMongoCommentUserIdentifier, $aSavedNotifications)) {
                $oMongoNotification = $this->saveNotification(
                    $sNotificationType,
                    $sSenderIdentifier,
                    $sMongoCommentUserIdentifier,
                    $sDependencyIdentifier,
                    $sDependencyType,
                    $sObjectIdentifier
                );

                $aSavedNotifications[$sMongoCommentUserIdentifier] = $oMongoNotification;
            }
        }

        if ($sParentObjectIdentifier && $oParentMongoComment) {
            /*
             * Add the author of the parent comment if
             * he is not already through the already selected users.
             */
            if (!array_key_exists($oParentMongoComment->getUserIdentifier(), $aSavedNotifications)) {
                $oMongoNotification = $this->saveNotification(
                    $sNotificationType,
                    $sSenderIdentifier,
                    $oParentMongoComment->getUserIdentifier(),
                    $sDependencyIdentifier,
                    $sDependencyType,
                    $sObjectIdentifier
                );

                $aSavedNotifications[$oParentMongoComment->getUserIdentifier()] = $oMongoNotification;
            }
        } else {
            /*
             * Add dependency author if he is not
             * already through the already selected users.
             */
            if (!array_key_exists($oMongoThread->getUserIdentifier(), $aSavedNotifications)) {
                $oMongoNotification = $this->saveNotification(
                    $sNotificationType,
                    $sSenderIdentifier,
                    $oMongoThread->getUserIdentifier(),
                    $sDependencyIdentifier,
                    $sDependencyType,
                    $sObjectIdentifier
                );

                $aSavedNotifications[$oMongoThread->getUserIdentifier()] = $oMongoNotification;
            }
        }

        return $aSavedNotifications;
    }

    /**
     * Function that marks a notification or many notifications as read.
     *
     * @param string      $sDependencyType
     * @param string      $sUserIdentifier
     * @param string|null $sNotificationIdentifier
     *
     * @return bool
     */
    public function markAsRead($sDependencyType, $sUserIdentifier, $sNotificationIdentifier = null)
    {
        /** @var MongoNotificationRepository $oMongoNotificationRepository */
        $oMongoNotificationRepository = $this->getMongoRepository(MongoNotification::class);

        switch ($sDependencyType) {
            case ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST:
                $aMongoNotifications = $oMongoNotificationRepository
                    ->findBy([
                        'type'               => self::NEW_CONTACT_REQUEST_NOTIFICATION,
                        'receiverIdentifier' => $sUserIdentifier,
                    ])
                ;

                /** @var MongoNotification $oMongoNotification */
                foreach ($aMongoNotifications as $oMongoNotification) {
                    $oMongoNotification->setIsRead(true);
                    $this->saveMongoObject($oMongoNotification);
                }

                break;

            case ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST_ACCEPTED:
                $aMongoNotifications = $oMongoNotificationRepository
                    ->findBy([
                        'type'               => self::NEW_CONTACT_REQUEST_ACCEPTED_NOTIFICATION,
                        'receiverIdentifier' => $sUserIdentifier,
                    ])
                ;

                /** @var MongoNotification $oMongoNotification */
                foreach ($aMongoNotifications as $oMongoNotification) {
                    $oMongoNotification->setIsRead(true);
                    $this->saveMongoObject($oMongoNotification);
                }

                break;

            default:
                /** @var MongoNotification $oMongoNotification */
                $oMongoNotification = $oMongoNotificationRepository
                    ->findOneBy([
                        'identifier'         => $sNotificationIdentifier,
                        'receiverIdentifier' => $sUserIdentifier,
                    ])
                ;

                if ($oMongoNotification) {
                    $oMongoNotification->setIsRead(true);
                    $this->saveMongoObject($oMongoNotification);
                }
        }

        return true;
    }

    /**
     * Checks if a notification is valid or not.
     *
     * @param string $sNotificationIdentifier
     *
     * @return bool
     */
    public function checkValidNotification($sNotificationIdentifier) {
        /** @var MongoNotificationRepository $oMongoNotificationRepository */
        $oMongoNotificationRepository = $this->getMongoRepository(MongoNotification::class);
        /** @var MongoThreadRepository $oMongoThreadRepository */
        $oMongoThreadRepository = $this->odm->getRepository(MongoThread::class);
        /** @var MongoCommentRepository $oMongoCommentRepository */
        $oMongoCommentRepository = $this->odm->getRepository(MongoComment::class);

        /** @var MongoNotification $oMongoNotification */
        $oMongoNotification = $oMongoNotificationRepository
            ->findOneBy([
                'identifier' => $sNotificationIdentifier,
            ])
        ;

        if ($oMongoNotification) {
            $sDependencyIdentifier = $oMongoNotification->getDependencyIdentifier();
            $sObjectIdentifier = $oMongoNotification->getObjectIdentifier();

            /** @var MongoThread $oMongoThread */
            $oMongoThread = $oMongoThreadRepository
                ->findOneBy([
                    'dependencyIdentifier' => $sDependencyIdentifier,
                ])
            ;

            if (!$oMongoThread) {
                return false;
            }

            $aEntityTypes = array_keys(self::$aKeyToEntityTypes);

            if (in_array($oMongoNotification->getDependencyType(), $aEntityTypes)) {
                /** @var DocumentManager|EntityManager $oManager */
                $sEntityKey = $oMongoNotification->getDependencyType();
                $sEntityIdentifier = $oMongoNotification->getDependencyIdentifier();
                $oManager = is_subclass_of(self::$aKeyToEntityTypes[$sEntityKey], AbstractEntity::class) ?
                    $this->getEntityManager() :
                    $this->getMongoDbManager()
                ;

                /**
                 * @var MedicalCases|MongoFeed $oDependencyObject
                 */
                $oDependencyObject = $oManager
                    ->getRepository(self::$aKeyToEntityTypes[$sEntityKey])
                    ->findOneBy([
                        'identifier' => $sEntityIdentifier
                    ])
                ;

                if (!$oDependencyObject) {
                    return false;
                }
            }

            if ($oMongoNotification->getType() !== self::NEW_LIKE_NOTIFICATION) {
                /** @var MongoComment $oMongoComment */
                $oMongoComment = $oMongoCommentRepository
                    ->findOneBy([
                        'identifier' => $sObjectIdentifier
                    ])
                ;

                if (!$oMongoComment) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Get redirect URI for a notification based on notification dependency type.
     *
     * @param string $sDependencyType
     * @param string $sDependencyIdentifier
     * @param string $sNotificationIdentifier
     *
     * @return string
     */
    public function getUriByDependencyType($sDependencyType, $sDependencyIdentifier, $sNotificationIdentifier)
    {
        switch ($sDependencyType) {
            case ObjectTypeConstants::OBJECT_MEDICAL_CASE:
                $oMedicalCase = $this
                    ->getRepository(MedicalCases::class)
                    ->findOneBy([
                        'identifier' => $sDependencyIdentifier,
                    ])
                ;

                $sUri = $this->router->generate(
                    self::$aKeyToActions[$sDependencyType],
                    [
                        'sIdentifier'             => $sDependencyIdentifier,
                        'sMedicalCaseLanguage'    => $oMedicalCase->getMedicalCaseLanguageIso(),
                        'sMedicalCaseSlug'        => $oMedicalCase->getSlugTitle(),
                        'sNotificationIdentifier' => $sNotificationIdentifier,
                    ]
                );

                break;
            case ObjectTypeConstants::OBJECT_MONGO_FEED:
                $sUri = $this->router->generate(
                    self::$aKeyToActions[$sDependencyType],
                    [
                        'sIdentifier'             => $sDependencyIdentifier,
                        'sNotificationIdentifier' => $sNotificationIdentifier,
                    ]
                );

                break;
            case ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST:
                $sUri = $this->router->generate(
                    self::$aKeyToActions[$sDependencyType],
                    [
                        'contactsCategory' => self::$statusPending,
                    ]
                );

                break;
            case ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST_ACCEPTED:
                $sUri = $this->router->generate(self::$aKeyToActions[$sDependencyType]);

                break;
            default:

                $sUri = '';
        }

        return $sUri;
    }
}
