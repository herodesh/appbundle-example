<?php

namespace Flendoc\AppBundle\Manager;

/**
 * Interface FlendocManagerInterface
 * @package Flendoc\AppBundle\Manager
 */
interface FlendocManagerInterface
{
    /**
     * @param $oObject
     */
    public function saveObject($oObject);

    /**
     * @param $oObject
     *
     * @return mixed
     */
    public function saveMongoObject($oObject);

    /**
     * @param $oObject
     */
    public function deleteObject($oObject): void;

    /**
     * @param $sObject
     *
     * @return object
     */
    public function createObject($sObject);
}
