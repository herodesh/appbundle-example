<?php

namespace Flendoc\AppBundle\Traits\Mails;

use Flendoc\AppBundle\Entity\Mails\MailsConstants;

/**
 * Trait AdminsNotificationsMailsTrait
 * @package Flendoc\AppBundle\Traits\Mails
 */
trait AdminsNotificationsMailsTrait
{
    /**
     * Set adminsNotificationsMails
     */
    public function getAdminsNotificationsMails()
    {
        return [
            MailsConstants::DOCTORS_FEEDBACK => [
                'emailIdentifier'  => MailsConstants::DOCTORS_FEEDBACK,
                'variables'        => "userEmail,content",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New feedback from doctors website notification',
                        'subject'          => 'Flendoc.com - New feedback from doctors area',
                        'content'          => '
                        <h2>Hi Admin</h2> 
                        You have a new feedback from %s.<br />
                        Message content:<br />
                        %s',
                    ],
                ],
            ],

            MailsConstants::DOCTORS_CONTACT_US => [
                'emailIdentifier'  => MailsConstants::DOCTORS_CONTACT_US,
                'variables'        => "senderEmail,subject,content",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New message from "Contact us" Form',
                        'subject'          => 'Flendoc.com - New "Contact us" message',
                        'content'          => '
                        <h2>Hi Admin</h2> 
                        You have a new message from <strong>%s</strong>, with subject <strong>%s</strong>.<hr />
                        <h4>Message content:</h4>
                        %s',
                    ],
                ],
            ],

            MailsConstants::NOTIFY_ADMIN_OF_NEW_USER_REGISTRATION => [
                'emailIdentifier'  => MailsConstants::NOTIFY_ADMIN_OF_NEW_USER_REGISTRATION,
                'variables'        => "day,content",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New user registration notification',
                        'subject'          => 'Flendoc.com - New user registration notification',
                        'content'          => '
                        <h2>Hi Admin</h2> 
                        Today %s we have new registered users on our platform: <br />
                        <strong>%s</strong>',
                    ],
                ],
            ],

            MailsConstants::NOTIFY_ADMIN_OF_NEWLY_SUBMITTED_VERIFICATION_DOCUMENTS => [
                'emailIdentifier'  => MailsConstants::NOTIFY_ADMIN_OF_NEWLY_SUBMITTED_VERIFICATION_DOCUMENTS,
                'variables'        => "content",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New submitted verification documents notification',
                        'subject'          => 'Flendoc.com - New submitted verification documents notification',
                        'content'          => '
                        <h2>Hi Admin</h2> 
                        You have received confirmation documents from the following users: <br />
                        %s',
                    ],
                ],
            ],

            MailsConstants::ADMIN_NEW_ACCOUNT => [
                'emailIdentifier'  => MailsConstants::ADMIN_NEW_ACCOUNT,
                'variables'        => "firstName,adminLink,generatedPassword",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New admin account',
                        'subject'          => 'Flendoc.com - New admin account',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        You have been added as an admin on <a href="%s">Flendoc.com</a>. <br />
                        Your username is your email address and your password is <strong>%s</strong>.<br />
                        It is recommended that you change your password for security reasons',
                    ],
                ],
            ],
        ];

    }
}
