<?php

namespace Flendoc\AppBundle\Traits\Mails;

use Flendoc\AppBundle\Entity\Mails\MailsConstants;

/**
 * Trait MedicalCasesMailsTrait
 * @package Flendoc\AppBundle\Traits\Mails
 */
trait MedicalCasesMailsTrait
{
    /**
     * @return array
     */
    public function getMedicalCasesMails()
    {

        return [

            MailsConstants::MEDICAL_CASE_APPROVED => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASE_APPROVED,
                'variables'        => 'firstName,medicalCaseUrl',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Medical case  - Accepted medical case',
                        'subject'          => 'Flendoc.com - Your medical case was accepted',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        You have submitted a new medical case or changes to an existing one.<br /> 
                        
                        We want to inform you that your changes has been approved and can be visible by other users.<br />
                         
                        Click <a href="%s">here</a> to view your medical case.
                    ',
                    ],
                ],
            ],

            MailsConstants::MEDICAL_CASE_USER_ACTION_REQUIRED => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASE_USER_ACTION_REQUIRED,
                'variables'        => 'firstName,requiredAction,termsAndConditionsUrl,medicalCaseUrl',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Medical case admin review - User action required',
                        'subject'          => 'Flendoc.com - Your medical case post needs updates',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        You have submitted a new medical case or changes to an existing one. 
                        After review we noticed that your post needs the following updates:<br />
                        
                        %s<br />
                        
                        Update your changes so it does not conflict with our <a href="%s">terms and conditions</a><br />
                        
                        Please note that your changes were marked as <strong>unpublished</strong>. This means that you have 
                        limited time to update your changes before they get permanently deleted from our system.<br />
                        
                        <em>
                        If your medical case was already approved, and the unpublished changes are deleted from our system, 
                        the original version will NOT be deleted!
                        </em> 
                         
                        Click <a href="%s">here</a> to edit your medical case.
                    ',
                    ],
                ],
            ],

            MailsConstants::MEDICAL_CASE_DELETED => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASE_DELETED,
                'variables'        => 'firstName,datePosted,communityGuides,myMedicalCases,supportEmail',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Medical case not approved',
                        'subject'          => 'Flendoc.com - Your medical case was not approved',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        <p>Your medical case posted on %s was not approved.</p>
                        The reason for this action is that you did not follow the <a href="%s">Community guides</a> when posting your case.<br />
                        Also we would like to inform you that the case was deleted from our systems and, if any, all the images attached to it were deleted as well.<br /> 
                        If you want to post a new medical case click <a href="%s">here</a> to go to your medical cases area.<br />
                        If you have further questions about how to post medical cases on our platform please send us 
                        an <a href="mailto:%s?subject=How to create a new medical case">email</a>. 
                    ',
                    ],
                ],
            ],

            MailsConstants::MEDICAL_CASE_REENABLED => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASE_REENABLED,
                'variables'        => 'firstName,medicalCaseTitle,medicalCaseEditUrl',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Your medical case is ready to be edited',
                        'subject'          => 'Flendoc.com - Your medical case can now be edited',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        <p>Your medical case <strong>"%s"</strong> is now ready to be edited.</p>
                        Please <a href="%s">review your medical case</a> and when you are ready with your changes we will review it and
                        approve it for public view. 
                    ',
                    ],
                ],
            ],

            MailsConstants::MEDICAL_CASE_UNPUBLISHED_AUTOMATIC_DELETION => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASE_UNPUBLISHED_AUTOMATIC_DELETION,
                'variables'        => 'firstName,rulesOfPostingMedicalCasesUrl,createMedicalUrl',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Medical case  - Deletion of unpublished medical cases',
                        'subject'          => 'Flendoc.com - Your medical case was automatically deleted',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        You have started adding a medical case into our system but never finished it up.<br /> 
                        
                        Based on our <a href="%s">rules of posting medical cases</a> we want to let you know that the 
                        medical cases that are not being published and being abandoned will be automatically deleted 
                        from our system.<br />
                        
                        If you want to add a new medical case please feel free to do so <a href="%s">here</a>.
                    ',
                    ],
                ],
            ],

            MailsConstants::NEW_MEDICAL_CASE_ADDED => [
                'emailIdentifier'  => MailsConstants::NEW_MEDICAL_CASE_ADDED,
                'variables'        => 'firstName,myMedicalCases',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New medical case is pending approval',
                        'subject'          => 'Flendoc.com - New medical case is pending approval',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        A new medical case is waiting for your approval.
                        Click <a href="%s">here</a> to go to medical cases admin area.
                    ',
                    ],
                ],
            ],

            MailsConstants::MEDICAL_CASES_AWAITS_ADMIN_APPROVAL => [
                'emailIdentifier'  => MailsConstants::MEDICAL_CASES_AWAITS_ADMIN_APPROVAL,
                'variables'        => 'numberOfCases,pendingMedicalCasesUrl',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => true,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal' => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Medical cases that needs to be approved',
                        'subject'          => 'Flendoc.com - Medical cases needs to be approved',
                        'content'          => '
                        <h2>Hi, admin</h2>
                        There are <strong>%s</strong> medical cases that require your attention.<br />
                        Click <a href="%s">here</a> to go to pending medical cases admin area.
                    ',
                    ],
                ],
            ],
        ];
    }
}
