<?php

namespace Flendoc\AppBundle\Traits\Mails;

use Flendoc\AppBundle\Entity\Mails\MailsConstants;

/**
 * Trait ValidationDocumentsMailsTrait
 * @package Flendoc\AppBundle\Traits\Mails
 */
trait ValidationDocumentsMailsTrait
{
    /**
     * @return array
     */
    public function getValidationDocumentsMails()
    {
        return [
            MailsConstants::VALIDATION_DOCUMENTS_ACCEPTED => [
                'emailIdentifier'  => MailsConstants::VALIDATION_DOCUMENTS_ACCEPTED,
                'variables'        => 'firstName,url',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Accepted documents notification',
                        'subject'          => 'Flendoc.com - Your documents were accepted',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        Thank you for submitting your documents to us for validation.<br />
                        We are happy to inform you that your documents are valid and your account is verified.<br />
                        Click <a href="%s">here</a> to go to Flendoc.com.
                    ',
                    ],
                ],
            ],

            MailsConstants::ACCOUNT_ACTIVATED => [
                'emailIdentifier'  => MailsConstants::ACCOUNT_ACTIVATED,
                'variables'        => 'firstName,url',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Account activated by admin',
                        'subject'          => 'Flendoc.com - Your account is now active',
                        'content'          => '
                        <h2>Hi, %s</h2>
                        Thank you for registering with Flendoc.com.<br />
                        We are happy to inform you that your account is now verified and active.<br />
                        Click <a href="%s">here</a> to go to Flendoc.com.
                    ',
                    ],
                ],
            ],

            MailsConstants::VALIDATION_DOCUMENTS_INCOMPLETE => [
                'emailIdentifier'  => MailsConstants::VALIDATION_DOCUMENTS_INCOMPLETE,
                'variables'        => 'firstName,issueList',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Documents problem notification',
                        'subject'          => 'Flendoc.com - There is a problem with your submitted documents',
                        'content'          => '
                    <h2>Hi, %s</h2>
                    Thank you for submitting your documents to us for validation.<br />
                    Unfortunately there are problems with your documents.<br />
                    Please check the list of issues and fix them in order to get verified:<br />
                    %s',
                    ],
                ],
            ],

            MailsConstants::VALIDATION_DOCUMENTS_NOT_SENT => [
                'emailIdentifier'  => MailsConstants::VALIDATION_DOCUMENTS_NOT_SENT,
                'variables'        => 'firstName,homeUrl,documentsUrl,reminder',
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Documents notification',
                        'subject'          => 'Flendoc.com - No documents associated to your account',
                        'content'          => '
                    <h2>Hi, %s</h2>
                    Thank you for registering to <a href="%s">Flendoc.com.</a><br />
                    In order to access Flendoc.com you need to provide proof that you are a medical professional, researcher or student.<br />
                    We did not received this documents yet. Click here to <a href="%s">submit your documents</a>.<br />
                    %s.<br />
                    ',
                    ],
                ],
            ],
        ];
    }
}
