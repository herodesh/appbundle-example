<?php

namespace Flendoc\AppBundle\Traits\Mails;

use Flendoc\AppBundle\Entity\Mails\MailsConstants;

/**
 * Trait DoctorNotificationsMailsTrait
 * @package Flendoc\AppBundle\Traits\Mails
 */
trait DoctorNotificationsMailsTrait
{
    /**
     * Set doctorNotificationsMails
     */
    public function getDoctorNotificationsMails()
    {
        return [
            MailsConstants::REGISTRATION_CONFIRMATION => [
                'emailIdentifier'  => MailsConstants::REGISTRATION_CONFIRMATION,
                'variables'        => "firstName,activationLink",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Registration confirmation notification',
                        'subject'          => 'Flendoc.com - Registration confirmation',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        Thank you for registering on our portal.<br />
                        You need to confirm your account in order to be able to use it.<br />
                        Click <a href="%s">here</a> to activate your account.',
                    ],
                ],
            ],

            MailsConstants::RESET_PASSWORD => [
                'emailIdentifier'  => MailsConstants::RESET_PASSWORD,
                'variables'        => "firstName,action",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Request password reset notification',
                        'subject'          => 'Flendoc.com - Password reset requested',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        You have asked for a new password.<br />
                        Click <a href="%s">here</a> in order to reset your password<br />',
                    ],
                ],
            ],

            MailsConstants::CONTACT_REQUEST_NOTIFICATION => [
                'emailIdentifier'  => MailsConstants::CONTACT_REQUEST_NOTIFICATION,
                'variables'        => "firstName,requesterName,manageLink",
                'subjectVariables' => "requesterName",
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'A new contact request notification',
                        'subject'          => 'Flendoc.com - %s sent you a contact request',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        You have a new contact request from %s.<br />
                        Click the <a href="%s">following link</a> to go to contact requests management page.',
                    ],
                ],
            ],

            MailsConstants::CONTACT_REQUEST_ACCEPTED => [
                'emailIdentifier'  => MailsConstants::CONTACT_REQUEST_ACCEPTED,
                'variables'        => "firstName,acceptUser,manageLink,sendMessage",
                'subjectVariables' => "contactName",
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'Contact accepted your contact request',
                        'subject'          => 'Flendoc.com - %s is now your contact',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        Your contact request to %s was accepted.<br />
                        Click the <a href="%s">following link</a> to go to contact requests management page 
                        or send him a <a href="%s">message</a>.',
                    ],
                ],
            ],

            MailsConstants::NEW_MESSAGE_NOTIFICATION => [
                'emailIdentifier'  => MailsConstants::NEW_MESSAGE_NOTIFICATION,
                'variables'        => "firstName,senderName,inboxLink",
                'subjectVariables' => "senderName",
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => false,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New message notification',
                        'subject'          => 'Flendoc.com - %s sent you a new message',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        You have received a new message from %s.<br />
                        Click <a href="%s">here</a> to open your inbox.',
                    ],
                ],
            ],

            MailsConstants::CONTACT_INVITATION => [
                'emailIdentifier'  => MailsConstants::CONTACT_INVITATION,
                'variables'        => "senderName,urlLink",
                'subjectVariables' => "senderName",
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => true,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'New invitation to website',
                        'subject'          => 'Flendoc.com - %s invites you to join Flendoc.com',
                        'content'          => '
                        <h2>Hi,</h2> 
                        %s has sent you an invitation to join Flendoc.com<br />
                        Flendoc.com is a global digital community enabling all medical professionals to connect and collaborate with peers,
                        disseminate knowledge, start relevant discussions and debate health care policy and medical practice issues.<br />
                        Click <a href="%s">here</a> to open Flendoc.com.',
                    ],
                ],
            ],

            MailsConstants::ACCOUNT_LOCKED => [
                'emailIdentifier'  => MailsConstants::ACCOUNT_LOCKED,
                'variables'        => "firstName,lockReason,contactUrl",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'User account locked',
                        'subject'          => 'Flendoc.com - Your account has been locked',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        Your account has been <strong>locked</strong>. The reason for locking your account is:<br />
                        - %s<br />
                        <a href="%s">Contact us</a> in order to solve this issue.',
                    ],
                ],
            ],

            MailsConstants::ACCOUNT_UNLOCKED => [
                'emailIdentifier'  => MailsConstants::ACCOUNT_UNLOCKED,
                'variables'        => "firstName,flendocUrl",
                'subjectVariables' => null,
                'isActive'         => true,
                'isAdministrative' => false,
                'isAccount'        => true,
                'isPatient'        => false,
                'isExternal'       => false,
                'content'          => [
                    'en' => [
                        'shortDescription' => 'User account unlocked',
                        'subject'          => 'Flendoc.com - Your account has been unlocked',
                        'content'          => '
                        <h2>Hi, %s</h2> 
                        Your account has been <strong>unlocked</strong>. You can now login using your credentials.
                        <br />
                        Go to <a href="%s">Flendoc.com</a> now.',
                    ],
                ],
            ],
        ];
    }
}
