<?php

namespace Flendoc\AppBundle\Traits\Mails;

/**
 * Keeps the default email messages in english
 *
 * Trait MailsDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Mails
 */
trait MailsDefaultPropertiesTrait
{
    use AdminsNotificationsMailsTrait;
    use DoctorNotificationsMailsTrait;
    use MedicalCasesMailsTrait;
    use ValidationDocumentsMailsTrait;

    /**
     * @var array $aMails
     */
    protected $aMails;

    /**
     * Set all the emails
     */
    public function getMails()
    {
        $this->aMails = [
            $this->getAdminsNotificationsMails(),
            $this->getDoctorNotificationsMails(),
            $this->getValidationDocumentsMails(),
            $this->getMedicalCasesMails(),
        ];

        return $this;
    }
}
