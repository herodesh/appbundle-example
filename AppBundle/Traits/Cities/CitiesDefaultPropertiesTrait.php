<?php

namespace Flendoc\AppBundle\Traits\Cities;

/**
 * Class CitiesDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Cities
 */
trait CitiesDefaultPropertiesTrait
{
    /**
     * @var array Cities
     */
    protected $aCities = [
        'Switzerland' => [
            'code'      => 'ch',
            'is_active' => true,
            'name'      => [
                'en' => 'swiss_test_city',
                'de' => 'swiss_test_city',
                'ro' => 'swiss_test_city',
                'fr' => 'swiss_test_city',
                'es' => 'swiss_test_city',
            ],
        ],
        'France'      => [
            'code'      => 'fr',
            'is_active' => true,
            'name'      => [
                'en' => 'french_test_city',
                'de' => 'french_test_city',
                'ro' => 'french_test_city',
                'fr' => 'french_test_city',
                'es' => 'french_test_city',
            ],
        ],
        'Spain'       => [
            'code'             => 'es',
            'is_active'        => true,
            'name'             => [
                'en' => 'spanish_test_city',
                'de' => 'spanish_test_city',
                'ro' => 'spanish_test_city',
                'fr' => 'spanish_test_city',
                'es' => 'spanish_test_city',
            ],
        ],
        'Germany'     => [
            'code'             => 'de',
            'is_active'        => true,
            'name'             => [
                'en' => 'german_test_city',
                'de' => 'german_test_city',
                'ro' => 'german_test_city',
                'fr' => 'german_test_city',
                'es' => 'german_test_city',
            ],
        ],
        'Austria'     => [
            'code'             => 'at',
            'is_active'        => true,
            'name'             => [
                'en' => 'austrian_test_city',
                'de' => 'austrian_test_city',
                'ro' => 'austrian_test_city',
                'fr' => 'austrian_test_city',
                'es' => 'austrian_test_city',
            ],
        ],
        'Romania'     => [
            'code'             => 'ro',
            'is_active'        => true,
            'name'             => [
                'en' => 'romanian_test_city',
                'de' => 'romanian_test_city',
                'ro' => 'romanian_test_city',
                'fr' => 'romanian_test_city',
                'es' => 'romanian_test_city',
            ],
        ],
        'Ireland'     => [
            'code'             => 'ie',
            'is_active'        => true,
            'name'             => [
                'en' => 'irish_test_city',
                'de' => 'irish_test_city',
                'ro' => 'irish_test_city',
                'fr' => 'irish_test_city',
                'es' => 'irish_test_city',
            ],
        ],
    ];
}
