<?php

namespace Flendoc\AppBundle\Traits\Resume;

use Flendoc\AppBundle\Entity\Resumes\ResumeSections;

/**
 * Trait ResumeSectionTrait
 * @package Flendoc\AppBundle\Traits\Resume
 */
trait ResumeSectionTrait
{
    /** @var array */
    protected $aResumeSections = [
        'Header'                => [
            'section_name' => ResumeSections::RESUME_SECTION_HEADER,
            'content'      => [
                'en' => 'Header',
                'ro' => 'Header',
                'de' => 'Kopfzeile',
            ],
            'properties'   => [
                'order'       => 1,
                'isSecondary' => false,
            ],
        ],
        'ContactDetails'        => [
            'section_name' => ResumeSections::RESUME_SECTION_CONTACT_DETAILS,
            'content'      => [
                'en' => 'Contact details',
                'ro' => 'Detalii de contact',
                'de' => 'Kontaktdaten',
            ],
            'properties'   => [
                'order'       => 2,
                'isSecondary' => false,
            ],
        ],
        'ProfessionalObjective' => [
            'section_name' => ResumeSections::RESUME_SECTION_PROFESSIONAL_OBJECTIVE,
            'content'      => [
                'en' => 'Professional objective',
                'ro' => 'Obiective profesionale',
                'de' => 'Professionelle Ziele',
            ],
            'properties'   => [
                'order'       => 3,
                'isSecondary' => false,
            ],
        ],
        'WorkExperience'        => [
            'section_name' => ResumeSections::RESUME_SECTION_WORK_EXPERIENCE,
            'content'      => [
                'en' => 'Work experience',
                'ro' => 'Experienţă de lucru',
                'de' => 'Arbeitserfahrung',
            ],
            'properties'   => [
                'order'       => 4,
                'isSecondary' => false,
            ],
        ],
        'Education'             => [
            'section_name' => ResumeSections::RESUME_SECTION_EDUCATION,
            'content'      => [
                'en' => 'Education',
                'ro' => 'Educaţie',
                'de' => 'Ausbildung',
            ],
            'properties'   => [
                'order'       => 5,
                'isSecondary' => false,
            ],
        ],
        'Skills'                => [
            'section_name' => ResumeSections::RESUME_SECTION_SKILLS,
            'content'      => [
                'en' => 'Skills',
                'ro' => 'Aptitudini',
                'de' => 'Fertigkeiten',
            ],
            'properties'   => [
                'order'       => 6,
                'isSecondary' => false,
            ],
        ],
        'ForeignLanguages'      => [
            'section_name' => ResumeSections::RESUME_SECTION_FOREIGN_LANGUAGES,
            'content'      => [
                'en' => 'Foreign languages',
                'ro' => 'Limbi străine',
                'de' => 'Fremdsprachen',
            ],
            'properties'   => [
                'order'       => 7,
                'isSecondary' => false,
            ],
        ],
        'OtherInfo'             => [
            'section_name' => ResumeSections::RESUME_SECTION_OTHER_INFORMATION,
            'content'      => [
                'en' => 'Other information',
                'ro' => 'Alte informaţii',
                'de' => 'Andere Informationen',
            ],
            'properties'   => [
                'order'       => 8,
                'isSecondary' => false,
            ],
        ],
        'DesiredJob'            => [
            'section_name' => ResumeSections::RESUME_SECTION_DESIRED_JOB,
            'content'      => [
                'en' => 'Desired job',
                'ro' => 'Loc de muncă dorit',
                'de' => 'Gewünschte Arbeit',
            ],
            'properties'   => [
                'order'       => 9,
                'isSecondary' => true,
            ],
        ],
        'Preferences'           => [
            'section_name' => ResumeSections::RESUME_SECTION_PREFERENCES,
            'content'      => [
                'en' => 'Preferences',
                'ro' => 'Preferniţe',
                'de' => 'Präferenzen',
            ],
            'properties'   => [
                'order'       => 10,
                'isSecondary' => true,
            ],
        ],
    ];
}
