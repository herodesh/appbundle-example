<?php

namespace Flendoc\AppBundle\Traits\Resume;

/**
 * Trait ResumeEducationDiplomaTypesTrait
 * @package Flendoc\AppBundle\Traits
 */
trait ResumeEducationDiplomaTypesTrait
{
    /** @var array */
    protected $aResumeEducationDiplomaTypes = [
        'elementarySchool'       => [
            'en' => 'Elementary School',
            'ro' => 'Şcoala generală',
        ],
        'professionalSchool'     => [
            'en' => 'Professional School',
            'ro' => 'Şcoala profesională',
        ],
        'highSchool'             => [
            'en' => 'High School',
            'ro' => 'Liceu',
        ],
        'undergraduate'          => [
            'en' => 'Undergraduate',
            'ro' => 'Şcoala postliceală',
        ],
        'collegeBachelorsDegree' => [
            'en' => 'College / Bachellor\'s degree',
            'ro' => 'Facultate / Colegiu',
        ],
        'mastersDegree'          => [
            'en' => 'Masters degree',
            'ro' => 'Masterat',
        ],
        'mba'                    => [
            'en' => 'MBA (Master of Business Administration)',
            'ro' => 'MBA (Master of Business Administration)',
        ],
        'doctorate'              => [
            'en' => 'Doctorate',
            'ro' => 'Doctorat',
        ],
    ];
}
