<?php

namespace Flendoc\AppBundle\Traits\Users;

/**
 * Trait UsersDocumentsTypeTrait
 * @package Flendoc\AppBundle\Traits\Users
 */
trait UsersDocumentsRejectionTypeTrait
{
    /** @var array */
    protected $aUsersDocumentRejections = [
        'unknownDocument' => [
            'en' => [
                'description'     => 'Unknown document',
                'rejectionReason' => 'The document sent is of an unknown type',
            ],
            'ro' => [
                'description'     => 'Document necunoscut',
                'rejectionReason' => 'Documentul trimis este necunoscut',
            ],
        ],

        'notReadable'         => [
            'en' => [
                'description'     => 'Document is unreadable',
                'rejectionReason' => 'The document you have sent is not readable or partially readable.',
            ],
            'ro' => [
                'description'     => 'Documentul nu este lizibil',
                'rejectionReason' => 'Documentul trimis nu este lizibil sau este parţial lizibil',
            ],
        ],

        'unacceptedDocuments' => [
            'en' => [
                'description'     => 'Unaccepted documents',
                'rejectionReason' => 'The document you have sent is not accepted. Please read our documents requirement before uploading.',
            ],
            'ro' => [
                'description'     => 'Documente neacceptate',
                'rejectionReason' => 'Documentul trimis nu poate fi acceptat. Vǎ rugǎm sa citiţi cu atenţie ce documente sunt acceptate.',
            ],
        ],
    ];
}
