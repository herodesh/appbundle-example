<?php

namespace Flendoc\AppBundle\Traits\Users;

/**
 * Trait UsersDocumentsTypeTrait
 * @package Flendoc\AppBundle\Traits\Users
 */
trait UsersDocumentsTypeTrait
{
    /** @var array */
    protected $aUsersDocumentsType = [
        'identityCard'      => [
            'isIdCard'     => true,
            'documentName' => [
                'en' => 'Identity card',
                'ro' => 'Carte de identitate',
                'fr' => 'Carte d\'identite',
            ],
        ],
        'driverLicense'     => [
            'isIdCard'     => true,
            'documentName' => [
                'en' => 'Driver license',
                'ro' => 'Carnet de şofer',
            ],
        ],
        'universityDiploma' => [
            'isIdCard'     => false,
            'documentName' => [
                'en' => 'University diploma',
                'ro' => 'Diplomă de universitate',
            ],
        ],
        'unknown' => [
            'isIdCard'     => false,
            'documentName' => [
                'en' => 'Unknown',
                'ro' => 'Necunoscut',
            ],
        ],
    ];
}
