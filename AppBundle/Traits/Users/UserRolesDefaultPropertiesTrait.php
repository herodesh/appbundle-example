<?php

namespace Flendoc\AppBundle\Traits\Users;

/**
 * Trait UserRolesDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Users
 */
trait UserRolesDefaultPropertiesTrait
{
    /**
     * @var array UserRoles
     */
    protected $aUserRoles = [
        'ROLE_ADMIN'               => [
            'id'   => '1',
            'name' => 'ROLE_ADMIN',
        ],
        'ROLE_DOCTOR'              => [
            'id'   => '2',
            'name' => 'ROLE_DOCTOR',
        ],
        'ROLE_DOCTOR_NOT_VERIFIED' => [
            'id'   => '3',
            'name' => 'ROLE_DOCTOR_NOT_VERIFIED',
        ],
        'ROLE_MODERATOR'           => [
            'id'   => '4',
            'name' => 'ROLE_MODERATOR',
        ],
        'ROLE_GHOST'               => [
            'id'   => '5',
            'name' => 'ROLE_GHOST',
        ],
    ];
}
