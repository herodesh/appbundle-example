<?php

namespace Flendoc\AppBundle\Traits\Users;

/**
 * Trait DefaultTestUsersTrait
 * @package Flendoc\AppBundle\Traits\Users
 */
trait DefaultTestUsersTrait
{
    /** @var array */
    protected $aUsers = [
        //complete user details
        '0' => [
            'userDetails'  => [
                'gender'          => 'm',
                'academicTitle'   => '',
                'firstName'       => 'Ghost',
                'lastName'        => 'User',
                'username'        => 'ghostuser@flendoc.com',
                'plainPassword'   => 'password',
                'defaultLanguage' => 'en',
                'terms'           => true,
                'verified'        => true,
                'enabled'         => true,
                'isGhost'         => true,
            ],
            'userLocation' => [
            ],
        ],
        '1' => [
            'userDetails'  => [
                'gender'          => 'm',
                'academicTitle'   => 'dr',
                'firstName'       => 'John',
                'lastName'        => 'Doe',
                'username'        => 'johndoe@flendoc.com',
                'plainPassword'   => 'password',
                'defaultLanguage' => 'en',
                'terms'           => true,
                'verified'        => true,
                'enabled'         => true,
                'isGhost'         => false,
            ],
            'userLocation' => [
                'country' => 'ro',
            ],
        ],
        '2' => [
            'userDetails'  => [
                'gender'          => 'm',
                'academicTitle'   => '',
                'firstName'       => 'Flen',
                'lastName'        => 'Doc',
                'username'        => 'office@flendoc.com',
                'plainPassword'   => 'password',
                'defaultLanguage' => 'en',
                'terms'           => true,
                'verified'        => true,
                'enabled'         => true,
                'isGhost'         => false,
            ],
            'userLocation' => [
                'country' => 'de',
            ],
        ],
        '3' => [
            'userDetails'  => [
                'gender'          => 'm',
                'academicTitle'   => 'dr',
                'firstName'       => 'Mihai',
                'lastName'        => 'Puscasu',
                'username'        => 'herodesh@gmail.com',
                'plainPassword'   => 'password',
                'defaultLanguage' => 'de',
                'terms'           => true,
                'verified'        => true,
                'enabled'         => true,
                'isGhost'         => false,
            ],
            'userLocation' => [
                'country' => 'de',
            ],
        ],
    ];
}
