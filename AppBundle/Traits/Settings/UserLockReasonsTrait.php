<?php

namespace Flendoc\AppBundle\Traits\Settings;

use Flendoc\AppBundle\Constants\UserLockReasonsConstants;

/**
 * Trait UserLockReasonsTrait
 * @package Flendoc\AppBundle\Traits\Settings
 */
trait UserLockReasonsTrait
{
    /**
     * @return array
     */
    protected $userLockReason = [
        UserLockReasonsConstants::USER_LOCK_FAILED_LOGIN_ATTEMPTS => [
            'stringIdentifier' => UserLockReasonsConstants::USER_LOCK_FAILED_LOGIN_ATTEMPTS,
            'content' => [
                'en' => [
                    'title'       => 'Too many failed login attempts',
                    'description' => 'Your account was locked because of too many failed login attempts',
                ],
            ],
        ],
    ];
}
