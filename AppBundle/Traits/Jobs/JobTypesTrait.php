<?php

namespace Flendoc\AppBundle\Traits\Jobs;

/**
 * Trait JobTypesTrait
 * @package Flendoc\AppBundle\Traits\Jobs
 */
trait JobTypesTrait
{
    /** @var array */
    protected $aJobTypes = [
        'fullTime' => [
            'en' => 'Full time',
            'ro' => 'Program întreg',
        ],
        'partTime' => [
            'en' => 'Part time',
            'ro' => 'Jumătate de normă',
        ],
        'internship' => [
            'en' => 'Internship',
            'ro' => 'Stagiu de pregătire',
        ],
        'projectBased' => [
            'en' => 'Project based',
            'ro' => 'Proiecte',
        ],
        'volunteering' => [
            'en' => 'Volunteering',
            'ro' => 'Voluntariat',
        ],
    ];
}
