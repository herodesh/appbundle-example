<?php

namespace Flendoc\AppBundle\Traits\Jobs;

/**
 * Trait CareerLevelTrait
 * @package Flendoc\AppBundle\Traits\Jobs
 */
trait CareerLevelTrait
{
    /** @var array */
    protected $aCareerLevel = [
        'student'        => [
            'en' => 'Student',
            'ro' => 'Student',
        ],
        'residentYear1'  => [
            'en' => 'Resident year 1',
            'ro' => 'Medic rezident anul 1',
        ],
        'residentYear2'  => [
            'en' => 'Resident year 2',
            'ro' => 'Medic rezident anul 2',
        ],
        'residentYear3'  => [
            'en' => 'Resident year 3',
            'ro' => 'Medic rezident anul 3',
        ],
        'residentYear4'  => [
            'en' => 'Resident year 4',
            'ro' => 'Medic rezident anul 4',
        ],
        'residentYear5'  => [
            'en' => 'Resident year 5',
            'ro' => 'Medic rezident anul 5',
        ],
        'specialist'     => [
            'en' => 'Specialist',
            'ro' => 'Medic specialist',
        ],
        'MD'             => [
            'en' => 'MD',
            'ro' => 'Medic primar',
        ],
        'chiefPhysician' => [
            'en' => 'Chief Physician',
            'ro' => 'Doctor Şef',
        ],
    ];
}
