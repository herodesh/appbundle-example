<?php

namespace Flendoc\AppBundle\Traits\Contacts;

/**
 * Class CitiesDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Cities
 */
trait ContactRequestDefaultStatusesTrait
{
    static $statusSent = 'sent';
    static $statusPending = 'pending';
    static $statusAccepted = 'accepted';
    static $statusBlocked = 'blocked';
}
