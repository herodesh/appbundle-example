<?php

namespace Flendoc\AppBundle\Traits\MedicalCases;

/**
 * Trait MedicalCasesUserActionRequiredTrait
 * @package Flendoc\AppBundle\Traits\MedicalCases
 */
trait MedicalCasesUserActionRequiredTrait
{
    /** @var array */
    protected $aMedicalCasesUserActionRequired = [
        'patientDataDisplayed' => [
            'en' => [
                'description'    => 'Visible patient data',
                'userNotificationMessage' => 'Images or text contains patient identification data (ex. Name, address, skin marks, etc)',
            ],
            'ro' => [
                'description'    => 'Date despre pacient vizibile',
                'userNotificationMessage' => 'Imaginile sau textul propus au date de identificare ale pacientului (ex. Nume, adresă, semne distinctive pe piele, etc.)',
            ],
        ],

        'incompleteMedicalCase' => [
            'en' => [
                'description'    => 'Incomplete medical case',
                'userNotificationMessage' => 'The proposed medical case is incomplete',
            ],
            'ro' => [
                'description'    => 'Caz medical incomplet',
                'userNotificationMessage' => 'Cazul medical propus este incomplet',
            ],
        ],

    ];
}
