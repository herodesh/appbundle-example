<?php

namespace Flendoc\AppBundle\Traits\Languages;

/**
 * Class LanguagesDefaultTrait
 * @package Flendoc\AppBundle\Traits\Languages
 */
trait LanguagesDefaultTrait
{
    /** @var array */
    protected $aLanguages = [
        'English'    => [
            'isActive'       => true,
            'isPrimary'      => true,
            'iso'            => 'en',
            'nameIdentifier' => 'english',
            'languages'      => [
                'en' => 'English',
                'de' => 'Englisch',
                'fr' => 'Anglais',
                'es' => 'Inglés',
                'pl' => 'Angielski',
                'sv' => 'Engelska',
                'fi' => 'Englanti',
                'it' => 'Inglese',
                'nl' => 'Engels',
                'pt' => 'Inglês',
                'ro' => 'Engleză',
                'cs' => 'Angličtina',
                'el' => 'Αγγλικά',
                'sr' => 'енглески',
                'da' => 'Engelsk',
                'hu' => 'Angol',
                'sl' => 'Angleščina',
                'hr' => 'Engleski',
                'bg' => 'английски',
                'nb' => 'Engelsk',
                'sk' => 'Angličtina',
                'tr' => 'İngilizce',
            ],
        ],
        'German'     => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'de',
            'nameIdentifier' => 'german',
            'languages'      => [
                'en' => 'German',
                'de' => 'Deutsch',
                'fr' => 'Allemand',
                'es' => 'Alemán ',
                'pl' => 'Niemiecki',
                'sv' => 'Tyska',
                'fi' => 'Saksa',
                'it' => 'Tedesco',
                'nl' => 'Duits',
                'pt' => 'Alemão',
                'ro' => 'Germană',
                'cs' => 'Němčina',
                'el' => 'Γερμανικά',
                'sr' => 'немачки',
                'da' => 'Tysk',
                'hu' => 'Német',
                'sl' => 'Nemščina',
                'hr' => 'Njemački',
                'bg' => 'немски',
                'nb' => 'Tysk',
                'sk' => 'Nemčina',
                'tr' => 'Almanca',
            ],
        ],
        'French'     => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'fr',
            'nameIdentifier' => 'french',
            'languages'      => [
                'en' => 'French',
                'de' => 'Französisch ',
                'fr' => 'Français',
                'es' => 'Francés',
                'pl' => 'Francuski',
                'sv' => 'Franska',
                'fi' => 'Ranska',
                'it' => 'Francese',
                'nl' => 'Frans',
                'pt' => 'Francês',
                'ro' => 'Franceză',
                'cs' => 'Francouzština',
                'el' => 'Γαλλικά',
                'sr' => 'француски',
                'da' => 'Fransk',
                'hu' => 'Francia',
                'sl' => 'Francoščina',
                'hr' => 'Francuski',
                'bg' => 'френски',
                'nb' => 'Fransk',
                'sk' => 'Francúzština',
                'tr' => 'Fransızca',
            ],
        ],
        'Spanish'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'es',
            'nameIdentifier' => 'spanish',
            'languages'      => [
                'en' => 'Spanish',
                'de' => 'Spanisch',
                'fr' => 'Espagnol',
                'es' => 'Español ',
                'pl' => 'Hiszpański',
                'sv' => 'Spanska',
                'fi' => 'Espanja',
                'it' => 'Spagnolo',
                'nl' => 'Spaans',
                'pt' => 'Espanhol',
                'ro' => 'Spaniolă',
                'cs' => 'Španělština',
                'el' => 'Ισπανικά',
                'sr' => 'шпански',
                'da' => 'Spansk',
                'hu' => 'Spanyol',
                'sl' => 'Španščina',
                'hr' => 'Španjolski',
                'bg' => 'испански',
                'nb' => 'Spansk',
                'sk' => 'Španielčina',
                'tr' => 'İspanyolca',
            ],
        ],
        'Polish'     => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'pl',
            'nameIdentifier' => 'polish',
            'languages'      => [
                'en' => 'Polish',
                'de' => 'Polnisch',
                'fr' => 'Polonais',
                'es' => 'Polaco',
                'pl' => 'Polski',
                'sv' => 'Polska',
                'fi' => 'Puola',
                'it' => 'Polacco',
                'nl' => 'Pools',
                'pt' => 'Polonês',
                'ro' => 'Poloneză',
                'cs' => 'Polština',
                'el' => 'Πολωνικά',
                'sr' => 'пољски',
                'da' => 'Polsk',
                'hu' => 'Lengyel',
                'sl' => 'Poljščina',
                'hr' => 'Poljski',
                'bg' => 'полски',
                'nb' => 'Polsk',
                'sk' => 'Poľština',
                'tr' => 'Lehçe',
            ],
        ],
        'Swedish'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'sv',
            'nameIdentifier' => 'swedish',
            'languages'      => [
                'en' => 'Swedish',
                'de' => 'Schwedisch',
                'fr' => 'Suédois',
                'es' => 'Sueco',
                'pl' => 'Szwedzki',
                'sv' => 'Svenska',
                'fi' => 'Ruotsi',
                'it' => 'Svedese',
                'nl' => 'Zweeds',
                'pt' => 'Sueco',
                'ro' => 'Suedeză',
                'cs' => 'Švédština',
                'el' => 'Σουηδικά',
                'sr' => 'шведски',
                'da' => 'Svensk',
                'hu' => 'Svéd',
                'sl' => 'Švedščina',
                'hr' => 'Švedski',
                'bg' => 'шведски',
                'nb' => 'Svensk',
                'sk' => 'Švédčina',
                'tr' => 'İsveççe',
            ],
        ],
        'Finnish'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'fi',
            'nameIdentifier' => 'finnish',
            'languages'      => [
                'en' => 'Finnish',
                'de' => 'Finnisch',
                'fr' => 'Finnois',
                'es' => 'Finés',
                'pl' => 'Fiński',
                'sv' => 'Finska',
                'fi' => 'Suomi',
                'it' => 'Finlandese',
                'nl' => 'Fins',
                'pt' => 'Finlandês',
                'ro' => 'Finlandeză',
                'cs' => 'Finština',
                'el' => 'Φινλανδικά',
                'sr' => 'фински',
                'da' => 'Finsk',
                'hu' => 'Finn',
                'sl' => 'Finščina',
                'hr' => 'Finski',
                'bg' => 'фински',
                'nb' => 'Finsk',
                'sk' => 'Fínčina',
                'tr' => 'Fince',
            ],
        ],
        'Italian'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'it',
            'nameIdentifier' => 'italian',
            'languages'      => [
                'en' => 'Italian',
                'de' => 'Italienisch',
                'fr' => 'Italien',
                'es' => 'Italiano',
                'pl' => 'Włoski',
                'sv' => 'Italienska',
                'fi' => 'Italia',
                'it' => 'Italiano',
                'nl' => 'Italiaans',
                'pt' => 'Italiano',
                'ro' => 'Italiană',
                'cs' => 'Italština',
                'el' => 'Ιταλικά',
                'sr' => 'италијански',
                'da' => 'Italiensk',
                'hu' => 'Olasz',
                'sl' => 'Italijanščina',
                'hr' => 'Talijanski',
                'bg' => 'италиански',
                'nb' => 'Italiensk',
                'sk' => 'Taliančina',
                'tr' => 'İtalyanca',
            ],
        ],
        'Dutch'      => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'nl',
            'nameIdentifier' => 'dutch',
            'languages'      => [
                'en' => 'Dutch',
                'de' => 'Niederländisch',
                'fr' => 'Néerlandais',
                'es' => 'Neerlandés',
                'pl' => 'Niderlandzki',
                'sv' => 'Nederländska',
                'fi' => 'Hollanti',
                'it' => 'Olandese',
                'nl' => 'Nederlands',
                'pt' => 'Holandês',
                'ro' => 'Olandeză',
                'cs' => 'Nizozemština',
                'el' => 'Ολλανδικά',
                'sr' => 'холандски',
                'da' => 'Hollandsk',
                'hu' => 'Holland',
                'sl' => 'Nizozemščina',
                'hr' => 'Nizozemski',
                'bg' => 'нидерландски',
                'nb' => 'Nederlandsk',
                'sk' => 'Holandčina',
                'tr' => 'Hollandaca',
            ],
        ],
        'Portuguese' => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'pt',
            'nameIdentifier' => 'portuguese',
            'languages'      => [
                'en' => 'Portuguese',
                'de' => 'Portugiesisch',
                'fr' => 'Portugais',
                'es' => 'Portugués',
                'pl' => 'Portugalski',
                'sv' => 'Portugisiska',
                'fi' => 'Portugali',
                'it' => 'Portoghese',
                'nl' => 'Portugees',
                'pt' => 'Português',
                'ro' => 'Portugheză',
                'cs' => 'Portugalština',
                'el' => 'Πορτογαλικά',
                'sr' => 'португалски',
                'da' => 'Portugisisk',
                'hu' => 'Portugál',
                'sl' => 'Portugalščina',
                'hr' => 'Portugalski',
                'bg' => 'португалски',
                'nb' => 'Portugisisk',
                'sk' => 'Portugalčina',
                'tr' => 'Portekizce',
            ],
        ],
        'Romanian'   => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'ro',
            'nameIdentifier' => 'romanian',
            'languages'      => [
                'en' => 'Romanian',
                'de' => 'Rumänisch',
                'fr' => 'Roumain',
                'es' => 'Rumano',
                'pl' => 'Rumuński',
                'sv' => 'Rumänska',
                'fi' => 'Romania',
                'it' => 'Rumeno',
                'nl' => 'Roemeens',
                'pt' => 'Romeno',
                'ro' => 'Română',
                'cs' => 'Rumunština',
                'el' => 'Ρουμανικά',
                'sr' => 'румунски',
                'da' => 'Rumænsk',
                'hu' => 'Román',
                'sl' => 'Romunščina',
                'hr' => 'Rumunjski',
                'bg' => 'румънски',
                'nb' => 'Rumensk',
                'sk' => 'Rumunčina',
                'tr' => 'Romence',
            ],
        ],
        'Czech'      => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'cs',
            'nameIdentifier' => 'czech',
            'languages'      => [
                'en' => 'Czech',
                'de' => 'Tschechisch',
                'fr' => 'Tchèque',
                'es' => 'Checo',
                'pl' => 'Czeski',
                'sv' => 'Tjeckiska',
                'fi' => 'Tšekki',
                'it' => 'Ceco',
                'nl' => 'Tsjechisch',
                'pt' => 'Tcheco',
                'ro' => 'Cehă',
                'cs' => 'Čeština',
                'el' => 'Τσεχικά',
                'sr' => 'чешки',
                'da' => 'Tjekkisk',
                'hu' => 'Cseh',
                'sl' => 'Češčina',
                'hr' => 'Češki',
                'bg' => 'чешки',
                'nb' => 'Tsjekkisk',
                'sk' => 'Čeština',
                'tr' => 'Çekçe',
            ],
        ],
        'Greek'      => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'el',
            'nameIdentifier' => 'greek',
            'languages'      => [
                'en' => 'Greek',
                'de' => 'Griechisch',
                'fr' => 'Grec',
                'es' => 'Griego',
                'pl' => 'Grecki',
                'sv' => 'Grekiska',
                'fi' => 'Kreikka',
                'it' => 'Greco',
                'nl' => 'Grieks',
                'pt' => 'Grego',
                'ro' => 'Greacă',
                'cs' => 'Řečtina',
                'el' => 'Ελληνικά',
                'sr' => 'грчки',
                'da' => 'Græsk',
                'hu' => 'Görög',
                'sl' => 'Grščina',
                'hr' => 'Grčki',
                'bg' => 'гръцки',
                'nb' => 'Gresk',
                'sk' => 'Gréčtina',
                'tr' => 'Yunanca',
            ],
        ],
        'Serbian'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'sr',
            'nameIdentifier' => 'serbian',
            'languages'      => [
                'en' => 'Serbian',
                'de' => 'Serbisch',
                'fr' => 'Serbe',
                'es' => 'Serbio',
                'pl' => 'Serbski',
                'se' => 'Serbiska',
                'fi' => 'Serbia',
                'it' => 'Serbo',
                'nl' => 'Servisch',
                'pt' => 'Sérvio',
                'ro' => 'Sârbă',
                'cs' => 'Srbština',
                'el' => 'Σερβικά',
                'sr' => 'српски',
                'da' => 'Serbisk',
                'hu' => 'Szerb',
                'sl' => 'Srbščina',
                'hr' => 'Srpski',
                'bg' => 'сръбски',
                'nb' => 'Serbisk',
                'sk' => 'Srbčina',
                'tr' => 'Sırpça',
            ],
        ],
        'Danish'     => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'da',
            'nameIdentifier' => 'danish',
            'languages'      => [
                'en' => 'Danish',
                'de' => 'Dänisch',
                'fr' => 'Danois',
                'es' => 'Danés',
                'pl' => 'Duński',
                'se' => 'Danska',
                'fi' => 'Tanska',
                'it' => 'Danese',
                'nl' => 'Deens',
                'pt' => 'Dinamarquês',
                'ro' => 'Daneză',
                'cs' => 'Dánština',
                'el' => 'Δανικά',
                'sr' => 'дански',
                'da' => 'Dansk',
                'hu' => 'Dán',
                'sl' => 'Danščina',
                'hr' => 'Danski',
                'bg' => 'датски',
                'nb' => 'Dansk',
                'sk' => 'Dánčina',
                'tr' => 'Danca',
            ],
        ],
        'Hungarian'  => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'hu',
            'nameIdentifier' => 'hungarian',
            'languages'      => [
                'en' => 'Hungarian',
                'de' => 'Ungarisch',
                'fr' => 'Hongrois',
                'es' => 'Húngaro',
                'pl' => 'Węgierski',
                'se' => 'Ungerska',
                'fi' => 'Unkari',
                'it' => 'Ungherese',
                'nl' => 'Hongaars',
                'pt' => 'Húngaro',
                'ro' => 'Maghiară',
                'cs' => 'Maďarština',
                'el' => 'Ουγγρικά',
                'sr' => 'мађарски',
                'da' => 'Ungarsk',
                'hu' => 'Magyar',
                'sl' => 'Madžarščina',
                'hr' => 'Mađarski',
                'bg' => 'унгарски',
                'nb' => 'Ungarsk',
                'sk' => 'Maďarčina',
                'tr' => 'Macarca',
            ],
        ],
        'Slovenian'  => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'sl',
            'nameIdentifier' => 'slovenian',
            'languages'      => [
                'en' => 'Slovenian',
                'de' => 'Slowenisch',
                'fr' => 'Slovène',
                'es' => 'Esloveno',
                'pl' => 'Słoweński',
                'se' => 'Slovenska',
                'fi' => 'Sloveeni',
                'it' => 'Sloveno',
                'nl' => 'Sloveens',
                'pt' => 'Esloveno',
                'ro' => 'Slovenă',
                'cs' => 'Slovinština',
                'el' => 'Σλοβενικά',
                'sr' => 'словеначки',
                'da' => 'Slovensk',
                'hu' => 'Szlovén',
                'sl' => 'Slovenščina',
                'hr' => 'Slovenski',
                'bg' => 'словенски',
                'nb' => 'Slovensk',
                'sk' => 'Slovinčina',
                'tr' => 'Slovence',
            ],
        ],
        'Croatian'   => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'hr',
            'nameIdentifier' => 'croatian',
            'languages'      => [
                'en' => 'Croatian',
                'de' => 'Kroatisch',
                'fr' => 'Croate',
                'es' => 'Croata',
                'pl' => 'Chorwacki',
                'se' => 'Kroatiska',
                'fi' => 'Kroatia',
                'it' => 'Croato',
                'nl' => 'Kroatisch',
                'pt' => 'Croata',
                'ro' => 'Croată',
                'cs' => 'Chorvatština',
                'el' => 'Κροατικά',
                'sr' => 'хрватски',
                'da' => 'Kroatisk',
                'hu' => 'Horvát',
                'sl' => 'Hrvaščina',
                'hr' => 'Hrvatski',
                'bg' => 'хърватски',
                'nb' => 'Kroatisk',
                'sk' => 'Chorvátčina',
                'tr' => 'Hırvatça',
            ],
        ],
        'Bulgarian'  => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'bg',
            'nameIdentifier' => 'bulgarian',
            'languages'      => [
                'en' => 'Bulgarian',
                'de' => 'Bulgarisch',
                'fr' => 'Bulgare',
                'es' => 'Búlgaro',
                'pl' => 'Bułgarski',
                'se' => 'Bulgariska',
                'fi' => 'Bulgaria',
                'it' => 'Bulgaro',
                'nl' => 'Bulgaars',
                'pt' => 'Búlgaro',
                'ro' => 'Bulgară',
                'cs' => 'Bulharština',
                'el' => 'Βουλγαρικά',
                'sr' => 'бугарски',
                'da' => 'Bulgarsk',
                'hu' => 'Bolgár',
                'sl' => 'Bolgarščina',
                'hr' => 'Bugarski',
                'bg' => 'български',
                'nb' => 'Bulgarsk',
                'sk' => 'Bulharčina',
                'tr' => 'Bulgarca',
            ],
        ],
        'Norwegian'  => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'nb',
            'nameIdentifier' => 'norwegian',
            'languages'      => [
                'en' => 'Norwegian',
                'de' => 'Norwegisch',
                'fr' => 'Norvégien',
                'es' => 'Noruego',
                'pl' => 'Norweski',
                'sv' => 'Norskt',
                'fi' => 'Norjan',
                'it' => 'Norvegese',
                'nl' => 'Noors',
                'pt' => 'Norueguês',
                'ro' => 'Norvegiană',
                'cs' => 'Norština',
                'el' => 'Νορβηγικά',
                'sr' => 'норвешки',
                'da' => 'Norsk',
                'hu' => 'Norvég',
                'sl' => 'Norveščina',
                'hr' => 'Norveški',
                'bg' => 'норвежки',
                'nb' => 'Norsk',
                'sk' => 'Nórčina',
                'tr' => 'Norveççe',
            ],
        ],
        'Slovak'     => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'sk',
            'nameIdentifier' => 'slovak',
            'languages'      => [
                'en' => 'Slovak',
                'de' => 'Slowakisch',
                'fr' => 'Slovaque',
                'es' => 'Eslovaco',
                'pl' => 'Słowacki',
                'sv' => 'Slovakiska',
                'fi' => 'Slovakki',
                'it' => 'Slovacco',
                'nl' => 'Slowaaks',
                'pt' => 'Eslovaco',
                'ro' => 'Slovacă',
                'cs' => 'Slovenština',
                'el' => 'Σλοβακικά',
                'sr' => 'словачки',
                'da' => 'Slovakisk',
                'hu' => 'Szlovák',
                'sl' => 'Slovaščina',
                'hr' => 'Slovački',
                'bg' => 'словашки',
                'nb' => 'Slovakisk',
                'sk' => 'Slovenčina',
                'tr' => 'Slovakça',
            ],
        ],
        'Turkish'    => [
            'isActive'       => false,
            'isPrimary'      => false,
            'iso'            => 'tr',
            'nameIdentifier' => 'turkish',
            'languages'      => [
                'en' => 'Turkish',
                'de' => 'Türkisch',
                'fr' => 'Turc',
                'es' => 'Turco',
                'pl' => 'Turecki',
                'sv' => 'Turkiska',
                'fi' => 'Turkki',
                'it' => 'Turco',
                'nl' => 'Turks',
                'pt' => 'Turco',
                'ro' => 'Turcă',
                'cs' => 'Turečtina',
                'el' => 'Τουρκικά',
                'sr' => 'турски',
                'da' => 'Tyrkisk',
                'hu' => 'Török',
                'sl' => 'Turščina',
                'hr' => 'Turski',
                'bg' => 'турски',
                'nb' => 'Tyrkisk',
                'sk' => 'Turečtina',
                'tr' => 'Türkçe',
            ],
        ],
    ];
}
