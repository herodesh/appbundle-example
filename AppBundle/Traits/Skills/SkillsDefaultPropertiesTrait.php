<?php

namespace Flendoc\AppBundle\Traits\Skills;

/**
 * Trait SkillsDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Skills
 */
trait SkillsDefaultPropertiesTrait
{
    /**
     * @var array skills
     */
    protected $aSkills = [
        'Allergology'                       => [
            'en' => 'Allergology',
            'ro' => 'Alergologie',
            'de' => 'Allergologie',
        ],
        'Oral Medicine (Stomatology)'       => [
            'en' => 'Oral Medicine (Stomatology)',
            'ro' => 'Stomatologie',
            'de' => 'Stomatologie',
        ],
        'Oncology'                          => [
            'en' => 'Oncology',
            'ro' => 'Oncologie',
            'de' => 'Onkologie',
        ],
        'Cardiology'                        => [
            'en' => 'Cardiology',
            'ro' => 'Cardiologie',
            'de' => 'Kardiologie',
        ],
        'Internal medicine'                 => [
            'en' => 'Internal medicine',
            'ro' => 'Medicină internă',
            'de' => 'Innere Medizin',
        ],
        'Pharmacology'                      => [
            'en' => 'Pharmacology',
            'ro' => 'Farmacologie',
            'de' => 'Pharmakologie',
        ],
        'ENT (Otolaryngology)'              => [
            'en' => 'ENT (Otolaryngology)',
            'ro' => 'ORL (Otorinolaringologie)',
            'de' => 'HNO (Hals-Nasen-Ohrenheilkunde)',
        ],
        'Chiropractor (Manual Medicine)'    => [
            'en' => 'Chiropractor (Manual Medicine)',
            'ro' => 'Chiropractor (Medicină manuală)',
            'de' => 'Chiropraktiker (Manuelle Medizin)',
        ],
        'Ophthalmology'                     => [
            'en' => 'Ophthalmology',
            'ro' => 'Oftalmologie',
            'de' => 'Augenheilkunde (Ophthalmologie)',
        ],
        'Dermatology'                       => [
            'en' => 'Dermatology',
            'ro' => 'Dermatologie',
            'de' => 'Dermatologie',
        ],
        'General Medicine'                  => [
            'en' => 'General Medicine',
            'ro' => 'Medicină generală',
            'de' => 'Allgemeinmedizin',
        ],
        'Neurology'                         => [
            'en' => 'Neurology',
            'ro' => 'Neurologie',
            'de' => 'Neurologie',
        ],
        'Neurosurgery'                      => [
            'en' => 'Neurosurgery',
            'ro' => 'Neurochirugie',
            'de' => 'Neurochirugie',
        ],
        'Orthopedic surgery'                => [
            'en' => 'Orthopedic surgery',
            'ro' => 'Ortopedie',
            'de' => 'Orthopädie',
        ],
        'Gastroenterology'                  => [
            'en' => 'Gastroenterology',
            'ro' => 'Gastroenterologie',
            'de' => 'Gastroenterologie',
        ],
        'Infectious Disease (Infectiology)' => [
            'en' => 'Infectious Disease (Infectiology)',
            'ro' => 'Boli infecţioase',
            'de' => 'Infektiologie',
        ],
        'Venereology'                       => [
            'en' => 'Venereology',
            'ro' => 'Venerologie',
            'de' => 'Venerologie',
        ],
        'Urology'                           => [
            'en' => 'Urology',
            'ro' => 'Urologie',
            'de' => 'Urologie',
        ],
        'Gynecology'                        => [
            'en' => 'Gynecology',
            'ro' => 'Ginecologie',
            'de' => 'Gynäkologie',
        ],
        'Geriatrics'                        => [
            'en' => 'Geriatrics',
            'ro' => 'Geriatrie',
            'de' => 'Geriatrie',
        ],
        'Endocrinology'                     => [
            'en' => 'Endocrinology',
            'ro' => 'Endocrinologie',
            'de' => 'Endokrinologie',
        ],
        'Nephrology'                        => [
            'en' => 'Nephrology',
            'ro' => 'Nefrologie',
            'de' => 'Nephrologie',
        ],
        'Physiology'                        => [
            'en' => 'Physiology',
            'ro' => 'Psihologie',
            'de' => 'Physiologie',
        ],
        'Pediatrics'                        => [
            'en' => 'Pediatrics',
            'ro' => 'Pediatrie',
            'de' => 'Pädiatrie (Kinderheilkunde)',
        ],
        'Pulmonology'                       => [
            'en' => 'Pulmonology',
            'ro' => 'Pneumologie',
            'de' => 'Pneumologie',
        ],
        'Podiatry'                          => [
            'en' => 'Podiatry',
            'ro' => 'Podiatrie',
            'de' => 'Podiatrie',
        ],
        'Colorectal surgery (Proctology)'   => [
            'en' => 'Colorectal surgery (Proctology)',
            'ro' => 'Proctologie',
            'de' => 'Proktologie',
        ],
        'Psychiatry and Psychotherapy'      => [
            'en' => 'Psychiatry and Psychotherapy',
            'ro' => 'Psihiatrie şi Psihoterapie',
            'de' => 'Psychiatrie und Psychotherapie',
        ],
        'Radiology'                         => [
            'en' => 'Radiology',
            'ro' => 'Radiologie',
            'de' => 'Radiologie',
        ],
        'Phlebology'                        => [
            'en' => 'Phlebology',
            'ro' => 'Flebologie',
            'de' => 'Phlebologie',
        ],
        'Hematology'                        => [
            'en' => 'Hematology',
            'ro' => 'Hematologie',
            'de' => 'Hämatologie',
        ],
        'Rheumatology'                      => [
            'en' => 'Rheumatology',
            'ro' => 'Reumatologie',
            'de' => 'Rheumatologie',
        ],
        'Physical therapy'                  => [
            'en' => 'Physical therapy',
            'ro' => 'Fizioterapie',
            'de' => 'Physiotherapie',
        ],
        'Cardiac Surgery'                   => [
            'en' => 'Cardiac Surgery',
            'ro' => 'Chirurgie Cardiacă',
            'de' => 'Herzchirurgie',
        ],
        'General surgery'                   => [
            'en' => 'General surgery',
            'ro' => 'Chirurgia generală',
            'de' => 'Allgemeinchirugie',
        ],
        'Plastic and cosmetic surgery'      => [
            'en' => 'Plastic and cosmetic surgery',
            'ro' => 'Chirurgie plastică/estetică',
            'de' => 'Plastische und Ästhetische Chirurgie',
        ],
        'Vascular surgery'                  => [
            'en' => 'Vascular surgery',
            'ro' => 'Chirurgie vasculară',
            'de' => 'Gefäßchirurgie',
        ],
        'Diabetology'                       => [
            'en' => 'Diabetology',
            'ro' => 'Diabetologie',
            'de' => 'Diabetologie',
        ],
        'Orthodontics'                      => [
            'en' => 'Orthodontics',
            'ro' => 'Ortodonţie',
            'de' => 'Orthodontie',
        ],
        'Periodontology'                    => [
            'en' => 'Periodontology',
            'ro' => 'Parodontologie',
            'de' => 'Parodontologie',
        ],
        'Anesthesiology'                    => [
            'en' => 'Anesthesiology',
            'ro' => 'Anesteziologie.',
            'de' => 'Anästhesiologie',
        ],
        'Occupational medicine'             => [
            'en' => 'Occupational medicine',
            'ro' => 'Medicina Muncii',
            'de' => 'Arbeitsmedizin',
        ],
        'Andrology'                         => [
            'en' => 'Andrology',
            'ro' => 'Andrologie',
            'de' => 'Andrologie',
        ],
        'Bacteriology'                      => [
            'en' => 'Bacteriology',
            'ro' => 'Bacteriologie',
            'de' => 'Bakteriologie',
        ],
        'Biology'                           => [
            'en' => 'Biology',
            'ro' => 'Biologie',
            'de' => 'Biologie',
        ],
        'Angiology'                         => [
            'en' => 'Angiology',
            'ro' => 'Angiologie',
            'de' => 'Angiologie',
        ],
        'Naturopathy'                       => [
            'en' => 'Naturopathy',
            'ro' => 'Medicină naturistă',
            'de' => 'Naturheilkunde',
        ],
        'Alternative medicine'              => [
            'en' => 'Alternative medicine',
            'ro' => 'Medicină alternativă',
            'de' => 'Alternativmedizin',
        ],
        'Neuroradiology'                    => [
            'en' => 'Neuroradiology',
            'ro' => 'Neuroradiologie',
            'de' => 'Neuroradiologie',
        ],
        'Nuclear medicine'                  => [
            'en' => 'Nuclear medicine',
            'ro' => 'Medicină Nucleară',
            'de' => 'Nuklearmedizin',
        ],
        'Pathology'                         => [
            'en' => 'Pathology',
            'ro' => 'Patologie',
            'de' => 'Pathologie',
        ],
        'Implantology'                      => [
            'en' => 'Implantology',
            'ro' => 'Implantologie',
            'de' => 'Implantologie',
        ],
        'Sports medicine'                   => [
            'en' => 'Sports medicine',
            'ro' => 'Medicina sportivă',
            'de' => 'Sportmedizin',
        ],
        'Speech-language Pathology'         => [
            'en' => 'Speech-language Pathology',
            'ro' => 'Logopedie',
            'de' => 'Logopädie',
        ],
        'Veterinary medicine'               => [
            'en' => 'Veterinary medicine',
            'ro' => 'Medicină Veterinară',
            'de' => 'Veterinärmedizin',
        ],
        'Anatomy'                           => [
            'en' => 'Anatomy',
            'ro' => 'Anatomie',
            'de' => 'Anatomie',
        ],
        'Biochemistry'                      => [
            'en' => 'Biochemistry',
            'ro' => 'Biochimie',
            'de' => 'Biochemie',
        ],
        'Human genetics'                    => [
            'en' => 'Human genetics',
            'ro' => 'Genetica umană',
            'de' => 'Humangenetik',
        ],
        'Virology'                          => [
            'en' => 'Virology',
            'ro' => 'Virologie (Virusologie)',
            'de' => 'Virologie',
        ],
        'Microbiology'                      => [
            'en' => 'Microbiology',
            'ro' => 'Microbiologie',
            'de' => 'Mikrobiologie',
        ],
        'Medical jurisprudence'             => [
            'en' => 'Medical jurisprudence',
            'ro' => 'Jurisprudența medicală',
            'de' => 'Rechtsmedizin',
        ],
        'Neonatology'                       => [
            'en' => 'Neonatology',
            'ro' => 'Neonatologie',
            'de' => 'Neonatologie',
        ],
        'Emergency medicine'                => [
            'en' => 'Emergency medicine',
            'ro' => 'Medicina de Urgenţă',
            'de' => 'Notfall medizin',
        ],
        'Family medicine'                   => [
            'en' => 'Family medicine',
            'ro' => 'Medicina de familie',
            'de' => 'Familienmedizin',
        ],
        'Forensic pathology'                => [
            'en' => 'Forensic pathology',
            'ro' => 'Medicina legală',
            'de' => 'Gerichtsmedizin',
        ],
        'Hepatology'                        => [
            'en' => 'Hepatology',
            'ro' => 'Hepatologie',
            'de' => 'Hepatologie',
        ],
        'Public Health'                     => [
            'en' => 'Public Health',
            'ro' => 'Sănătate Publică',
            'de' => 'Gesundheitssystem',
        ],
        'Thoracic surgery'                  => [
            'en' => 'Thoracic surgery',
            'ro' => 'Chirurgie toracică ',
            'de' => 'Thoraxchirurgie',
        ],

    ];
}
