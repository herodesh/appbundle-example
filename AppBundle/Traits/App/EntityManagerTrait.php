<?php

namespace Flendoc\AppBundle\Traits\App;

use Flendoc\AppBundle\Entity\Languages\Languages;

/**
 * Trait EntityManagerTrait
 * @package Flendoc\AppBundle\Traits\App
 */
trait EntityManagerTrait
{
    /**
     * Get EntityManager
     *
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Get ODM Manager
     *
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getMongoManager()
    {
        return $this->get('doctrine_mongodb')->getManager();
    }

    /**
     * @param $repository
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository($repository)
    {
        return $this->getEntityManager()->getRepository($repository);
    }

    /**
     * @param $repository
     * @param $identifier
     *
     * @return mixed
     */
    protected function getReference($repository, $identifier)
    {
        return $this->getEntityManager()->getReference($repository, $identifier);
    }

    /**
     * Sometimes in admin we do not need to display all the available languages.
     * Check findAllActiveLanguages() for active languages
     *
     * @return array|\Flendoc\AppBundle\Entity\Languages\Languages[]
     */
    protected function findAllLanguages()
    {
        return $this->getRepository(Languages::class)->findAll();
    }

    /**
     * Sometimes in admin we do not need to display all the available languages
     *
     * @return object[]
     */
    protected function findAllActiveLanguages()
    {
        return $this->getRepository(Languages::class)->findBy([
            'active' => true,
        ]);
    }

    /**
     * @param $repository
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getMongoRepository($repository)
    {
        return $this->getMongoManager()->getRepository($repository);
    }
}
