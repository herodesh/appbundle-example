<?php

namespace Flendoc\AppBundle\Traits\App;

use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Flendoc\AppBundle\Entity\Chats\DoctorChat;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCases;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;

/**
 * Trait ObjectEntityTrait
 * @package Flendoc\AppBundle\Traits\Feeds
 */
trait ObjectKeyEntityTrait
{

    /** @var array */
    static $aKeyToEntityTypes = [
        ObjectTypeConstants::OBJECT_MEDICAL_CASE => MedicalCases::class,
        ObjectTypeConstants::OBJECT_DOCTOR_CHAT  => DoctorChat::class,
        ObjectTypeConstants::OBJECT_MONGO_FEED   => MongoFeed::class,
    ];

    /** @var array */
    static $aEntityToKey = [
        MedicalCases::class => ObjectTypeConstants::OBJECT_MEDICAL_CASE,
        DoctorChat::class   => ObjectTypeConstants::OBJECT_DOCTOR_CHAT,
        MongoFeed::class    => ObjectTypeConstants::OBJECT_MONGO_FEED,
    ];
}
