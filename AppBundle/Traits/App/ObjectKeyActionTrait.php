<?php

namespace Flendoc\AppBundle\Traits\App;

use Flendoc\AppBundle\Constants\ObjectTypeConstants;

/**
 * Trait ObjectEntityTrait
 * @package Flendoc\AppBundle\Traits\Feeds
 */
trait ObjectKeyActionTrait
{
    /** @var array */
    static $aKeyToActions = [
        ObjectTypeConstants::OBJECT_MEDICAL_CASE                   => 'doctors_view_medical_case_notification',
        ObjectTypeConstants::OBJECT_MONGO_FEED                     => 'doctors_feeds_view_feed_notification',
        ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST          => 'doctors_my_contacts_overview',
        ObjectTypeConstants::RELATIONSHIP_CONTACT_REQUEST_ACCEPTED => 'doctors_my_contacts_overview',
    ];
}
