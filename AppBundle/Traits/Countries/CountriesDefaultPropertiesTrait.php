<?php

namespace Flendoc\AppBundle\Traits\Countries;

/**
 * Class CountriesDefaultPropertiesTrait
 * @package Flendoc\AppBundle\Traits\Countries
 */
trait CountriesDefaultPropertiesTrait
{
    /**
     * @var array Countries
     */
    protected $aCountries = [
        'Afghanistan'                                                => [
            'code'      => 'af',
            'is_active' => true,
            'name'      => [
                'en' => 'Afghanistan',
                'ro' => 'Afganistan',
                'de' => 'Afghanistan',
            ],
        ],
        'Albania'                                                    => [
            'code'      => 'al',
            'is_active' => true,
            'name'      => [
                'en' => 'Albania',
                'ro' => 'Albania',
                'de' => 'Albanien',
            ],
        ],
        'Algeria'                                                    => [
            'code'      => 'dz',
            'is_active' => true,
            'name'      => [
                'en' => 'Algeria',
                'ro' => 'Algeria',
                'de' => 'Algerien',
            ],
        ],
        'American Samoa'                                             => [
            'code'      => 'as',
            'is_active' => true,
            'name'      => [
                'en' => 'American Samoa',
                'ro' => 'Samoa Americană',
                'de' => 'Amerikanische Samoa',
            ],
        ],
        'Andorra, Principality of'                                   => [
            'code'      => 'ad',
            'is_active' => true,
            'name'      => [
                'en' => 'Andorra, Principality of',
                'ro' => 'Andorra, Principatul',
                'de' => 'Andorra, Fürstentum',
            ],
        ],
        'Angola'                                                     => [
            'code'      => 'ao',
            'is_active' => true,
            'name'      => [
                'en' => 'Angola',
                'ro' => 'Angola',
                'de' => 'Angola',
            ],
        ],
        'Anguilla'                                                   => [
            'code'      => 'ai',
            'is_active' => true,
            'name'      => [
                'en' => 'Anguilla',
                'ro' => 'Anguilla',
                'de' => 'Anguilla',
            ],
        ],
        'Antarctica'                                                 => [
            'code'      => 'aq',
            'is_active' => true,
            'name'      => [
                'en' => 'Antarctica',
                'ro' => 'Antarctica',
                'de' => 'Antarktis',
            ],
        ],
        'Antigua and Barbuda'                                        => [
            'code'      => 'ag',
            'is_active' => true,
            'name'      => [
                'en' => 'Antigua and Barbuda',
                'ro' => 'Antigua şi Barbuda',
                'de' => 'Antigua und Barbuda',
            ],
        ],
        'Argentina'                                                  => [
            'code'      => 'ar',
            'is_active' => true,
            'name'      => [
                'en' => 'Argentina',
                'ro' => 'Argentina',
                'de' => 'Argentinien',
            ],
        ],
        'Armenia'                                                    => [
            'code'      => 'am',
            'is_active' => true,
            'name'      => [
                'en' => 'Armenia',
                'ro' => 'Armenia',
                'de' => 'Armenien',
            ],
        ],
        'Aruba'                                                      => [
            'code'      => 'aw',
            'is_active' => true,
            'name'      => [
                'en' => 'Aruba',
                'ro' => 'Aruba',
                'de' => 'Aruna',
            ],
        ],
        'Australia'                                                  => [
            'code'      => 'au',
            'is_active' => true,
            'name'      => [
                'en' => 'Australia',
                'ro' => 'Australia',
                'de' => 'Australien',
            ],
        ],
        'Austria'                                                    => [
            'code'      => 'at',
            'is_active' => true,
            'name'      => [
                'en' => 'Austria',
                'ro' => 'Austria',
                'de' => 'Österreich',
            ],
        ],
        'Azerbaidjan'                                  => [
            'code'      => 'az',
            'is_active' => true,
            'name'      => [
                'en' => 'Azerbaidjan',
                'ro' => 'Azerbaidjan',
                'de' => 'Aserbaidschan',
            ],
        ],
        'Bahamas, The'                                               => [
            'code'      => 'bs',
            'is_active' => true,
            'name'      => [
                'en' => 'Bahamas, The',
                'ro' => 'Bahamas',
                'de' => 'Bahamas, Die',
            ],
        ],
        'Bahrain, Kingdom of'                                        => [
            'code'      => 'bh',
            'is_active' => true,
            'name'      => [
                'en' => 'Bahrain, Kingdom of',
                'ro' => 'Bahrain, Regatul',
                'de' => 'Bahrain, Königreich',
            ],
        ],
        'Bangladesh'                                                 => [
            'code'      => 'bd',
            'is_active' => true,
            'name'      => [
                'en' => 'Bangladesh',
                'ro' => 'Bangladesh',
                'de' => 'Bangladesch',
            ],
        ],
        'Barbados'                                                   => [
            'code'      => 'bb',
            'is_active' => true,
            'name'      => [
                'en' => 'Barbados',
                'ro' => 'Barbados',
                'de' => 'Barbados',
            ],
        ],
        'Belarus'                                                    => [
            'code'      => 'by',
            'is_active' => true,
            'name'      => [
                'en' => 'Belarus',
                'ro' => 'Belarus',
                'de' => 'Weißrussland',
            ],
        ],
        'Belgium'                                                    => [
            'code'      => 'be',
            'is_active' => true,
            'name'      => [
                'en' => 'Belgium',
                'ro' => 'Belgia',
                'de' => 'Belgien',
            ],
        ],
        'Belize'                                                     => [
            'code'      => 'bz',
            'is_active' => true,
            'name'      => [
                'en' => 'Belize',
                'ro' => 'Belize',
                'de' => 'Belize',
            ],
        ],
        'Benin, Republic of'                                          => [
            'code'      => 'bj',
            'is_active' => true,
            'name'      => [
                'en' => 'Benin, Republic of',
                'ro' => 'Benin, Republica',
                'de' => 'Benin, Republik',
            ],
        ],
        'Bermuda'                                                    => [
            'code'      => 'bm',
            'is_active' => true,
            'name'      => [
                'en' => 'Bermuda',
                'ro' => 'Bermude, Insulele',
                'de' => 'Bermuda',
            ],
        ],
        'Bhutan, Kingdom of'                                         => [
            'code'      => 'bt',
            'is_active' => true,
            'name'      => [
                'en' => 'Bhutan, Kingdom of',
                'ro' => 'Bhutan, Regatul',
                'de' => 'Bhutan, Königreich',
            ],
        ],
        'Bolivia'                                                    => [
            'code'      => 'bo',
            'is_active' => true,
            'name'      => [
                'en' => 'Bolivia',
                'ro' => 'Bolivia',
                'de' => 'Bolivien',
            ],
        ],
        'Bosnia and Herzegovina'                                     => [
            'code'      => 'ba',
            'is_active' => true,
            'name'      => [
                'en' => 'Bosnia and Herzegovina',
                'ro' => 'Bosnia şi Herţegovina',
                'de' => 'Bosnien und Herzegowina',
            ],
        ],
        'Botswana'                                                   => [
            'code'      => 'bw',
            'is_active' => true,
            'name'      => [
                'en' => 'Botswana',
                'ro' => 'Botswana',
                'de' => 'Botsuana',
            ],
        ],
        'Bouvet Island (Territory of Norway)'                        => [
            'code'      => 'bv',
            'is_active' => true,
            'name'      => [
                'en' => 'Bouvet Island (Territory of Norway)',
                'ro' => 'Bouvet, Insula',
                'de' => 'Bouvet, Insel',
            ],
        ],
        'Brazil'                                                     => [
            'code'      => 'br',
            'is_active' => true,
            'name'      => [
                'en' => 'Brazil',
                'ro' => 'Brazilia',
                'de' => 'Brasilien',
            ],
        ],
        'British Indian Ocean Territory (BIOT)'                      => [
            'code'      => 'io',
            'is_active' => true,
            'name'      => [
                'en' => 'British Indian Ocean Territory (BIOT)',
                'ro' => 'Teritoriul Britanic din Oceanul Indian',
                'de' => 'Britisches Territorium im Indischen Ozean',
            ],
        ],
        'Brunei (Negara Brunei Darussalam)'                          => [
            'code'      => 'bn',
            'is_active' => true,
            'name'      => [
                'en' => 'Brunei (Negara Brunei Darussalam)',
                'ro' => 'Brunei, Statul',
                'de' => 'Brunei, Das Sultanat',
            ],
        ],
        'Bulgaria'                                                   => [
            'code'      => 'bg',
            'is_active' => true,
            'name'      => [
                'en' => 'Bulgaria',
                'ro' => 'Bulgaria',
                'de' => 'Bulgarien',
            ],
        ],
        'Burkina Faso'                                               => [
            'code'      => 'bf',
            'is_active' => true,
            'name'      => [
                'en' => 'Burkina Faso',
                'ro' => 'Burkina Faso',
                'de' => 'Burkina Faso',
            ],
        ],
        'Burundi'                                                    => [
            'code'      => 'bi',
            'is_active' => true,
            'name'      => [
                'en' => 'Burundi',
                'ro' => 'Burundi',
                'de' => 'Burundi',
            ],
        ],
        'Cambodia, Kingdom of'                                       => [
            'code'      => 'kh',
            'is_active' => true,
            'name'      => [
                'en' => 'Cambodia, Kingdom of',
                'ro' => 'Cambodgia',
                'de' => 'Kambodscha',
            ],
        ],
        'Cameroon'                                                   => [
            'code'      => 'cm',
            'is_active' => true,
            'name'      => [
                'en' => 'Cameroon',
                'ro' => 'Camerun',
                'de' => 'Kamerun',
            ],
        ],
        'Canada'                                                     => [
            'code'      => 'ca',
            'is_active' => true,
            'name'      => [
                'en' => 'Canada',
                'ro' => 'Canada',
                'de' => 'Kanada',
            ],
        ],
        'Cape Verde'                                                 => [
            'code'      => 'cv',
            'is_active' => true,
            'name'      => [
                'en' => 'Cape Verde',
                'ro' => 'Capul Verde',
                'de' => 'Kap Verde',
            ],
        ],
        'Cayman Islands'                                             => [
            'code'      => 'ky',
            'is_active' => true,
            'name'      => [
                'en' => 'Cayman Islands',
                'ro' => 'Cayman, Insulele',
                'de' => 'Caymaninseln',
            ],
        ],
        'Central African Republic'                                   => [
            'code'      => 'cf',
            'is_active' => true,
            'name'      => [
                'en' => 'Central African Republic',
                'ro' => 'Centrafricană, Republica',
                'de' => 'Zentralafrikanische Republik',
            ],
        ],
        'Chad'                                                       => [
            'code'      => 'td',
            'is_active' => true,
            'name'      => [
                'en' => 'Chad',
                'ro' => 'Ciad',
                'de' => 'Tschad',
            ],
        ],
        'Chile'                                                      => [
            'code'      => 'cl',
            'is_active' => true,
            'name'      => [
                'en' => 'Chile',
                'ro' => 'Chile',
                'de' => 'Chile',
            ],
        ],
        'China'                                                      => [
            'code'      => 'cn',
            'is_active' => true,
            'name'      => [
                'en' => 'China',
                'ro' => 'China',
                'de' => 'China',
            ],
        ],
        'Christmas Island'                                           => [
            'code'      => 'cx',
            'is_active' => true,
            'name'      => [
                'en' => 'Christmas Island',
                'ro' => 'Insula Crăciunului',
                'de' => 'Weihnachtsinsel',
            ],
        ],
        'Cocos (Keeling) Islands'                                    => [
            'code'      => 'cc',
            'is_active' => true,
            'name'      => [
                'en' => 'Cocos (Keeling) Islands',
                'ro' => 'Cocos, Insulele',
                'de' => 'Kokosinseln',
            ],
        ],
        'Colombia'                                                   => [
            'code'      => 'co',
            'is_active' => true,
            'name'      => [
                'en' => 'Colombia',
                'ro' => 'Columbia',
                'de' => 'Kolumbien',
            ],
        ],
        'Comoros, Union of the'                                      => [
            'code'      => 'km',
            'is_active' => true,
            'name'      => [
                'en' => 'Comoros, Union of the',
                'ro' => 'Comore, Uniunea',
                'de' => 'Komoren, Union der',
            ],
        ],
        'Congo, Democratic Republic of the'                          => [
            'code'      => 'cd',
            'is_active' => true,
            'name'      => [
                'en' => 'Congo, Democratic Republic of the',
                'ro' => 'Congo, Republica Democrată',
                'de' => 'Kongo, Demokratische Republik',
            ],
        ],
        'Congo, Republic of the'                                     => [
            'code'      => 'cg',
            'is_active' => true,
            'name'      => [
                'en' => 'Congo, Republic of the',
                'ro' => 'Congo, Republica',
                'de' => 'Kongo, Republik',
            ],
        ],
        'Cook Islands'                                               => [
            'code'      => 'ck',
            'is_active' => true,
            'name'      => [
                'en' => 'Cook Islands',
                'ro' => 'Cook, Insulele',
                'de' => 'Cookinseln',
            ],
        ],
        'Costa Rica'                                                 => [
            'code'      => 'cr',
            'is_active' => true,
            'name'      => [
                'en' => 'Costa Rica',
                'ro' => 'Costa Rica',
                'de' => 'Costa Rica',
            ],
        ],
        'Cote D\'Ivoire'                                             => [
            'code'      => 'ci',
            'is_active' => true,
            'name'      => [
                'en' => 'Cote D\'Ivoire',
                'ro' => 'Coasta de Fildeş',
                'de' => 'Elfenbeinküste',
            ],
        ],
        'Croatia (Hrvatska)'                                         => [
            'code'      => 'hr',
            'is_active' => true,
            'name'      => [
                'en' => 'Croatia (Hrvatska)',
                'ro' => 'Croaţia',
                'de' => 'Kroatien',
            ],
        ],
        'Cuba'                                                       => [
            'code'      => 'cu',
            'is_active' => true,
            'name'      => [
                'en' => 'Cuba',
                'ro' => 'Cuba',
                'de' => 'Kuba',
            ],
        ],
        'Cyprus'                                                     => [
            'code'      => 'cy',
            'is_active' => true,
            'name'      => [
                'en' => 'Cyprus',
                'ro' => 'Cipru',
                'de' => 'Zypern',
            ],
        ],
        'Czech Republic'                                             => [
            'code'      => 'cz',
            'is_active' => true,
            'name'      => [
                'en' => 'Czech Republic',
                'ro' => 'Cehia',
                'de' => 'Tschechien',
            ],
        ],
        'Denmark'                                                    => [
            'code'      => 'dk',
            'is_active' => true,
            'name'      => [
                'en' => 'Denmark',
                'ro' => 'Danemarca',
                'de' => 'Dänemark',
            ],
        ],
        'Djibouti'                                                   => [
            'code'      => 'dj',
            'is_active' => true,
            'name'      => [
                'en' => 'Djibouti',
                'ro' => 'Djibouti',
                'de' => 'Dschibuti',
            ],
        ],
        'Dominica'                                                   => [
            'code'      => 'dm',
            'is_active' => true,
            'name'      => [
                'en' => 'Dominica',
                'ro' => 'Dominica',
                'de' => 'Dominica',
            ],
        ],
        'Dominican Republic'                                         => [
            'code'      => 'do',
            'is_active' => true,
            'name'      => [
                'en' => 'Dominican Republic',
                'ro' => 'Republica Dominicană',
                'de' => 'Dominikanische Republik',
            ],
        ],
        'East Timor'                                                 => [
            'code'      => 'tp',
            'is_active' => true,
            'name'      => [
                'en' => 'East Timor',
                'ro' => 'Timorul de Est',
                'de' => 'Osttimor',
            ],
        ],
        'Ecuador'                                                    => [
            'code'      => 'ec',
            'is_active' => true,
            'name'      => [
                'en' => 'Ecuador',
                'ro' => 'Ecuador',
                'de' => 'Ecuador',
            ],
        ],
        'Egypt'                                                      => [
            'code'      => 'eg',
            'is_active' => true,
            'name'      => [
                'en' => 'Egypt',
                'ro' => 'Egipt',
                'de' => 'Ägypten',
            ],
        ],
        'El Salvador'                                                => [
            'code'      => 'sv',
            'is_active' => true,
            'name'      => [
                'en' => 'El Salvador',
                'ro' => 'El Salvador',
                'de' => 'El Salvador',
            ],
        ],
        'Equatorial Guinea'                                          => [
            'code'      => 'gq',
            'is_active' => true,
            'name'      => [
                'en' => 'Equatorial Guinea',
                'ro' => 'Guineea Ecuatorială',
                'de' => 'Äquatorialguinea',
            ],
        ],
        'Eritrea'                                                    => [
            'code'      => 'er',
            'is_active' => true,
            'name'      => [
                'en' => 'Eritrea',
                'ro' => 'Eritreea',
                'de' => 'Eritrea',
            ],
        ],
        'Estonia'                                                    => [
            'code'      => 'ee',
            'is_active' => true,
            'name'      => [
                'en' => 'Estonia',
                'ro' => 'Estonia',
                'de' => 'Estland',
            ],
        ],
        'Ethiopia'                                                   => [
            'code'      => 'et',
            'is_active' => true,
            'name'      => [
                'en' => 'Ethiopia',
                'ro' => 'Etiopia',
                'de' => 'Äthiopien',
            ],
        ],
        'Falkland Islands'                                           => [
            'code'      => 'fk',
            'is_active' => true,
            'name'      => [
                'en' => 'Falkland Islands (Islas Malvinas)',
                'ro' => 'Falkland, Insulele',
                'de' => 'Falklandinseln',
            ],
        ],
        'Faroe Islands'                                              => [
            'code'      => 'fo',
            'is_active' => true,
            'name'      => [
                'en' => 'Faroe Islands',
                'ro' => 'Feroe, Insulele',
                'de' => 'Färöer',
            ],
        ],
        'Fiji'                                                       => [
            'code'      => 'fj',
            'is_active' => true,
            'name'      => [
                'en' => 'Fiji',
                'ro' => 'Fiji',
                'de' => 'Fidschi',
            ],
        ],
        'Finland'                                                    => [
            'code'      => 'fi',
            'is_active' => true,
            'name'      => [
                'en' => 'Finland',
                'ro' => 'Finlanda',
                'de' => 'Finnland',
            ],
        ],
        'France'                                                     => [
            'code'      => 'fr',
            'is_active' => true,
            'name'      => [
                'en' => 'France',
                'ro' => 'Franţa',
                'de' => 'Frankreich',
            ],
        ],
        'French Guiana or French Guyana'                             => [
            'code'      => 'gf',
            'is_active' => true,
            'name'      => [
                'en' => 'French Guiana or French Guyana',
                'ro' => 'Guyana Franceză',
                'de' => 'Französisch Guyana',
            ],
        ],
        'French Polynesia'                                           => [
            'code'      => 'pf',
            'is_active' => true,
            'name'      => [
                'en' => 'French Polynesia',
                'ro' => 'Polinezia Franceză',
                'de' => 'Französisch Polynesien',
            ],
        ],
        'French Southern Territories and Antarctic Lands'            => [
            'code'      => 'tf',
            'is_active' => true,
            'name'      => [
                'en' => 'French Southern Territories and Antarctic Lands',
                'ro' => 'Teritoriile australe şi antarctice franceze',
                'de' => 'Französische Süd- und Antarktisgebiete',
            ],
        ],
        'Gabon (Gabonese Republic)'                                  => [
            'code'      => 'ga',
            'is_active' => true,
            'name'      => [
                'en' => 'Gabon (Gabonese Republic)',
                'ro' => 'Gabon',
                'de' => 'Gabun',
            ],
        ],
        'Gambia, The'                                                => [
            'code'      => 'gm',
            'is_active' => true,
            'name'      => [
                'en' => 'Gambia, The',
                'ro' => 'Gambia',
                'de' => 'Gambia',
            ],
        ],
        'Georgia'                                                    => [
            'code'      => 'ge',
            'is_active' => true,
            'name'      => [
                'en' => 'Georgia',
                'ro' => 'Georgia',
                'de' => 'Georgien',
            ],
        ],
        'Germany'                                                    => [
            'code'      => 'de',
            'is_active' => true,
            'name'      => [
                'en' => 'Germany',
                'ro' => 'Germania',
                'de' => 'Deutschland',
            ],
        ],
        'Ghana'                                                      => [
            'code'      => 'gh',
            'is_active' => true,
            'name'      => [
                'en' => 'Ghana',
                'ro' => 'Ghana',
                'de' => 'Ghana',
            ],
        ],
        'Gibraltar'                                                  => [
            'code'      => 'gi',
            'is_active' => true,
            'name'      => [
                'en' => 'Gibraltar',
                'ro' => 'Gibraltar',
                'de' => 'Gibraltar',
            ],
        ],
        'Great Britain (United Kingdom)'                             => [
            'code'      => 'gb',
            'is_active' => true,
            'name'      => [
                'en' => 'Great Britain (United Kingdom)',
                'ro' => 'Regatul Unit ',
                'de' => 'Vereinigtes Königreich',
            ],
        ],
        'Greece'                                                     => [
            'code'      => 'gr',
            'is_active' => true,
            'name'      => [
                'en' => 'Greece',
                'ro' => 'Grecia',
                'de' => 'Griechenland',
            ],
        ],
        'Greenland'                                                  => [
            'code'      => 'gl',
            'is_active' => true,
            'name'      => [
                'en' => 'Greenland',
                'ro' => 'Groenlanda',
                'de' => 'Grönland',
            ],
        ],
        'Grenada'                                                    => [
            'code'      => 'gd',
            'is_active' => true,
            'name'      => [
                'en' => 'Grenada',
                'ro' => 'Grenada',
                'de' => 'Grenada',
            ],
        ],
        'Guadeloupe'                                                 => [
            'code'      => 'gp',
            'is_active' => true,
            'name'      => [
                'en' => 'Guadeloupe',
                'ro' => 'Guadelupa',
                'de' => 'Guadeloupe',
            ],
        ],
        'Guam'                                                       => [
            'code'      => 'gu',
            'is_active' => true,
            'name'      => [
                'en' => 'Guam',
                'ro' => 'Guam',
                'de' => 'Guam',
            ],
        ],
        'Guatemala'                                                  => [
            'code'      => 'gt',
            'is_active' => true,
            'name'      => [
                'en' => 'Guatemala',
                'ro' => 'Guatemala',
                'de' => 'Guatemala',
            ],
        ],
        'Guinea'                                                     => [
            'code'      => 'gn',
            'is_active' => true,
            'name'      => [
                'en' => 'Guinea',
                'ro' => 'Guinea',
                'de' => 'Guinea',
            ],
        ],
        'Guinea-Bissau'                                              => [
            'code'      => 'gw',
            'is_active' => true,
            'name'      => [
                'en' => 'Guinea-Bissau',
                'ro' => 'Guinea-Bissau',
                'de' => 'Guinea-Bissau',
            ],
        ],
        'Guyana'                                                     => [
            'code'      => 'gy',
            'is_active' => true,
            'name'      => [
                'en' => 'Guyana',
                'ro' => 'Guyana',
                'de' => 'Guyana',
            ],
        ],
        'Haiti'                                                      => [
            'code'      => 'ht',
            'is_active' => true,
            'name'      => [
                'en' => 'Haiti',
                'ro' => 'Haiti',
                'de' => 'Haiti',
            ],
        ],
        'Heard Island and McDonald Islands (Territory of Australia)' => [
            'code'      => 'hm',
            'is_active' => true,
            'name'      => [
                'en' => 'Heard Island and McDonald Islands (Territory of Australia)',
                'ro' => 'Insula Heard şi Insulele McDonald',
                'de' => 'Heard und McDonaldinseln',
            ],
        ],
        'Honduras'                                                   => [
            'code'      => 'hn',
            'is_active' => true,
            'name'      => [
                'en' => 'Honduras',
                'ro' => 'Honduras',
                'de' => 'Honduras',
            ],
        ],
        'Hong Kong'                                                  => [
            'code'      => 'hk',
            'is_active' => true,
            'name'      => [
                'en' => 'Hong Kong',
                'ro' => 'Hong Kong',
                'de' => 'Hongkong',
            ],
        ],
        'Hungary'                                                    => [
            'code'      => 'hu',
            'is_active' => true,
            'name'      => [
                'en' => 'Hungary',
                'ro' => 'Ungaria',
                'de' => 'Ungarn',
            ],
        ],
        'Iceland'                                                    => [
            'code'      => 'is',
            'is_active' => true,
            'name'      => [
                'en' => 'Iceland',
                'ro' => 'Islanda',
                'de' => 'Island',
            ],
        ],
        'India'                                                      => [
            'code'      => 'in',
            'is_active' => true,
            'name'      => [
                'en' => 'India',
                'ro' => 'India',
                'de' => 'Indien',
            ],
        ],
        'Indonesia'                                                  => [
            'code'      => 'id',
            'is_active' => true,
            'name'      => [
                'en' => 'Indonesia',
                'ro' => 'Indonezia',
                'de' => 'Indonesien',
            ],
        ],
        'Iran, Islamic Republic of'                                  => [
            'code'      => 'ir',
            'is_active' => true,
            'name'      => [
                'en' => 'Iran, Islamic Republic of',
                'ro' => 'Iran, Republica Islamică',
                'de' => 'Iran, Islamische Republik',
            ],
        ],
        'Iraq'                                                       => [
            'code'      => 'iq',
            'is_active' => true,
            'name'      => [
                'en' => 'Iraq',
                'ro' => 'Irak',
                'de' => 'Irak',
            ],
        ],
        'Ireland'                                                    => [
            'code'      => 'ie',
            'is_active' => true,
            'name'      => [
                'en' => 'Ireland',
                'ro' => 'Irlanda',
                'de' => 'Irland',
            ],
        ],
        'Israel'                                                     => [
            'code'      => 'il',
            'is_active' => true,
            'name'      => [
                'en' => 'Israel',
                'ro' => 'Israel',
                'de' => 'Israel',
            ],
        ],
        'Italy'                                                      => [
            'code'      => 'it',
            'is_active' => true,
            'name'      => [
                'en' => 'Italy',
                'ro' => 'Italia',
                'de' => 'Italien',
            ],
        ],
        'Jamaica'                                                    => [
            'code'      => 'jm',
            'is_active' => true,
            'name'      => [
                'en' => 'Jamaica',
                'ro' => 'Jamaica',
                'de' => 'Jamaika',
            ],
        ],
        'Japan'                                                      => [
            'code'      => 'jp',
            'is_active' => true,
            'name'      => [
                'en' => 'Japan',
                'ro' => 'Japonia',
                'de' => 'Japan',
            ],
        ],
        'Jordan'                                                     => [
            'code'      => 'jo',
            'is_active' => true,
            'name'      => [
                'en' => 'Jordan',
                'ro' => 'Iordania',
                'de' => 'Jordanien',
            ],
        ],
        'Kazakstan or Kazakhstan'                                    => [
            'code'      => 'kz',
            'is_active' => true,
            'name'      => [
                'en' => 'Kazakstan or Kazakhstan',
                'ro' => 'Kazahstan',
                'de' => 'Kasachstan',
            ],
        ],
        'Kenya'                                                      => [
            'code'      => 'ke',
            'is_active' => true,
            'name'      => [
                'en' => 'Kenya',
                'ro' => 'Kenya',
                'de' => 'Kenya',
            ],
        ],
        'Kiribati'                                                   => [
            'code'      => 'ki',
            'is_active' => true,
            'name'      => [
                'en' => 'Kiribati',
                'ro' => 'Kiribati',
                'de' => 'Kiribati',
            ],
        ],
        'North Korea'                                                => [
            'code'      => 'kp',
            'is_active' => true,
            'name'      => [
                'en' => 'North Korea',
                'ro' => 'Coreea de Nord',
                'de' => 'Nord Korea',
            ],
        ],
        'Korea, Republic of (South Korea)'                           => [
            'code'      => 'kr',
            'is_active' => true,
            'name'      => [
                'en' => 'Korea, Republic of (South Korea)',
                'ro' => 'Coreea de Sud',
                'de' => 'Süd Korea',
            ],
        ],
        'Kuwait'                                                     => [
            'code'      => 'kw',
            'is_active' => true,
            'name'      => [
                'en' => 'Kuwait',
                'ro' => 'Kuweit',
                'de' => 'Kuwait',
            ],
        ],
        'Kyrgyzstan'                                                 => [
            'code'      => 'kg',
            'is_active' => true,
            'name'      => [
                'en' => 'Kyrgyzstan',
                'ro' => 'Kârgâzstan',
                'de' => 'Kirgisistan',
            ],
        ],
        'Laos'                                                       => [
            'code'      => 'la',
            'is_active' => true,
            'name'      => [
                'en' => 'Laos',
                'ro' => 'Laos',
                'de' => 'Laos',
            ],
        ],
        'Latvia'                                                     => [
            'code'      => 'lv',
            'is_active' => true,
            'name'      => [
                'en' => 'Latvia',
                'ro' => 'Letonia',
                'de' => 'Lettland',
            ],
        ],
        'Lebanon'                                                    => [
            'code'      => 'lb',
            'is_active' => true,
            'name'      => [
                'en' => 'Lebanon',
                'ro' => 'Liban',
                'de' => 'Libanon',
            ],
        ],
        'Lesotho'                                                    => [
            'code'      => 'ls',
            'is_active' => true,
            'name'      => [
                'en' => 'Lesotho',
                'ro' => 'Lesotho',
                'de' => 'Lesotho',
            ],
        ],
        'Liberia'                                                    => [
            'code'      => 'lr',
            'is_active' => true,
            'name'      => [
                'en' => 'Liberia',
                'ro' => 'Liberia',
                'de' => 'Liberia',
            ],
        ],
        'Libya'                                                      => [
            'code'      => 'ly',
            'is_active' => true,
            'name'      => [
                'en' => 'Libya',
                'ro' => 'Libia',
                'de' => 'Libyen',
            ],
        ],
        'Liechtenstein'                                              => [
            'code'      => 'li',
            'is_active' => true,
            'name'      => [
                'en' => 'Liechtenstein',
                'ro' => 'Liechtenstein',
                'de' => 'Liechtenstein',
            ],
        ],
        'Lithuania'                                                  => [
            'code'      => 'lt',
            'is_active' => true,
            'name'      => [
                'en' => 'Lithuania',
                'ro' => 'Lituania',
                'de' => 'Litauen',
            ],
        ],
        'Luxembourg'                                                 => [
            'code'      => 'lu',
            'is_active' => true,
            'name'      => [
                'en' => 'Luxembourg',
                'ro' => 'Luxemburg',
                'de' => 'Luxemburg',
            ],
        ],
        'Macau'                                                      => [
            'code'      => 'mo',
            'is_active' => true,
            'name'      => [
                'en' => 'Macau',
                'ro' => 'Macao',
                'de' => 'Macau',
            ],
        ],
        'Macedonia'                                                  => [
            'code'      => 'mk',
            'is_active' => true,
            'name'      => [
                'en' => 'Macedonia',
                'ro' => 'Macedonia',
                'de' => 'Mazedonien',
            ],
        ],
        'Madagascar'                                                 => [
            'code'      => 'mg',
            'is_active' => true,
            'name'      => [
                'en' => 'Madagascar',
                'ro' => 'Madagascar',
                'de' => 'Madagaskar',
            ],
        ],
        'Malawi'                                                     => [
            'code'      => 'mw',
            'is_active' => true,
            'name'      => [
                'en' => 'Malawi',
                'ro' => 'Malawi',
                'de' => 'Malawi',
            ],
        ],
        'Malaysia'                                                   => [
            'code'      => 'my',
            'is_active' => true,
            'name'      => [
                'en' => 'Malaysia',
                'ro' => 'Malaezia',
                'de' => 'Malaysia',
            ],
        ],
        'Maldives'                                                   => [
            'code'      => 'mv',
            'is_active' => true,
            'name'      => [
                'en' => 'Maldives',
                'ro' => 'Maldive',
                'de' => 'Malediven',
            ],
        ],
        'Mali'                                                       => [
            'code'      => 'ml',
            'is_active' => true,
            'name'      => [
                'en' => 'Mali',
                'ro' => 'Mali',
                'de' => 'Mali',
            ],
        ],
        'Malta'                                                      => [
            'code'      => 'mt',
            'is_active' => true,
            'name'      => [
                'en' => 'Malta',
                'ro' => 'Malta',
                'de' => 'Malta',
            ],
        ],
        'Marshall Islands'                                           => [
            'code'      => 'mh',
            'is_active' => true,
            'name'      => [
                'en' => 'Marshall Islands',
                'ro' => 'Insulele Marshall',
                'de' => 'Marshallinseln',
            ],
        ],
        'Martinique (French)'                                        => [
            'code'      => 'mq',
            'is_active' => true,
            'name'      => [
                'en' => 'Martinique (French)',
                'ro' => 'Martinica',
                'de' => 'Martinique',
            ],
        ],
        'Mauritania'                                                 => [
            'code'      => 'mr',
            'is_active' => true,
            'name'      => [
                'en' => 'Mauritania',
                'ro' => 'Mauritania',
                'de' => 'Mauretanien',
            ],
        ],
        'Mauritius'                                                  => [
            'code'      => 'mu',
            'is_active' => true,
            'name'      => [
                'en' => 'Mauritius',
                'ro' => 'Mauritius',
                'de' => 'Mauritius',
            ],
        ],
        'Mayotte'                                                    => [
            'code'      => 'yt',
            'is_active' => true,
            'name'      => [
                'en' => 'Mayotte',
                'ro' => 'Mayotte',
                'de' => 'Mayotte',
            ],
        ],
        'Mexico'                                                     => [
            'code'      => 'mx',
            'is_active' => true,
            'name'      => [
                'en' => 'Mexico',
                'ro' => 'Mexic',
                'de' => 'Mexiko',
            ],
        ],
        'Micronesia, Federated States of'                            => [
            'code'      => 'fm',
            'is_active' => true,
            'name'      => [
                'en' => 'Micronesia, Federated States of',
                'ro' => 'Microneziei, Statele Federate ale',
                'de' => 'Mikronesien',
            ],
        ],
        'Moldova, Republic of'                                       => [
            'code'      => 'md',
            'is_active' => true,
            'name'      => [
                'en' => 'Moldova, Republic of',
                'ro' => 'Moldova, Republica' ,
                'de' => 'Moldau, Republik',
            ],
        ],
        'Monaco, Principality of'                                    => [
            'code'      => 'mc',
            'is_active' => true,
            'name'      => [
                'en' => 'Monaco, Principality of',
                'ro' => 'Monaco, Principatul',
                'de' => 'Monaco Fürstentum',
            ],
        ],
        'Mongolia'                                                   => [
            'code'      => 'mn',
            'is_active' => true,
            'name'      => [
                'en' => 'Mongolia',
                'ro' => 'Mongolia',
                'de' => 'Mongolei',
            ],
        ],
        'Montserrat'                                                 => [
            'code'      => 'ms',
            'is_active' => true,
            'name'      => [
                'en' => 'Montserrat',
                'ro' => 'Montserrat',
                'de' => 'Montserrat',
            ],
        ],
        'Morocco'                                                    => [
            'code'      => 'ma',
            'is_active' => true,
            'name'      => [
                'en' => 'Morocco',
                'ro' => 'Maroc',
                'de' => 'Marokko',
            ],
        ],
        'Mozambique'                                                 => [
            'code'      => 'mz',
            'is_active' => true,
            'name'      => [
                'en' => 'Mozambique',
                'ro' => 'Mozambic',
                'de' => 'Mosambik',
            ],
        ],
        'Myanmar, Union of'                                          => [
            'code'      => 'mm',
            'is_active' => true,
            'name'      => [
                'en' => 'Myanmar, Union of',
                'ro' => 'Myanmar',
                'de' => 'Myanmar',
            ],
        ],
        'Namibia'                                                    => [
            'code'      => 'na',
            'is_active' => true,
            'name'      => [
                'en' => 'Namibia',
                'ro' => 'Namibia',
                'de' => 'Namibia',
            ],
        ],
        'Nauru'                                                      => [
            'code'      => 'nr',
            'is_active' => true,
            'name'      => [
                'en' => 'Nauru',
                'ro' => 'Nauru',
                'de' => 'Nauru',
            ],
        ],
        'Nepal'                                                      => [
            'code'      => 'np',
            'is_active' => true,
            'name'      => [
                'en' => 'Nepal',
                'ro' => 'Nepal',
                'de' => 'Nepal',
            ],
        ],
        'Netherlands'                                                => [
            'code'      => 'nl',
            'is_active' => true,
            'name'      => [
                'en' => 'Netherlands',
                'ro' => 'Olanda',
                'de' => 'Niederlande',
            ],
        ],
        'Netherlands Antilles'                                       => [
            'code'      => 'an',
            'is_active' => true,
            'name'      => [
                'en' => 'Netherlands Antilles',
                'ro' => 'Antilele Olandeze',
                'de' => 'Niederländischen Antille',
            ],
        ],
        'New Caledonia'                                              => [
            'code'      => 'nc',
            'is_active' => true,
            'name'      => [
                'en' => 'New Caledonia',
                'ro' => 'Noua Caledonie',
                'de' => 'Neukaledonien',
            ],
        ],
        'New Zealand (Aotearoa)'                                     => [
            'code'      => 'nz',
            'is_active' => true,
            'name'      => [
                'en' => 'New Zealand (Aotearoa)',
                'ro' => 'Noua Zeelandă',
                'de' => 'Neuseeland',
            ],
        ],
        'Nicaragua'                                                  => [
            'code'      => 'ni',
            'is_active' => true,
            'name'      => [
                'en' => 'Nicaragua',
                'ro' => 'Nicaragua',
                'de' => 'Nicaragua',
            ],
        ],
        'Niger'                                                      => [
            'code'      => 'ne',
            'is_active' => true,
            'name'      => [
                'en' => 'Niger',
                'ro' => 'Niger',
                'de' => 'Niger',
            ],
        ],
        'Nigeria'                                                    => [
            'code'      => 'ng',
            'is_active' => true,
            'name'      => [
                'en' => 'Nigeria',
                'ro' => 'Nigeria',
                'de' => 'Nigeria',
            ],
        ],
        'Niue'                                                       => [
            'code'      => 'nu',
            'is_active' => true,
            'name'      => [
                'en' => 'Niue',
                'ro' => 'Niue',
                'de' => 'Niue',
            ],
        ],
        'Norfolk Island'                                             => [
            'code'      => 'nf',
            'is_active' => true,
            'name'      => [
                'en' => 'Norfolk Island',
                'ro' => 'Insulele Norfolk',
                'de' => 'Norfolkinsel',
            ],
        ],
        'Northern Mariana Islands'                                   => [
            'code'      => 'mp',
            'is_active' => true,
            'name'      => [
                'en' => 'Northern Mariana Islands',
                'ro' => 'Comunitatea Insulelor Mariane de Nord',
                'de' => 'Nördliche Marianen',
            ],
        ],
        'Norway'                                                     => [
            'code'      => 'no',
            'is_active' => true,
            'name'      => [
                'en' => 'Norway',
                'ro' => 'Norvegia',
                'de' => 'Norwegen',
            ],
        ],
        'Oman, Sultanate of'                                         => [
            'code'      => 'om',
            'is_active' => true,
            'name'      => [
                'en' => 'Oman, Sultanate of',
                'ro' => 'Oman, Sultanatul',
                'de' => 'Oman Sultanat',
            ],
        ],
        'Pakistan'                                                   => [
            'code'      => 'pk',
            'is_active' => true,
            'name'      => [
                'en' => 'Pakistan',
                'ro' => 'Pakistan',
                'de' => 'Pakistan',
            ],
        ],
        'Palau'                                                      => [
            'code'      => 'pw',
            'is_active' => true,
            'name'      => [
                'en' => 'Palau',
                'ro' => 'Palau',
                'de' => 'Palau',
            ],
        ],
        'Palestine'                                                  => [
            'code'      => 'ps',
            'is_active' => true,
            'name'      => [
                'en' => 'Palestine',
                'ro' => 'Palestina',
                'de' => 'Palästina',
            ],
        ],
        'Panama'                                                     => [
            'code'      => 'pa',
            'is_active' => true,
            'name'      => [
                'en' => 'Panama',
                'ro' => 'Panama',
                'de' => 'Panama',
            ],
        ],
        'Papua New Guinea'                                           => [
            'code'      => 'pg',
            'is_active' => true,
            'name'      => [
                'en' => 'Papua New Guinea',
                'ro' => 'Papua Noua Guinee',
                'de' => 'Papua-Neuguinea',
            ],
        ],
        'Paraguay'                                                   => [
            'code'      => 'py',
            'is_active' => true,
            'name'      => [
                'en' => 'Paraguay',
                'ro' => 'Paraguay',
                'de' => 'Paraguay',
            ],
        ],
        'Peru'                                                       => [
            'code'      => 'pe',
            'is_active' => true,
            'name'      => [
                'en' => 'Peru',
                'ro' => 'Peru',
                'de' => 'Peru',
            ],
        ],
        'Philippines'                                                => [
            'code'      => 'ph',
            'is_active' => true,
            'name'      => [
                'en' => 'Philippines',
                'ro' => 'Filipine',
                'de' => 'Philippinen',
            ],
        ],
        'Pitcairn Island'                                            => [
            'code'      => 'pn',
            'is_active' => true,
            'name'      => [
                'en' => 'Pitcairn Island',
                'ro' => 'Pitcairn, Insulele',
                'de' => 'Pitcairninseln',
            ],
        ],
        'Poland'                                                     => [
            'code'      => 'pl',
            'is_active' => true,
            'name'      => [
                'en' => 'Poland',
                'ro' => 'Polonia',
                'de' => 'Polen',
            ],
        ],
        'Portugal'                                                   => [
            'code'      => 'pt',
            'is_active' => true,
            'name'      => [
                'en' => 'Portugal',
                'ro' => 'Portugalia',
                'de' => 'Portugal',
            ],
        ],
        'Puerto Rico'                                                => [
            'code'      => 'pr',
            'is_active' => true,
            'name'      => [
                'en' => 'Puerto Rico',
                'ro' => 'Puerto Rico',
                'de' => 'Puerto Rico',
            ],
        ],
        'Qatar, State of'                                            => [
            'code'      => 'qa',
            'is_active' => true,
            'name'      => [
                'en' => 'Qatar, State of',
                'ro' => 'Qatar, Statul',
                'de' => 'Katar, Staat',
            ],
        ],
        'Reunion'                                                    => [
            'code'      => 're',
            'is_active' => true,
            'name'      => [
                'en' => 'Reunion',
                'ro' => 'Reunion',
                'de' => 'Reunion',
            ],
        ],
        'Romania'                                                    => [
            'code'      => 'ro',
            'is_active' => true,
            'name'      => [
                'en' => 'Romania',
                'ro' => 'România',
                'de' => 'Rumänien',
            ],
        ],
        'Russia (Russian Federation)'                                 => [
            'code'      => 'ru',
            'is_active' => true,
            'name'      => [
                'en' => 'Russia (Russian Federation)',
                'ro' => 'Rusia (Federaţia Rusă)',
                'de' => 'Russland (Russische Föderation) ',
            ],
        ],
        'Rwanda (Rwandese Republic)'                                 => [
            'code'      => 'rw',
            'is_active' => true,
            'name'      => [
                'en' => 'Rwanda (Rwandese Republic)',
                'ro' => 'Rwanda, Republica',
                'de' => 'Ruanda, Republik ',
            ],
        ],
        'Saint Helena'                                               => [
            'code'      => 'sh',
            'is_active' => true,
            'name'      => [
                'en' => 'Saint Helena',
                'ro' => 'Saint Helena',
                'de' => 'Saint Helena',
            ],
        ],
        'Saint Kitts and Nevis'                                      => [
            'code'      => 'kn',
            'is_active' => true,
            'name'      => [
                'en' => 'Saint Kitts and Nevis',
                'ro' => 'Sfântul Cristofor şi Nevis',
                'de' => 'Saint Kitts und Nevis',
            ],
        ],
        'Saint Lucia'                                                => [
            'code'      => 'lc',
            'is_active' => true,
            'name'      => [
                'en' => 'Saint Lucia',
                'ro' => 'Sfânta Lucia',
                'de' => 'Saint Lucia',
            ],
        ],
        'Saint Pierre and Miquelon'                                  => [
            'code'      => 'pm',
            'is_active' => true,
            'name'      => [
                'en' => 'Saint Pierre and Miquelon',
                'ro' => 'Sfântul Pierre şi Miquelon ',
                'de' => 'Saint Pierre und Miquelon',
            ],
        ],
        'Saint Vincent and the Grenadines'                           => [
            'code'      => 'vc',
            'is_active' => true,
            'name'      => [
                'en' => 'Saint Vincent and the Grenadines',
                'ro' => 'Sfântul Vicenţiu şi Grenadinele',
                'de' => 'Saint Vincent und die Grenadinen',
            ],
        ],
        'Samoa'                                                      => [
            'code'      => 'ws',
            'is_active' => true,
            'name'      => [
                'en' => 'Samoa',
                'ro' => 'Samoa',
                'de' => 'Samoa',
            ],
        ],
        'San Marino'                                                 => [
            'code'      => 'sm',
            'is_active' => true,
            'name'      => [
                'en' => 'San Marino',
                'ro' => 'San Marino',
                'de' => 'San Marino',
            ],
        ],
        'São Tomé and Principe'                                      => [
            'code'      => 'st',
            'is_active' => true,
            'name'      => [
                'en' => 'São Tomé and Principe',
                'ro' => 'São Tomé şi Príncipe',
                'de' => 'São Tomé und Príncipe',
            ],
        ],
        'Saudi Arabia'                                               => [
            'code'      => 'sa',
            'is_active' => true,
            'name'      => [
                'en' => 'Saudi Arabia',
                'ro' => 'Arabia Saudită',
                'de' => 'Saudi-Arabien',
            ],
        ],
        'Serbia, Republic of'                                        => [
            'code'      => 'rs',
            'is_active' => true,
            'name'      => [
                'en' => 'Serbia, Republic of',
                'ro' => 'Serbia, Republica',
                'de' => 'Serbien, Republik',
            ],
        ],
        'Senegal'                                                    => [
            'code'      => 'sn',
            'is_active' => true,
            'name'      => [
                'en' => 'Senegal',
                'ro' => 'Senegal',
                'de' => 'Senegal',
            ],
        ],
        'Seychelles'                                                 => [
            'code'      => 'sc',
            'is_active' => true,
            'name'      => [
                'en' => 'Seychelles',
                'ro' => 'Seychelles',
                'de' => 'Seychellen',
            ],
        ],
        'Sierra Leone'                                               => [
            'code'      => 'sl',
            'is_active' => true,
            'name'      => [
                'en' => 'Sierra Leone',
                'ro' => 'Sierra Leone',
                'de' => 'Sierra Leone',
            ],
        ],
        'Singapore'                                                  => [
            'code'      => 'sg',
            'is_active' => true,
            'name'      => [
                'en' => 'Singapore',
                'ro' => 'Singapore',
                'de' => 'Singapur',
            ],
        ],
        'Slovakia'                                                   => [
            'code'      => 'sk',
            'is_active' => true,
            'name'      => [
                'en' => 'Slovakia',
                'ro' => 'Slovacia',
                'de' => 'Slowakei',
            ],
        ],
        'Slovenia'                                                   => [
            'code'      => 'si',
            'is_active' => true,
            'name'      => [
                'en' => 'Slovenia',
                'ro' => 'Slovenia',
                'de' => 'Slowenien',
            ],
        ],
        'Solomon Islands'                                            => [
            'code'      => 'sb',
            'is_active' => true,
            'name'      => [
                'en' => 'Solomon Islands',
                'ro' => 'Solomon, Insulele',
                'de' => 'Solomoninseln',
            ],
        ],
        'Somalia'                                                    => [
            'code'      => 'so',
            'is_active' => true,
            'name'      => [
                'en' => 'Somalia',
                'ro' => 'Somalia',
                'de' => 'Somalia',
            ],
        ],
        'South Africa'                                               => [
            'code'      => 'za',
            'is_active' => true,
            'name'      => [
                'en' => 'South Africa',
                'ro' => 'Africa de Sud',
                'de' => 'Südafrika',
            ],
        ],
        'South Georgia and the South Sandwich Islands'               => [
            'code'      => 'gs',
            'is_active' => true,
            'name'      => [
                'en' => 'South Georgia and the South Sandwich Islands',
                'ro' => 'Georgia de Sud şi Insulele Sandwich de Sud',
                'de' => 'Südgeorgien und die Südlichen Sandwichinseln',
            ],
        ],
        'Spain'                                                      => [
            'code'      => 'es',
            'is_active' => true,
            'name'      => [
                'en' => 'Spain',
                'ro' => 'Spania',
                'de' => 'Spanien',
            ],
        ],
        'Sri Lanka'                                                  => [
            'code'      => 'lk',
            'is_active' => true,
            'name'      => [
                'en' => 'Sri Lanka',
                'ro' => 'Sri Lanka',
                'de' => 'Sri Lanka',
            ],
        ],
        'Sudan'                                                      => [
            'code'      => 'sd',
            'is_active' => true,
            'name'      => [
                'en' => 'Sudan',
                'ro' => 'Sudan',
                'de' => 'Sudan',
            ],
        ],
        'South Sudan'                                                => [
            'code'      => 'ss',
            'is_active' => true,
            'name'      => [
                'en' => 'South Sudan',
                'ro' => 'Sudanul de Sud',
                'de' => 'Südsudan',
            ],
        ],
        'Suriname'                                                   => [
            'code'      => 'sr',
            'is_active' => true,
            'name'      => [
                'en' => 'Suriname',
                'ro' => 'Suriname',
                'de' => 'Suriname',
            ],
        ],
        'Svalbard (Spitzbergen) and Jan Mayen Islands'               => [
            'code'      => 'sj',
            'is_active' => true,
            'name'      => [
                'en' => 'Svalbard (Spitzbergen) and Jan Mayen Islands',
                'ro' => 'Svalbard (Spitzbergen) si Insula Jan Mayen',
                'de' => 'Svalbard (Spitzbergen) und Jan Mayen Insel',
            ],
        ],
        'Swaziland, Kingdom of'                                      => [
            'code'      => 'sz',
            'is_active' => true,
            'name'      => [
                'en' => 'Swaziland, Kingdom of',
                'ro' => 'Swaziland',
                'de' => 'Swasiland',
            ],
        ],
        'Sweden'                                                     => [
            'code'      => 'se',
            'is_active' => true,
            'name'      => [
                'en' => 'Sweden',
                'ro' => 'Suedia',
                'de' => 'Schweden',
            ],
        ],
        'Switzerland'                                                => [
            'code'      => 'ch',
            'is_active' => true,
            'name'      => [
                'en' => 'Switzerland',
                'ro' => 'Elveţia',
                'de' => 'Schweiz',
            ],
        ],
        'Syria'                                                      => [
            'code'      => 'sy',
            'is_active' => true,
            'name'      => [
                'en' => 'Syria',
                'ro' => 'Siria',
                'de' => 'Syrien',
            ],
        ],
        'Taiwan'                                                     => [
            'code'      => 'tw',
            'is_active' => true,
            'name'      => [
                'en' => 'Taiwan',
                'ro' => 'Taiwan',
                'de' => 'Taiwan',
            ],
        ],
        'Tadjikistan'                                                => [
            'code'      => 'tj',
            'is_active' => true,
            'name'      => [
                'en' => 'Tadjikistan',
                'ro' => 'Tadjikistan',
                'de' => 'Tadschikistan',
            ],
        ],
        'Tanzania, United Republic of'                               => [
            'code'      => 'tz',
            'is_active' => true,
            'name'      => [
                'en' => 'Tanzania, United Republic of',
                'ro' => 'Tanzania, Republica Unită',
                'de' => 'Tansania, Vereinigte Republik',
            ],
        ],
        'Thailand'                                                   => [
            'code'      => 'th',
            'is_active' => true,
            'name'      => [
                'en' => 'Thailand',
                'ro' => 'Thailanda',
                'de' => 'Thailand',
            ],
        ],
        'Togo'                                                       => [
            'code'      => 'tg',
            'is_active' => true,
            'name'      => [
                'en' => 'Togo',
                'ro' => 'Togo',
                'de' => 'Togo',
            ],
        ],
        'Tokelau'                                                    => [
            'code'      => 'tk',
            'is_active' => true,
            'name'      => [
                'en' => 'Tokelau',
                'ro' => 'Tokelau',
                'de' => 'Tokelau',
            ],
        ],
        'Tonga, Kingdom of'                                          => [
            'code'      => 'to',
            'is_active' => true,
            'name'      => [
                'en' => 'Tonga, Kingdom of',
                'ro' => 'Tonga, Regatul',
                'de' => 'Tonga, Königreich',
            ],
        ],
        'Trinidad and Tobago'                                        => [
            'code'      => 'tt',
            'is_active' => true,
            'name'      => [
                'en' => 'Trinidad and Tobago',
                'ro' => 'Trinidad şi Tobago',
                'de' => 'Trinidad und Tobago',
            ],
        ],
        'Tromelin Island'                                            => [
            'code'      => 'te',
            'is_active' => true,
            'name'      => [
                'en' => 'Tromelin Island',
                'ro' => 'Tromelin, Insula',
                'de' => 'Tromelininsel',
            ],
        ],
        'Tunisia'                                                    => [
            'code'      => 'tn',
            'is_active' => true,
            'name'      => [
                'en' => 'Tunisia',
                'ro' => 'Tunisia',
                'de' => 'Tunesien',
            ],
        ],
        'Turkey'                                                     => [
            'code'      => 'tr',
            'is_active' => true,
            'name'      => [
                'en' => 'Turkey',
                'ro' => 'Turcia',
                'de' => 'Türkei',
            ],
        ],
        'Turkmenistan'                                               => [
            'code'      => 'tm',
            'is_active' => true,
            'name'      => [
                'en' => 'Turkmenistan',
                'ro' => 'Turkmenistan',
                'de' => 'Turkmenistan',
            ],
        ],
        'Turks and Caicos Islands'                                   => [
            'code'      => 'tc',
            'is_active' => true,
            'name'      => [
                'en' => 'Turks and Caicos Islands',
                'ro' => 'Turks şi Insulele Caicos ',
                'de' => 'Turks und Caicosinseln',
            ],
        ],
        'Tuvalu'                                                     => [
            'code'      => 'tv',
            'is_active' => true,
            'name'      => [
                'en' => 'Tuvalu',
                'ro' => 'Tuvalu',
                'de' => 'Tuvalu',
            ],
        ],
        'Uganda, Republic of'                                        => [
            'code'      => 'ug',
            'is_active' => true,
            'name'      => [
                'en' => 'Uganda, Republic of',
                'ro' => 'Uganda, Republica',
                'de' => 'Uganda, Republik',
            ],
        ],
        'Ukraine'                                                    => [
            'code'      => 'ua',
            'is_active' => true,
            'name'      => [
                'en' => 'Ukraine',
                'ro' => 'Ucraina',
                'de' => 'Ukraine',
            ],
        ],
        'United Arab Emirates (UAE)'                                 => [
            'code'      => 'ae',
            'is_active' => true,
            'name'      => [
                'en' => 'United Arab Emirates (UAE)',
                'ro' => 'Emiratele Arabe Unite',
                'de' => 'Vereinigte Arabische Emirate',
            ],
        ],
        'United States'                                              => [
            'code'      => 'us',
            'is_active' => true,
            'name'      => [
                'en' => 'United States',
                'ro' => 'Statele Unite',
                'de' => 'Vereinigte Staaten',
            ],
        ],
        'United States Minor Outlying Islands'                       => [
            'code'      => 'um',
            'is_active' => true,
            'name'      => [
                'en' => 'United States Minor Outlying Islands',
                'ro' => 'Insulele Minore Îndepărtate ale Statelor Unite',
                'de' => 'Kleinere Inselbesitzungen der Vereinigten Staaten',
            ],
        ],
        'Uruguay, Oriental Republic of'                              => [
            'code'      => 'uy',
            'is_active' => true,
            'name'      => [
                'en' => 'Uruguay, Oriental Republic of',
                'ro' => 'Uruguay, Republica Orientală',
                'de' => 'Uruguay, Republik Östlich des',
            ],
        ],
        'Uzbekistan'                                                 => [
            'code'      => 'uz',
            'is_active' => true,
            'name'      => [
                'en' => 'Uzbekistan',
                'ro' => 'Uzbekistan',
                'de' => 'Usbekistan',
            ],
        ],
        'Vanuatu'                                                    => [
            'code'      => 'vu',
            'is_active' => true,
            'name'      => [
                'en' => 'Vanuatu',
                'ro' => 'Vanuatu',
                'de' => 'Vanuatu',
            ],
        ],
        'Vatican City State (Holy See)'                              => [
            'code'      => 'va',
            'is_active' => true,
            'name'      => [
                'en' => 'Vatican City State (Holy See)',
                'ro' => 'Vatican, Statul',
                'de' => 'Vatikanstadt, Staat',
            ],
        ],
        'Venezuela'                                                  => [
            'code'      => 've',
            'is_active' => true,
            'name'      => [
                'en' => 'Venezuela',
                'ro' => 'Venezuela',
                'de' => 'Venezuela',
            ],
        ],
        'Vietnam'                                                    => [
            'code'      => 'vn',
            'is_active' => true,
            'name'      => [
                'en' => 'Vietnam',
                'ro' => 'Vietnam',
                'de' => 'Vietnam',
            ],
        ],
        'Virgin Islands, British'                                    => [
            'code'      => 'vi',
            'is_active' => true,
            'name'      => [
                'en' => 'Virgin Islands, British',
                'ro' => 'Insulele Virgine Britanice',
                'de' => 'Britischen Jungferninsel',
            ],
        ],
        'Virgin Islands, United States'                              => [
            'code'      => 'vq',
            'is_active' => true,
            'name'      => [
                'en' => 'Virgin Islands, United States',
                'ro' => 'Insulele Virgine Americane',
                'de' => 'Amerikanischen Jungferninseln',
            ],
        ],
        'Wallis and Futuna Islands'                                  => [
            'code'      => 'wf',
            'is_active' => true,
            'name'      => [
                'en' => 'Wallis and Futuna Islands',
                'ro' => 'Wallis şi Futuna, Insulele',
                'de' => 'Wallis und Futuna',
            ],
        ],
        'Western Sahara'                                             => [
            'code'      => 'eh',
            'is_active' => true,
            'name'      => [
                'en' => 'Western Sahara',
                'ro' => 'Sahara de Vest',
                'de' => 'Westsahara',
            ],
        ],
        'Yemen'                                                      => [
            'code'      => 'ye',
            'is_active' => true,
            'name'      => [
                'en' => 'Yemen',
                'ro' => 'Yemen',
                'de' => 'Yemen',
            ],
        ],
        'Zambia, Republic of'                                        => [
            'code'      => 'zm',
            'is_active' => true,
            'name'      => [
                'en' => 'Zambia, Republic of',
                'ro' => 'Zambia, Republica',
                'de' => 'Sambia, Republik',
            ],
        ],
        'Zimbabwe, Republic of'                                      => [
            'code'      => 'zw',
            'is_active' => true,
            'name'      => [
                'en' => 'Zimbabwe, Republic of',
                'ro' => 'Zimbabwe, Republica',
                'de' => 'Simbabwe, Republik',
            ],
        ],
    ];
}
