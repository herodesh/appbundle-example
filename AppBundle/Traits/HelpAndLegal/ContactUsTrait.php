<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal;

use Flendoc\AppBundle\Constants\ContactUsConstants;

/**
 * Trait ContactUsTrait
 * @package Flendoc\AppBundle\Traits\HelpAndLegal
 */
trait ContactUsTrait
{
    /** @var array */
    protected $aContactUs = [
        ContactUsConstants::CU_ACCOUNT_PROBLEM => [
            'contactUsIdentifier' => ContactUsConstants::CU_ACCOUNT_PROBLEM,
            'content'             => [
                'en' => [
                    'title' => 'I have a problem with my account',
                ],
                'ro' => [
                    'title' => 'Am o problemǎ cu contul meu',
                ],
            ],
        ],

        ContactUsConstants::CU_DELETE_ACCOUNT => [
            'contactUsIdentifier' => ContactUsConstants::CU_DELETE_ACCOUNT,
            'content'             => [
                'en' => [
                    'title' => 'I would like to delete my account',
                ],
                'ro' => [
                    'title' => 'Aş dori să îmi şterg contul',
                ],
            ],
        ],

        ContactUsConstants::CU_FAQ_REQUEST => [
            'contactUsIdentifier' => ContactUsConstants::CU_FAQ_REQUEST,
            'content'             => [
                'en' => [
                    'title' => 'FAQ question request',
                ],
                'ro' => [
                    'title' => 'Cerere de întrebare pentru "Întrebări frecvente"',
                ],
            ],
        ],

        ContactUsConstants::CU_OTHERS => [
            'contactUsIdentifier' => ContactUsConstants::CU_OTHERS,
            'content'             => [
                'en' => [
                    'title' => 'Other',
                ],
                'ro' => [
                    'title' => 'Altele',
                ],
            ],
        ],
    ];
}
