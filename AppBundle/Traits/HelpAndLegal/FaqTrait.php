<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal;

use Flendoc\AppBundle\Traits\HelpAndLegal\Faq\FaqDoctorsVerificationTrait;
use Flendoc\AppBundle\Traits\HelpAndLegal\Faq\FaqMedicalCasesTrait;
use Flendoc\AppBundle\Traits\HelpAndLegal\Faq\FaqUsersTrait;

/**
 * Trait FaqTrait
 * @package Flendoc\AppBundle\Traits\HelpAndLegal
 */
trait FaqTrait
{
    use FaqMedicalCasesTrait;
    use FaqUsersTrait;
    use FaqDoctorsVerificationTrait;

    /** @var array */
    protected $aFaqs;

    /**
     * @return $this
     */
    public function getFaqs()
    {
        $this->aFaqs = [
            $this->getMedicalCasesFaq(),
            $this->getUsersFaq(),
            $this->getDoctorsVerificationFaq(),
        ];

        return $this;
    }
}
