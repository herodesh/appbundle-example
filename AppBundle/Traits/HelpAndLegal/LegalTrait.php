<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal;

/**
 * Trait LegalTrait
 * @package Flendoc\AppBundle\Traits\MedicalCases
 */
trait LegalTrait
{
    /** @var array */
    protected $aLegal = [
        'termsAndConditions' => [
            'legalIdentifier' => 'terms_and_conditions',
            'content'         => [
                'en' => [
                    'title'       => 'Terms and conditions',
                    'description' => 'Content to be edited by admin',
                ],
                'ro' => [
                    'title'       => 'Termeni şi condiţii',
                    'description' => 'Conţinutul va trebui adăugat de admin',
                ],
            ],
        ],

        'privacy' => [
            'legalIdentifier' => 'privacy_policy',
            'content'         => [
                'en' => [
                    'title'       => 'Privacy',
                    'description' => 'Content to be edited by the admin',
                ],
                'ro' => [
                    'title'       => 'Confidenţialitate',
                    'description' => 'Conţinutul va trebui adăugat de admin',
                ],
            ],
        ],

    ];
}
