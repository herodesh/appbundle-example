<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal\Faq;

use Flendoc\AppBundle\Constants\FaqConstants;

/**
 * Trait FaqUsersTrait
 * @package Flendoc\AppBundle\Traits\HelpAndLegal\Faq
 */
trait FaqUsersTrait
{
    /**
     * @return array
     */
    public function getUsersFaq()
    {
        return [
            FaqConstants::FAQ_USERS_WHAT_IS_A_GHOST_USER => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_WHAT_IS_A_GHOST_USER,
                'position'      => 1,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'What is a Ghost User?',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_USERS_HOW_DO_I_REGISTER => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_HOW_DO_I_REGISTER,
                'position'      => 2,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'How do i register?',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_USERS_RESET_PASSWORD => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_RESET_PASSWORD,
                'position'      => 3,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Reset your password',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_USERS_UPDATE_YOUR_PASSWORD => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_UPDATE_YOUR_PASSWORD,
                'position'      => 4,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Update your password',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_USERS_UPDATE_EMAIL_ADDRESS => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_UPDATE_EMAIL_ADDRESS,
                'position'      => 5,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'How do I change my email address',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_USERS_UPDATE_YOUR_PROFILE => [
                'faqIdentifier' => FaqConstants::FAQ_USERS_UPDATE_YOUR_PROFILE,
                'position'      => 6,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'How do I update my profile?',
                        'description' => 'Content to be edited by admin',
                    ],
                ],
            ],
        ];
    }
}
