<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal\Faq;

use Flendoc\AppBundle\Constants\FaqConstants;

/**
 * Trait FaqDoctorsVerificationTrait
 * @package Flendoc\AppBundle\Traits\HelpAndLegal\Faq
 */
trait FaqDoctorsVerificationTrait
{
    /**
     * @return array
     */
    public function getDoctorsVerificationFaq()
    {
        return [
            FaqConstants::FAQ_DOCTORS_VERIFICATION_HOW_DO_I_BECOME_VERIFIED => [
                'faqIdentifier' => FaqConstants::FAQ_DOCTORS_VERIFICATION_HOW_DO_I_BECOME_VERIFIED,
                'position'      => 1,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'How do I become verified?',
                        'description' => 'Text by admin',
                    ],
                ],
            ],
            FaqConstants::FAQ_DOCTORS_VERIFICATION_WHY_DO_I_NEED_TO_BE_VERIFIED => [
                'faqIdentifier' => FaqConstants::FAQ_DOCTORS_VERIFICATION_WHY_DO_I_NEED_TO_BE_VERIFIED,
                'position'      => 2,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Why do I need to verify myself?',
                        'description' => 'Text by admin',
                    ],
                ],
            ],
        ];
    }
}
