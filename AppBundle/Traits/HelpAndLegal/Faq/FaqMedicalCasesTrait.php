<?php

namespace Flendoc\AppBundle\Traits\HelpAndLegal\Faq;

use Flendoc\AppBundle\Constants\FaqConstants;

/**
 * Trait FaqMedicalCasesTrait
 * @package Flendoc\AppBundle\Traits\HelpAndLegal\Faq
 */
trait FaqMedicalCasesTrait
{
    /**
     * @return array
     */
    public function getMedicalCasesFaq()
    {
        return [
            FaqConstants::FAQ_MEDICAL_CASES_PATIENT_IDENTIFIER_LIST => [
                'faqIdentifier' => FaqConstants::FAQ_MEDICAL_CASES_PATIENT_IDENTIFIER_LIST,
                'position'      => 1,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'What are patients identification elements?',
                        'description' => 'Text by admin',
                    ],
                ],
            ],

            FaqConstants::FAQ_MEDICAL_CASES_CREATE_NEW_CASE => [
                'faqIdentifier' => FaqConstants::FAQ_MEDICAL_CASES_CREATE_NEW_CASE,
                'position'      => 2,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Create new Medical case',
                        'description' => 'Text by admin',
                    ],
                ],
            ],

            FaqConstants::FAQ_MEDICAL_CASES_PUBLISH_CASE => [
                'faqIdentifier' => FaqConstants::FAQ_MEDICAL_CASES_PUBLISH_CASE,
                'position'      => 3,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Publish newly created Medical case',
                        'description' => 'Text by admin',
                    ],
                ],
            ],

            FaqConstants::FAQ_MEDICAL_CASES_CREATE_NEW_CASE_WITHOUT_IMAGE => [
                'faqIdentifier' => FaqConstants::FAQ_MEDICAL_CASES_CREATE_NEW_CASE_WITHOUT_IMAGE,
                'position'      => 4,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Share medical cases without an image',
                        'description' => 'Text by admin',
                    ],
                ],
            ],

            FaqConstants::FAQ_MEDICAL_CASES_DELETE_MEDICAL_CASE => [
                'faqIdentifier' => FaqConstants::FAQ_MEDICAL_CASES_DELETE_MEDICAL_CASE,
                'position'      => 5,
                'isActive'      => false,
                'content'       => [
                    'en' => [
                        'title'       => 'Delete medical case',
                        'description' => 'Text by admin',
                    ],
                ],
            ],
        ];
    }
}
