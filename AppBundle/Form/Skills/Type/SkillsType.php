<?php

namespace Flendoc\AppBundle\Form\Skills\Type;

use Flendoc\AppBundle\Entity\Skills\SkillLanguages;
use Flendoc\AppBundle\Form\MedicalCases\Type\MedicalCaseUserActionLanguagesType;
use Flendoc\AppBundle\Form\Skills\EventListener\AddSkillLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SkillsType
 * @package Flendoc\AppBundle\Form\Skills\Type
 */
class SkillsType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('skillLanguages', CollectionType::class, [
                'entry_type'   => SkillLanguagesType::class,
                'by_reference' => false,
            ]);

        $builder->addEventSubscriber(new AddSkillLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Flendoc\AppBundle\Entity\Skills\Skills',
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_skill';
    }
}
