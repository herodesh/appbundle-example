<?php

namespace Flendoc\AppBundle\Form\Skills\EventListener;

use Flendoc\AppBundle\Entity\Skills\Skills;
use Flendoc\AppBundle\Entity\Skills\SkillLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddSkillLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Skills\EventListener
 */
class AddSkillLanguagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddSkillLanguagesSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oSkills  = $oEvent->getData();

        if ($oSkills->getSkillLanguages()->isEmpty()) {
            $this->_addSkillLanguage($oSkills, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oSkills->getSkillLanguages() as $oSkillLanguages) {
                $aExistingLanguagesIds[] = $oSkillLanguages->getLanguages()->getId();
            }
            $this->_addSkillLanguage($oSkills, $aExistingLanguagesIds);
        }

        $oEvent->setData($oSkills);
    }

    /**
     * Add Skill Languages
     *
     * @param Skills $oSkills
     * @param array  $aExistingLanguagesIds
     *
     * @return Skills
     */
    protected function _addSkillLanguage(Skills $oSkills, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oSkillLanguages = new SkillLanguages();

                $oSkillLanguages->setLanguages($oLanguage);
                $oSkillLanguages->setSkills($oSkills);

                $oSkills->addSkillLanguage($oSkillLanguages);
            }
        }

        return $oSkills;
    }
}
