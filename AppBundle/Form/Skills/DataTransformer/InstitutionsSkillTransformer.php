<?php

namespace Flendoc\AppBundle\Form\Skills\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Institutions\InstitutionSkills;
use Flendoc\AppBundle\Entity\Institutions\Institutions;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class InstitutionsSkillTransformer
 * @package Flendoc\AppBundle\Form\Skills\DataTransformer
 */
class InstitutionsSkillTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var Languages
     */
    protected $userLanguage;

    /** @var Institutions */
    protected $institution;

    /**
     * InstitutionsSkillTransformer constructor.
     *
     * @param ObjectManager $manager
     * @param Languages     $oLanguage
     * @param Institutions  $oInstitution
     */
    public function __construct(ObjectManager $manager, Languages $oLanguage, Institutions $oInstitution)
    {
        $this->manager      = $manager;
        $this->userLanguage = $oLanguage;
        $this->institution  = $oInstitution;
    }

    /**
     * Transform
     *
     * @param mixed $oPersistentCollection
     *
     * @return null
     */
    public function transform($oPersistentCollection)
    {
        if ($oPersistentCollection->isEmpty()) {
            return json_encode([]);
        }

        $aInstitutionSkills = [];
        foreach ($oPersistentCollection as $oInstitutionSkill) {
            $aInstitutionSkills[] = [
                'id'   => $oInstitutionSkill->getId(),
                'text' => $oInstitutionSkill->getDisplayName($this->userLanguage),
            ];
        }

        return json_encode($aInstitutionSkills);
    }

    /**
     * Reverse transform
     *
     * @param mixed $value
     *
     * @return ArrayCollection|null
     */
    public function reverseTransform($value)
    {

        if (empty(json_decode($value))) {
            return null;
        }

        $this->institution->setInstitutionSkills(new ArrayCollection());
        $aInstitutionSkill = new ArrayCollection();
        foreach (json_decode($value) as $oValue) {
            $aInstitutionSkill->add($this->manager->getReference('AppBundle:Skills\Skills', $oValue->id));
        }

        return $aInstitutionSkill;
    }
}
