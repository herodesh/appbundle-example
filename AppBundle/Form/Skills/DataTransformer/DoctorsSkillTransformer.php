<?php

namespace Flendoc\AppBundle\Form\Skills\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class DoctorsSkillTransformer
 * @package Flendoc\AppBundle\Form\Skills\DataTransformer
 */
class DoctorsSkillTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var Languages
     */
    protected $userLanguage;

    /**
     * DoctorsSkillTransformer constructor.
     *
     * @param ObjectManager $manager
     * @param Languages     $oLanguage
     */
    public function __construct(ObjectManager $manager, Languages $oLanguage)
    {
        $this->manager      = $manager;
        $this->userLanguage = $oLanguage;
    }

    /**
     * Transform
     *
     * @param mixed $oPersistentCollection
     *
     * @return mixed|string
     */
    public function transform($oPersistentCollection)
    {
        if ($oPersistentCollection->isEmpty()) {
            return json_encode([]);
        }

        $aDoctorSkills = [];
        foreach ($oPersistentCollection as $oDoctorSkill) {

            $aSkillResult = $this->manager->getRepository(Skills::class)
                                          ->findByLanguageAndId($this->userLanguage, [$oDoctorSkill->getId()]);

            $aDoctorSkills[] = [
                'id'   => $aSkillResult[0]['id'],
                'text' => $aSkillResult[0]['name'],
            ];
        }

        return json_encode($aDoctorSkills);
    }

    /**
     * Reverse transform
     *
     * @param mixed $value
     *
     * @return ArrayCollection|null
     */
    public function reverseTransform($value)
    {
        if (empty(json_decode($value))) {
            return null;
        }

        $aDoctorsSkill = new ArrayCollection();

        foreach (json_decode($value) as $oValue) {
            //avoid inserting duplicates into the DB
            if (!$aDoctorsSkill->contains($this->manager->getReference(Skills::class, $oValue->id))) {
                $aDoctorsSkill->add($this->manager->getReference(Skills::class, $oValue->id));
            }
        }

        return $aDoctorsSkill;
    }
}
