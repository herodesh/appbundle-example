<?php

namespace Flendoc\AppBundle\Form\Jobs\Type;

use Flendoc\AppBundle\Form\Jobs\EventListener\AddCareerLevelLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CareerLevelType
 * @package Flendoc\AppBundle\Form\Jobs\Type
 */
class CareerLevelType extends AbstractType
{
    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('careerLevelLanguages', CollectionType::class, [
                'entry_type'   => CareerLevelLanguagesType::class,
                'by_reference' => false,
            ]);

        $builder->addEventSubscriber(new AddCareerLevelLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Jobs\CareerLevels',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_career_level_form';
    }
}
