<?php

namespace Flendoc\AppBundle\Form\Jobs\Type;

use Flendoc\AppBundle\Form\Jobs\EventListener\AddJobTypeLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JobTypeType
 * @package Flendoc\AppBundle\Form\Jobs\Type
 */
class JobTypeType extends AbstractType
{
    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('jobTypeLanguages', CollectionType::class, [
                'entry_type'   => JobTypeLanguagesType::class,
                'by_reference' => false,
            ]);

        $builder->addEventSubscriber(new AddJobTypeLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Jobs\JobTypes',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_job_types_form';
    }
}
