<?php

namespace Flendoc\AppBundle\Form\Jobs\EventListener;

use Flendoc\AppBundle\Entity\Jobs\CareerLevelLanguages;
use Flendoc\AppBundle\Entity\Jobs\CareerLevels;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddCareerLevelLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Jobs\EventListener
 */
class AddCareerLevelLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddCareerLevelLanguagesSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oData                 = $oEvent->getData();

        if ($oData->getCareerLevelLanguages()->isEmpty()) {

            $oCareerLevel = new CareerLevels();
            $this->_addCareerLevelLanguage($oCareerLevel, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oData->getCareerLevelLanguages() as $oCareerLevelLanguages) {
                $aExistingLanguagesIds[] = $oCareerLevelLanguages->getLanguages()->getId();
            }
            $oCareerLevel = $this->_addCareerLevelLanguage($oData, $aExistingLanguagesIds);
        }

        $oEvent->setData($oCareerLevel);

    }

    /**
     * @param CareerLevels $oCareerLevel
     * @param array        $aExistingLanguagesIds
     *
     * @return CareerLevels
     */
    protected function _addCareerLevelLanguage(CareerLevels $oCareerLevel, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {
            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oCareerLevelLanguages = new CareerLevelLanguages();

                $oCareerLevelLanguages->setLanguages($oLanguage);
                $oCareerLevelLanguages->setCareerLevel($oCareerLevel);

                $oCareerLevel->addCareerLevelLanguage($oCareerLevelLanguages);
            }
        }

        return $oCareerLevel;
    }
}
