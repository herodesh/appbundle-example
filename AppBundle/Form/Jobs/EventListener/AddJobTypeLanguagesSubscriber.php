<?php
namespace Flendoc\AppBundle\Form\Jobs\EventListener;

use Flendoc\AppBundle\Entity\Jobs\JobTypeLanguages;
use Flendoc\AppBundle\Entity\Jobs\JobTypes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddJobTypeLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Jobs\EventListener
 */
class AddJobTypeLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddJobTypeLanguagesSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oData                 = $oEvent->getData();

        if ($oData->getJobTypeLanguages()->isEmpty()) {

            $oJobType = new JobTypes();
            $this->_addJobTypeLanguage($oJobType, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oData->getJobTypeLanguages() as $oJobTypeLanguages) {
                $aExistingLanguagesIds[] = $oJobTypeLanguages->getLanguages()->getId();
            }
            $oJobType = $this->_addJobTypeLanguage($oData, $aExistingLanguagesIds);
        }

        $oEvent->setData($oJobType);

    }

    /**
     * Add job type languages
     *
     * @param JobTypes $oJobTypes
     * @param array    $aExistingLanguagesIds
     *
     * @return JobTypes
     */
    protected function _addJobTypeLanguage(JobTypes $oJobTypes, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {
            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oJobTypeLanguages = new JobTypeLanguages();

                $oJobTypeLanguages->setLanguages($oLanguage);
                $oJobTypeLanguages->setJobType($oJobTypes);

                $oJobTypes->addJobTypeLanguage($oJobTypeLanguages);
            }
        }

        return $oJobTypes;
    }
}
