<?php

namespace Flendoc\AppBundle\Form\Languages\EventListener;

use Flendoc\AppBundle\Entity\Countries\Countries;
use Flendoc\AppBundle\Entity\Countries\CountryLanguages;
use Flendoc\AppBundle\Entity\Languages\TranslatedLanguage;
use Flendoc\AppBundle\Entity\Languages\TranslatedLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddTranslatedLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Languages\EventListener
 */
class AddTranslatedLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddTranslatedLanguagesSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {

        $aExistingLanguagesIds = [];
        $oLanguages            = $oEvent->getData();

        if (empty($oLanguages->getTranslatedLanguages()->isEmpty())) {

            $this->_addTranslatedLanguage($oLanguages, $aExistingLanguagesIds);
        } else {

            //get languages ids
            foreach ($oLanguages->getTranslatedLanguages() as $oTranslatedLanguages) {
                $aExistingLanguagesIds[] = $oTranslatedLanguages->getLanguages()->getId();
            }
            $this->_addTranslatedLanguage($oLanguages, $aExistingLanguagesIds);
        }

        $oEvent->setData($oLanguages);
    }

    /**
     * @param       $oLanguages
     * @param array $aExistingLanguagesIds
     *
     * @return mixed
     */
    protected function _addTranslatedLanguage($oLanguages, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oTranslatedLanguages = new TranslatedLanguages();

                $oTranslatedLanguages->setLanguage($oLanguage);
                $oTranslatedLanguages->setTranslatedLanguages($oTranslatedLanguages);

                $oLanguages->addTranslatedLanguage($oTranslatedLanguages);
            }
        }

        return $oLanguages;
    }
}
