<?php

namespace Flendoc\AppBundle\Form\Languages\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LanguagesType
 * @package Flendoc\AppBundle\Form\Languages\Type
 */
class LanguagesType extends AbstractType
{
    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languages = $options['languages'];

        $builder
            ->add('translatedLanguages', CollectionType::class, [
                'entry_type'   => TranslatedLanguagesType::class,
                'by_reference' => false,
            ])
            ->add('iso', TextType::class, [
                'label' => 'admin.languages.iso',
            ])
            ->add('nameIdentifier', TextType::class, [
                'label' => 'admin.languages.nameIdentifier',
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'admin.languages.active',
            ])
            ->add('isPrimary', CheckboxType::class, [
                'label' => 'admin.languages.isPrimary',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_languages';
    }
}
