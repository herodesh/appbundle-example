<?php

namespace Flendoc\AppBundle\Form\Languages\Type;

use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TranslatedLanguagesType
 * @package Flendoc\AppBundle\Form\Languages\Type
 */
class TranslatedLanguagesType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages'
            ])
            ->add('name', TextType::class, [
                'label' => 'admin.languages.name',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Languages\TranslatedLanguages',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_translated_languages';
    }
}
