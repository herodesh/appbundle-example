<?php

namespace Flendoc\AppBundle\Form\Languages\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class SingleLanguageSelectorTransformer
 * @package Flendoc\AppBundle\Form\Languages\DataTransformer
 */
class SingleLanguageSelectorTransformer implements DataTransformerInterface
{
    /** @var ObjectManager */
    protected $manager;

    /** @var string */
    protected $languageIso;

    /** @var bool */
    protected $jsonFormat;

    /**
     * SingleLanguageSelectorTransformer constructor.
     *
     * @param ObjectManager $manager
     * @param               $languageIso
     * @param bool          $jsonFormat
     */
    public function __construct(ObjectManager $manager, $languageIso, $jsonFormat = false)
    {
        $this->manager     = $manager;
        $this->languageIso = $languageIso;
        $this->jsonFormat  = $jsonFormat;
    }

    /**
     * Transforms LanguageObject to json
     *
     * @param Languages $oLanguage
     *
     * @return string
     */
    public function transform($oLanguage)
    {

        if ($this->jsonFormat) {
            if (null === $oLanguage) {
                return json_encode([]);
            }

            $aLanguage[] = [
                'id'   => $oLanguage->getId(),
                'text' => $oLanguage->getNameByIso($this->languageIso),
            ];

            return json_encode($aLanguage);
        } else {
            if (null === $oLanguage) {
                return '';
            }

            return $oLanguage->getId();
        }
    }

    /**
     * Transforms LanguageId to LanguageObject
     *
     * @param mixed $sLanguage
     *
     * @return null
     */
    public function reverseTransform($sLanguage)
    {
        if (empty(json_decode($sLanguage))) {
            return null;
        }

        $aLanguage   = json_decode($sLanguage, true);
        $sLanguageId = $aLanguage[0]['id'];

        //it's always 1 entry. $aCity[0]
        $oLanguage = $this->manager->getReference('AppBundle:Languages\Languages', $sLanguageId);

        return $oLanguage;
    }
}
