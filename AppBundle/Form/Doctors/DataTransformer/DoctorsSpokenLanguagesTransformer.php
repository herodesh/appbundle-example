<?php

namespace Flendoc\AppBundle\Form\Doctors\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class DoctorsSpokenLanguagesTransformer
 * @package Flendoc\AppBundle\Form\Doctors\DataTransformer
 */
class DoctorsSpokenLanguagesTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var Languages
     */
    protected $userLanguage;

    /**
     * DoctorsSpokenLanguagesTransformer constructor.
     *
     * @param ObjectManager $manager
     * @param Languages     $oLanguage
     */
    public function __construct(ObjectManager $manager, Languages $oLanguage)
    {
        $this->manager      = $manager;
        $this->userLanguage = $oLanguage;
    }

    /**
     * Transform
     *
     * @param mixed $oPersistentCollection
     *
     * @return null
     */
    public function transform($oPersistentCollection)
    {
        if ($oPersistentCollection->isEmpty()) {
            return json_encode([]);
        }

        $aDoctorSpokenLanguages = [];
        foreach ($oPersistentCollection as $oDoctorSpokenLanguage) {

            $aDoctorSpokenLanguages[] = [
                'id'   => $oDoctorSpokenLanguage->getId(),
                'text' => $oDoctorSpokenLanguage->getNameByIso($this->userLanguage->getIso()),
            ];
        }

        return json_encode($aDoctorSpokenLanguages);
    }

    /**
     * Reverse transform
     *
     * @param mixed $value
     *
     * @return ArrayCollection|null
     */
    public function reverseTransform($value)
    {

        if (empty(json_decode($value))) {
            return null;
        }

        $aDoctorsSpokenLanguages = new ArrayCollection();

        foreach (json_decode($value) as $oValue) {
            $aDoctorsSpokenLanguages->add($this->manager->getReference('AppBundle:Languages\Languages', $oValue->id));
        }

        return $aDoctorsSpokenLanguages;
    }
}
