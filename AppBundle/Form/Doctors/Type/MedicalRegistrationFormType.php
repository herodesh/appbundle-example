<?php

namespace Flendoc\AppBundle\Form\Doctors\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Flendoc\AppBundle\Validator\Constraints\CityBelongsToCountry;
use Flendoc\AppBundle\Validator\Constraints\EmailUsed;
use Flendoc\AppBundle\Validator\Constraints\IsNotDisposableEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Registers a medical tpe of user. For institutions we will have a different form
 *
 * Class RegistrationFormType
 * @package Flendoc\AppBundle\Form\Doctors\Type
 */
class MedicalRegistrationFormType extends AbstractType
{
    /** @var string */
    protected $locale;

    /** @var Languages */
    protected $languages;

    /** @var EntityManager */
    protected $manager;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->locale    = $options['locale'];
        $this->languages = $options['languages'];
        $this->manager   = $options['manager'];

        $aChoices = [];

        foreach ($this->languages as $language) {
            $aChoices[$language['name']] =
                $language['iso'];
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'registration.title.male'   => 'm',
                    'registration.title.female' => 'f',
                ],
                'label'   => 'registration.user.gender',
            ])
            ->add('isStudent', ChoiceType::class, [
                'label'   => 'registration.user.type.of.medical.registrant',
                'choices' => [
                    //send true if student false otherwise
                    'registration.is.medical.professional' => 0,
                    'registration.is.student'              => 1,
                ],
            ])
            ->add('academicTitle', TextType::class, [
                'label'       => 'registration.user.title',
                'attr'        => [
                    'placeholder' => 'Example: MD, PhD, Student, ...',
                ],
                //seems that this do not work you should check the annotations on entity
                'constraints' => [
                    new NotNull(['groups' => 'registration']),
                    new Length([
                        'min'        => 1,
                        'max'        => 100,
                        'maxMessage' => "validator.academic.title.size",
                        'groups'     => 'registration',
                    ]),
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'registration.first.name',
                'attr'  => [
                    'placeholder' => 'registration.first.name',
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.last.name',
                'attr'  => [
                    'placeholder' => 'registration.last.name',
                ],
            ])
            ->add('country', TextType::class, [
                'label'       => 'doctors.select.country',
                'attr'        => [
                    'style' => 'display:none',
                ],
                //seems that this do not work you should check the annotations on entity
                'constraints' => [
                    new NotNull(),
                ],
            ])
            ->add('city', TextType::class, [
                'label'       => 'doctors.select.city',
                'attr'        => [
                    'style' => 'display:none',
                ],
                //seems that this do not work you should check the annotations on entity
                'constraints' => [
                    new NotNull(),
                    new CityBelongsToCountry(),
                ],
            ])
            ->add('username', TextType::class, [
                'label'       => 'registration.username',
                'attr'        => [
                    'placeholder' => 'registration.username',
                ],
                //seems that this do not work you should check the annotations on entity
                'constraints' => [
                    new IsNotDisposableEmail(),
                    new EmailUsed(),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'first_options'  => [
                    'label' => 'registration.password',
                    'attr'  => [
                        'placeholder' => 'registration.password',
                    ],
                ],
                'second_options' => [
                    'label' => 'registration.retype.password',
                    'attr'  => [
                        'placeholder' => 'registration.retype.password',
                    ],
                ],
            ])
            ->add('defaultLanguage', ChoiceType::class, [
                'label'   => 'registration.user.default.language',
                'choices' => $aChoices,
            ])
            ->add('terms', CheckboxType::class, [
                'label' => 'registration.terms',
            ]);

        $builder->get('country')->addModelTransformer(new CountryTransformer($this->manager, $this->languages, true));
        $builder->get('city')->addModelTransformer(new CityTransformer($this->manager, $this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => 'registration',
            'locale'            => null,
            'manager'           => null,
            'languages'         => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'doctors_registration_form';
    }

}
