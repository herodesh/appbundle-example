<?php

namespace Flendoc\AppBundle\Form\Doctors\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Validator\Constraints\IsNotDisposableEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class RegistrationFormType
 * @package Flendoc\AppBundle\Form\Doctors\Type
 */
class AdministrativeRegistrationFormType extends AbstractType
{
    /** @var string */
    protected $locale;

    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->locale    = $options['locale'];
        $this->languages = $options['languages'];

        $aChoices = [];
        foreach ($this->languages as $language) {
            $aChoices[$language->getNameByIso($this->locale)] =
                $language->getIso();
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'registration.title.male'   => 'm',
                    'registration.title.female' => 'f',
                ],
                'label'   => 'registration.user.gender',
            ])
            ->add('isAdministrative', HiddenType::class, [
                'data' => true
            ])
            ->add('firstName', TextType::class, [
                'label' => 'registration.first.name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.last.name',
            ])
            ->add('country', TextType::class, [
                'label' => 'doctors.select.country',
                'attr'  => [
                    'style' => 'display:none',
                ],
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'doctors.select.city',
                'attr'  => [
                    'style' => 'display:none',
                ],
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('username', TextType::class, [
                'label' => 'registration.username',
                'constraints' => [
                    new IsNotDisposableEmail()
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'first_options'  => [
                    'label' => 'registration.password',
                ],
                'second_options' => [
                    'label' => 'registration.retype.password',
                ],
            ])
            ->add('defaultLanguage', ChoiceType::class, [
                'label'   => 'registration.user.default.language',
                'choices' => $aChoices,
            ])
            ->add('terms', CheckboxType::class, [
                'label' => 'registration.terms',
            ]);

        $builder->remove('academicTitle');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => 'registration',
            'locale'            => null,
            'languages'         => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'doctors_registration_form';
    }

}
