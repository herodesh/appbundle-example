<?php

namespace Flendoc\AppBundle\Form\Doctors\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class EditLocationType
 * @package Flendoc\AppBundle\Form\Doctors\Type
 */
class EditLocationType extends AbstractType
{
    /** @var ObjectManager */
    protected $manager;

    /** @var  Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->language = $options['language'];
        $this->manager  = $options['manager'];

        $builder
            ->add('country', TextType::class, [
                'label'       => 'doctors.select.country',
                'attr'        => [
                    'style' => 'display:none',
                ],
                'constraints' => [
                    new NotNull(),
                ],
            ])
            ->add('city', TextType::class, [
                'label'       => 'doctors.select.city',
                'attr'        => [
                    'style' => 'display:none',
                ],
                'constraints' => [
                    new NotNull(),
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit',
            ]);
        $builder->get('country')->addModelTransformer(new CountryTransformer($this->manager, $this->language, true));
        $builder->get('city')->addModelTransformer(new CityTransformer($this->manager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\DoctorProfile',
            'attr'       => ['novalidate' => 'novalidate'],
            'language'   => null,
            'manager'    => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'users_change_location';
    }
}
