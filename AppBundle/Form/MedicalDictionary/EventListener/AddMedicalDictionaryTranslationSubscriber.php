<?php

namespace Flendoc\AppBundle\Form\MedicalDictionary\EventListener;

use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryTranslations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddMedicalDictionaryTranslationSubscriber
 * @package Flendoc\AppBundle\Form\MedicalDictionary\EventListener
 */
class AddMedicalDictionaryTranslationSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddMedicalDictionaryTranslationSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        /** @var MedicalDictionary $oMedicalDictionary */
        $oMedicalDictionary = $oEvent->getData();

        if ($oMedicalDictionary->getMedicalDictionaryTranslation()->isEmpty()) {
            $this->_addMedicalDictionaryTranslation($oMedicalDictionary, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oMedicalDictionary->getMedicalDictionaryTranslation() as $oMedicalDictionaryTranslation) {
                $aExistingLanguagesIds[] = $oMedicalDictionaryTranslation->getLanguage()->getId();
            }
            $this->_addMedicalDictionaryTranslation($oMedicalDictionary, $aExistingLanguagesIds);
        }

        $oEvent->setData($oMedicalDictionary);
    }

    /**
     * Add MedicalDictionary Languages
     *
     * @param MedicalDictionary $oMedicalDictionary
     * @param array             $aExistingLanguagesIds
     *
     * @return mixed
     */
    protected function _addMedicalDictionaryTranslation(
        MedicalDictionary $oMedicalDictionary,
        array $aExistingLanguagesIds
    ) {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds) && $oLanguage->getActive()) {
                $oMedicalDictionaryTranslations = new MedicalDictionaryTranslations();

                $oMedicalDictionaryTranslations->setLanguage($oLanguage);
                $oMedicalDictionaryTranslations->setMedicalDictionary($oMedicalDictionary);

                $oMedicalDictionary->addMedicalDictionaryTranslations($oMedicalDictionaryTranslations);
            }
        }

        return $oMedicalDictionary;
    }
}
