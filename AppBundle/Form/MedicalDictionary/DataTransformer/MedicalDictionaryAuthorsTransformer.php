<?php

namespace Flendoc\AppBundle\Form\MedicalDictionary\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class MedicalDictionaryAuthorsTransformer
 * @package Flendoc\AppBundle\Form\MedicalDictionary\DataTransformer
 */
class MedicalDictionaryAuthorsTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * MedicalDictionaryAuthorsTransformer constructor.
     *
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transform for user display
     *
     * @param mixed $oPersistentCollection
     *
     * @return false|mixed|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function transform($oPersistentCollection)
    {
        if ($oPersistentCollection->isEmpty()) {
            return json_encode([]);
        }

        $aAuthor = [];
        foreach ($oPersistentCollection as $oAuthor) {

            $aAuthorsResult = $this->manager->getRepository(Doctors::class)
                                            ->findSimpleUserProfileDetails($oAuthor->getIdentifier());

            $aAuthor[] = [
                'id'         => $aAuthorsResult['id'],
                'identifier' => $aAuthorsResult['identifier'],
                'text'       => $aAuthorsResult['fullName'],
            ];
        }

        return json_encode($aAuthor);
    }

    /**
     * Reverse transform for database usage
     *
     * @param mixed $value
     *
     * @return ArrayCollection|null
     */
    public function reverseTransform($value)
    {
        if (empty(json_decode($value))) {
            return null;
        }

        $aAuthors = new ArrayCollection();

        foreach (json_decode($value) as $oValue) {
            //avoid inserting duplicates into the DB
            if (!$aAuthors->contains($this->manager->getReference(Doctors::class, $oValue->id))) {
                $aAuthors->add($this->manager->getReference(Doctors::class, $oValue->id));
            }
        }

        return $aAuthors;
    }
}
