<?php

namespace Flendoc\AppBundle\Form\MedicalDictionary\Type;

use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MedicalDictionarySocialType
 * @package Flendoc\AppBundle\Form\MedicalDictionary\Type
 */
class MedicalDictionarySocialType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isSocialAdded', CheckboxType::class, [
                'label' => 'admin.medical.dictionary.is.social.added',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => MedicalDictionary::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_medical_dictionary_social_added';
    }
}
