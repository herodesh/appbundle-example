<?php

namespace Flendoc\AppBundle\Form\MedicalDictionary\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionary;
use Flendoc\AppBundle\Form\MedicalDictionary\DataTransformer\MedicalDictionaryAuthorsTransformer;
use Flendoc\AppBundle\Form\MedicalDictionary\EventListener\AddMedicalDictionaryTranslationSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class MedicalDictionaryType
 * @package Flendoc\AppBundle\Form\MedicalDictionary\Type
 */
class MedicalDictionaryType extends AbstractType
{

    /**
     * @var Languages $languages
     */
    protected $languages;

    /** @var EntityManager */
    protected $entityManager;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages     = $options['languages'];
        $this->entityManager = $options['entityManager'];

        $builder
            ->add('medicalDictionaryTranslation', CollectionType::class, [
                'entry_type'   => MedicalDictionaryTranslationType::class,
                'by_reference' => false,
            ])
            //we add this one as a free input string as the data is being processed afterwards
            ->add('authors', TextType::class, [
                'label'              => 'admin.medical.dictionary.authors',
                'attr'               => [
                    'style' => 'display:none',
                    'class' => '_f-full-width',
                ],
                'constraints'        => [
                    new NotNull(),
                ],
                'translation_domain' => 'messages',
            ])
            ->add('sources', TextareaType::class, [
                'label' => 'admin.medical.dictionary.sources',
                'attr' => [
                    'placeholder' => 'admin.medical.dictionary.sources.placeholder'
                ]
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.medical.dictionary.is.active',
            ])
            ->add('isSocialAdded', CheckboxType::class, [
                'label' => 'admin.medical.dictionary.is.social.added',
            ])
        ;

        $builder->get('authors')->addModelTransformer(new MedicalDictionaryAuthorsTransformer($this->entityManager));
        $builder->addEventSubscriber(new AddMedicalDictionaryTranslationSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'    => MedicalDictionary::class,
                'attr'          => [
                    'novalidate' => 'novalidate',
                ],
                'languages'     => null,
                'entityManager' => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_medical_dictionary';
    }
}
