<?php

namespace Flendoc\AppBundle\Form\MedicalDictionary\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\LegalTranslations;
use Flendoc\AppBundle\Entity\MedicalDictionary\MedicalDictionaryTranslations;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MedicalDictionaryTranslationType
 * @package Flendoc\AppBundle\Form\MedicalDictionary\Type
 */
class MedicalDictionaryTranslationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('title', TextType::class, [
                'label' => 'admin.medical.dictionary.title',
                'required' => true
            ])
            ->add('definition', TextareaType::class, [
                'label' => 'admin.medical.dictionary.definition',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'admin.medical.dictionary.description',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('causes', TextareaType::class, [
                'label' => 'admin.medical.dictionary.causes',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('symptoms', TextareaType::class, [
                'label' => 'admin.medical.dictionary.symptoms',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('diagnosis', TextareaType::class, [
                'label' => 'admin.medical.dictionary.diagnosis',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('treatment', TextareaType::class, [
                'label' => 'admin.medical.dictionary.treatment',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('prognosis', TextareaType::class, [
                'label' => 'admin.medical.dictionary.prognosis',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ->add('prevention', TextareaType::class, [
                'label' => 'admin.medical.dictionary.prevention',
                'attr' => [
                    'class' => 'tinymce'
                ]
            ])
            ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MedicalDictionaryTranslations::class,
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_medical_dictionary_translations';
    }
}
