<?php

namespace Flendoc\AppBundle\Form\Institutions\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Institutions\Institutions;
use Flendoc\AppBundle\Form\Skills\DataTransformer\InstitutionsSkillTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Validator\Constraints\IsInstitutionPrimaryLanguage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstitutionType
 * @package Flendoc\AppBundle\Form\Institutions\Type
 */
class InstitutionType extends AbstractType
{

    /**
     * @var string
     */
    protected $countryIdentifier;

    /**
     * @var Languages
     */
    protected $countryLanguages;

    /**
     * @var Languages
     */
    protected $userLanguage;

    /** @var Institutions */
    protected $institution;

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->countryIdentifier = $options['countryIdentifier'];
        $this->countryLanguages  = $options['countryLanguages'];
        $this->userLanguage      = $options['userLanguage'];
        $this->manager           = $options['manager'];
        $this->institution       = $options['institution'];

        $builder
            ->add('institutionLanguages', CollectionType::class, [
                'entry_type'  => InstitutionLanguagesType::class,
                'constraints' => [
                    new IsInstitutionPrimaryLanguage(),
                ],
            ])
            ->add('institutionCountry', HiddenType::class, [
                'label' => 'admin.institutions.country',
            ])
            ->add('institutionCity', TextType::class, [
                'label' => 'admin.institutions.city',
                'attr'  => [
                    'style' => 'display:none',
                ],
            ])
            ->add('institutionSkills', TextType::class, [
                'attr' => [
                    'style' => 'display:none',
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'admin.institutions.name',
            ])
            ->add('address', TextType::class, [
                'label' => 'admin.institutions.address',
            ])
            ->add('postalCode', TextType::class, [
                'label' => 'admin.institutions.postal.code',
            ])
            ->add('phone', TextType::class, [
                'label' => 'admin.institutions.phone',
            ])
            ->add('mobile', TextType::class, [
                'label' => 'admin.institutions.mobile',
            ])
            ->add('fax', TextType::class, [
                'label' => 'admin.institutions.fax',
            ])
            ->add('email', TextType::class, [
                'label' => 'admin.institutions.email',
            ])
            ->add('website', TextType::class, [
                'label' => 'admin.institutions.website',
            ])
            ->add('isPublic', CheckboxType::class, [
                'label' => 'admin.institutions.is.public',
            ]);

        $builder->get('institutionCountry')->addModelTransformer(new CountryTransformer($this->manager));
        $builder->get('institutionCity')->addModelTransformer(new CityTransformer($this->manager, true));
        $builder->get('institutionSkills')
                ->addModelTransformer(new InstitutionsSkillTransformer($this->manager, $this->userLanguage, $this->institution));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'        => 'Flendoc\AppBundle\Entity\Institutions\Institutions',
                'attr'              => [
                    'novalidate' => 'novalidate',
                ],
                'countryIdentifier' => null,
                'countryLanguages'  => null,
                'userLanguage'      => null,
                'institution'       => null,
                'manager'           => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_add_institutions';
    }
}
