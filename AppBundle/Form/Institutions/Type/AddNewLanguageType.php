<?php

namespace Flendoc\AppBundle\Form\Institutions\Type;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Validator\Constraints\IsPrimaryResume;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AddNewLanguageType
 * @package Flendoc\AppBundle\Form\Institutions\Type
 */
class AddNewLanguageType extends AbstractType
{

    /**
     * @var UserInterface
     */
    protected $user;

    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->user     = $options['user'];
        $this->language = $options['language'];

        $builder
            ->add('languages', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Languages\Languages',
                'label'         => 'admin.institutions.add.new.language',
                'choice_label'  => function ($language) {
                    return $language->getNameByIso($this->user->getDefaultLanguage());
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l');
                },
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ]);
    }

    /**
     * @param OptionsResolver $oResolver
     */
    public function configureOptions(OptionsResolver $oResolver)
    {
        $oResolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Institutions\InstitutionLanguages',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'user'       => null,
            'language'   => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'institutions_add_new_language';
    }
}
