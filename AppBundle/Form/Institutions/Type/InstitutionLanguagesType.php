<?php

namespace Flendoc\AppBundle\Form\Institutions\Type;

use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstitutionLanguagesType
 * @package Flendoc\AppBundle\Form\Institutions\Type
 */
class InstitutionLanguagesType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('languages', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('areaOfExpertise', TextareaType::class, [
                'label' => 'admin.institutions.area.of.expertise',
            ])
            ->add('rangeOfServices', TextareaType::class, [
                'label' => 'admin.institutions.range.of.services',
            ])
            ->add('extraDetails', TextareaType::class, [
                'label' => 'admin.institutions.extra.details',
            ])
            ->add('isPrimaryDescription', CheckboxType::class, [
                'label' => 'admin.institutions.is.primary.description',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Flendoc\AppBundle\Entity\Institutions\InstitutionLanguages',
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_institution_languages';
    }
}
