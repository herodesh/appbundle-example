<?php

namespace Flendoc\AppBundle\Form\Feed\Type;

use Flendoc\AppBundle\Constants\FeedConstants;
use Flendoc\AppBundle\Document\Feed\MongoFeed;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FeedPostType
 * @package Flendoc\Appbundle\Form\Feed\Type
 */
class FeedPostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'label' => 'doctors.feeds.share.something.with.your.contacts',
                'label_attr' => [
                    'style' => 'display:none !important',
                ],
                'attr' => [
                    'class' => '_f-write-on-feed',
                    'rows'  => 1,
                ],
            ])
            ->add('privacy', ChoiceType::class, [
                'expanded' => false,
                'choices'  => [
                    'doctors.feeds.property.public'        => FeedConstants::FEED_PROPERTY_PUBLIC,
                    'doctors.feeds.property.contacts.only' => FeedConstants::FEED_PROPERTY_CONTACTS_ONLY,
                ],
                'data'     => $options['userDefaultWallSettings'], //set with user defaults
                'attr'     => [
                    'class' => '_f-post-privacy',
                ],
            ])
            ->add('originalObjectIdentifier', TextType::class, [
                'attr'       => [
                    'style' => 'display:none',
                ],
                'label_attr' => [
                    'style' => 'display:none !important',
                ],
                'data'       => (null == $options['sItemIdentifier']) ? $options['sOriginalObjectIdentifier'] :
                    $options['sItemIdentifier'],
            ])
            ->add('originalObjectDataType', HiddenType::class, [
                'data' => $options['sOriginalObjectDataType'],
            ])
            ->add('originalObjectEntityType', HiddenType::class, [
                'data' => $options['sOriginalObjectEntityType'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'doctors.feeds.post',
                'attr'  => [
                    'class' => '_f-button-blue tiny _f-condensed _f-fs-14',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'                => MongoFeed::class,
            'attr'                      => [
                'novalidate' => 'novalidate',
            ],
            'userDefaultWallSettings'   => null,
            'sItemIdentifier'           => null,
            'sOriginalObjectIdentifier' => null,
            'sOriginalObjectEntityType' => null,
            'sOriginalObjectDataType'   => null,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'doctor_feed_status_update';
    }
}
