<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\EventListener;

use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddFaqTranslationSubscriber
 * @package Flendoc\AppBundle\Form\LegalAndHelp\EventListener
 */
class AddFaqTranslationSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddFaqTranslationSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        /** @var Faq $oFaqs */
        $oFaqs = $oEvent->getData();

        if ($oFaqs->getFaqTranslations()->isEmpty()) {
            $this->_addFaqTranslation($oFaqs, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oFaqs->getFaqTranslations() as $oFaqTranslation) {
                $aExistingLanguagesIds[] = $oFaqTranslation->getLanguage()->getId();
            }
            $this->_addFaqTranslation($oFaqs, $aExistingLanguagesIds);
        }

        $oEvent->setData($oFaqs);
    }

    /**
     * Add Faqs Languages
     *
     * @param Faq   $oFaqs
     * @param array $aExistingLanguagesIds
     *
     * @return Faq
     */
    protected function _addFaqTranslation(Faq $oFaqs, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oFaqTranslations = new FaqTranslations();

                $oFaqTranslations->setLanguage($oLanguage);
                $oFaqTranslations->setFaq($oFaqs);

                $oFaqs->addFaqTranslations($oFaqTranslations);
            }
        }

        return $oFaqs;
    }
}
