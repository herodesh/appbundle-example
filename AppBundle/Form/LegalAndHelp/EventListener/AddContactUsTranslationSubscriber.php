<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\EventListener;

use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUs;
use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUsTranslations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddContactUsTranslationSubscriber
 * @package Flendoc\AppBundle\Form\LegalAndHelp\EventListener
 */
class AddContactUsTranslationSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddFaqTranslationSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        /** @var ContactUs $oContactUs */
        $oContactUs = $oEvent->getData();

        if ($oContactUs->getContactUsTranslations()->isEmpty()) {
            $this->_addContactUsTranslation($oContactUs, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oContactUs->getContactUsTranslations() as $oContactUsTranslation) {
                $aExistingLanguagesIds[] = $oContactUsTranslation->getLanguage()->getId();
            }
            $this->_addContactUsTranslation($oContactUs, $aExistingLanguagesIds);
        }

        $oEvent->setData($oContactUs);
    }

    /**
     * Add contactUs Languages
     *
     * @param ContactUs $oContatUs
     * @param array     $aExistingLanguagesIds
     *
     * @return ContactUs
     */
    protected function _addContactUsTranslation(ContactUs $oContatUs, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oContactUsTranslations = new ContactUsTranslations();

                $oContactUsTranslations->setLanguage($oLanguage);
                $oContactUsTranslations->setContactUs($oContatUs);

                $oContatUs->addContactUsTranslations($oContactUsTranslations);
            }
        }

        return $oContatUs;
    }
}
