<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\EventListener;

use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroupsTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddFaqGroupsTranslationSubscriber
 * @package Flendoc\AppBundle\Form\LegalAndHelp\EventListener
 */
class AddFaqGroupsTranslationSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddFaqTranslationSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        /** @var Faq $oFaqs */
        $oFaqGroups = $oEvent->getData();

        if ($oFaqGroups->getFaqGroupTranslations()->isEmpty()) {
            $this->_addFaqTranslation($oFaqGroups, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oFaqGroups->getFaqGroupTranslations() as $oFaqGroupsTranslation) {
                $aExistingLanguagesIds[] = $oFaqGroupsTranslation->getLanguage()->getId();
            }
            $this->_addFaqTranslation($oFaqGroups, $aExistingLanguagesIds);
        }

        $oEvent->setData($oFaqGroups);
    }

    /**
     * Add Faqs Languages
     *
     * @param FaqGroups   $oFaqGroups
     * @param array $aExistingLanguagesIds
     *
     * @return FaqGroups
     */
    protected function _addFaqTranslation(FaqGroups $oFaqGroups, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oFaqGroupsTranslations = new FaqGroupsTranslations();

                $oFaqGroupsTranslations->setLanguage($oLanguage);
                $oFaqGroupsTranslations->setFaqGroup($oFaqGroups);

                $oFaqGroups->addFaqGroupTranslations($oFaqGroupsTranslations);
            }
        }

        return $oFaqGroups;
    }
}
