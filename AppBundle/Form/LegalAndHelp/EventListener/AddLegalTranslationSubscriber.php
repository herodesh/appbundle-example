<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\EventListener;

use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\Legal;
use Flendoc\AppBundle\Entity\LegalAndHelp\LegalTranslations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddLegalTranslationSubscriber
 * @package Flendoc\AppBundle\Form\LegalAndHelp\EventListener
 */
class AddLegalTranslationSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddFaqTranslationSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        /** @var Legal $oLegals */
        $oLegals = $oEvent->getData();

        if ($oLegals->getLegalTranslations()->isEmpty()) {
            $this->_addLegalTranslation($oLegals, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oLegals->getLegalTranslations() as $oLegalTranslation) {
                $aExistingLanguagesIds[] = $oLegalTranslation->getLanguage()->getId();
            }
            $this->_addLegalTranslation($oLegals, $aExistingLanguagesIds);
        }

        $oEvent->setData($oLegals);
    }

    /**
     * Add Legals Languages
     *
     * @param Legal $oLegals
     * @param array $aExistingLanguagesIds
     *
     * @return Legal
     */
    protected function _addLegalTranslation(Legal $oLegals, array $aExistingLanguagesIds)
    {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oLegalTranslations = new LegalTranslations();

                $oLegalTranslations->setLanguage($oLanguage);
                $oLegalTranslations->setLegal($oLegals);

                $oLegals->addLegalTranslations($oLegalTranslations);
            }
        }

        return $oLegals;
    }
}
