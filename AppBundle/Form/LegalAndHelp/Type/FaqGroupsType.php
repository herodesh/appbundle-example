<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddFaqGroupsTranslationSubscriber;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddFaqTranslationSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FaqGroupType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class FaqGroupsType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('faqGroupTranslations', CollectionType::class, [
                'entry_type'   => FaqGroupsTranslationType::class,
                'by_reference' => false,
            ])
            ->add('faqGroupIdentifier', TextType::class, [
                'label' => 'admin.legal.and.help.string.identifier',
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.faq.is.active'
            ])
        ;

        $builder->addEventSubscriber(new AddFaqGroupsTranslationSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => FaqGroups::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_admin_and_legal_faq_groups';
    }
}
