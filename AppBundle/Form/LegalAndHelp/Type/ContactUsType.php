<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUs;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddContactUsTranslationSubscriber;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddFaqTranslationSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ContactUsType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class ContactUsType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('contactUsTranslations', CollectionType::class, [
                'entry_type'   => ContactUsTranslationType::class,
                'by_reference' => false,
            ])
            ->add('contactUsIdentifier', TextType::class, [
                'label' => 'admin.legal.and.help.string.identifier'
            ])
        ;

        $builder->addEventSubscriber(new AddContactUsTranslationSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => ContactUs::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_admin_and_legal_contact_us';
    }
}
