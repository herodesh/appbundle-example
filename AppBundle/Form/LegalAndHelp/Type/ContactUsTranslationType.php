<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUsTranslations;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactUsTranslationType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class ContactUsTranslationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('title', TextType::class, [
                'label' => 'admin.faq.title',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactUsTranslations::class,
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_help_and_legal_contact_us_translations';
    }
}
