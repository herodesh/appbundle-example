<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroupsTranslations;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FaqGroupsTranslationType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class FaqGroupsTranslationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('title', TextType::class, [
                'label' => 'admin.faq.group.title'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FaqGroupsTranslations::class,
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_help_and_legal_faq_groups_translations';
    }
}
