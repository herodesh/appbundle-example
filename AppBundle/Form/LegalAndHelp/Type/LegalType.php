<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\LegalAndHelp\Legal;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddLegalTranslationSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LegalType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class LegalType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('legalTranslations', CollectionType::class, [
                'entry_type'   => LegalTranslationType::class,
                'by_reference' => false,
            ])
            ->add('legalIdentifier', TextType::class, [
                'label' => 'admin.legal.and.help.string.identifier',
            ]);

        $builder->addEventSubscriber(new AddLegalTranslationSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Legal::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_admin_and_legal_legal';
    }
}
