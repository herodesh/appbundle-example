<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUs;
use Flendoc\AppBundle\Entity\LegalAndHelp\ContactUsTranslations;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddContactUsTranslationSubscriber;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddFaqTranslationSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class ContactUsType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class DoctorContactUsFormType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /** @var Doctors */
    protected $user;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        //we use it like this because the form is not tight to a concrete entity object
        $this->languages = $options['data']['languages'];
        $this->user      = $options['data']['user'];

        if (null === $this->user) {
            $builder->add('email', TextType::class, [
                'label'       => 'doctors.contact.us.your.email',
                'constraints' => [
                    new Email([
                        'message' => 'doctors.contact.us.valid.email',
                    ]),
                    new NotBlank([
                        'message' => 'doctors.contact.us.not.blank',
                    ]),
                ],
            ]);
        } else {
            $builder->add('email', HiddenType::class, [
                'label'       => 'doctors.contact.us.your.email',
                'label_attr'  => [
                    'style' => 'display:none !important',
                ],
                'data'        => $this->user->getUsername(),
                'constraints' => [
                    new Email([
                        'message' => 'doctors.contact.us.valid.email',
                    ]),
                ],
            ]);
        }

        $builder
            ->add('contactUsReasons', EntityType::class, [
                'class'         => ContactUs::class,
                'label'         => 'doctors.contact.us.reason',
                'choice_label'  => function ($contactUsTranslation) {
                    return $contactUsTranslation->getDisplayTitle($this->languages);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('cu');
                },
                'expanded'      => false,
                'placeholder'   => 'doctors.contact.us.select.reason',
                'constraints'   => [
                    new NotNull(['message' => 'doctors.contact.us.reason.needed']),
                ],
            ])
            ->add('content', TextareaType::class, [
                'label' => 'doctors.contact.us.content',
                'constraints' => [
                    new NotNull(['message' => 'doctors.contact.us.write.message'])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'doctors.contact.us.submit',
                'attr'  => [
                    'class' => 'button tiny radius _f-caps _f-condensed',
                ],
            ]);

        if (null === $this->user) {
            $builder->add('googleRecaptcha', TextType::class, [
                'attr'       => [
                    'class' => '_f-display-none',
                ],
                'label_attr' => [
                    'style' => 'display:none !important',
                ],

            ]);
        };
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
                'user'       => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_admin_and_legal_contact_us';
    }
}
