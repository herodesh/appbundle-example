<?php

namespace Flendoc\AppBundle\Form\LegalAndHelp\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\LegalAndHelp\Faq;
use Flendoc\AppBundle\Entity\LegalAndHelp\FaqGroups;
use Flendoc\AppBundle\Form\LegalAndHelp\EventListener\AddFaqTranslationSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class FaqType
 * @package Flendoc\AppBundle\Form\LegalAndHelp\Type
 */
class FaqType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /** @var Languages */
    protected $userLanguage;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages    = $options['languages'];
        $this->userLanguage = $options['userLanguage'];

        $builder
            ->add('faqTranslations', CollectionType::class, [
                'entry_type'   => FaqTranslationType::class,
                'by_reference' => false,
            ])
            ->add('faqGroups', EntityType::class, [
                'class'         => FaqGroups::class,
                'choice_label'  => function ($faqGroups) {
                    return $faqGroups->getDisplayTitle($this->userLanguage);
                },
                'placeholder' => 'admin.faq.choose.group',
                'constraints' => [
                    new NotNull()
                ],
            ])
            ->add('faqIdentifier', TextType::class, [
                'label' => 'admin.legal.and.help.string.identifier',
                'attr' => [
                    'readonly' => 'readonly'
                ]
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.legal.and.help.is.active',
            ]);

        $builder->addEventSubscriber(new AddFaqTranslationSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'   => Faq::class,
                'attr'         => [
                    'novalidate' => 'novalidate',
                ],
                'languages'    => null,
                'userLanguage' => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_admin_and_legal_faq';
    }
}
