<?php

namespace Flendoc\AppBundle\Form\Resumes\EventListener;

use Flendoc\AppBundle\Entity\Resumes\ResumeSectionLanguages;
use Flendoc\AppBundle\Entity\Resumes\ResumeSections;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddResumeSectionLanguageSubscriber
 * @package Flendoc\AppBundle\Form\Resumes\EventListener
 */
class AddResumeSectionLanguageSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddResumeSectionLanguageSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oData                 = $oEvent->getData();

        if ($oData->getResumeSectionLanguages()->isEmpty()) {

            $oResumeSection = new ResumeSections();
            $this->_addResumeSectionLanguage($oResumeSection, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oData->getResumeSectionLanguages() as $oResumeSectionLanguages) {
                $aExistingLanguagesIds[] = $oResumeSectionLanguages->getLanguages()->getId();
            }
            $oResumeSection = $this->_addResumeSectionLanguage($oData, $aExistingLanguagesIds);
        }

        $oEvent->setData($oResumeSection);

    }

    /**
     * @param ResumeSections $oResumeSection
     * @param array          $aExistingLanguagesIds
     *
     * @return ResumeSections
     */
    protected function _addResumeSectionLanguage(ResumeSections $oResumeSection, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oResumeSectionLanguages = new ResumeSectionLanguages();

                $oResumeSectionLanguages->setLanguages($oLanguage);
                $oResumeSectionLanguages->setResumeSections($oResumeSection);

                $oResumeSection->addResumeSectionLanguages($oResumeSectionLanguages);
            }
        }

        return $oResumeSection;
    }
}
