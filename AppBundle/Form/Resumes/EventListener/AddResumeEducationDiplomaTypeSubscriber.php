<?php

namespace Flendoc\AppBundle\Form\Resumes\EventListener;

use Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypeLanguages;
use Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddResumeEducationDiplomaTypeSubscriber
 * @package Flendoc\AppBundle\Form\Resumes\EventListener
 */
class AddResumeEducationDiplomaTypeSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddResumeEducationDiplomaTypeSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oData                 = $oEvent->getData();

        if ($oData->getResumeEducationDiplomaTypeLanguages()->isEmpty()) {

            $oResumeEducationDiplomaType = new ResumeEducationDiplomaTypes();
            $this->_addResumeEducationDiplomaTypeLanguage($oResumeEducationDiplomaType, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oData->getResumeEducationDiplomaTypeLanguages() as $oResumeEducationDiplomaTypeLanguage) {
                $aExistingLanguagesIds[] = $oResumeEducationDiplomaTypeLanguage->getLanguages()->getId();
            }
            $oResumeEducationDiplomaType = $this->_addResumeEducationDiplomaTypeLanguage($oData, $aExistingLanguagesIds);
        }

        $oEvent->setData($oResumeEducationDiplomaType);

    }

    /**
     * @param ResumeEducationDiplomaTypes $oResumeEducationDiplomaType
     * @param array                       $aExistingLanguagesIds
     *
     * @return mixed
     */
    protected function _addResumeEducationDiplomaTypeLanguage(ResumeEducationDiplomaTypes $oResumeEducationDiplomaType, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oResumeEducationDiplomaTypeLanguage = new ResumeEducationDiplomaTypeLanguages();

                $oResumeEducationDiplomaTypeLanguage->setLanguages($oLanguage);
                $oResumeEducationDiplomaTypeLanguage->setResumeEducationDiplomaType($oResumeEducationDiplomaType);

                $oResumeEducationDiplomaType->addResumeEducationDiplomaTypeLanguage($oResumeEducationDiplomaTypeLanguage);
            }
        }

        return $oResumeEducationDiplomaType;
    }
}
