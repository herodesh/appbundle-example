<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeEducationDiplomaTypeLanguageType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeEducationDiplomaTypeLanguageType extends AbstractType
{
    /** @var string */
    protected $userLanguage;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->userLanguage = $options['userLanguage'];

        $builder
            ->add('languages', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('name', TextType::class, [
                'label'      => 'admin.resumes.education.diploma.type.name',
                'label_attr' => ['translation_domain_language' => $this->userLanguage],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'   => 'Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypeLanguages',
            'attr'         => [
                'novalidate' => 'novalidate',
            ],
            'userLanguage' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_resume_education_diploma_type_languages';
    }
}
