<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Flendoc\AppBundle\Form\Resumes\EventListener\AddResumeEducationDiplomaTypeSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeEducationDiplomaTypeType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeEducationDiplomaTypeType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /** @var $userLanguage */
    protected $userLanguage;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $this->languages    = $options['languages'];
        $this->userLanguage = $options['userLanguage'];

        $builder
            ->add('resumeEducationDiplomaTypeLanguages', CollectionType::class, [
                'entry_type'    => ResumeEducationDiplomaTypeLanguageType::class,
                'entry_options' => [
                    'userLanguage' => $this->userLanguage->getIso(),
                ],
                'by_reference'  => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->userLanguage->getIso(),
                ],
            ]);

        $builder->addEventSubscriber(new AddResumeEducationDiplomaTypeSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'   => 'Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypes',
            'attr'         => [
                'novalidate' => 'novalidate',
            ],
            'languages'    => null,
            'userLanguage' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'admin_resume_education_diploma_type';
    }
}
