<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Resumes\ResumeForeignLanguages;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Flendoc\AppBundle\Form\Languages\DataTransformer\SingleLanguageSelectorTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeForeignLanguageType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeForeignLanguageType extends AbstractType
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->language      = $options['language'];
        $this->entityManager = $options['entityManager'];

        $builder
            ->add('language', TextType::class, [
                'label'      => 'doctors.resume.foreign.language.select.language',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
                'attr'       => [
                    'style' => 'display:none',
                ],
            ])
            ->add('knowledgeLevel', ChoiceType::class, [
                'choices'      => [
                    'doctors.resume.foreign.language.knowledge.beginner' => ResumeForeignLanguages::FOREIGN_LANGUAGE_BEGINNER_LEVEL,
                    'doctors.resume.foreign.language.knowledge.advanced' => ResumeForeignLanguages::FOREIGN_LANGUAGE_ADVANCED_LEVEL,
                    'doctors.resume.foreign.language.knowledge.fluent'   => ResumeForeignLanguages::FOREIGN_LANGUAGE_FLUENT_LEVEL,
                ],
                'label' => 'doctors.resumes.foreign.language.knowledge.level',
                'attr'         => [
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);

        $builder->get('language')
                ->addModelTransformer(new SingleLanguageSelectorTransformer($this->entityManager, $this->language->getIso(), true));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'Flendoc\AppBundle\Entity\Resumes\ResumeForeignLanguages',
            'attr'          => [
                'novalidate' => 'novalidate',
            ],
            'entityManager' => null,
            'language'      => null,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'resume_foreign_language_section';
    }
}
