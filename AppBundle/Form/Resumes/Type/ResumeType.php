<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Validator\Constraints\IsPrimaryResume;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResumeType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeType extends AbstractType
{

    /**
     * @var UserInterface
     */
    protected $user;

    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->user     = $options['user'];
        $this->language = $options['language'];

        $builder
            ->add('languages', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Languages\Languages',
                'label'         => 'doctors.resume.language',
                'choice_label'  => function ($language) {
                    return $language->getNameByIso($this->user->getDefaultLanguage());
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l');
                },
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('name', TextType::class, [
                'label'       => 'doctors.resume.name',
                'constraints' => [
                    new NotBlank(),
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('isPrimary', CheckboxType::class, [
                'label'       => 'doctors.resume.is.primary',
                'constraints' => [
                    new IsPrimaryResume(['user' => $this->user]),
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ]);
    }

    /**
     * @param OptionsResolver $oResolver
     */
    public function configureOptions(OptionsResolver $oResolver)
    {
        $oResolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Resumes\Resumes',
            'attr'       => [
                'novalidate' => 'novalidate',
                'id'         => '_f-doctors-add-cv',
            ],
            'user'       => null,
            'language'   => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'doctors_resume';
    }
}
