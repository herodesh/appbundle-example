<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeEducationType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeEducationType extends AbstractType
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var  Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];

        $builder
            ->add('city', TextType::class, [
                'label'      => 'doctors.resume.city',
                'attr'       => [
                    'style' => 'display: none',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('country', TextType::class, [
                'label'      => 'doctors.resume.country',
                'attr'       => [
                    'style' => 'display:none',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('institutionName', TextType::class, [
                'label'      => 'doctors.resume.education.institution',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('startDate', BirthdayType::class, [
                'label'      => 'doctors.resume.start.date',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
                'html5'      => false,
                'years'      => range(date('Y') - 40, date('Y')),

            ])
            ->add('endDate', BirthdayType::class, [
                'label'      => 'doctors.resume.end.date',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
                'html5'      => false,
                'years'      => range(date('Y') - 40, date('Y')),

            ])
            ->add('isCurrent', CheckboxType::class, [
                'label'      => 'doctors.resume.is.current.education',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('resumeEducationDiplomaType', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Resumes\ResumeEducationDiplomaTypes',
                'label'         => 'admin.resumes.education.diploma.type',
                'choice_label'  => function ($diplomaType) {
                    return $diplomaType->getDisplayName($this->language->getIso());
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('redt');
                },
            ])
            ->add('studyProfile', TextType::class, [
                'label'      => 'doctors.resume.study.profile',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('description', TextareaType::class, [
                'label'      => 'doctors.resume.education.description',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);

        $builder->get('country')
                ->addModelTransformer(new CountryTransformer($this->entityManager, $this->language, true));
        $builder->get('city')->addModelTransformer(new CityTransformer($this->entityManager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'Flendoc\AppBundle\Entity\Resumes\ResumeEducation',
            'attr'          => [
                'novalidate' => 'novalidate',
            ],
            'language'      => null,
            'entityManager' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'resume_education_data';
    }
}
