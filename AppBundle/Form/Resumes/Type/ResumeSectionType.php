<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Resumes\EventListener\AddResumeSectionLanguageSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeSectionType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeSectionType extends AbstractType
{
    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languages = $options['languages'];

        $builder
            ->add('resumeSectionLanguages', CollectionType::class, [
                'entry_type'   => ResumeSectionLanguageType::class,
                'by_reference' => false,
            ])
            ->add('isSecondary', CheckboxType::class, [
                'label' => 'admin.resumes.sections.is.secondary',
            ]);

        $builder->addEventSubscriber(new AddResumeSectionLanguageSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Resumes\ResumeSections',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages' => null
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'admin_resume_sections';
    }
}
