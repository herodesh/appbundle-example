<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Resumes\ResumeOtherInformation;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeOtherInformationType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeOtherInformationType extends AbstractType
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var  Languages */
    protected $language;
    /** @var null */
    protected $formType;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];
        $this->formType      = $options['formType'];

        $builder
            ->add('otherInformationType', TextType::class, [
                'attr' => ['style' => 'display:none'],
                'data' => $this->formType,
            ])
            ->add('startDate', BirthdayType::class, [
                'label'      => 'doctors.resume.start.date',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
                'html5'      => false,
                'years'      => range(date('Y') - 20, date('Y')),

            ])
            ->add('endDate', BirthdayType::class, [
                'label'      => 'doctors.resume.end.date',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
                'html5'      => false,
                'years'      => range(date('Y') - 20, date('Y')),

            ])
            ->add('name', TextType::class, [
                'label'      => 'doctors.resume.other.information.name',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('description', TextareaType::class, [
                'label'      => 'doctors.resume.other.information.description',
                'attr'       => [
                    'autocomplete' => 'off',
                    'html5'        => false,
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'Flendoc\AppBundle\Entity\Resumes\ResumeOtherInformation',
            'attr'          => [
                'novalidate' => 'novalidate',
            ],
            'language'      => null,
            'entityManager' => null,
            'formType'      => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'resume_other_information_data';
    }
}
