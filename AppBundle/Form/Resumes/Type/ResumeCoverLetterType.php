<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Validator\Constraints\IsPrimaryResume;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResumeCoverLetterType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeCoverLetterType extends AbstractType
{

    /**
     * @var UserInterface
     */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->language = $options['language'];

        $builder
            ->add('language', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Languages\Languages',
                'label'         => 'doctors.resume.cover.letter.language',
                'choice_label'  => function ($language) {
                    return $language->getNameByIso($this->language);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l');
                },
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('title', TextType::class, [
                'label'       => 'doctors.resume.cover.letters.title',
                'constraints' => [
                    new NotBlank(),
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('description', TextareaType::class, [
                'label'       => 'doctors.resume.cover.letters.description',
                'constraints' => [
                    new NotBlank(),
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ]);
    }

    /**
     * @param OptionsResolver $oResolver
     */
    public function configureOptions(OptionsResolver $oResolver)
    {
        $oResolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Resumes\CoverLetters',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'language'       => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'doctors_resume_cover_letters';
    }
}
