<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeHeaderType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeHeaderType extends AbstractType
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var  Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];

        $builder
            ->add('city', TextType::class, [
                'label'      => 'doctors.resume.city',
                'attr'       => [
                    'style' => 'display: none',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('country', TextType::class, [
                'label'      => 'doctors.resume.country',
                'attr'       => [
                    'style' => 'display:none',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('firstName', TextType::class, [
                'label'      => 'doctors.resume.first.name',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('lastName', TextType::class, [
                'label'      => 'doctors.resume.last.name',
                'attr'       => [
                    'autocomplete' => 'off',
                ],
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('birthDate', BirthdayType::class, [
                'label'       => 'doctors.resume.birth.date',
                'label_attr'  => ['translation_domain_language' => $this->language->getIso()],
                'html5'       => false,
                'placeholder' => [
                    'year'  => 'site.date.year',
                    'month' => 'site.date.month',
                    'day'   => 'site.date.day',
                ],
            ])
            ->add('hideCurrentJob', CheckboxType::class, [
                'label'      => 'doctors.resume.hide.current.job',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);

        $builder->get('country')
                ->addModelTransformer(new CountryTransformer($this->entityManager, $this->language, true));
        $builder->get('city')->addModelTransformer(new CityTransformer($this->entityManager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'                  => 'Flendoc\AppBundle\Entity\Resumes\ResumeHeader',
            'attr'                        => [
                'novalidate' => 'novalidate',
            ],
            'language'                    => null,
            'entityManager'               => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'resume_header_data';
    }
}
