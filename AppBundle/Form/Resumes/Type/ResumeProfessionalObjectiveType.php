<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeProfessionalObjectiveType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeProfessionalObjectiveType extends AbstractType
{
    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->language = $options['language'];

        $builder
            ->add('description', TextareaType::class, [
                'label'      => 'doctors.resume.professional.objective',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Resumes\ResumeProfessionalObjective',
            'attr'       => ['novalidate' => 'novalidate'],
            'language'   => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'resume_professional_objective_section';
    }
}
