<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Skills\DataTransformer\DoctorsSkillTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeSkillsType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeSkillsType extends AbstractType
{

    /** @var EntityManager */
    protected $entityManager;

    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];

        $builder
            ->add('resumeSkillsList', TextType::class, [
                'label' => 'doctors.resume.skills',
                'attr'  => [
                    'style' => 'display:none',
                    'class' => '_f-full-width',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class' => 'button tiny radius _f-caps _f-condensed',
                ],
            ]);

        $builder->get('resumeSkillsList')
                ->addModelTransformer(new DoctorsSkillTransformer($this->entityManager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'    => 'Flendoc\AppBundle\Entity\Resumes\ResumeSkills',
            'attr'          => ['novalidate' => 'novalidate'],
            'language'      => null,
            'entityManager' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'edit_doctor_resume_skills_section';
    }
}
