<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ResumeContactDetailsType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumeContactDetailsType extends AbstractType
{
    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->language = $options['language'];

        $builder
            ->add('emailAddress', TextType::class, [
                'label'      => 'doctors.resume.email.address',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('contactPhone', TextType::class, [
                'label'      => 'doctors.resume.phone',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('contactAddress', TextType::class, [
                'label'      => 'doctors.resume.address',
                'label_attr' => ['translation_domain_language' => $this->language->getIso()],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Resumes\ResumeContactDetails',
            'attr'       => ['novalidate' => 'novalidate'],
            'language'   => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'resume_contact_details_section';
    }
}
