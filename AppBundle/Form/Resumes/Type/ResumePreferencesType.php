<?php

namespace Flendoc\AppBundle\Form\Resumes\Type;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Skills\DataTransformer\DoctorsSkillTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResumePreferencesType
 * @package Flendoc\AppBundle\Form\Resumes\Type
 */
class ResumePreferencesType extends AbstractType
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var  Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];

        $builder
            ->add('careerLevel', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Jobs\CareerLevels',
                'label'         => 'doctors.resume.career.level',
                'choice_label'  => function ($careerLevel) {
                    return $careerLevel->getDisplayName($this->language);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('cl');
                },
                'expanded'      => true,
                'constraints' => [
                    new NotBlank(['message' => 'doctors.resume.select.career.level'])
                ]
            ])
            ->add('jobTypes', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Jobs\JobTypes',
                'label'         => 'doctors.resume.job.types',
                'choice_label'  => function ($jobTypes) {
                    return $jobTypes->getDisplayName($this->language);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('jt');
                },
                'multiple'      => true,
                'expanded'      => true,
            ])
            ->add('skills', TextType::class, [
                'label' => 'doctors.resume.skills',
                'attr'  => [
                    'style' => 'display:none',
                    'class' => '_f-full-width',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
                'attr'  => [
                    'class'                       => 'button tiny radius _f-caps _f-condensed',
                    'translation_domain_language' => $this->language->getIso(),
                ],
            ]);

        $builder->get('skills')
                ->addModelTransformer(new DoctorsSkillTransformer($this->entityManager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'Flendoc\AppBundle\Entity\Resumes\ResumePreferences',
            'attr'          => [
                'novalidate' => 'novalidate',
            ],
            'language'      => null,
            'entityManager' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'resume_preferences_data';
    }
}
