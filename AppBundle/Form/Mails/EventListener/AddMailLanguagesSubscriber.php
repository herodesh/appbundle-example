<?php

namespace Flendoc\AppBundle\Form\Mails\EventListener;

use Flendoc\AppBundle\Entity\Mails\MailLanguages;
use Flendoc\AppBundle\Entity\Mails\Mails;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddMailLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\EventListener
 */
class AddMailLanguagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddCountryLanguagesSubscriber constructor.
     *
     * @param               $languages
     */
    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oMails                = $oEvent->getData();

        if ($oMails->getMailLanguages()->isEmpty()) {

            $this->_addMailLanguage($oMails, $aExistingLanguagesIds);
        } else {

            //get languages ids
            foreach ($oMails->getMailLanguages() as $oMailLanguages) {
                $aExistingLanguagesIds[] = $oMailLanguages->getLanguages()->getId();
            }
            $this->_addMailLanguage($oMails, $aExistingLanguagesIds);
        }

        $oEvent->setData($oMails);
    }

    /**
     * Add Mails Languages
     *
     * @param Mails $oMail
     * @param array $aExistingLanguagesIds
     *
     * @return Mails
     */
    protected function _addMailLanguage(Mails $oMail, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oMailLanguages = new MailLanguages();

                $oMailLanguages->setLanguages($oLanguage);
                $oMailLanguages->setMail($oMail);
                $oMail->addMailLanguage($oMailLanguages);
            }
        }

        return $oMail;
    }
}
