<?php

namespace Flendoc\AppBundle\Form\Mails\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MailLanguageType
 * @package Flendoc\AppBundle\Form\Mails\Type
 */
class MailLanguageType extends AbstractType
{
    /**
     * @string
     */
    protected $languages;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shortDescription', TextType::class, [
                'label' => 'admin.emails.short.description'
            ])
            ->add('subject', TextType::class, [
                'label' => 'admin.emails.subject',
            ])
            ->add('content', TextareaType::class, [
                'label' => 'admin.emails.content',
                'attr'  => [
                    'class' => 'tinymce',
                ],
            ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Mails\MailLanguages',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * Block prefix
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_emails_languages';
    }
}
