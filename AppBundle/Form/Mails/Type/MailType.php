<?php

namespace Flendoc\AppBundle\Form\Mails\Type;

use Flendoc\AppBundle\Form\Mails\EventListener\AddMailLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MailType
 * @package Flendoc\AppBundle\Form\Mails\Type
 */
class MailType extends AbstractType
{

    /**
     * @string
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('mailLanguages', CollectionType::class, [
                'entry_type'   => MailLanguageType::class,
                'by_reference' => false,
            ])
            ->add('identifier', TextType::class, [
                'label' => 'admin.emails.identifier',
            ])
            ->add('variables', TextareaType::class, [
                'label' => 'admin.emails.variables',
            ])
            ->add('subjectVariables', TextareaType::class, [
                'label' => 'admin.emails.subject.variables',
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.emails.is.active',
            ])
            ->add('isAdministrative', CheckboxType::class, [
                'label' => 'admin.emails.is.administrative',
            ])
            ->add('isAccount', CheckboxType::class, [
                'label' => 'admin.emails.is.account',
            ])
            ->add('isExternal', CheckboxType::class, [
                'label' => 'admin.emails.is.external',
            ])
        ;

        $builder->addEventSubscriber(new AddMailLanguagesSubscriber($this->languages));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Mails\Mails',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages'  => null,
        ]);
    }

    /**
     * Block prefix
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_emails';
    }
}
