<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\RegistrationDocuments\EventListener\AddRegistrationDocumentsLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationDocumentsType
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\Type
 */
class RegistrationDocumentsType extends AbstractType
{
    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languages = $options['languages'];

        $builder
            ->add('registrationDocumentTypeLanguages', CollectionType::class, [
                'entry_type'   => RegistrationDocumentsLanguagesType::class,
                'by_reference' => false,
            ])
            ->add('isIdentificationDocument', CheckboxType::class, [
                'label' => 'admin.doctors.registration.document.is.identification',
            ]);

        $builder->addEventSubscriber(new AddRegistrationDocumentsLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes',
            'attr'       => ['novalidate' => 'novalidate'],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration_documents_types';
    }
}
