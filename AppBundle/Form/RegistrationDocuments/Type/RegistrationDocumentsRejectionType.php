<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\RegistrationDocuments\EventListener\AddRegistrationDocumentsRejectionLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationDocumentsRejectionType
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\Type
 */
class RegistrationDocumentsRejectionType extends AbstractType
{
    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var languages */
        $this->languages = $options['languages'];

        $builder->add('registrationDocumentsRejectionLanguages', CollectionType::class, [
            'entry_type'   => RegistrationDocumentsRejectionLanguagesType::class,
            'by_reference' => false,
        ]);

        $builder->addEventSubscriber(new AddRegistrationDocumentsRejectionLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection',
            'attr'       => ['novalidate' => 'novalidate'],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration_documents_rejection_type';
    }
}
