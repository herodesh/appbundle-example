<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationDocumentsRejectionLanguagesType
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\Type
 */
class RegistrationDocumentsRejectionLanguagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, [
                'label' => 'admin.doctors.registration.document.rejection.description',
            ])
            ->add('userNotificationMessage', TextareaType::class, [
                'label' => 'admin.doctors.registration.document.rejection.email.notification',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejectionLanguages',
            'attr'       => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration_documents_rejection_languages_types';
    }
}
