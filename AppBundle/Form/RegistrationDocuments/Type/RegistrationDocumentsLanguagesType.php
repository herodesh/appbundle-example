<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationDocumentsType
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\Type
 */
class RegistrationDocumentsLanguagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'admin.doctors.registration.document.name',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypeLanguages',
            'attr'       => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration_documents_languages_types';
    }
}
