<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\Type;

use Doctrine\ORM\EntityRepository;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocuments;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class RegistrationDocumentsProcessingType
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\Type
 */
class RegistrationDocumentsProcessingType extends AbstractType
{

    protected $userLanguage;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->userLanguage = $options['userLanguage'];

        $builder
            ->add('id', HiddenType::class)
            ->add('doctors', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Doctors\Doctors',
            ])
            ->add('documentType', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes',
                'label'         => 'admin.doctors.registration.document.types',
                'choice_label'  => function ($registrationDocumentTypes) {
                    return $registrationDocumentTypes->getDisplayName($this->userLanguage);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('rdt');
                },
                'placeholder'   => 'admin.doctors.registration.select.document.type',
            ])
            ->add('rejectionReason', EntityType::class, [
                'class'         => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection',
                'label'         => 'admin.doctors.registration.document.rejection.reason',
                'attr'          => ['disabled' => 'disabled'],
                'choice_label'  => function ($registrationDocumentRejectionReasons) {
                    return $registrationDocumentRejectionReasons->getDisplayName($this->userLanguage);
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('rdr');
                },
                'placeholder'   => 'admin.doctors.registration.select.rejection.reason',
            ])
            ->add('adminNotes', TextareaType::class, [
                'label' => 'admin.doctors.registration.admin.notes',
            ])
            ->add('isAccepted', CheckboxType::class, [
                'label' => 'admin.doctors.registration.document.accepted',
                'attr'  => ['onclick' => 'singleCheck(this)'],
            ])
            ->add('isRejected', CheckboxType::class, [
                'label' => 'admin.doctors.registration.document.rejected',
                'attr'  => ['onclick' => 'rejectedCheck(this)'],
            ])
            ->add(RegistrationDocuments::REGISTRATION_DOCUMENT_SAVE, SubmitType::class, [
                'label' => 'admin.doctors.registration.save.and.continue',
                'attr'  => [
                    'class' => 'button tiny radius _f-m-b-10',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'   => 'Flendoc\AppBundle\Entity\Doctors\RegistrationDocuments',
            'attr'         => [
                'novalidate' => 'novalidate',
            ],
            'userLanguage' => null,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'admin_registration_documents_processing';
    }
}
