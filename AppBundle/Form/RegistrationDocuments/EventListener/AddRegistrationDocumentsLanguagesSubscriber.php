<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\EventListener;

use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypeLanguages;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentTypes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddRegistrationDocumentsLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\RegistrationDocuments\EventListener
 */
class AddRegistrationDocumentsLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddCountryLanguagesSubscriber constructor.
     *
     * @param array $languages
     */
    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds      = [];
        $oRegistrationDocumentTypes = $oEvent->getData();

        if ($oRegistrationDocumentTypes->getRegistrationDocumentTypeLanguages()->isEmpty()) {

            $this->_addRegistrationDocumentTypesLanguage($oRegistrationDocumentTypes, $aExistingLanguagesIds);
        } else {

            //get languages ids
            foreach ($oRegistrationDocumentTypes->getRegistrationDocumentTypeLanguages() as $oRegistrationDocumentTypeLanguages) {
                $aExistingLanguagesIds[] = $oRegistrationDocumentTypeLanguages->getLanguages()->getId();
            }
            $this->_addRegistrationDocumentTypesLanguage($oRegistrationDocumentTypes, $aExistingLanguagesIds);
        }

        $oEvent->setData($oRegistrationDocumentTypes);
    }

    /**
     * @param RegistrationDocumentTypes $oRegistrationDocumentTypes
     * @param array                     $aExistingLanguagesIds
     *
     * @return RegistrationDocumentTypes
     */
    protected function _addRegistrationDocumentTypesLanguage(
        RegistrationDocumentTypes $oRegistrationDocumentTypes,
        array $aExistingLanguagesIds
    ) {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oRegistrationDocumentTypeLanguages = new RegistrationDocumentTypeLanguages();

                $oRegistrationDocumentTypeLanguages->setLanguages($oLanguage);
                $oRegistrationDocumentTypeLanguages->setRegistrationDocumentTypes($oRegistrationDocumentTypes);
                $oRegistrationDocumentTypes->addRegistrationDocumentTypeLanguages($oRegistrationDocumentTypeLanguages);
            }
        }

        return $oRegistrationDocumentTypes;
    }
}
