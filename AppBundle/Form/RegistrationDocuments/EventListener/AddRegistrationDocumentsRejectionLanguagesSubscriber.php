<?php

namespace Flendoc\AppBundle\Form\RegistrationDocuments\EventListener;

use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejection;
use Flendoc\AppBundle\Entity\Doctors\RegistrationDocumentsRejectionLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddRegistrationDocumentsRejectionLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Users\EventListener
 */
class AddRegistrationDocumentsRejectionLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddCountryLanguagesSubscriber constructor.
     *
     * @param               $languages
     */
    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds          = [];
        $oRegistrationDocumentRejection = $oEvent->getData();

        if ($oRegistrationDocumentRejection->getRegistrationDocumentsRejectionLanguages()->isEmpty()) {

            $this->_addRegistrationDocumentRejectionLanguage($oRegistrationDocumentRejection, $aExistingLanguagesIds);
        } else {

            //get languages ids
            foreach ($oRegistrationDocumentRejection->getRegistrationDocumentsRejectionLanguages() as $oRegistrationDocumentRejectionLanguages) {
                $aExistingLanguagesIds[] = $oRegistrationDocumentRejectionLanguages->getLanguages()->getId();
            }
            $this->_addRegistrationDocumentRejectionLanguage($oRegistrationDocumentRejection, $aExistingLanguagesIds);
        }

        $oEvent->setData($oRegistrationDocumentRejection);
    }

    /**
     * @param RegistrationDocumentsRejection $oRegistrationDocumentRejection
     * @param array                          $aExistingLanguagesIds
     *
     * @return RegistrationDocumentsRejection
     */
    protected function _addRegistrationDocumentRejectionLanguage(
        RegistrationDocumentsRejection $oRegistrationDocumentRejection,
        array $aExistingLanguagesIds
    ) {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oRegistrationDocumentRejectionLanguages = new RegistrationDocumentsRejectionLanguages();

                $oRegistrationDocumentRejectionLanguages->setLanguages($oLanguage);
                $oRegistrationDocumentRejectionLanguages->setRegistrationDocumentsRejection($oRegistrationDocumentRejection);
                $oRegistrationDocumentRejection->addRegistrationDocumentsRejectionLanguage($oRegistrationDocumentRejectionLanguages);
            }
        }

        return $oRegistrationDocumentRejection;
    }
}
