<?php

namespace Flendoc\DoctorsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Registration form. Step one. See on landing page ('/')
 *
 * Class ForgotPasswordType
 * @package Flendoc\DoctorsBundle\Form\Type
 */
class ForgotPasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                ],
                'label'       => 'site.email',
            ])
            ->add('user_password', PasswordType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
                'label'       => 'site.password',
            ])
            ->add('retype_password', PasswordType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
                'label'       => 'site.retype_password',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'forgot_password_form';
    }

}
