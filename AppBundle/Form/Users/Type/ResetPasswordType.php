<?php
namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResetPasswordType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class ResetPasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type'            => PasswordType::class,
                'first_options'   => [
                    'label' => 'registration.password',
                ],
                'second_options'  => [
                    'label' => 'registration.retype.password',
                ],
                'constraints'     => [
                    new NotBlank(),
                ],
                'invalid_message' => 'Password does not match',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => ['Default', 'form_validation_only'],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'reset_password';
    }
}
