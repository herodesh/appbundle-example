<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Validator\Constraints\EmailUsed;
use Flendoc\AppBundle\Validator\Constraints\IsNotDisposableEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AdminRegistrationFormType
 * @package Flendoc\AppBundle\Form\Doctors\Type
 */
class AdminRegistrationFormType extends AbstractType
{
    /** @var string */
    protected $locale;

    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->locale    = $options['locale'];
        $this->languages = $options['languages'];

        $aChoices = [];

        foreach ($this->languages as $language) {
            $aChoices[$language['name']] =
                $language['iso'];
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'registration.title.male'   => 'm',
                    'registration.title.female' => 'f',
                ],
                'label'   => 'registration.user.gender',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'registration.first.name',
                'constraints'     => [
                    new NotBlank(),
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.last.name',
                'constraints'     => [
                    new NotBlank(),
                ],
            ])
            ->add('username', TextType::class, [
                'label' => 'registration.username',
                'constraints'     => [
                    new NotBlank(),
                    new Email(),
                    new IsNotDisposableEmail()
                ],
            ])
            ->add('defaultLanguage', ChoiceType::class, [
                'label'   => 'registration.user.default.language',
                'choices' => $aChoices,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'locale'            => null,
            'languages'         => null,
        ]);
    }
}
