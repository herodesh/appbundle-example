<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Users\EventListener\AddUserLockReasonsLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserLockReasonsType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class UserLockReasonsType extends AbstractType
{
    /** @var Languages */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var languages */
        $this->languages = $options['languages'];

        $builder
            ->add('stringIdentifier', TextType::class, [
                'label' => 'admin.doctors.lock.reasons.string.identifier',
                'attr' => [
                    'readonly' => 'readonly'
                ]
            ])
            ->add('userLockReasonsLanguages', CollectionType::class, [
                'entry_type'   => UserLockReasonsLanguagesType::class,
                'by_reference' => false,
            ]);

        $builder->addEventSubscriber(new AddUserLockReasonsLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\UserLockReasons',
            'attr'       => ['novalidate' => 'novalidate'],
            'languages'  => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user_lock_reasons_type';
    }
}
