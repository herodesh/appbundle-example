<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Registration confirmation resend
 */
class RegistrationConfirmationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'registration.username',
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration_resend_confirmation';
    }
}
