<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class RequestResetPasswordType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class RequestResetPasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label'       => 'registration.provide.email',
                'constraints' => [
                    new NotNull(['message' => 'registration.field.not.blank']),
                    new Email(['message' => 'registration.email.is.not.valid']),
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'resetting.request.submit',
                'attr'  => [
                    'class' => '_f-button-orange _f-condensed _f-caps',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr'              => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'doctors_request_reset_password';
    }
}
