<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UpdateUserLanguageType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class UpdateUserLanguageType extends AbstractType
{

    /** @var EntityManager */
    protected $entityManager;

    /** @var Languages */
    protected $languages;

    /** @var string */
    protected $locale;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->locale        = $options['locale'];
        $this->languages     = $options['languages'];
        $this->entityManager = $options['entityManager'];

        $aChoices = [];

        foreach ($this->languages as $language) {
            $aChoices[$language['name']] =
                $language['iso'];
        }

        $builder
            ->add('defaultLanguage', ChoiceType::class, [
                'label'   => 'registration.user.default.language',
                'choices' => $aChoices,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'doctors.profile.submit',
                'attr'  => [
                    'class' => 'button tiny radius _f-caps _f-condensed',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Doctors::class,
            'attr'          => ['novalidate' => 'novalidate'],
            'languages'     => null,
            'entityManager' => null,
            'locale'        => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_edit_user_language';
    }
}
