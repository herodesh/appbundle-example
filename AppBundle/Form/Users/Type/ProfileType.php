<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Entity\Doctors\DoctorProfile;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Form\Skills\DataTransformer\DoctorsSkillTransformer;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Flendoc\AppBundle\Form\Cities\DataTransformer\CityTransformer;
use Flendoc\AppBundle\Form\Countries\DataTransformer\CountryTransformer;
use Flendoc\AppBundle\Form\Doctors\DataTransformer\DoctorsSpokenLanguagesTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class ProfileType
 * @package Flendoc\DoctorsBundle\Form\Type
 */
class ProfileType extends AbstractType
{

    /** @var EntityManager */
    protected $entityManager;

    /** @var Languages */
    protected $language;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->entityManager = $options['entityManager'];
        $this->language      = $options['language'];

        $builder
            ->add('doctor', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Doctors\Doctors',
            ])
            ->add('aboutMe', TextareaType::class, [
                'label' => 'doctors.profile.about.me',
                'attr'  => [
                    'class' => 'tinymce',
                ],
            ])
            ->add('country', TextType::class, [
                'label' => 'doctors.profile.country',
                'attr' => [
                    'style' => 'display:none'
                ],
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'doctors.profile.city',
                'attr' => [
                    'style' => 'display:none'
                ],
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('doctorSkills', TextType::class, [
                'label' => 'doctors.profile.skills',
                'attr' => [
                    'style' => 'display:none',
                    'class' => '_f-full-width'
                ]
            ])
            ->add('doctorSpokenLanguages', TextType::class, [
                'label' => 'doctors.profile.spoken.languages',
                'attr' => [
                    'style' => 'display:none',
                ]
            ])
            /*->add('birthday', DateType::class, [
                'label'  => 'doctors.profile.birth.date',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])*/
            ->add('phone', TextType::class, [
                'label' => 'doctors.profile.phone',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('website', TextType::class, [
                'label' => 'doctors.profile.homepage',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'doctors.profile.submit',
                'attr' => [
                    'class' => 'button tiny radius _f-caps _f-condensed'
                ]
            ]);

        $builder->get('country')
                ->addModelTransformer(new CountryTransformer($this->entityManager, $this->language, true));
        $builder->get('city')->addModelTransformer(new CityTransformer($this->entityManager, $this->language));
        $builder->get('doctorSkills')
                ->addModelTransformer(new DoctorsSkillTransformer($this->entityManager, $this->language));
        $builder->get('doctorSpokenLanguages')
                ->addModelTransformer(new DoctorsSpokenLanguagesTransformer($this->entityManager, $this->language));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => DoctorProfile::class,
            'attr'          => ['novalidate' => 'novalidate'],
            'language'      => null,
            'entityManager' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'edit_doctor_profile';
    }
}
