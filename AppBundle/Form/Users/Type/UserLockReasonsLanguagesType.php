<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationDocumentsType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class UserLockReasonsLanguagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'admin.doctors.lock.reasons.name',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'admin.doctors.lock.reasons.description',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Doctors\UserLockReasonsLanguages',
            'attr'       => ['novalidate' => 'novalidate'],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user_lock_reasons_languages_type';
    }
}
