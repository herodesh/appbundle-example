<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Required;

/**
 * Class ChangePasswordType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class ChangePasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPlainPassword', PasswordType::class, [
                'constraints'     => [
                    new UserPassword(),
                    new Required(),
                ],
                'label' => 'resetting.change.old.password',
                'mapped'          => false,
                'invalid_message' => 'Password does not match',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'            => PasswordType::class,
                'first_options'   => [
                    'label' => 'registration.password',
                ],
                'second_options'  => [
                    'label' => 'registration.retype.password',
                ],
                'invalid_message' => 'Password does not match',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'change_password';
    }
}
