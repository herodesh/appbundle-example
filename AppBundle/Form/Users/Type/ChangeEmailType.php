<?php

namespace Flendoc\AppBundle\Form\Users\Type;

use Flendoc\AppBundle\Validator\Constraints\IsNotDisposableEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Flendoc\AppBundle\Validator\Constraints\EmailUsed;

/**
 * Class ChangeEmailType
 * @package Flendoc\AppBundle\Form\Users\Type
 */
class ChangeEmailType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints'     => [
                    new NotBlank(),
                    new Email(),
                    new EmailUsed(),
                    new IsNotDisposableEmail()
                ],
                'label'  => 'resetting.change.email'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'change_email';
    }
}
