<?php

namespace Flendoc\AppBundle\Form\Users\EventListener;

use Flendoc\AppBundle\Entity\Doctors\UserLockReasons;
use Flendoc\AppBundle\Entity\Doctors\UserLockReasonsLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddUserLockReasonsLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Users\EventListener
 */
class AddUserLockReasonsLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddUserLockReasonsLanguagesSubscriber constructor.
     *
     * @param array $languages
     */
    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oUserLockReasons      = $oEvent->getData();

        if ($oUserLockReasons->getUserLockReasonsLanguages()->isEmpty()) {
            $this->_addUserLockReasonsLanguage($oUserLockReasons, $aExistingLanguagesIds);
        } else {

            //get languages ids
            foreach ($oUserLockReasons->getUserLockReasonsLanguages() as $oUserLockReasonsLanguages) {
                $aExistingLanguagesIds[] = $oUserLockReasonsLanguages->getLanguages()->getId();
            }
            $this->_addUserLockReasonsLanguage($oUserLockReasons, $aExistingLanguagesIds);
        }

        $oEvent->setData($oUserLockReasons);
    }

    /**
     * @param UserLockReasons $oUserLockReasons
     * @param array           $aExistingLanguagesIds
     *
     * @return UserLockReasons
     */
    protected function _addUserLockReasonsLanguage(
        UserLockReasons $oUserLockReasons,
        array $aExistingLanguagesIds
    ) {
        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oUserLockReasonsLanguages = new UserLockReasonsLanguages();

                $oUserLockReasonsLanguages->setLanguages($oLanguage);
                $oUserLockReasonsLanguages->setUserLockReasons($oUserLockReasons);
                $oUserLockReasons->addUserLockReasonsLanguage($oUserLockReasonsLanguages);
            }
        }

        return $oUserLockReasons;
    }
}
