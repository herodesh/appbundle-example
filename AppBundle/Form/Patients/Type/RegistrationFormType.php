<?php
namespace Flendoc\AppBundle\Form\Patients\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Registration form. Step one. See on landing page
 */
class RegistrationFormType extends AbstractType
{

    // !!!!!!!!!!!! This might not be used @TODO check if everything is fine

    //http://stackoverflow.com/questions/25853472/in-symfony-2-how-do-i-handle-routing-for-multiple-domains

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'registration.title.male'   => 'm',
                    'registration.title.female' => 'f',
                ],
                'label'   => 'registration.user.gender',
            ])
            ->add('academicTitle', ChoiceType::class, [
                'choices' => [
                    'site.title.dr'         => 'dr',
                    'site.title.assist'     => 'assist',
                    'site.title.assoc_prof' => 'assoc_prof',
                    'site.title.prof'       => 'prof',
                ],
                'label'   => 'registration.user.title',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'registration.first.name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'registration.last.name',
            ])
            ->add('username', TextType::class, [
                'label' => 'registration.username',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'first_options'  => [
                    'label' => 'registration.password',
                ],
                'second_options' => [
                    'label' => 'registration.retype.password',
                ],
            ])
            ->add('defaultLanguage', EntityType::class, [
                'label'        => 'registration.user.default.language',
                'class'        => 'Flendoc\AppBundle\Entity\Languages\Languages',
                'choice_label' => 'getName',
                'choice_value' => 'getIso',
            ])
            ->add('terms', CheckboxType::class, [
                'mapped'      => false,
                'constraints' => [
                    new IsTrue(),
                ],
                'label'       => 'registration.terms',
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'flendoc_registration_form';
    }

}
