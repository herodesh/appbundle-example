<?php

namespace Flendoc\AppBundle\Form\Settings\Cronjobs\Type;

use Flendoc\AppBundle\Entity\Settings\Cronjobs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CronjobsType
 * @package Flendoc\AppBundle\Form\Settings\Cronjobs\Type
 */
class CronjobsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commandName', TextType::class, [
                'label' => 'admin.settings.cronjob.command.name',
            ])
            ->add('commandDescription', TextType::class, [
                'label' => 'admin.settings.cronjob.command.description',
            ])
            ->add('hasTaskToDo', CheckboxType::class, [
                'label' => 'admin.settings.cronjob.command.has.task.to.do',
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.settings.cronjob.command.is.active',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'admin.settings.save',
                'attr'  => [
                    'class' => '_f-button-blue',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Cronjobs::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_settings_cronjobs_form';
    }
}
