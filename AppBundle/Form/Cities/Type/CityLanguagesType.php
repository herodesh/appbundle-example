<?php

namespace Flendoc\AppBundle\Form\Cities\Type;

use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CityLanguagesType
 * @package Flendoc\AppBundle\Form\Cities\Type
 */
class CityLanguagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('languages', EntityIdType::class, [
                'class' => 'Flendoc\AppBundle\Entity\Languages\Languages',
            ])
            ->add('name', TextType::class, [
                'label' => 'admin.cities.name',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Cities\CityLanguages',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_city_languages';
    }
}
