<?php

namespace Flendoc\AppBundle\Form\Cities\Type;

use Flendoc\AppBundle\Form\Cities\EventListener\AddCityLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CityType
 * @package Flendoc\AppBundle\Form\Cities\Type
 */
class CityType extends AbstractType
{
    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @var $country
     */
    protected $country;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languages = $options['languages'];
        $this->country = $options['country'];

        $builder
            ->add('cityLanguages', CollectionType::class, [
                    'entry_type'   => CityLanguagesType::class,
                    'by_reference' => false,
                ]
            )
            ->add('isActive', CheckboxType::class, [
                    'label' => 'admin.cities.active',
                ]
            );

        $builder->addEventSubscriber(new AddCityLanguagesSubscriber($this->languages, $this->country));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Flendoc\AppBundle\Entity\Cities\Cities',
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages' => null,
                'country' => null
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_cities';
    }
}
