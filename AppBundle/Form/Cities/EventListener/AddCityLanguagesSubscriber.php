<?php

namespace Flendoc\AppBundle\Form\Cities\EventListener;

use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\Cities\CityLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddCityLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Cities\EventListener
 */
class AddCityLanguagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * @var  $country
     */
    protected $country;

    /**
     * AddCountryLanguagesSubscriber constructor.
     *
     * @param array|null $languages
     */
    public function __construct($languages, $country)
    {
        $this->languages = $languages;
        $this->country   = $country;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesId = [];
        $oCity                = $oEvent->getData();

        if ($oCity->getCityLanguages()->isEmpty()) {
            $this->_addCityLanguages($oCity, $aExistingLanguagesId);
        } else {
            //get existing languages ids
            foreach ($oCity->getCityLanguages() as $oCityLanguage) {
                $aExistingLanguagesId[] = $oCityLanguage->getLanguages()->getId();
            }

            $this->_addCityLanguages($oCity, $aExistingLanguagesId);
        }

        $oEvent->setData($oCity);
    }

    /**
     * @param FormEvent $oEvent
     *
     * @return mixed
     */
    public function preSubmit(FormEvent $oEvent)
    {
        $oFormData = $oEvent->getForm()->getData();

        if (null == $oFormData->getCountry()) {
            return $oFormData->setCountry($this->country);
        }
    }

    /**
     * Add city languages
     *
     * @param Cities $oCity
     * @param array  $aExistingLanguagesId
     *
     * @return Cities
     */
    private function _addCityLanguages(Cities $oCity, array $aExistingLanguagesId)
    {
        foreach ($this->languages as $oLanguage) {
            if (!in_array($oLanguage->getId(), $aExistingLanguagesId)) {
                $oCityLanguages = new CityLanguages();

                $oCityLanguages->setLanguages($oLanguage);
                $oCityLanguages->setCities($oCity);
                $oCity->addCityLanguages($oCityLanguages);
            }
        }

        return $oCity;
    }

}
