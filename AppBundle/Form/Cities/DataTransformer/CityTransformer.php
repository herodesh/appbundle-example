<?php

namespace Flendoc\AppBundle\Form\Cities\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class CityTransformer
 * @package Flendoc\AppBundle\Form\Cities\DataTransformer
 */
class CityTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectManager
     */
    protected $manager;

    /** @var Languages */
    protected $userLanguage;

    /**
     * CityTransformer constructor.
     *
     * @param ObjectManager    $manager
     * @param Languages|string $sLanguage
     */
    public function __construct(ObjectManager $manager, $sLanguage)
    {
        $this->manager      = $manager;
        $this->userLanguage = $sLanguage;
    }

    /**
     * Transforms CityObject to json
     *
     * @param mixed $oCity
     *
     * @return string
     */
    public function transform($oCity)
    {
        if (null === $oCity) {
            return json_encode([]);
        }

        $aCity[] = [
            'id'         => $oCity->getId(),
            'text'       => $oCity->getDisplayCityName($this->userLanguage),
            'identifier' => $oCity->getIdentifier(),
        ];

        return json_encode($aCity);
    }

    /**
     * Transforms cityIdentifier to City Object
     *
     * @param mixed $sCity
     *
     * @return mixed|null
     */
    public function reverseTransform($sCity)
    {
        if (empty(json_decode($sCity))) {
            return null;
        }

        $aCity           = json_decode($sCity, true);
        $sCityIdentifier = $aCity[0]['identifier'];

        //it's always 1 entry. $aCity[0]
        $oCity = $this->manager->getRepository(Cities::class)->findOneByIdentifier($sCityIdentifier);

        return $oCity;
    }
}
