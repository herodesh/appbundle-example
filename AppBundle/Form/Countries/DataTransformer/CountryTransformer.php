<?php

namespace Flendoc\AppBundle\Form\Countries\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Entity\Countries\Countries;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class CountryTransformer
 * @package Flendoc\AppBundle\Form\Countries\DataTransformer
 */
class CountryTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /** @var bool */
    protected $jsonFormat;

    /** @var null */
    protected $language;

    /**
     * CountryTransformer constructor.
     *
     * @param ObjectManager $manager
     * @param null          $language
     * @param bool          $jsonFormat
     */
    public function __construct(ObjectManager $manager, $language = null, $jsonFormat = false)
    {
        $this->manager    = $manager;
        $this->jsonFormat = $jsonFormat;
        $this->language   = $language;
    }

    /**
     * Transforms CountryObject to json
     *
     * @param mixed $oCountry
     *
     * @return mixed|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function transform($oCountry)
    {
        if ($this->jsonFormat) {

            if (null === $oCountry) {
                return json_encode([]);
            }

            $aCountryResult = $this->manager->getRepository('AppBundle:Countries\Countries')
                                            ->findSingleCountryByIdAndUserLanguage($oCountry->getId(), $this->language);

            $aCountry[] = [
                'id'         => $aCountryResult['id'],
                'text'       => $aCountryResult['name'],
                'identifier' => $aCountryResult['identifier'],
            ];

            return json_encode($aCountry);
        } else {

            if (null === $oCountry) {
                return '[]';
            }

            if ($oCountry instanceof Countries) {
                return $oCountry->getIdentifier();
            } else {
                //simple integer
                return $oCountry;
            }
        }
    }

    /**
     * Transforms countryIdentifier to Country Object
     *
     * @param mixed $sCountry
     *
     * @return Countries
     * @throws TransformationFailedException
     */
    public function reverseTransform($sCountry)
    {
        if (empty(json_decode($sCountry))) {
            return null;
        }

        $aCountry           = json_decode($sCountry, true);
        $sCountryIdentifier = $aCountry[0]['identifier'];

        $oCountry = $this->manager->getRepository(Countries::class)->findOneByIdentifier($sCountryIdentifier);

        return $oCountry;
    }
}
