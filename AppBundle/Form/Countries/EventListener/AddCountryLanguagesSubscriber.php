<?php

namespace Flendoc\AppBundle\Form\Countries\EventListener;

use Flendoc\AppBundle\Entity\Countries\Countries;
use Flendoc\AppBundle\Entity\Countries\CountryLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddCountryLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\Countries\EventListener
 */
class AddCountryLanguagesSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    protected $languages;

    /**
     * AddCountryLanguagesSubscriber constructor.
     *
     * @param               $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oCountries            = $oEvent->getData();

        //get languages ids
        foreach ($oCountries->getCountryLanguages() as $oCountryLanguages) {
            $aExistingLanguagesIds[] = $oCountryLanguages->getLanguages()->getId();
        }
        $this->_addCountryLanguage($oCountries, $aExistingLanguagesIds);

        $oEvent->setData($oCountries);
    }

    /**
     * Add Country Languages
     *
     * @param Countries $oCountry
     * @param array     $aExistingLanguagesIds
     *
     * @return Countries
     */
    protected function _addCountryLanguage(Countries $oCountry, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oCountryLanguages = new CountryLanguages();

                $oCountryLanguages->setLanguages($oLanguage);
                $oCountryLanguages->setCountries($oCountry);
                $oCountry->addCountryLanguage($oCountryLanguages);
            }
        }

        return $oCountry;
    }
}
