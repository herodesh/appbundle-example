<?php

namespace Flendoc\AppBundle\Form\Countries\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Flendoc\AppBundle\Form\Countries\EventListener\AddCountryLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CountryType
 * @package Flendoc\AppBundle\Form\Countries\Type
 */
class CountryType extends AbstractType
{

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languages = $options['languages'];
        $this->manager   = $options['manager'];

        $builder
            ->add('countryLanguages', CollectionType::class, [
                'entry_type'   => CountryLanguagesType::class,
                'by_reference' => false,
            ])
            ->add('code', TextType::class, [
                'label' => 'admin.countries.country.code',
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'admin.countries.active',
            ]);

        $builder->addEventSubscriber(new AddCountryLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Flendoc\AppBundle\Entity\Countries\Countries',
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
            'languages'  => null,
            'manager'    => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_countries';
    }
}
