<?php

namespace Flendoc\AppBundle\Form\MedicalCases\EventListener;

use Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserAction;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserActionLanguages;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddMedicalCaseUserActionLanguagesSubscriber
 * @package Flendoc\AppBundle\Form\MedicalCases\EventListener
 */
class AddMedicalCaseUserActionLanguagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var array
     */
    protected $languages;

    /**
     * AddSkillLanguagesSubscriber constructor.
     *
     * @param $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $oEvent
     */
    public function preSetData(FormEvent $oEvent)
    {
        $aExistingLanguagesIds = [];
        $oMedicalCaseUserAction  = $oEvent->getData();

        if ($oMedicalCaseUserAction->getMedicalCasesUserActionLanguages()->isEmpty()) {
            $this->_addMedicalCaseUserActionLanguage($oMedicalCaseUserAction, $aExistingLanguagesIds);
        } else {
            //get languages id's
            foreach ($oMedicalCaseUserAction->getMedicalCasesUserActionLanguages() as $oMedicalCaseUserActionLanguages) {
                $aExistingLanguagesIds[] = $oMedicalCaseUserActionLanguages->getLanguages()->getId();
            }
            $this->_addMedicalCaseUserActionLanguage($oMedicalCaseUserAction, $aExistingLanguagesIds);
        }

        $oEvent->setData($oMedicalCaseUserAction);
    }

    /**
     * Add MedicalCaseUserAction Languages
     *
     * @param MedicalCasesUserAction $oMedicalCaseUserAction
     * @param array                  $aExistingLanguagesIds
     *
     * @return MedicalCasesUserAction
     */
    protected function _addMedicalCaseUserActionLanguage(MedicalCasesUserAction $oMedicalCaseUserAction, array $aExistingLanguagesIds)
    {

        foreach ($this->languages as $oLanguage) {

            if (!in_array($oLanguage->getId(), $aExistingLanguagesIds)) {
                $oMedicalCaseUserActionLanguages = new MedicalCasesUserActionLanguages();

                $oMedicalCaseUserActionLanguages->setLanguages($oLanguage);
                $oMedicalCaseUserActionLanguages->setMedicalCasesUserAction($oMedicalCaseUserAction);

                $oMedicalCaseUserAction->addMedicalCasesUserActionLanguage($oMedicalCaseUserActionLanguages);
            }
        }

        return $oMedicalCaseUserAction;
    }
}
