<?php

namespace Flendoc\AppBundle\Form\MedicalCases\Type;

use Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserAction;
use Flendoc\AppBundle\Form\MedicalCases\EventListener\AddMedicalCaseUserActionLanguagesSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MedicalCaseUserActionType
 * @package Flendoc\AppBundle\Form\Skills\Type
 */
class MedicalCaseUserActionType extends AbstractType
{

    /**
     * @var $languages
     */
    protected $languages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->languages = $options['languages'];

        $builder
            ->add('medicalCasesUserActionLanguages', CollectionType::class, [
                'entry_type'   => MedicalCaseUserActionLanguagesType::class,
                'by_reference' => false,
            ]);

        $builder->addEventSubscriber(new AddMedicalCaseUserActionLanguagesSubscriber($this->languages));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => MedicalCasesUserAction::class,
                'attr'       => [
                    'novalidate' => 'novalidate',
                ],
                'languages'  => null,
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_medical_case_action';
    }
}
