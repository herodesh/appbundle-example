<?php

namespace Flendoc\AppBundle\Form\MedicalCases\Type;

use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalCases\MedicalCasesUserActionLanguages;
use Flendoc\AppBundle\Form\App\Type\EntityIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MedicalCaseUserActionLanguagesType
 * @package Flendoc\AppBundle\Form\Skills\Type
 */
class MedicalCaseUserActionLanguagesType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('languages', EntityIdType::class, [
                'class' => Languages::class,
            ])
            ->add('description', TextType::class, [
                'label' => 'admin.medical.cases.description',
            ])
            ->add('userNotificationMessage', TextareaType::class, [
                'label' => 'admin.medical.cases.user.action.notification.message',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MedicalCasesUserActionLanguages::class,
            'attr'       => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_medical_cases_action_languages';
    }
}
