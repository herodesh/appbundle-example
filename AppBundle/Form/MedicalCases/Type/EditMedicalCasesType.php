<?php

namespace Flendoc\AppBundle\Form\MedicalCases\Type;

use Doctrine\ORM\EntityManager;
use Flendoc\AppBundle\Constants\AppConstants;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\MedicalCases\PendingMedicalCases;
use Flendoc\AppBundle\Form\Skills\DataTransformer\DoctorsSkillTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class EditMedicalCasesType
 * @package Flendoc\AppBundle\Form\SecondOpinion\Type
 */
class EditMedicalCasesType extends AbstractType
{

    /** @var EntityManager */
    protected $entityManager;

    /** @var Languages */
    protected $userLanguage;

    /** @var Languages */
    protected $availableLanguages;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager entityManager */
        $this->entityManager = $options['entityManager'];
        /** @var Languages language */
        $this->userLanguage = $options['userLanguage'];
        /** @var Languages availableLanguages */
        $this->availableLanguages = $options['availableLanguages'];

        $aChoices = [];

        foreach ($this->availableLanguages as $availableLanguage) {
            $aChoices[$availableLanguage['name']] =
                $availableLanguage['iso'];
        }

        $builder
            ->add('medicalCaseLanguageIso', ChoiceType::class, [
                'label'   => 'doctors.medical.cases.language',
                'choices' => $aChoices,
            ])
            ->add('title', TextType::class, [
                'label'              => 'doctors.medical.cases.title',
                'translation_domain' => 'messages',
                'constraints'        => [
                    new NotNull(),
                ],
            ])
            ->add('description', TextareaType::class, [
                'label'              => 'doctors.medical.cases.case.description',
                'attr'               => [
                    'class'       => 'tinymce',
                    'placeholder' => 'doctors.medical.cases.case.description.placeholder',
                ],
                'constraints'        => [
                    new NotNull(),
                    new Length([
                        'min'        => AppConstants::MEDICAL_CASE_MINIMUM_DESCRIPTION_CHARACTERS,
                        'minMessage' => 'validator.medical.cases.minimum.characters.description',
                    ]),
                ],
                'translation_domain' => 'messages',
            ])
            ->add('investigationResults', TextareaType::class, [
                'label'              => 'doctors.medical.cases.case.investigation.results',
                'attr'               => [
                    'class' => 'tinymce',
                ],
                'translation_domain' => 'messages',
            ])
            ->add('sources', TextareaType::class, [
                'label'              => 'doctors.medical.cases.case.sources',
                'attr' => [
                    'placeholder' => 'doctors.medical.cases.case.sources.description'
                ],
                'translation_domain' => 'messages',
            ])
            ->add('medicalCaseSkills', TextType::class, [
                'label'              => 'doctors.medical.cases.case.categories',
                'attr'               => [
                    'style' => 'display:none',
                    'class' => '_f-full-width',
                ],
                'constraints'        => [
                    new NotNull(),
                ],
                'translation_domain' => 'messages',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'doctors.medical.cases.save.draft',
                'attr'  => [
                    'class' => '_f-button-blue',
                ],
            ]);

        $builder->get('medicalCaseSkills')
                ->addModelTransformer(new DoctorsSkillTransformer($this->entityManager, $this->userLanguage));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => PendingMedicalCases::class,
            'attr'               => [
                'novalidate' => 'novalidate',
            ],
            'entityManager'      => null,
            'userLanguage'       => null,
            'availableLanguages' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'edit_doctor_medical_cases';
    }
}
