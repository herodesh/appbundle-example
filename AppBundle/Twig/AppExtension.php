<?php

namespace Flendoc\AppBundle\Twig;

use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Manager\Resumes\ResumeManager;
use Flendoc\AppBundle\Utils\TextUrlConvertor;
use Flendoc\AppBundle\Utils\TextUrlPlaceholderParser;
use Symfony\Component\Translation\Translator;

/**
 * Class AppExtension
 * @package Flendoc\AppBundle\Twig
 *
 * See below image functions with base64 images
 */
class AppExtension extends \Twig_Extension
{

    /** @var array */
    protected $appDefaults;

    /** @var Translator */
    protected $translator;

    /** @var ResumeManager */
    protected $resumeManager;

    /** @var TextUrlPlaceholderParser */
    protected $textUrlPlaceholderParser;

    /** @var TextUrlConvertor */
    protected $textUrlConvertor;

    /**
     * AppExtension constructor.
     *
     * @param                          $appDefaults
     * @param Translator               $translator
     * @param ResumeManager            $resumeManager
     * @param TextUrlPlaceholderParser $textUrlPlaceholderParser
     * @param TextUrlConvertor         $textUrlConvertor
     */
    public function __construct(
        $appDefaults,
        Translator $translator,
        ResumeManager $resumeManager,
        TextUrlPlaceholderParser $textUrlPlaceholderParser,
        TextUrlConvertor $textUrlConvertor
    ) {
        $this->appDefaults              = $appDefaults;
        $this->translator               = $translator;
        $this->resumeManager            = $resumeManager;
        $this->textUrlPlaceholderParser = $textUrlPlaceholderParser;
        $this->textUrlConvertor         = $textUrlConvertor;
    }

    /**
     * Defines filters
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('academicTitle', [$this, 'academicTitle']),
            new \Twig_SimpleFilter('getControllerName', [$this, 'getControllerName']),
            new \Twig_SimpleFilter('calculateDateDiff', [$this, 'calculateDateDiff']),
            new \Twig_SimpleFilter('calculateDateDiffAuto', [$this, 'calculateDateDiffAuto']),
            new \Twig_SimpleFilter('displayTimeDivisions', [$this, 'displayTimeDivisions']),
            //@TODO move this to it's specific extension - eg. ResumeExtension.php?
            new \Twig_SimpleFilter('resumeOtherInfoSectionTitles', [$this, 'resumeOtherInfoSectionTitles']),
            new \Twig_SimpleFilter('json_decode', [$this, 'json_decode']),
            new \Twig_SimpleFilter('convertUrls', [$this, 'convertUrls']),
            new \Twig_SimpleFilter('textUrlPlaceholderParser', [$this, 'textUrlPlaceholderParser']),
            new \Twig_SimpleFilter('instanceof', [$this, 'instanceof']),

        ];
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('twigDie', [$this, 'twigDie']),
            new \Twig_SimpleFunction('s3Url', [$this, 's3Url']),
            new \Twig_SimpleFunction('displayCountryNameByIso', [$this, 'displayCountryNameByIso']),
        ];
    }

    /**
     * @param string  $sString
     * @param boolean $bIsLink
     *
     * @return mixed
     */
    public function textUrlPlaceholderParser($sString, $bIsLink = true)
    {
        return $this->textUrlPlaceholderParser->parseText($sString, $bIsLink);
    }

    /**
     * Display a country name by it's locale
     *
     * @param $sString
     * @param $sLocale
     *
     * @return string
     */
    public function displayCountryNameByIso($sString, $sLocale)
    {
        return \Locale::getDisplayRegion($sString, $sLocale);
    }

    /**
     * Gets s3 folders in twig where needed
     *
     * @param string $sDirectory
     *
     * @return string
     */
    public function s3Url($sDirectory)
    {
        return AppStatic::getS3StorageFolder($sDirectory, $this->appDefaults);
    }

    /**
     * This is a test function in case we need to test object instances in twig file
     *
     * @param object $object
     * @param string $class
     *
     * @return bool
     * @throws \ReflectionException
     */
    public function instanceof($object, $class)
    {
        $reflectionClass = new \ReflectionClass($class);

        return $reflectionClass->isInstance($object);
    }

    /**
     * Method that finds different occurrences of urls or email addresses in a string.
     * Inspired from:
     * @link https://github.com/liip/LiipUrlAutoConverterBundle/blob/master/Extension/UrlAutoConverterTwigExtension.php
     *
     * @param string $string input string
     *
     * @return string with replaced links
     */
    public function convertUrls($string)
    {
        return $this->textUrlConvertor->convertUrls($string);
    }

    /**
     * Decode Json string inside a twig file
     *
     * @param $encodedString
     *
     * @return mixed
     */
    public function json_decode($encodedString)
    {
        return json_decode($encodedString, true);
    }

    /**
     * Displays the resume "Other Information" section translated titles
     *
     * @param $otherInfoSectionKey
     * @param $languageIso
     *
     * @return string
     */
    public function resumeOtherInfoSectionTitles($otherInfoSectionKey, $languageIso)
    {
        return $this->resumeManager->otherInformationTitleFromSectionType($otherInfoSectionKey, $languageIso);
    }

    /**
     * Displays the time difference from date A to date B (ex: 2 years and 2 months)
     *
     * @param        $startDate
     * @param string $endDate
     * @param string $return Return the type of the response (eg. y - year, m - month, d - day)
     *
     * @return mixed
     */
    public function calculateDateDiff($startDate, $endDate = 'now', $return = 'y')
    {
        if (!$endDate instanceof \DateTime) {
            $endDate = date_create($endDate);
        }

        return date_diff($startDate, $endDate)->$return;
    }

    /**
     * Displays the time difference from date A to date B (ex: 2 years ago/ 2 months ago etc. )
     *
     * @param        $startDate
     * @param string $endDate
     *
     * @return mixed
     */
    public function calculateDateDiffAuto($startDate, $endDate = 'now')
    {
        if (!$endDate instanceof \DateTime) {
            $endDate = date_create($endDate);
        }

        $dateDiff = date_diff($startDate, $endDate);

        $y = $dateDiff->y;
        $m = $dateDiff->m;
        $d = $dateDiff->d;
        $h = $dateDiff->h;

        if ($y) {
            return $this->translator->transChoice('site.date.years.ago', $y, [], 'messages');
        }

        if ($m) {
            return $this->translator->transChoice('site.date.months.ago', $m, [], 'messages');
        }

        if ($d) {
            return $this->translator->transChoice('site.date.days.ago', $d, [], 'messages');
        }

        if ($h) {
            return $this->translator->transChoice('site.date.hours.ago', $h, [], 'messages');
        }

        return $this->translator->trans('site.date.minutes.ago');
    }

    /**
     * @TODO check why this is a duplicate with AppStatic::displayTimeDivisions (because of the translator?)
     *
     * Display singular or plural for months based on the count
     *
     * @param int    $count
     * @param string $subdivision
     *
     * @return string
     */
    public function displayTimeDivisions($count = 0, $subdivision = 'y')
    {
        switch ($subdivision) {
            case 'd':
                return ($count > 1) ? 'site.date.days' : 'site.date.day';
                break;

            case 'm':
                return ($count > 1) ? 'site.date.months' : 'site.date.month';
                break;

            default:
                return ($count > 1) ? 'site.date.years' : 'site.date.year';
                break;
        }
    }

    /**
     * Gets controller name inside a twig template
     *
     * @param $sControllerPath
     *
     * @return bool|string
     */
    public function getControllerName($sControllerPath)
    {
        return AppStatic::getControllerName($sControllerPath);
    }

    /**
     * Stops twig execution. Use for Debug purposes
     */
    public function twigDie()
    {
        die();
    }

    /**
     * Add translated display text to the academic titles
     *
     * @param $user
     *
     * @return bool
     */
    public function academicTitle($user)
    {
        $academicTitles = [
            'dr'         => 'site.title.dr',
            'assist'     => 'site.title.assist',
            'assoc_prof' => 'site.title.assocProf',
            'prof'       => 'site.title.prof',
        ];

        foreach ($academicTitles as $key => $academicTitle) {
            if ($key == $user) {
                return $academicTitle;
                continue;
            }
        }

        return false;
    }

    /**
     * Extension name
     *
     * @return string
     */
    public function getName()
    {
        return 'app_extension';
    }
}
