<?php

namespace Flendoc\AppBundle\Twig;

use Doctrine\ODM\MongoDB\DocumentManager;
use Flendoc\AppBundle\Document\Comments\MongoComment;
use Flendoc\AppBundle\Document\Comments\MongoThread;
use Flendoc\AppBundle\Manager\Notifications\NotificationManager;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;
use Flendoc\AppBundle\Repository\Comments\MongoCommentRepository;
use Flendoc\AppBundle\Repository\Comments\MongoThreadRepository;
use Flendoc\AppBundle\Traits\App\ObjectKeyActionTrait;
use Flendoc\AppBundle\Traits\Contacts\ContactRequestDefaultStatusesTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Translator;

/**
 * Class NotificationsExtension
 *
 * @package Flendoc\AppBundle\Twig
 */
class NotificationsExtension extends \Twig_Extension
{
    use ObjectKeyActionTrait;
    use ContactRequestDefaultStatusesTrait;

   /** @var DocumentManager */
    protected $odm;

    /** @var Translator */
    protected $translator;

    /** @var UrlGeneratorInterface */
    protected $generator;

    /**
     * NotificationsExtension constructor.
     *
     * @param DocumentManager       $odm
     * @param Translator            $translator
     * @param UrlGeneratorInterface $generator
     */
    public function __construct(
        DocumentManager $odm,
        Translator $translator,
        UrlGeneratorInterface $generator
    ) {
        $this->odm        = $odm;
        $this->translator = $translator;
        $this->generator  = $generator;
    }

    /**
     * Defines functions
     *
     * @return array|\Twig_Filter[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('displayNotificationMessage', [$this, 'displayNotificationMessage']),
        ];
    }

    /**
     * @param string      $sUserIdentifier
     * @param string      $sUserName
     * @param string      $sNotificationType
     * @param string      $sDependencyIdentifier
     * @param string      $sDependencyType
     * @param string|null $sObjectIdentifier
     *
     * @return mixed
     */
    public function displayNotificationMessage(
        $sUserIdentifier,
        $sUserName,
        $sNotificationType,
        $sDependencyIdentifier,
        $sDependencyType,
        $sObjectIdentifier
    ) {
        switch ($sNotificationType) {
            case NotificationManager::NEW_CONTACT_REQUEST_NOTIFICATION:
                return $this->translator->trans('doctors.notifications.new-contact-request', ['%username%' => $sUserName], 'messages');

                break;
            case NotificationManager::NEW_CONTACT_REQUEST_ACCEPTED_NOTIFICATION:
                return $this->translator->trans('doctors.notifications.new-contact-request-accepted', ['%username%' => $sUserName], 'messages');

                break;
            case NotificationManager::NEW_COMMENT_LIKE_NOTIFICATION:
                return $this->translator->trans('doctors.notifications.new-comment-like', ['%username%' => $sUserName], 'messages');

                break;
            case NotificationManager::NEW_LIKE_NOTIFICATION:

                switch ($sDependencyType) {
                    case ObjectTypeConstants::OBJECT_MEDICAL_CASE:
                        return $this->translator->trans('doctors.notifications.new-medical-case-like', ['%username%' => $sUserName], 'messages');
                    case ObjectTypeConstants::OBJECT_MONGO_FEED:
                        return $this->translator->trans('doctors.notifications.new-post-like', ['%username%' => $sUserName], 'messages');
                    default:
                        return;
                }

                break;
            case NotificationManager::NEW_COMMENT_NOTIFICATION:
                /** @var MongoThreadRepository $oMongoThreadRepository */
                $oMongoThreadRepository = $this->odm->getRepository(MongoThread::class);
                /** @var MongoCommentRepository $oMongoCommentRepository */
                $oMongoCommentRepository = $this->odm->getRepository(MongoComment::class);

                /** @var MongoThread $oMongoThread */
                $oMongoThread = $oMongoThreadRepository
                    ->findOneBy([
                        'dependencyIdentifier' => $sDependencyIdentifier,
                    ])
                ;

                /** @var MongoComment $oMongoComment */
                $oMongoComment = $oMongoCommentRepository
                    ->findOneBy([
                        'identifier' => $sObjectIdentifier
                    ])
                ;

                switch ($sDependencyType) {
                    case ObjectTypeConstants::OBJECT_MEDICAL_CASE:
                        if ($oMongoComment && $oMongoComment->getParent()) {
                            return $this->translator->trans('doctors.notification.new-reply-comment', ['%username%' => $sUserName], 'messages');
                        }

                        if ($oMongoThread && $oMongoThread->getUserIdentifier() === $sUserIdentifier) {
                            return $this->translator->trans('doctors.notification.new-medical-case-comment', ['%username%' => $sUserName], 'messages');
                        }

                        return $this->translator->trans('doctors.notification.new-medical-case-comment-all-users', ['%username%' => $sUserName], 'messages');
                    case ObjectTypeConstants::OBJECT_MONGO_FEED:
                        if ($oMongoComment && $oMongoComment->getParent()) {
                            return $this->translator->trans('doctors.notification.new-reply-comment', ['%username%' => $sUserName], 'messages');
                        }

                        if ($oMongoThread && $oMongoThread->getUserIdentifier() === $sUserIdentifier) {
                            return $this->translator->trans('doctors.notifications.new-post-comment', ['%username%' => $sUserName], 'messages');
                        }

                        return $this->translator->trans('doctors.notification.new-post-comment-all-users', ['%username%' => $sUserName], 'messages');
                    default:
                        return;
                }
                break;
            default:
                return;
        }
    }
}
