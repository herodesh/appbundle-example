<?php

namespace Flendoc\AppBundle\Twig;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Flendoc\AppBundle\Entity\Cities\Cities;
use Flendoc\AppBundle\Entity\Countries\Countries;
use Flendoc\AppBundle\Entity\Languages\Languages;
use Flendoc\AppBundle\Entity\Skills\Skills;
use Symfony\Component\Validator\Constraints\Language;

/**
 * Class TranslatedElementsExtension
 * @package Flendoc\AppBundle\Twig
 *
 * Everything related to translated elements (country names, city names, skills names, etc)
 */
class TranslatedElementsExtension extends \Twig_Extension
{
    /** @var EntityManager */
    protected $em;

    /**
     * TranslatedElementsExtension constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Defines filters
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('displayCountryName', [$this, 'displayCountryName']),
            new \Twig_SimpleFilter('displayCityName', [$this, 'displayCityName']),
            new \Twig_SimpleFilter('displaySkillsName', [$this, 'displaySkillsName']),

        ];
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('displaySkillNameByIdentifier', [$this, 'displaySkillNameByIdentifier']),
        ];
    }

    /**
     * Display country Name, Iso and Id
     *
     * @param Countries      $oCountry
     * @param Languages|null $oLanguage
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function displayCountryName(Countries $oCountry, Languages $oLanguage = null)
    {
        $oCountryRepository = $this->em->getRepository('AppBundle:Countries\Countries');

        $aCountryData = $oCountryRepository->findSingleCountryByIdAndUserLanguage($oCountry->getId(), $oLanguage);

        return $aCountryData;
    }

    /**
     * Display City name based on country main language
     *
     * @param Cities         $oCity
     * @param Countries|null $oCountry
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function displayCityName(Cities $oCity, Countries $oCountry = null)
    {
        $oCityRepository = $this->em->getRepository(Cities::class);

        $aCityData = $oCityRepository->findSingleCityByCountryMainSpokenLanguage($oCountry, $oCity);

        return $aCityData;
    }

    /**
     * Returns an array of skills names and ids
     *
     * @param PersistentCollection $aSkills
     * @param Languages|null       $oLanguages
     *
     * @return array
     */
    public function displaySkillsName(PersistentCollection $aSkills, Languages $oLanguages = null)
    {
        $oSkillsRepo = $this->em->getRepository(Skills::class);

        $aSkillsData = $oSkillsRepo->findByLanguageAndId($oLanguages, $aSkills);

        return $aSkillsData;
    }

    /**
     * Return single skill name based on user language
     *
     * @param Languages $oLanguage
     * @param string $sSkillIdentifier
     *
     * @return array
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function displaySkillNameByIdentifier(Languages $oLanguage, $sSkillIdentifier)
    {
        $oSkillsRepo = $this->em->getRepository(Skills::class);

        return $oSkillsRepo->findByLanguageAndIdentifier($oLanguage, $sSkillIdentifier);
    }

    /**
     * Extension name
     *
     * @return string
     */
    public function getName()
    {
        return 'translated_elements_extension';
    }
}
