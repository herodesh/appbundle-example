<?php

namespace Flendoc\AppBundle\Twig;

use Flendoc\AppBundle\AppStatic;
use Flendoc\AppBundle\Constants\FeedConstants;
use Flendoc\AppBundle\Constants\ObjectTypeConstants;

/**
 * Class NewsFeedExtension
 * @package Flendoc\AppBundle\Twig
 */
class NewsFeedExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    protected $rexUrlLinker;

    /**
     * Defines filters
     *
     * @return array|\Twig_Filter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('makeUrlsClickable', [$this, 'makeUrlsClickable']),
        ];
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('displayPrivacyIcon', [$this, 'displayPrivacyIcon']),
        ];
    }

    /**
     * Returns the css class for the specific icon
     *
     * @param $sText
     *
     * @return string|void
     */
    public function displayPrivacyIcon($sText)
    {
        if ($sText == FeedConstants::FEED_PROPERTY_PUBLIC) {
            return 'fa fa-globe';
        }

        if ($sText == FeedConstants::FEED_PROPERTY_CONTACTS_ONLY) {
            return 'fa fa-users';
        }

        return;
    }

    /**
     * Transforms plain text into valid HTML, escaping special characters and
     * turning URLs into links.
     *
     * @param string $text
     *
     * @return string
     */
    public function makeUrlsClickable($text)
    {
        if (!is_string($text)) {
            return;
        }

        $text = html_entity_decode($text);
        $text = " ".htmlspecialchars($text);
        $text = preg_replace("/(^|[\n ])([\w]*?)([\w]*?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a target='_blank' href=\"$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((www|wap)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a target='_blank' href=\"http://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a target='_blank' href=\"$4://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a target='_blank' href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $text = preg_replace("/(^|[\n ])(mailto:[a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a target='_blank' href=\"$2@$3\">$2@$3</a>", $text);
        $text = preg_replace("/(^|[\n ])(skype:[^ \,\"\t\n\r<]*)/i", "$1<a target='_blank' href=\"$2\">$2</a>", $text);

        return $text;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'doctor_news_feed_extension';
    }

}
