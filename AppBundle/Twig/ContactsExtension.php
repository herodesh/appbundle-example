<?php

namespace Flendoc\AppBundle\Twig;

use Flendoc\AppBundle\Adapter\Neo4j\Neo4JManager;
use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ContactsExtension
 * @package Flendoc\AppBundle\Twig
 */
class ContactsExtension extends \Twig_Extension
{
    /** @var RegistryInterface */
    protected $doctrine;

    /** @var Neo4JManager */
    protected $neoManager;

    /**
     * ContactsExtension constructor.
     *
     * @param RegistryInterface $doctrine
     * @param Neo4JManager      $neoManager
     */
    public function __construct(RegistryInterface $doctrine, Neo4JManager $neoManager)
    {
        $this->doctrine   = $doctrine;
        $this->neoManager = $neoManager;
    }

    /**
     * Defines functions
     *
     * @return array|\Twig_Filter[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('detectUserFriend', [$this, 'detectUserFriend']),
            new \Twig_SimpleFunction('detectGhostUser', [$this, 'detectGhostUser']),
        ];
    }

    /**
     * Determines if a user is a ghostUser
     *
     * @param $sUserIdentifier
     *
     * @return mixed
     */
    public function detectGhostUser($sUserIdentifier)
    {
        $oUser = $this->doctrine->getRepository(Doctors::class)->findOneBy([
            'identifier' => $sUserIdentifier,
        ]);

        return $oUser->getIsGhostUser();
    }

    /**
     * If user has a friend then we will display a friend menu otherwise option to add the user
     *
     * @param $sUserIdentifier
     * @param $sContactIdentifier
     *
     * @return bool
     */
    public function detectUserFriend($sUserIdentifier, $sContactIdentifier)
    {
        $response = $this->neoManager->client
            ->run('
                    MATCH(n {identifier: {sUserIdentifier}})-[:isFriendWith]-(:Doctor {identifier: {sContactIdentifier}})
                    RETURN DISTINCT n.identifier as identifier
                ', [
                    'sUserIdentifier'    => $sUserIdentifier,
                    'sContactIdentifier' => $sContactIdentifier,
                ]
            );

        return !empty($response->getRecords());
    }
}
