<?php

namespace Flendoc\AppBundle\Twig;

use Flendoc\AppBundle\Entity\Doctors\Doctors;
use Flendoc\AppBundle\Manager\FlendocManager;
use Flendoc\AppBundle\Security\Voter\UserCanSeeVoter;
use Flendoc\DoctorsBundle\Traits\TypeIdentifierProfileTrait;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\Translator;

/**
 * Class DoctorProfileExtension
 * @package Flendoc\AppBundle\Twig
 */
class DoctorProfileExtension extends \Twig_Extension
{
    use TypeIdentifierProfileTrait;

    /** @var RegistryInterface */
    protected $doctrine;

    /** @var FlendocManager */
    protected $flendocManager;

    /** @var Router */
    protected $router;

    /** @var AuthorizationCheckerInterface */
    protected $authChecker;

    /** @var TokenStorage */
    protected $tokenStorage;

    /** @var Translator */
    protected $translator;

    /** @var array */
    protected $appDefaults;

    /**
     * DoctorProfileExtension constructor.
     *
     * @param RegistryInterface             $doctrine
     * @param FlendocManager                $flendocManager
     * @param Router                        $router
     * @param AuthorizationCheckerInterface $authChecker
     * @param TokenStorage                  $tokenStorage
     * @param Translator                    $translator
     * @param                               $appDefaults
     */
    public function __construct(
        RegistryInterface $doctrine,
        FlendocManager $flendocManager,
        Router $router,
        AuthorizationCheckerInterface $authChecker,
        TokenStorage $tokenStorage,
        Translator $translator,
        $appDefaults
    ) {
        $this->doctrine       = $doctrine;
        $this->flendocManager = $flendocManager;
        $this->router         = $router;
        $this->authChecker    = $authChecker;
        $this->tokenStorage   = $tokenStorage;
        $this->translator     = $translator;
        $this->appDefaults    = $appDefaults;
    }

    /**
     * Defines filters
     *
     * @return array|\Twig_Filter[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('displayUserLocation', [$this, 'displayUserLocation']),
            new \Twig_SimpleFunction('displayUserName', [$this, 'displayUserName']),
            new \Twig_SimpleFunction('displayAndLinkUserName', [$this, 'displayAndLinkUserName']),
            new \Twig_SimpleFunction('displayAcademicTitle', [$this, 'displayAcademicTitle']),
        ];
    }

    /**
     * Display images that are returned in a JsonFormat for a specified user
     *
     * @param string $sUserIdentifier
     * @param string $languageIso
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function displayUserLocation($sUserIdentifier, $languageIso, $type = null)
    {
        //get user profile from redis if not regenerate it
        $aResult = $this->doctrine->getRepository(Doctors::class)->findUserLocation($sUserIdentifier, $languageIso);
        $sName   = '';

        if ('country' === $type) {
            return $aResult['countryName'];
        }

        if ('city' === $type) {
            return $aResult['cityName'];
        }

        if ($aResult['countryName']) {
            $sName = $aResult['countryName'];
            if ($aResult['cityName']) {
                $sName .= ', '.$aResult['cityName'];
            }
        }

        return $sName;
    }

    /**
     * @param string $sUserIdentifier
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function displayUserName($sUserIdentifier)
    {
        $currentLoggedInUser = $this->tokenStorage->getToken()->getUser();
        $aUserProfile = $this->getOrRegenerateRedisUserProfile($sUserIdentifier, $this->flendocManager, $this->doctrine);

        if (!$this->authChecker->isGranted(UserCanSeeVoter::USER_CAN_SEE) && $sUserIdentifier != $currentLoggedInUser->getIdentifier()) {
            return $this->translator->trans('doctors.permissions.resource.not.available');
        }

        return sprintf('%s %s %s', $aUserProfile['academicTitle'], $aUserProfile['firstName'], $aUserProfile['lastName']);
    }

    /**
     * @param string $sUserIdentifier
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function displayAndLinkUserName($sUserIdentifier)
    {
        $currentLoggedInUser = $this->tokenStorage->getToken()->getUser();

        if (
            'anon.' === $currentLoggedInUser ||
            !$this->authChecker->isGranted(UserCanSeeVoter::USER_CAN_SEE) && $sUserIdentifier != $currentLoggedInUser->getIdentifier()

        ) {
            return $this->translator->trans('doctors.permissions.resource.not.available');
        }

        $aUserProfile  = $this->getOrRegenerateRedisUserProfile($sUserIdentifier, $this->flendocManager, $this->doctrine);
        $sFullUserName = sprintf('%s %s %s', $aUserProfile['academicTitle'], $aUserProfile['firstName'], $aUserProfile['lastName']);
        $html          = '<a href="'.$this->router->generate('doctor_profile_view', ['sUserIdentifier' => $sUserIdentifier]).'">';
        $html          .= $sFullUserName;
        $html          .= '</a>';

        return $html;
    }

    /**
     * @param string $sUserIdentifier
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function displayAcademicTitle($sUserIdentifier)
    {
        $aUserProfile = $this->getOrRegenerateRedisUserProfile($sUserIdentifier, $this->flendocManager, $this->doctrine);

        return $aUserProfile['academicTitle'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'doctor_profile_extension';
    }

}
